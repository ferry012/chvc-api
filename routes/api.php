<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('ping', 'Member\PosController@ping')->name('pos.ping');
Route::post('/token/refresh', 'Auth\LoginController@refresh')->name('login.refresh');

// QR Login
Route::middleware(['role1:qr-login'])->group(function () {
    Route::post('/qr/login', 'Member\QrloginController@qr_login');
    Route::post('/qr/init', 'Member\QrloginController@init_qr');
    Route::post('/qr/check', 'Member\QrloginController@check_qr');
});
// mbr rebates
Route::middleware(['role1:member-points'])->group(function () {
    Route::post('/points/reserved', 'Member\PosController@points_reserved')->name('pos.points.reserved');
    Route::post('/points/unreserved', 'Member\PosController@points_unreserved')->name('pos.points.unreserved');
    Route::post('/points/award', 'Member\PosController@points_award')->name('pos.points.award');
    Route::post('/points/deduct', 'Member\PosController@points_deduct')->name('pos.points.deduct');
    Route::get('/points/check', 'Member\PosController@points_check')->name('pos.points.check');
    Route::get('/points/available/{mbrId}', 'Member\PosController@points_available')->name('pos.points.available');
    /* deprecated */
    Route::post('reserved', 'Member\PosController@reserved')->name('pos.reserved');
    Route::post('unreserved', 'Member\PosController@unreserved')->name('pos.unreserved');
});
// mbr Transaction
Route::middleware(['role1:member-transaction'])->group(function () {
    Route::post('/transaction', 'Member\PosController@transaction')->name('pos.transaction');
    Route::get('/transaction/item', 'Member\PosController@transaction_item')->name('pos.transaction_item');
    Route::get('/transaction/check_transaction/{trans_id}', 'Member\PosController@check_transaction')->name('pos.check_transaction');
    Route::get('/transaction/items/{trans_id}/{item_id}', 'Member\PosController@get_transaction_item')->name('pos.get_transaction_item');
    Route::get('transaction/redemptions/{mbrId}/{from?}/{to?}', 'Member\HachiController@redemptions')->name('hachi.transaction.redemptions');
    Route::get('transaction/retrieve/{mbrId}/{from?}/{to?}', 'Member\HachiController@personalTransaction')->name('hachi.transaction.personalTransaction');

    Route::get('/membership/activate/{mbr_type}/{mbr_id}/{trans_id}', 'Member\TransactionController@member_activate')->name('pos.transaction.member_activate');
    Route::get('/membership/renew/{mbr_type}/{mbr_id}/{trans_id}', 'Member\TransactionController@member_renew')->name('pos.transaction.member_renew');
    Route::get('/membership/upgrade/{mbr_type}/{mbr_id}/{trans_id}', 'Member\TransactionController@member_upgrade')->name('pos.transaction.member_upgrade');
    Route::get('/membership/expire/{mbr_id}', 'Member\TransactionController@member_deactivate')->name('pos.transaction.member_deactivate');
});

// membership
Route::middleware(['role1:member-register'])->group(function () {
    Route::get('/member_item', 'Member\RegisterController@getMemberItem');
    Route::get('/member_item_pos', 'Member\RegisterController@getMemberItemPos');

    Route::get('/validate/{memberId}', 'Member\PosController@getMemberInfo')->name('pos.validate.info');
    Route::get('/validate_mvip/{memberId}', 'Member\PosController@getMvipInfo');
    Route::post('/update_member', 'Member\CorporateController@update_member')->name('pos.update_member');

    Route::post('/register', 'Member\PosController@register')->name('pos.register');
    Route::post('/otp_register', 'Member\RegisterController@register_otp');

    Route::post('/corporate_register', 'Member\CorporateController@corporate_register')->name('pos.corporate_register');
    Route::post('/corporate_associate_register', 'Member\CorporateController@corporate_associate_register')->name('pos.corporate_associate_register');
    Route::post('/cust_register/{cust_id}', 'Member\CorporateController@cust_register')->name('pos.cust_register');
    Route::get('/get_corporate_associate/{main_id}', 'Member\CorporateController@get_corporate_associate')->name('pos.get_corporate_associate');
    Route::get('/latest_register/{main_id}', 'Member\CorporateController@latest_register')->name('pos.latest_register');
    Route::get('/corporate/all_cust', 'Member\CorporateController@all_cust')->name('pos.all_cust');
    Route::get('/get_cust/{cust_id}', 'Member\CorporateController@get_cust')->name('pos.get_cust');
    Route::get('/corporate/profile/{mbrId}', 'Member\CorporateController@corporateProfile')->name('hachi.member.corporateProfile');
});
Route::middleware(['role1:member-details'])->group(function () {
    Route::get('/member/emailPreference/{mbrId}', 'Member\HachiController@emailPreference')->name('hachi.member.emailPreference');
    Route::post('/member/emailPreference', 'Member\HachiController@editEmailPreference')->name('hachi.member.editEmailPreference');
    Route::get('/member/verify/{mbrId}/{pwd}/{pwd2}', 'Member\PosController@verifyMember')->name('pos.member.verifyMember');
    Route::post('/member/chckMbrPwd2/{mbrId}/{pwd2}', 'Member\PosController@chckMbrPwd2Member')->name('pos.member.chckMbrPwd2');
    Route::get('/member/profileDetails/{mbrId}', 'Member\HachiController@profileDetail')->name('hachi.member.profileDetails');
    Route::get('/member/membershipDetails/{mbrId}', 'Member\HachiController@membershipDetails')->name('hachi.member.membershipDetails');
});

// Redeem Voucher
Route::middleware(['role1:redeem-voucher'])->group(function () {
    Route::post('/redeem/voucher', 'Member\HachiController@redeemVoucher')->name('hachi.redeem.voucher');
    Route::post('/redemption/items', 'Member\RedemptionController@getRedeemItemLists')->name('member.redemption.items');
    Route::post('/redemption/transaction', 'Member\RedemptionController@doStarRedemptionSummary')->name('member.redemption.summary');
});

// Convert Staff
Route::middleware(['role1:convert-staff'])->group(function () {
    Route::post('/staff/convert/{mbrId}/{staffId}', 'Staff\StaffController@convert')->name('staff.convert');
    Route::post('/staff/terminate/{mbrId}/{staffId}', 'Staff\StaffController@terminate')->name('staff.terminate');
});

// Coupon Redemption
Route::middleware(['role1:member-voucher'])->group(function () {
    Route::post('/coupon/issue', 'Member\PosController@saveVoucher')->name('pos.saveVoucher');
    Route::get('/coupon/details/{mbrId}/{statusLevel?}', 'Member\PosController@getVoucher')->name('pos.getVoucher');
    Route::get('/coupon/credit/{serial}', 'Member\PosController@get_credit')->name('pos.get_credit');
    Route::post('/coupon/credit', 'Member\PosController@save_credit')->name('pos.save_credit');
    Route::post('/coupon/welcomepack', 'Member\PosController@issue_coupon')->name('pos.issueCoupon');
    Route::post('/coupon/partner', 'Member\PosController@partner_coupon')->name('pos.issueCoupon');
    Route::post('/coupon/redeem', 'Member\HachiController@redeemCoupon')->name('pos.redeemCoupon');
    Route::get('/coupon/list/{mbrId}', 'Member\HachiController@voucherCoupon')->name('hachi.voucher.voucherCoupon');
    Route::get('/coupon/tnc/{mbrId}/{serialNo}', 'Member\HachiController@rewardsTnc')->name('hachi.voucher.rewardsTnc');
    Route::get('/coupon/payment/{mbrId}/{status?}', 'Member\HachiController@paymentCoupon')->name('hachi.voucher.payment');
    Route::post('/big-coupon/issue', 'Member\PosController@saveBigVoucher');
    Route::post('/coupon/utilize', 'Member\PosController@voucherUtilize');
});

// ECredit Egift
Route::middleware(['role1:member-ecredit'])->group(function () {
    Route::post('/ecredits/reserved', 'Member\PosController@credit_reserved')->name('pos.credit.reserved');
    Route::post('/ecredits/unreserved', 'Member\PosController@credit_unreserved')->name('pos.credit.unreserved');
    Route::get('/ecredits/{mbrId}/{status?}', 'Member\HachiController@eCredits')->name('hachi.voucher.eCredits');
    Route::post('/egifts/email', 'Member\HachiController@emailActEGift')->name('hachi.voucher.emailEGifts');
    Route::get('/egifts/egift_claimed/{mbrId}/{status?}', 'Member\EgiftController@egift_claimed')->name('hachi.egift.egift_claimed');
    Route::get('/egifts/egift_purchased/{transId}', 'Member\EgiftController@egift_purchased')->name('hachi.egift.egift_purchased');
    Route::post('/egifts/insert', 'Member\EgiftController@insert')->name('hachi.egift.insert');
    Route::post('/egifts/claim', 'Member\EgiftController@claim')->name('hachi.egift.claim');
    Route::post('/egifts/update_egift', 'Member\EgiftController@update_egift')->name('hachi.egift.update');
    Route::post('/egifts/update_egift_coupon', 'Member\EgiftController@update_egift_coupon')->name('hachi.egift.update_egift_coupon');
});

// Contract
Route::middleware(['role1:contract'])->group(function () {
    //Full contract flow
    Route::post('/contract/subscription', 'Member\ContractController@customer_subscription')->name('pos.contract.customer_subscription');
    Route::get('/contract/process_subscription', 'Member\ContractController@process_subscription')->name('pos.contract.process_subscription');
    Route::get('/contract/process_non_subscription', 'Member\ContractController@process_non_subscription')->name('pos.contract.process_non_subscription');
    Route::post('/contract/subscription', 'Member\ContractController@customer_subscription')->name('pos.contract.customer_subscription_post');

    Route::post('/contract/customer', 'Member\ContractController@customer_register')->name('pos.contract.customer_register');
    Route::get('/contract/customer/{mbr_id}', 'Member\ContractController@get_customer')->name('pos.contract.get_customer');
    Route::post('/contract/create', 'Member\ContractController@create_contract')->name('pos.contract.create_contract');
    Route::post('/contract/pay', 'Member\ContractController@pay_contract')->name('pos.contract.pay_contract');
    Route::get('/contract/{contract_id}/cancel', 'Member\ContractController@cancel_contract')->name('pos.contract.cancel_contract');
    Route::get('/contract/{contract_id}', 'Member\ContractController@get_contract')->name('pos.contract.get_contract');
    Route::get('/contract/{contract_id}/check', 'Member\ContractController@check_contract')->name('pos.contract.check_contract');
    Route::get('/contract/{contract_id}/terminate', 'Member\ContractController@terminate_contract')->name('pos.contract.terminate_contract');


    Route::get('/contract/partner/valueclub/register/{contract_id}', 'Member\ContractController@member_register')->name('pos.contract.member_register');
    Route::get('/contract/partner/valueclub/activate/{mbr_type}/{contract_id}/{trans_id}', 'Member\ContractController@member_activate')->name('pos.contract.member_activate');
    Route::get('/contract/partner/valueclub/renew/{mbr_type}/{contract_id}/{trans_id}', 'Member\ContractController@member_renew')->name('pos.contract.member_renew');
    Route::get('/contract/partner/valueclub/expire/{contract_id}', 'Member\ContractController@member_deactivate')->name('pos.contract.member_deactivate');

});

/***
 * Member Function Migration
 * date format: {Ymd} eg:20200101
 * 1. "/setExpired": Set status to -1 for expired members
 * 2. "/deleteNoPay": Delete members with status -9 (Info inputted but not paid)
 * 3. "/rebateClaimRenewed": Update crm_rebate_claim  for those renewed and expired yesterday
 * 4. "/rebateClaimNotRenewed": Update crm_rebate_claim for those not-renewed and expired yesterday
 * 5. "/approvalRebateClaim": if valid_until > today, process rebate voucher
 * 6. "/clearExpiredPoints": Update Points and savings for those Expired more than 90 days
 * 7. "/processExpirePoints": Process member expiry points
 */
Route::group(['middleware' => ['role1:function-migration'], 'prefix' => 'function', 'namespace' => 'FunctionMigration'], function () {
    Route::post('/setExpired/{date?}', 'FunctionMigrationController@setExpired')->name('function.setExpired');
    Route::post('/deleteNoPay/{date?}', 'FunctionMigrationController@deleteNoPay')->name('function.deleteNoPay');
    Route::post('/rebateClaimRenewed/{date?}', 'FunctionMigrationController@rebateClaimRenewed')->name('function.rebateClaimRenewed');
    Route::post('/rebateClaimNotRenewed/{date?}', 'FunctionMigrationController@rebateClaimNotRenewed')->name('function.rebateClaimNotRenewed');
    Route::post('/approvalRebateClaim/{date?}', 'FunctionMigrationController@approvalRebateClaim')->name('function.approvalRebateClaim');
    Route::post('/clearExpiredPoints/{date?}', 'FunctionMigrationController@clearAllExpiredPoints')->name('function.clearExpiredPoints');
    Route::post('/processExpirePoints/{date?}', 'FunctionMigrationController@processExpirePoints')->name('function.processExpirePoints');
});

// Emarsys
Route::post('/emarsys/{mailstreamId}', 'Mailstream\EmarsysController');
Route::post('/mailstream/{mailstreamId}', 'Mailstream\EmarsysController');
Route::post('/emarsys_user', 'Mailstream\EmarsysController@registerEmail');
Route::get('/emarsys_job', 'Mailstream\EmarsysController@invoke_job');

// Member API
Route::middleware(['auth:api', 'scope:vc-app'])->group(function () {

    Route::post('resetpwd', 'Auth\ResetPasswordController@resetPassword')->name('member.resetpwd');

    Route::group(['prefix' => 'member', 'namespace' => 'Member'], function () {

        // Points API
        Route::group(['prefix' => 'points', 'namespace' => 'Points'], function () {
            Route::get('recentexpiry', 'PointsController@recentExpiringPoints')->name('member.point.expiry');
            Route::get('summary', 'PointsController@summary')->name('member.point.summary');
            Route::get('enough/{points}', 'PointsController@isPointEnough')->name('member.point.enough');
            Route::get('balance', 'PointsController@balancePoints')->name('member.point.balance');
            Route::get('nextline', 'PointsController@getNextLineNum')->name('member.point.nextline');
//            Route::get('redemption/{points}', 'PointsController@pointRedemptionValue')->name('member.point.redemption');
//            Route::get('list/{perPage}', 'PointsController@getQuery')->name('member.point.query');
            Route::get('nextexpiry', 'PointsExpiryController@getNextExpiryDate')->name('member.point.nextexpiry');
        });

        // Voucher API
        Route::group(['prefix' => 'voucher', 'namespace' => 'Voucher'], function () {
            Route::get('myvouchers/{fdate}/{tdate}', 'VoucherController@myVouchers')->name('member.voucher.show');
            Route::get('myegift/{fdate}/{tdate}', 'VoucherController@myeGift')->name('member.voucher.gift');
//            Route::post('addvoucher', 'VoucherController@addVoucher')->name('member.voucher.create');
//            Route::get('available/{mbrid}/{couponid}', 'VoucherController@getAvailableVouchers')->name('member.voucher.available');
//            Route::get('expiry/{mbrid}', 'VoucherController@getExpiredUnutilizedVouchers')->name('member.voucher.expiry');

        });

        // Transaction API
        Route::group(['prefix' => 'transaction', 'namespace' => 'Transaction'], function () {
            Route::get('mytransaction/{fdate}/{tdate}/{tfrom?}', 'TransactionController@myTransaction')->name('member.transaction.show');
            Route::get('myredemption/{fdate}/{tdate}', 'TransactionController@myRedemption')->name('member.transaction.redemption');
            Route::get('mywarranty/{fdate}/{tdate}/{tfrom?}', 'TransactionController@myWarranty')->name('member.transaction.warranty');
        });

    });


});

//For VcApp.
Route::middleware([\App\Http\Middleware\EnsureTokenIsValid::class])->group(function () {

    Route::group(['prefix' => 'vc2', 'namespace' => 'Member'], function () {

        Route::get('/vctest', 'VcController@vcTest')->name('member.info.test');

        //For Challenger API.
        Route::post('/myvouchers/{mbr_id}/{orderby?}/{desc?}/{fdate?}/{tdate?}', 'VcController@myVouchersGet')->name('member.voucher.myvouchersget');
        Route::get('/updateOrInsertMbr/{mbr_id}/{hachipass}/{is_return?}/{is_force?}', 'VcController@updateOrInsertMbr')->name('member.updateofinsertmbrget');
        Route::post("/sendUpdateProfileEmail", 'VcController@sendUpdateProfileEmail')->name('member.sendUpdateProfileEmail');
        Route::get('/resetPassReturn/{edata}', 'VcController@resetPassReturn')->name('member.resetPassReturn');
        Route::get('/mypurchases/{mbr_id}/{fdate?}/{tdate?}/{filter?}/{page_no?}', 'VcController@myPurchasesGet')->name('member.myPurchasesGet');
        Route::get('/myredemption/{mbr_id}/{fdate?}/{tdate?}/{page_no?}', 'VcController@myRedemptionGet')->name('member.myRedemptionGet');
        Route::get('/mymembership/{mbr_id}', 'VcController@GetmyMembershipLists')->name('member.GetmyMembershipLists');
        Route::get('/FPass/{to}/{code}', 'VcController@FPassGet')->name('member.FPassGet');
        Route::get('/FPassEmail/{email}', 'VcController@FPassEmail')->name('member.FPassEmail');
        Route::post('/updateMemberInfoCtl', 'VcController@updateMemberInfoCtl')->name('member.updateMemberInfoCtl');
        Route::post('/updateMemberSubInd', 'VcController@updateMemberSubInd')->name('member.updateMemberSubInd');
        Route::get("/generateCTLMbrId", 'VcController@generateCTLMbrId')->name('member.generateCTLMbrId');
        Route::get("/generateNextTransId", 'VcController@generateNextTransId')->name('member.generateNextTransId');

        //VcApp API
        Route::post('/autologin_hachi', 'VcController@autologin_hachi')->name('member.autologin_hachi');
        Route::get('/savePostalCode', 'VcController@savePostalCode')->name('member.savepostalcode');
        Route::get('/saveMemberDevices/{token?}', 'VcController@saveMemberDevices')->name('member.saveMemberDevices');
        Route::post('/getNotificationInbox2', 'VcController@getNotificationInbox')->name('member.getinbox');
        Route::post('/myMembership', 'VcController@myMembership')->name('member.mymembership');
        Route::post('/verifyUpdateDetail', 'VcController@verifyUpdateDetail')->name('member.verifydetail');
        Route::post('/checkLoginSession', 'VcController@checkLoginSession')->name('member.checkloginsession');
        Route::get('/CheckSess/{mbr_id}', 'VcController@CheckSess')->name('member.checksess');
        Route::post("/getproduct", 'VcController@getproduct')->name('member.getproduct');
        Route::post("/getproductstock", 'VcController@getproductstock')->name('member.getproductstock');
        Route::post('getTnC', 'VcController@getTnC')->name('member.gettnc');
        Route::post('/validateRenewAndUpgrade', 'VcController@validateRenewAndUpgrade')->name('member.validaterenewupgrade');
        Route::post('ChPass', 'VcController@ChPass')->name('member.chpass');
        Route::post('searchPostal2', 'VcController@searchPostal')->name('member.searchpostal');
        Route::post('searchPostal', 'VcController@searchPostal')->name('member.searchpostal');
        Route::post('/info', 'VcController@updateMemberInfo')->name('member.info.update');
        Route::post('/updateMemberInfo', 'VcController@updateMemberInfo')->name('member.updatemember');
        Route::post('/deletePushItem', 'VcController@deletePushItem')->name('member.deletepushitem');
        Route::post('/deleteAllNotifications', 'VcController@deleteAllNotifications')->name('member.deleteallnotifications');
        Route::post('/updateLatestPush', 'VcController@updateLatestPush')->name('member.updatelatestpush');
        Route::post('/updateInboxReadStatusNew', 'VcController@updateInboxReadStatus')->name('member.updateinboxreadstatus');
        Route::post('/updatePushClickStatusNew', 'VcController@updatePushClickStatus')->name('member.updatepushclickstatus');
        Route::post('/updatePushId', 'VcController@updatePushId')->name('member.updatepushid');
        Route::post('/checkWishListValid', 'VcController@checkWishListValid')->name('member.checkwishlistvalid');
        Route::post('/getGamificationLists', 'VcController@getGamificationLists')->name('member.getgamificationlists');

        Route::post('/UpdateEmailDetail', 'VcController@UpdateEmailDetail')->name('member.updateemaildetail');
        Route::post('/getMemberPhoto', 'VcController@getMemberPhoto')->name('member.getmemberphoto');
        Route::post('/uploadMemberPhoto', 'VcController@uploadMemberPhoto')->name('member.uploadMemberPhoto');
        Route::post('/removeMemberPhoto', 'VcController@removeMemberPhoto')->name('member.removememberphoto');
        Route::post("/getAssociateMbr", 'VcController@getAssociateMbr')->name('member.getassociatembr');
        Route::post("/sendAssociateMbr2", "VcController@sendAssociateMbr")->name('member.sendassombr');
        Route::post("/cancelAssoInvite2", "VcController@cancelAssoInvite")->name('member.cancelassoinvite');
        Route::post("/signupAssociateMbr", "VcController@signupAssociateMbr")->name('member.signupassociamtembr');
        Route::post("/getProductReview", 'VcController@getProductReview')->name('member.getproductreview');
        Route::post('/submitProductReview', 'VcController@submitProductReview')->name('member.submitproductreview');
        Route::post('sendReferFriends', 'VcController@sendReferFriends')->name('member.sendreferfriends');
        Route::post('/getVoteStaffInfo', 'VcController@getVoteStaffInfo')->name('member.getvotestaffinfo');
        Route::post('/voteStaff', 'VcController@voteStaff')->name('member.votestaff');
        Route::post('/addVoucher', 'VcController@addVoucher')->name('member.addvoucher');
        Route::post('/GetMemberByMbrId', 'VcController@GetMemberByMbrId')->name('member.getmemberbymbrid');
        Route::post('/myHistory2', 'VcController@myHistory2')->name('member.myhistory2');
        Route::post('/myPurchases', 'VcController@myPurchases')->name('member.mypurchase2');
        Route::post('/myRedemption', 'VcController@myRedemptions')->name('member.myredemptions2');
        Route::post('/myCoupons2', 'VcController@myCoupons')->name('member.voucher.mycoupons');
        Route::post('/myWarranty', 'VcController@myWarranty')->name('member.voucher.mywarranty');
        Route::post('/myDigitalDownload', 'VcController@myDigitalDownload')->name('member.voucher.mydigitaldownload');
        Route::post('/myeGift', 'VcController@myeGift')->name('member.voucher.myegift');
        Route::post('/myVouchers', 'VcController@myVouchers')->name('member.voucher.myvouchers');
        Route::post('/myeCredits', 'VcController@myeCredits')->name('member.voucher.myecredits');
        Route::post('/getcouponlist', 'VcController@getCouponList')->name('member.voucher.getcoupon');
        Route::post('/getInsertVoucher', 'VcController@getInsertVoucher')->name('member.voucher.getcoupon');
        Route::post('/getEasyInkLists', 'VcController@getEasyInkLists')->name('member.geteasyinklists');
        Route::post('/putHpInkSubscribeStatus', 'VcController@putHpInkSubscribeStatus')->name('member.putHpInkSubscribeStatus');
        Route::post('/getHpSalesIdLists', 'VcController@getHpSalesIdLists')->name('member.getHpSalesIdLists');
        Route::post('/getAllEligiblePrinters', 'VcController@getAllEligiblePrinters')->name('member.getAllEligiblePrinters');
        Route::post('/getEligiblePrinterByLists', 'VcController@getEligiblePrinterByLists')->name('member.getEligiblePrinterByLists');
        Route::post('/addEasyInkPrinter', 'VcController@addEasyInkPrinter')->name('member.addEasyInkPrinter');
        Route::post('/loginKeyOverride', 'VcController@loginKeyOverride')->name('member.loginKeyOverride');
        Route::post('/Signup', 'VcController@Signup')->name('member.Signup');
        Route::post('/Upgrade', 'VcController@Upgrade')->name('member.Upgrade');
        Route::post('/Renew', 'VcController@Renew')->name('member.Renew');
        Route::post('/subscribeNewHpInkDevice', 'VcController@subscribeNewHpInkDevice')->name('member.subscribeNewHpInkDevice');
        Route::post('/getRedeemItemLists', 'VcController@getRedeemItemLists')->name('member.getRedeemItemLists');
        Route::post('/doStarRedemptionSummary', 'VcController@doStarRedemptionSummary')->name('member.doStarRedemptionSummary');
        Route::post('/HpPrinterVoucher', 'VcController@HpPrinterVoucher')->name('member.HpPrinterVoucher');
        Route::post('/getResendLinkSTU/{email}', 'VcController@getResendLinkSTU')->name('member.getResendLinkSTU');
        Route::post('/setCtlLoginToken', 'VcController@setCtlLoginToken')->name('member.setCtlLoginToken');
        Route::post('/getCtlLoginToken', 'VcController@getCtlLoginToken')->name('member.getCtlLoginToken');
        Route::post('/starRedemptionPayment', 'VcController@starRedemptionPayment')->name('member.starRedemptionPayment');
        Route::post('/checkOmsTempMember', 'VcController@checkOmsTempMember')->name('member.checkOmsTempMember');
        Route::post("/validateNSMenSignup", "VcController@validateNSMenSignup")->name('member.validateNSMenSignup');
        Route::post("/signupNSFMember", "VcController@signupNSFMember")->name('member.signupNSFMember');
        Route::post("/getStarRedemptionStock", 'VcController@getStarRedemptionStock')->name('member.getStarRedemptionStock');
        Route::post('/getRedeemHCLItemLists', 'VcController@getRedeemHCLItemLists')->name('member.getRedeemHCLItemLists');

        //BATCh JOB.
        Route::post('/batchSendStudentMbrship', 'VcController@batchSendStudentMbrship')->name('member.batchSendStudentMbrship');
        Route::post('/batchSendProductReview', 'VcController@batchSendProductReview')->name('member.batchSendProductReview');
        Route::post('/batchUpdateProductReview/{hachiPass?}', 'VcController@batchUpdateProductReview')->name('member.batchUpdateProductReview');
        Route::post('/sendProductReviewEDM', 'VcController@sendProductReviewEDM')->name('member.sendProductReviewEDM');
        Route::post('/batchHpInkInsert/{date?}', 'VcController@batchHpInkInsert')->name('member.batchHpInkInsert');
        Route::get('/savePostalCode/{token}', 'VcController@savePostalCode')->name('member.savepostalcodejob');
        Route::post('/batchLoiDeviceReminder', 'VcController@batchLoiDeviceReminder')->name('member.batchLoiDeviceReminder');
        Route::post('/batchSaveResetPwdLists', 'VcController@batchSaveResetPwdLists')->name('member.batchSaveResetPwdLists');
        Route::post('/batchHpSendReminderToSubscribe/{date?}', 'VcController@batchHpSendReminderToSubscribe')->name('member.batchHpSendReminderToSubscribe');
        Route::post('/batchHpPrintersOrder/{date?}', 'VcController@batchHpPrintersOrder')->name('member.batchHpPrintersOrder');
        Route::post('/batchOneSignalPushRedis', 'VcController@batchOneSignalPushRedis')->name('member.batchOneSignalPushRedis');
        Route::post('/batchHpSendInactiveReminder', 'VcController@batchHpSendInactiveReminder')->name('member.batchHpSendInactiveReminder');
        Route::post('/batchCeaseAssoMember', 'VcController@batchCeaseAssoMember')->name('member.batchCeaseAssoMember');
        Route::post('/batchPushClickStatus', 'VcController@batchPushClickStatus')->name('member.batchPushClickStatus');
        Route::post('/batchSendResetPwdEmail', 'VcController@batchSendResetPwdEmail')->name('member.batchSendResetPwdEmail');
        Route::post('/batchEmailStudent', 'VcController@batchEmailStudent')->name('member.batchEmailStudent');
        Route::post('/getBingoLists', 'VcController@getBingoLists')->name('member.getBingoLists');
        Route::post('/VcBingoCampaign', 'VcController@VcBingoCampaign')->name('member.VcBingoCampaign');
        Route::post('/VcCny2022Campaign', 'VcController@VcCny2022Campaign')->name('member.VcCny2022Campaign');
        Route::post('/VcMay2022Campaign', 'VcController@VcMay2022Campaign')->name('member.VcMay2022Campaign');
        Route::post('/generateCnyScratchNumber', 'VcController@generateCnyScratchNumber')->name('member.generateCnyScratchNumber');
        Route::post('/getCnyScratchCards', 'VcController@getCnyScratchCards')->name('member.getCnyScratchCards');
        Route::post('/updateCny2022Rebate', 'VcController@updateCny2022Rebate')->name('member.updateCny2022Rebate');
        Route::post('/getMay22CatalogueDetail', 'VcController@getMay22CatalogueDetail')->name('member.getMay22CatalogueDetail');
        Route::get('/givemefiveAddStar/{mbr_id?}', 'VcController@givemefiveAddStar')->name('member.givemefiveAddStar');

        //Batch Job But GET.
        Route::get('/batchSendStudentMbrship', 'VcController@batchSendStudentMbrship')->name('member.batchSendStudentMbrshipGet');
        Route::get('/batchSendProductReview', 'VcController@batchSendProductReview')->name('member.batchSendProductReviewGet');
        Route::get('/batchUpdateProductReview/{hachiPass?}', 'VcController@batchUpdateProductReview')->name('member.batchUpdateProductReviewGet');
        Route::get('/sendProductReviewEDM', 'VcController@sendProductReviewEDM')->name('member.sendProductReviewEDMGet');
        Route::get('/batchHpInkInsert/{date?}', 'VcController@batchHpInkInsert')->name('member.batchHpInkInsertGet');
        Route::get('/batchSaveResetPwdLists', 'VcController@batchSaveResetPwdLists')->name('member.batchSaveResetPwdListsGet');
        Route::get('/batchHpPrintersOrder/{date?}', 'VcController@batchHpPrintersOrder')->name('member.batchHpPrintersOrderGet');
        Route::get('/batchOneSignalPushRedis', 'VcController@batchOneSignalPushRedis')->name('member.batchOneSignalPushRedisGet');
        Route::get('/batchHpSendInactiveReminder', 'VcController@batchHpSendInactiveReminder')->name('member.batchHpSendInactiveReminderGet');
        Route::get('/batchCeaseAssoMember', 'VcController@batchCeaseAssoMember')->name('member.batchCeaseAssoMemberGet');
        Route::get('/batchPushClickStatus', 'VcController@batchPushClickStatus')->name('member.batchPushClickStatusGet');
        Route::get('/batchSendResetPwdEmail/{limit?}', 'VcController@batchSendResetPwdEmail')->name('member.batchSendResetPwdEmailGet');
        Route::get('/batchLoiDeviceReminder', 'VcController@batchLoiDeviceReminder')->name('member.batchLoiDeviceReminderGet');
        Route::get('/batchHpSendReminderToSubscribe/{date?}', 'VcController@batchHpSendReminderToSubscribe')->name('member.batchHpSendReminderToSubscribeGet');
        Route::get('/batchEmailStudent', 'VcController@batchEmailStudent')->name('member.batchEmailStudentGet');
        Route::get('/batchSendStarRedemptionReport', 'VcController@batchSendStarRedemptionReport')->name('member.batchSendStarRedemptionReport');
        Route::get('/batchHpReissueVoucher', 'VcController@batchHpReissueVoucher')->name('member.batchHpReissueVoucher');
        Route::get('/batchDeleteAllMemberCache/{date?}', 'VcController@batchDeleteAllMemberCache')->name('member.batchDeleteAllMemberCache');
        Route::get('/batchImportToOneSignal', 'VcController@batchImportToOneSignal')->name('member.batchImportToOneSignal');
        Route::get('/batchUpdateMembers/{limit?}', 'VcController@batchUpdateMembers')->name('member.batchUpdateMembers');
        Route::get('/batchCeaseExpMember/{limit?}', 'VcController@batchCeaseExpMember')->name('member.batchCeaseExpMember');
        Route::get('/batchUpdateNotificationAurora', 'VcController@batchUpdateNotificationAurora')->name('member.batchUpdateNotificationAurora');
        Route::get('/manualRunLoiReminder', 'VcController@manualRunLoiReminder')->name('member.manualRunLoiReminder');
        Route::get('/clearHashRedis/{mbr}', 'VcController@clearHashRedis')->name('member.clearHashRedis');
        Route::get('/VcBingoCampaign/{pass}/{mbr?}/{update?}', 'VcController@VcBingoCampaign')->name('member.VcBingoCampaignGet');
        Route::get('/batchCallBingoCampaign', 'VcController@batchCallBingoCampaign')->name('member.batchCallBingoCampaign');
        Route::get('/batchBingoDailyReports', 'VcController@batchBingoDailyReports')->name('member.batchBingoDailyReports');
        Route::get('/batchCnyDailyReports', 'VcController@batchCnyDailyReports')->name('member.batchCnyDailyReports');

        Route::get('/batchMay22StarDailyReport', 'VcController@batchMay22StarDailyReport')->name('member.batchMay22StarDailyReport');
        Route::get('/batchMay22ChanceDailyReport', 'VcController@batchMay22ChanceDailyReport')->name('member.batchMay22ChanceDailyReport');


        Route::get('/batchCallCny2022Campaign', 'VcController@batchCallCny2022Campaign')->name('member.batchCallCny2022Campaign');
        Route::get('/batchCny2022DailyReports', 'VcController@batchCny2022DailyReports')->name('member.batchCny2022DailyReports');
        Route::get('/VcCny2022Campaign/{pass}/{mbr?}/{update?}', 'VcController@VcCny2022Campaign')->name('member.VcCny2022CampaignGet');
        Route::get('/VcMay2022Campaign/{pass}/{mbr?}/{update?}', 'VcController@VcMay2022Campaign')->name('member.VcMay2022CampaignGet');

        Route::get('/batchCallMay22GM5Campaign', 'VcController@batchCallMay22GM5Campaign')->name('member.batchCallMay22GM5CampaignGet');

        //Job
        Route::post('/clearAllVcRedis', 'VcController@clearAllVcRedis')->name('member.clearAllVcRedis');
        Route::post('/insertEmarsysPush', 'VcController@insertEmarsysPush')->name('member.insertEmarsysPush');
        Route::post('/manualHpImportHrId', 'VcController@manualHpImportHrId')->name('member.manualHpImportHrId');
        Route::post('/testtoken', 'VcController@testtoken')->name('member.testtoken');
        Route::get('/deleteBatchRedis/{pass}', 'VcController@deleteBatchRedis')->name('member.deleteBatchRedis');
        Route::post('/manualRefundRebate', 'VcController@manualRefundRebate')->name('member.manualRefundRebate');

        Route::post('/odinvoicerefund', 'VcController@odInvoiceRefund')->name('member.odInvoiceRefund');

        //Eventbridge Batch Job Testing.
        Route::get('/testBatchJob', 'VcController@testBatchJob')->name('member.testBatchJob');

    });
});

//For VcApp, But No Need X_Auth.
Route::group(['prefix' => 'vc2', 'namespace' => 'Member'], function () {

    //For VcApp Test.
    Route::get('/test', 'VcController@test')->name('member.test');
    Route::get('/testtrans', 'VcController@testtrans')->name('member.testtrans');
    Route::get('/testredis', 'VcController@testredis')->name('member.testredis');
    Route::get('/testCurl/{email?}/{mbr_id?}/{contact?}', 'VcController@testCurl')->name('member.testCurl');
    Route::get('/testemail', 'VcController@testemail')->name('member.testemail');
    Route::post('/testemarsys', 'VcController@testEmarsys')->name('member.testemarsys');
    Route::get('/deploycheck', 'VcController@deployCheck')->name('member.deployCheck');
    Route::get('/testMay22/{email}', 'VcController@testMay22')->name('member.testMay22');
    Route::get('/testPush/{mbr_id}', 'VcController@testPush')->name('member.testPush');
    Route::get('/valueclub-qr-login/{code?}', 'VcController@vcAppQrLogin')->name('member.vcAppQrLogin');

    //Live API.
    Route::post('/Login', 'VcController@Login')->name('member.login');
    Route::post('/Logout', 'VcController@logout')->name('login.logout');
    Route::post('/getBrandLists', 'VcController@getBrandLists')->name('member.getbrandlists');
    Route::post('/getBrandServiceCentreLists', 'VcController@getBrandServiceCentreLists')->name('member.getbrandservicecentrelists');
    Route::post('Fpass', 'VcController@FPass')->name('member.fpass');
    Route::post('/Version', 'VcController@Version')->name('member.version');
    Route::post("/regEduEamil", "VcController@regEduEmail")->name('member.regeduemail');
    Route::post("/validateStaffCode", "VcController@validateStaffCode")->name('member.validatestaffcode');
    Route::post("/validateStuSignup", "VcController@validateStuSignUp")->name('member.validatestusignup');
    Route::post('/validateRenew', 'VcController@validateRenew')->name('member.validaterenew');
    Route::post('/validateSignup', 'VcController@validateSignup')->name('member.validatesignup');
    Route::get('/initRebatePage/{page}/{mbr_id}', 'VcController@initRebatePage')->name('member.initrebate');
    Route::get("/Viewpay/{tnx_id}", 'VcController@Viewpay')->name('member.Viewpay');
    Route::get("/ViewpaySr/{tnx_id}", 'VcController@ViewpaySr')->name('member.ViewpaySr');
    Route::post("/postpay_2c2p", 'VcController@postpay_2c2p')->name('member.postpay_2c2p');
    Route::get("/callback_signup/{txn_id}/{status}", 'VcController@callback_signup')->name('member.callback_signup');
    Route::get("/callback_renew/{txn_id}/{status}", 'VcController@callback_renew')->name('member.callback_renew');
    Route::get("/callback_upgrade/{txn_id}/{status}", 'VcController@callback_upgrade')->name('member.callback_upgrade');
    Route::get("/callback_sr/{txn_id}/{status}", 'VcController@callback_sr')->name('member.callback_sr');
    Route::get("/getMemberItems/{web?}", 'VcController@getMemberItems')->name('member.getMemberItems');

    //API For Challenger.
    Route::post("/member_signup", 'VcController@member_signup')->name('member.member_signup');
    Route::post("/member_renew", 'VcController@member_renew')->name('member.member_renew');
    Route::post("/member_upgrade", 'VcController@member_upgrade')->name('member.member_upgrade');
    Route::post("/member_renew_rebate", 'VcController@member_renew_rebate')->name('member.member_renew_rebate');
    Route::post("/validate_member", 'VcController@validateEmailContact')->name('member.validateEmailContact');
    Route::post("/membership_payment", 'VcController@membershipPaymentCurl')->name('member.membershipPaymentCurl');
    Route::post("/callback_vc_payment", 'VcController@callbackPaymentResult')->name('member.callbackPaymentResult');
    Route::get("/callback_vc_payment", 'VcController@callbackPaymentResult')->name('member.callbackPaymentResultGet');


    Route::post("/renew_viarebates", 'VcController@renew_viaRebates')->name('member.renew_viaRebates');
    Route::get('/resetpw/{edata}', 'VcController@resetpw')->name('member.resetpw');
    Route::get('/openvcapp', 'VcController@openvcapp')->name('member.openvcapp');
    Route::get('/activateMember/{edata}', 'VcController@activateMember')->name('member.activatemember');
    Route::get('/signup_associate/{edata}', 'VcController@signup_associate')->name('member.signup_associate');
    Route::get('/SignupStudentPage/{edata}', 'VcController@SignupStudentPage')->name('member.SignupStudentPage');
    Route::get('/ProductReviewPage/{edata}', 'VcController@ProductReviewPage')->name('member.ProductReviewPage');
    Route::get('/activateNSMember/{edata}', 'VcController@activateNSMember')->name('member.activateNSMember');

    Route::get('/getAllProductReview/{lists}', 'VcController@getAllProductReview')->name('member.getAllProductReview');
    Route::get('/getItemProductReview/{item_id}', 'VcController@getItemProductReview')->name('member.getItemProductReview');

    //Some Batch JOb.
    Route::get('/batchRedisServiceCenter/{hachipass}', 'VcController@batchRedisServiceCenter')->name('member.batchRedisServiceCenter');

    Route::get('/bitlyl/{data}', 'VcController@bitlyl')->name('member.bitlyl');
    Route::post('/my_membership', 'VcController@my_membership')->name('member.my_membership');
    Route::post('/update_status/{mbr_id}/{item_id}/{trans_id}', 'ContractController@update_status')->name('member.update_status');
    Route::get('/testUpdate', 'VcController@testUpdate')->name('member.testUpdate');

    Route::post('/donate_rebate', 'VcController@donate_rebate')->name('member.donate_rebate');
    Route::post('/transfer_point', 'VcController@transfer_point')->name('member.transfer_point');
    Route::post('/get_member_byid', 'VcController@get_member_byid')->name('member.get_member_byid');
    Route::get('/invoice', 'VcController@getInvoicePdf')->name('member.getInvoicePdf');
    Route::get('/invoice_refund', 'VcController@getInvoicePdf')->name('member.getInvoicePdf');

    Route::get('/secureurl/{token}/{url}', 'VcController@secureurl')->name('member.secureurl');
    Route::get('/manualIssueVoucher/{mbr_id}', 'VcController@manualIssueVoucher')->name('member.manualIssueVoucher');

    Route::get('/removeLoginRedis/{mbr_id}/{hachipass}', 'VcController@removeLoginRedis')->name('member.removeLoginRedis');
    Route::get('/getResendLinkSTU/{email}', 'VcController@getResendLinkSTU')->name('member.getResendLinkSTU');

    Route::get('/setCtlLoginTokenData/{email}', 'VcController@setCtlLoginTokenGet')->name('member.setCtlLoginToken');
    Route::get('/getCtlLoginTokenData/{access}', 'VcController@getCtlLoginTokenGet')->name('member.getCtlLoginToken');

    Route::get('/manualSendPushToEmail/{token?}', 'VcController@manualSendPushToEmail')->name('member.manualSendPushToEmail');
    Route::get('/manualInsertStarRedemptionList/{insert?}', 'VcController@manualInsertStarRedemptionList')->name('member.manualInsertStarRedemptionList');


});


Route::group(['prefix' => 'order', 'namespace' => 'Member'], function () {
    Route::post('/get_schedule', 'VcController@getScheduleItem')->name('member.order.get_schedule');
    Route::post('/update_schedule', 'VcController@updateEmailSchedule')->name('member.order.update_schedule');
});



// Admin API
//Route::middleware('auth:admin_api')->group(function() {
//    Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Member'], function() {
//
//        // Point API
//        Route::get('point', 'PointController@index')->name('admin.point.index');
//        Route::get('point/{id}/{lineNum?}', 'PointController@show')->name('admin.point.show');
////        Route::post('point', 'PointController@store')->name('admin.point.store');
////        Route::post('point/{id}/{lineNum}', 'PointController@update')->name('admin.point.update');
//        Route::delete('point/{id}/{lineNum}', 'PointController@destroy')->name('admin.point.destroy');
//
//        // Transaction API
//        Route::get('transaction', 'TransactionController@index')->name('admin.transaction.index');
//        Route::get('transaction/{id}/{transId}/{lineNum?}', 'TransactionController@show')->name('admin.transaction.show');
////        Route::post('transaction', 'TransactionController@store')->name('admin.transaction.store');
////        Route::post('transaction/{id}/{lineNum}', 'TransactionController@update')->name('admin.transaction.update');
//        Route::delete('transaction/{id}/{transId}/{lineNum}', 'TransactionController@destroy')->name('admin.transaction.destroy');
//
//        // Voucher API
//        Route::get('voucher', 'VoucherController@index')->name('admin.voucher.index');
//        Route::get('voucher/{id}/{couponId?}', 'VoucherController@show')->name('admin.voucher.show');
////        Route::post('voucher', 'VoucherController@store')->name('admin.voucher.store');
////        Route::post('voucher/{id}/{lineNum}', 'VoucherController@update')->name('admin.voucher.update');
//        Route::delete('voucher/{id}/{couponId}', 'VoucherController@destroy')->name('admin.voucher.destroy');
//
//    });
//});
