<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('test');
//});
Route::get('/test', function () {
    $a = [1];
    dd(!empty($a),'JAY');
});
Route::get('member-points/send_file_email', ['uses' => 'Voyager\MemberPointsSummaryController@sendFileEmail', 'as' => 'voyager.member-points-summary.send_file_email']);
Route::group(['prefix' => '/'], function () {
    Voyager::routes();
    Route::post('claim', ['uses' => 'Voyager\RebateClaimController@claim',  'as' => 'voyager.rebate-claim.claim']);
    Route::post('claim_approval', ['uses' => 'Voyager\RebateClaimController@claim_approval',  'as' => 'voyager.rebate-claim.claim_approval']);
    Route::post('claim_reject', ['uses' => 'Voyager\RebateClaimController@claim_reject',  'as' => 'voyager.rebate-claim.claim_reject']);
    Route::post('updateType', ['uses' => 'Voyager\MemberListController@updateMemberType', 'as' => 'voyager.member-list.update_type']);
    Route::post('awardPoints', ['uses' => 'Voyager\MemberListController@points_award', 'as' => 'voyager.member-list.points_award']);
    Route::post('deductPoints', ['uses' => 'Voyager\MemberListController@points_deduct', 'as' => 'voyager.member-list.points_deduct']);
    Route::get('member-list/{id}/show-member-voucher', ['uses' => 'Voyager\MemberListController@showMemberVoucher', 'as' => 'voyager.member-list.show_member_voucher']);
    Route::get('member-list/{id}/show-member-voucher/edit/{couponId}', ['uses' => 'Voyager\MemberListController@editMemberVoucher', 'as' => 'voyager.member-list.edit_member_voucher']);
    Route::put('member-list/{id}/show-member-voucher/edit/{couponId}', ['uses' => 'Voyager\MemberListController@updateMemberVoucher', 'as' => 'voyager.member-list.update_member_voucher']);
//    Route::get('member-list/{id}/show-member-transaction/{trans_id}', ['uses' => 'Voyager\MemberListController@showTransaction', 'as' => 'voyager.member-list.show_member_transaction']);
    Route::get('member-list/resetpwd/{email}', ['uses' => 'Voyager\MemberListController@resetPassword', 'as' => 'voyager.member-list.reset_password']);
    Route::post('member-list/reminder', ['uses' => 'Voyager\MemberListController@reminder', 'as' => 'voyager.member-list.reminder']);

    Route::get('member-points/download_file', ['uses' => 'Voyager\MemberPointsSummaryController@downloadFile', 'as' => 'voyager.member-points-summary.download_file']);

    Route::post('coupon-type/add_coupon_id', ['uses' => 'Voyager\CouponTypeController@create_coupon_id', 'as' => 'voyager.coupon-type.create_coupon_id']);

    Route::get('search-product/getallinventory/{id}', ['uses' => 'Voyager\SearchProductController@getallinventory']);

    Route::get('mvip-list', ['uses' => 'Voyager\MviplistController@index']);

});
Route::get('member-list/{id}/show-member-transaction/{trans_id}', ['uses' => 'Voyager\MemberListController@showTransaction', 'as' => 'voyager.member-list.show_member_transaction']);