<?php namespace App\Facades\Member\Voucher;


use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB;

/**
 * @see App\Services\Member\Voucher\VoucherService
 */
class Voucher extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'voucherService';
    }

    public static function myVouchers($mbr_id, $coy_id, $fdate, $tdate){

        $query= DB::connection('pgsql')
            ->table('crm_voucher_list as ct')
            ->select('ct.coupon_id', DB::raw('ct.receipt_id1 as trans_id'), 'iml.item_id as item_id','ct.status_level as status_level',
                DB::raw('cast(ct.issue_date as date) as issue_date'),'ct.coupon_serialno','d.long_desc as long_desc',
            DB::raw("CASE WHEN ISNULL(iml.item_desc,'')!=''  THEN iml.item_desc ELSE cl.coupon_name END as item_desc"),'issue_type',
            DB::raw("case when ISNULL(ct.receipt_id2,'') = '' THEN case WHEN cast(ct.expiry_date as datetime) > cast(getdate() as datetime) THEN 'Valid' ELSE 'Expired' END ELSE 'Redeemed' END as status"),
                DB::raw("case when ISNULL(ct.receipt_id2,'') = '' and cast(ct.expiry_date as date) > cast(getdate() as date) THEN 1 ELSE 0 END as valid_status"),
            'ct.mbr_id', "url.url as url",
            DB::raw("CASE WHEN ISNULL(cl.coupon_name,'')!=''  THEN cl.coupon_name  ELSE iml.item_desc END as coupon_name"),
            DB::raw("CASE WHEN ISNULL(iml.item_desc,'')!=''  THEN iml.item_desc ELSE cl.coupon_name END as coupon_desc"),
            DB::raw("CASE WHEN isnull(d.item_terms,'')!='' THEN d.item_terms ELSE cl.coupon_desc END as tnc"),
            'ct.expiry_date as expiry_date', DB::raw("cast(ct.expiry_date as date)  as valid_until"),
            'ct.created_on', DB::raw("CASE WHEN (ct.coupon_id LIKE '%!VCH-QR%' and cast(ct.expiry_date as date) < cast(getdate() as date) and ISNULL(ct.receipt_id2,'') = '') THEN 'noshow' ELSE 'show' END as qr_show"))
            ->leftJoin('o2o_coupon_list AS cl', 'cl.coupon_id', '=', 'ct.coupon_id' )
            ->leftJoin('ims_item_list AS iml', 'iml.item_id', '=', DB::raw('RTRIM(ct.coupon_id)') , 'iml.coy_id', '=', $coy_id)
            ->leftJoin('oms_voucher_list AS d' ,DB::raw('RTRIM(ct.coupon_id)') , '=' , 'd.item_id')
            ->leftJoin('o2o_voucher_url AS url', 'url.voucher_id', '=', 'ct.coupon_id')
            ->where('ct.mbr_id', $mbr_id)
            ->whereRaw("cast(ct.expiry_date as datetime) > cast(getdate() as datetime)")
            ->whereRaw("ISNULL(ct.issue_type, '') NOT IN ('GV', 'CREDIT' )")
            ->whereIn('ct.status_level' , ['0','1','2']);

        if($fdate && $tdate){
            $query->whereBetween(DB::raw('cast(ct.expiry_date as date)') , [$fdate, $tdate]);
        }

        $vouchers= $query->orderBy('ct.expiry_date')->take(10)->get();

        return $vouchers;


//        $sql=<<<str
//SELECT * From (
//SELECT ROW_NUMBER() OVER(ORDER By valid_status desc,expiry_date desc) rownum, * FROM(
//SELECT * FROM (
//SELECT ct.receipt_id1 as trans_id, iml.item_id as item_id,ct.status_level as status_level,cast(ct.issue_date as date) as issue_date,ct.coupon_serialno,
//d.long_desc as long_desc,
//CASE WHEN ISNULL(iml.item_desc,'')!=''  THEN iml.item_desc ELSE cl.coupon_name END as item_desc, 1 as qty_reserved,issue_type,
//case when ISNULL(ct.receipt_id2,'') = '' THEN
//		 case WHEN cast(ct.expiry_date as datetime) > cast(getdate() as datetime) THEN 'Valid' ELSE 'Expired' END
//    ELSE 'Redeemed'
//    END	 as status,
//    case when ISNULL(ct.receipt_id2,'') = '' and cast(ct.expiry_date as date) > cast(getdate() as date) THEN 1 ELSE 0 END as valid_status,
//    ct.mbr_id,
//    ISNULL(url.url,'') as url,
// CASE WHEN ISNULL(cl.coupon_name,'')!=''  THEN cl.coupon_name  ELSE iml.item_desc END as coupon_name,
//CASE WHEN ISNULL(iml.item_desc,'')!=''  THEN iml.item_desc ELSE cl.coupon_name END as coupon_desc,
//CASE WHEN isnull(d.item_terms,'')!='' THEN d.item_terms ELSE cl.coupon_desc END as tnc,ct.expiry_date as expiry_date,
//       convert(varchar, ct.expiry_date, 23)  as valid_until,
//ct.created_on,
//CASE WHEN (ct.coupon_id LIKE '%!VCH-QR%' and cast(ct.expiry_date as date) < cast(getdate() as date) and ISNULL(ct.receipt_id2,'') = '') THEN 'noshow' ELSE 'show' END as qr_show
//                FROM crm_voucher_list ct
//                LEFT JOIN o2o_coupon_list cl ON cl.coupon_id = ct.coupon_id
//                LEFT JOIN ims_item_list iml ON iml.item_id = ct.coupon_id and iml.coy_id = '$coy_id'
//				LEFT JOIN oms_voucher_list d on ct.coupon_id = d.item_id
//				LEFT JOIN o2o_voucher_url url on url.voucher_id= ct.coupon_id
//                WHERE ct.mbr_id = :mbr_id
//                AND cast(ct.expiry_date as datetime) > cast(getdate() as datetime)
//                AND ISNULL(ct.issue_type, '') NOT IN (
//                        'GV', 'CREDIT'
//                        ) and (ct.status_level = '0' OR ct.status_level = '1' OR ct.status_level = '2')
//    AND cast(ct.expiry_date as date) between '$fdate' and '$tdate'
//) a WHERE a.qr_show!='noshow' ) a ) a  WHERE a.rownum between :page_no and :page_to
//str;
//
//        $vouchers = DB::connection('pgsql')->select(DB::raw($sql), array(
//            "page_no" =>$page_from,
//            "page_to" => $page_to,
//            "mbr_id" => $mbr_id
//        ));
//
//        $vouchercount = 0;
//        for ($i = 0; $i < count($vouchers); $i++) {
//            $vouchers[$i]->long_desc = urldecode($vouchers[$i]->long_desc);
//            $vouchers[$i]->tnc = str_replace("&nbsp;", '', $vouchers[$i]->tnc);
//            $vouchers[$i]->url = $vouchers[$i]->url ? json_decode($vouchers[$i]->url, 1) : "";
//            if ($vouchers[$i]->status == "Valid") {
//                $vouchercount = $vouchercount + 1;
//            }
//        }
//
//        return $vouchers;

    }

    public static function myGifts($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to){

        $sql=<<<str
            	SELECT * FROM 
  (
        SELECT ROW_NUMBER() OVER(ORDER BY valid_until desc) rownum,*  FROM (
            SELECT  CASE WHEN ISNULL(t.receipt_id2,'')!='' THEN 'Redeemed'
                        WHEN ISNULL(t.receipt_id2,'')='' THEN
                        CASE WHEN cast(t.expiry_date as date) >= cast(getdate() as date) THEN 'Valid' ELSE 'Expired' END 
                END	 as status,
                oms.item_terms as tnc
                ,CONCAT('$',t.voucher_amount) as amount, 
                CAST(t.expiry_date as date) as expiry_date, 
                t.coupon_serialno, 
                item.item_id, item.item_desc as item_name,
				CONCAT('$',t.voucher_amount,' ',item.item_desc) as item_desc, 
				 otv.message as long_desc,
                convert(varchar, t.expiry_date, 23)  as valid_until
            FROM crm_voucher_list t
            left join o2o_coupon_list l on l.coupon_id=t.coupon_id  
            left join ims_item_list item on item.item_id= t.coupon_id
            left join oms_voucher_list as oms on oms.item_id=t.coupon_id 
            left join o2o_transaction_voucher otv on otv.code=t.coupon_serialno
            WHERE (isnull(t.mbr_id,'')= '$mbr_id'
                and isnull(t.mbr_id,'')<>'' ) 
                and (t.coupon_id LIKE '!EGIFT%')
                and isnull(t.voucher_amount,0)>0
        ) a WHERE cast(a.valid_until as date) between '$fdate' and '$tdate'
	) a
	WHERE a.rownum between :page_no and :page_to;
str;

        $gifts= DB::connection('pgsql')->select(DB::raw($sql), array(
            "page_no" =>$page_from,
            "page_to" => $page_to
        ));

        for ($i = 0; $i < count($gifts); $i++) {
            $gifts[$i]->long_desc = urldecode($gifts[$i]->long_desc);
            $gifts[$i]->tnc = str_replace("\r\n", '', strip_tags($gifts[$i]->tnc));
        }


        return $gifts;
    }

    public static function myCredits($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to)
    {

        $sql = <<<str
            	SELECT * FROM 
  (
        SELECT ROW_NUMBER() OVER(ORDER BY valid_until desc)rownum,*  FROM (
            SELECT  CASE WHEN ISNULL(t.receipt_id2,'')!='' THEN 'Redeemed'
                        WHEN ISNULL(t.receipt_id2,'')='' THEN
                        CASE WHEN cast(t.expiry_date as date) >= cast(getdate() as date) THEN 'Valid' ELSE 'Expired' END 
                END	 as status,
                CASE WHEN ISNULL(t.receipt_id2,'')='' THEN '-'
				ELSE t.receipt_id2 END
				 as utilised_invoice
                ,CONCAT('$',t.voucher_amount) as amount, 
                CAST(t.expiry_date as date) as expiry_date, 
                t.coupon_serialno, 
                item.item_id, ' eCredits' as item_name,
				CONCAT('$',t.voucher_amount,' eCredits') as item_desc, 
                   CASE WHEN ISNULL(product.long_desc,'')!='' THEN product.long_desc ELSE item.long_desc END as long_desc,
                  convert(varchar, t.expiry_date, 23) as valid_until
            FROM crm_voucher_list t
            left join o2o_coupon_list l on l.coupon_id=t.coupon_id  
            left join ims_item_list item on item.item_id= t.coupon_id
            left join ims_o2o_product product on product.item_id= t.coupon_id
            WHERE (isnull(t.mbr_id,'')= :mbr_id
                and isnull(t.mbr_id,'')<>'' ) 
                and (t.coupon_id='!VCH-CREDIT' and t.issue_type ='CREDIT')
                and isnull(t.voucher_amount,0)>0
        ) a WHERE cast(a.valid_until as date) between '$fdate' and '$tdate'
	) a
	WHERE a.rownum between :page_no and :page_to;
str;

        $credits = DB::connection('pgsql')->select(DB::raw($sql), array(
            "page_no" => $page_from,
            "page_to" => $page_to,
            "mbr_id" =>$mbr_id
        ));

        for ($i = 0; $i < count($credits); $i++) {
            $credits[$i]->long_desc = urldecode($credits[$i]->long_desc);
        }

        return $credits;
    }

}