<?php namespace App\Facades\Member\Points;

use Illuminate\Support\Facades\Facade;
/**
 * @see App\Services\Member\Points\PointsService
 */
class Points extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'pointsService';
    }
}