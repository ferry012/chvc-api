<?php namespace App\Facades\Member\Transaction;

use Illuminate\Support\Facades\Facade;
/**
 * @see App\Services\Member\Transaction\TransactionService
 */
class Transaction extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'transactionService';
    }
}