<?php

namespace App\Widgets;

use App\Models\Member\RebateClaim;
use CHG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use CHG\Voyager\Facades\Voyager;

class RebateClaimDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = RebateClaim::claimEntitled()->count();
        $string = $count == 1 ? 'Rebate Record' : 'Rebate Records';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-people',
            'title'  => "{$count} {$string}",
            'text'   => "{$count} {$string}. Click on button to view all {$string}",
            'button' => [
                'text' => "View all {$string}",
                'link' => route('voyager.member-list.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/05.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Voyager::can('rebate');
    }
}
