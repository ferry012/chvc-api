<?php

namespace App\Exports;


use App\Models\Member\Points\PointsSummary;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PointsExport implements FromArray, WithHeadings, ShouldAutoSize, WithMapping, WithEvents, WithTitle, WithStrictNullComparison
{
    public $from;
    public $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return Builder
     */
    public function array(): array
    {
        $from = $this->from;
        $to = $this->to;
        $data = PointsSummary::where(['date_start'=>$from, 'closing_date'=>$to])->select('line_num', 'from_range', 'to_range', 'total_points_balance',
            'total_members','total_points_issued', 'total_points_redeemed', 'total_points_expired', 'total_members_expired')->get()->toArray();
        $total_points_balance = 0;
        $total_members = 0;
        $total_points_issued = 0;
        $total_points_redeemed = 0;
        $total_points_expired = 0;
        $total_members_expired = 0;
        foreach ($data as $k=>$v) {
            $total_points_balance += $v['total_points_balance'];
            $total_members += $v['total_members'];
            $total_points_issued += $v['total_points_issued'];
            $total_points_redeemed += $v['total_points_redeemed'];
            $total_points_expired += $v['total_points_expired'];
            $total_members_expired += $v['total_members_expired'];
        }
        array_push($data, array('line_num'=>'','from_range'=>'', 'to_range'=>'Total:',
                                        'total_points_balance' => $total_points_balance, 'total_members' => $total_members,
                                        'total_points_issued' => $total_points_issued, 'total_points_redeemed' => $total_points_redeemed,
                                        'total_points_expired' => $total_members_expired, 'total_members_expired'=>$total_members_expired));
        return $data;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->mergeCells('A1:I1');
                $event->sheet->getDelegate()->mergeCells('A2:I2');
                $event->sheet->getStyle('A1:I1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'size' => 14
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A2:I2')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'size' => 12
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:I3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getStyle('C21')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getStyle('A5:I21')->applyFromArray(
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                    ]
                );
                $event->sheet->getStyle('A1:I21')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ]);

            },
        ];
    }

//    public function styles(Worksheet $sheet)
//    {
//        return [
//            // Style the first row as bold text.
//            'A1'    => ['font' => ['bold' => true]],
//            '2'    => ['font' => ['bold' => true]],
//            '3'    => ['font' => ['bold' => true]],
//            'C16'    => ['font' => ['bold' => true]],
//
//            // Styling a specific cell by coordinate.
////            'B2' => ['font' => ['italic' => true]],
//
//            // Styling an entire column.
////            'C'  => ['font' => ['size' => 16]],
//        ];
//    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            ['CHALLENGER TECHNOLOGIES LIMITED'],
            ['Member Points Summary'],
            ['Date From', Carbon::parse($this->from)->format('d-m-Y'), 'to', Carbon::parse($this->to)->format('d-m-Y')],
            [
            'No',
            'From Range',
            'To Range',
            'Total Points Balance(V$)',
            'Total Members',
            'Total Points Issued(V$)',
            'Total Points Redeemed(V$)',
            'Total Points Expired(V$)',
            'Total Members Expired'
            ]
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row['line_num'],
            $row['from_range'],
            $row['to_range'],
            number_format(($row['total_points_balance']/100), 2, '.', ','),
            number_format(($row['total_members']), 0, '.', ','),
            number_format(($row['total_points_issued']/100), 2, '.', ','),
            number_format(($row['total_points_redeemed']/100), 2, '.', ','),
            number_format(($row['total_points_expired']/100), 2, '.', ','),
            number_format(($row['total_members_expired']), 0, '.', ',')
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return '111';
    }
}