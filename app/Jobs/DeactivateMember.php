<?php

namespace App\Jobs;

use App\Http\Controllers\Member\TransactionController;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeactivateMember extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $mbr_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mbr_id)
    {
        $this->mbr_id = $mbr_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TransactionController $transactionController)
    {
        $transactionController->member_deactivate($this->mbr_id);
    }
}
