<?php

namespace App\Jobs;

use App\Services\EmarSys\EmarsysServiceV2;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RegisterEmarsysUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EmarsysServiceV2 $emarsysService)
    {
        $emarsysService->registerUser($this->email);
    }
}
