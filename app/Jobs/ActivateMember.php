<?php

namespace App\Jobs;

use App\Http\Controllers\Member\TransactionController;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ActivateMember extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $mbr_type, $mbr_id, $trans_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mbr_type, $mbr_id, $trans_id)
    {
        $this->mbr_type = $mbr_type;
        $this->mbr_id = $mbr_id;
        $this->trans_id = $trans_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TransactionController $transactionController)
    {
        $transactionController->member_activate($this->mbr_type, $this->mbr_id, $this->trans_id);
    }
}
