<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'mbr_id' => $this->mbr_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->fullName(),
            'birth_date' => $this->birth_date,
            'email_addr' => $this->email_addr,
            'contact_num' => $this->contact_num,
            'status_level' => $this->status_level,
            'join_date' => $this->join_date,
            'exp_date' => $this->exp_date,
            'last_renewal' => $this->last_renewal,
            'points_accumulated' => $this->points_accumulated,
            'points_reserved' => $this->points_reserved,
            'points_redeemed' => $this->points_redeemed,
            'points_expired' => $this->points_expired,
            'mbr_savings' => $this->mbr_savings,
            'rebate_voucher' => $this->rebate_voucher,
            'sub_ind1' => $this->sub_ind1,
            'sub_ind2' => $this->sub_ind2,
            'sub_date' => $this->sub_date,
            'mbr_token' => $this->mbr_token,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'modified_by' => $this->modified_by,
            'modified_on' => $this->modified_on,
            'updated_on' => $this->updated_on
        ];
    }

    protected function fullName()
    {
        if ($this->last_name) {
            return $this->first_name.' '.$this->last_name;
        }

        return $this->first_name;
    }

    public function with($request)
    {
        return [
           'status' => 'success',
           'status_code' => 200,
        ];
    }
}
