<?php

namespace App\Providers;

use App\Services\Member\Points\PointsService;
use App\Services\Member\Transaction\TransactionService;
use App\Services\Member\Voucher\VoucherService;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Faker\Generator as FakerGenerator;
use Faker\Factory as FakerFactory;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if (env('FORCE_SSL')==1) {
//            \Illuminate\Support\Facades\URL::forceScheme('https');
            $this->app['request']->server->set('HTTPS','on');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        DB::listen(function($query) {
//            $tmp = str_replace('?', '"'.'%s'.'"', $query->sql);
//            $tmp = vsprintf($tmp, $query->bindings);
//            $tmp = str_replace("\\","",$tmp);
//            Log::info($tmp."\n\n\t");
//        });

        $this->app->bind('voucherService', function ($app) {
            return new VoucherService();
        });

        $this->app->bind('transactionService', function($app) {
            return new TransactionService();
        });

        $this->app->bind('pointsService', function($app) {
            return new PointsService();
        });

        Passport::tokensCan([
            'vc-app' => 'ValueClub APP',
            'qr-login' => 'QR Login',
            'member-points' => 'Member Points',
            'member-transaction' => 'Member Transaction',
            'member-register' => 'Member Register',
            'member-details' => 'Member Details',
            'member-voucher' => 'Member Voucher',
            'member-ecredit' => 'Member ECredit',
            'redeem-voucher' => 'Redeem Voucher',
            'convert-staff' => 'Convert Staff',
            'function-migration' => 'Function Migration'
        ]);
    }
}
