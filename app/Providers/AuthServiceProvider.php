<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->app['auth']->provider('custom', function ($app, array $config) {
//            dd($app['hash']);
            $model = $app['config']['auth.providers.members.model'];
            return new CustomUserProvider($app['hash'], $model);
        });

        Passport::routes();
        Passport::enableImplicitGrant();
        Passport::tokensExpireIn(Carbon::now()->addDay(1));

        Passport::refreshTokensExpireIn(Carbon::now()->addMonths(24));

        //
    }
}
