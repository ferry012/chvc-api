<?php

namespace App\Services\EmarSys;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\RequestInterface;

class EmarsysServiceV2
{
    private $url;
    private $clientSecretLibrary;
    private $clientId;
    private $clientSecret;

    public function __construct()
    {
        $this->url = 'https://api.emarsys.net/api/v2/';

        $this->clientSecretLibrary = [
            'hachi001'          => 'HZyv6cG90hDXJiNkH76X',
            'hachi_trans002'    => 'kQcR6qQpmbMteeAyiXcO'
        ];
    }

    public function registerUser($email) {

        $this->clientSecret = $this->clientSecretLibrary['hachi_trans002'];

        $requestMethod = 'POST';
        $requestUrl = $this->url . 'contact';
        $requestBody = [
            'key_id' => '3',
            '3' => $email,
        ];

        $response = $this->sendApi($requestMethod, $requestUrl, $requestBody);

        return [
            'endpoint'  => $requestUrl,
            'client'    => $this->clientId,
            'request'   => $requestBody,
            'response'  => $response
        ];

    }

    public function sendEmail($client, $requestMethod, $serviceUri, $template, $dataModelling = '', $dataset = null) {

        $this->clientId = $client;
        $this->clientSecret = $this->clientSecretLibrary[$client];

        $requestUrl = $this->url . $serviceUri;
        if (trim($dataModelling) == '')
            $requestBody = $this->buildEmailData($template, $dataset);
        else
            $requestBody = $template;

        $response = $this->sendApi($requestMethod, $requestUrl, $requestBody);

        return [
            'endpoint'  => $requestUrl,
            'client'    => $this->clientId,
            'request'   => $requestBody,
            'response'  => $response
        ];

    }


    public function registerUserEmail($email) {

        $this->clientId = 'hachi_trans002';
        $this->clientSecret = 'kQcR6qQpmbMteeAyiXcO';

        $requestUrl = $this->url . 'contact';

        $contact_data = [
            'key_id' => '3',
            '3' => $email,
        ];
        $requestBody = json_encode($contact_data);
        $response = $this->sendApi('POST', $requestUrl, $requestBody);

        return [
            'endpoint'  => $requestUrl,
            'client'    => $this->clientId,
            'request'   => $requestBody,
            'response'  => $response
        ];

    }

    public function sendApi($requestMethod, $requestUrl, $requestBody = '')
    {
        if (!in_array($requestMethod, array('GET', 'POST', 'PUT', 'DELETE'))) {
            throw new Exception('Send first parameter must be "GET", "POST", "PUT" or "DELETE"');
        }

        if (is_array($requestBody)) {
            $requestBody = json_encode($requestBody);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($requestMethod) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
        }

        $nonce = md5(rand());
        $timestamp = date('c');
        $passwordDigest = base64_encode(sha1($nonce . $timestamp . $this->clientSecret));

        $jheader = array("Content-Type: application/json",
            "X-WSSE: UsernameToken Username=\"$this->clientId\""
            . ", PasswordDigest=\"$passwordDigest\""
            . ", Nonce=\"$nonce\""
            . ", Created=\"$timestamp\""
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $jheader);
        curl_setopt($ch, CURLOPT_USERAGENT, "PhpRestClient");
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $requestUrl);

        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response,true);
        return $response;
    }

    public function buildEmailData($template, $dataset = null){

        function ___map_template($tpl, $dts) {
            foreach ($tpl as $k => $v) {
                if (is_array($tpl[$k])) {
                    $tpl[$k] = ___map_template($tpl[$k], $dts);
                }
                else {
                    if (isset($dts[$k]))
                        $tpl[$k] = $dts[$k];
                }
            }
            return $tpl;
        }

        if ($dataset === null) {
            $dataset = request()->post('data');
        }

        // Foreach $template_arr replace with dataset if available
        $template_arr = json_decode($template, true);
        $template_arr = ___map_template($template_arr, $dataset);

        if (isset($template_arr['email']))
            $template_arr['email'] = (isset($dataset['email'])) ? $dataset['email'] : request()->post('email');
        if (isset($template_arr['external_id']))
            $template_arr['external_id'] = (isset($dataset['email'])) ? $dataset['email'] : request()->post('email');
        if (isset($template_arr['key_id']))
            $template_arr['key_id'] = '3';

        return $template_arr;

    }
}
