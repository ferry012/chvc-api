<?php namespace App\Services\Member\Transaction;

use App\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\IMS\Cherps2Service;

class TransactionService
{
    private $db20;
    public function __construct()
    {
        $this->db20 = DB::connection('pgsql');
    }

    public function myTransaction($mbrId, $fdate, $tdate, $transType)
    {
        $transactions = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereIn('a.trans_type', $transType)
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
//            ->get()
            ->paginate(5);
        return $transactions;
    }

    public function myTransactionHistory($mbrId, $fdate, $tdate, $transType)
    {
        $trans1 = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereIn('a.trans_type', $transType)
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
            ->get();
        $trans2 = DB::connection('pgsql')->table('crm_member_trans_history AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereIn('a.trans_type', $transType)
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
            ->get();
        $transactions = $trans2->merge($trans1)->all();
        $transactions = (new Collection($transactions))->paginate(5);
        return $transactions;
    }

    public function myRedemption($mbrId, $fdate, $tdate)
    {
        $redemptions = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->where('a.coy_id', 'CTL')
            ->whereRaw('(a.trans_type = \'RD\' OR a.trans_type = \'TP\')')
            ->where('a.trans_points', '<', '0')
            ->distinct()
            ->orderby('a.created_on', 'desc')
            ->paginate(5);
        return $redemptions;
    }

    public function myRedemptionHistory($mbrId, $fdate, $tdate)
    {
        $redemption1 = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->where('a.coy_id', 'CTL')
            ->whereRaw('(a.trans_type = \'RD\' OR a.trans_type = \'TP\')')
            ->where('a.trans_points', '<', '0')
            ->distinct()
            ->orderby('a.created_on', 'desc')
            ->get();
        $redemption2 = DB::connection('pgsql')->table('crm_member_trans_history AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->where('a.coy_id', 'CTL')
            ->whereRaw('(a.trans_type = \'RD\' OR a.trans_type = \'TP\')')
            ->where('a.trans_points', '<', '0')
            ->distinct()
            ->orderby('a.created_on', 'desc')
            ->get();
        $redemptions = $redemption2->merge($redemption1)->all();
        $redemptions = (new Collection($redemptions))->paginate(5);
        return $redemptions;
    }

    public function myWarranty($mbrId, $fdate, $tdate)
    {
        $warranty = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
            ->paginate(5);
        return $warranty;
//        WHERE a.trans_type IN ($transType)
//        and a.item_id NOT IN (select a.item_id from ims_item_alt_id a, ims_item_list b where a.coy_id = b.coy_id and a.alt_id = b.item_id and b.coy_id = 'CTL' and b.item_type = 'Z')
//        and a.item_id not like '#%' and (a.item_id not like '!%' or a.item_id LIKE '!MEMBER-%' or a.item_id LIKE '!EGIFT-%') and a.item_id != '!MEMBER-HACON' and a.item_desc!=''
//        and ssew.serial_num is not null  and ssew.ext_warranty_upc not in (select item_id from ims_item_list where item_desc like '%SSEW%' and inv_dim6='6 MTH')
    }

    public function myWarrantyHistory($mbrId, $fdate, $tdate)
    {
        $warranty1 = DB::connection('pgsql')->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
            ->get();
        $warranty2 = DB::connection('pgsql')->table('crm_member_trans_history AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbrId)->whereBetween('a.trans_date', [$fdate, $tdate])
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->distinct()
            ->orderBy('a.created_on', 'desc')
            ->get();
        $warranty = $warranty2->merge($warranty1)->all();
        $warranty = (new Collection($warranty))->paginate(5);
        return $warranty;
    }

    public function hachiRedemptions($mbrId, $from, $to)
    {
        $res = DB::connection('pgsql')->select("
                                                (select cmt.loc_id, rtrim(cmt.mbr_id) mbr_id, rtrim(cmt.trans_id) trans_id, cmt.trans_date, cmt.item_desc, cmt.item_qty, cmt.regular_price
                                                from crm_member_transaction cmt
                                                where cmt.trans_type in ('RD', 'TP') and cmt.item_qty>0 and cmt.mbr_id= :mbr_id and cmt.trans_id not like '%-%' 
                                                and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date)
                                                UNION
                                                (select cmt.loc_id, rtrim(cmt.mbr_id) mbr_id, rtrim(cmt.trans_id) trans_id, cmt.trans_date, cmt.item_desc, cmt.item_qty, cmt.regular_price
                                                from crm_member_trans_history cmt
                                                where cmt.trans_type in ('RD', 'TP') and cmt.item_qty>0 and cmt.mbr_id= :mbr_id and cmt.trans_id not like '%-%' and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date)
                                                 order by trans_date desc", ["mbr_id" => $mbrId, "from_date" => $from, "to_date" => $to]);
        return $res;
    }


    public function hachiTransactionItem($mbrId, $from, $to)
    {
        $res = DB::connection('pgsql')->select("
                 SELECT rtrim(cmt.loc_id), rtrim(cmt.mbr_id), rtrim(cmt.trans_id),rtrim(cmt.item_id), cmt.trans_type, cmt.trans_date, cmt.item_desc, cmt.item_qty, rtrim(cmt.item_id)
                 FROM crm_member_transaction cmt 
                 WHERE cmt.trans_type in ('HI','PS','IO','RX','HR','DU')
                 AND cmt.item_id not like '#%' 
                 and cmt.trans_id not like 'HOC%' 
                 and cmt.item_desc != '' 
                 and (cmt.item_id not like '!%' or cmt.item_id LIKE '!MEMBER-%' or cmt.item_id LIKE '!EGIFT-%') 
                 and cmt.item_id != '!MEMBER-HACON' 
                 and cmt.mbr_id = :mbr_id and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date
                 UNION ALL
                 SELECT rtrim(cmt.loc_id), rtrim(cmt.mbr_id), rtrim(cmt.trans_id),rtrim(cmt.item_id), cmt.trans_type, cmt.trans_date, cmt.item_desc, cmt.item_qty, rtrim(cmt.item_id)
                 FROM crm_member_trans_history cmt 
                 WHERE cmt.trans_type in ('HI','PS','IO','RX','HR','DU')
                 AND cmt.item_id not like '#%' 
                 and cmt.trans_id not like 'HOC%' 
                 and cmt.item_desc != '' 
                 and (cmt.item_id not like '!%' or cmt.item_id LIKE '!MEMBER-%' or cmt.item_id LIKE '!EGIFT-%') 
                 and cmt.item_id != '!MEMBER-HACON' 
                 and cmt.mbr_id = :mbr_id and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date
                 ORDER BY cmt.trans_date DESC", ["mbr_id" => $mbrId, "from_date" => $from, "to_date" => $to]);
        return $res;
    }

    public function hachiTransactionList($mbrId, $from, $to)
    {
        $res = DB::connection('pgsql')->select("
                 SELECT rtrim(cmt.loc_id), rtrim(cmt.mbr_id), 
                    rtrim(cmt.trans_id), cmt.trans_type, CAST(cmt.trans_date AS DATE) as trans_date
                 FROM crm_member_transaction cmt 
                 WHERE cmt.trans_type in ('HI','PS','IO','RX','HR','DU')
                 and cmt.item_id not like '#%' 
                 and cmt.trans_id not like 'HOC%' 
                 and cmt.item_desc != '' 
                 and (cmt.item_id not like '!%' or cmt.item_id LIKE '!MEMBER-%' or cmt.item_id LIKE '!EGIFT-%') 
                 and cmt.item_id != '!MEMBER-HACON' 
                 and cmt.mbr_id = :mbr_id and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date
                 UNION 
                 SELECT rtrim(cmt.loc_id), rtrim(cmt.mbr_id), 
                    rtrim(cmt.trans_id), cmt.trans_type, CAST(cmt.trans_date AS DATE) as trans_date
                 FROM crm_member_trans_history cmt 
                 WHERE cmt.trans_type in ('HI','PS','IO','RX','HR','DU')
                 and cmt.item_id not like '#%'  
                 and cmt.trans_id not like 'HOC%' 
                 and cmt.item_desc != '' 
                 and (cmt.item_id not like '!%' or cmt.item_id LIKE '!MEMBER-%' or cmt.item_id LIKE '!EGIFT-%') 
                 and cmt.item_id != '!MEMBER-HACON' 
                 and cmt.mbr_id = :mbr_id and cmt.trans_date >= :from_date and cmt.trans_date  <= :to_date
                 GROUP BY cmt.loc_id, cmt.mbr_id, cmt.trans_id, cmt.trans_type, CAST(cmt.trans_date AS DATE)
                 ORDER BY trans_date DESC", ["mbr_id" => $mbrId, "from_date" => $from, "to_date" => $to]);
        return $res;
    }





}