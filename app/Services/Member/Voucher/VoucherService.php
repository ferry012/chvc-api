<?php namespace App\Services\Member\Voucher;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VoucherService {

    public function myVouchers($mbrId, $fdate, $tdate) {
        $now = Carbon::now();
        $datas = DB::connection('pgsql')->table('crm_voucher_list AS a')
            ->select('a.trans_id', 'a.status_level AS status_level', 'a.issue_date', 'a.coupon_serialno','a.issue_type',
                'a.expiry_date',DB::raw('rtrim(a.receipt_id2) receipt_id2, rtrim(a.mbr_id) mbr_id'))
            ->where('a.mbr_id', $mbrId)
            ->whereNotNull('a.issue_type')->whereNotIn('a.issue_type', ['GV', 'CREDIT'])
            ->where('a.status_level', '<>', '-1')->whereBetween('a.expiry_date', [$fdate, $tdate])
            ->orderby('a.expiry_date', 'desc')
            ->get();
        foreach ($datas as $data=>$value) {
            if (empty($value->receipt_id2) || ctype_space($value->receipt_id2)) {
                if ($value->expiry_date > $now) {
                    $value->status = 'Valid';
                    $value->valid_status = 1;
                } else {
                    $value->status = 'Expired';
                    $value->valid_status = 0;
                }
            } else {
                $value->status = 'Redeemed';
            }
            $value->qty_reserved = 1;
        }
        return $datas;
    }

    public function getAvailableVouchers($mbrId, $couponId)
    {
        $now = Carbon::now();
        if ($couponId == '*') {
            $datas = DB::connection('pgsql')->table('crm_voucher_list AS a')
                ->select('a.trans_id', 'a.status_level AS status_level', 'a.issue_date', 'a.coupon_serialno','a.issue_type',
                    'a.expiry_date', 'a.receipt_id2','a.mbr_id')
                ->where('a.mbr_id', $mbrId)
                ->whereNotNull('a.issue_type')->whereNotIn('a.issue_type', ['GV', 'CREDIT'])
                ->whereBetween('a.status_level', [0, 1])->where('a.expiry_date', '>', $now)
                ->orderby('a.expiry_date', 'desc')->get();
        } else {
            $datas = DB::connection('pgsql')->table('crm_voucher_list AS a')
                ->select('a.trans_id', 'a.status_level AS status_level', 'a.issue_date', 'a.coupon_serialno','a.issue_type',
                    'a.expiry_date', 'a.receipt_id2','a.mbr_id')
                ->where('a.mbr_id', $mbrId)
                ->whereNotNull('a.issue_type')->whereNotIn('a.issue_type', ['GV', 'CREDIT'])
                ->whereBetween('a.status_level', [0, 1])->where('a.expiry_date', '>', $now)->where('a.coupon_id', $couponId)
                ->orderby('a.expiry_date', 'desc')->get();
        }
        foreach ($datas as $data=>$value) {
            if (empty($value->receipt_id2) || ctype_space($value->receipt_id2)) {
                if ($value->expiry_date > $now) {
                    $value->status = 'Valid';
                    $value->valid_status = 1;
                } else {
                    $value->status = 'Expired';
                    $value->valid_status = 0;
                }
            } else {
                $value->status = 'Redeemed';
            }
            $value->qty_reserved = 1;
        }
        return $datas;
    }

    public function getPosVoucher($mbrId)
    {
        $now = Carbon::now();

        $data = DB::connection('pgsql')->table('crm_voucher_list AS a')
            ->leftJoin('crm_voucher_data AS d', 'a.coupon_id', '=', 'd.coupon_id')
            ->select('a.expiry_date',
                DB::raw("rtrim(a.coupon_id) as coupon_id"),
                DB::raw("rtrim(a.coupon_serialno) as coupon_code"),
                DB::raw("CASE WHEN d.coupon_name IS NOT NULL THEN d.coupon_name ELSE rtrim(a.coupon_id) END AS coupon_name"),
                DB::raw("CASE WHEN d.coupon_excerpt IS NOT NULL THEN d.coupon_excerpt ELSE rtrim(a.coupon_id) END AS coupon_excerpt")
//                , DB::raw('count(*) as quantity')
            )
            ->where('a.status_level', 0)
            ->where('a.expiry_date', '>', $now)
            ->whereRaw('LOWER(mbr_id)=?', trim(strtolower($mbrId)))
            ->where('a.coupon_id', '<>', "!VCH-CREDIT")
//            ->groupBy('a.expiry_date', 'a.coupon_id', 'd.coupon_name', 'd.coupon_excerpt')
            ->get()->toArray();

        $rebate_voucher = DB::connection('pgsql')->table('crm_member_list')->where('coy_id', 'CTL')->where('mbr_id', $mbrId)->first()->rebate_voucher;
        $rebate_data = [];
        if (!empty(rtrim($rebate_voucher, " "))) {
            $rebate_data = DB::connection('pgsql')->select('select v.expiry_date, rtrim(v.item_id) coupon_id, rtrim(v.voucher_id) coupon_code, \'$40 Rebate Voucher\', \'$40 Rebate Voucher\'
                                    from sms_voucher_list v where v.coy_id=\'CTL\' and v.item_id=\'!VCH-REBATE\' and v.voucher_id=? and v.status_level=2', array($rebate_voucher));
        }
        $data = array_merge($data, $rebate_data);

        return $data;
    }

    public function getExpiredUnutilizedVouchers($mbrId)
    {
        $now = Carbon::now();
        $datas = DB::connection('pgsql')->table('crm_voucher_list AS a')
            ->select('a.trans_id', 'a.status_level AS status_level', 'a.issue_date', 'a.coupon_serialno', 'a.issue_type',
                'a.expiry_date','a.receipt_id2', 'a.mbr_id')
            ->where('a.mbr_id', $mbrId)
            ->whereNotNull('a.issue_type')->whereNotIn('a.issue_type', ['GV', 'CREDIT'])
            ->whereBetween('a.status_level', [0, 1])->where('a.expiry_date', '<=', $now)
            ->orderby('a.expiry_date', 'desc')->get();
        foreach ($datas as $data=>$value) {
            if (empty($value->receipt_id2) || ctype_space($value->receipt_id2)) {
                if ($value->expiry_date > $now) {
                    $value->status = 'Valid';
                    $value->valid_status = 1;
                } else {
                    $value->status = 'Expired';
                    $value->valid_status = 0;
                }
            } else {
                $value->status = 'Redeemed';
            }
            $value->qty_reserved = 1;
        }
        return $datas;
    }

    public function myeGift($mbrId, $fdate, $tdate)
    {
        $now = Carbon::now();
        $datas = DB::connection('pgsql')->table('crm_voucher_list AS t')
            ->select('t.receipt_id2', 't.expiry_date as valid_until', 't.voucher_amount as amount', 't.coupon_serialno',
                't.voucher_amount as amount')
            ->where('t.mbr_id', $mbrId)
            ->where('issue_type', 'GV')
            ->whereNotNull('t.voucher_amount')->where('t.voucher_amount', '>', 0)
            ->whereBetween('t.expiry_date', [$fdate, $tdate])
            ->orderby('t.expiry_date', 'desc')
            ->get();
        foreach ($datas as $data=>$value) {
            if (empty($value->receipt_id2) || ctype_space($value->receipt_id2)) {
                if ($value->valid_until > $now) {
                    $value->status = 'Valid';
                } else {
                    $value->status = 'Expired';
                }
            } else {
                $value->status = 'Redeemed';
            }
            $value->amount = '$'.$value->amount;
            unset($value->receipt_id2);
        }
        return $datas;
    }

    public function myCredit($mbrId, $status)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = DB::connection('pgsql')->table('crm_voucher_list AS v')
            ->select('expiry_date', 'status_level', 'coupon_serialno', DB::raw("rtrim(receipt_id1) receipt_id1, rtrim(receipt_id2) receipt_id2"), 'voucher_amount', 'issue_date',
                'utilize_date'
            )->whereRaw("NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)") //and (rtrim(coupon_id)='!VCH-CREDIT' or rtrim(coupon_id)='!MREP-10OFF50AG')
            ->where('mbr_id', $mbrId)
            ->where('status_level', $status)
            ->whereRaw("(rtrim(coupon_id)='!VCH-CREDIT'  or rtrim(coupon_id)='!MREP-10OFF50AG')")
            ->where('expiry_date', '>=', $now)
            ->orderBy('expiry_date', 'DESC')->get();
        return $data;
    }

    public function paymentVoucher($mbrId, $status)
    {
        $now = Carbon::now()->toDateTimeString();

        $sql = "SELECT v.coupon_id, coalesce(n.coupon_name,v.coupon_id) coupon_name, v.expiry_date, v.status_level, v.coupon_serialno, rtrim(v.receipt_id1) receipt_id1, rtrim(v.receipt_id2) receipt_id2, v.voucher_amount, v.issue_date, v.utilize_date
        FROM crm_voucher_list v
        JOIN ims_item_list i on v.coy_id=i.coy_id and rtrim(v.coupon_id)=rtrim(i.item_id) and i.item_type in ('C','E')
        LEFT JOIN crm_voucher_data n on v.coy_id=n.coy_id and rtrim(v.coupon_id)=rtrim(n.coupon_id)
        WHERE v.coy_id = ? and v.mbr_id= ? and v.coupon_id != '!MREP-10OFF50AG'
        and v.status_level = ? and expiry_date >= ?   and v.sale_date <= ?
            AND NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)
        UNION ALL
        SELECT v.coupon_id, coalesce(n.coupon_name,v.coupon_id) coupon_name, v.expiry_date, v.status_level, v.coupon_serialno, rtrim(v.receipt_id1) receipt_id1, rtrim(v.receipt_id2) receipt_id2, v.voucher_amount, v.issue_date, v.utilize_date
            FROM crm_voucher_list v
            JOIN ims_item_list i on v.coy_id=i.coy_id and rtrim(v.coupon_id)=rtrim(i.item_id) and i.item_type in ('C','E')
            LEFT JOIN crm_voucher_data n on v.coy_id=n.coy_id and rtrim(v.coupon_id)=rtrim(n.coupon_id)
            WHERE v.coy_id = ? and v.mbr_id = ? 
            and 0=(select SUM(case when coalesce(utilize_date::date,'1900-01-01') = now()::date then 1 else 0 end) from crm_voucher_list cvl where coupon_id = '!MREP-10OFF50AG' and mbr_id= ?)
            and v.coupon_serialno  in (select min(coupon_serialno) from crm_voucher_list cvl where coupon_id = '!MREP-10OFF50AG' and mbr_id=? and status_level=0)
            and v.status_level = ? and expiry_date >= ?   and v.sale_date <= ?
            AND NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)";

//      $db =  DB::connection('pgsql')->select($sql, ['CTL', $mbrId, $status, $now, $now,'CTL', $mbrId,$mbrId,$mbrId, $status, $now, $now]);
////       dd($db);
      // $sql = "SELECT v.coupon_id, coalesce(n.coupon_name,v.coupon_id) coupon_name, v.expiry_date, v.status_level, v.coupon_serialno, rtrim(v.receipt_id1) receipt_id1, rtrim(v.receipt_id2) receipt_id2, v.voucher_amount, v.issue_date, v.utilize_date
        //         FROM crm_voucher_list v
        //         JOIN ims_item_list i on v.coy_id=i.coy_id and rtrim(v.coupon_id)=rtrim(i.item_id) and i.item_type in ('C','E')
        //         LEFT JOIN crm_voucher_data n on v.coy_id=n.coy_id and rtrim(v.coupon_id)=rtrim(n.coupon_id)
        //         WHERE v.coy_id = ? and v.mbr_id= ? and v.status_level = ? and expiry_date >= ?   and v.sale_date <= ?
        //             AND NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)
        //         ORDER BY v.expiry_date DESC";
        return DB::connection('pgsql')->select($sql, ['CTL', $mbrId, $status, $now, $now,'CTL', $mbrId,$mbrId,$mbrId, $status, $now, $now]);
/*
        $data = DB::connection('pgsql')->table('crm_voucher_list AS v')
            ->select('coupon_id', 'expiry_date', 'status_level', 'coupon_serialno', DB::raw("rtrim(receipt_id1) receipt_id1, rtrim(receipt_id2) receipt_id2"), 'voucher_amount', 'issue_date',
                'utilize_date'
            )->whereRaw('NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)')
            ->where('mbr_id', $mbrId)
            ->where('status_level', $status)
//            ->whereRaw("rtrim(coupon_id)='!VCH-CREDIT'")
            ->whereRaw("coalesce(coupon_id, '') IN (select rtrim(item_id) from ims_item_list where item_type in ('C', 'E'))")
            ->where('expiry_date', '>=', $now)
            ->orderBy('expiry_date', 'DESC')->get();
        return $data;
*/
    }

    public function getCreditBySerial($serial)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = DB::connection('pgsql')->table('crm_voucher_list')
            ->select('expiry_date', 'status_level', 'coupon_serialno', DB::raw("rtrim(receipt_id1) receipt_id1, rtrim(receipt_id2) receipt_id2"), 'voucher_amount', 'issue_date',
                'utilize_date', 'mbr_id'
            )->where(['coy_id'=>'CTL','coupon_serialno'=>$serial])
            ->whereRaw("rtrim(coupon_id)='!VCH-CREDIT'")
            ->first();
        if (!is_null($data)) {
            $reserve = DB::connection('pgsql')->table('crm_member_trans_credit_reserved')
                ->where(['coupon_serialno'=>$serial, 'status'=>1])->where('expires', '>', $now)->exists();
            if ($reserve) {
                $data->reserved = 1;
            } else {
                $data->reserved = 0;
            }
        }
        return $data;
    }

    public function checkCreditExists($coyId, $mbrId, $serialNo)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = DB::connection('pgsql')->table('crm_voucher_list AS v')
            ->whereRaw('NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)')
            ->where('status_level', 0)
            ->where('expiry_date', '>=', $now)
            ->where([
                'mbr_id' => $mbrId,
                'issue_type' => 'CREDIT',
                'coy_id' => $coyId,
                'coupon_serialno' => $serialNo
                ])->exists();
        return $data;
    }

    public function checkVoucherExists($coyId, $mbrId, $serialNo)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = DB::connection('pgsql')->table('crm_voucher_list AS v')
            ->whereRaw('NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)')
            ->where('status_level', 0)
            ->where('expiry_date', '>=', $now)
            ->where([
                'mbr_id' => $mbrId,
                'issue_type' => 'PAY_VCH',
                'coy_id' => $coyId,
                'coupon_serialno' => $serialNo
            ])->exists();
        return $data;
    }

    public function checkGiftExists($coyId, $mbrId, $serialNo)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = DB::connection('pgsql')->table('crm_voucher_list AS v')
            ->whereRaw('NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)')
            ->where('status_level', '>=', 0)
            ->where('expiry_date', '>=', $now)
            ->where([
                'mbr_id' => $mbrId,
                'issue_type' => 'GV',
                'coy_id' => $coyId,
                'coupon_serialno' => $serialNo
            ])->exists();
        return $data;
    }

    public function hachiGift($mbrId, $status)
    {
        $now = Carbon::now()->toDateTimeString();
        return DB::connection('pgsql')->table('crm_voucher_list AS v')->leftJoin('crm_voucher_egift as e', 'v.coupon_serialno', '=', 'e.coupon_serialno')
            ->select(
                'e.gift_image','v.expiry_date', 'v.voucher_amount', 'v.status_level', 'v.coupon_serialno'
            )->whereRaw('NOT EXISTS (SELECT 1 FROM crm_member_trans_credit_reserved c
              WHERE v.mbr_id = c.mbr_id AND v.coy_id = c.coy_id AND v.coupon_serialno = c.coupon_serialno AND c.expires > now() AND c.status = 1)')
            ->where('v.mbr_id', $mbrId)
            ->where('v.status_level', $status)
            ->where('v.issue_type', 'GV')
            ->where('v.expiry_date', '>=', $now)
            ->orderBy('v.expiry_date', 'DESC')->get();
    }





}