<?php namespace App\Services\Member\Points;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PointsService {
    public function pointsSummary($from, $to)
    {
//        DB::beginTransaction();
//        try {
            DB::statement("DROP TABLE IF EXISTS xx_crm_member_list");
            DB::insert("SELECT
                         a.coy_id
                        ,a.mbr_id
                        ,a.exp_date
                        ,(a.points_accumulated - a.points_redeemed-a.points_expired) as points_balance
                        ,0 as points_balance_after
                        ,0 as points_balance_after_his
                        ,0 as points_issued
                        ,0 as points_issued_his
                        ,0 as points_redeemed
                        ,0 as points_redeemed_his
                        ,0 as points_expired  -- will remove AP after running 1st 2013 report
                        ,0 as points_expired_his  -- will remove AP after running 1st 2013 report
                        ,50000 points_group, 50000 points_group_to
                    into xx_crm_member_list
                    from crm_member_list a where a.coy_id='CTL'");
//            -- a. points_balance_after
            DB::update("UPDATE xx_crm_member_list SET points_balance_after = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_transaction b
                            where b.trans_date > :dateTo
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to]);
//            -- b. points_issued
            DB::update("UPDATE xx_crm_member_list SET points_issued = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_transaction b
                            where b.trans_date::date >= :dateFrom and b.trans_date::date <= :dateTo
                                and not (b.trans_type ='RD' or b.trans_type = 'EP' or (b.trans_type='AP' and left(b.item_desc,5) ='Accum'))
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- b. points_issued
            DB::update("UPDATE xx_crm_member_list SET points_issued = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_transaction b
                            where b.trans_date::date >= :dateFrom and b.trans_date::date <= :dateTo
                                and not (b.trans_type ='RD' or b.trans_type = 'EP' or (b.trans_type='AP' and left(b.item_desc,5) ='Accum'))
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- c. points_redeemed
            DB::update("UPDATE xx_crm_member_list SET points_redeemed = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_transaction b
                            where b.trans_date::date <= :dateTo and b.trans_date::date >= :dateFrom
                                and b.trans_type ='RD'
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- d. points_expired
            DB::update("UPDATE xx_crm_member_list SET points_expired = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_transaction b
                            where b.trans_date::date >= :dateFrom and b.trans_date::date <= :dateTo
                                and (b.trans_type ='EP' or (b.trans_type='AP' and left(b.item_desc,5) ='Accum'))
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- a. points_balance_after_his
            DB::update("UPDATE xx_crm_member_list SET points_balance_after_his = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_trans_history b
                            where b.trans_date > :dateTo
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to]);
//            -- b. points_issued_his
            DB::update("UPDATE xx_crm_member_list SET points_issued_his = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_trans_history b
                            where b.trans_date::date >= :dateFrom and b.trans_date::date <= :dateTo
                                and not (b.trans_type ='RD' or b.trans_type = 'EP' or (b.trans_type='AP' and left(b.item_desc,5) ='Accum'))
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- c. points_redeemed_his
            DB::update("UPDATE xx_crm_member_list SET points_redeemed_his = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_trans_history b
                            where b.trans_date::date <= :dateTo and b.trans_date::date >= :dateFrom
                                and b.trans_type ='RD'
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
//            -- d. points_expired_his
            DB::update("UPDATE xx_crm_member_list SET points_expired_his = g.pts
                        FROM (
                            select coy_id,mbr_id, sum(b.trans_points) pts from crm_member_trans_history b
                            where b.trans_date::date >= :dateFrom and b.trans_date::date <= :dateTo
                                and (b.trans_type ='EP' or (b.trans_type='AP' and left(b.item_desc,5) ='Accum'))
                            group by coy_id,mbr_id
                        ) g
                        WHERE xx_crm_member_list.coy_id=g.coy_id and xx_crm_member_list.mbr_id=g.mbr_id", ['dateTo' => $to, 'dateFrom' => $from]);
            DB::update("update xx_crm_member_list set points_balance_after=0 where points_balance_after is null");
            DB::update("update xx_crm_member_list set points_issued=0 where points_issued is null");
            DB::update("update xx_crm_member_list set points_redeemed=0 where points_redeemed is null");
            DB::update("update xx_crm_member_list set points_expired=0 where points_expired is null");
            DB::update("update xx_crm_member_list set points_balance_after_his=0 where points_balance_after_his is null");
            DB::update("update xx_crm_member_list set points_issued_his=0 where points_issued_his is null");
            DB::update("update xx_crm_member_list set points_redeemed_his=0 where points_redeemed_his is null");
            DB::update("update xx_crm_member_list set points_expired_his=0 where points_expired_his is null");
            DB::update("update xx_crm_member_list set points_balance_after = points_balance_after + points_balance_after_his");
            DB::update("update xx_crm_member_list set points_issued = points_issued + points_issued_his");
            DB::update("update xx_crm_member_list set points_redeemed = points_redeemed + points_redeemed_his");
            DB::update("update xx_crm_member_list set points_expired = points_expired + points_expired_his");
//           Determine the points_group
            DB::update("update xx_crm_member_list b set
                            points_group = CASE
                                WHEN (b.points_balance - b.points_balance_after) >=-100000 and (b.points_balance - b.points_balance_after) <=499 THEN 1
                                WHEN (b.points_balance - b.points_balance_after) >= 500 and (b.points_balance - b.points_balance_after) <=999 THEN 500
                                WHEN (b.points_balance - b.points_balance_after) >= 1000 and (b.points_balance - b.points_balance_after) <=1499 THEN 1000
                                WHEN (b.points_balance - b.points_balance_after) >= 1500 and (b.points_balance - b.points_balance_after) <=1999 THEN 1500
                                WHEN (b.points_balance - b.points_balance_after) >= 2000 and (b.points_balance - b.points_balance_after) <=2999 THEN 2000
                                WHEN (b.points_balance - b.points_balance_after) >= 3000 and (b.points_balance - b.points_balance_after) <=3499 THEN 3000
                                WHEN (b.points_balance - b.points_balance_after) >= 3500 and (b.points_balance - b.points_balance_after) <=3999 THEN 3500
                                WHEN (b.points_balance - b.points_balance_after) >= 4000 and (b.points_balance - b.points_balance_after) <=4999 THEN 4000
                                WHEN (b.points_balance - b.points_balance_after) >= 5000 and (b.points_balance - b.points_balance_after) <=6999 THEN 5000
                                WHEN (b.points_balance - b.points_balance_after) >= 7000 and (b.points_balance - b.points_balance_after) <=7499 THEN 7000
                                WHEN (b.points_balance - b.points_balance_after) >= 7500 and (b.points_balance - b.points_balance_after) <=7999 THEN 7500
                                WHEN (b.points_balance - b.points_balance_after) >= 8000 and (b.points_balance - b.points_balance_after) <=9999 THEN 8000
                                WHEN (b.points_balance - b.points_balance_after) >= 10000 and (b.points_balance - b.points_balance_after) <=19999 THEN 10000
                                WHEN (b.points_balance - b.points_balance_after) >= 20000 and (b.points_balance - b.points_balance_after) <=34999 THEN 20000
                                WHEN (b.points_balance - b.points_balance_after) >= 35000 and (b.points_balance - b.points_balance_after) <=49999 THEN 35000
                                WHEN (b.points_balance - b.points_balance_after) >= 50000 and (b.points_balance - b.points_balance_after) <=9999999 THEN 50000
                                END
                            , points_group_to = CASE
                                WHEN (b.points_balance - b.points_balance_after) >=-100000 and (b.points_balance - b.points_balance_after) <=499 THEN 499
                                WHEN (b.points_balance - b.points_balance_after) >= 500 and (b.points_balance - b.points_balance_after) <=999 THEN 999
                                WHEN (b.points_balance - b.points_balance_after) >= 1000 and (b.points_balance - b.points_balance_after) <=1499 THEN 1499
                                WHEN (b.points_balance - b.points_balance_after) >= 1500 and (b.points_balance - b.points_balance_after) <=1999 THEN 1999
                                WHEN (b.points_balance - b.points_balance_after) >= 2000 and (b.points_balance - b.points_balance_after) <=2999 THEN 2999
                                WHEN (b.points_balance - b.points_balance_after) >= 3000 and (b.points_balance - b.points_balance_after) <=3499 THEN 3499
                                WHEN (b.points_balance - b.points_balance_after) >= 3500 and (b.points_balance - b.points_balance_after) <=3999 THEN 3999
                                WHEN (b.points_balance - b.points_balance_after) >= 4000 and (b.points_balance - b.points_balance_after) <=4999 THEN 4999
                                WHEN (b.points_balance - b.points_balance_after) >= 5000 and (b.points_balance - b.points_balance_after) <=6999 THEN 6999
                                WHEN (b.points_balance - b.points_balance_after) >= 7000 and (b.points_balance - b.points_balance_after) <=7499 THEN 7499
                                WHEN (b.points_balance - b.points_balance_after) >= 7500 and (b.points_balance - b.points_balance_after) <=7999 THEN 7999
                                WHEN (b.points_balance - b.points_balance_after) >= 8000 and (b.points_balance - b.points_balance_after) <=9999 THEN 9999
                                WHEN (b.points_balance - b.points_balance_after) >= 10000 and (b.points_balance - b.points_balance_after) <=19999 THEN 19999
                                WHEN (b.points_balance - b.points_balance_after) >= 20000 and (b.points_balance - b.points_balance_after) <=34999 THEN 34999
                                WHEN (b.points_balance - b.points_balance_after) >= 35000 and (b.points_balance - b.points_balance_after) <=49999 THEN 49999
                                WHEN (b.points_balance - b.points_balance_after) >= 50000 and (b.points_balance - b.points_balance_after) <=9999999 THEN 9999999
                                END");
//            final: insert stats to crm_member_points_summary
            DB::insert("insert into crm_member_points_summary
                        select 	'CTL' , '$from' , '$to', ROW_NUMBER() OVER(ORDER BY points_group) line_num ,
                                    points_group p_from, points_group_to p_to,
                                sum(b.points_balance - b.points_balance_after) points_balance,
                                sum(b.points_issued) points_issued,
                                sum(b.points_redeemed) * -1 points_redeemed,
                                sum(b.points_expired) * -1 points_expired,
                                sum(case when exp_date::date > :dateFrom and exp_date::date <= :dateTo then 1 else 0 end) total_expired,
                                sum(case when exp_date::date > :dateTo then 1 else 0 end) total_members,
                            'vclub', current_timestamp, 'vclub', current_timestamp
                            FROM xx_crm_member_list b
                            GROUP BY points_group, points_group_to ORDER BY points_group", ['dateTo' => $to, 'dateFrom' => $from]);

//            DB::commit();
//
//        } catch (\Exception $e) {
//            DB::rollback();
//        }
    }
}