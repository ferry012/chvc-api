<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class ApiClient
{
    private $apiKey;
    private $url;
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->apiKey = 'Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ';
        $this->url = env('IMSAPI_URL', '');
    }

    public function get($endpoint, $options = [])
    {
        $res = $this->rawRequest('GET', $endpoint, $options);
        if ($res === 404){
            return $res;
        } else if ($res === 403){
            return [];
        } else {
            return json_decode($res->getBody()->getContents(), true);
        }
    }

    public function post($endpoint, $options = [])
    {
//        print_r($options);
        return json_decode(
            $this->rawRequest('POST', $endpoint, $options)
                ->getBody()
                ->getContents(),
            true
        );
    }

    public function put($endpoint, $options = [])
    {
        return json_decode(
            $this->rawRequest('PUT', $endpoint, $options)
                ->getBody()
                ->getContents()
        );
    }

    public function delete(string $endpoint, array $options = [])
    {
        return json_decode(
            $this->rawRequest('DELETE', $endpoint, $options)
                ->getBody()
                ->getContents()
        );
    }

    public function rawRequest(string $method, $uri, array $options = [])
    {
        try {
            if ( substr($uri,0,4)=='http' ) {
                $url = $uri;
            }
            else {
                $url = $this->url . $uri;
            }
//            echo $url;
            $options['headers']['Authorization'] = 'Bearer ' . $this->apiKey;
            $options['headers']['Content-Type'] = 'application/json';
            $res = $this->client->request($method, $url, $options);
            return $res;
        } catch (BadResponseException $e) {
            return $e->getCode();
        }
    }
}
