<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class PosapiService
{
    protected $authorization;
    protected $endpoint;

    public function __construct(Client $client) {
        $this->endpoint = env("POSAPI_URL");
        $this->authorization = 'Bearer fw6sl9LBIQJIYwRJse5DanMH4OTwtbIm';
    }

    public function get_invoice($id){

        $apiurl = $this->endpoint . 'orders/'. $id .'';

        $apibody = [
            'headers'       => ['Content-Type' => 'application/json', 'Authorization' => $this->authorization],
            'http_errors'   => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $product = json_decode($response->getBody()->getContents(),true);

        return $product;

    }

}
