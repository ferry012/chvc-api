<?php

namespace App\Services\POS;

class PosService extends AbstractService
{
    protected $client;
    protected $endpoint;

    public function __construct(ApiClient $client) {
        $this->client = $client;
    }

    public function get_transaction($trans_id){
        $endpoint = $trans_id;
        $trans = $this->client->get($endpoint, []);
        return $trans;
    }

}
