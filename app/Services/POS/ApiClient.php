<?php

namespace App\Services\POS;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class ApiClient
{
    private $apiKey;
    private $url;
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->apiKey = '?refund=Y&token=01HQPlbhcsJGdURriLvoa4fh0Meu62Qf';
        $this->url = 'http://pos.api.valueclub.asia/orders/';
    }

    public function get($endpoint, $options = [])
    {
        $res = $this->rawRequest('GET', $endpoint, $options);
        if ($res === 404){
            return $res;
        } else if ($res === 403){
            return [];
        } else {
            return json_decode($res->getBody()->getContents(), true);
        }
    }

    public function post($endpoint, $options = [])
    {
        return json_decode(
            $this->rawRequest('POST', $endpoint, $options)
                ->getBody()
                ->getContents(),
            true
        );
    }

    public function put($endpoint, $options = [])
    {
        return json_decode(
            $this->rawRequest('PUT', $endpoint, $options)
                ->getBody()
                ->getContents()
        );
    }

    public function delete(string $endpoint, array $options = [])
    {
        return json_decode(
            $this->rawRequest('DELETE', $endpoint, $options)
                ->getBody()
                ->getContents()
        );
    }

    public function rawRequest(string $method, $uri, array $options = [])
    {
        try {
            if ( substr($uri,0,4)=='http' ) {
                $url = $uri;
            }
            else {
                $url = $this->url . $uri . $this->apiKey;
            }
//            $options['headers']['Authorization'] = 'Bearer ' . $this->apiKey;
            $options['headers']['Content-Type'] = 'application/json';
            $res = $this->client->request($method, $url, $options);
            return $res;
        } catch (BadResponseException $e) {
            return $e->getCode();
        }
    }
}
