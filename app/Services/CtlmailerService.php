<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class CtlmailerService
{
    protected $client;
    protected $endpoint;
    protected $authorization;

    public function __construct(Client $client) {

        $this->client = $client;
        $this->endpoint = 'https://ctlmailer.api.valueclub.asia/';

        $this->authorization = '8f413c39b4d1b4e7c584943df22e7a8c';
    }

    public function send_sms($recipients, $subject, $from = 'VALUECLUB'){

        if (strlen($recipients) !== 8 && substr($recipients,0,1) != '8' && substr($recipients,0,1) != '9') {
            return (object) [
                'code'    => 0,
                'message' => 'Phone number not allowed'
            ];
        }

        $endpoint = $this->endpoint . 'sms';
        $body = [
            'recipients'    => '+65' . $recipients,
            'from_address'  => $from,
            'subject'       => substr($subject, 0, 160)
        ];

        $call = $this->client->post($endpoint, [
            'headers'       => ['Authorization' => $this->authorization],
            'body'          => json_encode($body),
            'http_errors'   => false
        ]);
        $response = $call->getBody()->getContents();
        return json_decode($response);

    }

}
