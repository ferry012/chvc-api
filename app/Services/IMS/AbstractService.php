<?php

namespace App\Services\IMS;

use App\Services\ApiClient;

abstract class AbstractService
{
    /** @var ApiClient $client */
    protected $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }
}