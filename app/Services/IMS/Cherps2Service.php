<?php

namespace App\Services\IMS;

use App\Services\ApiClient;

class Cherps2Service extends AbstractService
{
    protected $client;
    protected $endpoint;

    public function __construct(ApiClient $client) {

        $this->client = $client;
//        if (env('APP_ENV')=="production")
//            $this->endpoint = 'http://chvoices.challenger.sg/';
//        else
//            $this->endpoint = 'http://chvoices-test.challenger.sg/';
    }

    public function my_warranty($mbr_id, $eff_from, $eff_to){
        $endpoint = $this->endpoint . 'api/my_warranty/' . $mbr_id . '/' . $eff_from .'/' . $eff_to;
        $warranty = $this->client->get($endpoint, []);
        return $warranty['data'];
    }

    public function get_ref_key($trans_id){
        $endpoint = $this->endpoint . 'api/get_url_key/' . $trans_id;
        $ref_key = $this->client->get($endpoint, []);
        return $ref_key['data'];
    }

    public function get_cust($cust_id)
    {
        $endpoint = 'api/get_cust/' . $cust_id;
        $cust_info = $this->client->get($endpoint, []);
        return $cust_info['data'];
    }

    public function all_cust($cust)
    {
        $endpoint = 'api/all_cust';
        $cust_info = $this->client->post($endpoint, $cust);
        return $cust_info['data'];
    }

    public function create_cust($cust_info)
    {
        $endpoint = 'api/create_cust';
        $cust_info = $this->client->post($endpoint, $cust_info);
        return $cust_info['data']['cust_id'];
    }

}
