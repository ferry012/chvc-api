<?php

namespace App\Models\Member\Redemption;

use Illuminate\Database\Eloquent\Model;

class Redemption extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_redemption_list';

    protected $guarded = [];

    protected $primaryKey = 'line_num';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}