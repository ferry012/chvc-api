<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'coy_address_book';

    protected $guarded = [];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

//    protected $primaryKey = 'ref_id';

//    const CREATED_AT = 'created_on';
//    const UPDATED_AT = 'updated_on';
}
