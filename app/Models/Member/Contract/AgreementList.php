<?php

namespace App\Models\Member\Contract;

use Illuminate\Database\Eloquent\Model;

class AgreementList extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'sls_agreement_list';

    protected $guarded = [];

    protected $primaryKey = 'contract_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
