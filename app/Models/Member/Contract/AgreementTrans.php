<?php

namespace App\Models\Member\Contract;

use Illuminate\Database\Eloquent\Model;

class AgreementTrans extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'sls_agreement_trans';

    protected $guarded = [];

    protected $primaryKey = 'contract_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
