<?php

namespace App\Models\Member\Contract;

use Illuminate\Database\Eloquent\Model;

class ItemList extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'sls_item_list';

    protected $guarded = [];

    protected $primaryKey = 'item_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
