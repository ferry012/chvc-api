<?php

namespace App\Models\Member\Contract;

use Illuminate\Database\Eloquent\Model;

class CustomerList extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'sls_customer_list';

    protected $guarded = [];

    protected $primaryKey = 'cust_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
