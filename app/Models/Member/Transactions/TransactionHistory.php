<?php

namespace App\Models\Member\Transactions;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_trans_history';

    protected $guarded = [];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;
}