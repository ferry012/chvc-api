<?php

namespace App\Models\Member\Transactions;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_transaction';

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    /**
     * Create the line number of transaction.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
//    public static function boot()
//    {
//        parent::boot();
//
//        static::creating(function ($transaction) {
//            $next_line = Transaction::where('coy_id','CTL')->where('mbr_id', $transaction->mbr_id)->where('trans_id',$transaction->trans_id)
//                ->max('line_num');
//            if ($next_line === null) {
//                $line_num = 1;
//            } else {
//                $line_num = $next_line + 1;
//            }
//            $transaction->line_num = $line_num;
//
//        });
//    }
}