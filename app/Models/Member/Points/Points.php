<?php

namespace App\Models\Member\Points;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_points';

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    /**
     * create the line number of points.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($points) {
            $next_line = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? ", ['CTL', trim($points->mbr_id)])
                //where('coy_id', 'CTL')->where('mbr_id', $points->mbr_id)
                ->max('line_num');
            if ($next_line) {
                $line_num = $next_line + 1;
            } else {
                $line_num = 1;
            }
            $points->line_num = $line_num;
        });
    }

}
