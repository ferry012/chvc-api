<?php

namespace App\Models\Member\Points;

use Illuminate\Database\Eloquent\Model;

class RebateReserved extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_trans_reserved';

    protected $guarded = [];

    protected $primaryKey = 'trans_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
