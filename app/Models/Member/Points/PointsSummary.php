<?php

namespace App\Models\Member\Points;

use Illuminate\Database\Eloquent\Model;

class PointsSummary extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_points_summary';

    protected $guarded = [];

    protected $primaryKey = 'coy_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    protected $perPage = 16;

    /**
     * create the line number of points.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
//    public static function boot()
//    {
//        parent::boot();
//
//        static::creating(function ($points) {
//            $next_line = Points::where('coy_id', 'CTL')->where('mbr_id', $points->mbr_id)->max('line_num');
//            if ($next_line) {
//                $line_num = $next_line + 1;
//            } else {
//                $line_num = 1;
//            }
//            $points->line_num = $line_num;
//
//        });
//    }

}
