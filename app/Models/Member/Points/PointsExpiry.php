<?php

namespace App\Models\Member\Points;

use Illuminate\Database\Eloquent\Model;

class PointsExpiry extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_points_expiry';

    protected $guarded = [];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
