<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Model;

class CorporateList extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_corporate_list';

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
