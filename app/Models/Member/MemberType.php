<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_type';

    protected $guarded = [];

//    protected $primaryKey = ['mbr_id', 'eff_from', 'eff_to', 'line_num'];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    /**
     * Update the line number of member type.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($memberType) {
            $next_line = MemberType::where('coy_id','CTL')->where('mbr_id', $memberType->mbr_id)->max('line_num');
            if ($next_line === null) {
                $line_num = 1;
            } else {
                $line_num = $next_line + 1;
            }
            $memberType->line_num = $line_num;
        });
    }

}
