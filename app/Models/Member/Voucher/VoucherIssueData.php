<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;

class VoucherIssueData extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_voucher_issue_data';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $primaryKey = 'id';

}
