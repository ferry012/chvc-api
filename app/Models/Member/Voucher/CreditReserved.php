<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;

class CreditReserved extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_trans_credit_reserved';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $primaryKey = 'trans_id';

}
