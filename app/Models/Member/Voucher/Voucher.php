<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_voucher_list';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

}
