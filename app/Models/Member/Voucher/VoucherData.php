<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;

class VoucherData extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_voucher_data';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';
}
