<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Egift extends Model
{
    protected $table = 'crm_voucher_egift';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

//    protected $primaryKey = 'mbr_id';

    /**
     * Update the serial number of Egift.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($egift) {
            do {
                //generate a random string using Laravel's str_random helper
                $token = 'GV'.strtoupper(Str::random(8));
            }
            //check if the token already exists and if it does, try again
            while (Egift::where('coupon_serialno', $token)->first());
            $egift->coupon_serialno = $token;
        });
    }
}
