<?php

namespace App\Models\Member\Voucher;

use Illuminate\Database\Eloquent\Model;

class VoucherRedeem extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_redeem_voucher';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $primaryKey = 'voucher_serialno';

}
