<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Model;

class MemberMeta extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_member_meta';

    protected $guarded = [];

//    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
