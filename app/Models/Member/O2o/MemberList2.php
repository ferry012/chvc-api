<?php

namespace App\Models\Member\O2o;

use Illuminate\Database\Eloquent\Model;

class MemberList2 extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'o2o_member_list_options2';

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;
}
