<?php

namespace App\Models\Member;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RebateClaim extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_rebate_claim';

    protected $guarded = [];

    protected $primaryKey = 'mbr_id';

//    protected $primaryKey = ['mbr_id', 'eff_from', 'eff_to', 'line_num'];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    public function scopeClaimEntitled($query)
    {
        return $query->whereIn('claim_entitled', ['R','Y'])->where('rebate_voucher','');
    }

    /**
     * Update the line number of member type.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($rebateClaim) {
            $next_line = RebateClaim::where('coy_id','CTL')->where('mbr_id', $rebateClaim->mbr_id)->max('line_num');
            if ($next_line === null) {
                $line_num = 1;
            } else {
                $line_num = $next_line + 1;
            }
            $rebateClaim->line_num = $line_num;
        });
    }

}
