<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SysTransList extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'sys_trans_list';

    protected $guarded = [];

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

}
