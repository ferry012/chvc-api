<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LogLogin extends Model
{
    protected $table = 'crm_log_logins';
    protected $fillable = ['mbr_id', 'email_addr', 'ip', 'type', 'desc'];
    public function saveLoginLog($id, $email)
    {
        $request = request();
//        dd($request->user());
        $time = Carbon::now()->toDateTimeString();
        $data = [
            'mbr_id' => $id,
            'email_addr' => $email,
            'ip' => $request->ip(),
            'type' => 'login',
            'desc' => $email.' is login at '.$time,
            'created_at' => $time,
            'updated_at' => $time
        ];
        $this->insert($data);
    }

    public function saveLogoutLog($user)
    {
        //dd($user);
        $request = request();
        $time = Carbon::now()->toDateTimeString();
        $data = [
            'mbr_id' => $user->mbr_id,
            'email_addr' => $user->email_addr,
            'ip' => $request->ip(),
            'type' => 'logout',
            'desc' => $user->email_addr.' is logout at '.$time,
            'created_at' => $time,
            'updated_at' => $time
        ];
        $this->insert($data);
    }
}
