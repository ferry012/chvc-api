<?php

namespace App\Models\Log;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LogWork extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'crm_log_works';

    protected $guarded = [];

    //disable timestamps
    public $timestamps = false;

    public function log($info)
    {
        $request = request();
        $time = Carbon::now()->toDateTimeString();
        $data = [
            'mbr_id' => $info['mbr_id'],
            'trans_id' => isset($info['trans_id']) ? $info['trans_id'] : '',
            'ip' => $request->ip(),
            'type' => $info['type'],
            'desc' => $info['desc'],
            'route_name' => $info['route_name'],
            'created_on' => $time,
            'created_by' => $info['user'],
            'modified_on' => $time,
            'modified_by' => $info['user']
        ];
        $this->insert($data);
    }
}
