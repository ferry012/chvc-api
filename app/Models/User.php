<?php

namespace App\Models;

use App\Models\Member\Points\RebateReserved;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'pgsql';

    protected $table = 'crm_member_list';

    protected $primaryKey = 'mbr_id';

    //disable id increment
    public $incrementing = false;

    //disable timestamps
    public $timestamps = false;

    protected $fillable = [
        'email_addr', 'mbr_pwd', 'mbr_pwd2', 'last_login', 'points_accumulated', 'points_reserved', 'points_redeemed', 'points_expired', 'points_reserved'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mbr_pwd'
    ];

    public function scopeName($query)
    {
        $name = trim(strtolower(request()->input('email_addr')));
        if (isset($name)) {
            return $query = $query->whereRaw('LOWER(email_addr) like ?', '%'.$name.'%');
        } else {
            return $query;
        }
    }

    public function scopeEmail($query)
    {
        $email = trim(strtolower(request()->input('email_addr')));
        if (isset($email)) {
            return $query = $query->whereRaw('LOWER(email_addr) like ?', '%'.$email.'%');
        } else {
            return $query;
        }
    }

    /**
     * Change auth default password to mbr_pwd
     */
    public function getAuthPassword () {
        return $this->mbr_pwd;
    }

    /**
     * Change auth default email to email_addr
     */
    public function getAuthIdentifierName()
    {
        return 'mbr_id';
    }

    /**
     * Change passport default username to email_addr.
     *
     * **** Import Tips ****
     * Must change /chvc-api/vendor/laravel/passport/src/Bridge/UserRepository.php line 53 to:
     * '} elseif (! md5($password) == $user->getAuthPassword()) {'
     *
     */
    public function findForPassport($username) {
        return $this->orWhereRaw('LOWER(mbr_id)=?', trim(strtolower($username)))->orWhereRaw('LOWER(contact_num)=?', trim(strtolower($username)))->orWhereRaw('LOWER(email_addr)=?', trim(strtolower($username)))->first();
    }

    /**
     * Update login data to Member list.
     */
    public function updateLogin($id, $email)
    {
        $time = Carbon::now()->toDateTimeString();
        $this->timestamps = false;
        $this->whereRaw('LOWER(email_addr) = ?', trim(strtolower($email)))->whereRaw('LOWER(mbr_id)=?', trim(strtolower($id)))->update(['last_login' => $time]);
    }

    /**
     * Get the points of member remaining.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public function getPointsRemainingAttribute()
    {
        if ($this->points_accumulated == 0) {
            return 0;
        }
        return $this->points_accumulated - $this->points_reserved - $this->points_redeemed - $this->points_expired - $this->trans_points_reserved;
    }

    /**
     * Get the points of member remaining.
     *
     * @return \Illuminate\Support\Collection|int|mixed|static
     */
    public function getPointsAvailableAttribute()
    {
        if ($this->points_accumulated == 0) {
            return 0;
        }
        return $this->points_accumulated - $this->points_redeemed - $this->points_expired;
    }

    /**
     * Get the points of member reserved.
     *
     * @return mixed
     */
    public function getTransPointsReservedAttribute()
    {
        $reserved_total = RebateReserved::where('mbr_id', rtrim($this->mbr_id, " "))
            ->where('expires', '>', Carbon::now())
            ->where('status', 1)
            ->sum('rebate_amount');
        return $reserved_total*100;
    }
}
