<?php namespace App\Repositories\Member\Contract;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ItemListRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Contract\ItemList';
    }

    public function get_item($item_id)
    {
        return $this->model->where('item_id', $item_id)->first();
    }
}