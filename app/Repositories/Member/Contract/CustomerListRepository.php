<?php namespace App\Repositories\Member\Contract;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CustomerListRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Contract\CustomerList';
    }

    public function check_customer_exists($email, $contact)
    {
        $is_exists = $this->model->where(['email_addr' => $email, 'contact_num' => $contact])->exists();
        if ($is_exists) {
            return $this->model->where(['email_addr' => $email, 'contact_num' => $contact])->first();
        } else {
            return false;
        }
    }

    public function get_cust($mbr_id)
    {
        return $this->model->orWhere('cust_id', $mbr_id)->orWhere('mbr_id', $mbr_id)->orWhere('email_addr', $mbr_id)->orWhere('contact_num', $mbr_id)->first();
    }

    public function check_exist($cust_id)
    {
        return $this->model->where('cust_id', $cust_id)->exists();
    }

    public function get_cust_by_cust($cust_id)
    {
        return $this->model->where('cust_id', $cust_id)->first();
    }
}