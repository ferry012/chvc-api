<?php namespace App\Repositories\Member\Contract;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AgreementTransRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Contract\AgreementTrans';
    }

    public function get_trans($contract_id)
    {
        return $this->model->where('contract_id', $contract_id)->get();
    }

    public function get_max_trans($contract_id, $cycle_num, $max_num)
    {
        $retry_num = $this->model->where(['contract_id'=>$contract_id, 'cycle_num'=>$cycle_num])->max('retry_num');
        if (is_null($retry_num)) {
            return 1;
        } else {
            if ($retry_num + 1 < $max_num) {
                return $retry_num+1;
            } else {
                return false;
            }
        }
    }
}