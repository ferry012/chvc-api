<?php namespace App\Repositories\Member\Contract;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AgreementListRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Contract\AgreementList';
    }

    public function get_list($cust_id)
    {
        return $this->model->where('cust_id', $cust_id)->get();
    }

    public function get_contract($contract_id)
    {
        return $this->model->where('contract_id', $contract_id)->first();
    }

    public function update_pay($contract_id, $cycle_num, $cycle_date, $user)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where('contract_id', $contract_id)->update([
            'cycle_num' => $cycle_num,
            'cycle_date' => $cycle_date,
            'status_level' => 2,
            'modified_by' => $user,
            'modified_on' => $now
        ]);
    }

    public function cancel_contract($contract_id, $status)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where('contract_id', $contract_id)->update([
            'status_level' => $status,
            'modified_by' => 'VC',
            'modified_on' => $now
        ]);
    }

}