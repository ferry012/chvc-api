<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AddressRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Address';
    }

    public function formatAddress($result) {
        $address = trim($result["street_line1"]) . (empty(trim($result["street_line1"])) ? '' : ' ');
        $address = $address . (empty(trim($result["street_line2"])) ? '' : '#') . trim($result["street_line2"]);
        $address = $address . (empty(trim($result["street_line3"])) ? '' : '-') . trim($result["street_line3"]) . (empty(trim($result["street_line3"])) ? '' : ' ');
        $address = $address . trim($result["street_line4"]) . (empty(trim($result["street_line4"])) ? '' : ' ');
        $address = trim($address) . (empty(trim($result["country_id"])) ? '' : ', ') . trim($result["country_id"]) . (empty(trim($result["country_id"])) ? '' : ' ');
        $address = $address . trim($result["postal_code"]);
        return $address;
    }

    public function updateMemberInfo($mbrId, $data)
    {
        $now = Carbon::now();
        if (isset($data['primary_details']) && !empty($data['primary_details'])) {
            $pd = $data['primary_details'];
            $pd_addr_text = $this->formatAddress($pd);
            $result1 = $this->model->where('ref_id',$mbrId)->where([
                'ref_type'  => 'MEMBER',
                'coy_id'    => 'CTL',
                'addr_type' => '0-PRIMARY']);
            if($result1->exists()) {
                $this->model->where('ref_id', $mbrId)->where([
                    'ref_type'  => 'MEMBER',
                    'coy_id'    => 'CTL',
                    'addr_type' => '0-PRIMARY'])->update([
                    'postal_code'  => isset($pd['postal_code']) ? $pd['postal_code'] : '',
                    'street_line1' => isset($pd['street_line1']) ? $pd['street_line1'] : '',
                    'street_line2' => isset($pd['street_line2']) ? $pd['street_line2'] : '',
                    'street_line3' => isset($pd['street_line3']) ? $pd['street_line3'] : '',
                    'street_line4' => isset($pd['street_line4']) ? $pd['street_line4'] : '',
                    'country_id'   => isset($pd['country_id']) ? $pd['country_id'] : '',
                    'city_name'    => isset($pd['city_name']) ? $pd['city_name'] : '',
                    'state_name'   => isset($pd['state_name']) ? $pd['state_name'] : '',
                    'addr_text'    => $pd_addr_text,
                    'updated_on'   => $now,
                ]);
            } else {
                $this->model->create([
                    'ref_type'  => 'MEMBER',
                    'coy_id'    => 'CTL',
                    'addr_type' => '0-PRIMARY',
                    'ref_id'    => $mbrId,
                    'postal_code'=> isset($pd['postal_code']) ? $pd['postal_code'] : '',
                    'street_line1'=> isset($pd['street_line1']) ? $pd['street_line1'] : '',
                    'street_line2'=> isset($pd['street_line2']) ? $pd['street_line2'] : '',
                    'street_line3'=> isset($pd['street_line3']) ? $pd['street_line3'] : '',
                    'street_line4'=> isset($pd['street_line4']) ? $pd['street_line4'] : '',
                    'country_id'=> isset($pd['country_id']) ? $pd['country_id'] : '',
                    'city_name'=> isset($pd['city_name']) ? $pd['city_name'] : '',
                    'state_name'=> isset($pd['state_name']) ? $pd['state_name'] : '',
                    'addr_format'=> isset($pd['addr_format']) ? $pd['addr_format'] : '',
                    'addr_text'=> $pd_addr_text,
                    'created_by'=> isset($pd['created_by']) ? $pd['created_by'] : '',
                    'modified_by'=> isset($pd['modified_by']) ? $pd['modified_by'] : '',
                    'created_on'=> $now,
                    'updated_on'=> $now,
                    'modified_on'=> $now,
                ]);
            }
        }

        if (isset($data['delivery_details']) && !empty($data['delivery_details'])) {
            $dd = $data['delivery_details'];
            $dd_addr_text = $this->formatAddress($dd);
            $result2 = $this->model->where('ref_id',$mbrId)->where([
                'ref_type'  => 'MEMBER',
                'coy_id'    => 'CTL',
                'addr_type' => '2-DELIVERY']);
            if($result2->exists()) {
                $this->model->where('ref_id',$mbrId)->where([
                    'ref_type'  => 'MEMBER',
                    'coy_id'    => 'CTL',
                    'addr_type' => '2-DELIVERY'])->update([
                    'postal_code'  => isset($dd['postal_code']) ? $dd['postal_code'] : '',
                    'street_line1' => isset($dd['street_line1']) ? $dd['street_line1'] : '',
                    'street_line2' => isset($dd['street_line2']) ? $dd['street_line2'] : '',
                    'street_line3' => isset($dd['street_line3']) ? $dd['street_line3'] : '',
                    'street_line4' => isset($dd['street_line4']) ? $dd['street_line4'] : '',
                    'country_id'   => isset($dd['country_id']) ? $dd['country_id'] : '',
                    'city_name'    => isset($dd['city_name']) ? $dd['city_name'] : '',
                    'state_name'   => isset($dd['state_name']) ? $dd['state_name'] : '',
                    'addr_text'    => $dd_addr_text,
                    'updated_on'   => $now,
                ]);
            } else {
                $this->model->create([
                    'ref_type'  => 'MEMBER',
                    'coy_id'    => 'CTL',
                    'addr_type' => '2-DELIVERY',
                    'ref_id'    => $mbrId,
                    'postal_code'=> isset($dd['postal_code']) ? $dd['postal_code'] : '',
                    'street_line1'=> isset($dd['street_line1']) ? $dd['street_line1'] : '',
                    'street_line2'=> isset($dd['street_line2']) ? $dd['street_line2'] : '',
                    'street_line3'=> isset($dd['street_line3']) ? $dd['street_line3'] : '',
                    'street_line4'=> isset($dd['street_line4']) ? $dd['street_line4'] : '',
                    'country_id'=> isset($dd['country_id']) ? $dd['country_id'] : '',
                    'city_name'=> isset($dd['city_name']) ? $dd['city_name'] : '',
                    'state_name'=> isset($dd['state_name']) ? $dd['state_name'] : '',
                    'addr_format'=> isset($dd['addr_format']) ? $dd['addr_format'] : '',
                    'addr_text'=> $pd_addr_text,
                    'created_by'=> isset($dd['created_by']) ? $dd['created_by'] : '',
                    'modified_by'=> isset($dd['modified_by']) ? $dd['modified_by'] : '',
                    'created_on'=> $now,
                    'updated_on'=> $now,
                    'modified_on'=> $now,
                ]);
            }
        }
    }

    public function getAddress($mbrId, $addrType)
    {
        return $this->model->where(['ref_id'=>$mbrId, 'addr_type'=>$addrType])->first();
    }

    public function getCustAddress($custId, $addrType)
    {
        return $this->model->where(['ref_type'=>'CUSTOMER','ref_id'=>$custId, 'addr_type'=>$addrType])->first();
    }

}