<?php namespace App\Repositories\Member\Points;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class PointsExpiryRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Points\PointsExpiry';
    }

    public function getNextExpiryDate()
    {
        $now = Carbon::now();

        $result = $this->model->where(['coy_id' => 'CTL'])->where(function ($query) use ($now) {
            $query->where('eff_from', '<=', $now)->where('eff_to', '>=', $now);
        })->first();

        if ($result) {
            return $result->exp_date;
        } else {
            // if expiry date is not preset, use the assumed date instead
            if ($now->month > 6) {
                $expDate = $now->addYear()->endOfYear()->startOfDay();
            } else {
                $expDate = $now->addYear()->endOfYear()->subMonths(6)->subDay()->startOfDay();
            }
            return $expDate;
        }
    }

    public function getAllExpiryDate()
    {
        $now = Carbon::now();

        $result = $this->model->select('exp_date')->where('coy_id','CTL')->where('exp_date', '>', $now)->distinct('exp_date')
            ->orderBy('exp_date','desc')->pluck('exp_date');
        return $result;
    }

}