<?php namespace App\Repositories\Member\Points;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RebateReservedRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Points\RebateReserved';
    }

    public function is_exists($trans_id)
    {
        return $this->model->where('trans_id', $trans_id)->exists();
    }

    public function updateOrSave($type, $data)
    {
        if ($type === 'update') {
            $res = $this->model->where('trans_id', $data['trans_id'])
                ->update($data);
        } elseif ($type === 'save') {
            $res = $this->model->insert($data);
        }
        return $res;
    }

    public function unreserve($trans_id)
    {
        $is_exists = $this->model->where('trans_id', $trans_id)->exists();
        if ($is_exists) {
            $res = $this->model->where('trans_id', $trans_id)->update([
                'status' => 0,
                'updated_at' => Carbon::now()
            ]);
            return $res;
        }
        return false;
    }

    public function get_rebate_amount($trans_id, $mbr_id)
    {
        return $this->model->where(['trans_id' => $trans_id, 'mbr_id' => $mbr_id, 'status' => 1])
            ->where('expires', '>', Carbon::now())->first();
    }

}