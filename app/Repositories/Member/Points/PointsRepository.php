<?php namespace App\Repositories\Member\Points;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PointsRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Points\Points';
    }

    public function getRecentExpiringPoints($mbrId)
    {
        return $this->model->select("coy_id", DB::raw("rtrim(mbr_id) mbr_id"), "line_num", "exp_date", "points_accumulated", "points_redeemed",
            "points_expired", DB::raw("rtrim(created_by) created_by"), "created_on", DB::raw("rtrim(modified_by) modified_by"), "modified_on",
            "updated_on")->where('coy_id', 'CTL')->where('mbr_id', $mbrId)->where('exp_date', '>=', Carbon::now()->startOfDay())->orderby('exp_date')->first();
    }

    public function summary($mbrId)
    {
        $result = $this->model
            ->selectRaw('SUM(points_accumulated) AS points_accumulated, SUM(points_redeemed) AS points_redeemed, SUM(points_expired) AS points_expired')
            ->where([
                'coy_id' => 'CTL',
                'mbr_id' => $mbrId,
            ])
            ->first();
        $result->points_accumulated = $result->points_accumulated ?? 0;
        $result->points_redeemed    = $result->points_redeemed ?? 0;
        $result->points_expired     = $result->points_expired ?? 0;

        return $result;
    }

//    public function isPointEnough($mbrId, $points)
//    {
//        return 0 < $points || 0 < $this->balancePoints($mbrId);
//    }

    public function balancePoints($mbrId)
    {
        $now    = Carbon::now();
        $result = $this->model->selectRaw('SUM(points_accumulated - points_redeemed - points_expired) as balance')
            ->where('mbr_id', $mbrId)->where('exp_date', '>=', $now->startOfDay())
            ->havingRaw('SUM(points_accumulated - points_redeemed - points_expired) >= 0')
            ->first();

        $balance = $result->balance ?? 0;
        return $balance;
    }

    public function getNextLineNum($mbrId)
    {
        $lineNum = $this->model->where(['coy_id' => 'CTL'])->where('mbr_id', $mbrId)->max('line_num');

        $data = is_null($lineNum) ? 1 : $lineNum + 1;

        return $data;
    }

    public function getQuery($perPage)
    {
        return $this->model->where(['coy_id' => 'CTL'])->simplePaginate($perPage);
    }

    public function showAdmin($mbrId, $lineNum)
    {
        if (isset($lineNum)) {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'line_num' => $lineNum])->get();
        } else {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])->get();
        }

    }

    public function deleteAdmin($mbrId, $lineNum)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'line_num' => $lineNum])->delete();
    }

    public function getRebateDate($mbrId)
    {
        return $this->model->select('exp_date')->where('mbr_id', $mbrId)->orderBy('exp_date', 'DESC')->first();
    }

    public function isPointsRecordExist($mbrId, $exp_date)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'exp_date' => $exp_date])->exists();
    }

    public function clearPoints($mbr_id)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where(['mbr_id'=>$mbr_id,'coy_id'=>'CTL'])->update([
            'points_accumulated'=>0,
            'points_redeemed'=>0,
            'points_expired'=>0,
            'modified_by'=>'MIS',
            'modified_on'=>$now,
            'updated_on'=>$now
        ]);
    }

    public function getExpiredPoints($from, $to)
    {
        return $this->model->where('exp_date','>=',$from)->where('exp_date','<=',$to)->get();
    }

    public function approvalRebateClaim($mbr_id)
    {
        $now = Carbon::now()->toDateTimeString();
        $res = $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id])
            ->where('exp_date','>=',$now)->orderBy('exp_date','asc')->get();
        return $res;
    }

    public function addExpiredPoints($mbr_id, $line_num, $points)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $line_num])
            ->increment('points_expired', $points, ['modified_by' => 'MIS', 'modified_on' => $now]);
    }

    public function getExpDate($mbrId)
    {
        $now = Carbon::now()->toDateTimeString();
        return $this->model->select(DB::raw("exp_date::DATE AS exp_date, SUM((points_accumulated - points_redeemed - points_expired)) AS points_available"))
            ->where('mbr_id', $mbrId)->where('exp_date', '>', $now)
            ->whereRaw("(points_accumulated - points_redeemed - points_expired) > 0")
            ->groupBy(DB::raw('CAST(exp_date AS DATE)'))
            ->orderBy('exp_date', 'ASC')->get()->toArray();
    }

    public function getPoints($mbrId, $exp_date)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'exp_date' => $exp_date]);
    }

}