<?php namespace App\Repositories\Member\Points;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PointsSummaryRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Points\PointsSummary';
    }

    public function getYear()
    {
        $data = $this->model->select(DB::raw("datepart(yyyy, date_start) year"))->groupBy(DB::raw("datepart(yyyy, date_start)"))->orderBy('year','desc')->get()->pluck('year', 'year');
        return $data;
    }

    public function checkExists($from, $to)
    {
        return $this->model->where(['date_start'=>$from, 'closing_date'=>$to])->exists();
    }

}