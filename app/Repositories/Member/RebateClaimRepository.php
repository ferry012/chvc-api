<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RebateClaimRepository extends Repository {

    public function model() {
        return 'App\Models\Member\RebateClaim';
    }

    public function getRebateClaim($from, $to)
    {
        $res = $this->model->where('last_expiry', '>=', $from)
            ->where('last_expiry', '<', $to)->get();
        return $res;
    }

    public function updateRebateClaimRenewed($mbr_id, $line_num, $mbr_savings, $claim_entitled)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where('mbr_id',$mbr_id)
            ->where('line_num', $line_num)
            ->update([
                'claim_entitled'=>$claim_entitled,
                'mbr_savings'=>$mbr_savings,
                'modified_by'=>'MIS',
                'modified_on'=>$now,
                'updated_on'=>$now
            ]);
    }

    public function needClaim($from, $to)
    {
        $this->model->where('modified_on', '>=', $from)->where('modified_on', '<', $to)
            ->where('claim_entitled', 'R')
            ->where("rebate_voucher", '<>', '')
            ->update(['claim_entitled'=>'A']);
        return $this->model->where('modified_on', '>=', $from)->where('modified_on', '<', $to)
            ->where('claim_entitled', 'R')
            ->where("rebate_voucher", '')
            ->get();
    }

    public function approvalRebateClaim($mbr_id, $line_num)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where([
            'mbr_id'=>$mbr_id,
            'line_num'=>$line_num
        ])->update([
            'claim_entitled'=>'A',
            'modified_by'=>'MIS',
            'modified_on'=>$now,
            'updated_on'=>$now
        ]);
    }

    public function isExists($mbr_id, $exp_date)
    {
        $is_exists = $this->model->where('mbr_id', $mbr_id)->where('last_expiry', $exp_date)->exists();
        return $is_exists;
    }

}