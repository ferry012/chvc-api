<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class VcMemberRepository extends Repository
{
    private $db20;
    private $mssqldb;
    private $chapps;
    private $crm_member_list;
    private $CTL_COY_ID = 'CTL';
    private $overrideRedis = FALSE;  // FALSE in production;
    private $listChappsKey = ['#notificationInbox#'];

    private $ENVIRONMENT = "";
    private $server_name;

    private $oneMonth = 2630000;
    private $threeDays = 259200;
    const MEMBER = 'M';
    const MEMBER_08 = 'M08';
    const MEMBER_18 = 'M18';
    const MEMBER_28 = 'M28';
    const MEMBER_STUDENT = 'MST';
    const MEMBER_STUDENT_PLUS = 'MSTP';
    const MEMBER_NS = 'MNS';
    const MEMBER_FC = 'MFC';
    const MEMBER_TYPES = [
        self::MEMBER, self::MEMBER_08, self::MEMBER_18, self::MEMBER_28,
    ];
    const MEMBER_TYPE_MONTHS = [
        self::MEMBER => 24, self::MEMBER_08 => 8, self::MEMBER_18 => 18,
        self::MEMBER_28 => 28, self::MEMBER_STUDENT => 3, self::MEMBER_NS => 24,
        self::MEMBER_FC => 24, self::MEMBER_STUDENT_PLUS => 12
    ];
    private $_redis_prefix = "ValueClub30#";
    private $_login_token_prefix = "VC_Token30";
    private $_redis_expiry_time = 259200;
    private $_comp_lists = "_HpCompLists";

    private $ims_authorization = 'Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ';
    private $ims_url = "";

    private $HACHI_OVERRIDE_PASS = '';

    private $associate_mbr_limit = 3;
    private $appVersion = ["2.5.5", "2.5.6", "2.5.7", "2.5.8", "2.5.9", "2.7.53", "2.7.54", "2.7.55", "2.8.27"];
    private $message = array(
        "OLDER_VERSION_APP" => array(
            "title" => "A newer version of this application is available. Would you like to update now?",
            "message" => "A newer version of this application is available. Would you like to update now?"
        ),
        "KEEP_LOGIN_SECURE" => array(
            "status" => 1,
            "message" => "Safeguard your rebates/redemptions/transfers and donations.<br/>Keep your log-in details secure."
        ),
        "SIGNUP_INCOMPLETE" => array(
            "title" => "Sign up Info",
            "message" => "Field required"
        ),
    );
    private $member_type_list = array(
        "STUDENT" => 'MST',
        "NSMEN" => 'MNS',
        "ASSOCIATE" => 'MAS',
        "B2B" => "MB2B"
    );


    private $apiUrl = "";

    public function __construct()
    {

        $prod_mode = env("APP_ENV");

        if (strpos($_SERVER['REQUEST_URI'], 'mode=prod') !== false) {
            $prod_mode = 'production';
        }
        //Test
//        $prod_mode = "production";


        $this->db20 = DB::connection('pgsql');
        $this->mssqldb = DB::connection('sqlsrv');
        $this->chapps = DB::connection('vcapps');

        $this->HACHI_OVERRIDE_PASS = '@hachi!#@' . date('Ymd');

        if (($prod_mode == 'production')) {
            $this->ENVIRONMENT = "production";
//            $this->server_name = "chstaff.challenger.sg";
            $this->server_name = "vc.api.valueclub.asia";
            $this->ims_url = "https://ims.api.valueclub.asia/api/";

            //For Testing.
//            $this->ims_url = "http://chims-test.sghachi.com/api/";

        } else if ($prod_mode == 'local') {

            $this->ENVIRONMENT = "development";
            $this->server_name = "chvc-api.sghachi.com";
            $this->ims_url = "http://chims-test.sghachi.com/api/";

        } else {

            $this->ENVIRONMENT = "development";
            $this->server_name = "chvc-api.sghachi.com";
            $this->ims_url = "http://chims-test.sghachi.com/api/";

        }

    }

    public function model()
    {
        return 'App\Models\User';
    }

    public function testDb()
    {
        $lists = $this->db20
            ->table('o2o_push_notify')
            ->get();

        return $lists;
    }

    public function login(&$params)
    {

        $table = 'crm_member_list';

        $user = (isset($params["user"])) ? $params["user"] : "";
        $password = (isset($params["password"])) ? $params["password"] : "";

        if (empty($user) || empty($password)) {
            return -4;
        }
        $return = $this->_loginquery($table, $user);
        $result = $return ? $return["data"] : false;

        if ($result === -1) {
            $params["duplicateUser"] = 1;
            return -5;
        }
        if (empty($result)) {
            $params["invalidUser"] = 1;
            return FALSE;
        }

        if ($result->mbr_type == '' || ($result->mbr_type == 'MAS' && $result->status_level == -1)) {
            return -6;
        }
        if ($result->login_locked == 'Y') {
            $params["invalidUser"] = 1;
            return -1;
        } else if ($result->mbr_type == 'X') {
            $params["suspendedUser"] = 1;
            return -2;
        } else if ($result->status_level == 0) {
            $params["invalidUser"] = 1;
            return -3;
        }

        if ($password !== NULL) {

            $hashedPassword = $result->mbr_pwd;
            if (!$this->_passwordCheck($password, $hashedPassword)) {
                $params["invalidPassword"] = 1;
                return FALSE;
            }

            $session_token = md5($this->GUID());
            $is_redis = 'LARAVEL';

            if (isset($result->contact_num) && ($password == substr(trim($result->contact_num), -4))) {
                $params["is_mobile_digit_pwd"] = 1;
            }

            $has_device = $this->saveSessionToken($session_token, trim($result->mbr_id));
            if ($has_device) {
                $params["hasOldDevice"] = 1;
            }

            $setdate = new Carbon();
            $setdate->modify('+10 day');
            $expiry_time = date("Y-m-d H:i:s", strtotime($setdate->format('Y-m-d H:i:s')));

            $return = [
                'email_addr' => $result->email_addr,
                'expiry_date' => $expiry_time,
                'first_name' => $result->first_name,
                'session_token' => $session_token,
                "is_redis" => $is_redis,
                'last_name' => $result->last_name,
                'mbr_id' => trim($result->mbr_id),
                'cart_id' => trim($result->mbr_id),
                'user_type' => trim($result->mbr_type),
                'member' => trim($result->mbr_type) === 'M',
                'redis_key' => trim($this->server_name . $this->_redis_prefix . $result->mbr_id),
                'mbr_token' => $result->mbr_token2,
                "is_popup" => true, // Show message for popup_message after login
                "popup_message" => $this->message["KEEP_LOGIN_SECURE"]["message"],
                'override_pass' => ($password === $this->HACHI_OVERRIDE_PASS) ? true : false
            ];
            return $return;
        }
    }

    public function stdToArray($object)
    {
        $data = json_decode(json_encode($object), true);
        return $data;
    }

    public function saveSessionToken($token, $mbr_id)
    {
        $key = $this->server_name . $this->_login_token_prefix . $mbr_id; //. $key;

//        echo $key;

        $hasOldDevice = false;
        $session = Redis::get($key);

//        print_r($session);

        $session_token = ($session) ? unserialize($session) : '';

//        echo $session_token;

        if ($token !== $session_token && $session_token !== '') {
            $hasOldDevice = true;
        }
        Redis::set($key, serialize($token));
        Redis::expire($key, $this->_redis_expiry_time);

        return $hasOldDevice;

    }

    public function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    protected function _passwordCheck($password, $hashed)
    {
        $server_name = $this->server_name;
        $password2 = bin2hex(openssl_digest(trim($password), 'sha256',true));
        if (strpos($server_name, 'beta') !== false) {
            return $password == $this->HACHI_OVERRIDE_PASS;
        } else {
            return $password2 == trim($hashed) || md5($password) == trim($hashed) || $password == $this->HACHI_OVERRIDE_PASS;
        }
    }

    protected function _loginquery($table, $user)
    {
        $coy_id = $this->CTL_COY_ID;
        $sql = "SELECT 
                coy_id,mbr_id, mbr_title, first_name, last_name, birth_date, nationality_id, email_addr, contact_num, mbr_pwd,
                status_level, join_date, exp_date, last_renewal, points_accumulated, points_reserved, points_redeemed, points_expired,
                RTRIM(mbr_type) as mbr_type , 
                RTRIM(main_id) as main_id, 
                salesperson_id, 
                RTRIM(login_locked) as login_locked
                , mbr_token as mbr_token2
                FROM crm_member_list
                WHERE coy_id = ? AND (LOWER(email_addr) = ? or RTRIM(contact_num) = ? or RTRIM(mbr_id) = ?)";

        $row_ctl = $this->db20->select($sql, [$coy_id, strtolower($user), $user, $user]);
        if (count($row_ctl) > 1) {
            return [
                "data" => -1,
                "coy" => 'CTL'
            ];
        } else if (count($row_ctl) == 1) {
            return [
                "data" => $row_ctl[0],
                "coy" => 'CTL'
            ];
        }
        return false;
    }

    public function getNotificationInbox($mbr_id)
    {
        $redisKey = "#notificationInbox#";

        $sql = <<<str
    select a.* from (
         select notify.id,notify.datetime,

        notify.type,notify.title,notify.message,notify.require_login,
notify.feature,notify.url, notify.pushtype,notify.updated_on as push_date,? as mbr_id,
coalesce(lg.platform,'') as platform,coalesce(lg.status,0) as status ,
notify.long_message, notify.image_url,
           CASE WHEN coalesce(notify.link_text,'')=''
           THEN 'Ok' ELSE notify.link_text END as link_text,
                coalesce(notify.link_url,'') as link_url
from o2o_push_notify notify
 left join o2o_push_log lg on lg.id = ( select  id from o2o_push_log as toplg
 where toplg.batch_id=notify.id
   and toplg.mbr_id= ? order by toplg.created_on desc limit 1)
where
notify.status= 1 and coalesce(lg.status,0) >= -1 and notify.datetime <= current_timestamp and (pushtype='public' or
(
(pushtype='private' and notify.mbr_id = ? ))
)
order by notify.datetime desc limit 20
) as a where a.status > -1;
str;


        $result = $this->IsRedisOrDb($sql, [$mbr_id, $mbr_id ,$mbr_id], $redisKey, 900, $mbr_id, false);
        return $result;

//        $res = $this->db20->select($sql, ["mbr_id" => $mbr_id]);
//        return $res;

    }

    public function IsRedisOrDb($sql, $param_array, $key, $expire_seconds, $mbr_id, $overRideRedis = false)
    {

        if (in_array($key, $this->listChappsKey)) {

            $lists = $this->getNotificationChapps($sql, $param_array, $key, $expire_seconds, $mbr_id, $overRideRedis);
            return $lists;
        } else {
//            echo "this";

            $lists = $this->db20->select($sql, $param_array);

            return $lists;

        }
    }

    public function getNotificationChapps($sql, $param_array, $key, $expire_seconds = 900, $mbr_id = '', $overRideRedis = false)
    {
        if (!$overRideRedis) {
            $lists = $this->getTableMeta('Members_datatype', $mbr_id, $key);
            if (count($lists) > 0) {
//                echo "here";
//                print_r($lists);

                $data = json_decode($lists[0]->meta, true);
                if ($data) {
                    return $data;
                } else {
                    $isPushTime = $this->checkIfBlockSql();
                    if ($isPushTime) {
                        return array();
                    }

                    $data = $this->db20->select($sql, $param_array);
                    $items = array();
                    foreach ($data as $index => $list) {
                        $item = $this->stdToArray($list);
                        $items[] = $item;
                    }

                    $this->setTableMeta($mbr_id, $items, $key);
                    return $items;
                }
            } else {
                $isPushTime = $this->checkIfBlockSql();
                if ($isPushTime) {
                    return array();
                }

                $data = $this->db20->select($sql, $param_array);
                $items = array();
                foreach ($data as $index => $list) {
                    $item = $this->stdToArray($list);
                    $items[] = $item;
                }

                $this->setTableMeta($mbr_id, $items, $key);
                return $items;
            }
        } else {
            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
            $cachedata = Redis::hget($hkey, $key);
            if (!$cachedata || $this->overrideRedis) {

                $data = $this->db20->select($sql, $param_array);
                if (Redis::hset($hkey, $key, serialize($data)) == FALSE) {
                    return false;
                }
                Redis::expire($hkey, $expire_seconds);
                $this->setTableMeta($mbr_id, $data, $key);
                return $data;
            } else {
                $this->setTableMeta($mbr_id, unserialize($cachedata), $key);
                return unserialize($cachedata);
            }
        }
    }

    private function checkIfBlockSql()
    {
        $currentTime = date("Y-m-d H:i:s");
        $checkRedisKey = $this->ENVIRONMENT . "#Vc_pushNotify_time#";
        $checkPushTimes = Redis::hgetall($checkRedisKey);
//		print_r($checkPushTimes);
        $isTime = false;
        foreach ($checkPushTimes as $thetime) {
            $onehourLater = date("Y-m-d H:i:s", strtotime('+5 minutes', strtotime($thetime)));
            if ($currentTime > $thetime && $currentTime <= $onehourLater) {
                $isTime = true;
                break;
            }
        }
        return $isTime;
    }

    public function getTableMeta($table, $mbr_id = '', $type = '')
    {
        $mbrdata = '';
        if ($mbr_id) {
            if ($type) {
                $mbrdata = <<<str
     where mbr_id= '$mbr_id' and keytype= '$type'
str;
            } else {
                $mbrdata = <<<str
     where mbr_id= '$mbr_id'
str;
            }

            $query = <<<str
    SELECT * FROM "$table" $mbrdata
str;
            $result = $this->chapps->select($query);

            return $result;

        } else {
            return array();
        }
    }

    public function setTableMeta($mbr_id, $meta, $type = '', $table = '')
    {

        $this->chapps->beginTransaction();
        if ($type && $mbr_id) {
            $table = 'Members_datatype';
            $query = <<<str
        INSERT INTO "$table" (mbr_id, meta , keytype) 
        VALUES (?, ? ,?)
        ON CONFLICT (mbr_id,keytype) DO UPDATE 
          SET meta = ? 
str;
//            echo $query;
            $insert_res = $this->chapps->statement($query, [$mbr_id, json_encode($meta), $type, json_encode($meta)]);
//            if (!$insert_res || $this->chapps->trans_status() === FALSE) {
//                $this->chapps->trans_rollback();
//                return false;
//            }
        }

        $this->chapps->commit();
        return true;
    }


    //Below are Old Functions.

    public function getPointExpiringLists($mbr_id)
    {
        $sql = <<<str
select cast(exp_date as date) exp_date, 
    cast(SUM(coalesce(points_accumulated,0))-SUM(coalesce(points_redeemed,0))-SUM(coalesce(points_expired,0)) as int) points_bal 
from crm_member_points 
where coy_id='CTL' and mbr_id= :mbr_id  and cast(exp_date as date) >= cast(current_timestamp as date) 
GROUP BY cast(exp_date as date)
HAVING SUM(coalesce(points_accumulated,0))-SUM(coalesce(points_redeemed,0))-SUM(coalesce(points_expired,0)) > 0
ORDER BY exp_date
str;
        $res = $this->db20->select($sql, ["mbr_id" => $mbr_id]);
//        print_r($res);

        return $res;
    }

    public function verifyCurPass($email_addr, $cur_pass)
    {
        $coy_id = 'CTL';

        $member = $this->db20
            ->table('crm_member_list')
            ->select('mbr_pwd')
            ->whereRaw("LOWER(email_addr)= '$email_addr'")
            ->where([
                'coy_id' => $coy_id
            ])->first();

        $cur_pass_md5 = md5($cur_pass);
        if(!$member){
            return false;
        }

        if ($cur_pass_md5 == $member->mbr_pwd) {
            return true;
        }
        return false;
    } 

    public function updateNewPassCtl($email, $new_pass)
    {
        $coy_id = 'CTL';

//        print_r($email);
//        print_r($new_pass);

        $hashed_password = md5(trim($new_pass));
        $hashed_pwd2 = bin2hex(openssl_digest(trim($new_pass), 'sha256',true));
        $this->db20
            ->table('crm_member_list')
            ->where('coy_id', $coy_id)
            ->whereRaw("LOWER(email_addr)= '$email'")
            ->update([
                'mbr_pwd' => $hashed_password,
                'mbr_pwd2' => $hashed_pwd2,
                'login_locked' => 'N',
                'pwd_changed' => Carbon::now(),
            ]);

        return true;
    }

    public function updateNewPass($email_addr, $mbr_id, $new_pass)
    {
        $coy_id = 'CTL';

//        $hashed_password = md5(trim($new_pass));
        $hashed_password = md5(trim($new_pass));
        $hashed_pwd2 = bin2hex(openssl_digest(trim($new_pass), 'sha256',true));
        $this->db20
            ->table('crm_member_list')
            ->where([
                'mbr_id' => $mbr_id,
                'coy_id' => $coy_id
            ])
            ->update([
                'mbr_pwd' => $hashed_password,
                'mbr_pwd2' => $hashed_pwd2,
                'login_locked' => 'N',
                'email_addr' => $email_addr,
                'pwd_changed' => Carbon::now(),
            ]);

        return true;
    }


    public function getAssociateMbrList($mbr_id, $status_level = false)
    {
        $onlyActive = $status_level ? ' and status_level=1' : '';
        $sql = <<<str
    select 
	  email_addr, mbr_id, concat(first_name , ' ' , last_name) as full_name,
	  status_level as status
    from crm_member_list where main_id= :mbr_id  $onlyActive
    order by status_level desc, join_date
str;
//        echo $sql;
        $res = $this->db20->select($sql, ["mbr_id" => $mbr_id]);
//        print_r($res);
        return $res;

    }

    public function getProductReviewItem($mbr_id, $item_id, $trans_id)
    {
        $sql = <<<str
    select * FROM "Product_review" WHERE mbr_id= ? and item_id = ? and trans_id = ?
str;
        $validate = DB::connection('vcapps')->select($sql, [$mbr_id, $item_id, $trans_id]);
        return $validate;
    }

    public function getMemberById($mbr_id)
    {
        $user = $this->db20->table('crm_member_list')
            ->select('mbr_id', 'email_addr', 'last_name', 'first_name', 'contact_num', 'exp_date')
            ->where([
                'mbr_id' => $mbr_id,
                'coy_id' => 'CTL'
            ])->first();

        return $user;
    }

    public function updateProductReviewStat($status, $star_count, $like_count, $dislike_count, $mbr_id, $item_id, $trans_id)
    {
        DB::connection('vcapps')
            ->table('Product_review')
            ->where([
                'mbr_id' => $mbr_id,
                'item_id' => $item_id,
                'trans_id' => $trans_id
            ])->update([
                'status_level' => $status,
                'star_count' => $star_count,
                'like_count' => $like_count,
                'dislike_count' => $dislike_count,
                'modified_on' => Carbon::now(),
                'modified_by' => $mbr_id
            ]);

        return true;
    }

    public function updateProductReviewFinish($mbr_id, $item_id, $trans_id)
    {
        DB::connection('vcapps')
            ->table('Product_review')
            ->where([
                'mbr_id' => $mbr_id,
                'item_id' => $item_id,
                'trans_id' => $trans_id
            ])->update([
                'status_level' => 2,
                'modified_on' => Carbon::now(),
                'modified_by' => $mbr_id
            ]);

        return true;
    }

    public function getReceiverUser($mbr_id)
    {
        $coy_id = 'CTL';

        $list = $this->db20
            ->table('crm_member_list')->select('first_name', 'mbr_id', 'email_addr',
                DB::raw('(points_accumulated - points_redeemed - points_reserved - points_expired) as point_avail'))
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id
            ])->first();

        if ($list) {
            return $list;
        }
        return false;
    }

    public function transferPointsToVCMember($mbr_id, $transfer_id, $points)
    {
        $coy_id = $this->CTL_COY_ID;

        $mbr_member = $this->db20->select(
            "SELECT 1 FROM crm_member_list WHERE coy_id = '$coy_id' AND mbr_id = ? AND status_level = 1"
            , [$mbr_id]);

        $transfer_member = $this->db20->select(
            "SELECT 1 FROM crm_member_list WHERE coy_id = '$coy_id' AND mbr_id = ? AND status_level = 1"
            , [$transfer_id]);

        if (!$mbr_member || !$transfer_member) {
            $return = array(
                "status" => 0,
                "msg" => "Recipient's account is inactive, please check with Customer Service."
            );
            return $return;
        }

//        if (count($transfer_member) > 1 && $transfer_member[0]->main_id != '') {
//            $return = array(
//                "status" => 0,
//                "msg" => "Unable to transfer point to Associate member."
//            );
//            return $return;
//        }

        $sql_check_point = <<<str
SELECT coalesce(SUM(points_accumulated - points_redeemed - points_expired),0) as point
FROM crm_member_points
WHERE coy_id = 'CTL' AND mbr_id = ? AND current_timestamp < exp_date
str;
        $point_list = $this->db20->select($sql_check_point, [$mbr_id]);
//        echo $point_list[0]->point/100 . "<br/>";
//        echo $points;
        if (count($point_list) === 0 || $points > $point_list[0]->point) {
            $return = array(
                "status" => 0,
                "msg" => "Insufficient rebates to transfer"
            );
            return $return;
        }

        //Get Trans ID
        $yearMonth = date("Ym");
        $trans_prefix = 'HO_TYPETP' . $yearMonth;

        $inv_id_header = 'TP' . $yearMonth;

        $tp_trans_id = $this->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);


        //Deduct.
        $func = "points/deduct";
        $post_data = array(
            "mbr_id" => $mbr_id,
            "trans_id" => $tp_trans_id,
            "rebate_amount" => -$points / 100,
            "rebate_description" => "Points transfer to ValueClub Member: " . $transfer_id
        );
        $res = $this->curlCHPos($func, $post_data);
        $this->_logprocess('REBATE_SEND_D', $points, json_encode($res), $mbr_id, $tp_trans_id);


        $tp_trans_id2 = $this->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);

        //Add Reward.
        $func = "points/award";
        $post_data = array(
            "mbr_id" => $transfer_id,
            "trans_id" => $tp_trans_id2,
            "rebate_amount" => $points / 100,
            "rebate_description" => "Points Received From ValueClub Member: " . $mbr_id,
            "transfer" => 1
        );
        $res2 = $this->curlCHPos($func, $post_data);
        $this->_logprocess('REBATE_SEND_A', $points, json_encode($res2), $transfer_id, $tp_trans_id2);

        $return = array(
            "status" => 1,
            "msg" => "Success."
        );
        return $return;
    }

    private function _logprocess($process_id, $status, $remarks, $mbr_id, $created_by = 'VcApp')
    {
        $coy_id = $this->CTL_COY_ID;

        $after_setting = array(
            'coy_id' => $coy_id,
            "process_id" => substr($process_id, 0, 30),
            "status" => substr($status, 0, 3),
            "remarks" => $remarks,
            "created_by" => substr($created_by, 0, 15),
            "mbr_id" => substr($mbr_id, 0, 20),
            "created_on" => Carbon::now()
        );

        $this->db20->table('o2o_log_live_processes')
            ->insert($after_setting);
    }

    protected function _logNewprocess($process_id, $trans_id, $invoice_id, $status, $remarks, $mbr_id, $email_addr, $created_by = 'VcApp', $modified_on = null)
    {
        $coy_id = $this->CTL_COY_ID;

        $after_setting = array(
            'coy_id' => $coy_id,
            "process_id" => substr($process_id, 0, 30),
            "trans_id" => $trans_id,
            "invoice_id" => $invoice_id,
            "status" => substr($status, 0, 3),
            "remarks" => substr($remarks, 0, 200),
            "mbr_id" => substr($mbr_id, 0, 20),
            "created_by" => substr($created_by, 0, 15),
            "created_on" => Carbon::now(),
            "modified_on" => $modified_on,
            "email_addr" => $email_addr
        );

        $this->db20->table('o2o_log_live_processes_new')
            ->insert($after_setting);
    }

    public function getLatestPushId()
    {
        $id = 'latest_push_id';

        $sql = <<<str
    select * from "Other_meta" where meta= ?
str;
        $data = $this->chapps->select($sql, [$id]);
        if (count($data) > 0) {
            return $data[0]->value;
        }
        return 0;
    }

    public function updateMetaTableStatus($params, $keytype, $isReplace = false)
    {
//        print_r($params);
        $mbr_id = $params["mbr_id"];
        $batch_id = $params["batch_id"];
        $status = $params["status"];

        if ($mbr_id && $keytype) {
            $lists = $this->getTableMeta('Members_datatype', $mbr_id, $keytype);
            if (count($lists) === 0) {
                return true;
            }

            $datas = json_decode($lists[0]->meta, true);

            $isFound = false;
            foreach ($datas as $index => $list) {
                if ($list["id"] == $batch_id) {
                    $isFound = true;
                    $datas[$index]["status"] = $status;
                    break;
                }
            }

            if ($isFound || $isReplace) {
                $this->setTableMeta($mbr_id, $datas, $keytype);
            } else {
                $this->deleteTableMeta($mbr_id, $keytype);
            }
        }
        return true;
    }

    //No More Using Soon.
    public function insertOrUpdatePushLogs($mbr_id, $get_id, $platform, $pushid, $status)
    {
        $key = $mbr_id . $platform . $get_id;
        $created_by = 'VcApp';

        $sql = <<<str
do $$
    begin
        IF not EXISTS(SELECT id FROM o2o_push_log WHERE pushid= '$pushid' and mbr_id= '$mbr_id' and batch_id= '$get_id')
          THEN
              INSERT INTO o2o_push_log (pushid, mbr_id,batch_id, platform, date, remarks, status, created_on, created_by)
              VALUES ('$pushid','$mbr_id','$get_id','$platform',current_timestamp,'$key','$status',current_timestamp,'$created_by');
        ELSE
           update o2o_push_log set status= '$status' , updated_on= current_timestamp ,
           updated_by='Batch' where pushid= '$pushid' and mbr_id= '$mbr_id' and batch_id= '$get_id' ;
        end if;   
    end      
$$;
str;
        $lists = $this->db20->statement($sql);

        return $lists;
    }


    public function getPushStatusQuery($list)
    {
        $params = array();

        $mbr_id = $list["mbr_id"];
        $platform = $list["platform"];
        $get_id = $list["batch_id"];
        $pushid = $list["pushId"];
        $status = $list["status"];

        $key = $mbr_id . $platform . $get_id;
        $created_by = 'VcApp';

        $sql = <<<str
do $$
    begin
        IF not EXISTS(SELECT id FROM o2o_push_log WHERE pushid= '$pushid' and mbr_id= '$mbr_id' and batch_id= '$get_id')
          THEN
              INSERT INTO o2o_push_log (pushid, mbr_id,batch_id, platform, date, remarks, status, created_on, created_by)
              VALUES ('$pushid','$mbr_id','$get_id','$platform',current_timestamp,'$key','$status',current_timestamp,'$created_by');
        ELSE
           update o2o_push_log set status= '$status' , updated_on= current_timestamp ,
           updated_by='Batch' where pushid= '$pushid' and mbr_id= '$mbr_id' and batch_id= '$get_id' ;
        end if;   
    end      
$$;
str;

        $params["sql"] = $sql;

        $params["status"] = $list["status"];

        $params["params"] = [];

        return $params;
    }

    public function deleteTableMeta($mbr_id = '', $type = '')
    {
        if ($mbr_id) {
            $query = <<<str
    DELETE FROM "Members_datatype" where mbr_id= ? and keytype= ?
str;
            $this->chapps->statement($query, [$mbr_id, $type]);
            return true;
        }

        return;
    }

    public function donateRebates($mbr_id, $transfer_id, $points)
    {
        $coy_id = $this->CTL_COY_ID;

        $mbr_member = $this->db20->select(
            "SELECT 1 FROM crm_member_list WHERE coy_id = '$coy_id' AND mbr_id = ? AND status_level = 1"
            , [$mbr_id]);

        $transfer_member = $this->db20->select(
            "SELECT 1 FROM crm_member_list WHERE coy_id = '$coy_id' AND mbr_id = ? AND status_level = 1"
            , [$transfer_id]);

        if (!$mbr_member || !$transfer_member) {
            $return = array(
                "status" => 0,
                "msg" => "Recipient's account is inactive, please check with Customer Service."
            );
            return $return;
        }

        $sql_check_point = <<<str
SELECT coalesce(SUM(points_accumulated - points_redeemed - points_expired),0) as point
FROM crm_member_points
WHERE coy_id = 'CTL' AND mbr_id = ? AND current_timestamp < exp_date
str;
        $point_list = $this->db20->select($sql_check_point, [$mbr_id]);
//        echo $point_list[0]->point/100 . "<br/>";
//        echo $points;
        if (!$point_list || $points > $point_list[0]->point) {
            $return = array(
                "status" => 0,
                "msg" => "Insufficient rebates to transfer"
            );
            return $return;
        }

        //Get Trans ID
        $yearMonth = date("Ym");
        $trans_prefix = 'HO_TYPETP' . $yearMonth;

        $inv_id_header = 'TP' . $yearMonth;

        $tp_trans_id = $this->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);

        //Deduct.
        $func = "points/deduct";
        $post_data = array(
            "mbr_id" => $mbr_id,
            "trans_id" => $tp_trans_id,
            "rebate_amount" => -$points / 100,
            "rebate_description" => "Donation to Singapore Children 's Society."
        );
        $res = $this->curlCHPos($func, $post_data);
        $this->_logprocess('DONATE_REBATE', $points, json_encode($res), $mbr_id, $tp_trans_id);


        $tp_trans_id2 = $this->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);
        //Add Reward.
        $func = "points/award";
        $post_data = array(
            "mbr_id" => $transfer_id,
            "trans_id" => $tp_trans_id2,
            "rebate_amount" => $points / 100,
            "rebate_description" => "Donation to Singapore Children 's Society. ->" . (string)date("Y-m-d")
        );
        $res2 = $this->curlCHPos($func, $post_data);
        $this->_logprocess('DONATE_REBATE_2', $points, json_encode($res2), $mbr_id, $tp_trans_id);


        //Insert TP Log for Donators.
        $sql_upd = <<<str
update crm_member_transaction set trans_type='TP',regular_price=1, item_qty=1 where trans_id in (?, ?)
str;
        $this->db20->statement($sql_upd, [$tp_trans_id2, $tp_trans_id]);

//        $item_desc = "Donation to Singapore Children Society ->" . (string)date("Y-m-d");
//        $sql_ins = <<<str
//		INSERT INTO crm_member_transaction
//				(coy_id, mbr_id, trans_id, line_num, trans_type, trans_date, loc_id, pos_id, item_id , item_desc  ,item_qty, regular_price, unit_price,
//					disc_percent, disc_amount, trans_points, salesperson_id, mbr_savings, created_by, created_on, modified_by, modified_on, updated_on)
//	VALUES ('CTL' , '$transfer_id', '$tp_trans_id2' , 1 , 'TP', current_timestamp , '', '', '', '$item_desc', 1,  1, $points/100,
//		0, 0 , $points, '',0, 'VcApp' , NOW(), left('$transfer_id', 15) , NOW(), NOW());
//
//str;
//        $this->db20->statement($sql_ins);


        $return = array(
            "status" => 1,
            "msg" => "Success."
        );
        return $return;
    }

    private function curlCHPos($func, $data)
    {
        $url = "https://chvc-api.sghachi.com/";
        $keycode = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
        if ($this->ENVIRONMENT == 'production') {
            $url = "https://vc.api.valueclub.asia/";
            $keycode = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
        }
        $url = $url . "api/" . $func . "?key_code=" . $keycode;
        $headers = array(
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);

        return $result;
    }

    public function getProductReview($mbr_id, $invoice_id = '', $item_id = '')
    {
        if ($invoice_id && $item_id) {
            $sql = <<<str
    select * from "Product_review" where mbr_id= ? and item_id= ? and trans_id= ? limit 1
str;
            $item = $this->chapps->select($sql, [$mbr_id, $item_id, $invoice_id]);
//            print_r($this->chapps->last_query());
            if ($item) {
                return $item[0];
            }
            return array();
        } else {
            $sql = <<<str
    select * from "Product_review" where mbr_id= ?  order by created_on desc
str;
            $lists = $this->chapps->select($sql, [$mbr_id]);
            if ($lists) {
                return $lists;
            }
            return array();
        }
    }

    public function getProductReviewByMbrId($mbr_id)
    {
//        $result = [];

//        $query= <<<str
//select * from crm_member_list where mbr_id= :mbr_id
//str;

//            ->table('"Members"')
//            ->where(['mbr_id' => $mbr_id])->first();

        $sql = <<<str
SELECT RTRIM(item_id) as item_id, RTRIM(trans_id) as trans_id,RTRIM(mbr_id) as mbr_id, item_desc,img_name,
COALESCE(star_count,0) as star_count, COALESCE(like_count,0) as like_count , COALESCE(dislike_count,0) as dislike_count  ,created_on, modified_on, COALESCE(status_level,0) as status_level
FROM "Product_review" WHERE mbr_id= :mbr_id
order by status_level, modified_on desc
str;
        $result = DB::connection('vcapps')->select($sql, ['mbr_id' => $mbr_id]);


//        foreach ($result as $index => $item) {
//            $result[$index]["star_count"] = (int)$result[$index]["star_count"];
//            $result[$index]["like_count"] = (int)$result[$index]["like_count"];
//            $result[$index]["dislike_count"] = (int)$result[$index]["dislike_count"];
//            $result[$index]["status_level"] = (int)$result[$index]["status_level"];
//        }

        return $result;

    }

    public function deletePushItem($mbr_id, $pushid, $deviceid, $platform)
    {
        $remarks = trim($mbr_id) . trim($pushid) . $platform;

        $sql = <<<str
DO $$
    begin
        IF NOT EXISTS(SELECT * FROM o2o_push_log where mbr_id= '$mbr_id' and batch_id= '$pushid')
        then
            INSERT INTO o2o_push_log (pushid, mbr_id,batch_id, platform, date, remarks, status, created_on, created_by)
             VALUES
            ('$deviceid','$mbr_id','$pushid','$platform',current_timestamp,'$remarks',-1, current_timestamp , 'VcApp2') ;
        ELSE
            update o2o_push_log set status= -1, updated_on= current_timestamp, updated_by= 'VcApp2' where mbr_id= '$mbr_id' and batch_id= '$pushid' ;
        end if;
    end
$$;
str;

        $redisKey = $this->ENVIRONMENT . "#Vc_pushNotification_all";
        $indexKey = $mbr_id . $platform . $pushid . "DELETE";

        $lists["status"] = 0;
        $lists["sql"] = $sql;
        $lists["params"] = [];

        $array[$indexKey] = serialize($lists);

        Redis::hMset($redisKey, $array);
        Redis::expire($redisKey, $this->threeDays);

        $this->deleteTableMeta($mbr_id, '#notificationInbox#');

        return true;
    }


    public function deleteAllNotifications($mbr_id, $deviceid, $platform)
    {
        $this->db20->table('o2o_push_log')->where('mbr_id', $mbr_id)
            ->update([
                'status' => -1,
                'updated_on' => Carbon::now(),
                'updated_by' => 'deleteAll'
            ]);

        $lists = $this->db20->table('o2o_push_notify as notify')
            ->select('notify.id')
            ->where('notify.status', 1)
            ->where('notify.datetime', '<=', Carbon::now())
            ->whereRaw("pushtype='public' or (pushtype='private' and notify.mbr_id like '%$mbr_id%')")
            ->whereRaw("cast(notify.datetime as date) > '2019-06-01'")
            ->orderBy('notify.datetime', 'desc')
            ->take(25)->get();

        foreach ($lists as $list) {
            $push_id = $list->id;
            $remarks = trim($mbr_id) . trim($push_id) . $platform;

            $sql_upd2 = <<<str
do $$
begin
    IF NOT EXISTS(SELECT * FROM o2o_push_log where mbr_id= '$mbr_id' and batch_id= '$push_id')
    then                 
          INSERT INTO o2o_push_log (pushid, mbr_id,batch_id, platform, date, remarks, status, created_on, created_by)
             VALUES
            ('$deviceid','$mbr_id','$push_id','$platform', current_timestamp ,'$remarks',-1, current_timestamp , '$mbr_id');
    end if;
end $$
str;
            $this->db20->statement($sql_upd2);
        }
    }

    public function getTnc()
    {
        $lists = $this->db20
            ->table('oms_pages')
            ->where([
                'category' => 'TnC',
            ])
            ->whereRaw("coalesce(status,'') ='Y' ")
            ->orderBy('page_id')
            ->get();

        return $lists;
    }

    public function validateRenewAndUpgrade($mbr_id)
    {

        //For Testing.
        $realTest = true;
        $isPass = false;
        //New Validation For Renew
        $today = Carbon::now();

        $member = $this->db20
            ->table('crm_member_list')
            ->select(DB::raw("-DATE_PART('day', exp_date::timestamp - current_timestamp) as daydiff"), "mbr_type")
            ->where([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id
            ])->first();

//        print_r($member);

        if (!$member) {
            $result["renew"] = false;
            $result["upgrade"] = false;
            return $this->setResult(0, "Member has already expired", "Member has already expired");
        } else if ($member && $member->daydiff > 90) {
            $result["renew"] = false;
            $result["upgrade"] = false;
            return $this->setResult(0, "Member has already expired", $result);
        }

        $res = $this->isEligibleToRenew($mbr_id);

        if (!$res && !$isPass) {
            //"Member is not eligable to renew now.",
            $result["renew"] = false;
        } else {
            $result["renew"] = true;
        }

        $resUpg = $this->isEligibleToUpgrade($mbr_id);

        $member = $this->getById($mbr_id);
        if (!$resUpg && !$isPass) {
            //"Member is not eligable to renew now.",
            $result["upgrade"] = false;
        } else if ($member && in_array($member->mbr_type, ['MSTP', 'MST', 'MNS', 'MAS'])) {
            $result["upgrade"] = false;

        } else {
            $result["upgrade"] = true;
        }

        $result["msg"] = 'Success';
        $result["code"] = 1;

        return $result;
    }

    public function isEligibleToRenew($mbr_id)
    {
        $member = $this->getById($mbr_id);

        if ($member === null) {
            return true;
        }
        $mbrType = $member->mbr_type;

        $currMemberTier = $this->getCurrentMemberTier($mbr_id);
        if ($currMemberTier->mbr_type != '') {
            $member->exp_date = $currMemberTier->exp_date;
            $mbrType = strtoupper(trim($currMemberTier->mbr_type));
        }

        if (!in_array($mbrType, self::MEMBER_TYPES, true)) {
            return false;
        }

        preg_match('#M(\d+)#', $mbrType, $matches);

        isset($matches[1]) ?: $matches[1] = '';

        $key = 'CRM_REN' . $matches[1] . '_LIMIT';
        $daysLimit = (int)$this->getKeyConfig($key, $mbrType, 60);
//        print_r($daysLimit);

        $this->getNextMemberTier($mbr_id);

        if ($daysLimit >= $this->getMembershipDaysRemaining($member)) {
            $tier = $this->getNextMemberTier($mbr_id);

            return empty($tier->mbr_type);
        }

        return false;
    }

    private function getKeyConfig($key, $mbr_type, $default = 60)
    {
//        echo $mbr_type;
        if ($mbr_type == 'M08' || $mbr_type == 'M') {
            $default = 60;
        } else if ($mbr_type == 'M18') {
            $default = 120;
        } else if ($mbr_type == 'M28') {
            $default = 180;
        } else {
            $default = 60;
        }


//        $url = $this->ims_url . "api/vcapp/key_config/AMK";
//        $headers = array(
//            'Authorization: Bearer ' . $this->ims_authorization,
//            'Content-Type: application/json'
//        );
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, false);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
////        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
//        $result = curl_exec($ch);
//        curl_close($ch);
////        print_r($result);
//        $response = json_decode($result, true);


        return $default;
    }

    public function verifyNewMbrId($mbr_id)
    {
        if (substr($mbr_id, 0, 1) == 'V') {
            return true;
        }

        return false;
    }

    public function isEmailValid($email)
    {
        $coy_id = 'CTL';
        $email = strtolower($email);
        $member = $this->db20->table('crm_member_list')
            ->select('mbr_token as member_token', 'status_level', 'first_name',
                DB::raw("cast(exp_date as timestamp) as exp_date"),
                DB::raw("case when exp_date >= current_timestamp then 1 else 0 end as is_exist"), 'mbr_id',
                DB::raw('RTRIM(mbr_type) as mbr_type'))
            ->where('coy_id', $coy_id)
            ->whereRaw("LOWER(email_addr) = '$email'")
            ->where('mbr_type', '!=', 'X')
            ->first();

        if ($member) {
            $status_level = $member->status_level;
            $is_exist = $member->is_exist;
            $mbr_type = $member->mbr_type;

            if ($status_level == 0 && $is_exist && $mbr_type == 'MAS') {
                return array(
                    "valid" => -1,
                    "data" => null
                );
            }

            return array(
                "valid" => true,
                "data" => $member
            );
        }
        return array(
            "valid" => false,
            "data" => null
        );
    }


    public function getMembershipDaysRemaining($member, $month = false)
    {
        $nowDate = new \DateTime();
        $expDate = new \DateTime(date('Y-m-d', strtotime($member->exp_date)));

        $diff = $nowDate->diff($expDate);

        if (!$month) {
            // get the day
            $remaining = $diff->days;
        } else {
            // get the months
            $remaining = ($diff->y * 12) + $diff->m;
        }

        $diff->invert === 0 or $remaining *= -1;

        return $remaining;
    }

    public function getCurrentMemberTier($mbr_id)
    {
        $coy_id = 'CTL';

        $tier = $this->db20->table('crm_member_type')
            ->select('*')
            ->where('coy_id', $coy_id)
            ->where('mbr_id', $mbr_id)
            ->whereRaw("eff_from < current_timestamp and eff_to > current_timestamp")
            ->orderBy('line_num', 'desc')
            ->first();

        $r = new \stdClass();
        $r->exp_date = '';
        $r->mbr_type = '';

        if ($tier) {
            $r->exp_date = $tier->eff_to;
            $r->mbr_type = $tier->mbr_type;
        } else {

            $result = $this->getById($mbr_id);

            if ($result) {
                $r->exp_date = $result->exp_date;
                $r->mbr_type = $result->mbr_type;
            }
        }

        return $r;
    }

    public function getNextMemberTier($mbr_id)
    {
        $coy_id = 'CTL';

        $tier = $this->db20->table('crm_member_type')
            ->select('*')
            ->where('coy_id', $coy_id)
            ->where('mbr_id', $mbr_id)
            ->whereRaw("eff_from < current_timestamp and eff_to > current_timestamp")
            ->orderBy('line_num', 'desc')
            ->first();

        $r = new \stdClass();
        $r->exp_date = '';
        $r->mbr_type = '';

        if ($tier) {

            $nextTier = $this->db20->table('crm_member_type')
                ->select('*')
                ->where('coy_id', $coy_id)
                ->where('mbr_id', $mbr_id)
                ->where('line_num', '>', $tier->line_num)
                ->whereRaw("eff_from > current_timestamp")
                ->orderBy('line_num', 'asc')
                ->first();

            if ($nextTier) {
                $r->exp_date = $nextTier->eff_to;
                $r->mbr_type = $nextTier->mbr_type;
            }
        }

        return $r;
    }

    public function isEligibleToUpgrade($mbr_id)
    {
        $member = $this->getById($mbr_id);
        if ($member) {
            $member->mbr_type = rtrim($member->mbr_type);

            return in_array($member->mbr_type, [
                    self::MEMBER_08, self::MEMBER_18,
                ], true) && $this->getMembershipDaysRemaining($member) > 0;
        }

        return false;
    }

    public function getById($mbr_id)
    {
        $member = $this->db20->table('crm_member_list')->where([
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id
        ])->first();
        return $member;

    }

    private function setResult($code, $msg, $result)
    {
        $resp["code"] = $code;
        $resp["msg"] = $msg;
        $resp["result"] = $result;

        return $resp;
    }

    public function getFaceIdFromLog($mbr_id)
    {
//        $sql = <<<str
//select remarks as face_id from o2o_log_live_processes
//where process_id='VC_STAFF_ADD_PHOTO' and mbr_id=? order by created_on desc
//str;

        $item = $this->db20
            ->table('o2o_log_live_processes')
            ->select('remarks as face_id')
            ->where([
                'process_id' => 'VC_STAFF_ADD_PHOTO',
                'mbr_id' => $mbr_id
            ])
            ->orderBy('created_on', 'desc')
            ->first();

        $face_id = $item ? $item->face_id : '';

        return $face_id;
    }

    public function updateMemberSubStatus($mbr_id, $sub_ind1, $sub_ind2)
    {
        $coy_id = $this->CTL_COY_ID;

        $this->db20->table('crm_member_list')
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id
            ])->update([
                'sub_ind1' => $sub_ind1,
                'sub_ind2' => $sub_ind2,
                'modified_on' => Carbon::now()
            ]);

        return true;
    }


    public function checkMemberExpiry($main_id)
    {
        $coy_id = $this->CTL_COY_ID;

        $member = $this->db20
            ->table('crm_member_list')
            ->where('coy_id', $coy_id)
            ->where('mbr_id', $main_id)
            ->whereRaw('exp_date > current_timestamp')
            ->first();
        if (!$member) {
            return false;
        } else {
            return true;
        }
    }

    public function isAssoEmailExist($email)
    {
        $email = strtolower($email);

        $list = $this->db20
            ->table('crm_member_list')
            ->whereRaw("LOWER(email_addr)= '$email'")
            ->first();

        if ($list) {
            return $list;
        }
        return array();
    }

    public function findByMbrId($memberId)
    {
        $data = $this->db20->table('crm_member_list')->whereRaw('LOWER(mbr_id)=?', trim(strtolower($memberId)))->first();
        return $data;
    }

    public function has_associate($mbr_id)
    {
        $res = $this->db20->table('crm_member_list')
            ->select('email_addr', 'mbr_id', DB::raw("CONCAT(first_name ,' ', last_name) as full_name"), 'mbr_type')
            ->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])->first();
        return $res;
    }

    public function getMainMember($mbr_id)
    {
        $res = $this->db20->table('crm_member_list')
            ->select('email_addr', 'mbr_id', DB::raw("CONCAT(first_name ,' ', last_name) as full_name"), 'mbr_type')
            ->where(['status_level' => '1', 'mbr_id' => $mbr_id])->first();
        return $res;
    }

    public function getSessionToken($mbr_id)
    {
        $key = $this->server_name . $this->_login_token_prefix . $mbr_id; //. $key;
        $session = unserialize(Redis::get($key));
        if ($session) {
            return $session;
        }
        return '';
    }

    public function get_associate($mbr_id)
    {
        return $this->db20->table('crm_member_list')->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])->get();
    }

    public function isMemberEmailExist($email)
    {
        $email = strtolower($email);

        $result = $this->db20
            ->table('crm_member_list')->select('mbr_id')
            ->whereRaw("LOWER(email_addr)='$email'")
            ->first();

        return empty($result->mbr_id) ? false : true;
    }

    public function getAssoMainId($main_id, $coy_id)
    {
        $user = $this->db20->table('crm_member_list')
            ->select(DB::raw("RTRIM(mbr_id) as mbr_id"), 'exp_date', DB::raw("CONCAT(first_name , ' ' ,last_name) as full_name"))
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $main_id
            ])->first();

        return $user;

    }

    //Use Existing generateMbrId.
    public function generateNewMbrId($source = '')
    {
        if (strlen($source) < 2) {
            $source = '00';
        } else {
            $source = strlen($source) == 2 ? strtoupper($source) : strtoupper(substr($source, 0, 2));
        }
        $now = Carbon::now()->toDateTimeString();
        $year = date('y');
        $coy_id = 'CTL';
        $sys_id = 'CRM';
        $prefix = 'V' . $year;
        $trans_prefix = 'MBR-' . $prefix;
        $is_exists = $this->db20->table('sys_trans_list')->where(['coy_id' => $coy_id, 'sys_id' => $sys_id, 'trans_prefix' => $trans_prefix])->first();
        $next_num = 1;
        if (!$is_exists) {
            $this->db20->table('sys_trans_list')->create(['coy_id' => $coy_id, 'sys_id' => $sys_id, 'trans_prefix' => $trans_prefix, 'next_num' => $next_num + 1, 'sys_date' => $now]);
        }
        $next_num = $this->db20->table('sys_trans_list')->where(['coy_id' => $coy_id, 'sys_id' => $sys_id, 'trans_prefix' => $trans_prefix])->first()->next_num;
        $chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $len_next_num = strlen($next_num);
        $first_next = $len_next_num > 4 ? substr($chars, substr((string)$next_num, 0, $len_next_num - 4) - 1, 1) : 0;
        $second_next = sprintf("%04s", substr($next_num, -4));
        $mbr_id = $prefix . $first_next . $second_next . $source;
        $this->db20->table('sys_trans_list')->where(['coy_id' => $coy_id, 'sys_id' => $sys_id, 'trans_prefix' => $trans_prefix])->update(['next_num' => $next_num + 1, 'sys_date' => $now]);
        return $mbr_id;

//        $coy_id = 'CTL';
//        $sys_id = 'CRM';
//        $year = date('y');
//        $trans_prefix = 'MBR-V' . $year;
//        $length = 10;
//        $so_id_header = 'V' . $year;
//
//        $remain_length = $length - strlen($so_id_header) - strlen($code);
//
//        $sql = <<<str
//select next_num
//from sys_trans_list
//where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'
//
//str;
//        $result = $this->db20->select($sql, []);
//        if (count($result) === 0) {
//            $sql_ins = <<<str
//INSERT INTO sys_trans_list(coy_id,sys_id,trans_prefix,next_num,sys_date)
//VALUES
//( ? , ? , ? ,2,current_timestamp )
//str;
//            $this->db20->statement($sql_ins, [$coy_id, $sys_id, $trans_prefix]);
//            $next_num = 1;
//            $so_id_num = str_pad($next_num, $remain_length, '0', STR_PAD_LEFT);
//
//        } else {
//            $sql_upd = <<<str
//update sys_trans_list
//set next_num = next_num +1,sys_date = current_timestamp
//where coy_id = ? and sys_id = ?  and trans_prefix = ?
//
//str;
//            $this->db20->statement($sql_upd, [$coy_id, $sys_id, $trans_prefix]);
//            $next_num = $result[0]->next_num;
//            $so_id_num = str_pad($next_num, $remain_length, '0', STR_PAD_LEFT);
//        }
//
//        return $so_id_header . $so_id_num . $code;
    }

    public function generateNextNum($trans_prefix, $so_id_header = '', $length = 15, $doc_type = 'MEMBER', $sys_id = 'CRM', $coy_id = 'CTL')
    {

        $remain_length = $length - strlen($so_id_header);

        $sql = <<<str
select next_num
from sys_trans_list
where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'

str;

        $result = $this->db20->select($sql, []);
        if (count($result) === 0) {
            $sql_ins = <<<str
INSERT INTO sys_trans_list(coy_id,sys_id,trans_prefix,next_num,sys_date)
VALUES
( ? , ? , ? ,2,current_timestamp )
str;
            $this->db20->statement($sql_ins, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = 1;
            $so_id_num = str_pad($next_num, $remain_length, '0', STR_PAD_LEFT);

        } else {
            $sql_upd = <<<str
update sys_trans_list 
set next_num = next_num +1,sys_date = current_timestamp 
where coy_id = ? and sys_id = ?  and trans_prefix = ? 

str;
            $this->db20->statement($sql_upd, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = $result[0]->next_num;
            $so_id_num = str_pad($next_num, $remain_length, '0', STR_PAD_LEFT);
        }

        return $so_id_header . $so_id_num;
    }

    public function getNextTransIdNo($trans_type = 'OD', $length = 8)
    {
        $coy_id = $this->CTL_COY_ID;
        $sys_id = "SMS";

        $trans_prefix = $trans_type . date("Y");
        $dev = '';
//        if ($this->ENVIRONMENT != 'production') {
//            $dev = 'T';
//            $str_length = 6;
//        }

        $alphabet_list = ['E', 'F', 'G', 'H', 'I'];
        if (date("Y") == '2021') {
            $year_alphabet = 'D';
        } else {
            $year_date = (int)date("Y") - 2021;
            $year_alphabet = $alphabet_list[$year_date];
        }

        $so_id_header = $trans_type . $year_alphabet . $dev;
        $str_length = $length - strlen($so_id_header);

        $sql = <<<str
select next_num
from sys_trans_list
where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'

str;
        $result = $this->db20->select($sql, []);
        if (count($result) === 0) {
            $sql_ins = <<<str
INSERT INTO sys_trans_list(coy_id,sys_id,trans_prefix,next_num,sys_date)
VALUES
( ? , ? , ? ,2,current_timestamp )
str;
            $this->db20->statement($sql_ins, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = 1;
            $so_id_num = str_pad($next_num, $str_length, '0', STR_PAD_LEFT);

        } else {
            $sql_upd = <<<str
update sys_trans_list 
set next_num = next_num +1,sys_date = current_timestamp 
where coy_id = ? and sys_id = ?  and trans_prefix = ? 

str;
            $this->db20->statement($sql_upd, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = $result[0]->next_num;
            $so_id_num = str_pad($next_num, $str_length, '0', STR_PAD_LEFT);
        }

        return $so_id_header . $so_id_num;

    }

    public function getNextInvoiceNo()
    {
        $coy_id = $this->CTL_COY_ID;
        $sys_id = "SMS";

        $trans_prefix = 'MBRTEMP' . date("Ym");
        $dev = '';
        $str_length = 7;
        if ($this->ENVIRONMENT != 'production') {
            $dev = 'T';
            $str_length = 6;
        }
        $so_id_header = 'OS' . date("Ym") . $dev;

        $sql = <<<str
select next_num
from sys_trans_list
where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'

str;

        $result = $this->db20->select($sql, []);
        if (count($result) === 0) {
            $sql_ins = <<<str
INSERT INTO sys_trans_list(coy_id,sys_id,trans_prefix,next_num,sys_date)
VALUES
( ? , ? , ? ,2,current_timestamp )
str;
            $this->db20->statement($sql_ins, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = 1;
            $so_id_num = str_pad($next_num, $str_length, '0', STR_PAD_LEFT);

        } else {
            $sql_upd = <<<str
update sys_trans_list 
set next_num = next_num +1,sys_date = current_timestamp 
where coy_id = ? and sys_id = ?  and trans_prefix = ? 

str;
            $this->db20->statement($sql_upd, [$coy_id, $sys_id, $trans_prefix]);
            $next_num = $result[0]->next_num;
            $so_id_num = str_pad($next_num, $str_length, '0', STR_PAD_LEFT);
        }

        return $so_id_header . $so_id_num;
    }


//    public function getOSInvoiceNo($trans_type, $AP_prefix)
//    {
////        array(0));
//        $coy_id = "CTL";
//        $sys_id = "SMS";
//
//        $trans_prefix = $AP_prefix;
//        $so_id_header = $trans_type . date("Ym");
//
//        $sql = <<<str
//select next_num
//from sys_trans_list
//where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'
//
//str;
//
//        $result = $this->db20->select($sql, []);
//        if (count($result) === 0) {
//            $sql_ins = <<<str
//INSERT INTO sys_trans_list(coy_id,sys_id,trans_prefix,next_num,sys_date)
//VALUES
//( ? , ? , ? ,2,current_timestamp )
//str;
//            $this->db20->statement($sql_ins, [$coy_id, $sys_id, $trans_prefix]);
//            $next_num = 1;
//            $so_id_num = str_pad($next_num, 7, '0', STR_PAD_LEFT);
//
//        } else {
//            $sql_upd = <<<str
//update sys_trans_list
//set next_num = next_num +1,sys_date = current_timestamp
//where coy_id = ? and sys_id = ?  and trans_prefix = ?
//
//str;
//            $this->db20->statement($sql_upd, [$coy_id, $sys_id, $trans_prefix]);
//            $next_num = $result[0]->next_num;
//            $so_id_num = str_pad($next_num, 7, '0', STR_PAD_LEFT);
//        }
//
//        return $so_id_header . $so_id_num;
//    }


    public function updatePushOption($value, $meta)
    {
        $table = 'Other_meta';
        $query = <<<str
        INSERT INTO "$table" ( meta , value) 
        VALUES (?, ?)
        ON CONFLICT (meta) DO UPDATE 
          SET value = ? 
str;
        $this->chapps->statement($query, [$meta, $value, $value]);
    }

    public function getLatestPushLogId()
    {
        $sql = <<<str
select MAX(batch_id) as batch_id from o2o_push_log
str;
        $result = $this->db20->select($sql, []);
        if (count($result) > 0 && isset($result[0]->batch_id)) {
            return $result[0]->batch_id;
        }
        return false;
    }

    public function addAccount(array $data = [], $is_ctl_member = FALSE)
    {
        if (empty($data)) {
            return FALSE;
        }

        $data['coy_id'] = $this->CTL_COY_ID;

        if (!isset($data['mbr_type']) || $is_ctl_member) {
            $data['mbr_type'] = 'M';
        }

        if (!isset($data['mbr_id'])) {
            return false;
        }

        if (isset($data['mbr_pwd']) && strlen($data['mbr_pwd']) !== 32) {
            $data['mbr_pwd'] = md5($data['mbr_pwd']);
        }

        if (!isset($data['status_level'])) {
            $data['status_level'] = 1;
        }

        if (!isset($data["main_id"])) {
            $data["main_id"] = '';
        }

        $data["mbr_title"] = "";
        $data["nationality_id"] = '';
        $data["pwd_changed"] = Carbon::now();
        $data["last_login"] = Carbon::now();
        $data["mbr_addr"] = "1-PRIMARY";
        $data["delv_addr"] = "2-DELIVERY";
        $data["send_info"] = "";
        $data["send_type"] = "";
        $data["join_date"] = Carbon::now();
        $data["last_renewal"] = Carbon::now();

        $data["points_accumulated"] = 0;
        $data["points_reserved"] = 0;
        $data["points_redeemed"] = 0;
        $data["points_expired"] = 0;
        $data["mbr_savings"] = 0;

        $data["rebate_voucher"] = '';
        $data["sub_ind1"] = 'Y';
        $data["sub_ind2"] = 'YYYY';
        $data["sub_date"] = Carbon::now();

        $data["created_by"] = $data["main_id"];
        $data["created_on"] = Carbon::now();
        $data["modified_by"] = $data["main_id"];
        $data["modified_on"] = Carbon::now();
        $data["updated_on"] = Carbon::now();
        $data["login_locked"] = 'N';


        $res = $this->db20->table('crm_member_list')->insert($data);

        return $res;
    }

    public function insertMemberType($mbr_id, $main_id, $exp_date, $mbr_type)
    {
        $coy_id = $this->CTL_COY_ID;

        $insert_data = array(
            "coy_id" => $coy_id,
            'mbr_id' => $mbr_id,
            "line_num" => 1,
            "eff_from" => Carbon::now()->startOfDay()->toDateTimeString(),
            "eff_to" => $exp_date,
            "mbr_type" => $mbr_type,
            "created_by" => $main_id,
            "created_on" => Carbon::now(),
            "modified_by" => '',
            "modified_on" => Carbon::now()
        );

        $res = $this->db20
            ->table('crm_member_type')
            ->insert($insert_data);

        return $res;
    }

    public function isValidEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (strpos($email, '@') === false || strpos($email, '.') === false) {
            return false;
        }

        return true;
    }

    public function isMemberContactExist($contact)
    {
        $result = $this->db20->table('crm_member_list')->select('mbr_id')
            ->where('contact_num', $contact);

        return empty($result->mbr_id) ? false : true;
    }

    public function isContactNum($contact)
    {
        if (substr($contact, 0, 1) === "+" || substr($contact, 0, 1) === "#" || substr($contact, 0, 1) === "*") {
            $contact = substr($contact, 1);
        }

        if (strlen($contact) < 8 || strlen($contact) > 10) {
            return false;
        }

        if (!(preg_match('/^[0-9]+$/', $contact) && strval(intval($contact)) == strval($contact))) {
            return false;
        }

        if (substr($contact, 0, 1) != '8' && substr($contact, 0, 1) != '9' && substr($contact, 0, 1) != '6') {
            return false;
        }

        return true;
    }

    public function isStaffCodeValid($staffcode)
    {
        $prefix = substr($staffcode, 0, 5);
        //-- Mindef
        $date = date("Ymd");
        if ($staffcode == 'hachi1002' && $date >= '20210413' && $date <= '20211201') {
            return true;
        }
        //-- eof Mindef
        if ($prefix !== 'hachi') {
            return false;
        }
        $staffCode = strtolower(str_replace("hachi", "", $staffcode));

        $url = $this->ims_url . "vcapp/getcoyuserid";

//        echo $staffCode;

        $post_data = array(
            "staff_id" => $staffCode
        );

        $response = $this->curlImsApi($url, $post_data);

//        echo "here";

        $saleperson_id = $response && isset($response["saleperson_id"]) ? $response["saleperson_id"] : '';
//        dd($saleperson_id);
        if (!$saleperson_id) {
            $result2 = DB::connection('chopeshift')
                ->table('ee_list')
                ->select('ee_id as saleperson_id')
                ->where([
                    'status_level' => 1,
                    'ee_id' => strtoupper($staffCode)
                ])->first();
//            dd($result2);
            return empty($result2->saleperson_id) ? false : true;
        } else {
            return true;
        }
    }

    private function curlImsApi($url, $post_data)
    {
        $headers = array(
            'Authorization: Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);


        $response = json_decode($result, true);
        curl_close($ch);

//        print_r($response);

        return $response["data"];
    }


    public function isEduEmailValid($email)
    {
        $email0 = explode("@", $email);
        $email1 = explode(".", strtolower($email0[1]));
        $surfixCheck = strtolower($email0[count($email0) - 1]);
        if (in_array('edu', $email1)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCustomerInfo($mbr_id)
    {
        $customer = $this->db20->table('crm_member_list')
            ->select('email_addr', 'first_name')
            ->where('mbr_id', $mbr_id)
            ->where('coy_id', 'CTL')
            ->first();

        if (empty($customer)) {
            return NULL;
        }
        return $customer;
    }

    public function getLoginRedis($mbr_id, $isChapps = false)
    {
        if ($isChapps) {

            $query = <<<str
    SELECT * FROM "Members" WHERE mbr_id= ? 
str;
            $result = $this->chapps->select($query, [$mbr_id]);
//            print_r($result);

            if (count($result) === 0) {
                return array();
            }
            $data = json_decode($result[0]->meta, true);
            return $data;
        } else {
            $key = '#LOGININFO#';
            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id;
            $session = unserialize(Redis::hget($hkey, $key));
            if ($session) {
                return $session;
            }
            return array();
        }
    }

    public function checkNonMemberTransaction($staff_id, $trans_id)
    {
        $sql2 = <<<str
SELECT COUNT(trans_id) purchased_trans, COUNT(trans_id) - (select COUNT(*) from o2o_log_live_processes_new as new where process_id='VOTE_STAFF'
    and mbr_id='' and remarks= ? and new.trans_id= trans_id and cast(created_on as date)=cast(current_timestamp as date)) AS vote_count from (
      
        select trans_id
        from crm_member_transaction
        where mbr_id='' and trans_id= ?
        and cast(trans_date as date) = cast(current_timestamp as date)
        and trans_type in ('PS','RF','OS','VC','EX','PU','ID','DZ','DU','HI')

     ) as a
str;

        $validate = $this->db20
            ->select($sql2, [$staff_id, $trans_id]);

//        print_r($validate);

        if (count($validate) === 0) {
            return -1;
        } else if ($validate[0]->purchased_trans === 0) {
            return -1;
        } else if ($validate[0]->vote_count <= 0) {
            return 0;
        }
        return 1;


    }

    public function isMemberExist($mbr_id)
    {
        $member = $this->db20->table('crm_member_list')
            ->select(DB::raw("COUNT(*)"))
            ->where('mbr_id', $mbr_id)
            ->where('status_level', 1)
            ->get();

        if (count($member) > 0) {
            return true;
        }
        return false;
    }

    public function setTokenMbr($token_id, $remarks, $expire_on = 300, $mbr_id = '', $trans_id = '')
    {
        $timeNow = Carbon::now();
        $timeNow->addSeconds($expire_on);

        $this->db20->table('vc_token_list')
            ->insert([
                'token_id' => $token_id,
                'remarks' => $remarks,
                'mbr_id' => $mbr_id,
                'status_level' => 0,
                'expired_on' => $timeNow,
                'created_on' => Carbon::now(),
                'created_by' => 'VcApp',
                'trans_id' => $trans_id
            ]);

        return true;
    }

    public function checkDailyTransaction($mbr_id, $staff_id)
    {
        $lists = $this->db20->table('crm_member_transaction')
            ->select('trans_id')
            ->where('mbr_id', $mbr_id)
            ->whereIn('trans_type', ['PS', 'RF', 'OS', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'HI'])
            ->whereRaw("cast(trans_date as date)= cast(current_timestamp as date)")
            ->get();

        if (count($lists) === 0) {
            return -1;
        } else {
            $isUsed = false;
            $trans_id = '';

            foreach ($lists as $list) {
                $check_trans_id = $list->trans_id;

                $check_sql = <<<str
		select status from 
	o2o_log_live_processes_new process where process_id='VOTE_STAFF' and mbr_id= ?
	  and process.invoice_id= ? and remarks= ?
str;
                $validate = $this->db20->select($check_sql, [$mbr_id, $check_trans_id, $staff_id]);
//				print_r($validate);
                if (!$validate) {
                    $isUsed = true;
                    $trans_id = $check_trans_id;
                    break;
                }
            }

            if ($isUsed) {
                return $trans_id;
            } else {
                return 0;
            }
        }
    }

    public function IsPutRedis($redisKey, $data, $mbr_id, $expiry = 300)
    {
        $isPushTime = $this->checkIfBlockSql();
        if ($isPushTime) {
            return array();
        }

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        if (Redis::hset($hkey, $redisKey, serialize($data)) == FALSE) {
            return false;
        }
        Redis::expire($hkey, $expiry);
    }

    public function CheckIsRedis($redisKey, $mbr_id)
    {
        $isPushTime = $this->checkIfBlockSql();
        if ($isPushTime) {
            return array();
        }

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;

        $cachedata = Redis::hget($hkey, $redisKey);
        if ($cachedata) {
//            echo "redis";
            $data = unserialize($cachedata);
            return $data;
        }
        return null;
    }

    public function getTransSql($mbr_id, $page_from, $page_to, $transType, $fdate, $tdate, $ctlInvType, $hsgInvType, $tfrom, $coy_id)
    {
        if ($page_from === 1 || $page_from === 0) {
            $page_from = 0;
        }

        $redisKey = "#myPurchases#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate . $tfrom;
        $transactions_redis = $this->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }

//        echo $redisKey;
//        echo $page_to;

        $trans_query = $this->db20->table('crm_member_transaction')
            ->selectRaw('DISTINCT(trans_id) , trans_date')
            ->where('mbr_id', $mbr_id)
            ->whereIn('trans_type', $transType)
            ->whereRaw(" LEFT(trans_id, 3) != 'NEW'")
            ->whereRaw("cast(trans_date as date) >= '$fdate'")
            ->whereRaw("cast(trans_date as date) <= '$tdate'")
            ->orderBy('trans_date', 'desc');

        if ($page_to == 99999) {

        } else {
            $trans_query->take(5)->offset($page_from);
        }


        $trans_lists = $trans_query->get();

//        print_r($transType);
//        print_r($trans_lists);
//        die();

        $trans_id_lists = [];
        foreach ($trans_lists as $index => $trans_list) {
            $the_trans_id = trim($trans_list->trans_id);
            if (!in_array($the_trans_id, $trans_id_lists)) {
                $trans_id_lists[] = $the_trans_id;
            }
        }


        $sql_not_items = <<<str
select a.item_id from ims_item_alt_id a, ims_item_list b where a.coy_id = b.coy_id and
    a.alt_id = b.item_id and b.coy_id = '$coy_id' and b.item_type = 'Z'
str;

        $not_item_querys = $this->db20->select($sql_not_items);
        $not_item_lists = [];
        foreach ($not_item_querys as $not_item_query) {
            $the_item_id = $not_item_query->item_id;
            $not_item_lists[] = $the_item_id;
        }
//        return $not_item_lists;

        $transactions = $this->db20->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.mbr_id', $mbr_id)
            ->whereRaw("cast(a.trans_date as date) >= '$fdate'")
            ->whereRaw("cast(a.trans_date as date) <= '$tdate'")
            ->whereIn('a.trans_type', $transType)
            ->whereIn('a.trans_id', $trans_id_lists)
            ->whereNotIn('item_id', $not_item_lists)
            ->whereRaw('LEFT(a.trans_id,3)<>\'NEW\'')
            ->where('a.item_id', 'NOT LIKE', '#DELV%')
            ->where('a.item_id', 'NOT LIKE', '#OTHERS%')
            ->where('a.item_id', 'NOT LIKE', '#HI-MISSING%')
            ->whereRaw('(a.item_id not like \'!%\' or a.item_id LIKE \'!MEMBER-%\' or a.item_id LIKE \'!EGIFT-%\')')
            ->where('a.item_id', '<>', '!MEMBER-HACON')
            ->where('a.item_desc', '<>', '')
            ->orderBy('a.trans_date', 'desc')
            ->get();

        if (count($transactions) > 0) {
            $store_lists = $this->getLocationLists();

            foreach ($transactions as $transaction) {
                $trans_id = trim($transaction->trans_id);
                $rowkey = array_search($trans_id, $trans_id_lists) + $page_from;

                $trans_type = rtrim($transaction->trans_type);
                $trans_date = $transaction->trans_date;
                $loc_id = $transaction->loc_id;
                $ho_id = $trans_type === 'HR' ? '' : $trans_id;
                if (in_array($trans_type, $ctlInvType)) {
                    $inv_type = 1;

                    $invoice_id = $trans_id;
                    $sub = substr(trim($invoice_id), -3, 3);
                    $signature = md5($invoice_id . '@Challenger.' . $sub);
                    $url_link = "https://www.challenger.com.sg/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;

                } elseif (in_array($trans_type, $hsgInvType)) {
                    $inv_type = 2;

                    $invoice_id = $trans_id;
                    $sub = substr(trim($invoice_id), -3, 3);
                    $signature = md5($invoice_id . '@Challenger.' . $sub);
                    $url_link = "https://www.hachi.tech/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;

                } else {
                    $inv_type = 0;

                    $invoice_id = $trans_id;
                    $sub = substr(trim($invoice_id), -3, 3);
                    $signature = md5($invoice_id . '@Challenger.' . $sub);
                    $url_link = "https://www.hachi.tech/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;

                }
                $trdate = Carbon::parse($trans_date)->toDateString();
//                $ref_item = $cherps2Service->get_ref_key($trans_id);
//                $ref_key = ($ref_item && $ref_item["ref_key"]) ? trim($ref_item["ref_key"]) : '';
                $ref_key = '';

                if (substr($trans_id, 0, 2) === 'HI') {
                    $store = '';
                } elseif (strpos($loc_id, '-C') !== false || strpos($loc_id, '-E') !== false || strpos($loc_id, '-I') !== false || strpos($loc_id, '-O') !== false || strpos($loc_id, '-P') !== false || strpos($loc_id, '-S') !== false) {
                    $store = 'Trade Show';
                } elseif (strpos($loc_id, '-RS') !== false) {
                    $store = 'Road Show';
                } else {

                    $store = $this->getStoreName($store_lists, $loc_id);
                }
                $transaction->rownum = $rowkey;
                $transaction->ref_key = $trans_id;
                $transaction->store = $store;
                $transaction->trdate = $trdate;
                $transaction->inv_type = $inv_type;
                $transaction->ho_id = $ho_id;

                $enc_trans_id = $this->encrypt_aes($trans_id);
                $transaction->trans_id2 = $enc_trans_id;

                $transaction->url_link = $url_link;

            }
        }

        $this->IsPutRedis($redisKey, $transactions, $mbr_id);


        return $transactions;
    }

    public function getRedemptionSql($mbr_id, $page_from, $page_to, $fdate, $tdate, $coy_id)
    {
//        if (!$isLoad && ($page_from === 1 || $page_from === 0)) {
//            $page_from = 0;
//        }

        $redisKey = "#myRedemption#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate;
        $transactions_redis = $this->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }

        $trans_lists = $this->db20->table('crm_member_transaction')
            ->select('trans_id')
            ->where('coy_id', $coy_id)
            ->where('mbr_id', $mbr_id)
            ->whereIn('trans_type', ['RD', 'TP', 'OD'])
            ->whereRaw("cast(trans_date as date) >= '$fdate'")
            ->whereRaw("cast(trans_date as date) <= '$tdate'")
            ->where('trans_points', '<', 0)
            ->orderBy('trans_date', 'desc')
            ->get();

        $trans_id_lists = [];
        foreach ($trans_lists as $index => $trans_list) {
            if ($index >= $page_from - 1 && $index <= $page_to - 1) {
                $the_trans_id = trim($trans_list->trans_id);
                if (!in_array($the_trans_id, $trans_id_lists)) {
                    $trans_id_lists[] = $the_trans_id;
                }
            }
        }
//        print_r($trans_id_lists);

        $transactions = $this->db20->table('crm_member_transaction AS a')
            ->select('a.coy_id', DB::raw("rtrim(a.mbr_id) mbr_id, rtrim(a.trans_id) trans_id"), 'line_num', 'trans_type', 'trans_date',
                DB::raw("rtrim(a.loc_id) loc_id, rtrim(pos_id) pos_id, rtrim(item_id) item_id"), 'item_desc', 'regular_price', 'unit_price',
                'disc_percent', 'disc_amount', 'trans_points',
                DB::raw("rtrim(a.salesperson_id) salesperson_id"), 'mbr_savings', DB::raw("rtrim(a.created_by) created_by, rtrim(a.modified_by) modified_by"), 'a.created_on', 'a.modified_on', 'updated_on',
                DB::raw('ABS(item_qty) as item_qty'))
            ->where('a.coy_id', $coy_id)
            ->where('a.mbr_id', $mbr_id)
            ->whereIn('a.trans_id', $trans_id_lists)
            ->whereIn('a.trans_type', ['RD', 'TP', 'OD'])
            ->where('a.trans_points', '<', 0)
            ->orderBy('a.trans_date', 'desc')
            ->get();

        if (count($transactions) > 0) {
            $store_lists = $this->getLocationLists();

            foreach ($transactions as $transaction) {
                $trans_id = trim($transaction->trans_id);
                $rowkey = array_search($trans_id, $trans_id_lists) + $page_from;

                $trans_type = rtrim($transaction->trans_type);
                $trans_date = $transaction->trans_date;
                $loc_id = trim($transaction->loc_id);

                $ho_id = in_array($trans_type, ['HR', 'TP']) ? '' : $trans_id;

                $invoice_id = $trans_id;
                $sub = substr(trim($invoice_id), -3, 3);
                $signature = md5($invoice_id . '@Challenger.' . $sub);

                $inv_id = "";
                if (substr($trans_id, 0, 2) != 'HI' && substr($trans_id, 0, 2) != 'OS' && $trans_id != '') {
                    $inv_id = "";
                }

                $trdate = Carbon::parse($trans_date)->toDateString();

                if (substr($trans_id, 0, 2) === 'HI') {
                    $store = '';
                    $inv_type = 2;

                    $url_link = "https://www.hachi.tech/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;


                } elseif (strpos($loc_id, '-C') !== false || strpos($loc_id, '-E') !== false || strpos($loc_id, '-I') !== false || strpos($loc_id, '-O') !== false || strpos($loc_id, '-P') !== false || strpos($loc_id, '-S') !== false) {
                    $store = 'Trade Show';
                    $inv_type = 1;
                    $url_link = "https://www.challenger.com.sg/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;


                } elseif (strpos($loc_id, '-RS') !== false) {
                    $store = 'Road Show';
                    $inv_type = 1;
                } else {
                    $store = $this->getStoreName($store_lists, $loc_id);

                    $inv_type = 1;

                    $url_link = "https://www.challenger.com.sg/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;

                }
                $transaction->rownum = $rowkey;
                $transaction->store = $store;
                $transaction->trdate = $trdate;
                $transaction->inv_type = $inv_type;
                $transaction->ho_id = $ho_id;
                $transaction->inv_id = $inv_id;

                $transaction->url_link = $url_link;

                $enc_trans_id = $this->encrypt_aes($trans_id);
                $transaction->trans_id2 = $enc_trans_id;
            }
        }

        $this->IsPutRedis($redisKey, $transactions, $mbr_id);

        return $transactions;
    }


    private function getStoreName($lists, $the_loc_id)
    {
        $isFound = false;
        $store_name = '';
        if (count($lists) > 0) {

            foreach ($lists as $key => $list) {
                $loc_id = $list["loc_id"] ? trim($list["loc_id"]) : '';
                if ($loc_id == $the_loc_id) {
                    $store_name = $list["shop_name"];
                    $isFound = true;
                }

                if ($isFound) {
                    break;
                }
            }
        }

        return $store_name;
    }

    private function encrypt_aes($data)
    {
        return base64_encode($data);
    }

    private function decrypt_aes($data)
    {
//        $transdata = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, "ch@ll3ng3r408553", hex2bin($data), MCRYPT_MODE_ECB);
//        return preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $transdata);
        return $data;
    }


    public function getLocationLists()
    {
        $redisKey = $this->server_name . "#LOCATION_LISTS";
        $redis = Redis::get($redisKey);
        if ($redis) {
            $result = json_decode($redis, true);
            return $result;
        }

        if ($this->ENVIRONMENT == 'development') {
            $url = "http://chappos-api.sghachi.com/api/function/store_location";
        } else {
            $url = "https://pos8.api.valueclub.asia/api/function/store_location";
        }

        $headers = array(
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);

        $data = array();
        if ($result["status_code"] == 200) {
            $data = $result["data"];

            Redis::set($redisKey, json_encode($data));
            Redis::expire($redisKey, $this->oneMonth);

        }

        return $data;
    }

    public function getMyVoucher($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to, $orderBy = '', $desc = '')
    {
        $redisKey = "#myVouchers#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate . $orderBy . $desc;
        $transactions_redis = $this->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }

        if ($orderBy && $orderBy != "0") {
            $orderBySql = " order by $orderBy $desc";
        } else {
            $orderBySql = " ";
        }

        $sql = <<<str
    	SELECT * FROM
  (
        SELECT ROW_NUMBER() OVER(ORDER BY valid_until desc) rownum,*  FROM (
            select ct.receipt_id1                                                                                               as trans_id,
       data.coupon_id                                                                                               as item_id,
       ct.status_level                                                                                              as status_level,
       cast(ct.issue_date as date)                                                                                  as issue_date,
       ct.coupon_serialno,
       data.coupon_description                                                                                      as long_desc,
       data.coupon_excerpt                                                                                          as item_desc,
       1                                                                                                            as qty_reserved,
       ct.issue_type                                                                                                as issue_type,
       case
           when coalesce(ct.receipt_id2, '') = '' THEN
               case
                   WHEN cast(ct.expiry_date as timestamp) > cast(current_timestamp as timestamp) THEN 'Valid'
                   ELSE 'Expired' END
           ELSE 'Redeemed'
           END                                                                                                      as status,
       case
           when coalesce(ct.receipt_id2, '') = '' and cast(ct.expiry_date as date) > cast(current_timestamp as date)
               THEN 1
           ELSE 0 END                                                                                               as valid_status,
       ct.mbr_id,
       ''                                                                                                           as url,
       ct.created_on,
       CASE
           WHEN coalesce(data.coupon_name, '') != '' THEN data.coupon_name
           ELSE data.coupon_description END                                                                         as coupon_name,
       CASE
           WHEN coalesce(data.coupon_description, '') != '' THEN data.coupon_description
           ELSE data.coupon_name END                                                                                as coupon_desc,
       CASE
           WHEN coalesce(data.coupon_tnc, '') != '' THEN data.coupon_tnc
           ELSE data.coupon_description END                                                                         as tnc,
       ct.expiry_date                                                                                               as expiry_date,
       cast(ct.expiry_date as date)                                                                                 as valid_until,
       CASE
           WHEN (ct.coupon_id LIKE '%!VCH-QR%' and cast(ct.expiry_date as date)
               < cast(current_timestamp as date) and coalesce(ct.receipt_id2, '') = '') THEN 'noshow'
           ELSE 'show' END                                                                                          as qr_show,
           ct.trans_date
FROM crm_voucher_list ct
         LEFT JOIN crm_voucher_data data ON RTRIM(data.coupon_id) = RTRIM(ct.coupon_id)
where RTRIM(ct.mbr_id) = ?
  AND cast(ct.expiry_date as timestamp) > cast(current_timestamp as timestamp)
  AND coalesce(ct.issue_type, '') NOT IN ('GV', 'CREDIT')
    ) a WHERE cast(a.trans_date as date) between '$fdate' and '$tdate'
	) a
	WHERE a.rownum between ? and ? $orderBySql 
str;

        $vouchers = $this->db20->select($sql, [$mbr_id, $page_from, $page_to]);


        $this->IsPutRedis($redisKey, $vouchers, $mbr_id);


        return $vouchers;
    }

    public function getMyGifts($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to)
    {
        $redisKey = "#myGifts#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate;
        $transactions_redis = $this->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }


        $sql = <<<str
       	SELECT * FROM
  (
        SELECT ROW_NUMBER() OVER(ORDER BY valid_until desc) rownum,*  FROM (
            SELECT  CASE WHEN coalesce(t.receipt_id2,'')!='' THEN 'Redeemed'
                        WHEN coalesce(t.receipt_id2,'')='' THEN
                        CASE WHEN cast(t.expiry_date as date) >= cast(current_timestamp as date) THEN 'Valid' ELSE 'Expired' END
                END	 as status,
                data.coupon_tnc as tnc
                ,CONCAT('$',t.voucher_amount) as amount,
                CAST(t.expiry_date as date) as expiry_date,
                t.coupon_serialno,
                data.coupon_id as item_id, data.coupon_description as item_name,
				CONCAT('$',t.voucher_amount,' ', data.coupon_description) as item_desc,
				  data.coupon_excerpt as long_desc,
                cast(t.expiry_date as date)  as valid_until
            FROM crm_voucher_list t
            left join crm_voucher_data data on RTRIM(data.coupon_id)=RTRIM(t.coupon_id)
            WHERE RTRIM(t.mbr_id) = ?
                and (t.coupon_id LIKE '!EGIFT%')
                and coalesce(t.voucher_amount,0)>0
        ) a WHERE cast(a.valid_until as date) between '$fdate' and '$tdate'
	) a
	WHERE a.rownum between ? and ? ;

str;
//        echo $sql;

        $gifts = $this->db20->select($sql, [$mbr_id, $page_from, $page_to]);

        for ($i = 0; $i < count($gifts); $i++) {
            $gifts[$i]->long_desc = urldecode($gifts[$i]->long_desc);
            $gifts[$i]->tnc = str_replace("\r\n", '', strip_tags($gifts[$i]->tnc));
        }

        $this->IsPutRedis($redisKey, $gifts, $mbr_id);


        return $gifts;
    }

    public function getMyCredits($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to)
    {
        $redisKey = "#myCredits#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate;
        $transactions_redis = $this->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }


        $sql = <<<str
            	SELECT * FROM
  (
        SELECT ROW_NUMBER() OVER(ORDER BY valid_until desc)rownum,*  FROM (
            SELECT  CASE WHEN coalesce(t.receipt_id2,'')!='' THEN 'Redeemed'
                        WHEN coalesce(t.receipt_id2,'')='' THEN
                        CASE WHEN cast(t.expiry_date as date) >= cast(current_timestamp as date) THEN 'Valid' ELSE 'Expired' END
                END	 as status,
                CASE WHEN coalesce(t.receipt_id2,'')='' THEN '-'
				ELSE t.receipt_id2 END
				 as utilised_invoice
                ,CONCAT('$',t.voucher_amount) as amount,
                CAST(t.expiry_date as date) as expiry_date,
                t.coupon_serialno,
                data.coupon_id as item_id, ' eCredits' as item_name,
				CONCAT('$',t.voucher_amount,' eCredits') as item_desc,
                   CASE WHEN coalesce(data.coupon_description,'')!='' THEN data.coupon_description ELSE data.coupon_excerpt END as long_desc,
                  cast(t.expiry_date as date) as valid_until
            FROM crm_voucher_list t
            left join crm_voucher_data as data on RTRIM(data.coupon_id)= RTRIM(t.coupon_id)
            WHERE RTRIM(t.mbr_id)= ?
                and (RTRIM(t.coupon_id)='!VCH-CREDIT' and t.issue_type ='CREDIT')
                and coalesce(t.voucher_amount,0)>0
        ) a WHERE cast(a.valid_until as date) between '$fdate' and '$tdate'
	) a
	WHERE a.rownum between ? and ?;
str;

//        echo $sql;

        $credits = $this->db20->select($sql, [$mbr_id, $page_from, $page_to]);

        for ($i = 0; $i < count($credits); $i++) {
            $credits[$i]->long_desc = urldecode($credits[$i]->long_desc);
        }

        $this->IsPutRedis($redisKey, $credits, $mbr_id);


        return $credits;
    }

    public function getCouponQuery($mbr_id)
    {
//TODO:: need image and couponrequired ?
        $sql = <<<str
SELECT * from (
select
	RTRIM(data.coupon_id) as coupon_id,
	data.coupon_name as item_desc,
	data.coupon_description as long_desc,
	data.coupon_type,
	CASE
		WHEN list.mbr_id is null THEN 0
		WHEN list.mbr_id is not null AND coalesce(list.receipt_id2,'')='' THEN 1
		WHEN list.mbr_id is not null AND coalesce(list.receipt_id2,'')!='' THEN 2
		END as status,
	data.coupon_tnc as tnc,
	'1' as coupon_code,
	data.coupon_qty as remaining,
	data.created_on,
	to_char(eff_to, 'DD/MM/YYYY')  as expiry_date,
	'' as image
	from  crm_voucher_data data
	LEFT JOIN crm_voucher_list as list on RTRIM(list.mbr_id)= ?
	        and RTRIM(list.coupon_id)= RTRIM(data.coupon_id)
	WHERE data.mobile_show='Y'  and list.mbr_id is null and
	      data.eff_from <= current_timestamp and  data.eff_to >= current_timestamp ) as a
	ORDER by a.status,a.created_on limit 100;
str;

        $data = $this->db20->select($sql, [$mbr_id]);

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['long_desc'] = urldecode($data[$i]['long_desc']);
            $data[$i]['tnc'] = str_replace("&nbsp;", ' ', $data[$i]['tnc']);
        }

        return $data;
    }

    //TODO:: insert grab voucher.
    public function validateAddVoucher($mbr_id, $voucher_id)
    {

        return true;
    }

    public function getAvailablePoints($mbr_id, $coy_id = 'CTL')
    {

        $sql = "SELECT (points_accumulated - points_redeemed - points_reserved - points_expired) AS points_available
                FROM crm_member_list 
                WHERE coy_id = ? AND mbr_id = ? AND status_level = 1 AND exp_date >= current_timestamp limit 1";


        $row = $this->db20->select($sql, [$coy_id, $mbr_id]);

        if ($row) {
            return $row[0]->points_available;
        }

        return 0;
    }

    public function getEmailListings($process_id)
    {
        $cc_process_id = $process_id . '_CC';

        $email_detail = array(
            "email" => '',
            "cc" => '',
            "lists" => [],
            "cc_lists" => []
        );

        $sql = <<<str
	select process_id, remarks from o2o_log_live_processes where process_id in ('$process_id' ,'$cc_process_id') and status= '1';
str;
        $lists = $this->db20->select($sql, []);
        foreach ($lists as $item) {
            $list = $this->stdToArray($item);

            $the_id = $list["process_id"];
            $email = $list["remarks"];
            if ($the_id == $process_id) {
                $email_detail["lists"][] = $email;
            } else {
                $email_detail["cc_lists"][] = $email;
            }
        }

        if (count($email_detail["lists"]) > 0) {
            foreach ($email_detail["lists"] as $index => $email) {

                $email_detail["email"] .= $email;

                if ($index !== count($email_detail["lists"]) - 1) {
                    $email_detail["email"] .= ", ";
                }
            }
        }

        if (count($email_detail["cc_lists"]) > 0) {
            foreach ($email_detail["cc_lists"] as $index => $cc) {

                $email_detail["cc"] .= $cc;

                if ($index !== count($email_detail["cc_lists"]) - 1) {
                    $email_detail["cc"] .= ", ";
                }
            }

        }


        return $email_detail;
    }

    public function getBrandServiceCentres($brand_id)
    {
        if ($brand_id == "HUA WEI") {
            $brand_id = "HUAWEI";
        }

        $redisKey = $this->ENVIRONMENT . "_serviceCenter#";
        $lists = json_decode(Redis::hMget($redisKey, $brand_id)[0], true);

        return $lists;
    }

    public function getMemberBySerialNumber($serial_no, $color_code = 'K')
    {
        $coy_id = $this->CTL_COY_ID;

        $redisKey = $this->ENVIRONMENT . $this->_comp_lists;

        $url = $this->ims_url . "vcapp/getuserbyhpserial";
//        echo $url;

        $post_data = array(
            "serial_num" => $serial_no
        );
//        print_r($post_data);

        $response = $this->curlImsApi($url, $post_data);
//        print_r($response);
//        die();

        $trans_id = $response && isset($response["trans_id"]) ? trim($response["trans_id"]) : '';
        $line_num = $response && isset($response["line_num"]) ? $response["line_num"] : '';

        $item_id = '';
        $item_desc = '';
        $comp_id = '';
        $user = array();

        echo "Trans: " . $trans_id . " , line_num: " . $line_num;

        if ($trans_id && $line_num) {

            $sql_user = <<<str
select
    RTRIM(item_id) as item_id, item_desc, RTRIM(member.mbr_id) as mbr_id,
       email_addr, last_name, first_name, contact_num, exp_date
from crm_member_transaction trans
   inner join crm_member_list member on member.mbr_id= trans.mbr_id
where
  trans.coy_id= ? and trans_id= ? and line_num= ?
str;
            $members = $this->db20->select($sql_user, [$coy_id, $trans_id, $line_num]);
//            print_r($members);
//            echo "member";

            if (count($members) > 0) {
                $user = $this->stdToArray($members[0]);

                $item_id = $user["item_id"];
                $item_desc = $user["item_desc"];

            }

            if ($item_id) {

                $redisData = Redis::get($redisKey);
                if ($redisData) {
                    $item_lists = unserialize($redisData);

                } else {

                    if ($this->ENVIRONMENT == 'development') {
                        $url = "http://chappos-api.sghachi.com/api/report/hpinklist";
                    } else {
                        $url = "https://pos8.api.valueclub.asia/api/report/hpinklist";
                    }

                    $item_lists = $this->curlPosApi($url, [], false);

                    Redis::set($redisKey, serialize($item_lists));
                    Redis::expire($redisKey, 3600);
                }

                foreach ($item_lists as $item_list) {
                    $item_item_id = $item_list["item_id"];
                    $ref_id = $item_list["ref_id"];
                    $item_comp_id = $item_list["comp_id"];

                    if ($item_item_id == $item_id && $ref_id == $color_code) {
                        $comp_id = $item_comp_id;
                    }

                }
            }

        }


        return array(
            "user" => $user,
            "item_id" => $item_id,
            "item_desc" => $item_desc,
            "comp_id" => $comp_id
        );
    }

    public function curlPosApi($url, $post_data = array(), $is_post = true)
    {
        $headers = array(
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if (!$is_post) {
            curl_setopt($ch, CURLOPT_POST, false);
        } else {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

//        print_r($result);
//        die();

        $response = json_decode($result, true);
        curl_close($ch);

        return $response["data"];
    }


    public function sendEmailByCtlMailer($from, $to, $body, $subject, $cc = '', $file_url = '', $csv = '', $file_name = '')
    {
        $url = "https://ctlmailer.api.valueclub.asia/send";
        $key_code = "8f413c39b4d1b4e7c584943df22e7a8c";
        $headers = array(
            'Content-Type: application/json',
            'Authorization: ' . $key_code
        );
        $body = array(
            "from_address" => $from,
            "recipients" => $to,
            "body" => $body,
            "subject" => $subject
        );
        if ($cc != '') {
            $body["copy_recipients"] = $cc;
        }
        if ($file_url) {
            $body["file_url"] = $file_url;
        }

        if ($csv && $file_name) {
            $body["base64_filename"] = $file_name;
            $body["base64_content"] = base64_encode($csv);
        }
//		print_r($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        $result = curl_exec($ch);

        curl_close($ch);
        $result_object = json_decode($result);

//		print_r($result_object);
//      die();

        return $result_object;
    }


}