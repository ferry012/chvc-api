<?php namespace App\Repositories\Member\Transaction;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TransactionRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Transactions\Transaction';
    }

    public function getTransId($mbrId, $fdate, $tdate)
    {
        return $this->model->select('trans_id')->where('mbr_id', $mbrId)
            ->whereBetween('trans_date', [$fdate, $tdate])
            ->whereIn('trans_type',['HI','PS','OS','RF','VC','EX','HR','PU','ID','DZ','DU'])
            ->whereRaw('LEFT(trans_id,3)!=\'NEW\'')
            ->distinct()->get();
    }

    public function getQuery($perPage)
    {
        return $this->model->where(['coy_id' => 'CTL'])->simplePaginate($perPage);
    }

    public function showAdmin($mbrId, $transId, $lineNum)
    {
        if (isset($lineNum)) {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'trans_id'=>$transId, 'line_num' => $lineNum])->get();
        } else {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'trans_id'=>$transId])->get();
        }

    }

    public function deleteAdmin($mbrId, $transId, $lineNum)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'trans_id'=>$transId, 'line_num' => $lineNum])->delete();
    }

    public function createPointsTransaction($now, $mbr_id, $trans_id, $trans_type, $item_desc, $points)
    {
        $this->model->create([
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => $trans_type,
            'trans_date' => $now,
            'loc_id' => '',
            'line_num' => 1,
            'pos_id' => '',
            'item_id' => '',
            'item_desc' => substr($item_desc,0,60),
            'item_qty' => 1,
            'regular_price' => 0,
            'unit_price' => 0,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => $points,
            'salesperson_id' => 'MIS',
            'mbr_savings' => 0,
            'created_by' => 'MIS',
            'created_on' => $now,
            'modified_by' => 'MIS',
            'modified_on' => $now,
            'updated_on' => $now,
        ]);
    }

    public function calculatePoints($mbr_id, $date)
    {
        return $this->model->where('mbr_id', $mbr_id)->where('coy_id','CTL')->where('trans_date', '>', $date)
            ->groupBy('trans_points')
            ->selectRaw('SUM(trans_points) OVER () AS trans_points')
            ->first()->trans_points;
    }

    public function getMemberTransaction($mbrId)
    {
        return $this->model->selectRaw('crm_member_transaction.trans_id, crm_member_transaction.item_id, crm_member_transaction.trans_date, ims_item_list_member.item_type')
            ->leftJoin('ims_item_list_member', function ($q) {
                $q->on('crm_member_transaction.item_id', '=', 'ims_item_list_member.item_id');
            })
            ->where('crm_member_transaction.mbr_id', $mbrId)
            ->where('crm_member_transaction.item_id', 'LIKE', '!MEMBER-%')->orderBy('crm_member_transaction.trans_date', 'desc')
            ->first();
    }

    public function get_mbr_trans($mbrId)
    {
        return $this->model
            ->where('mbr_id', $mbrId)
            ->where('item_id', 'LIKE', '!MEMBER-%')->orderBy('trans_date', 'desc')
            ->first();
    }

    public function getTransactionItem($mbrId, $itemId)
    {
        $from = Carbon::today()->toDateTimeString();
        $to = Carbon::tomorrow()->toDateTimeString();
        $data = $this->model
            ->selectRaw("trans_id,trans_date,item_qty, (case when trans_date>'$from' and trans_date<'$to' then 1 else 0 end) purchase_today")
            ->where(['mbr_id'=>$mbrId,'item_id'=>$itemId])
            ->orderBy('trans_date','desc')->first();
        if (is_null($data)) {
            return 0;
        }
        else {
            return $data;
        }
    }

    public function get_transaction_item($trans_id, $item_id)
    {
        $data = $this->model->where(['coy_id'=>'CTL', 'trans_id'=>$trans_id, 'item_id'=>$item_id])->first();
        return $data;
    }

    public function getTotalNum($mbrId, $itemId)
    {
        $data = $this->model->where(['mbr_id'=>$mbrId,'item_id'=>$itemId])->sum('item_qty');
        return $data;
    }

    public function checkTransSaving($mbrId, $from, $to)
    {
        return $this->model->where('mbr_id', $mbrId)->where('trans_date', '>=', $from)->where('trans_date', '<=', $to)->sum('mbr_savings');
    }

    public function retrieveTrans($mbrId, $from, $to)
    {
        $data = $this->model->where('mbr_id', $mbrId)->where('trans_date', '>=', $from)->where('trans_date', '<=', $to)
                ->whereIn('trans_type', ['HI','PS','IO','RX','HR','DU'])
                ->orderBy('trans_date', 'desc')->orderBy('line_num', 'asc')->get();
        $group = [];
        $n = 0;
        foreach ($data as $k=>$item) {
            $trans_from = in_array(rtrim($item['trans_type']), ['HI', 'HR']) ? 'HACHI' : 'CTL';
            $item_id = rtrim($item['item_id']);
            $unit_price = str_replace( ',', '', $item['unit_price']);
//            $image = Product::where('item_id', $item_id)->select('image_name')->first();
//            $image_name = empty($image) ? '' : $image->image_name;
            if(!in_array(rtrim($item['trans_id']), array_column($group, 'trans_id'))) {
                $group[$n]['trans_id'] = rtrim($item['trans_id']);
                $group[$n]['trans_type'] = rtrim($item['trans_type']);
                $group[$n]['trans_date'] = $item['trans_date'];
                $group[$n]['loc_id'] = rtrim($item['loc_id']);
                $group[$n]['trans_from'] = $trans_from;
                $group[$n]['total_amount'] = number_format($unit_price*$item['item_qty'], 2);
                $group[$n]['total_rebates'] = $item['trans_points'];
                $group[$n]['total_savings'] = number_format($item['mbr_savings'],2);
                $group[$n]['items'] = array([
                    'line_num' => $item['line_num'],
                    'item_id' => $item_id,
                    'item_desc' => $item['item_desc'],
                    'unit_price' => $unit_price,
                    'regular_price' => $item['regular_price'],
                    'item_qty' => $item['item_qty'],
                    'disc_amount' => $item['disc_amount'],
                    'rebates' => $item['trans_points'],
                    'mbr_savings' => $item['mbr_savings'],
//                    'image' => $image_name
                ]);
                $n++;
            } else {
                $existingIndex = array_search(rtrim($item['trans_id']), array_column($group, 'trans_id'));
                $total_amount = str_replace(',', '', $group[$existingIndex]['total_amount']);
                $trans_points = str_replace(',', '', $item['trans_points']);
                $total_rebates = str_replace(',', '', $group[$existingIndex]['total_rebates']);
                $total_savings = str_replace(',', '', $group[$existingIndex]['total_savings']);
                $mbr_savings = str_replace(',', '', $item['mbr_savings']);
                $group[$existingIndex]['total_amount'] = number_format($total_amount + ($unit_price*$item['item_qty']), 2);
                $group[$existingIndex]['total_rebates'] = $total_rebates + $trans_points;
                $group[$existingIndex]['total_savings'] = number_format($total_savings + $mbr_savings, 2);
                $group[$existingIndex]['items'][] = array(
                    'line_num' => $item['line_num'],
                    'item_id' => rtrim($item['item_id']),
                    'item_desc' => $item['item_desc'],
                    'unit_price' => $unit_price,
                    'regular_price' => $item['regular_price'],
                    'item_qty' => $item['item_qty'],
                    'disc_amount' => $item['disc_amount'],
                    'rebates' => $item['trans_points'],
                    'mbr_savings' => $item['mbr_savings'],
//                    'image' => $image_name
                );
            }
        }
        return $group;
    }

    public function get_warranty($trans_id, $item_id)
    {
        return $this->model->where([
            'trans_id' => $trans_id,
            'item_id' => $item_id
        ])->first();
    }

    public function check_transaction($trans_id)
    {
        return $this->model->where(['coy_id'=>'CTL','trans_id'=>$trans_id])->exists();
    }

}