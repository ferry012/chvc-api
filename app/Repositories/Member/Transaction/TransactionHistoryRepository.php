<?php namespace App\Repositories\Member\Transaction;

use Bosnadev\Repositories\Eloquent\Repository;

class TransactionHistoryRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Transactions\TransactionHistory';
    }

    public function getTransId($mbrId, $fdate, $tdate)
    {
        return $this->model->select('trans_id')->where('mbr_id', $mbrId)
            ->whereBetween('trans_date', [$fdate, $tdate])
            ->whereIn('trans_type',['HI','PS','OS','RF','VC','EX','HR','PU','ID','DZ','DU'])
            ->whereRaw('LEFT(trans_id,3)!=\'NEW\'')
            ->distinct()->get();
    }

    public function retrieveTrans($mbrId, $from, $to)
    {
        $data = $this->model->where('mbr_id', $mbrId)->where('trans_date', '>=', $from)->where('trans_date', '<=', $to)
            ->whereIn('trans_type', ['HI','PS','IO','RX','HR','DU'])
            ->orderBy('trans_date', 'desc')->orderBy('line_num', 'asc')->get();
        $group = [];
        $n = 0;
        foreach ($data as $item) {
            $trans_from = in_array(rtrim($item['trans_type']), ['HI', 'HR']) ? 'HACHI' : 'CTL';
            $item_id = rtrim($item['item_id']);
//            $image = Product::where('item_id', $item_id)->select('image_name')->first()->image_name;
//            $image_name = empty($image) ? '' : $image->image_name;
            if(!in_array(rtrim($item['trans_id']), array_column($group, 'trans_id'))) {
                $group[$n]['trans_id'] = rtrim($item['trans_id']);
                $group[$n]['trans_type'] = rtrim($item['trans_type']);
                $group[$n]['trans_date'] = $item['trans_date'];
                $group[$n]['loc_id'] = rtrim($item['loc_id']);
                $group[$n]['trans_from'] = $trans_from;
                $group[$n]['total_amount'] = number_format($item['unit_price']*$item['item_qty'],2);
                $group[$n]['total_rebates'] = $item['trans_points'];
                $group[$n]['total_savings'] = number_format($item['mbr_savings'], 2);
                $group[$n]['items'] = array([
                    'line_num' => $item['line_num'],
                    'item_id' => $item_id,
                    'item_desc' => $item['item_desc'],
                    'unit_price' => $item['unit_price'],
                    'regular_price' => $item['regular_price'],
                    'item_qty' => $item['item_qty'],
                    'disc_amount' => $item['disc_amount'],
                    'rebates' => $item['trans_points'],
                    'mbr_savings' => $item['mbr_savings'],
//                    'image' => $image_name
                ]);
                $n++;
            } else {
                $existingIndex = array_search(rtrim($item['trans_id']), array_column($group, 'trans_id'));
                $group[$existingIndex]['total_amount'] = number_format($group[$existingIndex]['total_amount'] + ($item['unit_price']*$item['item_qty']),2);
                $group[$existingIndex]['total_rebates'] = $group[$existingIndex]['total_rebates'] + $item['trans_points'];
                $group[$existingIndex]['total_savings'] = number_format($group[$existingIndex]['total_savings'] + $item['mbr_savings'], 2);
                $group[$existingIndex]['items'][] = array(
                    'line_num' => $item['line_num'],
                    'item_id' => $item_id,
                    'item_desc' => $item['item_desc'],
                    'unit_price' => $item['unit_price'],
                    'regular_price' => $item['regular_price'],
                    'item_qty' => $item['item_qty'],
                    'disc_amount' => $item['disc_amount'],
                    'rebates' => $item['trans_points'],
                    'mbr_savings' => $item['mbr_savings'],
//                    'image' => $image_name
                );
            }
        }
        return $group;
    }
}