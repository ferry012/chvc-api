<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\DB;

class SysTransListRepository extends Repository {

    public function model() {
        return 'App\Models\SysTransList';
    }

    public function generate_mbr_id($source = '')
    {
        if (strlen($source) < 2) {
            $source = '00';
        } else {
            $source = strlen($source) == 2 ? strtoupper($source) : strtoupper(substr($source, 0,2));
        }
        $now = Carbon::now()->toDateTimeString();
        $year = date('y');
        $coy_id = 'CTL';
        $sys_id = 'CRM';
        $prefix = 'V' . $year;
        $trans_prefix = 'MBR-' . $prefix;
        $is_exists = $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $trans_prefix])->first();
        $next_num = 1;
        if (!$is_exists) {
            $this->model->create(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $trans_prefix,'next_num' => $next_num+1,'sys_date' => $now]);
        }
        $next_num = $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $trans_prefix])->first()->next_num;
        $chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $len_next_num = strlen($next_num);
        $first_next = $len_next_num > 4 ? substr($chars, substr((string)$next_num, 0,$len_next_num - 4)-1, 1) : 0;
        $second_next = sprintf("%04s", substr($next_num, -4));
        $mbr_id = $prefix . $first_next . $second_next . $source;
        $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $trans_prefix])->update(['next_num'=>$next_num+1, 'sys_date'=>$now]);
        return $mbr_id;
    }

    public function generate_contract_id()
    {
        $now = Carbon::now();
        $year = $now->format('y');
        $month = $now->month;
        $prefix = 'CC' . $year . $month;
        $coy_id = 'CTL';
        $sys_id = 'CRM';
        $is_exists = $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix])->first();
        if (is_null($is_exists)) {
            $this->model->create(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix,'next_num' => 1,'sys_date' => $now]);
            $next_num = 1;
        } else {
            $next_num = $is_exists->next_num;
        }
        $contract_id = $prefix . sprintf("%06s", $next_num);
        $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix])->update(['next_num'=>$next_num+1, 'sys_date'=>$now]);
        return $contract_id;
    }

    public function generate_trans_id()
    {
        $now = Carbon::now();
        $year = $now->format('y');
        $month = $now->month;
        $prefix = 'SLS' . $year . $month;
        $coy_id = 'CTL';
        $sys_id = 'CRM';
        $is_exists = $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix])->first();
        if (is_null($is_exists)) {
            $this->model->create(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix,'next_num' => 1,'sys_date' => $now]);
            $next_num = 1;
        } else {
            $next_num = $is_exists->next_num;
        }
        $contract_id = $prefix . sprintf("%05s", $next_num);
        $this->model->where(['coy_id' => $coy_id,'sys_id' => $sys_id,'trans_prefix' => $prefix])->update(['next_num'=>$next_num+1, 'sys_date'=>$now]);
        return $contract_id;
    }

}