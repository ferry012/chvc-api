<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CorporateListRepository extends Repository {

    public function model() {
        return 'App\Models\Member\CorporateList';
    }

    public function get_all_corp()
    {
        return $this->model->where('cust_id', '<>', '')->orderBy('modified_on')->get();
    }

    public function check_cust_exists($cust_id)
    {
        return $this->model->where('cust_id', $cust_id)->exists();
    }

    public function check_mbr_exists($mbr_id)
    {
        $exists = $this->model->where('mbr_id', $mbr_id)->exists();
        if ($exists) {
            return $this->model->where('mbr_id', $mbr_id)->first();
        }
        return false;
    }
}