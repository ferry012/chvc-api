<?php namespace App\Repositories\Member\Redemption;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class RedemptionRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Redemption\Redemption';
    }

    public function getRedemption($min, $max)
    {
        $now = Carbon::now()->toDateTimeString();
        $data = $this->model->where('eff_from', '<', $now)->where('eff_to', '>', $now)
            ->selectRaw("'Bugis Junction (Flagship Store)' as loc_name,image_name as img_name,CONCAT('-',discount_amt) as discount_value,cast(points_required/100 as decimal(10,2)) as redeem_price,*")
            ->where('loc_id', 'BF')
            ->where('points_required', '>', $min)
            ->where('points_required', '<', $max)
            ->orderBy('item_desc')
            ->get();
        return $data;
    }

    public function check_redemption($item_id, $points_required)
    {
        return $this->model->where([
            'item_id'=>$item_id,
            'points_required'=>$points_required,
            'loc_id'=>'BF'
        ])->exists();
    }
}