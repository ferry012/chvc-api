<?php namespace App\Repositories\Member\O2o;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class MemberList2 extends Repository {

    public function model() {
        return 'App\Models\Member\O2o\MemberList2';
    }

    public function getCorporateProfile($mbrId)
    {
        return $this->model->where('mbr_id', $mbrId)->first();
    }

}