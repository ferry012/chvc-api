<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemberTypeRepository extends Repository {

    public function model() {
        return 'App\Models\Member\MemberType';
    }

    public function getExpiryDate($mbrId, $now)
    {
        $data = $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->where('eff_from', '<=', $now)
            ->where('eff_to', '>=', $now)->first();
        return $data;
    }

    public function updateEffTo($mbrId, $arr, $mbrType, $yesterday)
    {
        $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->where('eff_from', '<=', $yesterday)
            ->where('eff_to', '>=', $yesterday)
            ->where('mbr_type', '<>', $mbrType)->update($arr);
    }

    public function refundRebate($mbrId, $mbrType, $member_of_duration, $yesterday, $arr)
    {
        $member_type = $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->where('eff_from', '<=', $yesterday)
            ->where('eff_to', '>=', $yesterday)
            ->where('mbr_type', '!=', $mbrType)->first();

        if (!$member_type)
            return 0;

        $eff_to = new Carbon($member_type->eff_to);
        $points = number_format(($eff_to->startOfMonth()->diffInMonths($yesterday->addDay()->endOfMonth())) * 100, 2,'.', '');

        if (Carbon::parse($member_type->eff_from)->isSameDay(Carbon::now()) ) {
            $points = number_format($member_of_duration * 100, 2, '.', '');
        }
        $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'line_num' => $member_type->line_num])
            ->update($arr);
        return $points;
    }

    public function returnMember($mbrId)
    {
        $mbr_type = $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->orderBy('line_num', 'desc')->first();
        $line_num = $mbr_type->line_num;
        $trans_date = $mbr_type->created_on;
        $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'line_num'=>$line_num])->delete();
        return array($line_num, $trans_date);
    }

    public function getPosMemberTypeHistory($mbrId)
    {
        $mbr_type_his = $this->model->select('line_num', 'eff_from', 'eff_to', DB::raw("rtrim(mbr_type) as mbr_type"))->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->orderBy('line_num', 'desc')->get();
        return $mbr_type_his;
    }

    public function getPosMemberType($mbrId)
    {
        $now = Carbon::now();
        $mbr_type = $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])
            ->where('eff_from', '<=', $now)
            ->where('eff_to', '>=', $now)
            ->first();

        if (empty($mbr_type)) {
            $result = '';
        }
        else {
            $result = rtrim($mbr_type->mbr_type, " ");
        }
        return $result;
    }

    public function update_ass_mbr($mbr_id, $exp_date, $now)
    {
        $this->model->where('mbr_id', $mbr_id)->where('eff_from', '<', $now)->where('eff_to', '>', $now)
            ->update(['eff_to'=>$exp_date, 'modified_on'=>$now]);
    }

    public function update_eff_to($mbr_id, $line_num, $now)
    {
        $this->model->where(['mbr_id'=>$mbr_id,'line_num'=>$line_num])
            ->update([
                'eff_to' => Carbon::yesterday()->endOfDay()->toDateTimeString(),
                'modified_by' => 'CONVERT',
                'modified_on' => $now,
            ]);
    }

    public function update_member_type($mbr_id, $old_mbr_type, $new_mbr_type, $user)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where([
            'mbr_id' => $mbr_id,
            'mbr_type' => $old_mbr_type
        ])->where('eff_from', '<', $now)->where('eff_to', '>', $now)->update([
            'mbr_type' => $new_mbr_type,
            'modified_on' => $now,
            'modified_by' => $user
        ]);
    }

    public function update_member_exp_date($mbr_id, $user, $exp_date)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where('mbr_id', $mbr_id)->where('eff_from', '<', $now)->where('eff_to', '>', $now)->update([
            'eff_to' => $exp_date,
            'modified_on' => $now,
            'modified_by' => $user
            ]);
    }

    public function update_corporate_associate_type($mbr_id, $exp_date, $status_level, $user)
    {
        $now = Carbon::now()->toDateTimeString();

    }

}