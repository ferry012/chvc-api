<?php namespace App\Repositories\Member\Voucher;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VoucherRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Voucher\Voucher';
    }

    public function isVoucherExist($mbrId, $couponId)
    {
        return $this->model->where(['mbr_id' => $mbrId, 'coupon_id' => $couponId])->count();
    }

    public function getQuery($perPage)
    {
        return $this->model->where(['coy_id' => 'CTL'])->simplePaginate($perPage);
    }

    public function showAdmin($mbrId, $couponId)
    {
        if (isset($couponId)) {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'coupon_id' => $couponId])->get();
        } else {
            return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])->get();
        }

    }

    public function deleteAdmin($mbrId, $couponId)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'coupon_id' => $couponId])->delete();
    }

    public function getVoucher($mbrId, $statusLevel)
    {
        if ($statusLevel === null) {
            $data = $this->model
                ->select('coy_id', DB::raw("REPLACE(coupon_id, ' ', '') as coupon_id"), 'coupon_serialno', 'promocart_id', 'mbr_id', 'trans_id', 'trans_date',
                    'ho_ref', 'ho_date', 'issue_date', 'sale_date', 'utilize_date', 'expiry_date', 'print_date',
                    DB::raw("REPLACE(loc_id, ' ', '') as loc_id, REPLACE(receipt_id1, ' ', '') as receipt_id1"), 'voucher_amount', 'redeemed_amount', 'expired_amount',
                    'issue_type', 'issue_reason', 'status_level', DB::raw("REPLACE(created_by, ' ', '') as created_by, REPLACE(modified_by, ' ', '') as modified_by"),
                    'created_on', 'modified_on', DB::raw("CAST(guid AS VARCHAR(36)) AS guid"))
                ->where('coy_id', 'CTL')->whereRaw('LOWER(mbr_id)=?', trim(strtolower($mbrId)))->get();
        } else {
            $data = $this->model->where(['coy_id' => 'CTL', 'status_level' => (int)$statusLevel])->whereRaw('LOWER(mbr_id)=?', trim(strtolower($mbrId)))->get();
        }
        return $data;
    }

    public function updateMemberVoucher($mbrId, $couponId, $expiryDate)
    {
        return $this->model->where(['mbr_id'=>$mbrId, 'coupon_id'=>$couponId])->update(['expiry_date'=>$expiryDate]);
    }

    public function editVoucher($mbrId, $couponId)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId, 'coupon_id' => $couponId])->first();
    }

    /**
     * @param $mbrId
     * @return mixed
     * SELECT ct.*, isnull(cl.coupon_name,'') as coupon_name,  isnull(iml.item_desc,'') as coupon_desc,
     */
    public function hachiVoucher($mbrId)
    {
//        dd($this->model);
        return $this->model->select('expiry_date', 'status_level', DB::raw("rtrim(coupon_id) as coupon_id"), 'coupon_serialno',
                                    DB::raw("coalesce((select coupon_name from crm_voucher_data where coupon_id = crm_voucher_list.coupon_id), '') coupon_name"),
                                    DB::raw("case when (status_level = '1') then '3'
                                    when expiry_date  < now() then '4'
                                    when status_level = '0'  then '1' 
                                    end as seq_num"), 'voucher_amount', 'issue_date')
                            ->where('mbr_id', $mbrId)
                            ->where('status_level', '>=', 0)
//                            ->whereRaw("coalesce(issue_type, '') NOT IN ('GV', 'CREDIT')")
                            ->whereRaw("coalesce(coupon_id, '') NOT IN (select rtrim(item_id) from ims_item_list where item_type in ('C','E','V'))")
                            ->orderBy('seq_num', 'ASC')
                            ->orderBy('expiry_date', 'ASC')->get();
    }

    public function hachiRewards($mbrId, $serialNo)
    {
        return $this->model->select('coupon_serialno', 'issue_date', 'expiry_date',
            DB::raw("coalesce((select coupon_name from crm_voucher_data where coupon_id = crm_voucher_list.coupon_id), '') coupon_name"),
            DB::raw("coalesce((select coupon_description from crm_voucher_data where coupon_id = crm_voucher_list.coupon_id), '') coupon_desc")
//            DB::raw("coalesce((select image_url from crm_voucher_data where coupon_id = crm_voucher_list.coupon_id), '') image_url")
            )
            ->where('mbr_id', $mbrId)
            ->where('coupon_serialno', $serialNo)
            ->first();
    }

//    public function hachiECredits($mbrId)
//    {
//        return $this->model->select('expiry_date', 'status_level', 'coupon_serialno', DB::raw("rtrim(receipt_id1) receipt_id1, rtrim(receipt_id2) receipt_id2"), 'voucher_amount', 'issue_date',
//            'utilize_date'
//            )
//            ->where('mbr_id', $mbrId)
//            ->where('status_level', '>=', 0)
//            ->where('issue_type', 'CREDIT')
//            ->orderBy('expiry_date', 'DESC')->get();
//    }

    public function hachiEGifts($mbrId)
    {
        return $this->model->select(DB::raw("coalesce((select image_url from crm_voucher_data where coupon_id = crm_voucher_list.coupon_id), '') image"),
            'expiry_date', 'voucher_amount', 'status_level')
            ->where('mbr_id', $mbrId)
            ->where('status_level', '>=', 0)
            ->where('issue_type', 'GV')
            ->orderBy('expiry_date', 'DESC')->get();
    }

    public function checkGiftExists($couponNo)
    {
        return $this->model->where([
            'coy_id' => 'CTL',
//            'mbr_id' => $mbrId,
            'coupon_serialno' => $couponNo
        ])->exists();
    }

    public function updateGift($couponNo, $transId)
    {
        $now = Carbon::now()->toDateTimeString();
        return $this->model->where([
            'coy_id' => 'CTL',
//            'mbr_id' => $mbrId,
            'coupon_serialno' => $couponNo
        ])->update([
            'receipt_id2' => $transId,
            'status_level' => 1,
            'utilize_date' => $now,
            'modified_on' => $now,
            'modified_by' => 'hachi'
        ]);
    }

    public function hasOwner($couponNo, $coyId)
    {
        $mbr_id = $this->model->where(['coupon_serialno'=>$couponNo, 'coy_id'=>$coyId])->first()->mbr_id;
        if (empty(rtrim($mbr_id))) {
            return false;
        } else {
            return true;
        }
    }

    public function updateOwner($couponNo, $coyId, $mbrId)
    {
        $this->model->where(['coupon_serialno'=>$couponNo, 'coy_id'=>$coyId])->update(['mbr_id'=>$mbrId]);
    }

    public function check_by_serial($serial)
    {
        $exists = $this->model->where('coupon_serialno', $serial)->exists();
        if ($exists) {
            return $this->model->where('coupon_serialno', $serial)->first();
        }
        return false;
    }

    public function update_egift($serial, $expiry_date, $status_level, $update_by)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where(['coupon_serialno'=>$serial])->update([
            'expiry_date' => $expiry_date,
            'status_level' => $status_level,
            'modified_by' => $update_by,
            'modified_on' => $now
        ]);
    }

    public function is_exists($serial, $mbr_id)
    {
        $now = Carbon::now()->toDateTimeString();
        $voucher = $this->model->where(['coy_id'=>'CTL', 'coupon_serialno'=>$serial, 'status_level'=>0])->where('expiry_date', '>=', $now)
                    ->whereRaw("rtrim(mbr_id)=''");
        if ($voucher->exists()) {
            $voucher->update([
                'mbr_id' => $mbr_id,
                'modified_on' => $now,
                'modified_by' => 'hachi'
            ]);
            $data = $this->model->where(['coy_id'=>'CTL', 'coupon_serialno'=>$serial])->first();
            return $data;
        }
        return false;
    }

    public function save_credit($serial, $trans_id, $coupon_id)
    {
        $ret = Carbon::now()->toDateTimeString();
        $credit = $this->model->where(['coy_id'=>'CTL', 'coupon_serialno'=>$serial, 'status_level'=>0])->whereRaw("rtrim(coupon_id) = '$coupon_id'");
        if ($credit->exists()) {

            $voucher = $credit->select('mbr_id','coupon_id','coupon_serialno','expiry_date','voucher_amount')->get();

            $credit->update([
                'receipt_id2' => $trans_id,
                'utilize_date' => $ret,
                'status_level' => 1,
                'modified_by' => 'POS',
                'modified_on' => $ret
            ]);

            return $voucher;
        } else {
            return false;
        }

    }

}