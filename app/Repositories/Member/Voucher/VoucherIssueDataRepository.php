<?php namespace App\Repositories\Member\Voucher;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VoucherIssueDataRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Voucher\VoucherIssueData';
    }

    public function getCouponId($mbrType, $now)
    {
        return $this->model->where(['mbr_type' => $mbrType,'partner_id'=>'welcome-coupon'])->where('eff_from', '<=', $now)->where('eff_to', '>=', $now)->selectRaw('rtrim(coupon_id) coupon_id, days_to_expiry')->get();
    }

    public function getPartnerCoupon($couponId, $partnerId)
    {
        $now = Carbon::now()->toDateTimeString();
        $coupon = $this->model->where(['coupon_id'=>$couponId, 'partner_id' => $partnerId])
                    ->where('eff_from', '<', $now)
                    ->where('eff_to', '>=', $now);
        if ($coupon->exists()) {
            return $coupon->first();
        } else {
            return 0;
        }
    }

    public function checkCouponExists($mbr_type, $coupon_id)
    {
        return $this->model->where(['mbr_type'=>$mbr_type, 'coupon_id'=>$coupon_id])->exists();
    }
}