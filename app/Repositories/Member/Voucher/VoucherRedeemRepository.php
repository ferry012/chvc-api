<?php namespace App\Repositories\Member\Voucher;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VoucherRedeemRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Voucher\VoucherRedeem';
    }

    public function is_exist($sn)
    {
        return $this->model->where('voucher_serialno', $sn)->exists();
    }

    public function getRedeem($sn)
    {
        $now = Carbon::now()->toDateTimeString();
        return $this->model->where('voucher_serialno', $sn)->where('eff_from', '<', $now)->where('eff_to', '>', $now)
                ->whereRaw(DB::raw("rtrim(redeemed_by) = ''"));
    }

    public function updateRedeem($sn, $mbrId)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where('voucher_serialno', $sn)->update(['redeemed_by'=>$mbrId, 'redeemed_on'=>$now]);
    }
}