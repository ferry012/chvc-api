<?php namespace App\Repositories\Member\Voucher;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EgiftRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Voucher\Egift';
    }

    public function check_by_serial($serial, $email, $claim_by)
    {
        $exists = $this->model->where(['coupon_serialno'=>$serial, 'recipient_email'=>$email])->exists();
        if ($exists) {
            $egift = $this->model->where(['coupon_serialno'=>$serial, 'recipient_email'=>$email])->first();
            if (Carbon::parse($egift->claim_expiry)->lt(Carbon::now())) {
                return 1;
            }
            if ($egift->status_level==2) {
                return 2;
            }
            $now = Carbon::now()->toDateTimeString();
            $this->model->where('coupon_serialno', $serial)->update([
                'status_level' => 2,
                'claim_by' => $claim_by,
                'claim_on' => $now,
                'modified_by' => $claim_by,
                'modified_on' => $now
            ]);
            return 3;
        } else {
            return 0;
        }
    }

    public function check_exist($trans_id)
    {
        $exists = $this->model->where('trans_id',$trans_id)->exists();
        if ($exists) {
            return $this->model->where('trans_id', $trans_id)->first();
        }
        return false;
    }

    public function check_serial_exist($serial)
    {
        $exists = $this->model->where('coupon_serialno', $serial)->exists();
        if ($exists) {
            return $this->model->where('coupon_serialno', $serial)->first();
        }
        return false;
    }

    public function update_egift($serial, $expiry_date, $status_level, $update_by)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where(['coupon_serialno'=>$serial])->update([
            'claim_expiry' => $expiry_date,
            'status_level' => $status_level,
            'modified_by' => $update_by,
            'modified_on' => $now
        ]);
    }

}