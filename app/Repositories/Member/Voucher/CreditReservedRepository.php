<?php namespace App\Repositories\Member\Voucher;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreditReservedRepository extends Repository {

    public function model() {
        return 'App\Models\Member\Voucher\CreditReserved';
    }

    public function isExists($trans_id)
    {
        return $this->model->where([
            'trans_id' => $trans_id,
        ])->exists();
    }

    public function updateOrSave($type, $data)
    {
        if ($type === 'update') {
            $res = $this->model->where([
                'trans_id' => $data['trans_id'],
            ])->update($data);
        } elseif ($type === 'save') {
            $res = $this->model->insert($data);
        }
        return $res;
    }

    public function unreserve($trans_id)
    {
        $is_exists = $this->model->where('trans_id', $trans_id)->exists();
        if ($is_exists) {
            $this->model->where('trans_id', $trans_id)->update([
                'status' => 0,
                'updated_at' => Carbon::now()
            ]);
//            return $res;
        }
//        return false;
    }

    public function unreserve_hachi($trans_id)
    {
        $is_exists = $this->model->where('trans_id', $trans_id)->exists();
        if ($is_exists) {
            $res = $this->model->where('trans_id', $trans_id)->update([
                'status' => 0,
                'updated_at' => Carbon::now()
            ]);
            return $res;
        }
        return false;
    }
}