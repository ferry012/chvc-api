<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemberRepository extends Repository
{
    public function model()
    {
        return 'App\Models\User';
    }

    public function getMemberShip()
    {
        $data = $this->model->slectRaw('rtrim(a.coy_id) as coy_id, rtrim(a.mbr_id) as mbr_id, rtrim(a.first_name) as first_name, rtrim(a.last_name) as last_name,
			        rtrim(a.nationality_id) as nationality_id, rtrim(a.email_addr) as email_addr, rtrim(a.contact_num) as contact_num, a.last_login,
                     a.join_date, a.exp_date, a.last_renewal, rtrim(a.points_accumulated) as points_accumulated,
                    rtrim(a.points_reserved) as points_reserved, rtrim(a.points_redeemed) as points_redeemed, rtrim(a.points_expired) as points_expired, rtrim(a.mbr_savings) as mbr_savings, rtrim(a.rebate_voucher) as rebate_voucher,
                    rtrim(a.mbr_type) as mbr_type, rtrim(a.main_id) as main_id, rtrim(a.sub_date) as sub_date, rtrim(a.mbr_token) as mbr_token, rtrim(a.salesperson_id) as salesperson_id,
			        rtrim(a.created_by) as created_by, a.created_on, rtrim(a.modified_by) as modified_by, a.modified_on, a.updated_on,
			        CASE WHEN login_locked IS NULL or rtrim(login_locked)=\'\' THEN \'N\' ELSE login_locked END AS login_locked, ROW_NUMBER() OVER (ORDER BY mbr_id)
                    AS RowNum')->where('coy_id', 'CTL')->get();
        return $data;
    }

    public function getMemberInfo($memberId)
    {
        $data = $this->model->select('mbr_id', 'mbr_title', 'first_name', 'last_name', 'birth_date', 'email_addr', 'contact_num', 'join_date', 'exp_date', 'mbr_type', 'mbr_addr', 'delv_addr', 'login_locked', 'main_id',
            'points_accumulated', 'points_reserved', 'points_redeemed', 'points_expired', 'status_level', 'mbr_savings', DB::raw("rtrim('rebate_voucher') as rebate_voucher"),
//                                        DB::raw("(SELECT day_can_renew FROM ims_item_list_member WHERE item_type = mbr_type limit 1) AS day_can_renew"
            DB::raw("(SELECT day_can_renew FROM ims_item_list_member WHERE item_type = mbr_type limit 1) AS day_can_renew"),
            DB::raw("(CASE WHEN LEFT(mbr_type, 1) = 'M' THEN (SELECT rebate_tier FROM ims_item_list_member WHERE item_type = mbr_type limit 1)
                                                       WHEN LEFT(mbr_type, 1) = 'S' THEN (SELECT rebate_tier FROM ims_item_list_member WHERE item_type = 'S' limit 1)
                                                       ELSE 0 END) AS rebate_tier"),
            DB::raw("(CASE WHEN LEFT(mbr_type, 1) = 'M' THEN (SELECT month_of_duration FROM ims_item_list_member WHERE item_type = mbr_type limit 1)
               WHEN LEFT(mbr_type, 1) = 'S' THEN (SELECT month_of_duration FROM ims_item_list_member WHERE item_type = 'S' limit 1)
               ELSE 0 END) AS mbr_month"),
            DB::raw("(SELECT can_upgrade FROM ims_item_list_member WHERE item_type = mbr_type limit 1) AS can_upgrade"),
            'last_renewal'
//                                        DB::raw("(CASE
//                                        WHEN mbr_type='M08' and (exp_date - INTERVAL '1 DAY'*60)>NOW() THEN 'N'
//                                        WHEN mbr_type='M18' and (exp_date - INTERVAL '1 DAY'*120)>NOW() THEN 'N'
//                                        WHEN mbr_type='M28' and (exp_date - INTERVAL '1 DAY'*180)>NOW() THEN 'N'
//                                        ELSE 'Y'
//                                        END) AS can_renew")
        )
            ->selectRaw('COALESCE(mbr_pwd2, mbr_pwd) as mbr_pwd')
            ->orWhereRaw('LOWER(mbr_id)=?', trim(strtolower($memberId)))->orWhereRaw('LOWER(contact_num)=?', trim(strtolower($memberId)))->orWhereRaw('LOWER(email_addr)=?', trim(strtolower($memberId)))->first();
//            ->orWhere('mbr_id', $memberId)->orWhere('contact_num', $memberId)->orWhere('email_addr', $memberId)->orderBy('status_level', 'desc')->first();
        $exp_date = new Carbon($data['exp_date']);
        if ($data) {
            if ($data['day_can_renew'] === null) {
                $data['can_renew'] = 'Y';
            } else {
                if ($exp_date->subDays($data['day_can_renew']) > Carbon::now()) {
                    $data['can_renew'] = 'N';
                } else {
                    $data['can_renew'] = 'Y';
                }
            }
        } else {
            $data = [];
        }

        return $data;
    }



    public function convert_staff($mbrId, $staffId, $eff_to)
    {
        $is_exists = $this->model->where('mbr_type', 'S' . $staffId)->where('status_level', 1);
        if ($is_exists->exists()) {
            $is_exists->update([
                'status_level' => -1
            ]);
        }
        $member = $this->model->orWhere('mbr_id', $mbrId)->orWhere('contact_num', $mbrId)->orWhere('email_addr', $mbrId);
        $res = $member->update([
            'status_level' => 1,
            'mbr_type' => 'S' . $staffId,
            'exp_date' => $eff_to,
            'modified_by' => 'CONVERT',
            'modified_on' => Carbon::now()->toDateTimeString(),
        ]);
        if ($res) {
            $memberId = $member->first();
            return $memberId;
        } else {
            return '';
        }
    }

    public function setExpired($from, $to)
    {
        $res = $this->model->where('exp_date', '<', $to)->where('exp_date', '>=', $from)
            ->where(['status_level' => 1, 'coy_id' => 'CTL'])
            ->update(['status_level' => -1]);
        return $res;
    }

    public function getExpired($from, $to)
    {
        $res = $this->model->where('exp_date', '<', $to)->where('exp_date', '>=', $from)
            ->get();
        return $res;
    }

    public function deleteNoPay($from, $to)
    {
        $res = $this->model->where('status_level', -9)
            ->where('created_on', '>=', $from)
            ->where('created_on', '<', $to);
        if ($res->exists()) {
            $res->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function setSavingsZero($mbr_id)
    {
        $this->model->where('mbr_id', $mbr_id)->update([
            'mbr_savings' => 0
        ]);
    }

    public function addExpiredPoints($mbr_id, $points)
    {
        $this->model->where('mbr_id', $mbr_id)->increment('points_expired', $points);
    }

    public function reducePoints($mbr_id, $points_accumulated, $points_redeemed, $points_expired, $mbr_savings)
    {
        $this->model->where('mbr_id', $mbr_id)->update([
            'points_accumulated' => $points_accumulated,
            'points_redeemed' => $points_redeemed,
            'points_expired' => $points_expired,
            'mbr_savings' => $mbr_savings
        ]);
    }

    public function checkEmailUnique($mbr_id, $email_addr)
    {
        $res = $this->model->where('mbr_id', '<>', $mbr_id)->where('email_addr', $email_addr)->exists();
        return $res;
    }

    public function check_email_unique($email)
    {
        return $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email])->exists();
    }

    public function checkPhoneUnique($mbr_id, $contact_num)
    {
        $res = $this->model->where('mbr_id', '<>', $mbr_id)->where('contact_num', $contact_num)->exists();
        return $res;
    }

    public function check_contact_unique($contact)
    {
        return $this->model->where(['coy_id' => 'CTL', 'contact_num' => $contact])->exists();
    }

    public function getMemberDetail($memberId)
    {
        $data = $this->model->select('join_date', 'exp_date', 'mbr_type', 'main_id',
            'points_accumulated', 'points_reserved', 'points_redeemed', 'points_expired', 'status_level', 'mbr_savings',
            DB::raw("(SELECT day_can_renew FROM ims_item_list_member WHERE item_type = mbr_type limit 1) AS day_can_renew"),
            DB::raw("(CASE WHEN LEFT(mbr_type, 1) = 'M' THEN (SELECT rebate_tier FROM ims_item_list_member WHERE item_type = mbr_type limit 1)
               WHEN LEFT(mbr_type, 1) = 'S' THEN (SELECT rebate_tier FROM ims_item_list_member WHERE item_type = 'S' limit 1)
               ELSE 0 END) AS mbr_rebate_times"),
            DB::raw("(CASE WHEN LEFT(mbr_type, 1) = 'M' THEN (SELECT month_of_duration FROM ims_item_list_member WHERE item_type = mbr_type limit 1)
               WHEN LEFT(mbr_type, 1) = 'S' THEN (SELECT month_of_duration FROM ims_item_list_member WHERE item_type = 'S' limit 1)
               ELSE 0 END) AS mbr_month"),
            DB::raw("(SELECT can_upgrade FROM ims_item_list_member WHERE item_type = mbr_type limit 1) AS can_upgrade")
        )
            ->orWhereRaw('LOWER(mbr_id)=?', trim(strtolower($memberId)))->orWhereRaw('LOWER(contact_num)=?', trim(strtolower($memberId)))->orWhereRaw('LOWER(email_addr)=?', trim(strtolower($memberId)))->first();
//            ->Where('mbr_id', $memberId)->first();
        $exp_date = new Carbon($data['exp_date']);
        if ($data) {
            if ($data['day_can_renew'] === null) {
                $data['can_renew'] = 'Y';
            } else {
                if ($exp_date->subDay($data['day_can_renew']) > Carbon::now()) {
                    $data['can_renew'] = 'N';
                } else {
                    $data['can_renew'] = 'Y';
                }
            }
        } else {
            $data = [];
        }

        return $data;
    }

    public function findByMbrId($memberId)
    {
        $data = $this->model->whereRaw('LOWER(mbr_id)=?', trim(strtolower($memberId)))->first();
        return $data;
    }

    public function has_associate($mbr_id)
    {
        $res = $this->model->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])->exists();
        return $res;
    }

    public function get_associate($mbr_id)
    {
        return $this->model->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])->get();
    }


    public function getMemberPoints($mbr_id)
    {
        $coy_id = 'CTL';
        $now = date("Y-m-d H:i:s A");

        $vpointData = $this->model->where(['coy_id' => $coy_id, 'mbr_id' => $mbr_id, 'status_level' => 1])
            ->select('points_accumulated', 'points_redeemed', 'points_reserved', 'points_expired',
                DB::raw("(points_accumulated - points_redeemed - points_reserved - points_expired) as points_available")
            )
            ->first();

//        DB::enableQueryLog();
//        print_r(DB::getQueryLog());

        return $vpointData;
    }

    public function set_associate_expiry($mbr_id, $exp_date, $now)
    {
        $this->model->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])
            ->update(['exp_date' => $exp_date, 'last_renewal' => $now, 'status_level' => -1, 'main_id' => '', 'updated_on' => $now]);
    }

    public function update_associate($mbr_id, $exp_date, $now)
    {
        $this->model->where(['mbr_type' => 'MAS', 'main_id' => $mbr_id])
            ->update(['exp_date' => $exp_date, 'last_renewal' => $now, 'updated_on' => $now]);
    }

    public function delete_same_member($email, $contact)
    {
        $is_exists = $this->model->where(['email_addr' => $email, 'contact_num' => $contact, 'status_level' => -9])->exists();
        if ($is_exists) {
            $this->model->where(['email_addr' => $email, 'contact_num' => $contact, 'status_level' => -9])->delete();
        }
    }

    public function set_member_expiry($mbr_id)
    {
        $now = Carbon::now();
        $this->model->where('mbr_id', $mbr_id)
            ->update([
                'status_level' => -1,
                'modified_by' => 'HACHI',
                'modified_on' => $now
            ]);
    }

    public function valid_member($mbr_id)
    {
        $member = $this->model->where(['mbr_id' => $mbr_id, 'coy_id' => 'CTL', 'status_level' => 1])->first();
        return $member;
    }

    public function check_member_exists($email, $contact)
    {
//        $is_exists = $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact])->exists();
        $is_exists = $this->model->where('coy_id','CTL')
            ->where(function ($query) use ($email, $contact) {
            $query->where('email_addr', $email)
                ->orwhere('contact_num', $contact);
        })->exists();
        if ($is_exists) {
            return $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact])->first();
        } else {
            return false;
        }
    }

    public function delete_pre_mbr($email, $contact)
    {
        $member = $this->model->where(['coy_id'=>'CTL'])->where(function ($q) use($email, $contact) {
            $q->where('email_addr', $email)->orWhere('contact_num', $contact);
        });
        if ($member->exists()) {
            $mbr = $member->first();
            if ($mbr->status_level == -9) {
                $member->delete();
            } else {
                if ($mbr['email_addr'] == $email) {
                    return 5;
                } elseif ($mbr['contact_num']) {
                    return 6;
                }
            }
        }
        return false;
    }

    public function count_associate($mbr_id)
    {
        return $this->model->where(['coy_id' => 'CTL', 'main_id' => $mbr_id, 'mbr_type' => 'MAS'])->count();
    }

    public function check_associate_member_exists($email, $contact)
    {
        $is_exists = $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact, 'status_level' => 1])->exists();
        if ($is_exists) {
            return 3;
        } else {
            $not_activate = $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact, 'status_level' => 0])->exists();
            if ($not_activate) {
                return 2;
            } else {
                return '';
            }
        }
    }

    public function check_corporate_member($mbr_id)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'mbr_type' => 'CORP'])->exists();
    }

    public function findByEmailAndContact($email, $contact)
    {
        return $this->model->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact])->first();
    }

    public function get_corporate_associate($main_id)
    {
        return $this->model->where(['coy_id' => 'CTL', 'main_id' => $main_id])->select(DB::raw("rtrim(mbr_id) mbr_id"), 'first_name', 'last_name',
            'email_addr', 'contact_num', DB::raw("rtrim(mbr_type) mbr_type"), DB::raw("case when status_level <> 1 then 0 else 1 end as status_level"))->get();
    }

    public function update_corporate_associate($main_id, $exp_date, $status_level, $user)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->model->where(['coy_id' => 'CTL', 'main_id' => $main_id])
            ->update(['exp_date' => $exp_date, 'last_renewal' => $now, 'status_level' => $status_level, 'modified_on' => $now, 'modified_by' => $user]);
        return $this->model->where(['coy_id' => 'CTL', 'main_id' => $main_id])->get();
    }

    public function check_by_mbr_id($mbr_id)
    {
        return $this->model->where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id])->exists();
    }

    public function chckUpdateMbrPassword2($mbrId, $pwd2)
    {
        $hashedPassword2 = $pwd2;//bin2hex(openssl_digest(trim($pwd2), 'sha256',true));
        $member = $this->model
        ->orWhere('mbr_id', $mbrId)->orWhere('contact_num', $mbrId)->orWhere('email_addr', $mbrId)
        ->whereNull('mbr_pwd2')
        ->update([
            'mbr_pwd2' => $hashedPassword2
        ]);      
        
        return $member;
    }
}