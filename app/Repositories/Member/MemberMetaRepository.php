<?php namespace App\Repositories\Member;

use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemberMetaRepository extends Repository {

    public function model() {
        return 'App\Models\Member\MemberMeta';
    }

    public function getRenewRemainder($mbr_id)
    {
        $res = $this->model->where('mbr_id',$mbr_id)->get();

        $meta = [];

        foreach ($res as $v) {
            $meta[$v->meta_key] = $v->meta_value;
        }

        return (object)$meta;
    }

    public function updateOrInsertReminder($mbr_id, $v, $user_id)
    {
        $now = Carbon::now()->toDateTimeString();
        $reminder = $this->model->where(['mbr_id'=>$mbr_id, 'meta_key'=>'posreminder']);
        if ($reminder->exists()) {
            $res = $reminder->update([
                'meta_value'=>$v,
                'modified_by'=>$user_id,
                'modified_on'=>$now
            ]);
        } else {
            $res = $this->model->create([
                'mbr_id'=>$mbr_id,
                'meta_key'=>'posreminder',
                'meta_value'=>$v,
                'created_by'=>$user_id,
                'created_on'=>$now,
                'modified_by'=>$user_id,
                'modified_on'=>$now
            ]);
        }
        return $res;
    }

    public function getRemainder($mbr_id)
    {
        $data = $this->model->where(['mbr_id'=>$mbr_id, 'meta_key'=>'posreminder'])->first();
        if ($data) {
            $reminder = $data->meta_value;
        } else {
            $reminder = '';
        }
        return $reminder;
    }

}