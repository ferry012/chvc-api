<?php

namespace App\Listeners;

use App\Events\UserLogin;
use App\Models\Log\LogLogin;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WriteLoginEventToLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        //
//        $log = new LogLogin();
        $userLog = new User();

        $userLog->updateLogin($event->id, $event->email);
//        $log->saveLoginLog($event->id, $event->email);
    }
}
