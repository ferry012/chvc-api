<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class Role1
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$scopes)
    {
        if (empty(request()->key_code) && empty(request()->header('Authorization'))) {
            return response()->json([
                'status' => 'error',
                'status_code' => 403,
                'message' => 'No Auth Token'
            ], 403);
        }
        else {
            if (!empty(request()->header('Authorization'))) {
                $jwt = explode(" ", request()->header('Authorization'));
                if ($jwt[0] !== 'Bearer'){
                    return response()->json([
                        'status' => 'error',
                        'status_code' => 403,
                        'message' => 'Auth header invalid'
                    ], 403);
                }
                $jwt = $jwt[1];
            }
            if (!empty(request()->key_code)) {
                $jwt = request()->key_code;
            }
            $client = DB::connection('pgsql')->table('crm_api_token')->where(['access_token'=>$jwt]);
            if ($client->exists()) {
                $client_scopes = $client->first()->scopes;
                foreach ($scopes as $scope) {
                    if (!(in_array($scope, json_decode($client_scopes))||in_array("*",json_decode($client_scopes)))) {
                        return response()->json([
                            'status' => 'error',
                            'status_code' => 403,
                            'message' => 'Auth header invalid'
                        ], 403);
                    }
                }
                return $next($request);
            } else {
                return response()->json([
                    'status' => 'error',
                    'status_code' => 403,
                    'message' => 'Invalid Validated Token'
                ], 403);
            }
        }
    }

}
