<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class Role
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty(request()->key_code) && empty(request()->header('Authorization'))) {
            return response()->json([
                'status' => 'error',
                'status_code' => 403,
                'message' => 'No Auth Token'
            ], 403);
        } else {
            if (!empty(request()->header('Authorization'))) {
                $jwt = explode(" ", request()->header('Authorization'));
                if ($jwt[0] !== 'Bearer'){
                    return response()->json([
                        'status' => 'error',
                        'status_code' => 403,
                        'message' => 'Auth header invalid'
                    ], 403);
                }
                $jwt = $jwt[1];
            }
            if (!empty(request()->key_code)) {
                $jwt = request()->key_code;
            }
            $client = Client::where(['secret'=>$jwt, 'vender'=>true]);
            if ($client->exists()) {
                $access_token = $client->first()->access_token;
                $request->headers->set('Authorization', 'Bearer ' . $access_token);
                return $next($request);
            } else {
                return response()->json([
                    'status' => 'error',
                    'status_code' => 403,
                    'message' => 'Invalid Validated Token'
                ], 403);
            }
        }
    }

}
