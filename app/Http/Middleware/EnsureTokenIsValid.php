<?php

namespace App\Http\Middleware;

use Closure;

class EnsureTokenIsValid
{
    /**
     * FOR VC APP 2
     */
    private $access_token = 'NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG';

    public function handle($request, Closure $next)
    {
        $headers = getallheaders();
//        print_r($headers);
        $x_authorization = isset($headers["X_AUTHORIZATION"]) ? $headers["X_AUTHORIZATION"] : '';
        $x_authorization2 = isset($headers["x_authorization"]) ? $headers["x_authorization"] : '';
//        print_r($x_authorization);
        if ((empty($x_authorization) && empty($x_authorization2)) || ($x_authorization != $this->access_token) && $x_authorization2 != $this->access_token) {
            return response()->json([
                'code' => 0,
                'title' => "Invalid Token",
                'message' => 'You do not have permission to access this service',
                'result' => $x_authorization
            ], 200);
        }

        return $next($request);
    }
}
