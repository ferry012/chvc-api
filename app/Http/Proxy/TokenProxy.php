<?php

namespace App\Http\Proxy;

use App\Events\UserLogin;
use App\Events\UserLogout;
use App\Http\Requests\Request;
use App\Models\User;
use App\Repositories\Member\MemberRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TokenProxy
{
    protected $http;
    public $memberRepository;

    private $message = array(
        "UPDATE_INFO" => array(
            "status" => 1,
            "message" => "Members sharing their log in credentials are responsible for their account-related transactions, not limiting to rebates’ redemptions/transfer/donations."
        ),
        "KEEP_LOGIN_SECURE" => array(
            "status" => 1,
            "message" => "Safeguard your rebates/redemptions/transfers and donations.<br/>Keep your log-in details secure."
        )
    );

    /**
     * TokenProxy constructor.
     * @param $http
     */
    public function __construct(\GuzzleHttp\Client $http, MemberRepository $memberRepository)
    {
        $this->http = $http;
        $this->memberRepository = $memberRepository;
    }

    public function login($email_addr, $mbr_pwd)
    {
        $email = trim(strtolower($email_addr));
//        $user = User::find('POS-API');
//        dd($user->createToken('pos api',['qr-login','member-points', 'member-transaction', 'member-register', 'member-details', 'member-voucher', 'member-ecredit',
//            'redeem-voucher', 'convert-staff'])->accessToken);
        $member = User::orWhereRaw('LOWER(mbr_id)=?', $email)->orWhere('contact_num',$email)->orWhereRaw('LOWER(email_addr)=?', $email);
        $count_mbr = $member->count();
        if ($count_mbr === 0) {
            return response()->json([
                'status' => 'login error',
                'status_code' => 100105,
                'message' => 'Invalid User'
            ], 421);
        } elseif ($count_mbr > 1) {
            return response()->json([
                'status' => 'login error',
                'status_code' => 100106,
                'message' => 'Duplicate User'
            ], 421);
        } else {
            $member = $member->first();
            $now = Carbon::now();
            $exp_date = Carbon::parse($member->exp_date);
            if ($member->login_locked == 'Y') {
                return response()->json([
                    'status' => 'login error',
                    'status_code' => 100102,
                    'message' => 'Login locked'
                ], 421);
            } elseif (rtrim($member->mbr_type) == 'X') {
                return response()->json([
                    'status' => 'login error',
                    'status_code' => 100103,
                    'message' => 'Member type is forbid'
                ], 421);
            } elseif ($member->status_level == 0) {
                return response()->json([
                    'status' => 'login error',
                    'status_code' => 100104,
                    'message' => 'Member status is not activated'
                ], 421);
            } elseif ($member->status_level == 1 && $now->gt($exp_date)){
                $this->memberRepository->set_member_expiry($member->mbr_id);
                if ($now->gt($exp_date->addMonths(3))) {
                    return response()->json([
                        'status' => 'login error',
                        'status_code' => 100107,
                        'message' => 'Member Expiry'
                    ], 421);
                }
            } elseif ($member->status_level == -1 && $now->gt($exp_date->addMonths(3))) {
                return response()->json([
                    'status' => 'login error',
                    'status_code' => 100107,
                    'message' => 'Member Expiry'
                ], 421);
            } elseif ($member->mbr_pwd != md5($mbr_pwd)) {
                return response()->json([
                    'status' => 'login error',
                    'status_code' => 100101,
                    'message' => 'Username or password is wrong'
                ], 421);
            }
        }

        $pass =  $this->proxy('password', [
            'username' => $email,
            'password' => $mbr_pwd,
            'scope' => 'vc-app',
        ]);
        if ($pass) {
            event(new UserLogin($member->mbr_id, $member->email_addr));

            //DO Post THing hEre.
//            print_r($pass);



            return $pass;
        }
    }

    public function resetPassword($oldPwd, $newPwd){
        $now = Carbon::now();
        $user = request()->user();
        if($user->mbr_pwd !== md5($oldPwd))
            return response()->json(['status'=>'Reset password failed', 'status_code'=>100302, 'message' => 'The old password is incorrect. Please enter the correct account or password.']);
        $user->mbr_pwd = md5($newPwd);
        $user->pwd_changed = $now;
        if (!$user->save())
            return response()->json(['status'=>'Reset password failed', 'status_code'=>100304, 'message' => 'There was an error saving your new password. Please try again later']);
        return response()->json(['status'=>'Reset password success', 'status_code'=>200, 'message' => 'A new password has been generated']);
    }

    public function proxy($grantType, array $data = [])
    {
        $data = array_merge($data, [
            'client_id'     => env('PASSPORT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_CLIENT_SECRET'),
            'grant_type'    => $grantType,
        ]);
        $website = $_SERVER['HTTP_HOST'];
        $response = $this->http->post('http://' . $website . '/oauth/token', ['form_params' => $data]);
        $token = json_decode((string)$response->getBody(), true);
        return response()->json(['token' => $token['access_token'],
            'refresh_token' => $token['refresh_token'],
            'expires_in' => Carbon::now()->addSecond($token['expires_in'])->toDateTimeString(),
            'status' => 'success',
            'status_code' => 200
        ]);
    }

    public function logout()
    {
        $user = auth()->guard('api')->user();
        if (!isset($user)) {
            return response()->json([
                'status' => 'logout error',
                'status_code' => 100201,
                'message' => 'You do not login.'
            ], 404);
        }
        $accessToken = $user->token();
        app('db')->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);
//        app('cookie')->forget('refreshToken');
        $accessToken->revoke();
//        event(new UserLogout($user));
        return response()->json([
                'status' => 'success',
                'status_code' => 200,
                'message' => 'logout success'
            ], 200);
    }

    public function refresh($refreshToken)
    {
//        $refreshToken = request()->cookie('refreshToken');
        return $this->proxy('refresh_token',
            ['refresh_token' => $refreshToken]);
    }
}