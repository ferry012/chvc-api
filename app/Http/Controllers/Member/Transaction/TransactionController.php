<?php

namespace App\Http\Controllers\Member\Transaction;

use App\Facades\Member\Transaction\Transaction;
use App\Http\Controllers\Result;
use App\Http\Controllers\Controller;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Services\IMS\Cherps2Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    use Result;

    private $memberTransactionRepository;
    private $cherps2Service;
    private $dbconnMssqlCherps;

    public function __construct(TransactionRepository $memberTransactionRepository, Cherps2Service $cherps2Service)
    {
        $this->memberTransactionRepository = $memberTransactionRepository;
        $this->cherps2Service = $cherps2Service;
        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    /**
     * @api {get} /api/member/transaction/mytransaction/{fdate}/{tdate}/{tfrom?} My Transaction
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} fdate 2018-01-01
     * @apiParam {string} tdate 2018-12-31
     * @apiParam {string} [tfrom] 'CTL' or 'HSG', default all trans_type
     *
     * @apiSuccessExample Return all transaction data
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "coy_id": "CTL",
     *              "mbr_id": "+0966048869",
     *              "trans_id": "NPP100000248464",
     *              "line_num": 1,
     *              "trans_type": "HR",
     *              "trans_date": "2016-11-25 16:04:13.053",
     *              "loc_id": "NP",
     *              "pos_id": "P1",
     *              "item_id": "0763649075807",
     *              "item_desc": "Seagate Backup Plus Portable 2TB Gold 2998185",
     *              "regular_price": "159.00",
     *              "unit_price": "129.00000000",
     *              "disc_percent": "0.00",
     *              "disc_amount": "0.00",
     *              "trans_points": "129.00",
     *              "salesperson_id": "",
     *              "mbr_savings": "0.00",
     *              "created_by": "2779",
     *              "created_on": "2016-11-25 16:06:19.336",
     *              "modified_by": "2779",
     *              "modified_on": "2016-11-25 16:06:19.336",
     *              "updated_on": "2016-11-25 16:06:20.09",
     *              "tradte": "2016-11-25 16:04:13.053",
     *              "item_qty": "1.00",
     *              "ref_key": "97D91718-427A-4B5B-BAFE-89CC9FCD8689",
     *              "store": "Hachi.tech",
     *              "trdate": "2019-10-31",
     *              "inv_type": 2,
     *              "ho_id": "HIB18824"
     *          },
     *      ]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Get transaction error
     * HTTP/1.1 Get transaction error
     *  {
     *      "status": "error",
     *      "status_code": 400,
     *      "message": "Something wrong to get transaction"
     *  }
     **/
    public function myTransaction($fdate, $tdate, $tfrom='')
    {
        $fdate = Carbon::parse($fdate);
        $tdate = Carbon::parse($tdate);
        $trans_from = Carbon::parse("2017-01-01");
        $mbrId = trim(request()->user()->mbr_id," ");
        $transType = ['HI','PS','OS','RF','VC','EX','HR','PU','ID','DZ','DU'];
        if ($tfrom === 'CTL') {
            $transType = ['PS','RF','OS','VC','EX','PU','ID','DZ','DU'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI','HR'];
        }
        if ($fdate->gte($trans_from)) {
            $data = Transaction::myTransaction($mbrId, $fdate, $tdate, $transType);
        } else {
            $data = Transaction::myTransactionHistory($mbrId, $fdate, $tdate, $transType);
        }
        foreach ($data as $v) {
            $trans_id = $v->trans_id;
            $trans_type = rtrim($v->trans_type);
            $trans_date = $v->trans_date;
            $loc_id = $v->loc_id;
            $ho_id = $trans_type === 'HR' ? '' : $trans_id;
            if ($tfrom === 'CTL') {
                $inv_type = 1;
            } elseif ($tfrom === 'HSG') {
                $inv_type = 2;
            } else {
                $inv_type = 0;
            }
            $trdate = Carbon::parse($trans_date)->toDateString();
            $ref_key = $this->cherps2Service->get_ref_key($trans_id);
            $ref_key = !isset($ref_key) ? $ref_key->ref_key : '';
            if (substr($trans_id, 0, 2) === 'HI') {
                $store = 'Hachi.tech';
            } elseif (strpos($loc_id, '-C') !== false || strpos($loc_id, '-E') !== false || strpos($loc_id, '-I') !== false || strpos($loc_id, '-O') !== false || strpos($loc_id, '-P') !== false || strpos($loc_id, '-S') !== false) {
                $store = 'Trade Show';
            } elseif (strpos($loc_id, '-RS') !== false) {
                $store = 'Road Show';
            } else {
                $store = $this->dbconnMssqlCherps->table('o2o_store_location')->where('store_id', $loc_id)->first();
                $store = !empty($store) ? $store->title : '';
            }
            $v->ref_key = $ref_key;
            $v->store = $store;
            $v->trdate = $trdate;
            $v->inv_type = $inv_type;
            $v->ho_id = $ho_id;
        }
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('Something wrong to get transaction');
        }
    }

    /**
     * @api {get} /api/member/transaction/myredemption/{fdate}/{tdate} Get My Redemption
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} fdate 2018-01-01
     * @apiParam {string} tdate 2018-12-31
     *
     * @apiSuccessExample Return all redemption data
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "coy_id": "CTL",
     *              "mbr_id": "+0966048869",
     *              "trans_id": "V2A33747",
     *              "line_num": 3,
     *              "trans_type": "RD",
     *              "trans_date": "2018-07-24 17:30:43.546",
     *              "loc_id": "VC",
     *              "pos_id": "V2",
     *              "item_id": "0811571016525",
     *              "item_desc": "XXGoogle ChromeCast 2 (Black) 1-Year Warranty by Challenger",
     *              "item_qty": "1.00",
     *              "regular_price": "65.00",
     *              "unit_price": "65.00000000",
     *              "disc_percent": "0.00",
     *              "disc_amount": "0.00",
     *              "trans_points": "-65.00",
     *              "salesperson_id": "3503",
     *              "mbr_savings": "0.00",
     *              "created_by": "3503",
     *              "created_on": "2018-07-24 17:32:36.183",
     *              "modified_by": "3503",
     *              "modified_on": "2018-07-24 17:32:36.183",
     *              "updated_on": "2018-07-24 17:32:37.97",
     *              "trdate": "2018-07-24",
     *              "description": "Rebates Redeemed",
     *              "store": "Hachi.tech",
     *              "ho_id": "HIB18824",
     *              "inv_id": ""
     *          },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Get redemption error
     * HTTP/1.1 Get redemption error
     *  {
     *      "status": "error",
     *      "status_code": 400,
     *      "message": "Something wrong to get redemption"
     *  }
     **/
    public function myRedemption($fdate, $tdate)
    {
        $fdate = Carbon::parse($fdate);
        $tdate = Carbon::parse($tdate);
        $trans_from = Carbon::parse("2017-01-01");
        $mbrId = trim(request()->user()->mbr_id," ");
        if ($fdate->gte($trans_from)) {
            $data = Transaction::myRedemption($mbrId, $fdate, $tdate);
        } else {
            $data = Transaction::myRedemptionHistory($mbrId, $fdate, $tdate);
        }
        foreach ($data as $v) {
            $trans_id = $v->trans_id;
            $trans_type = $v->trans_type;
            $trans_date = $v->trans_date;
            $loc_id = $v->loc_id;
            $item_desc = $v->item_desc;
            $ho_id = !empty($trans_id) ? $trans_id : '';
            $trdate = Carbon::parse($trans_date)->toDateString();
            if ($trans_type === 'RD') {
                $description = 'Rebates Redeemed';
            } elseif (substr($item_desc, 0, 8) === 'Donation') {
                $description = 'Rebates Donated';
            } else {
                $description = 'Rebates Transferred';
            }
            if (substr($trans_id, 0, 2) === 'HI') {
                $store = 'Hachi.tech';
            } elseif ($loc_id === '' && $trans_type === 'TP') {
                $store = 'ValueClub App';
            } else {
                $store = $this->dbconnMssqlCherps->table('o2o_store_location')->where('store_id', $loc_id)->first();
                $store = !empty($store) ? $store->title : $loc_id;
            }
            if (substr($trans_id, 0, 2) !== 'HI' && $trans_type !== 'TP' && substr($trans_id, 0, 2) !== 'OS') {
                $inv_id = empty($trans_id) ? '' : $trans_id;
            } else {
                $inv_id = '';
            }
            $v->description = $description;
            $v->store = $store;
            $v->trdate = $trdate;
            $v->ho_id = $ho_id;
            $v->inv_id = $inv_id;
        }
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('Something wrong to get redemption');
        }
    }

    /**
     * @api {get} /api/member/transaction/mywarranty/{fdate}/{tdate}/{tfrom?} My Warranty
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} fdate 2018-01-01
     * @apiParam {string} tdate 2018-12-31
     *
     * @apiSuccessExample Return all warranty data
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "ref_key": "",
     *              "coy_id": "CTL",
     *              "mbr_id": "+0966048869",
     *              "ho_id": "A3C36416",
     *              "inv_type": 2,
     *              "trans_id": "V2A33747",
     *              "line_num": 3,
     *              "trans_type": "RD",
     *              "trans_date": "2018-07-24 17:30:43.546",
     *              "loc_id": "VC",
     *              "pos_id": "V2",
     *              "item_id": "0811571016525",
     *              "item_desc": "XXGoogle ChromeCast 2 (Black) 1-Year Warranty by Challenger",
     *              "item_qty": "1.00",
     *              "regular_price": "65.00",
     *              "unit_price": "65.00000000",
     *              "disc_percent": "0.00",
     *              "disc_amount": "0.00",
     *              "trans_points": "-65.00",
     *              "salesperson_id": "3503",
     *              "mbr_savings": "0.00",
     *              "created_by": "3503",
     *              "created_on": "2018-07-24 17:32:36.183",
     *              "modified_by": "3503",
     *              "modified_on": "2018-07-24 17:32:36.183",
     *              "updated_on": "2018-07-24 17:32:37.97",
     *              "store": "Hachi.tech",
     *              "trdate": "2018-07-24",
     *              "rownum": 1,
     *              "serial_num": "SDMPD68LWLMPD",
     *              "mode_id": "MUUJ2ZP/A",
     *              "item_model_id": "MUUJ2ZP/A",
     *              "warranty_month": "12",
     *              "ext_warranty_month": 24,
     *              "ext_warranty_id": "EW2020000032866",
     *              "prod_serial_no": "SDMPD68LWLMPD",
     *              "item_type": "IPAD WIFI",
     *              "warranty_year": "1",
     *              "ext_warranty_invoice_id": "A3C36416"
     *          },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     **/
    public function myWarranty($fdate, $tdate, $tfrom='')
    {
        $fdate = Carbon::parse($fdate)->toDateString();
        $tdate = Carbon::parse($tdate)->toDateString();
        $mbrId = trim(request()->user()->mbr_id," ");
        $transType = ['HI','PS','OS','RF','VC','EX','HR','PU','ID','DZ','DU'];
        if ($tfrom === 'CTL') {
            $transType = ['PS','RF','OS','VC','EX','PU','ID','DZ','DU'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI','HR'];
        }
        $data = $this->cherps2Service->my_warranty($mbrId, $fdate, $tdate);
        $warranty = [];
        foreach ($data as $k=>$v) {
            $trans_id = rtrim($v['offer_invoice_id']);
            $item_id = rtrim($v['offer_item_id']);
            $trans = $this->memberTransactionRepository->get_warranty($trans_id, $item_id);
            if (!empty($trans)) {
                $trans_type = $trans->trans_type;
                if (!in_array($trans_type, $transType)) {
                    continue;
                }
                $trans_date = $trans->trans_date;
                $loc_id = rtrim($trans->loc_id);
                $item_desc = $trans->item_desc;
                $ho_id = !empty($trans_id) ? $trans_id : '';
                $trdate = Carbon::parse($trans_date)->toDateString();
                $ref_key = $this->cherps2Service->get_ref_key($trans_id);
                $ref_key = !empty($ref_key) ? $ref_key->ref_key : '';
                if (substr($trans_id, 0, 2) === 'HI') {
                    $store = 'Hachi.tech';
                } elseif (strpos($loc_id, '-C') !== false || strpos($loc_id, '-E') !== false || strpos($loc_id, '-I') !== false || strpos($loc_id, '-O') !== false || strpos($loc_id, '-P') !== false || strpos($loc_id, '-S') !== false) {
                    $store = 'Trade Show';
                } elseif (strpos($loc_id, '-RS') !== false) {
                    $store = 'Road Show';
                } else {
                    $store = $this->dbconnMssqlCherps->table('o2o_store_location')->where('store_id', $loc_id)->first();
                    $store = !empty($store) ? $store->title : '';
                }
                if ($tfrom === 'CTL') {
                    $inv_type = 1;
                } elseif ($tfrom === 'HSG') {
                    $inv_type = 2;
                } else {
                    $inv_type = 0;
                }
                $warranty[] = [
                    "ref_key"=> $ref_key,
                    "coy_id"=> $trans->coy_id,
                    "mbr_id"=> $mbrId,
                    "ho_id"=> $ho_id,
                    "inv_type"=> $inv_type,
                    "trans_id"=> $trans_id,
                    "line_num"=> $trans->line_num,
                    "trans_type"=> $trans_type,
                    "trans_date"=> $trans_date,
                    "loc_id"=> $loc_id,
                    "pos_id"=> rtrim($trans->pos_id),
                    "item_id"=> $item_id,
                    "item_desc"=> $item_desc,
                    "item_qty"=> $trans->item_qty,
                    "regular_price"=> $trans->regular_price,
                    "unit_price"=> $trans->unit_price,
                    "disc_percent"=> $trans->disc_percent,
                    "disc_amount"=> $trans->disc_amount,
                    "trans_points"=> $trans->trans_points,
                    "salesperson_id"=> $trans->salesperson_id,
                    "mbr_savings"=> $trans->mbr_savings,
                    "created_by"=> $trans->created_by,
                    "created_on"=> $trans->created_on,
                    "modified_by"=> $trans->modified_by,
                    "modified_on"=> $trans->modified_on,
                    "updated_on"=> $trans->updated_on,
                    "store"=> $store,
                    "trdate"=> $trdate,
                    "rownum"=> $k + 1,
                    "serial_num"=> rtrim($v['serial_num']),
                    "mode_id"=> rtrim($v['model_id']),
                    "item_model_id"=> rtrim($v['model_id']),
                    "warranty_month"=> $v['string_udf1'],
                    "ext_warranty_month"=> $v['offer_valid_month'],
                    "ext_warranty_id"=> rtrim($v['offer_id']),
                    "prod_serial_no"=> rtrim($v['serial_num']),
                    "item_type"=> rtrim($v['product_type']),
                    "warranty_year"=> number_format((int)$v['string_udf1']/12, '1', '.', ''),
                    "ext_warranty_invoice_id"=> $trans_id
                ];
            }
        }
        return $this->successWithData($warranty);
    }


}
