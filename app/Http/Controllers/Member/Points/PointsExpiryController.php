<?php

namespace App\Http\Controllers\Member\Points;

use App\Http\Controllers\Result;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Http\Controllers\Controller;

class PointsExpiryController extends Controller
{
    use Result;

    private $memberPointsExpiryRepository;

    public function __construct(PointsExpiryRepository $memberPointsExpiryRepository)
    {
        $this->memberPointsExpiryRepository = $memberPointsExpiryRepository;
    }

    /**
     * @api {get} /api/member/points/nextexpiry Next Expiry Date
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return Next Expiry Date
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "date": "2019-12-31 00:00:00.000000",
     *              "timezone_type": 3,
     *              "timezone": "Asia/Singapore"
     *          }
     *      ]
     *      "status": "success",
     *      "status_code": 200
     * }
     **/
    public function getNextExpiryDate()
    {
        $data = $this->memberPointsExpiryRepository->getNextExpiryDate();
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('error');
        }

    }
}
