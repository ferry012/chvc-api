<?php

namespace App\Http\Controllers\Member\Points;

use App\Http\Controllers\Result;
use App\Http\Controllers\Controller;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\Points\PointsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PointsController extends Controller
{
    use Result;

    private $memberPointsRepository;
    private $memberRepository;

    public function __construct(PointsRepository $memberPointsRepository, MemberRepository $memberRepository)
    {
        $this->memberPointsRepository = $memberPointsRepository;
        $this->memberRepository = $memberRepository;
    }


    /**
     * @api {get} /api/member/points/recentexpiry Recent Expiring Points
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return recent expiring points
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "coy_id": "CTL",
     *              "mbr_id": "0966048869",
     *              "line_num": 1,
     *              "exp_date": "2028-06-30 00:00:00",
     *              "points_accumulated": "1160.00",
     *              "points_redeemed": "1124.00",
     *              "points_expired": "36.00",
     *              "created_by": "2198",
     *              "created_on": "2016-01-03 17:00:02.416",
     *              "modified_by": "cherps",
     *              "modified_on": "2018-07-01 01:02:24.06",
     *              "updated_on": "2016-03-19 19:15:00.783"
     *          }
     *      ]
     *      "status": "success",
     *      "status_code": 200
     *  }
     **/
    public function recentExpiringPoints()
    {
        $mbrId = request()->user()->mbr_id;
        $data = $this->memberPointsRepository->getRecentExpiringPoints($mbrId);
        if($data){
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('error');
        }

    }

    /**
     * @api {get} /api/member/points/summary Summary Points
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return Summary points
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "points_accumulated": 234,
     *              "points_redeemed": 117,
     *              "points_expired": 0
     *          }
     *      ]
     *      "status": "success",
     *      "status_code": 200
     *  }
     **/
    public function summary()
    {
        $mbrId = request()->user()->mbr_id;
        $data = $this->memberPointsRepository->summary($mbrId);
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('error');
        }

    }

    /**
     * @api {get} /api/member/points/enough/{points} Points is Enough
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return boolean
     * HTTP/1.1 200 OK
     *  {
     *      "data": true,  //or false
     *      "status": "success",
     *      "status_code": 200
     *  }
     **/
    public function isPointEnough($points)
    {
        $mbrId = request()->user()->mbr_id;
        $member = $this->memberRepository->findByMbrId($mbrId);
        $data = $member->points_remaining >= (int)$points ? true : false;
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/member/points/balance Balance Points.
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return balance points
     * HTTP/1.1 200 OK
     *  {
     *      "data": "x.xx",
     *      "status": "success",
     *      "status_code": 200
     *  }
     **/
    public function balancePoints()
    {
        $mbrId = request()->user()->mbr_id;
        $data = $this->memberPointsRepository->balancePoints($mbrId);
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('error');
        }

    }

    /**
     * @api {get} /api/member/points/nextline Get Next Line Number
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiSuccessExample Return next line number
     * HTTP/1.1 200 OK
     *  {
     *      "data": 2,
     *      "status": "success",
     *      "status_code": 200
     *  }
     **/
    public function getNextLineNum()
    {
        $mbrId = request()->user()->mbr_id;
        $data = $this->memberPointsRepository->getNextLineNum($mbrId);
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('error');
        }

    }

}
