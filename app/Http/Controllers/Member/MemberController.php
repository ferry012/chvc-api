<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Result;
use App\Http\Resources\User;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\VcMemberRepository;
use App\Services\EmarSys\EmarsysServiceV2;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    use Result;

    private $memberRepository;
    private $vcmemberRepository;
    private $addressRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $emarsysService;

    public function __construct(MemberRepository $memberRepository, AddressRepository $addressRepository, PointsRepository $pointsRepository, TransactionRepository $transactionRepository,
                                EmarsysServiceV2 $emarsysService, VcMemberRepository  $vcmemberRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->addressRepository = $addressRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->emarsysService = $emarsysService;
        $this->vcmemberRepository= $vcmemberRepository;

    }




}
