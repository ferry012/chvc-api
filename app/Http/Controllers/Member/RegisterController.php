<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Repositories\Member\SysTransListRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Services\CtlmailerService;

use App\Repositories\Member\MemberRepository;

class RegisterController extends Controller
{
    private $sysTransListRepository;
    private $memberRepository, $ctlmailerService;
    private $coy_id, $dbconn;

    public function __construct(MemberRepository $memberRepository, SysTransListRepository $sysTransListRepository, CtlmailerService $ctlmailerService)
    {
        $this->memberRepository = $memberRepository;
        $this->ctlmailerService = $ctlmailerService;
        $this->sysTransListRepository = $sysTransListRepository;

        $this->dbconn = DB::connection('pgsql');

        $this->coy_id = 'CTL';

        Validator::extend('contact_sg', function($attribute, $value, $parameters, $validator) {
            return in_array( substr($value,0,1) , ['6','8','9']);
        });
    }

    /**
     * @api {post} /api/member_item Get Membership Items
     * @apiVersion 1.0.0
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *           item_type: "M28",
     *           item_id: "!MEMBER-NEW28",
     *           item_desc: "28 Months",
     *           item_group: "NEW",
     *           unit_price: "0.00",
     *           rebate_tier: "2.00",
     *           can_upgrade: 0,
     *           day_can_renew: 180,
     *           month_of_duration: 28
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     */
    public function getMemberItem(){

        $status = (isset($_GET['status']) && strtoupper($_GET['status']) == 'ALL') ? -1 : 1;

        $items = $this->dbconn->table('ims_item_list_member')
            ->selectRaw('item_type,item_id,item_desc,item_group,unit_price,rebate_tier, can_upgrade,day_can_renew,month_of_duration, status_level')
            ->where('status_level','>=', $status)
            ->where('item_id','<>', "!MEMBER-NS")
            ->orderBy('item_group')->orderBy('item_type')->orderBy('item_id')->get();

        return $this->successWithData($items);
    }

    public function getMemberItemPos(){

        $status = (isset($_GET['status']) && strtoupper($_GET['status']) == 'ALL') ? -1 : 1;

        $items = $this->dbconn->table('ims_item_list_member')
            ->selectRaw('item_type,item_id,item_desc,item_group,unit_price,rebate_tier, can_upgrade,day_can_renew,month_of_duration, status_level')
            ->where('status_level','>=', $status)
            // ->orWhere('item_id', '!MEMBER-NS')
            ->orderBy('item_group')->orderBy('item_type')->orderBy('item_id')->get();

        return $this->successWithData($items);
    }

    /**
     * @api {post} /api/otp_register Pre-Register with OTP
     * @apiVersion 1.0.0
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} contact Contact number
     * @apiParam {string} email Email address
     * @apiParam {string} source pos id, or HI, or VC, etc
     * @apiParam {string} [first_name] Option First name
     * @apiParam {string} [last_name] Option Last name
     * @apiParam {string} [mbr_type] Option Member type
     * @apiParam {string} [message_id] Option Validating OTP to complete pre-registration
     * @apiParam {string} [otp_code] Option Validating OTP to complete pre-registration
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "contact": "81431879",
     *      "email": "test@test.com",
     *      "source": "B4",
     *      "first_name": "xx",
     *      "last_name": "xxxxx",
     *      "mbr_type": "M08"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "contact": "81431879",
     *          "email": "test@test.com",
     *          "source": "B4",
     *          "first_name": "xx",
     *          "last_name": "xxxxx",
     *          "mbr_type": "M08",
     *          "message": "Please retry this API with same message_id & valid otp_code",
     *          "message_id": "X1234567890",
     *          "otp_code": ""
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function register_otp()
    {
        $now = Carbon::now()->toDateTimeString();

        $input = request()->only(['contact', 'email', 'first_name', 'last_name', 'mbr_type', 'source', 'birth_date', 'mbr_pwd', 'otp_code', 'message_id']);
        $email = $input['email'];
        $contact = $input['contact'];
        $member = $this->memberRepository->check_member_exists($email, $contact);
        if (!empty($member)) {
            if ($member->status_level != -9) {
                $error = [
                    'membership' => ['Member information already exists. Please use another email account/contact number.']
                ];
                return $this->errorWithCodeAndInfo(422, $error);
            }

            // Update mbr info
            $first_name = $input['first_name'] ? substr($input['first_name'],0,40) : $member->first_name;
            $last_name = $input['last_name'] ? substr($input['last_name'],0,40) : $member->last_name;
            $mbr_type = $input['mbr_type'] ? $input['mbr_type'] : $member->mbr_type;
            $this->memberRepository->where(['coy_id' => 'CTL', 'email_addr' => $email, 'contact_num' => $contact])->update([
                "first_name" => $first_name,
                "last_name" => $last_name,
                "mbr_type" => $mbr_type,
                "modified_on" => $now
            ]);

            return $this->successWithData([
                'mbr_id' => rtrim($member->mbr_id),
                'first_name' => $first_name, //$member->first_name,
                'last_name' => $last_name, //$member->last_name,
                'email_addr' => $member->email_addr,
                'contact_num' => $member->contact_num,
                'join_date' => $member->join_date,
                'exp_date' => $member->exp_date,
                'mbr_type' => rtrim($mbr_type),
                'rebate' => 0,
                'can_renew' => 'X',
                'rebate_date' => null
            ]);

        }

        $rules = [
            'contact' => 'required|digits:8|contact_sg',
            'email' => 'required|email',
            'source' => 'required|size:2',
            'first_name' => 'max:40',
            'last_name' => 'max:40'
        ];
        $message = [
            'contact.required' => 'Contact number required.',
            //'contact.unique' => 'Contact number exists in our system.',
            'contact.digits' => 'Contact number is invalid, only SG contact number is accepted.',
            'contact.contact_sg' => 'Only SG contact number is accepted.',
            'email.required' => 'Email address required.',
            //'email.unique' => 'Email address exists in our system.',
            'email.email' => 'Email address is invalid.',
            'source.required' => 'Source ID required.',
            'source.size' => 'Invalid Source ID - only 2 characters'
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        // Validate unique contact & email, delete if existing is preregister
        $validateUnique = $this->dbconn->table('crm_member_list')
            ->where('contact_num', $input['contact'])->orWhere('email_addr', $input['email'])
            ->get();
        if ($validateUnique->count() > 0) {
            if ($validateUnique->pluck('status_level')->max() != -9) {
                return $this->errorWithCodeAndInfo(422, 'Contact number or email address exist in our system');
            }
            else {
                // Delete the pre-registered accounts
                $this->dbconn->table('crm_member_list')
                    ->where('contact_num', $input['contact'])->orWhere('email_addr', $input['email'])
                    ->delete();
            }
        }

        // Process OTP register
        if (isset($input['otp_code'])) {
            // Validate OTP
            $message_id = $input['message_id'] ?? '';
            $otp_record = $this->dbconn->table('crm_otp_list')->where('message_id', $message_id)->orderby('created_on', 'desc')->first();

            if (!$otp_record) {
                // Invalid message_id
                return $this->validateError(['message_id' => ['Wrong message_id.']]);
            }

            $udf_string = json_decode($otp_record->udf_string);

            if (!isset($udf_string->email) || $udf_string->email != $input['email']) {
                // Wrong details
                return $this->validateError(['email' => ['Wrong email address.']]);
            }

            if (!isset($udf_string->contact) || $udf_string->contact != $input['contact']) {
                // Wrong details
                return $this->validateError(['contact' => ['Wrong contact number.']]);
            }

            if ( $udf_string->expiry < time() ) {
                // Wrong details
                return $this->validateError(['otp_code' => ['OTP Expired, please register again.']]);
            }

            if ( trim($otp_record->otp_code) != trim($input['otp_code']) ) {
                // Wrong OTP
                return $this->validateError(['otp_code' => ['Wrong OTP, please try again']]);
            }


            // Success - Proceed with registration
            $source = $input['source'];
            $mbr_id = $this->sysTransListRepository->generate_mbr_id($source);
            $this->dbconn->table('crm_otp_list')->where('message_id', $message_id)->update([
                'mbr_id'        => $mbr_id,
                'status_level'  => 1,
                'modified_by'   => $source,
                'modified_on'   => $now
            ]);
            return $this->_register($input, $mbr_id);

        } else {
            // Calculate & generate OTP
            $expiry_ttl = 600;
            $signature_string = json_encode(['email' => $email, 'contact' => $contact, 'mbr_type' => $input['mbr_type'], 'expiry' => time() + $expiry_ttl, 'ttl' => $expiry_ttl]);
            $signature = md5($signature_string);
            $otp_code = str_pad(substr(preg_replace("/[^0-9]/", "", $signature), 0, 6), 6, '0');

            // Send SMS
            $subject = "Your verification code is: $otp_code.\nThis code will expire in 10 minutes. Please do not share this code with others.";
            $sms = $this->ctlmailerService->send_sms($contact, $subject);
            if ($sms->code == 1) {

                // Save if SMS sent
                $this->dbconn->table('crm_otp_list')->insert([
                    'coy_id' => $this->coy_id,
                    'mbr_id' => 'NEW_MEMBER',
                    'message_id' => $signature,
                    'otp_code' => $otp_code,
                    'udf_string' => $signature_string,
                    'status_level' => 0,
                    'created_by' => $input['source'],
                    'created_on' => $now,
                    'modified_by' => $input['source'],
                    'modified_on' => $now
                ]);

                $res_data = array_merge($input, [
                    'message_id' => $signature,
                    'otp_code' => '',
                    'message' => 'Please retry this API with same message_id & valid otp_code'
                ]);
                ksort($res_data);
                return $this->successWithData($res_data);

            } else {
                // Sending fail, cannot register
                return $this->errorWithInfo('OTP SMS problem, cannot register member.');
            }

        }

    }

    private function _register($input, $mbr_id = '')
    {
        $source = $input['source'];
        $contact = $input['contact'];
        $email = $input['email'];
        $mbr_id = (isset($mbr_id) && $mbr_id!='') ? $mbr_id : $this->sysTransListRepository->generate_mbr_id($source);
        $now = Carbon::now()->toDateTimeString();
        $endOfDay = Carbon::now()->endOfDay()->toDateTimeString();
        $default_date = new \DateTime('1900-01-01T00:00:00');
        $birth_date = isset($input['birth_date']) ? Carbon::parse($input['birth_date']) : $default_date;
        $first_name = $input['first_name'] ? substr($input['first_name'],0,40) : '';
        $last_name = $input['last_name'] ? substr($input['last_name'],0,40) : '';
        $mbr_type = $input['mbr_type'] ? $input['mbr_type'] : '';
        $mbr_pwd = isset($input['mbr_pwd']) ? md5($input['mbr_pwd']) : md5(substr($contact, -4));
        $mbr_pwd2 = isset($input['mbr_pwd']) ? bin2hex(openssl_digest($input['mbr_pwd'], 'sha256',true)) : bin2hex(openssl_digest(substr($contact, -4), 'sha256',true));
        $res = $this->_register_member($mbr_id, $mbr_pwd, $mbr_pwd2, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, -9);

        if ($res) {
            $res_data = [
                'mbr_id' => $mbr_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email_addr' => $email,
                'contact_num' => $contact,
                'join_date' => $now,
                'exp_date' => $endOfDay,
                'mbr_type' => $mbr_type,
                'rebate' => 0,
                'can_renew' => 'X',
                'rebate_date' => null
            ];
            return $this->successWithData($res_data);
        } else {
            return $this->errorWithInfo('Server problem, cannot register member.');
        }
    }

    public function _register_member($mbr_id, $mbr_pwd, $mbr_pwd2,$default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, $status_level, $created_by='', $main_id='')
    {
        $now = Carbon::now()->toDateTimeString();

        $data = [];
        $data['coy_id'] = 'CTL';
        $data['mbr_id'] = $mbr_id;
        $data['mbr_title'] = '';
        $data['first_name'] = $first_name;
        $data['last_name'] = $last_name;
        $data['birth_date'] = $birth_date;
        $data['nationality_id'] = '';
        $data['email_addr'] = trim(strtolower($email));
        $data['contact_num'] = $contact;
        $data['mbr_pwd'] = $mbr_pwd;
        $data['mbr_pwd2'] = $mbr_pwd2;
        $data['pwd_changed'] = $default_date;
        $data['last_login'] = $default_date;
        $data['mbr_addr'] = '';
        $data['delv_addr'] = '';
        $data['send_info'] = '';
        $data['send_type'] = '';
        $data['status_level'] = $status_level;
        $data['join_date'] = $now;
        $data['exp_date'] = $endOfDay;
        $data['last_renewal'] = $default_date;
        $data['points_accumulated'] = 0;
        $data['points_reserved'] = 0;
        $data['points_redeemed'] = 0;
        $data['points_expired'] = 0;
        $data['mbr_savings'] = 0;
        $data['rebate_voucher'] = '';
        $data['mbr_type'] = $mbr_type;
        $data['main_id'] = $main_id;
        $data['sub_ind1'] = 'Y';
        $data['sub_ind2'] = 'YYYYY';
        $data['sub_date'] = $now;
        $data['created_by'] = $created_by;
        $data['created_on'] = $now;
        $data['modified_by'] = $created_by;
        $data['modified_on'] = $now;
        $data['updated_on'] = $now;
        $data['login_locked'] = 'N';
        $res = $this->memberRepository->saveModel($data);
        return $res;
    }
}