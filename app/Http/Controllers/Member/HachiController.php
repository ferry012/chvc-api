<?php

namespace App\Http\Controllers\Member;

use App\Facades\Member\Transaction\Transaction;
use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\MemberMetaRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\RebateClaimRepository;
use App\Repositories\Member\Transaction\TransactionHistoryRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\VoucherIssueDataRepository;
use App\Repositories\Member\Voucher\VoucherRedeemRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use function Psy\debug;

class HachiController extends Controller
{
    use Result;
    private $memberRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $voucherRepository;
    private $pointsExpiryRepository;
    private $memberTypeRepository;
    private $rebateClaimRepository;
    private $memberMetaRepository;
    private $voucherIssueDataRepository;
    private $addressRepository;
    private $voucherRedeemRepository;
    private $dbconnMssqlCherps;
    private $transactionHistoryRepository;

    private $key_code = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                TransactionRepository $transactionRepository,
                                TransactionHistoryRepository $transactionHistoryRepository,
                                VoucherRepository $voucherRepository,
                                PointsExpiryRepository $pointsExpiryRepository,
                                MemberTypeRepository $memberTypeRepository,
                                RebateClaimRepository $rebateClaimRepository,
                                MemberMetaRepository $memberMetaRepository,
                                VoucherIssueDataRepository $voucherIssueDataRepository,
                                VoucherRedeemRepository $voucherRedeemRepository,
                                AddressRepository $addressRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->transactionHistoryRepository = $transactionHistoryRepository;
        $this->voucherRepository = $voucherRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->rebateClaimRepository = $rebateClaimRepository;
        $this->memberMetaRepository = $memberMetaRepository;
        $this->voucherIssueDataRepository = $voucherIssueDataRepository;
        $this->addressRepository = $addressRepository;
        $this->voucherRedeemRepository = $voucherRedeemRepository;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    private function getRebateTierFromType($mbr_type) {
        if (substr($mbr_type,0,1) == 'S') {
            // Staff membership - 1.5
            return 1.5;
        }
        $rebate_table = $this->dbconnMssqlCherps->table('ims_item_list_member')->where('item_type', $mbr_type)->first();
        return ($rebate_table) ? $rebate_table->rebate_tier : 0;
    }

    public function profileDetail($mbrId)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $member = $this->memberRepository->findByMbrId(rtrim($mbrId));
        if (!empty($member)) {
            $mbr_id = rtrim($member->mbr_id);
            $mbr_addr = rtrim($member->mbr_addr);
            $delv_addr = rtrim($member->delv_addr);
            $res_mbr_addr = [];
            $res_delv_addr = [];
            if ($mbr_addr !== '') {
                $mbr_addr_detail = $this->addressRepository->getAddress($mbrId, $mbr_addr);
                $res_mbr_addr = [
                    'street_line1' => $mbr_addr_detail->street_line1,
                    'street_line2' => $mbr_addr_detail->street_line2,
                    'street_line3' => $mbr_addr_detail->street_line3,
                    'street_line4' => $mbr_addr_detail->street_line4,
                    'country_id' => $mbr_addr_detail->country_id,
                    'postal_code' => $mbr_addr_detail->postal_code
                ];
            }
            if ($delv_addr !== '') {
                $delv_addr_detail = $this->addressRepository->getAddress($mbrId, $delv_addr);
                $res_delv_addr = [
                    'street_line1' => $delv_addr_detail->street_line1,
                    'street_line2' => $delv_addr_detail->street_line2,
                    'street_line3' => $delv_addr_detail->street_line3,
                    'street_line4' => $delv_addr_detail->street_line4,
                    'country_id' => $delv_addr_detail->country_id,
                    'postal_code' => $delv_addr_detail->postal_code
                ];
            }

            return $this->successWithData([
                'mbr_id' => $mbr_id,
                'mbr_title' => rtrim($member->mbr_title),
                'first_name' => $member->first_name,
                'last_name' => $member->last_name,
                'birth_date' => $member->birth_date,
                'email_addr' => $member->email_addr,
                'contact_num' => $member->contact_num,
                'billing' => empty($res_mbr_addr) ? [] : $res_mbr_addr,
                'shipping' => empty($res_delv_addr) ? [] : $res_delv_addr
            ]);
        }
    }

    public function membershipDetails($mbrId)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $member = $this->memberRepository->getMemberDetail($mbrId);
        if (!empty($member)) {
            $main_mbr_id = rtrim($member->main_id);
            $mbr_type = rtrim($member->mbr_type);
            $main_member_data = [];
            if ($main_mbr_id !== '') {
                $main_member = $this->memberRepository->findByMbrId($main_mbr_id);
                $main_member_data = [
                    'first_name' => $main_member->first_name,
                    'last_name' => $main_member->last_name
                ];
            }
            if ($mbr_type == 'MAS') {
                $main_type = $this->memberRepository->findByMbrId($member['main_id'])->mbr_type;
                $rebate_tier = $this->getRebateTierFromType($main_type);
            } else {
                $rebate_tier = $member->mbr_rebate_times;
            }
            $rebates_expiry = $this->pointsRepository->getExpDate($mbrId);
            return $this->successWithData([
                'mbr_months' => $member->mbr_month,
                'mbr_rebate_times' => $rebate_tier,
                'join_date' => $member->join_date,
                'expiry_date' => $member->exp_date,
                'MembershipRenewal' => $member->can_renew,
                'MemberUpgrade' => $member->can_upgrade === 1 ? "Y" : "N",
                'main_member' => $main_member_data,
                'points_accumulated' => $member->points_accumulated,
                'remaining_points' => $member->points_available,
                'points_redeemed' => $member->points_redeemed,
                'expiring_rebates' => $member->points_expired,
                'mbr_total_savings' => $member->mbr_savings,
                'rebates_expiry_date' => $rebates_expiry
            ]);
        }
    }

    /**
     * @api {get} /api/transaction/redemptions/{mbrId}/{from}/{to} Get Redemptions
     * @apiGroup 2.TRANSACTION
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {string} From Date From eg: 2019-01-01
     * @apiParam {string} To Date To eg: 2020-01-01
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "loc_id": "TRS-O",
     *          "mbr_id": "V183535400",
     *          "trans_id": "1RA00148",
     *          "trans_date": "2018-01-01 16:28:07",
     *          "item_desc": "Star Voucher Redemption",
     *          "item_qty": 55,
     *          "regular_price": 1
     *      },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function redemptions($mbrId, $from='', $to='')
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $now = Carbon::now()->startOfDay();
        $fdate = $from !== '' ? Carbon::parse($from) : $now->copy()->subYears(5);
        $tdate = $to !== '' ? Carbon::parse($to) : $now;
        $mbrId = trim($mbrId);
        $data = Transaction::hachiRedemptions($mbrId, $fdate->toDateTimeString(), $tdate->toDateTimeString());
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/transaction/retrieve/{mbrId}/{from}/{to} Get Transaction
     * @apiGroup 2.TRANSACTION
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {string} From Date From eg: 2019-01-01
     * @apiParam {string} To Date To eg: 2020-01-01
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "trans_id": "P8C01111",
     *          "trans_type": "PS",
     *          "trans_date": "2020-03-17 14:51:13",
     *          "loc_id": "PIT",
     *          "total_amount": 8,
     *          "total_rebates": 0,
     *          "total_savings": 0,
     *          "items": [
     *              {
     *                  "line_num": 1,
     *                  "item_id": "!MEMBER-NEW08",
     *                  "item_desc": "8-Month Membership",
     *                  "unit_price": 8,
     *                  "regular_price": 8,
     *                  "item_qty": 1,
     *                  "disc_amount": 0,
     *                  "rebates": 0,
     *                  "mbr_savings": 0
     *              },]
     *      },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function personalTransaction($mbrId, $from='', $to='')
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $now = Carbon::now()->startOfDay();
        $fdate = $from !== '' ? Carbon::parse($from)->toDateTimeString() : $now->copy()->subYears(3)->toDateTimeString();
        $tdate = $to !== '' ? Carbon::parse($to)->toDateTimeString() : Carbon::now()->toDateTimeString();
        $mbrId = trim($mbrId);


        $data = $this->transactionRepository->retrieveTrans($mbrId, $fdate, $tdate);
        $data_his = $this->transactionHistoryRepository->retrieveTrans($mbrId, $fdate, $tdate);
        $data = array_merge($data, $data_his);
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/coupon/list/{mbrId} Get Coupon in List
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "expiry_date": "2020-04-16 23:59:59",
     *          "status_level": 0,
     *          "coupon_id": "!WP-VCNEP-JAN  ",
     *          "coupon_serialno": "MEM-9CTL6J2",
     *          "seq_num": "1",
     *          "voucher_amount": 0,
     *          "issue_date": "2020-03-17 16:39:05"
     *      },],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function voucherCoupon($mbrId)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $data = $this->voucherRepository->hachiVoucher($mbrId);
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/coupon/payment/{mbrId}/{status?} Payment Voucher
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {int} [status] Status Level, 0 is not in use, 1 is used. default is 0
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *           "coupon_id": "!P-PCDREAMCTL",
     *           "coupon_name": "PC Dream Payment Voucher",
     *           "expiry_date": "2021-08-26 23:59:59",
     *           "status_level": 0,
     *           "coupon_serialno": "C21000000000683",
     *           "receipt_id1": "",
     *           "receipt_id2": "",
     *           "voucher_amount": "10.00",
     *           "issue_date": "2021-07-27 14:59:03",
     *           "utilize_date": "1900-01-01 00:00:00"
     *      },],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function paymentCoupon($mbrId, $status = 0)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        if ((int)$status !== 0 && (int)$status !== 1) {
            return $this->errorWithCodeAndInfo('500', 'Status should be 0 or 1.');
        }
//        dd('test');
        $status = (int)$status;
        $data = Voucher::paymentVoucher($mbrId, $status);
//        dd($data);
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/coupon/tnc/{mbrId}/{serialNo} Get Coupon T&C
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {string} serialNo Coupon Serial No
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "coupon_serialno": "MEM-W7L6I8U",
     *          "issue_date": "2019-08-14 14:47:26",
     *          "expiry_date": "2019-11-30 00:00:00",
     *          "coupon_name": "Purchase Valore LTL18 LED Table Lamp at $18",
     *          "coupon_desc": "Purchase Valore LTL18 LED Table Lamp at $18",
     *          "image_url": ""
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or serial no is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member ID or Serial No is null."
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function rewardsTnc($mbrId, $serialNo)
    {
        if (empty(rtrim($mbrId)) || empty(rtrim($serialNo))) {
            return $this->errorWithCodeAndInfo('500', 'Member ID or Serial No is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $data = $this->voucherRepository->hachiRewards($mbrId, $serialNo);
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/ecredits/{mbrId}/{status?} ECredits
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {int} [status] Status Level, 0 is not in use, 1 is used. default is 0
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "expiry_date": "2019-11-30 00:00:00",
     *          "status_level": 1,
     *          "coupon_serialno": "DD18-50OFF",
     *          "receipt_id1": "",
     *          "receipt_id2": "HIA24034",
     *          "voucher_amount": 0,
     *          "issue_date": "2018-11-01 00:00:00",
     *          "utilize_date": "2018-11-22 11:14:35"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or serial no is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member ID or Serial No is null."
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function eCredits($mbrId, $status = 0)
    {
        if (empty(rtrim($mbrId))) {
            return $this->errorWithCodeAndInfo('500', 'Member ID or Serial No is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
//        $data = $this->voucherRepository->hachiECredits($mbrId);
        if ((int)$status !== 0 && (int)$status !== 1) {
            return $this->errorWithCodeAndInfo('500', 'Status should be 0 or 1.');
        }
        $status = (int)$status;
        $data = Voucher::myCredit($mbrId, $status);
        return $this->successWithData($data);
    }

    /**
     * @api {post} /api/redeem/voucher Redeem Voucher
     * @apiVersion 1.0.0
     * @apiGroup 4.REDEMPTION
     *
     * @apiParam {string} voucher_serialno Coupon Serial Number
     * @apiParam {string} mbr_id Member ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "voucher_serialno": "UOB001",
     *      "mbr_id": "V100000100",
     *  }
     *
     * @apiSuccessExample Reserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Redemption Successful. V$60 rebate has been added into your account",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Server error
     * HTTP/1.1 reserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Whoops! There was a problem processing your redeem. Please try again."
     *  }
     *
     * @apiErrorExample Redeem expired or used
     * HTTP/1.1 reserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid coupon code"
     *  }
     *
     * @apiErrorExample Serial Number invalid
     * HTTP/1.1 reserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid coupon code"
     *  }
     *
     */
    public function redeemVoucher()
    {
        $input = request()->only(['mbr_id', 'voucher_serialno']);
        $rules = [
            'mbr_id'            => 'required',
            'voucher_serialno'  => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $sn = $input['voucher_serialno'];
        $mbr_id = $input['mbr_id'];
        $is_exists = $this->voucherRedeemRepository->is_exist($sn);
        if ($is_exists) {
            $redeem = $this->voucherRedeemRepository->getRedeem($sn);
            if ($redeem->exists()) {
                $now = Carbon::now()->toDateTimeString();
                $this->dbconnMssqlCherps->beginTransaction();
                try {
                    $redeem_res = $redeem->first();
                    if (rtrim($redeem_res->redeem_type) === 'REBATE') {
                        $points = (int) ($redeem_res->redeem_amount * 100);
                        $this->memberRepository->findByMbrId($mbr_id)->increment('points_accumulated', $points, ['updated_on' => $now]);
                        $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
                        $get_points = $this->pointsRepository->getPoints($mbr_id, $exp_date);
                        if ($get_points->exists()) {
                            $get_points->increment('points_accumulated', $points, ['updated_on' => $now]);
                        }
                        else {
                            $data_points = [
                                'coy_id' => 'CTL',
                                'mbr_id' => $mbr_id,
                                'exp_date' => $exp_date,
                                'points_accumulated' => $points,
                                'points_redeemed' => 0,
                                'points_expired' => 0,
                                'created_by' => 'MIS',
                                'created_on' => $now,
                                'modified_by' => 'MIS',
                                'modified_on' => $now,
                                'updated_on' => $now,
                            ];
                            $this->pointsRepository->create($data_points);
                        }
                    }
                    $this->voucherRedeemRepository->updateRedeem($sn, $mbr_id);
                }
                catch (\Exception $e) {
                    $this->dbconnMssqlCherps->rollBack();
                    return $this->errorWithInfo("Whoops! There was a problem processing your redeem. Please try again.");
                }
                $this->dbconnMssqlCherps->commit();
                $voucher_amount = (string)($points/100);
                return $this->successWithInfo("Redemption Successful. V$".$voucher_amount." rebate has been added into your account");
            } else {
                return $this->errorWithCodeAndInfo(500,"Invalid coupon code.");
            }
        } else {
            return $this->errorWithCodeAndInfo(500,"Invalid coupon code.");
        }
    }

    /**
     * @api {post} /api/coupon/redeem Redeem Coupon
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} voucher_serialno Coupon Serial Number
     * @apiParam {string} mbr_id Member ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "voucher_serialno": "UOB001",
     *      "mbr_id": "V100000100",
     *  }
     *
     * @apiSuccessExample Reserved success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "coy_id": "CTL",
     *          "coupon_id": "!WPSEPBUBM99",
     *          "coupon_serialno": "BFB4VE-00992562",
     *          "promocart_id": "!WPSEPBUBM99",
     *          "mbr_id": "V100000100",
     *          "trans_id": "",
     *          "trans_date": "2021-04-23 12:42:55",
     *          "ho_ref": "",
     *          "ho_date": "2021-04-23 12:42:55",
     *          "issue_date": "2021-04-23 12:42:55",
     *          "sale_date": "2021-04-23 12:42:55",
     *          "utilize_date": "2021-04-23 12:42:55",
     *          "expiry_date": "2019-09-17 23:59:00",
     *          "print_date": "2019-09-17 23:59:00",
     *          "loc_id": "PS",
     *          "receipt_id1": "B4A04139",
     *          "receipt_id2": "",
     *          "voucher_amount": -10,
     *          "redeemed_amount": "0.00",
     *          "expired_amount": "0.00",
     *          "issue_type": "eVoucher",
     *          "issue_reason": "",
     *          "status_level": 0,
     *          "created_by": "3800",
     *          "created_on": "2021-04-23 12:42:55",
     *          "modified_by": "hachi",
     *          "modified_on": "2021-04-23 12:48:50",
     *          "guid": "6863F0C4-E9E5-4Q92-AD34-2FA8F18FB1CA"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Serial Number invalid
     * HTTP/1.1
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid coupon code"
     *  }
     *
     */
    public function redeemCoupon()
    {
        $input = request()->only(['mbr_id', 'voucher_serialno']);
        $rules = [
            'mbr_id'            => 'required',
            'voucher_serialno'  => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $sn = $input['voucher_serialno'];
        $mbr_id = $input['mbr_id'];
        $is_exists = $this->voucherRepository->is_exists($sn, $mbr_id);
        if ($is_exists) {
            return $this->successWithData($is_exists);
        } else {
            return $this->errorWithCodeAndInfo(500,"Invalid coupon code.");
        }
    }

    /**
     * @api {get} /api/member/emailPreference/{mbrId} Email Preference
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} mbrId Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "first_name": "ABC",
     *          "last_name": "DEF",
     *          "sub_ind1": "Y",
     *          "sub_ind2": "YYYYY"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function emailPreference($mbrId)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $data = $this->memberRepository->findByMbrId($mbrId);
        return $this->successWithData([
            'first_name' => $data->first_name,
            'last_name' => $data->last_name,
            'sub_ind1' => $data->sub_ind1,
            'sub_ind2' => $data->sub_ind2
        ]);
    }

    /**
     * @api {post} /api/member/emailPreference  Edit Email Preference
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} mbr_id Member id
     * @apiParam {string} [sub_ind1] Sub Ind1, 'Y' or 'N'
     * @apiParam {string} [sub_ind2] Sub Ind2, 'YYYYY','NNYYY','YNYYY' or 'NYYYY'
     * @apiParam {string} updated_by Modified By
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V2100038AS",
     *      "sub_ind1": "Y",
     *      "sub_ind2": "NNYYY",
     *      "updated_by": "3811",
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successfully updated",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     */
    public function editEmailPreference()
    {
        $input = request()->only(['mbr_id', 'sub_ind1', 'sub_ind2', 'updated_by']);
        $rules = [
            'mbr_id' => 'required',
            'sub_ind1' => ['nullable', Rule::in(['Y','N'])],
            'sub_ind2' => ['nullable', Rule::in(['YYYYY','NNYYY', 'YNYYY', 'NYYYY'])],
            'updated_by' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = rtrim($input['mbr_id']);
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if ($member === null) {
            return $this->errorWithInfo("Sorry, you are not officially member.");
        }
        $user = $input['updated_by'];
        $now = Carbon::now()->toDateTimeString();
        $sub_ind1 = isset($input['sub_ind1']) ? $input['sub_ind1'] : $member->sub_ind1;
        $sub_ind2 = isset($input['sub_ind2']) ? $input['sub_ind2'] : $member->sub_ind2;
        $this->memberRepository->update([
            'sub_ind1'    => $sub_ind1,
            'sub_ind2'    => $sub_ind2,
            'modified_on'  => $now,
            'modified_by'  => $user
        ], $mbr_id, $attribute = 'mbr_id');
        return $this->successWithInfo("Successfully updated");
    }

    public function emailActEGift()
    {
        $input = request()->only(['to_name', 'amount', 'image', 'message', 'from_name', 'code', 'url', 'email', 'day']);
        $rules = [
            'amount'    => 'required|integer',
            'code'      => 'required',
            'url'       => 'required',
            'email'     => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $daysleft = number_format(59 - $input['day'], 0);

        $email_data = [
            'email' => 'li.liangze@challenger.sg',
            'data' => [
                'global' => [
                    'r_name' => 'Liangze',
                    'r_amount' => 'S$10',
                    'r_image' => "",
                    'r_message' => "Hi",
                    'r_fromname' => "Li",
                    'r_email' => 'li.liangze@challenger.sg',
                    'r_remainingdays' => $daysleft
                ]
            ]
        ];

        $email_result = null;

//        $email_result = $this->emarsysService->sendEmail('POST','4833473/broadcast', $email_data);
//        $email_result = EmarSys::sendEmail('mailstream/send_egift', $email_data);

        $recipient_id = $email_result['result'][0]['recipient_id'];
        $error = $email_result['result'][0]['error'];
        if (isset($email_result) && $error != '') {
            $status = 'Failed';
            $p_status = 'XOK';
        } elseif ((isset($email_result) && $recipient_id != '')) {
            $status = 'Sent';
            $p_status = 'OK';
            $error = 'Recipient id >> ' . $recipient_id;
//            $this->account_model->update_egift_sent($code, $guid);
        } else {
            $status = 'Failed';
            $p_status = 'XOK';
        }

    }
}
