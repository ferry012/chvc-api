<?php

namespace App\Http\Controllers\Member;

use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Jobs\RegisterEmarsysUser;
use App\Models\Member\Points\Points;
use App\Models\Member\Transactions\Transaction;
use App\Models\Member\Voucher\VoucherData;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\CorporateListRepository;
use App\Repositories\Member\MemberMetaRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Points\RebateReservedRepository;
use App\Repositories\Member\RebateClaimRepository;
use App\Repositories\Member\SysTransListRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\CreditReservedRepository;
use App\Repositories\Member\Voucher\VoucherIssueDataRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Services\EmarSys\EmarsysServiceV2;

class PosController extends Controller
{
    use Result;
    private $coyId;
    private $memberRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $voucherRepository;
    private $pointsExpiryRepository;
    private $memberTypeRepository;
    private $rebateClaimRepository;
    private $memberMetaRepository;
    private $voucherIssueDataRepository;
    private $addressRepository;
    private $creditReservedRepository;
    private $rebateReservedRepository;
    private $sysTransListRepository;
    private $dbconnMssqlCherps;
    private $emarsysService;

    private $key_code = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                TransactionRepository $transactionRepository,
                                VoucherRepository $voucherRepository,
                                PointsExpiryRepository $pointsExpiryRepository,
                                MemberTypeRepository $memberTypeRepository,
                                RebateClaimRepository $rebateClaimRepository,
                                MemberMetaRepository $memberMetaRepository,
                                AddressRepository $addressRepository,
                                CreditReservedRepository $creditReservedRepository,
                                RebateReservedRepository $rebateReservedRepository,
                                SysTransListRepository $sysTransListRepository,
                                VoucherIssueDataRepository $voucherIssueDataRepository,
                                EmarsysServiceV2 $emarsysService)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->voucherRepository = $voucherRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->rebateClaimRepository = $rebateClaimRepository;
        $this->memberMetaRepository = $memberMetaRepository;
        $this->voucherIssueDataRepository = $voucherIssueDataRepository;
        $this->addressRepository = $addressRepository;
        $this->creditReservedRepository = $creditReservedRepository;
        $this->rebateReservedRepository = $rebateReservedRepository;
        $this->sysTransListRepository = $sysTransListRepository;
        $this->emarsysService = $emarsysService;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
        $this->coyId = 'CTL';
    }

    private function getRebateTierFromType($mbr_type) { 
        $check_mbr_type = (substr($mbr_type,0,1) == 'S') ? 'S' : $mbr_type;
        $rebate_table = $this->dbconnMssqlCherps->table('ims_item_list_member')->where('item_type', $check_mbr_type)->first();
        return ($rebate_table) ? $rebate_table->rebate_tier : 0;
    }

    /**
     * @api {get} /api/validate/{memberId}?source={pos_id}&info={info?} Validate Member
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} memberId Member id
     * @apiParam {string} [source] POS ID. If source, it will response 'source_count'
     * @apiParam {string} [info] Additional info to retrieve
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "966048869",
     *          "mbr_title": "MR",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "birth_date": "1990-01-15 00:00:00",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "status_level": 1,
     *          "join_date": "2018-04-26 11:39:11.28",
     *          "exp_date": "2020-04-26 23:59:59",
     *          "mbr_type": "M",
     *          "rebate_tier": 1, // hachi membership details: mbr_rebate_times
     *          "rebate": "10",
     *          "mbr_savings": 80, // hachi membership details: mbr_total_savings
     *          "can_renew": "Y", // hachi membership details: MembershipRenewal
     *          "mbr_months": 8,
     *          "can_upgrade": "Y", // hachi membership details: MemberUpgrade
     *          "main_member": {
     *              "mbr_id": "V177017100",
     *              "first_name": "EDMUND",
     *              "last_name": "MEMBER"
     *          },
     *          "last_renewal": "1900-01-01 00:00:00",
     *          "rebate_date": "2018-06-30 00:00:00",
     *          "points_available": 3151, // hachi membership details: remaining_points
     *          "points_accumulated": 100,
     *          "points_redeemed": 139,
     *          "expiring_rebates": 0,
     *          "source_count": 1,
     *          "coupon": [
     *              {
     *                  "expiry_date": "2019-09-30 23:59:59",
     *                  "coupon_id": "!WPPAPERONE",
     *                  "coupon_code": "MEM-PO600580740",
     *                  "coupon_name": "$2.95 off for PaperOne A4 paper",
     *                  "coupon_excerpt": "Purchase PaperOne A4 paper at $2.90 (U.P $6.50)"
     *              },
     *          ],
     *          "mbr_type_history": [
     *               {
     *                  "line_num": 1,
     *                  "eff_from": "2017-10-04 23:59:59",
     *                  "eff_to": "2019-10-04 23:59:59",
     *                  "mbr_type": "M"
     *               }
     *           ],
     *          "meta": [
     *              {
     *                  "gplaylimit": "1000000",
     *                  "norenewreminder": "2021-11-25 23:59:59.000"
     *              }
     *          ],
     *          "billing": {
     *              "street_line1": "xxx xxx xxx",
     *              "street_line2": "00",
     *              "street_line3": "000",
     *              "street_line4": "xxx xxx",
     *              "country_id": "SG",
     *              "postal_code": "123456"
     *          },
     *          "shipping": [],
     *          "rebates_expiry_date": [
     *              {
     *                  "exp_date": "Jun 30 2020 12:00:00:AM",
     *                  "points_available": 99
     *              },
     *              {
     *                  "exp_date": "Jun 30 2021 12:00:00:AM",
     *                  "points_available": 1070
     *              }],
     *         "payment_mode": {
     *              "dbs": "Y"
     *         }
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
    v_mbr_mvip */
    public function getMemberInfo($memberId, Request $request)
    {
        if (empty($memberId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($memberId, " ")) < 7) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }

        $member = $this->memberRepository->getMemberInfo($memberId);

        if (!empty($member)) {
            $mbr_id = rtrim($member['mbr_id'], " ");
            $additionalInfo = ($request->get('info')) ? explode(',', $request->get('info')) : ['**'];
            $data = $this->get_member_info($member, $additionalInfo);

            /*
             * Save login source count
             */
            if (!empty($request->source)) {
                $posId = substr($request->source,0,5);
                $now = Carbon::now()->toDateTimeString();
                $pos_log = $this->dbconnMssqlCherps->table('crm_pos_login_log')->where('pos_id', $posId)->where('mbr_id', $mbr_id);
                if ($pos_log->exists()) {
                    $pos_log->increment('source_count', 1, ['updated_at' => $now]);
                    $source_count = $pos_log->first()->source_count;
                } else {
                    $this->dbconnMssqlCherps->table('crm_pos_login_log')->insert([
                        'mbr_id' => $mbr_id,
                        'email_addr' => $member['email_addr'],
                        'contact_num' => $member['contact_num'],
                        'pos_id' => $posId,
                        'source_count' => 1,
                        'updated_at' => $now,
                        'created_at' => $now
                    ]);
                    $source_count = 1;
                }
                $data['source_count'] = $source_count;
            }

            return response()->json([
                'data' => $data,
                'status' => 'success',
                'status_code' => 200,
            ], 200);

        } else {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
    }

    /**
     * @api {get} /api/validate_mvip/{memberId} Validate MVIP
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} memberId Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     * {
     *         "data": {
     *             "coy_id": "CTL",
     *             "mbr_id": "V120355400",
     *             "mbr_title": "MR",
     *             "first_name": "JIAN TING WARREN",
     *             "last_name": "TAN",
     *             "birth_date": "1991-06-05 00:00:00",
     *             "nationality_id": "SG",
     *             "email_addr": "warren_tjt@hotmail.com",
     *             "contact_num": "97552033",
     *             "last_login": "2021-10-24 13:06:38",
     *             "status_level": 1,
     *             "join_date": "2012-08-05 17:00:02.39",
     *             "exp_date": "2025-01-15 23:59:59",
     *             "last_renewal": "2021-04-27 19:27:03",
     *             "mbr_type": "MVIP",
     *             "points_accumulated": "62706.00",
     *             "points_reserved": "0.00",
     *             "points_redeemed": "39312.00",
     *             "points_expired": "0.00",
     *             "mbr_savings": "250.37",
     *             "is_mvip": "Y",
     *             "mvip_eff_from": "2022-03-01 00:00:00",
     *             "mvip_eff_to": "2023-02-28 23:59:59",
     *             "spending_since_mvip": "25.5000000000",
     *             "mvip_eligibility": 15000,
     *             "is_mvip_eligible": "N",
     *             "spending_past_year": "19741.11",
     *             "spending_to_mvip": "0"
     *         },
     *         "status": "success",
     *         "status_code": 200
     *     }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     */
    public function getMvipInfo($memberId, Request $request)
    {
        if (empty($memberId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($memberId, " ")) < 7) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }

        $coy_id = $this->coyId;
        $sql = "SELECT * FROM v_mbr_mvip WHERE coy_id=? AND mbr_id=?";
        $member = $this->dbconnMssqlCherps->select($sql, [$coy_id,$memberId]);

        if (!empty($member)) { 
            return response()->json([
                'data' => $member[0],
                'status' => 'success',
                'status_code' => 200,
            ], 200); 
        } else {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
    }

    /**
     * @api {get} /api/points/available/{mbrId} Points Remaining
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} mbrId Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "966048869",
     *          "rebate_amount": "51.69"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample No member found.
     * HTTP/1.1 No member found.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "No member found."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function points_available($mbrId)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }

        $member = $this->memberRepository->findByMbrId($mbrId);

        return $this->successWithData([
            'mbr_id' => $mbrId,
            'rebate_amount' => number_format($member->points_remaining/100, 2, '.', '')
        ]);
    }

    /**
     * @api {get} /api/member/verify/{mbrId}/{pwd} Verify Member
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} memberId Member id, email addr or contact num
     * @apiParam {string} pwd password Must md5()
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "966048869",
     *          "mbr_title": "MR",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "birth_date": "1990-01-15 00:00:00",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "status_level": 1,
     *          "join_date": "2018-04-26 11:39:11.28",
     *          "exp_date": "2020-04-26 23:59:59",
     *          "mbr_type": "M",
     *          "rebate_tier": 1, // hachi membership details: mbr_rebate_times
     *          "rebate": "10",
     *          "mbr_savings": 80, // hachi membership details: mbr_total_savings
     *          "can_renew": "Y", // hachi membership details: MembershipRenewal
     *          "mbr_months": 8,
     *          "can_upgrade": "Y", // hachi membership details: MemberUpgrade
     *          "main_member": {
     *              "mbr_id": "V177017100",
     *              "first_name": "EDMUND",
     *              "last_name": "MEMBER"
     *          },
     *          "last_renewal": "1900-01-01 00:00:00",
     *          "rebate_date": "2018-06-30 00:00:00",
     *          "points_available": 3151, // hachi membership details: remaining_points
     *          "points_accumulated": 100,
     *          "points_redeemed": 139,
     *          "expiring_rebates": 0,
     *          "coupon": [
     *              {
     *                  "expiry_date": "2019-09-30 23:59:59",
     *                  "coupon_id": "!WPPAPERONE",
     *                  "coupon_code": "MEM-PO600580740",
     *                  "coupon_name": "$2.95 off for PaperOne A4 paper",
     *                  "coupon_excerpt": "Purchase PaperOne A4 paper at $2.90 (U.P $6.50)"
     *              },
     *          ],
     *          "mbr_type_history": [
     *               {
     *                  "line_num": 1,
     *                  "eff_from": "2017-10-04 23:59:59",
     *                  "eff_to": "2019-10-04 23:59:59",
     *                  "mbr_type": "M"
     *               }
     *           ],
     *          "meta": [
     *              {
     *                  "gplaylimit": "1000000",
     *                  "norenewreminder": "2021-11-25 23:59:59.000"
     *              }
     *          ],
     *          "billing": {
     *              "street_line1": "xxx xxx xxx",
     *              "street_line2": "00",
     *              "street_line3": "000",
     *              "street_line4": "xxx xxx",
     *              "country_id": "SG",
     *              "postal_code": "123456"
     *          },
     *          "shipping": [],
     *          "rebates_expiry_date": [
     *              {
     *                  "exp_date": "Jun 30 2020 12:00:00:AM",
     *                  "points_available": 99
     *              },
     *              {
     *                  "exp_date": "Jun 30 2021 12:00:00:AM",
     *                  "points_available": 1070
     *              }]
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Member ID or password is null.
     * HTTP/1.1 Member ID or password is null.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member ID or password is null."
     *  }
     *
     * @apiErrorExample No member found.
     * HTTP/1.1 No member found.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "No member found."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     * @apiErrorExample Password invalid.
     * HTTP/1.1 Password invalid.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Password invalid."
     *  }
     * @apiErrorExample Member locked.
     * HTTP/1.1 Member locked.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member locked."
     *  }
     * @apiErrorExample Member expired.
     * HTTP/1.1 Member expired.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member expired."
     *  }
     */
    public function chckMbrPwd2Member($mbrId, $pwd2){
        $member = $this->memberRepository->chckUpdateMbrPassword2($mbrId,$pwd2);
    }    

    public function verifyMember($mbrId, $pwd, $pwd2 = null)
    {
        if (empty(rtrim($mbrId)) || empty(rtrim($pwd))) {
            return $this->errorWithCodeAndInfo('500', 'Member ID or password is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found.');
        }

        $mbr_id = rtrim($mbrId, " ");
        $member = $this->memberRepository->getMemberInfo($mbr_id);

        if (empty($member)) {
            return $this->errorWithCodeAndInfo('500', 'No member found.');
        }

        if(!empty($pwd2)){          
            if (trim($member['mbr_pwd']) !== trim($pwd2) && $member['mbr_pwd'] !== $pwd) {
                return $this->errorWithCodeAndInfo('500', 'Password invalid.');
            }              
        }
        else{         
            if ($member['mbr_pwd'] !== $pwd) {
                return $this->errorWithCodeAndInfo('500', 'Password invalid.');
            }  
        }    

        if ($member['login_locked'] === 'Y') {
            return $this->errorWithCodeAndInfo('500', 'Member locked.');
        }

//        if ($now->gt($expired_date)) {
//            return $this->errorWithCodeAndInfo('500', 'Member expired.');
//        }

        $data = $this->get_member_info($member);
        return $this->successWithData($data);
    }

    public function get_member_info($member, $additionalInfo = ['**'])
    {
        $coy_id = $this->coyId;
        $mbr_id = rtrim($member['mbr_id'], " ");
        $email_addr = $member['email_addr'];
        $contact_num = $member['contact_num'];
        $mbr_type = $this->memberTypeRepository->getPosMemberType($mbr_id);
        $mbr_type = $mbr_type == '' ? rtrim($member['mbr_type'], " ") : $mbr_type;

        $main_member_data = [];
        if ($mbr_type == 'MAS') {
            $main_mbr = $this->memberRepository->findByMbrId($member['main_id']);
            $rebate_tier = $this->getRebateTierFromType($this->memberTypeRepository->getPosMemberType(rtrim($main_mbr->mbr_id)));
            $main_member_data = [
                'mbr_id' => rtrim($main_mbr->mbr_id),
                'first_name' => $main_mbr->first_name,
                'last_name' => $main_mbr->last_name
            ];
        } else {
            $rebate_tier = $this->getRebateTierFromType($mbr_type);
        }

        $rebate_date = $this->pointsRepository->getRebateDate($mbr_id);
        $rebates_expiry = $this->pointsRepository->getExpDate($mbr_id);

        $data = [
            'mbr_id' => $mbr_id,
            'mbr_title' => rtrim($member['mbr_title']),
            'first_name' => rtrim($member['first_name'], " "),
            'last_name' => rtrim($member['last_name'], " "),
            'birth_date' => $member['birth_date'],
            'email_addr' => $email_addr,
            'contact_num' => $contact_num,
            'status_level' => $member['status_level'],
            'join_date' => $member['join_date'],
            'exp_date' => $member['exp_date'],
            'mbr_type' => $mbr_type,
            'mbr_savings' => $member['mbr_savings'],
            'main_member' => $main_member_data,
            'mbr_months' => $member['mbr_month'],
            'last_renewal' => $member['last_renewal'],
            'can_renew' => $member['can_renew'],
            'can_upgrade' => $member->can_upgrade === 1 ? "Y" : "N",

            'rebate_tier' => $rebate_tier,
            'rebate' => number_format($member->points_remaining / 100, 2, '.', ''),
            'rebate_date' => $rebate_date['exp_date'] ?? null,
            'rebates_expiry_date' => $rebates_expiry,
            'points_available' => $member->points_available,
            'points_accumulated' => $member['points_accumulated'],
            'points_redeemed' => $member['points_redeemed'],
            'expiring_rebates' => $member['points_expired'],
        ];

        if (in_array('coupon', $additionalInfo) || in_array('**', $additionalInfo)) {
            $voucher = Voucher::getPosVoucher($mbr_id);
            $data['coupon'] = $voucher;
        }

        if (in_array('mbr_type', $additionalInfo) || in_array('**', $additionalInfo)) {
            $mbr_type_his = $this->memberTypeRepository->getPosMemberTypeHistory($mbr_id);
            $data['mbr_type_history'] = $mbr_type_his;
        }

        if (in_array('meta', $additionalInfo) || in_array('**', $additionalInfo)) {
            $renew_remainder = $this->memberMetaRepository->getRenewRemainder($mbr_id);
            $data['meta'] = array($renew_remainder);
        }

        if (in_array('billing', $additionalInfo) || in_array('**', $additionalInfo)) {
            $res_mbr_addr = [];
            $mbr_addr = rtrim($member['mbr_addr']);
            if ($mbr_addr !== '') {
                $mbr_addr_detail = $this->addressRepository->getAddress($mbr_id, $mbr_addr);
                $res_mbr_addr = [
                    'street_line1' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line1) : '',
                    'street_line2' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line2) : '',
                    'street_line3' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line3) : '',
                    'street_line4' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line4) : '',
                    'country_id' => isset($mbr_addr_detail) ? $mbr_addr_detail->country_id : '',
                    'postal_code' => isset($mbr_addr_detail) ? $mbr_addr_detail->postal_code : ''
                ];
            }
            $data['billing'] = empty($res_mbr_addr) ? [] : $res_mbr_addr;
        }

        if (in_array('shipping', $additionalInfo) || in_array('**', $additionalInfo)) {
            $res_delv_addr = [];
            $delv_addr = rtrim($member['delv_addr']);
            if ($delv_addr !== '') {
                $delv_addr_detail = $this->addressRepository->getAddress($mbr_id, $delv_addr);
                $res_delv_addr = [
                    'street_line1' => rtrim($delv_addr_detail->street_line1 ?? ''),
                    'street_line2' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line2) : '',
                    'street_line3' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line3) : '',
                    'street_line4' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line4) : '',
                    'country_id' => isset($delv_addr_detail) ? $delv_addr_detail->country_id : '',
                    'postal_code' => isset($delv_addr_detail) ? $delv_addr_detail->postal_code : ''
                ];
            }
            $data['shipping'] = empty($res_delv_addr) ? [] : $res_delv_addr;
        }

        if (in_array('payment.dbs', $additionalInfo)) {

            $sql = "select a.coy_id,a.mbr_id, count(distinct a.trans_id) trans_count, max(a.trans_date) last_trans
                    from crm_member_transaction a
                    join crm_member_trans_payment b on a.coy_id=b.coy_id and a.trans_id=b.trans_id
                    join ims_item_list c on a.coy_id=c.coy_id and a.item_id=c.item_id
                    where a.coy_id = ? and a.trans_date >= '2021-01-01'
                        and c.inv_dim2 in ('APPLE','SMARTPHONE','DESKTOP','NOTEBOOK')
                        and b.trans_ref1 in ('DBS_CC','DBS_IPP')
                        and b.trans_amount >= 139
                        and a.mbr_id = ?
                    group by a.coy_id,a.mbr_id";
            $payments = $this->dbconnMssqlCherps->select($sql, [$coy_id,$mbr_id]);

            $data['payment_mode']['dbs'] = ($payments && count($payments) > 0) ? 'Y' : 'N';
        }

        return $data;
    }

    /**
     * @api {post} /api/register Pre-Register
     * @apiVersion 1.0.0
     * @apiGroup 1.MEMBERSHIP
     *
     * @apiParam {string} contact Contact number
     * @apiParam {string} email Email address
     * @apiParam {string} source pos id, or HI, or VC, etc
     * @apiParam {string} [first_name] Option First name
     * @apiParam {string} [last_name] Option Last name
     * @apiParam {string} [mbr_type] Option Member type
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "contact": "81431879",
     *      "email": "test@test.com",
     *      "source": "B4",
     *      "first_name": "xx",
     *      "last_name": "xxxxx",
     *      "mbr_type": "M08"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "V1900001B4",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "join_date": "2018-04-26 11:39:11.28",
     *          "exp_date": "2020-04-26 23:59:59",
     *          "mbr_type": "M08",
     *          "rebate": 0,
     *          "can_renew": "X",
     *          "rebate_date": null,
     *          "exists": 1 //if 1 exists, 0 new member.
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function register()
    {
        $input = request()->only(['contact', 'email', 'first_name', 'last_name', 'mbr_type', 'source', 'birth_date', 'mbr_pwd','mbr_title']);
        $now = Carbon::now()->toDateTimeString();
        $email = $input['email'];
        $contact = $input['contact'];
        $member = $this->memberRepository->check_member_exists($email, $contact);
        if (!empty($member)) {

            if ($member->status_level >= -1 || $member->status_level == -9) {
                if($member->contact_num == $contact && $member->email_addr == $email){
                    return $this->errorWithInfo("Email and Contact number exist.");
                }else if($member->contact_num == $contact){
                    return $this->errorWithInfo("Contact number exists");
                }else if($member->email_addr == $email){
                    return $this->errorWithInfo("Email exists");
                }
                return $this->errorWithCodeAndInfo(422, 'Member exist, please use renew membership');
            }

            return $this->successWithData([
                'mbr_id' => rtrim($member->mbr_id),
                'first_name' => $member->first_name,
                'last_name' => $member->last_name,
                'email_addr' => $member->email_addr,
                'contact_num' => $member->contact_num,
                'join_date' => $member->join_date,
                'exp_date' => $member->exp_date,
                'mbr_type' => rtrim($member->mbr_type),
                "mbr_title" => $member->mbr_title,
                'birth_date' => $member->birth_date,
                'rebate' => 0,
                'can_renew' => 'X',
                'rebate_date' => null,
                'exists' => 1
            ]);
        }
        $rules = [
            'contact' => 'required|numeric|min:8',
            'email' => 'required|email',
            'source' => 'required|min:2|max:2',
            'first_name' => 'max:40',
            'last_name' => 'max:40'
        ];
        $message = [
            'contact.required' => 'Contact required.',
            'contact.unique' => 'This contact number exists in our system.',
            'contact.numeric' => 'Contact number must be a number.',
            'contact.min' => 'Contact number must consist of minimum 8 digits.',
            'email.required' => 'Email address required.',
            'email.email' => 'Email address must comply with the rules.',
            'email.unique' => 'This email address exists in our system.',
            'source.required' => 'Missing Source ID required',
            'source.min' => 'Invalid Source ID - only 2 characters',
            'source.max' => 'Invalid Source ID - only 2 characters'
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $res = $this->memberRepository->delete_pre_mbr($email, $contact);
        if ($res == 5) {
            return $this->errorWithInfo("Email exists");
        } elseif ($res == 6) {
            return $this->errorWithInfo("Contact number exists");
        }
        $source = $input['source'];
        $mbr_id = $this->sysTransListRepository->generate_mbr_id(strtoupper($source));
        $default_date = new \DateTime('1900-01-01T00:00:00');
        $endOfDay = Carbon::now()->endOfDay()->toDateTimeString();
        $first_name = $input['first_name'] ? $input['first_name'] : '';
        $last_name = $input['last_name'] ? $input['last_name'] : '';
        $mbr_type = $input['mbr_type'] ? $input['mbr_type'] : '';
        $birth_date = isset($input['birth_date']) ? Carbon::parse($input['birth_date']) : $default_date;
        $mbr_title= isset($input["mbr_title"]) ? $input["mbr_title"] : '';
        $mbr_pwd = isset($input['mbr_pwd']) ? md5($input['mbr_pwd']) : md5(substr($contact, -4));
        $res = $this->_register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, -9, $mbr_id, '', $mbr_title);

        if ($res) {
            $res_data = [
                'mbr_id' => $mbr_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email_addr' => $email,
                'contact_num' => $contact,
                'join_date' => $now,
                'exp_date' => $endOfDay,
                'mbr_type' => $mbr_type,
                "mbr_title" => $mbr_title,
                'birth_date' => $birth_date,
                'rebate' => 0,
                'can_renew' => 'X',
                'rebate_date' => null,
                'exists' => 0
            ];
            return $this->successWithData($res_data);
        } else {
            return $this->errorWithInfo('Server problem, cannot register member.');
        }
    }

    public function _register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, $status_level, $created_by='', $main_id='',$mbr_title='')
    {
        $now = Carbon::now()->toDateTimeString();

        $data = [];
        $data['coy_id'] = $this->coyId;
        $data['mbr_id'] = $mbr_id;
        $data['mbr_title'] = $mbr_title;
        $data['first_name'] = $first_name;
        $data['last_name'] = $last_name;
        $data['birth_date'] = $birth_date;
        $data['nationality_id'] = '';
        $data['email_addr'] = trim(strtolower($email));
        $data['contact_num'] = $contact;
        $data['mbr_pwd'] = $mbr_pwd;
        $data['pwd_changed'] = $default_date;
        $data['last_login'] = $default_date;
        $data['mbr_addr'] = '';
        $data['delv_addr'] = '';
        $data['send_info'] = '';
        $data['send_type'] = '';
        $data['status_level'] = $status_level;
        $data['join_date'] = $now;
        $data['exp_date'] = $endOfDay;
        $data['last_renewal'] = $default_date;
        $data['points_accumulated'] = 0;
        $data['points_reserved'] = 0;
        $data['points_redeemed'] = 0;
        $data['points_expired'] = 0;
        $data['mbr_savings'] = 0;
        $data['rebate_voucher'] = '';
        $data['mbr_type'] = $mbr_type;
        $data['main_id'] = $main_id;
        $data['sub_ind1'] = 'Y';
        $data['sub_ind2'] = 'YYYYY';
        $data['sub_date'] = $now;
        $data['created_by'] = $created_by;
        $data['created_on'] = $now;
        $data['modified_by'] = $created_by;
        $data['modified_on'] = $now;
        $data['updated_on'] = $now;
        $data['login_locked'] = 'N';
        $res = $this->memberRepository->saveModel($data);
        return $res;
    }

    /**
     * @api {post} /api/points/award Points Award
     * @apiVersion 1.0.0
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} trans_id Transaction ID
     * @apiParam {int} rebate_amount Rebate amount
     * @apiParam {string} rebate_description Rebate Description
     * @apiParam {int} [transfer] Transfer 0 or 1, if 1, will ignore 'MAS'
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "P9C01234",
     *      "rebate_amount": 3.50,
     *      "rebate_description": "Valueclub App Game",
     *      "transfer": 1
     *  }
     *
     * @apiSuccessExample Award success
     * HTTP/1.1 200 OK
     *  {
     *      "info":"Successful save award",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Whoops! There was a problem processing your award. Please try again."
     *  }
     *
     */
    public function points_award()
    {
        $input = request()->only(['mbr_id', 'trans_id', 'rebate_amount', 'rebate_description', 'transfer']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required|unique:pgsql.crm_member_transaction,trans_id',
            'rebate_amount' => 'required|numeric|min:0',
            'rebate_description' => 'required',
            'transfer' => ['nullable', Rule::in([0, 1])]
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $award_points = (int) ($input['rebate_amount']*100);
        $trans_id = $input['trans_id'];
        $rebate_desc = $input['rebate_description'];
        $now = Carbon::now();
        $this->dbconnMssqlCherps->beginTransaction();
        try {
            // create transaction
            $this->transactionRepository->create([
                'coy_id' => $this->coyId,
                'mbr_id' => $mbr_id,
                'trans_id' => $trans_id,
                'trans_type' => 'AP',
                'trans_date' => $now,
                'loc_id' => '',
                'line_num' => 1,
                'pos_id' => '',
                'item_id' => '',
                'item_desc' => $rebate_desc,
                'item_qty' => 0,
                'regular_price' => 0,
                'unit_price' => 0,
                'disc_percent' => 0,
                'disc_amount' => 0,
                'trans_points' => $award_points,
                'salesperson_id' => 'MIS',
                'mbr_savings' => 0,
                'created_by' =>$mbr_id,
                'created_on' => $now,
                'modified_by' => $mbr_id,
                'modified_on' => $now,
                'updated_on' => $now,
            ]);
            // create/update crm_member_points and update crm_member_list
            $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
            $member = $this->memberRepository->findByMbrId($mbr_id);
            $transfer = isset($input['transfer']) ? $input['transfer'] : 0;
            if (rtrim($member->mbr_type, " ") == 'MAS' && $transfer == 0) {
                $main_id = $member->main_id;
                $points = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $main_id, 'exp_date' => $exp_date]);
                $main_member = $this->memberRepository->findByMbrId($main_id);
                if ($points->exists()) {
                    $points->increment('points_accumulated', $award_points);
                }
                else {
                    $data_points = [
                        'coy_id' => $this->coyId,
                        'mbr_id' => $main_id,
                        'exp_date' => $exp_date,
                        'points_accumulated' => $award_points,
                        'points_redeemed' => 0,
                        'points_expired' => 0,
                        'created_by' => 'MIS',
                        'created_on' => $now,
                        'modified_by' => 'MIS',
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ];
                    $this->pointsRepository->create($data_points);
                }
                $main_member->increment('points_accumulated', $award_points);
            }
            else {
                $points = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
                if ($points->exists()) {
                    $points->increment('points_accumulated', $award_points);
                } else {
                    $data_points = [
                        'coy_id' => $this->coyId,
                        'mbr_id' => $mbr_id,
                        'exp_date' => $exp_date,
                        'points_accumulated' => $award_points,
                        'points_redeemed' => 0,
                        'points_expired' => 0,
                        'created_by' => 'MIS',
                        'created_on' => $now,
                        'modified_by' => 'MIS',
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ];
                    $this->pointsRepository->create($data_points);
                }
                $member->increment('points_accumulated', $award_points);
            }

        }
        catch (\Exception $e) {
            Log::error($e);
            $this->dbconnMssqlCherps->rollBack();
            return $this->errorWithInfo("Whoops! There was a problem processing your award. Please try again.");
        }
        $this->dbconnMssqlCherps->commit();
        return $this->successWithInfo("Successful save award");
    }

    /**
     * @api {post} /api/points/deduct Points Deduct
     * @apiVersion 1.0.0
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} trans_id Transaction ID
     * @apiParam {int} rebate_amount Rebate amount
     * @apiParam {string} rebate_description Rebate Description
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "P9C01234",
     *      "rebate_amount": -3.50,
     *      "rebate_description": "Valueclub App Game"
     *  }
     *
     * @apiSuccessExample Award success
     * HTTP/1.1 200 OK
     *  {
     *      "info":"Successful save deduct",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Whoops! There was a problem processing your deduct. Please try again."
     *  }
     *
     */
    public function points_deduct()
    {
        $input = request()->only(['mbr_id', 'trans_id', 'rebate_amount', 'rebate_description']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required|unique:pgsql.crm_member_transaction,trans_id',
            'rebate_amount' => 'required|numeric|max:0',
            'rebate_description' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $award_points = (int) ($input['rebate_amount']*100);
        $trans_id = $input['trans_id'];
        $rebate_desc = $input['rebate_description'];
        $now = Carbon::now();
        $this->dbconnMssqlCherps->beginTransaction();
        try {
            // create transaction
            $this->transactionRepository->create([
                'coy_id' => $this->coyId,
                'mbr_id' => $mbr_id,
                'trans_id' => $trans_id,
                'trans_type' => 'AP',
                'trans_date' => $now,
                'loc_id' => '',
                'line_num' => 1,
                'pos_id' => '',
                'item_id' => '',
                'item_desc' => $rebate_desc,
                'item_qty' => 0,
                'regular_price' => 0,
                'unit_price' => 0,
                'disc_percent' => 0,
                'disc_amount' => 0,
                'trans_points' => $award_points,
                'salesperson_id' => 'MIS',
                'mbr_savings' => 0,
                'created_by' => $mbr_id,
                'created_on' => $now,
                'modified_by' => $mbr_id,
                'modified_on' => $now,
                'updated_on' => $now,
            ]);
            // create/update crm_member_points and update crm_member_list
            $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
            $member = $this->memberRepository->findByMbrId($mbr_id);

            $points = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
            if ($points->exists()) {
                $points->increment('points_accumulated', $award_points);
            } else {
                $data_points = [
                    'coy_id' => $this->coyId,
                    'mbr_id' => $mbr_id,
                    'exp_date' => $exp_date,
                    'points_accumulated' => $award_points,
                    'points_redeemed' => 0,
                    'points_expired' => 0,
                    'created_by' => 'MIS',
                    'created_on' => $now,
                    'modified_by' => 'MIS',
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->pointsRepository->create($data_points);
            }
            $member->increment('points_accumulated', $award_points);

        }
        catch (\Exception $e) {
            Log::error($e);
            $this->dbconnMssqlCherps->rollBack();
            return $this->errorWithInfo("Whoops! There was a problem processing your deduct. Please try again.");
        }
        $this->dbconnMssqlCherps->commit();
        return $this->successWithInfo("Successful save deduct");
    }

    /**
     * @api {get} /api/points/check Points Check
     * @apiVersion 1.0.0
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "P9C01234",
     *  }
     *
     * @apiSuccessExample Check success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "V100000100",
     *          "trans_id": "P9C01234",
     *          "rebate_available": "11.69",
     *          "trans_rebate": "3.50",
     *          "refund_status": true
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample No Record
     * HTTP/1.1 no record
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Whoops, There is no record in our server."
     *  }
     *
     */
    public function points_check()
    {
        $input = request()->only(['mbr_id', 'trans_id']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $trans_id = $input['trans_id'];
        $points = Transaction::where(['mbr_id'=>$mbr_id, 'trans_id'=>$trans_id, 'coy_id'=>$this->coyId]);
        $member = $this->memberRepository->findByMbrId($mbr_id);
//            ->first()->trans_points;
        if ($points->exists()) {
            $total_points = 0;
            foreach ($points->get() as $point) {
                $total_points = $total_points + $point->trans_points;
            }
            $rebate_available = number_format($member->points_remaining / 100, 2, '.', '');
            $trans_rebate = number_format((int)$total_points/100, 2, '.', '');
            $refund_status = $rebate_available >= $trans_rebate ? true : false;
            return $this->successWithData([
                'mbr_id'=>$mbr_id,
                'trans_id' => $trans_id,
                'rebate_available' => $rebate_available,
                'trans_rebate' => $trans_rebate,
                'refund_status' => $refund_status
            ]);
        } else {
            return $this->errorWithInfo("Whoops, There is no record in our server.");
        }

    }

    /**
     * @api {post} /api/points/reserved Points Reserved
     * @apiVersion 1.0.0
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} trans_id Transaction ID
     * @apiParam {float} rebate_amount Rebate Amount
     * @apiParam {string} [expiry_type] Set Expiry Date, 0 or 1. If 1 add 15 days.
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "test99",
     *      "rebate_amount": 5
     *  }
     *
     * @apiSuccessExample Reserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Transaction reserved successfully",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample rebate is not enough
     * HTTP/1.1 Key rebate is not enough
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "sorry, your rebate is not enough"
     *  }
     *
     * @apiErrorExample Reserved error
     * HTTP/1.1 reserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Transaction reserved error."
     *  }
     *
     */
    public function points_reserved()
    {
        $input = request()->only(['mbr_id', 'trans_id', 'rebate_amount', 'expiry_type']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required',
            'rebate_amount' => 'required|numeric',
            'expiry_type' => ['nullable', Rule::in([0,1])]
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $rebate_amount = $input['rebate_amount'];
        $trans_id = $input['trans_id'];
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if ($member === null) {
            return $this->errorWithCodeAndInfo('500', 'Sorry, we could not find your member id.');
        } else {
            $points_remaining = (int)$member->points_remaining;
            if ($points_remaining < (int)($rebate_amount * 100)) {
                return $this->errorWithCodeAndInfo('500', 'Sorry, your rebate is not enough.');
            }
        }
        /*
         * Transaction expires after X min
         */
        if (isset($input['expiry_type']) && $input['expiry_type'] === 1) {
            $trans_expires_time = Carbon::now()->addDays(1)->toDateTimeString();
        } else {
            $trans_expires_time = Carbon::now()->addMinutes(config('challenger.checkout_timeout_after'))->toDateTimeString();
        }
        $data = [
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'rebate_amount' => $rebate_amount,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'expires' => $trans_expires_time,
            'status' => 1
        ];
        if ($this->rebateReservedRepository->is_exists($trans_id)) {
            $res = $this->rebateReservedRepository->updateOrSave('update', $data);
        } else {
            $res = $this->rebateReservedRepository->updateOrSave('save', $data);
        }
        if ($res) {
            return $this->successWithInfo('Transaction reserved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Transaction reserved error.');
        }
    }

    /**
     * @api {post} /api/points/unreserved Points Unreserved
     * @apiVersion 1.0.0
     * @apiGroup 3.MBR REBATES
     *
     * @apiParam {string} [mbr_id] Option Member Id
     * @apiParam {string} trans_id Basket ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "test99",
     *  }
     *
     * @apiSuccessExample Unreserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Transaction unreserved successfully",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Unreserved error
     * HTTP/1.1 unreserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Transaction unreserved error."
     *  }
     *
     */
    public function points_unreserved()
    {
        $input = request()->only(['trans_id']);
        $rules = [
            'trans_id' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $res = $this->rebateReservedRepository->unreserve($input['trans_id']);
        if ($res) {
            return $this->successWithInfo('Transaction unreserved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Transaction unreserved error.');
        }
    }

    /**
     * @api {post} /api/ecredits/reserved eCredits Reserved
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} trans_id Transaction ID
     * @apiParam {string} [expiry_type] Set Expiry Date, 0 or 1. If 1 add 15 days.
     * @apiParam {Object[]} [ecredits] ECredits
     * @apiParam {string} ecredits.coy_id Coy ID
     * @apiParam {string} ecredits.coupon_serialno Coupon Serial Number
     * @apiParam {Object[]} [egifts] EGifts
     * @apiParam {string} egifts.coy_id Coy ID
     * @apiParam {string} egifts.coupon_serialno Coupon Serial Number
     * @apiParam {Object[]} [vouchers] Vouchers
     * @apiParam {string} vouchers.coy_id Coy ID
     * @apiParam {string} vouchers.coupon_serialno Coupon Serial Number
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "test99",
     *      "ecredits": [{
     *          "coy_id": "CTL",
     *          "coupon_serialno": "VCH-CR000000492"
     *      },]
     *      "egifts": [],
     *      "vouchers": [{
     *          "coy_id": "CTL",
     *          "coupon_serialno": "PC-DREAM"
     *      ]
     *  }
     *
     * @apiSuccessExample Reserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "eCredit or eGift reserved successfully",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Id invalid
     * HTTP/1.1 Member Id invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, we could not find your member id."
     *  }
     *
     * @apiErrorExample trans_id exists.
     * HTTP/1.1 trans_id exists
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, trans_id has exists."
     *  }
     *
     * @apiErrorExample eCredit invalid
     * HTTP/1.1 eCredit invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, your eCredit is invalid or reserved. Please check."
     *  }
     *
     * @apiErrorExample eGift invalid
     * HTTP/1.1 eCredit invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, your eGift is invalid or reserved. Please check."
     *  }
     *
     * @apiErrorExample Voucher invalid
     * HTTP/1.1 Voucher invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, your voucher is invalid or reserved. Please check."
     *  }
     *
     * @apiErrorExample Server error
     * HTTP/1.1 Server error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Whoops! There was a problem processing your reserve. Please try again."
     *  }
     *
     * @apiErrorExample egifts and ecredit empty
     * HTTP/1.1
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Please at least insert egifts of ecredits."
     *  }
     *
     */
    public function credit_reserved()
    {
        $input = request()->only(['mbr_id', 'trans_id', 'expiry_type', 'egifts', 'ecredits', 'vouchers']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required',
            'expiry_type' => ['nullable', Rule::in([0,1])]
//            'egifts' => 'required',
//            'ecredits' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $trans_id = $input['trans_id'];
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if ($member === null) {
            return $this->errorWithCodeAndInfo('500', 'Sorry, we could not find your member id.');
        }
        if ($this->creditReservedRepository->isExists($trans_id)) {
            return $this->errorWithCodeAndInfo(500, "Sorry, trans_id has exists.");
        }

        $this->dbconnMssqlCherps->beginTransaction();
        try {
            $egifts = isset($input['egifts']) ? $input['egifts'] : [];
            $ecredits = isset($input['ecredits']) ? $input['ecredits'] : [];
            $vouchers = isset($input['vouchers']) ? $input['vouchers'] : [];
            if (empty($egifts) && empty($ecredits) && empty($vouchers)) {
                return $this->errorWithCodeAndInfo(500, 'Please at least insert egifts, ecredits or vouchers.');
            }
            $trans_expires_time = Carbon::now()->addMinutes(config('challenger.checkout_timeout_after'))->toDateTimeString();
            if (isset($input['expiry_type'])) {
                if ($input['expiry_type']) {
                    $trans_expires_time = Carbon::now()->addDays(1)->toDateTimeString();
                }
            }
            foreach ($egifts as $g) {
                $coy_id_g = $g['coy_id'];
                $coupon_serialno_g = $g['coupon_serialno'];
                $has_owner = $this->voucherRepository->hasOwner($coupon_serialno_g, $coy_id_g);
                if (!$has_owner) {
                    $this->voucherRepository->updateOwner($coupon_serialno_g, $coy_id_g, $mbr_id);
                }
                $check_gift_exist = Voucher::checkGiftExists($coy_id_g, $mbr_id, $coupon_serialno_g);
                if (!$check_gift_exist) {
                    return $this->errorWithCodeAndInfo('500', 'Sorry, your eGift "' . $g['coupon_serialno'] . '" is invalid or reserved. Please check.');
                }
                $data = [
                    'coy_id' => $coy_id_g,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
//                    'coupon_id' => $coupon_id,
                    'coupon_serialno' => $coupon_serialno_g,
//                    'voucher_amount' => $voucher_amount,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'expires' => $trans_expires_time,
                    'status' => 1
                ];
                $this->creditReservedRepository->updateOrSave('save', $data);
            }
            foreach ($ecredits as $c) {
                $coy_id = $c['coy_id'];
                $coupon_serialno = $c['coupon_serialno'];
                $has_owner = $this->voucherRepository->hasOwner($coupon_serialno, $coy_id);
                if (!$has_owner) {
                    $this->voucherRepository->updateOwner($coupon_serialno, $coy_id, $mbr_id);
                }
                $check_credit_exist = Voucher::checkCreditExists($coy_id, $mbr_id, $coupon_serialno);
                if (!$check_credit_exist) {
                    return $this->errorWithCodeAndInfo('500', 'Sorry, your eCredit ' . $c['coupon_serialno'] . ' is invalid or reserved. Please check.');
                }
                $data = [
                    'coy_id' => $coy_id,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
//                    'coupon_id' => $coupon_id,
                    'coupon_serialno' => $coupon_serialno,
//                    'voucher_amount' => $voucher_amount,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'expires' => $trans_expires_time,
                    'status' => 1
                ];
                $this->creditReservedRepository->updateOrSave('save', $data);
            }
            foreach ($vouchers as $v) {
                $coy_id = $v['coy_id'];
                $coupon_serialno = $v['coupon_serialno'];
                $has_owner = $this->voucherRepository->hasOwner($coupon_serialno, $coy_id);
                if (!$has_owner) {
                    $this->voucherRepository->updateOwner($coupon_serialno, $coy_id, $mbr_id);
                }
                $check_voucher_exist = Voucher::checkVoucherExists($coy_id, $mbr_id, $coupon_serialno);
                if (!$check_voucher_exist) {
                    return $this->errorWithCodeAndInfo('500', 'Sorry, your voucher ' . $v['coupon_serialno'] . ' is invalid or reserved. Please check.');
                }
                $data = [
                    'coy_id' => $coy_id,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
//                    'coupon_id' => $coupon_id,
                    'coupon_serialno' => $coupon_serialno,
//                    'voucher_amount' => $voucher_amount,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'expires' => $trans_expires_time,
                    'status' => 1
                ];
                $this->creditReservedRepository->updateOrSave('save', $data);
            }
        } catch (\Exception $e) {
            $this->dbconnMssqlCherps->rollBack();
            return $this->errorWithInfo("Whoops! There was a problem processing your reserve. Please try again.");
        }
        $this->dbconnMssqlCherps->commit();

        return $this->successWithInfo('eCredit or eGift reserved successfully');
    }

    /**
     * @api {post} /api/ecredits/unreserved eCredits Unreserved
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} [mbr_id] Option Member Id
     * @apiParam {string} trans_id Trans ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "trans_id": "test99",
     *  }
     *
     * @apiSuccessExample Unreserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "ECredits unreserved successfully",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Unreserved error
     * HTTP/1.1 unreserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "ECredits unreserved error."
     *  }
     *
     */
    public function credit_unreserved()
    {
        $input = request()->only(['trans_id']);
        $rules = [
            'trans_id' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $res = $this->creditReservedRepository->unreserve_hachi($input['trans_id']);
        if ($res) {
            return $this->successWithInfo('ECredits unreserved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'ECredits unreserved error.');
        }
    }

    /**
     * @api {get} /api/coupon/credit/{serial} eCredit Serial
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} serial Coupon Serial Number
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "expiry_date": "2021-05-26 00:00:00",
     *          "status_level": 1,
     *          "coupon_serialno": "SPRFV-000000040",
     *          "receipt_id1": "S3D18867",
     *          "receipt_id2": "S3D18873",
     *          "voucher_amount": "898.00",
     *          "issue_date": "2021-04-26 20:22:36.663",
     *          "utilize_date": "2021-04-27 09:16:36",
     *          "mbr_id": "V183535400",
     *          "reserved": 0
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Invalid Serial
     * HTTP/1.1 Invalid Serial
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid Serial"
     *  }
     *
     */
    public function get_credit($serial)
    {
        $credit = Voucher::getCreditBySerial($serial);
        if (!is_null($credit)) {
            return $this->successWithData($credit);
        }
        return $this->errorWithInfo("Invalid serial number");
    }

    /**
     * @api {post} /api/coupon/credit Save Credit
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} coupon_serialno Serial Number
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "coupon_serialno": "V100000100",
     *      "trans_id": "test99"
     *  }
     *
     * @apiSuccessExample Save success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Save success",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample eCredit invalid
     * HTTP/1.1 eCredit invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid serial number"
     *  }
     *
     */
    public function save_credit()
    {
        $input = request()->only(['coupon_serialno', 'trans_id', 'coupon_id']);
        $rules = [
            'coupon_serialno' => 'required',
            'trans_id' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $serial = $input['coupon_serialno'];
        $trans_id = $input['trans_id'];
        $coupon_id = (isset($input['coupon_id'])) ? $input['coupon_id'] : '!VCH-CREDIT'; //Default to eCredit voucher
        $res = $this->voucherRepository->save_credit($serial, $trans_id, $coupon_id);
        if ($res) {


            return response()->json([
                'info' => "Save Success.",
                'status' => 'success',
                'status_code' => 200,
                'data' => $res
            ], 200);

            return $this->successWithInfo("Save Success.");
        } else {
            return $this->errorWithInfo("Invalid serial number");
        }
    }

    /** @deprecated */
    public function reserved()
    {
        $input = request()->only(['mbr_id', 'trans_id', 'rebate_amount']);
        $rules = [
            'mbr_id' => 'required',
            'trans_id' => 'required',
            'rebate_amount' => 'required|numeric'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $rebate_amount = $input['rebate_amount'];
        $trans_id = $input['trans_id'];
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if ($member === null) {
            return $this->errorWithCodeAndInfo('500', 'Sorry, we could not find your member id.');
        } else {
            $points_remaining = (int)$member->points_remaining;
            if ($points_remaining < (int)($rebate_amount * 100)) {
                return $this->errorWithCodeAndInfo('500', 'Sorry, your rebate is not enough.');
            }
        }
        /*
         * Transaction expires after X min
         */
        $trans_expires_time = Carbon::now()->addMinutes(config('challenger.checkout_timeout_after'));
        $data = [
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'rebate_amount' => $rebate_amount,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'expires' => $trans_expires_time,
            'status' => 1
        ];
        if ($this->rebateReservedRepository->is_exists($trans_id)) {
            $res = $this->rebateReservedRepository->updateOrSave('update', $data);
        } else {
            $res = $this->rebateReservedRepository->updateOrSave('save', $data);
        }
        if ($res) {
            return $this->successWithInfo('Transaction reserved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Transaction reserved error.');
        }
    }

    /** @deprecated */
    public function unreserved()
    {
        $input = request()->only(['trans_id']);
        $rules = [
            'trans_id' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $res = $this->rebateReservedRepository->unreserve($input['trans_id']);
        if ($res) {
            return $this->successWithInfo('Transaction unreserved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Transaction unreserved error.');
        }
    }

    /**
     * @api {post} /api/transaction Save Transaction
     * @apiVersion 1.0.0
     * @apiGroup 2.TRANSACTION
     *
     * @apiParam {string} loc_id Location ID
     * @apiParam {string} [pos_id] Pos ID
     * @apiParam {string} [rebates_earned] Rebates Earned
     * @apiParam {string} invoice_num Invoice Number(transaction id)
     * @apiParam {string} trans_type Transaction Type
     * @apiParam {string} trans_date Transaction Date
     * @apiParam {string} [cashier_id] Cashier ID. Option, default is ''.
     * @apiParam {string} [cashier_name] Cashier Name. Option, default is ''
     * @apiParam {string} [calculate_points] Whether calculate points. Option 'Y' or 'N', default is 'N'
     * @apiParam {Object[]} customer_info Customer Information
     * @apiParam {Object} customer_info.id Customer ID
     * @apiParam {Object[]} items Items
     * @apiParam {Object} items.line_num Items Line Number
     * @apiParam {Object} items.item_id Items ID
     * @apiParam {Object} items.item_desc Items Description
     * @apiParam {Object} items.qty_ordered Items Ordered quantity
     * @apiParam {Object} items.unit_price Items Unit Price
     * @apiParam {Object} [items.unit_discount] Items Unit Discount. Options default 0
     * @apiParam {Object} items.regular_price Items Regular Price
     * @apiParam {Object} items.total_price Items Total Price
     * @apiParam {Object} [items.trans_point] Items Transaction Points. Options default 0
     * @apiParam {Object} [items.mbr_savings] Items Member Savings. Options default 0
     * @apiParam {Object[]} [coupons] Coupons
     * @apiParam {Object} coupons.coupon_id Coupon ID
     * @apiParam {Object} coupons.coupon_name Coupon Name
     * @apiParam {Object} coupons.coupon_code Coupon Code
     * @apiParam {Object} coupons.amount Coupon Amount
     * @apiParam {Object[]} payments Payments
     * @apiParam {Object} payments.pay_mode Payment Mode
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "loc_id": "BF",
     *      "pos_id": "B9",
     *      "rebates_earned": "83.49",
     *      "invoice_num": "B9B00064",
     *      "trans_type": "PS",
     *      "trans_date": "2020-03-31 21:22:44",
     *      "cashier_id": "3810",
     *      "customer_info": {
     *          "id": "V100000100"
     *      },
     *      "items": [
     *      {
     *          "line_num": 1,
     *          "item_id": "!MEMBER-UPG18",
     *          "item_desc": "XXLogitech 920-008617/8 Slim Folio for iPad 9.7",
     *          "qty_ordered": 1,
     *          "unit_price": "18.00",
     *          "unit_discount": "0.00",
     *          "regular_price": "18.00",
     *          "total_price": "18.00",
     *          "mbr_savings": 66,
     *          "trans_point": 77,
     *      },],
     *      "payments": [
     *      {
     *          "pay_mode": "CASH",
     *          "info": "",
     *          "approval_code": null,
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "CARD",
     *          "info": "[UOB_AMEX/*****7522]",
     *          "approval_code": "423910",
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "REBATE",
     *          "info": "",
     *          "approval_code": null,
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "EGIFT",
     *          "coupon_serialno": "MEM-OTEPECB"
     *      },
     *      {
     *          "pay_mode": "ECREDIT",
     *          "coupon_serialno": "VCH-CR000000491"
     *      }],
     *      "coupons": [
     *          {
     *              "coupon_id": "!WPHP38INK",
     *              "coupon_name": "$38 off HP Ink Cartridges with min spend $38 ",
     *              "coupon_code": "HP-AIRPVA8D657",
     *              "amount": null
     *          },
     *      ]
     *  }
     *
     * @apiSuccessExample Transaction success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful save transaction",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Reserved rebate error
     * HTTP/1.1 reserved rebate error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, your reserved rebate is overdue or unactivated, please try again"
     *  }
     *
     * @apiErrorExample Transaction error
     * HTTP/1.1 transaction error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Whoops! There was a problem processing your transaction. Please try again."
     *  }
     *
     */
    public function transaction()
    {
        /*
         * Validate
         */
        $input = request()->only(['loc_id', 'pos_id', 'trans_type', 'trans_date', 'invoice_num', 'trans_num', 'cashier_id', 'calculate_points',
            'items', 'customer_info', 'payments', 'rebates_earned', 'total_savings', 'trans_date', 'coupons']);
        $rules = [
            'loc_id' => 'required',
            'trans_date' => 'required',
            'trans_type' => 'required',
            'invoice_num' => 'required',
            'items.*' => 'required',
            'customer_info.id' => 'required',
//            'rebates_earned' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        /*
         * Define variable
         */
        $now = new Carbon($input['trans_date']);
        $coy_id = $this->coyId;
        $mbr_id = trim($input['customer_info']['id']);
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if (empty($member)) {
            return $this->errorWithCodeAndInfo(500, "Sorry, member id is invalid.");
        }
        /*
         * Check if calculate_points is Y
         */
        if (isset($input['calculate_points']) && $input['calculate_points'] === 'Y') {
            $points_tier = $this->getRebateTierFromType( $this->memberTypeRepository->getPosMemberType($mbr_id) );
        }
        $loc_id = $input['loc_id'];
        $pos_id = isset($input['pos_id']) ? $input['pos_id'] : " ";
        $trans_type = rtrim($input['trans_type']);
        $trans_id = $input['invoice_num'];
        $inv_num = isset($input['trans_num']) ? rtrim($input['trans_num']) : $trans_id;
        $create_by = isset($input['cashier_id']) ? $input['cashier_id'] : '';
        $payments = $input['payments'];
        $items = $input['items'];
        $coupons = isset($input['coupons']) ? $input['coupons'] : [];
        $reserved_points = 0;
        if ($trans_type === 'DM' || $trans_type === 'DI') {
            if (in_array('REBATE', array_column($payments, "pay_mode"))) {
                $reserved_points = $this->_member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by);
                if (!$reserved_points) {
                    return $this->errorWithInfo("Sorry, your reserved rebate is overdue or unactivated, please try again");
                }
                $points_redeemed = $reserved_points ? $reserved_points : 0;
                $member_redeemed = $points_redeemed;
                $this->_member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, 0, $coy_id, 0, $create_by);
                return $this->successWithInfo("DM or DI type will not process and rebate payment deduct.");
            }
            return $this->successWithInfo("DM or DI type will not process.");
        }

        $this->dbconnMssqlCherps->beginTransaction();
        try {
            /*
             * Save transaction
             */
            $total_points = 0;
            $total_savings = 0;
            foreach ($items as $item) {
                $item_id = $item['item_id'];
                $item_qty = $item['qty_ordered'];
                $salesperson_id = isset($item['salesperson_id']) ? substr($item['salesperson_id'], 0, 15) : '';
                $mbr_savings = isset($item['mbr_savings']) ? number_format((float)str_replace(',', '', $item['mbr_savings']), 2, ".", "") : 0;
                $line_num = $item['line_num'];
                $disc_amount = isset($item['unit_discount']) ? $item['unit_discount'] : 0;
                $item_desc = strlen($item['item_desc']) > 60 ? substr($item['item_desc'],0,60) : $item['item_desc'];
                $unit_price = number_format((float)str_replace(',', '', $item['unit_price']), '2', '.', "");

                if (isset($input['calculate_points']) && $input['calculate_points'] === 'Y') {
                    $item_points = $this->calculate_points($points_tier, $unit_price, $item_qty);
                } else {
                    $item_points = isset($item['trans_point']) ? (float)$item['trans_point'] : 0;
                }

                $total_points = $total_points + $item_points;
                $total_savings = $total_savings + $mbr_savings;
                if (Transaction::where(['mbr_id' => $mbr_id, 'trans_id' => $trans_id, 'coy_id' => $this->coyId, 'line_num' => $line_num])->exists()) {
                    return $this->errorWithInfo("Sorry, " . $item_id . ", line number is " . $line_num . " in " . $trans_id . " has been saved.");
                }
                $data_transaction = [
                    'coy_id' => $coy_id,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
                    'trans_type' => $trans_type,
                    'trans_date' => $now,
                    'loc_id' => $loc_id,
                    'line_num' => $line_num,
                    'pos_id' => $pos_id,
                    'item_id' => $item_id,
                    'item_desc' => $item_desc,
                    'item_qty' => $item_qty,
                    'regular_price' => number_format((float)str_replace(',', '', $item['regular_price']), '2', '.', ""),
                    'unit_price' => $unit_price,
                    'disc_percent' => 0,
                    'disc_amount' => $disc_amount,
                    'trans_points' => $item_points,
                    'salesperson_id' => $salesperson_id,
                    'mbr_savings' => $mbr_savings,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->transactionRepository->create($data_transaction);
                /*
                 * Check if member item
                 */
                if (strpos($item_id, '!MEMBER') !== false) {
                    // Create member type record
                    $member_type = $this->dbconnMssqlCherps->table('ims_item_list_member')->where(['item_id' => $item_id, 'status_level' => 1])->first();
                    $mbr_type = trim($member_type->item_type);
                    $member_refund_rebate = $this->memberRepository->findByMbrId($mbr_id);
                    // Check if refund member
                    if ($item_qty < 0) {
                        $rtn_del_mbr = $this->memberTypeRepository->returnMember($mbr_id);
                        $rtn_line_num = $rtn_del_mbr[0];
                        $rtn_trans_date = $rtn_del_mbr[1];
                        $rtn_mbr = $this->dbconnMssqlCherps->table('crm_member_type')->where(['mbr_id' => $mbr_id, 'line_num' => $rtn_line_num - 1]);
                        if (!$rtn_mbr->exists()) {
                            $this->dbconnMssqlCherps->table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                                'status_level' => -1,
                                'exp_date' => $now,
                                'last_renewal' => $now,
                                'modified_on' => $now
                            ]);
                        }
                        else {
                            $rtn_mbr_type = rtrim($rtn_mbr->first()->mbr_type);
                            $rtn_duration_month = $this->dbconnMssqlCherps->table('ims_item_list_member')->where('item_type', $rtn_mbr_type)->first()->month_of_duration;
                            $rtn_eff_from = $rtn_mbr->first()->eff_from;
                            $rtn_eff_to = Carbon::parse($rtn_eff_from)->addMonth($rtn_duration_month);
                            $rtn_mbr->update(['eff_to' => $rtn_eff_to, 'modified_on' => $now]);
                            $rtn_status = $rtn_eff_to->gt(Carbon::now()) ? 1 : -1;

                            $this->dbconnMssqlCherps->table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                                'status_level' => $rtn_status,
                                'exp_date' => $rtn_eff_to,
                                'last_renewal' => $now,
                                'mbr_type' => $rtn_mbr_type,
                                'modified_on' => $now
                            ]);
                        }
                        \App\Models\Member\Voucher\Voucher::whereDate('trans_date', Carbon::parse($rtn_trans_date)->toDateString())->where('coupon_serialno', 'LIKE', 'MEM-'.'%')->delete();
                    }
                    else {
                        if ($member_type->is_from_today == 1) {
                            $eff_from = $now->toDateString();
                        } else {
                            $getExpiryDate = $this->memberTypeRepository->getExpiryDate($mbr_id, $now);
                            $eff_from = !is_null($getExpiryDate) ? Carbon::parse($getExpiryDate->eff_to)->addSeconds(1)->toDateTimeString() : $now->toDateString();
                        }
                        $format_eff_from = new Carbon($eff_from);
                        $eff_to = $format_eff_from->addMonth($member_type->month_of_duration)->subSeconds(1)->toDateTimeString();
                        // Associate member
                        if (strpos($item_id, '!MEMBER-NEW') === false && $this->memberRepository->has_associate($mbr_id)) {
                            $old_type = rtrim($member->mbr_type);
                            $this->_associate_member($mbr_id, $old_type, $mbr_type, $eff_to, $now, $create_by);
                        }
                        // exec o2o_sp_do_transaction_do_issue_member_coupon
                        $this->_issue_coupon($mbr_id, $mbr_type, $trans_id, $now);
//                        $this->dbconnMssqlCherps->select('EXEC [o2o_sp_do_transaction_do_issue_member_coupon] @mbrId=?, @invoiceId=?, @source=\'ONLINE_STORE\', @mbrType=?', array($mbr_id, $trans_id, $mbr_type));

                        $data_member_type = [
                            'coy_id' => $coy_id,
                            'mbr_id' => $mbr_id,
                            'eff_from' => $eff_from,
                            'eff_to' => $eff_to,
                            'mbr_type' => $mbr_type,
                            'created_by' => substr($trans_id,0,15),
                            'created_on' => $now,
                            'modified_by' => $create_by,
                            'modified_on' => $now,
                        ];
                        $this->memberTypeRepository->create($data_member_type);
                        // Update previous record eff_to and refund rebate
                        if ($member_type->need_update == 1) {
                            $data_update_eff_to = ['eff_to' => Carbon::parse($now)->startOfDay()->subSecond(1)->toDateTimeString(), 'modified_on' => $now];
                            // not work
                            $ystd = Carbon::parse($now)->subDay(1);
                            $refund_rebate = $this->memberTypeRepository->refundRebate($mbr_id, $mbr_type, $member_type->month_of_duration, $ystd->startOfDay(), $data_update_eff_to);
//                            $this->memberTypeRepository->updateEffTo($mbr_id, $data_update_eff_to, $mbr_type, $ystd);
//                            $max_line_mbr = Transaction::where('coy_id', $this->coyId)->where('mbr_id', $mbr_id)->where('trans_id', $trans_id)
//                                ->max('line_num');
                            if ($refund_rebate != 0) {
                                $max_line_mbr = end($items)['line_num'];
                                $this->transactionRepository->create([
                                    'coy_id' => $coy_id,
                                    'mbr_id' => $mbr_id,
                                    'trans_id' => $trans_id,
                                    'trans_type' => 'AP',
                                    'trans_date' => $now,
                                    'loc_id' => $loc_id,
                                    'line_num' => 0, // $max_line_mbr ? $max_line_mbr + 1 : 1,
                                    'pos_id' => $pos_id,
                                    'item_id' => '',
                                    'item_desc' => 'Membership refund via rebate',
                                    'item_qty' => $refund_rebate,
                                    'regular_price' => 1,
                                    'unit_price' => 1,
                                    'disc_percent' => 0,
                                    'disc_amount' => 0,
                                    'trans_points' => $refund_rebate,
                                    'salesperson_id' => $salesperson_id,
                                    'mbr_savings' => 0,
                                    'created_by' => $create_by,
                                    'created_on' => $now,
                                    'modified_by' => $create_by,
                                    'modified_on' => $now,
                                    'updated_on' => $now,
                                ]);
                                $total_points = $total_points + $refund_rebate;
                            }
                        }
                        /*
                         * Update member list expired_date and mbr_type
                         */
                        $last_renewal = $member_refund_rebate->last_renewal;
                        $last_expiry = $member_refund_rebate->exp_date;
                        $claim_entitled = $member_refund_rebate->mbr_type;
                        $this->memberRepository->update(['status_level' => 1, 'exp_date' => $eff_to, 'mbr_type' => $mbr_type, 'updated_on' => $now, 'last_renewal' => $now], $mbr_id, "mbr_id");
                        // Update crm_rebate_claim
                        if ($member_type->need_update_claim == 1 && $last_expiry > $now) {
                            $this->rebateClaimRepository->create([
                                'coy_id' => $this->coyId,
                                'mbr_id' => $mbr_id,
                                'last_renewal' => $last_renewal,
                                'last_expiry' => $last_expiry,
                                'claim_entitled' => 'N',
                                'claim_expiry' => Carbon::parse($last_expiry)->addDays(90)->toDateTimeString(),
                                'rebate_voucher' => '',
                                'mbr_savings' => 0,
                                'last_mbrtype' => $claim_entitled,
                                'created_by' => $salesperson_id,
                                'created_on' => $now,
                                'modified_by' => $salesperson_id,
                                'modified_on' => $now,
                                'updated_on' => $now
                            ]);
                        }

                        // Register email with emarsys
//                        $this->emarsysService->registerUser($member_refund_rebate->email_addr);
//                        $this->dispatch(new RegisterEmarsysUser($member_refund_rebate->email_addr));

                    }
                }
            }

            /*
             * Save coupon
             */
            foreach ($coupons as $coupon) {
                $coupon_id = $coupon['coupon_id'];
                $coupon_code = $coupon['coupon_code'];
                $coupon_amount = $coupon['amount'];
                if (!is_null($coupon_code)) {
                    $voucher = $this->dbconnMssqlCherps->table('crm_voucher_list')->where([
                        'coy_id' => $this->coyId,
                        'coupon_id' => $coupon_id,
                        'coupon_serialno' => $coupon_code,
                        'mbr_id' => $mbr_id
                    ]);
                    if ($voucher->exists()) {
                        $voucher->update([
                            'receipt_id2' => $trans_id,
                            'status_level' => 1,
                            'utilize_date' => $now
                        ]);
                    } else {
                        $this->voucherRepository->create([
                            'coy_id' => $this->coyId,
                            'coupon_id' => $coupon_id,
                            'coupon_serialno' => $coupon_code,
                            'promocart_id' => $coupon_id,
                            'mbr_id' => $mbr_id,
                            'trans_id' => '',
                            'trans_date' => $now,
                            'ho_ref' => '',
                            'ho_date' => $now,
                            'issue_date' => $now,
                            'sale_date' => $now,
                            'utilize_date' => $now,
                            'expiry_date' => $now,
                            'print_date' => $now,
                            'loc_id' => $loc_id,
                            'receipt_id1' => $trans_id,
                            'receipt_id2' => $trans_id,
                            'voucher_amount' => $coupon_amount,
                            'redeemed_amount' => 0,
                            'expired_amount' => 0,
                            'issue_type' => '',
                            'issue_reason' => '',
                            'status_level' => 1,
                            'created_by' => $create_by,
                            'created_on' => $now,
                            'modified_by' => $create_by,
                            'modified_on' => $now,
                        ]);
                    }
                }
            }

            /*
             * Update CRM Member Points
             */
            if (in_array('REBATE', array_column($payments, "pay_mode"))) {
                $reserved_points = $this->_member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by);
                if (!$reserved_points) {
                    return $this->errorWithInfo("Sorry, your reserved rebate is overdue or unactivated, please try again");
                }
            }
            $points_redeemed = $reserved_points ? $reserved_points : 0;
            $member_redeemed = $points_redeemed;

            /*
             * Update Ecredits & Egifts
             */
            if (in_array('ECREDIT', array_column($payments, "pay_mode")) || in_array('EGIFT', array_column($payments, "pay_mode")) || in_array('PAYVCH', array_column($payments, "pay_mode"))) {
                foreach ($payments as $p) {
                    $p_mode = $p['pay_mode'];
                    $trans_amount = $p['trans_amount'];
                    if ($trans_amount > 0) {
                        if ($p_mode === 'ECREDIT' || $p_mode === 'EGIFT' || $p_mode == 'PAYVCH') {
                            $coupon_no = isset($p['coupon_serialno']) ? $p['coupon_serialno'] : '';
                            if ($coupon_no === '') {
                                return $this->errorWithCodeAndInfo(500, 'If pay_mode is ECREDIT, EGIFT or PAY_VCH, coupon_serialno required');
                            }
                            $this->creditReservedRepository->unreserve($trans_id);
//                            if (!$res) {
//                                return $this->errorWithCodeAndInfo(500, 'Sorry, You should reserve eGifts of eCredits first.');
//                            }
                            if ($this->voucherRepository->checkGiftExists($coupon_no)) {
                                $this->voucherRepository->updateGift($coupon_no, $trans_id);
                            } else {
                                return $this->errorWithCodeAndInfo(500, 'Sorry, eGifts, eCredits or Voucher invalid.');
                            }
                        }
                    }
                }
            }

            /*
             * Save payment
             */
            if (is_array($input['payments']) && count($input['payments'])>0) {
                $payment_data = [];
                $pdi = 1;
                foreach ($input['payments'] as $payment) {
                    $trans_ref = explode('/', substr($payment['info'], 1, -1));
                    $payment_data[] = [
                        'coy_id' => $coy_id,
                        'mbr_id' => $mbr_id,
                        'trans_id' => $trans_id,
                        'line_num' => $pdi,
                        'pay_mode' => substr($payment['pay_mode'], 0, 60),
                        'trans_ref1' => (isset($trans_ref[0])) ? substr($trans_ref[0], 0, 60) : '',
                        'trans_ref2' => (isset($trans_ref[1])) ? substr($trans_ref[1], 0, 60) : '',
                        'trans_ref3' => (isset($trans_ref[2])) ? substr($trans_ref[2], 0, 60) : '',
                        'trans_amount' => number_format((float)str_replace(',', '', $payment['trans_amount']), '2', '.', ""),
                        'created_by' => $create_by,
                        'created_on' => $now,
                        'modified_by' => $create_by,
                        'modified_on' => $now,
                    ];
                    $pdi++;
                }
                $this->dbconnMssqlCherps->table('crm_member_trans_payment')->insert($payment_data);
            }

            $this->_member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, $total_points, $coy_id, $total_savings, $create_by);
            $this->memberRepository->delete_same_member($member->email_addr, $member->contact_num);
        } catch (\Exception $e) {
            Log::error($e);
            $this->dbconnMssqlCherps->rollBack();

            $log_data = [
                "api_description" => "SAVE_TRANS_" . $input['invoice_num'],
                "request_body" => json_encode($input),
                "response_body" => json_encode($e),
                "created_on" => date('Y-m-d H:i:s')
            ];
            $this->dbconnMssqlCherps->table('sys_api_log')->insert($log_data);


            $info = [
                'id' => $input['invoice_num'],
                'name' => $input['customer_info']['id'],
                'type' => false,
                'desc' => $e,
                'route_name' => 'pos.transaction'
            ];
//            event(new DataOperation($info));
            return $this->errorWithInfo("Whoops! There was a problem processing your transaction. Please try again. (CODE: PCT3222)");
        }
        $this->dbconnMssqlCherps->commit();

        $this->dbconnMssqlCherps->insert("INSERT INTO crm_member_transaction_points
                                            SELECT * FROM v_mbr_trans_points WHERE coy_id=? AND mbr_id=? AND trans_id=?", [$coy_id, $mbr_id, $trans_id]);
        /*
         * Save log
         */
//        $suc_info = [
//            'id' => $trans_id,
//            'name' => $mbr_id,
//            'type' => true,
//            'desc' => $mbr_id . ' successfully save ' . $trans_id . '. And increase ' . $total_points . ' points. At location ' . $pos_id . ' of ' . $loc_id . '. Transaction log is ' . json_encode($input),
//            'route_name' => 'pos.transaction'
//        ];
//        event(new DataOperation($suc_info));
        return $this->successWithInfo("Successful save transaction");
    }

    public function _member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, $total_points, $coy_id, $total_savings, $create_by)
    {
        $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
        if (rtrim($member->mbr_type, " ") == 'MAS') {
            if ($points_redeemed > 0) {
                //$redeemed = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                $redeemed = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ?",[$this->coyId, $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                foreach ($redeemed->get() as $k => $v) {
                    if ($points_redeemed > 0) {
                        $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed'], 2, '.', '');
                        if ($remain_point > $points_redeemed) {
                            //Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",[$this->coyId, $mbr_id, $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = 0;
                        } else {
                            //Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",[$this->coyId, $mbr_id, $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = number_format($points_redeemed - $remain_point, 2, '.', '');
                        }
                    }
                }
                $member->increment('points_redeemed', $member_redeemed);
            }
            $main_id = trim($member->main_id);
            //$points = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $main_id, 'exp_date' => $exp_date]);
            $points = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and exp_date = ?",[$this->coyId, $main_id, $exp_date]);
            $main_member = $this->memberRepository->findByMbrId($main_id);
            if ($points->exists()) {
                $points->increment('points_accumulated', $total_points, ['modified_by' => $create_by, 'modified_on' => $now]);
            }
            else {
                $data_points = [
                    'coy_id' => $coy_id,
                    'mbr_id' => trim($main_id),
                    'exp_date' => $exp_date,
                    'points_accumulated' => $total_points,
                    'points_redeemed' => $points_redeemed,
                    'points_expired' => 0,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->pointsRepository->create($data_points);
            }
            $main_member->increment('points_accumulated', $total_points);
            $main_member->increment('mbr_savings', $total_savings, ['modified_by' => $create_by, 'modified_on' => $now]);
        }
        else {
            //$points = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
            $points = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and exp_date = ?",[$this->coyId, $mbr_id, $exp_date]);
            if ($points_redeemed > 0) {
                //$redeemed = Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                $redeemed = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ?",[$this->coyId, $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                foreach ($redeemed->get() as $k => $v) {
                    if ($points_redeemed > 0) {
                        $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed'], 2, '.', '');
                        if ($remain_point > $points_redeemed) {
                            //Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",[$this->coyId, $mbr_id, $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = 0;
                        } else {
                            //Points::where(['coy_id' => $this->coyId, 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",[$this->coyId, $mbr_id, $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = number_format($points_redeemed - $remain_point, 2, '.', '');
                        }
                    }
                }
            }
            if ($points->exists()) {
                $points->increment('points_accumulated', $total_points, ['modified_by' => $create_by, 'modified_on' => $now]);
            } else {
                $data_points = [
                    'coy_id' => $coy_id,
                    'mbr_id' => trim($mbr_id),
                    'exp_date' => $exp_date,
                    'points_accumulated' => $total_points,
                    'points_redeemed' => $points_redeemed,
                    'points_expired' => 0,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->pointsRepository->create($data_points);
            }
            $member->increment('points_accumulated', $total_points);
            $member->increment('points_redeemed', $member_redeemed);
            $member->increment('mbr_savings', $total_savings, ['modified_by' => $create_by, 'modified_on' => $now]);
        }
    }

    public function _member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by)
    {
        $rebate_amount = $payments[array_search('REBATE', array_column($payments, "pay_mode"))]['trans_amount'];
        if ($rebate_amount <= 0) {
            $reserved_points = $rebate_amount * 100;
        } else {
            $trans_reserved = $this->rebateReservedRepository->get_rebate_amount($inv_num, $mbr_id);
            if ($trans_reserved) {
                $reserved_points = $trans_reserved->rebate_amount * 100;
                // Delete unreserved transaction
                $this->rebateReservedRepository->unreserve($inv_num);
            } else {
                return false;
            }
        }
        $max_line = Transaction::where('coy_id', $this->coyId)->where('mbr_id', $mbr_id)->where('trans_id', $trans_id)
            ->max('line_num');
        $this->transactionRepository->create([
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => 'RD',
            'trans_date' => $now,
            'line_num' => $max_line ? $max_line + 1 : 1,
            'loc_id' => $loc_id,
            'pos_id' => $pos_id,
            'item_id' => '!VCH-STAR001',
            'item_desc' => 'Rebates Redemption',
            'item_qty' => $reserved_points,
            'regular_price' => 0.01,
            'unit_price' => 0.01,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => -1 * $reserved_points,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => $create_by,
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
            'updated_on' => $now,
        ]);
        return $reserved_points;
    }

    public function _associate_member($main_id, $old_type, $new_type, $eff_to, $now, $create_by)
    {
        $eff_from = Carbon::parse($now)->startOfDay();
        $ass_member = $this->memberRepository->get_associate($main_id);
        if (strcasecmp($old_type, $new_type) > 0) {
            $exp_date = $now;
            $this->memberRepository->set_associate_expiry($main_id, $exp_date, $now);
            foreach ($ass_member as $a) {
                $this->memberTypeRepository->update_ass_mbr($a->mbr_id, $exp_date, $now);
            }
        }
        else {
            foreach ($ass_member as $a) {
                $this->memberTypeRepository->update_ass_mbr($a->mbr_id, Carbon::parse($now)->startOfDay()->subSecond(1), $now);
                $this->memberTypeRepository->create([
                    'coy_id' => $this->coyId,
                    'mbr_id' => $a->mbr_id,
                    'eff_from' => $eff_from,
                    'eff_to' => $eff_to,
                    'mbr_type' => 'MAS',
                    'created_by' => $main_id,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                ]);
            }
            $this->memberRepository->update_associate($main_id, $eff_to, $now);
        }
    }

    public function calculate_points($points_tier, $unit_price, $item_qty)
    {
        $points = floor($unit_price * (int)$item_qty * (float)$points_tier);
        return $points;
    }

    /**
     * @api {post} /api/coupon/issue Issue Coupon
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} [coy_id] Option, default is CTL
     * @apiParam {string} coupon_id Coupon ID
     * @apiParam {string} [coupon_serialno] Option, Default generate a serial no, start with "D"
     * @apiParam {string} [promocart_id] Option, Default same as coupon_id
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} [trans_id] Transaction ID, Option, default is ''
     * @apiParam {string} [trans_date] Transaction Date, Option, default is now()
     * @apiParam {string} [ho_ref] Option, default is ''
     * @apiParam {string} [ho_date] Option, default is now()
     * @apiParam {string} [issue_date] Option, default is now()
     * @apiParam {string} [sale_date] Option, default is now()
     * @apiParam {string} [utilize_date] Option, default is "1900-01-01 00:00:00"
     * @apiParam {string} [expired_date] Option, Default to +30days
     * @apiParam {string} [print_date] Option, Default today
     * @apiParam {string} [loc_id] Option, Default ''
     * @apiParam {string} [receipt_id1] Option, Default ''
     * @apiParam {float} [voucher_amount] Option, Default 0
     * @apiParam {string} [issue_type] Option, Default ''
     * @apiParam {int} [status_level] Option, Default 0
     * @apiParam {string} created_by Created BYs
     * @apiParam {string} [guid] GUID
     *
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "coy_id": "CTL",
     *      "coupon_id": "!WPSEPBUBM99",
     *      "coupon_serialno": "BFB4VE-00992562",
     *      "promocart_id": "!WPSEPBUBM99",
     *      "mbr_id": "V100000100",
     *      "trans_id": "",
     *      "ho_ref": "",
     *      "expiry_date": "2019-09-17 23:59:00",
     *      "print_date": "2019-09-17 23:59:00",
     *      "loc_id": "PS",
     *      "receipt_id1": "B4A04139",
     *      "voucher_amount": -10,
     *      "issue_type": "eVoucher",
     *      "status_level": 0,
     *      "created_by": "3800",
     *      "guid": "6863F0C4-E9E5-4Q92-AD34-2FA8F18FB1CA"
     *  }
     *
     * @apiSuccessExample Reserved success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Voucher saved successfully.",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Reserved error
     * HTTP/1.1 reserved error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Voucher saved error."
     *  }
     *
     */
    public function saveVoucher()
    {
        $input = request()->only([
            'coupon_id',
            'coupon_serialno', 'promocart_id', 'mbr_id', 'expiry_date', 'print_date',
            'loc_id', 'receipt_id1', 'voucher_amount', 'status_level',
            'created_by']);
        $rules = [
            'coupon_id' => 'required',
//            'coupon_serialno' => 'required',
//            'promocart_id' => 'required',
//            'expiry_date' => 'required',
//            'print_date' => 'required',
//            'loc_id' => 'required',
//            'receipt_id1' => 'required',
//            'voucher_amount' => 'required',
//            'status_level' => 'required',
            'created_by' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $default_date = '1900-01-01 00:00:00';
        $now = Carbon::now();
        $expiry_date = Carbon::now()->addDays(7);
        if (request()->expiry_date){
            $expiry_date = str_replace('/','-', request()->expiry_date);
            $expiry_date = date('Y-m-d 23:59:59', strtotime($expiry_date));
        }

        $coupon_serialno = $this->_generate_coupon_no('D');
//        dd($coupon_serialno);
        $data = [
            'coy_id' => request()->coy_id ? request()->coy_id : $this->coyId,
            'coupon_id' => request()->coupon_id,
            'coupon_serialno' => request()->coupon_serialno ? request()->coupon_serialno : $coupon_serialno,
            'promocart_id' => request()->coupon_id,
            'mbr_id' => request()->mbr_id,
            'trans_id' => request()->trans_id ? request()->trans_id : '',
            'trans_date' => $now,
            'ho_ref' => request()->ho_ref ? request()->ho_ref : '',
            'ho_date' => $now,
            'issue_date' => $now,
            'sale_date' => $now,
            'utilize_date' => $default_date,
            'expiry_date' => $expiry_date,
            'print_date' => $now,
            'loc_id' => request()->loc_id ? request()->loc_id:'',
            'receipt_id1' => request()->receipt_id1 ? request()->receipt_id1 : '',
            'receipt_id2' => '',
            'voucher_amount' => request()->voucher_amount ? request()->voucher_amount : 0,
            'redeemed_amount' => 0,
            'expired_amount' => 0,
            'issue_type' => request()->issue_type ? request()->issue_type : '',
            'issue_reason' => request()->issue_reason ? request()->issue_reason : 'ValueClub sign up / membership renewal',
            'status_level' => request()->status_level ? request()->status_level : 0,
            'created_by' => request()->created_by,
            'created_on' => $now,
            'modified_by' => request()->created_by,
            'modified_on' => $now,
//                'guid' => request()->guid,
        ];
//        dd($data);
        $res = $this->voucherRepository->saveModel($data);
        if ($res) {
            return $this->successWithInfo('Voucher saved successfully');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Voucher saved error.');
        }
    }

    public function voucherUtilize(Request $request){
        $request = $request->only(
            'coupons',
            'mbr_id'
        );
//        dd($request['coupons']);
        $checkVoucher = false;
        $voucher = ['!MREP-FLIP4','!MREP-IPHONE14'];
        for ($p = 0; $p < count($request['coupons']); $p++) {
            for($pp = 0; $pp < count($voucher); $pp++){
                if($request['coupons'][$p]['coupon_id'] === $voucher[$pp]){
                    $checkVoucher = true;
                }
            }
        }
//        dd($checkVoucher);
        if($checkVoucher){
            DB::connection('pgsql')->table('crm_voucher_list')->where('mbr_id',$request['mbr_id'])->whereIn('coupon_id',$voucher)->update(['mbr_id' => '']);
        }
        return response()->json([
            'message' => 'success',
            'code' => 1
        ]);
//        dd($data);
    }

    public function saveBigVoucher(Request $request){

        //coupon id =  !MREP-IPHONE14 and !MREP-FLIP4
        //!MREP-IPHONE14 = $1,469   and !MREP-FLIP4 = $1,498

        $valid_request = $request->only(
            'coupon_id',
            'voucher_amount',
            'expiry_date',
            'coupon_name',
            'eff_from',
            'eff_to',
            'coupon_qty'
        );

        $rules = [
            'coupon_id' => 'required|max:15|unique:pgsql.crm_voucher_list,coupon_id',
            'voucher_amount' => 'required|numeric',
            'expiry_date' => 'required',
            'coupon_name' => 'required|max:255',
            'eff_from' => 'required',
            'eff_to' => 'required',
            'coupon_qty' => 'required|numeric'
        ];
        $message = [
            'coupon_id.required' => 'Coupon id required.',
            'coupon_id.unique' => 'This coupon id exists in our system.',
            'coupon_id.max' => 'This coupon id must consist max 15 letters',
            'voucher_amount.numeric' => 'Voucher amount must be a number.',
            'coupon_name.max' => 'This coupon name must consist max 255 letters',
            'eff_from.required' => 'Effective from date is required',
            'eff_to.required' => 'Effective to date is required',
        ];

        $validator = Validator::make($valid_request, $rules, $message);
        $default_date = '1900-01-01 00:00:00';
        $now = Carbon::now();
        $expiry_date = Carbon::now()->addDays(7);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $coupon_serialno = $this->_generate_coupon_no('BG');
//        dd($request->coupon_id);
        $voucher = [
            'coy_id' => $request->coy_id ?? $this->coyId,
            'coupon_id' => $request->coupon_id,
            'coupon_serialno' => $request->coupon_serialno ?? $coupon_serialno,
            'promocart_id' => $request->coupon_id,
            'mbr_id' => $request->mbr_id ?? '',
            'trans_id' => $request->trans_id ?? '',
            'trans_date' => $now,
            'ho_ref' => $request->ho_ref ?? '',
            'ho_date' => $now,
            'issue_date' => $now,
            'sale_date' => $now,
            'utilize_date' => $default_date,
            'expiry_date' => $expiry_date,
            'print_date' => $now,
            'loc_id' => $request->loc_id ?? '',
            'receipt_id1' => $request->receipt_id1 ?? '',
            'receipt_id2' => '',
            'voucher_amount' => $request->voucher_amount ?? 0,
            'redeemed_amount' => 0,
            'expired_amount' => 0,
            'issue_type' => $request->issue_type ?? '',
            'issue_reason' => $request->issue_reason ?? 'ValueClub Give Back Promo Voucher',
            'status_level' => $request->status_level ?? 0,
            'created_by' => $request->created_by,
            'created_on' => $now,
            'modified_by' => $request->created_by,
            'modified_on' => $now,
        ];

        $res = $this->voucherRepository->saveModel($voucher);

        $voucher_data = new VoucherData();
        $voucher_data->coy_id = $request->coy_id ?? $this->coyId;
        $voucher_data->coupon_id = $request->coupon_id;
        $voucher_data->coupon_type = $request->coupon_type ?? 'coupon';
        $voucher_data->coupon_name = $request->coupon_name;
        $voucher_data->coupon_code = $request->coupon_code ?? '';
        $voucher_data->eff_from = $request->eff_from;
        $voucher_data->eff_to = $request->eff_to;
        $voucher_data->coupon_qty = $request->coupon_qty;
        $voucher_data->max_qty = $request->max_qty ?? 0;
        $voucher_data->coupon_excerpt = $request->coupon_name;
        $voucher_data->coupon_description = $request->coupon_name;
        $voucher_data->created_on = $now;
        $voucher_data->modified_on = $now;
        $voucher_data->save();

        return $this->successWithInfo('Voucher saved successfully');
    }

    /**
     * @api {get} /api/coupon/details/{mbrId}/{statusLevel?} Get Coupon in Details
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} memberId Member id
     * @apiParam {int} [statusLevel] Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "coy_id": "CTL",
     *          "coupon_id": "!WPFB20/200    ",
     *          "coupon_serialno": "VCV3VE-00000634",
     *          "promocart_id": "!WPFB20/200    ",
     *          "mbr_id": "S0124386A",
     *          "trans_id": "",
     *          "trans_date": "2018-01-19 21:02:09.043",
     *          "ho_ref": "",
     *          "ho_date": "2018-01-19 21:02:09.043",
     *          "issue_date": "2018-01-19 21:02:09.773",
     *          "sale_date": "2018-01-19 21:02:09.773",
     *          "utilize_date": "2018-01-19 21:02:09.043",
     *          "expiry_date": "2018-02-02 23:59:00",
     *          "print_date": "2018-01-19 21:02:09.773",
     *          "loc_id": "VC   ",
     *          "receipt_id1": "V3A01174       ",
     *          "receipt_id2": "               ",
     *          "voucher_amount": "-20.00",
     *          "redeemed_amount": "0.00",
     *          "expired_amount": "0.00",
     *          "issue_type": "eVoucher",
     *          "issue_reason": "ValueClub sign up / membership renewal",
     *          "status_level": 0,
     *          "created_by": "3506                                              ",
     *          "created_on": "2018-01-19 21:02:09.773",
     *          "modified_by": "3506                                              ",
     *          "modified_on": "2018-01-19 21:02:09.773",
     *          "guid": "77851114-18F4-4BC0-A25C-D1431F7D4363"
     *      },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "key code is invalid"
     *  }
     *
     * @apiErrorExample Voucher find error.
     * HTTP/1.1 Voucher find error.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Voucher find error."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function getVoucher($mbrId, $statusLevel = null, Request $request)
    {
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member is null.');
        }
        $voucher = $this->voucherRepository->getVoucher($mbrId, $statusLevel);
        if ($voucher) {
            return $this->successWithData($voucher);
        } else {
            return $this->errorWithCodeAndInfo('500', 'Voucher find error.');
        }
    }

    /**
     * @api {get} /api/ping Get Environment
     * @apiGroup 0.ENVIRONMENT
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "code": "200",
     *      "msg": "pong",
     *      "env": "local"
     *  }
     */
    public function ping()
    {
        return response()->json([
            "code" => "200",
            "msg" => "pong",
            "env" => env('APP_ENV')
        ], 200);
    }

    /**
     * @api {post} /api/coupon/welcomepack Issue Welcome Coupon
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {string} mbrType Member Type: only M08, M18, M28, MST
     * @apiParam {string} [transId] Trans Id
     * @apiParam {string} [transDate] Trans Date
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "mbr_type": "M18",
     *      "trans_id": "C1B1123456",
     *      "trans_date": "2020-01-01 14:00:00"
     *  }
     *
     * @apiSuccessExample Issue Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": "Success issue coupon",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample key code
     * HTTP/1.1 key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "key code is invalid"
     *  }
     *
     */
    public function issue_coupon()
    {
        $input = request()->only('mbr_id', 'mbr_type',  'trans_id', 'trans_date');
        $rules = [
            'mbr_id' => 'required',
            'mbr_type' => 'required|in:M08,M18,M28,MST'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbrId = $input['mbr_id'];
        $mbrType = $input['mbr_type'];
        $transDate = isset($input['trans_date']) ? $input['trans_date'] : Carbon::now()->toDateTimeString();
        $transId = isset($input['trans_id']) ? $input['trans_id'] : '';
        if ($this->_issue_coupon($mbrId, $mbrType, $transId, $transDate)) {
            return $this->successWithInfo('Success issue coupon');
        } else {
            return $this->errorWithCodeAndInfo(422, 'Error issue coupon');
        }
    }

    /**
     * @api {post} /api/coupon/partner Issue partner Coupon
     * @apiGroup 4.VOUCHERS
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {string} couponId Coupon Id
     * @apiParam {string} partnerId Partner Id
     * @apiParam {int} [voucherAmount] Voucher Amount
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "coupon_id": "M18",
     *      "partner_id": "C1B1123456",
     *      "voucher_amount": 10
     *  }
     *
     * @apiSuccessExample Issue Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": "Success issue coupon",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample key code
     * HTTP/1.1 key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Invalid Validated Token"
     *  }
     *
     */
    public function partner_coupon()
    {
        $input = request()->only('mbr_id', 'coupon_id', 'partner_id', 'voucher_amount', 'issue_type','issue_reason','receipt_id');
        $rules = [
            'mbr_id' => 'required',
            'coupon_id' => 'required',
            'partner_id' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        // TODO: check duplicate?
        $now = Carbon::now();
        $mbrId = $input['mbr_id'];
        if (!$this->memberRepository->findByMbrId($mbrId)) {
            return $this->errorWithInfo("Member invalid.");
        }
        $couponId = $input['coupon_id'];
        $partnerId = $input['partner_id'];
        $coupon = $this->voucherIssueDataRepository->getPartnerCoupon($couponId, $partnerId);
        if ($coupon !== 0) {
            $voucherAmount = (isset($coupon->amount) && $coupon->amount > 0) ? $coupon->amount : (isset($input['voucher_amount']) ? $input['voucher_amount'] : 0) ;
            $exp_date = Carbon::today()->endOfDay()->addDays((int)$coupon->days_to_expiry)->toDateTimeString();
            $couponNo = $this->_generate_coupon_no();

            $receipt_id1 = $input['receipt_id'] ?? '';
            $issue_type = 'PAY_VCH';
            $issue_reason = $input['issue_reason'] ?? '';

            $partnerId = substr($partnerId, 0, 45);

            $this->voucherRepository->create([
                'coy_id' => $this->coyId,
                'coupon_id' => $couponId,
                'coupon_serialno' => $couponNo,
                'promocart_id' => $couponId,
                'trans_id' => '',
                'trans_date' => $now,
                'ho_ref' => '',
                'ho_date' => $now,
                'print_date' => $now,
                'loc_id' => '',
                'mbr_id' => $mbrId,
                'status_level' => 0,
                'issue_date' => $now,
                'sale_date' => $now,
                'utilize_date' => '1900-01-01',
                'expiry_date' => $exp_date,
                'voucher_amount' => $voucherAmount,
                'receipt_id1' => $receipt_id1,
                'receipt_id2' => '',
                'redeemed_amount' => 0,
                'expired_amount' => 0,
                'issue_type' => $issue_type,
                'issue_reason' => $issue_reason,
                'created_by' => $partnerId,
                'created_on' => $now,
                'modified_by' => $partnerId,
                'modified_on' => $now
            ]);
            return $this->successWithInfo([
                "msg"               => 'Success issue coupon',
                'coupon_id'         => $couponId,
                "coupon_serialno"   => $couponNo,
                'expiry_date'       => $exp_date,
                'voucher_amount'    => $voucherAmount
            ]);
        } else {
            return $this->errorWithInfo("Coupon Id invalid or expiry.");
        }
    }

    public function _generate_coupon_no($trans_prefix='C')
    {
        $year = date('y');
        $trans_prefix = $trans_prefix . $year;
//        dd($trans_prefix);
        $is_exists = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => $this->coyId, 'sys_id' => 'VC', 'trans_prefix' => $trans_prefix])->exists();
        if (!$is_exists) {
            $this->dbconnMssqlCherps->table('sys_trans_list')->insert([
                'coy_id' => $this->coyId,
                'sys_id' => 'VC',
                'trans_prefix' => $trans_prefix,
                'next_num' => 0,
                'sys_date' => Carbon::now()]);
        }
//        dd($trans_prefix);
        $next_num = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => $this->coyId, 'sys_id' => 'VC', 'trans_prefix' => $trans_prefix]);
        $coupon_no = $trans_prefix . sprintf("%012s", $next_num->first()->next_num);
        $next_num->increment('next_num', 1);
        return $coupon_no;
    }

    /**
     * @api {get} /api/transaction/item Get Item Last Purchase
     * @apiGroup 2.TRANSACTION
     *
     * @apiParam {string} memberId Member id
     * @apiParam {string} itemId Item ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V100000100",
     *      "item_id": "11111",
     *  }
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "V100000100",
     *          "item_id": "11111",
     *          "trans_id": "",
     *          "trans_date": "",
     *          "trans_qty": 0,
     *          "total_num": 0,
     *          "is_today": false,
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function transaction_item()
    {
        $input = request()->only('mbr_id', 'item_id');
        $rules = [
            'mbr_id' => 'required',
            'item_id' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_id = $input['mbr_id'];
        $item_id = $input['item_id'];
        $res = $this->transactionRepository->getTransactionItem($mbr_id, $item_id);
        if ($res === 0) {
            return $this->successWithData([
                'mbr_id' => $mbr_id,
                'item_id' => $item_id,
                'is_today' => false,
                'trans_id' => '',
                'trans_date' => '',
                'trans_qty' => 0,
                'total_qty' => 0
            ]);
        }
        else {
            $total_qty = $this->transactionRepository->getTotalNum($mbr_id,$item_id);
            return $this->successWithData([
                'mbr_id' => $mbr_id,
                'item_id' => $item_id,
                'trans_id' => trim($res->trans_id),
                'trans_date' => $res->trans_date,
                'trans_qty' => $res->item_qty,
                'total_qty' => $total_qty,
                'is_today' => ($res->purchase_today == 1) ? true : false
            ]);
        }
    }

    public function get_transaction_item($trans_id, $item_id)
    {
        $data = Transaction::where(['trans_id'=>$trans_id, 'item_id'=>$item_id])->first();
//        $data = $this->transactionRepository->get_transaction_item($trans_id, $item_id);
        return $this->successWithData($data);
    }

    public function _issue_coupon($mbrId, $mbrType, $transId, $transDate)
    {
        $now = Carbon::now()->toDateTimeString();
        $id = $this->voucherIssueDataRepository->getCouponId($mbrType, $transDate);
        $data = [];
        foreach ($id as $v) {
            if ($mbrType === 'MST') {
                $exp_date = Carbon::today()->endOfDay()->addDays((int)$v->days_to_expiry)->toDateTimeString();
                $transId = '';
            } else {
                $exp_date = Carbon::today()->endOfDay()->addDays((int)$v->days_to_expiry)->toDateTimeString();
            }
            if ($mbrType === 'MST') {
                $is_exists = \App\Models\Member\Voucher\Voucher::where([
                    'mbr_id' => $mbrId,
                    'coupon_id' => $v->coupon_id
                ])->exists();
            } else {
                $is_exists = \App\Models\Member\Voucher\Voucher::where([
                    'mbr_id' => $mbrId,
                    'receipt_id1' => $transId,
                    'coupon_id' => $v->coupon_id
                ])->exists();
            }
            if (!$is_exists) {
                if ($mbrType === 'MST') {
                    $dt_format = Carbon::now()->format('yM');
                    $serial = 'STU' . strtoupper($dt_format) . '-' . strtoupper(Str::Random(6));
                } else {
                    $serial = $this->_quick_random(7);
                }
                $data[] = array(
                    'coy_id' => $this->coyId,
                    'coupon_id' => $v->coupon_id,
                    'coupon_serialno' => $serial,
                    'promocart_id' => $v->coupon_id,
                    'mbr_id' => $mbrId,
                    'status_level' => 0,
                    'issue_date' => $now,
                    'sale_date' => $now,
                    'utilize_date' => '1900-01-01 00:00:00',
                    'expiry_date' => $exp_date,
                    'voucher_amount' => 0,
                    'receipt_id1' => $transId,
                    'created_by' => 'hachi',
                    'created_on' => $now,
                    'modified_by' => 'hachi',
                    'modified_on' => $now
                );
            }
        }
        \App\Models\Member\Voucher\Voucher::insert($data);
//        ($data);
        return true;
    }

    public function _quick_random($len)
    {
        do {
            //generate a random string using Laravel's str_random helper
            $serial = strtoupper('MEM-' . Str::Random($len));
        } //check if already exists and if it does, try again
        while (\App\Models\Member\Voucher\Voucher::where('coupon_serialno', $serial)->first());
        return $serial;
    }

    public function check_transaction($trans_id)
    {
        $data = $this->transactionRepository->check_transaction($trans_id);
        return $this->successWithData($data);
    }
}
