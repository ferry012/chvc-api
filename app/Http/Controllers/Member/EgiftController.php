<?php

namespace App\Http\Controllers\Member;

use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\SysTransListRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\EgiftRepository;
use App\Repositories\Member\Voucher\VoucherIssueDataRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Mailstream\EmarsysController;

class EgiftController extends Controller
{
    use Result;
    private $memberRepository;
    private $transactionRepository;
    private $voucherRepository;
    private $voucherIssueDataRepository;
    private $sysTransListRepository;
    private $egiftRepository;
    private $emarsysController;

    public function __construct(MemberRepository $memberRepository,
                                TransactionRepository $transactionRepository,
                                VoucherRepository $voucherRepository,
                                SysTransListRepository $sysTransListRepository,
                                EgiftRepository $egiftRepository,
                                VoucherIssueDataRepository $voucherIssueDataRepository,
                                EmarsysController $emarsysController)
    {
        $this->memberRepository = $memberRepository;
        $this->transactionRepository = $transactionRepository;
        $this->voucherRepository = $voucherRepository;
        $this->voucherIssueDataRepository = $voucherIssueDataRepository;
        $this->sysTransListRepository = $sysTransListRepository;
        $this->egiftRepository = $egiftRepository;
        $this->emarsysController = $emarsysController;
    }

    /**
     * @api {post} /api/egifts/insert Insert Egift
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} coy_id Coy ID
     * @apiParam {string} coupon_id Coupon ID
     * @apiParam {string} trans_id Trans ID
     * @apiParam {string} buyer_id Buyer ID
     * @apiParam {string} buyer_name Buyer Name
     * @apiParam {string} recipient_name Recipient Name
     * @apiParam {string} recipient_email Recipient Email
     * @apiParam {string} gift_message Gift Message
     * @apiParam {string} gift_image Gift Image
     * @apiParam {number} trans_qty Trans Qty
     * @apiParam {number} trans_amount Trans Amount
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "coy_id": "CTL",
     *      "coupon_id": "!EGIFT-20",
     *      "trans_id": "TEST1111",
     *      "buyer_id": "V150357700",
     *      "buyer_name": "Test",
     *      "recipient_name": "test",
     *      "recipient_email": "li.liangze@challenger.sg",
     *      "gift_message": "Enjoy",
     *      "gift_image": "test.jpg",
     *      "trans_qty": 100,
     *      "trans_amount": 100
     *  }
     *
     * @apiSuccessExample Insert success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful insert egift",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Buyer id error
     * HTTP/1.1 buyer id error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, buyer_id invalid."
     *  }
     *
     * @apiErrorExample recipient_name invalid
     * HTTP/1.1 recipient_name invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, recipient_name invalid."
     *  }
     *
     */
    public function insert()
    {
        $input = request()->only(['coy_id', 'coupon_id', 'trans_id', 'buyer_id', 'buyer_name', 'recipient_name', 'recipient_email', 'gift_message',
            'gift_image', 'trans_qty', 'trans_amount']);
        $rules = [
            'coy_id' => 'required',
            'coupon_id' => 'required',
            'trans_id' => 'required',
            'buyer_id' => 'required',
            'buyer_name' => 'required',
            'recipient_name' => 'required',
            'recipient_email' => 'required',
            'gift_message' => 'required',
            'gift_image' => 'required',
            'trans_qty' => 'required',
            'trans_amount' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $mbr_from = $this->memberRepository->findByMbrId(rtrim($input['buyer_id']));
        if (empty($mbr_from)) {
            return $this->errorWithCodeAndInfo(500, "Sorry, buyer_id invalid.");
        }
//        $mbr_to = $this->memberRepository->findWhere(['coy_id'=>'CTL', 'email_addr'=>$input['recipient_email']])->first();
//        if (empty($mbr_to)) {
//            return $this->errorWithCodeAndInfo(500, "Sorry, recipient_name invalid.");
//        }
        $now = Carbon::now()->toDateTimeString();
        $expiry_date = Carbon::now()->addDays(60);

        $data = $this->egiftRepository->create([
            'coy_id' => $input['coy_id'],
            'coupon_id' => $input['coupon_id'],
            'trans_id' => $input['trans_id'],
            'buyer_id' => $input['buyer_id'],
            'buyer_name' => $input['buyer_name'],
            'recipient_name' => $input['recipient_name'],
            'recipient_email' => $input['recipient_email'],
            'gift_message' => $input['gift_message'],
            'gift_image' => $input['gift_image'],
            'trans_qty' => $input['trans_qty'],
            'trans_amount' => $input['trans_amount'],
            'status_level' => 0,
            'claim_expiry' => $expiry_date,
            'claim_by' => 'NA',
            'claim_on' => $now,
            'created_by' => 'hachi',
            'created_on' => $now,
            'modified_by' => 'hachi',
            'modified_on' => $now
        ]);

        // Trigger emarsys egift email
        $acturl = (env('APP_ENV')=="production") ? "https://www.challenger.sg/account/claim_egift" : "https://hachi3.sghachi.com/account/claim_egift";
        $mailstream_data = [
            "email"     => $data->recipient_email,
            "r_name"    => $data->recipient_name,
            "r_amount"  => '$' . $data->trans_amount,
            "r_image"   => "<img width='100%' src='" . $data->gift_image . "'>",
            "r_message" => $data->gift_message,
            "r_fromname"=> $data->buyer_name,
            "r_giftcode"=> $data->coupon_serialno,
            "r_acturl"  => "<a href='" . $acturl . "'>"
        ];
        $sent = $this->emarsysController->do_mailstream('EGIFT_RECIPIENT', $input['trans_id'], $input['recipient_email'], '', 'EGIFT', $mailstream_data);

        return $this->successWithInfo("Success insert egift");
    }

    /**
     * @api {post} /api/egifts/claim Claim Egift
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} recipient_email Recipient Email
     * @apiParam {string} coupon_serialno Coupon Serial Number
     * @apiParam {string} claim_by Claim By
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "recipient_email": "test@challenger.sg",
     *      "coupon_serialno": "GV8XA2WGKD",
     *      "claim_by": "3810"
     *  }
     *
     * @apiSuccessExample Claim success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful claim",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Coupon Expiry
     * HTTP/1.1 Coupon Expiry
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Coupon Expiry"
     *  }
     *
     * @apiErrorExample Coupon has been claimed
     * HTTP/1.1 Coupon has been claimed
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Coupon has been claimed"
     *  }
     *
     * @apiErrorExample Invalid Serial No
     * HTTP/1.1 Invalid Serial No
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Invalid Serial No"
     *  }
     *
     */
    public function claim()
    {
        $input = request()->only(['coupon_serialno', 'claim_by', 'recipient_email']);
        $rules = [
            'recipient_email' => 'required',
            'coupon_serialno' => 'required',
            'claim_by' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $now = Carbon::now()->toDateTimeString();
        $serial = $input['coupon_serialno'];
        $user = $input['claim_by'];
        $email = $input['recipient_email'];
        $res = $this->egiftRepository->check_by_serial($serial, $email, $user);
        if ($res === 3) {
            $coupon = $this->egiftRepository->findBy('coupon_serialno',$serial);
            $sender = $this->memberRepository->findByMbrId($coupon->buyer_id);
//            $member = $this->memberRepository->findBy('email_addr', $coupon->recipient_email);
            $exp_date = Carbon::now()->endOfDay()->addDays(60)->toDateTimeString();
            $this->voucherRepository->create([
                'coy_id' => 'CTL',
                'coupon_id' => $coupon->coupon_id,
                'coupon_serialno' => $serial,
                'promocart_id' => $coupon->coupon_id,
                'mbr_id' => $user,
                'trans_id' => '',
                'trans_date' => $now,
                'ho_ref' => '',
                'ho_date' => $now,
                'issue_date' => $now,
                'sale_date' => $now,
                'utilize_date' => $now,
                'expiry_date' => $exp_date,
                'print_date' => $now,
                'loc_id' => '',
                'receipt_id1' => $coupon->trans_id,
                'receipt_id2' => '',
                'voucher_amount' => $coupon->trans_amount,
                'redeemed_amount' => 0,
                'expired_amount' => 0,
                'issue_type' => 'GV',
                'issue_reason' => '',
                'status_level' => 0,
                'created_by' => $user,
                'created_on' => $now,
                'modified_by' => $user,
                'modified_on' => $now,
            ]);
            $dataset = [
                "email" => $sender->email_addr,
                "r_name" => $coupon->recipient_name,
                "r_amount" => "$".number_format($coupon->trans_amount,2),
                "r_image" => "<img width='100%' src='".$coupon->gift_image."'>",
                "r_message" => $coupon->gift_message,
                "r_fromname" => $coupon->buyer_name,
                "r_email" => $coupon->recipient_email
            ];
            $this->emarsysController->do_mailstream('EGIFT_ACTIVATED_SENDER', $coupon->trans_id, $sender->email_addr, '', 'EGIFT', $dataset);
//            app('App\Http\Controllers\Mailstream\EmarsysController')->do_mailstream('EGIFT_ACTIVATED_SENDER', "", $sender->email_addr, "hachi", $dataset);
            return $this->successWithInfo("Success claim");
        } elseif ($res === 1) {
            return $this->errorWithCodeAndInfo(500,"Coupon Expiry");
        } elseif ($res === 2) {
            return $this->errorWithCodeAndInfo(500,"Coupon has been claimed");
        } else {
            return $this->errorWithCodeAndInfo(500,"Invalid Serial No");
        }

    }

    /**
     * @api {get} /api/egifts/egift_purchased/{transId} eGifts Purchased
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} transId Trans id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "image": "",
     *          "claim_expiry": "2020-04-16 23:59:59",
     *          "voucher_amount": 0,
     *          "status_level": 0,
     *          "coupon_serialno": "MEM-9CTL6J2",
     *          "coupon_expiry_date": "2021-06-08 23:59:59"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Trans id invalid
     * HTTP/1.1 Trans id invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Trans id invalid"
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function egift_purchased($trans_id)
    {
        $res = $this->egiftRepository->check_exist($trans_id);
        if ($res) {
            $serial = $res->coupon_serialno;
            $voucher_expiry_date = '';
            if ($res->status_level == 2) {
                $voucher_exist = $this->voucherRepository->check_by_serial($serial);
                if ($voucher_exist) {
                    $voucher_expiry_date = $voucher_exist->expiry_date;
                }
            }
            return $this->successWithData([
                "image" => $res->gift_image,
                "claim_expiry" => $res->claim_expiry,
                "voucher_amount" => number_format($res->trans_amount,2),
                "status_level" => $res->status_level,
                "coupon_serialno" => $res->coupon_serialno,
                "coupon_expiry_date" => $voucher_expiry_date
            ]);
        } else {
            return $this->errorWithInfo("Trans id invalid");
        }
    }

    /**
     * @api {get} /api/egifts/egift_claimed/{mbrId}/{status?} eGifts Claimed
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} mbrId Member id
     * @apiParam {int} [status] Status Level, 0 is not in use, 1 is used. default is 0
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "image": "",
     *          "expiry_date": "2020-04-16 23:59:59",
     *          "voucher_amount": 0,
     *          "status_level": 0,
     *          "coupon_serialno": "MEM-9CTL6J2"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or serial no is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member ID or Serial No is null."
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function egift_claimed($mbrId, $status = 0)
    {
        if (empty(rtrim($mbrId))) {
            return $this->errorWithCodeAndInfo('500', 'Member ID or Serial No is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        if ((int)$status !== 0 && (int)$status !== 1) {
            return $this->errorWithCodeAndInfo('500', 'Status should be 0 or 1.');
        }
        $status = (int)$status;
        $data = Voucher::hachiGift($mbrId, $status);
        return $this->successWithData($data);
    }

    /**
     * @api {post} /api/egifts/update_egift Update Egift
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} coupon_serialno Coupon Serial Number
     * @apiParam {string} [expiry_date] Expiry Date
     * @apiParam {string} [status_level] Status Level only 0 or 1
     * @apiParam {string} updated_by Update By
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "coupon_serialno": "GV8XA2WGKD",
     *      "expiry_date": "2022-02-01 00:00:00",
     *      "status_level": 0,
     *      "updated_by": "3810"
     *  }
     *
     * @apiSuccessExample Update success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful update",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Invalid Serial No
     * HTTP/1.1 Invalid Serial No
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Invalid Serial No"
     *  }
     *
     */
    public function update_egift()
    {
        $input = request()->only(['coupon_serialno', 'expiry_date', 'status_level', 'updated_by']);
        $rules = [
            'coupon_serialno' => 'required',
            'updated_by' => 'required',
            'status_level' => ['nullable', Rule::in([0,1])]
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $serial = $input['coupon_serialno'];
        $user = $input['updated_by'];
        $gift_exist = $this->egiftRepository->check_serial_exist($serial);
        if (!$gift_exist) {
            return $this->errorWithInfo("Serial No invalid.");
        }
//        $gift = $this->voucherRepository->check_by_serial($serial);
//        if ($gift) {
        $status_level = $gift_exist->status_level;
        $expiry_date = isset($input['expiry_date']) ? $input['expiry_date'] : $gift_exist->expiry_date;
        $this->egiftRepository->update_egift($serial, $expiry_date, $status_level, $user);
        return $this->successWithInfo("Update egift success.");
    }

    /**
     * @api {post} /api/egifts/update_egift_coupon Update Egift Coupon
     * @apiVersion 1.0.0
     * @apiGroup 4.VOUCHERS-EGIFT
     *
     * @apiParam {string} coupon_serialno Coupon Serial Number
     * @apiParam {string} [expiry_date] Expiry Date
     * @apiParam {string} [status_level] Status Level only 0 or 1
     * @apiParam {string} updated_by Update By
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "coupon_serialno": "GV8XA2WGKD",
     *      "expiry_date": "2022-02-01 00:00:00",
     *      "status_level": 0,
     *      "updated_by": "3810"
     *  }
     *
     * @apiSuccessExample Update success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful update",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Invalid Serial No
     * HTTP/1.1 Invalid Serial No
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Invalid Serial No"
     *  }
     *
     */
    public function update_egift_coupon()
    {
        $input = request()->only(['coupon_serialno', 'expiry_date', 'status_level', 'updated_by']);
        $rules = [
            'coupon_serialno' => 'required',
            'updated_by' => 'required',
            'status_level' => ['nullable', Rule::in([0,1])]
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $serial = $input['coupon_serialno'];
        $user = $input['updated_by'];
        $gift = $this->voucherRepository->check_by_serial($serial);
        if ($gift) {
            $status_level = $gift->status_level;
            $expiry_date = isset($input['expiry_date']) ? Carbon::parse($input['expiry_date'])->toDateTimeString() : $gift->expiry_date;
            $this->voucherRepository->update_egift($serial, $expiry_date, $status_level, $user);
            return $this->successWithInfo("Update egift coupon success.");
        }
    }

}
