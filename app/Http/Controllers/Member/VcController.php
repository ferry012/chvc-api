<?php

namespace App\Http\Controllers\Member;

use App\Events\UserLogin;
use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Http\Resources\User;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\VcMemberRepository;
use App\Services\EmarSys\EmarsysServiceV2;
use App\Services\IMS\Cherps2Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

//use Hashids\Hashids;


class VcController extends Controller
{
    use Result;

    private $db20;
    private $chapps;
    private $memberRepository;
    private $vcmemberRepository;
    private $addressRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $cherps2Service;

    private $posTokenExpire = 30; // seconds
    private $threeMonths = 7890000;
    private $oneDays = 86400;
    private $threeDays = 259200;
    private $fiveDays = 432000;
    private $oneWeek = 604800;
    private $productReview_key = "_ProductReview";
    private $reviewLastUpdate = "_ReviewLastUpdate";
    private $emarsysService;
    private $IMAGE_URL = "https://cdn.hachi.tech/";

    private $associate_mbr_limit = 3;
    private $appVersion = ["2.8.27", "2.2.55", '2.8.28', '3.0.5', '3.0.7', '3.0.8', '3.0.10','3.0.11','3.0.12'];
    private $_oneSignal_AppID = "fb230403-0130-415e-9bb8-3bad87f3d7a6";
    private $messages = array(
        "OLDER_VERSION_APP" => array(
            "title" => "A newer version of this application is available. Would you like to update now?",
            "message" => "A newer version of this application is available. Would you like to update now?"
        ),
        "INVALID_ACCESS" => array(
            "title" => "Invalid Access",
            "message" => "You do not have permission to access this service"
        ),
        "ASSO_ACCOUNT_NOT_ACTIVATE" => array(
            "title" => "Account is inactive.",
            "message" => "Your associate account is not activated. Please check your email for activation link. Otherwise, please contact Customer Service for assistance."
        ),
        "KEEP_LOGIN_SECURE" => array(
            "status" => 1,
            "message" => "Safeguard your rebates/redemptions/transfers and donations.<br/>Keep your log-in details secure."
        ),
        "SIGNUP_INCOMPLETE" => array(
            "title" => "Sign up Info",
            "message" => "Field required"
        ),
        "EMAIL_SENT" => array(
            "title" => "Email Sent",
            "message" => "A link to reset your password has been sent to your email address, please check your inbox to reset password"
        ),
        "EMAIL_NOT_EXIST" => array(
            "title" => "Invalid Email",
            "message" => "Please contact Customer Service for assistance."
        ),
    );

    private $_donateRebatesMbrID = 'V101761400'; //CHSOC0010101 // 'S8835434F';

    private $mbrShipFee = array(
        "!MEMBER-NEW08" => "8.00",
        "!MEMBER-NEW18" => "18.00",
        "!MEMBER-NEW28" => "28.00",
        "!MEMBER-REN08" => "8.00",
        "!MEMBER-REN18" => "18.00",
        "!MEMBER-REN28" => "28.00",
    );
    private $mbrShipFee_viaRebate = array(
        "!MEMBER-NEW08" => "8.00",
        "!MEMBER-NEW18" => "18.00",
        "!MEMBER-NEW28" => "25.00",
        "!MEMBER-REN08" => "8.00",
        "!MEMBER-REN18" => "18.00",
        "!MEMBER-REN28" => "25.00",
    );
    private $signupFee = "30.00";
    private $payTypeDefault = "2c2p";

    private $member_type_list = array(
        "STUDENT" => 'MST',
        'STUDENT_PLUS' => 'MSTP',
        "NSMEN" => 'MNS',
        "ASSOCIATE" => 'MAS',
        "B2B" => "MB2B"
    );
    private $oneSignal = array(
        "APP_ID" => "fb230403-0130-415e-9bb8-3bad87f3d7a6",
        "APIKEY_BASIC" => "Mjk5ZWNiYTQtOWVlYi00MzAxLWFmZjMtYWI4NGZlYzFjNmMx",
        "URL_PLAYERS" => "https://onesignal.com/api/v1/players",
        "URL_NOTIFICATION" => "https://onesignal.com/api/v1/notifications"
    );
    private $mechantId = '702702000000169';
    private $renewRebateFee = "20.00";

    private $tokenKeys = array(
        "NOTIFY_FORGOT" => "vctoken#notifyforgot#",
        "NOPUSH_POSGETTOKEN" => "nopush_posgettoken#",
        "NOPUSH_BATCHNOTIFY" => "nopush_batchnotify#"
    );
    private $emarsys_template_list = array(
        "UPDATE_PROFILE" => "UPDATE_PROFILE",
        "SUCCESS_UPDATE_PROFILE" => "SUCCESS_UPDATE_PROFILE",
        "RESET_PASSWORD_RETURN" => "RESET_PASSWORD_RETURN",
        "VC_E_RECEIPT_MBR" => "VC_E_RECEIPT_MBR",
        "ASSOCIATE_MAIN" => "ASSOCIATE_MAIN",
        "ASSOCIATE_WELCOME" => "ASSOCIATE_WELCOME",
        "CHANGE_PASSWORD" => "CHANGE_PASSWORD",
        "STUDENT_MEMBERSHIP" => "STUDENT_MEMBERSHIP",
        "ASSOCIATE_REMINDER" => "ASSOCIATE_REMINDER",
        "VC_FORGETPASSWORD" => "VC_FORGETPASSWORD",
        "WEB_FORGOT_PASSWORD" => "WEB_FORGOT_PASSWORD",
        "VC_E_RECEIPT" => "VC_E_RECEIPT",
        "ASSOCIATE_INVITE" => "ASSOCIATE_INVITE",
        "FINISH_PROD_REVIEW" => "FINISH_PROD_REVIEW",
        "SEND_REFER_FRIENDS" => "SEND_REFER_FRIENDS",
        "SIGNUP_8MONTHS" => "SIGNUP_8MONTHS",
        "SIGNUP_18MONTHS" => "SIGNUP_18MONTHS",
        "SIGNUP_28MONTHS" => "SIGNUP_28MONTHS",
        "UPGRADE_MBRSHIP" => "UPGRADE_MBRSHIP",
        "RENEW_8MONTHS" => "RENEW_8MONTHS",
        "RENEW_18MONTHS" => "RENEW_18MONTHS",
        "RENEW_28MONTHS" => "RENEW_28MONTHS",
        "SENDER_REBATE_DONATE" => "SENDER_REBATE_DONATE",
        "SENDER_REBATE_TRANSFER" => "SENDER_REBATE_TRANSFER",
        "RECEIVER_REBATE_TRANSFER" => "RECEIVER_REBATE_TRANSFER",
        "STU_UPT_MBRSHIP_PLUS" => "STU_UPT_MBRSHIP_PLUS",
        "LOW_INK_REMINDER" => "LOW_INK_REMINDER",
        "HP_ACTIVATION_NEW" => "HP_ACTIVATION_NEW",
        "EXPIRED_ASSOCIATE" => "EXPIRED_ASSOCIATE",
        "PASSWORD_RESET_MOBILE" => "PASSWORD_RESET_MOBILE",
        "STU_UNLOCK_REMINDER" => "STU_UNLOCK_REMINDER",
        "NSMEN_SIGNUP_ACTIVATE" => "NSMEN_SIGNUP_ACTIVATE",
        "NSMEN_WELCOME" => "NSMEN_WELCOME"
    );

    private $s3Config = array(
        "srcBucket" => "https://s3-ap-southeast-1.amazonaws.com/ctl-public-test",
        "bucketFullpath" => "https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/assets/product_review/",
        "bucketSubpath" => "assets/product_review/",
        "bucketJsonpath" => "assets/json_hp/",
        "bucketSubpath2" => "assets/onesignal_push/",
        "bucketSubpathRun" => "assets/onesignal_push/run/",
        "bucketSubpathRunTest" => "assets/onesignal_push/test/",
        "awsAccessKey" => '',
        "awsSecretKey" => '',
        "googleURLShort" => "https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyCp0yryonM-yAsrHzrrAZcp6YyG0yP69kU"
    );

    private $chappsDb = true;
    private $emarsys_authorization_bearer = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
    private $vcapiUrl;
    private $server_name;
    private $redirectUrl;
    private $apiUrl;
    private $hachiPass = '@hachi1234';
    private $_accessPass = "@hachi1234";
    private $pdfUrl;
    private $CTL_COY_ID = 'CTL';
    private $imsUrl;
    private $ENVIRONMENT;
    private $X_authorization = "NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
    private $AUTOLOGIN_TOKEN = "NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
    private $hpAccess_token = 'NTExZWVlMTItZThkYy00ZmEwLWJmZTUtMjEyZjZlYzUyZjcy';
    private $hpPartnerId = "9898906fe5fb41a5a8171e334e8f9ced";
    private $_redis_prefix = "ValueClub30#";
    private $_login_token_prefix = "VC_Token30";
    private $_redis_expiry_time = 259200;
    private $hkey;

    public function __construct(MemberRepository $memberRepository, AddressRepository $addressRepository, PointsRepository $pointsRepository, TransactionRepository $transactionRepository,
                                EmarsysServiceV2 $emarsysService, VcMemberRepository $vcmemberRepository, Cherps2Service $cherps2Service)
    {
        $this->memberRepository = $memberRepository;
        $this->addressRepository = $addressRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->emarsysService = $emarsysService;
        $this->vcmemberRepository = $vcmemberRepository;
        $this->cherps2Service = $cherps2Service;


        $prod_mode = env("APP_ENV");

        if (strpos($_SERVER['REQUEST_URI'], 'mode=prod') !== false) {
            $prod_mode = 'production';
        }
        //Test
//        $prod_mode = "production";
//        echo $prod_mode;


        if (($prod_mode == 'production')) {
            $this->ENVIRONMENT = "production";
            $this->apiUrl = "https://web4.sghachi.com/api/Vc2/";
            $this->server_name = "vc.api.valueclub.asia";
//            $this->vcapiUrl = "https://vc.api.valueclub.asia/";
            $this->vcapiUrl = "https://chstaff.challenger.sg/";
            $this->redirectUrl = "chstaff.challenger.sg";
            $this->imsUrl = "https://ims.api.valueclub.asia/api/";
            $this->pdfUrl = 'https://hachi.tech/';


        } else if ($prod_mode == 'local') {

            $this->ENVIRONMENT = "development";
            $this->apiUrl = "https://web3.sghachi.com/api/Vc2/";
            $this->server_name = "chvc-api.sghachi.com";
            $this->redirectUrl = "chvc-api.sghachi.com";
            $this->imsUrl = "https://chims-test.sghachi.com/api/";
            $this->pdfUrl = 'https://hachi3.sghachi.com/';
            $this->vcapiUrl = "https://chvc-api.sghachi.com/";
//            $this->vcapiUrl = "http://localhost/";

        } else {
            //mode: development
            $this->ENVIRONMENT = "development";
            $this->apiUrl = "https://web3.sghachi.com/api/Vc2/";
            $this->server_name = "chvc-api.sghachi.com";
            $this->redirectUrl = "chvc-api.sghachi.com";
            $this->imsUrl = "https://chims-test.sghachi.com/api/";
            $this->pdfUrl = 'https://hachi3.sghachi.com/';
            $this->vcapiUrl = "https://chvc-api.sghachi.com/";

        }

        $this->hkey = $this->server_name . 'bitly#Keys';

        if ($this->ENVIRONMENT == "development") {
            $this->overrideRedis = false;
            $this->hpPartnerId = "T8K5mFjhe9U6o9WjUv7z2jnNyMuQUkh3";
        }

        //AWS Secret Key
        $this->s3Config["awsAccessKey"] = env('VC_AWS_ACCESS_KEY');
        $this->s3Config["awsSecretKey"] = env('VC_AWS_ACCESS_SECRET');


        if (!$this->db20) {
            $this->db20 = DB::connection('pgsql');
        }

        if (!$this->chapps) {
            $this->chapps = DB::connection('vcapps');
        }

    }

    public function test()
    {
        $time = Carbon::now()->startOfDay()->toDateTimeString();
        print_r($time);

//        $insert_data = array(
//            "coy_id" => 'CTL',
//            'mbr_id' => 'V123123123',
//            "line_num" => 1,
//            "eff_from" => Carbon::now()->startOfDay()->toDateTimeString(),
//            "eff_to" => Carbon::now(),
//            "mbr_type" => 'M08',
//            "created_by" => '3561',
//            "created_on" => Carbon::now(),
//            "modified_by" => '',
//            "modified_on" => Carbon::now()
//        );
//
//        $res = $this->db20
//            ->table('crm_member_type')
//            ->insert($insert_data);


        return $this->apiSuccessResponse(1, 'Success', "OKK");
    }

    public function deployCheck()
    {
        $number = '2022-07-27 17.40 (update qr_login)';
        return $this->apiSuccessResponse(1, 'Success', $number);
    }

    public function testtoken()
    {
        return $this->apiSuccessResponse(1, 'Success', "TOKEN OK");
    }

    public function callbackPaymentResult(Request $request)
    {
        echo "Redirecting to Challenger.sg...";

        $post = $request->data;

        // var_dump($post);
        $decoded_post = base64_decode($post);
        $this->_logprocess('CALLBACK_DATA', 'ok', $decoded_post, '');

        // echo ($post);
        $decoded_obj = json_decode($decoded_post);

        // print_r ($decoded_obj->response);
        if (property_exists($decoded_obj, 'response')) {
            $trans_id = $decoded_obj->response->refNumber;
            $mbr_id = $decoded_obj->response->userDefined3;
            // $userDefined4 = $decoded_obj->response->userDefined4;

            $sql_select = <<<str
            select * from oms_temp_member where txnref = '$trans_id' and mbr_id = '$mbr_id';
str;
            $user = $this->db20->select($sql_select);

            // var_dump($user[0]);
            $dob = $user[0]->dob;
            $mbr_title = $user[0]->mbr_title;
            $fname = $user[0]->first_name;
            $lname = $user[0]->last_name;
            $renew_type = $user[0]->upgrade_id;
            $renew_type = trim($renew_type);
            $decoded_obj->type = $renew_type;
            if ($renew_type == 'signup') {
                $item_id = "!MEMBER-NEW28";
            } else {
                $item_id = "!MEMBER-REN28";
            }
            $staff_id = $user[0]->staff_id ? $user[0]->staff_id : '';

            $sql_check = <<<str
    select * from o2o_log_live_processes where process_id='CALLBACK_SIGNUP' and mbr_id='$trans_id'
str;
            $validate = $this->db20->select($sql_check);
            if (!$validate) {
                //Continue if no log inserted.
                $this->_logprocess('CALLBACK_SIGNUP', 'ok', json_encode($decoded_obj), $trans_id);

                $data = [];

                if ($this->ENVIRONMENT == "development") {
                    $url = 'https://chvc-api.sghachi.com/api/vc2/update_status/' . $mbr_id . "/" . $item_id . "/" . $trans_id;
                } else {
                    $url = 'https://vc.api.valueclub.asia/api/vc2/update_status/' . $mbr_id . "/" . $item_id . "/" . $trans_id;
                }

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                $result = curl_exec($ch);
                curl_close($ch);

                print_r($result);

            }
            if ($renew_type == "renew") {
                $sql_update = <<<str
            UPDATE crm_member_list SET first_name = '$fname', last_name = '$lname', birth_date = '$dob', mbr_title='$mbr_title' WHERE mbr_id = '$mbr_id' ;
str;
                $this->db20->statement($sql_update);
            }
            $sql_updateTrans = <<<str
            UPDATE crm_member_transaction SET salesperson_id = '$staff_id' WHERE trans_id = '$trans_id' ;
str;
            $this->db20->statement($sql_updateTrans);

            // die();
        } else {
            //    6 MONTHS
            $cust_id = $decoded_obj->list->cust_id;
            $sql_select = <<<str
            select * from oms_temp_member where mbr_id = (
                select mbr_id from sls_customer_list where cust_id= '$cust_id'
                ) order by created_on desc limit 1;
str;
            $results = $this->db20->select($sql_select);
            $renew_type = $results[0]->upgrade_id;
            $renew_type = trim($renew_type);
            $mbr_id = $results[0]->mbr_id;
            $trans_id = $decoded_obj->trans[0]->trans_id;
            $staff_id = $results[0]->staff_id ? $results[0]->staff_id : '';
            $fname = $results[0]->first_name;
            $lname = $results[0]->last_name;
            $dob = $results[0]->dob;
            $mbr_title = $results[0]->mbr_title;


            if ($renew_type == "renew") {
                $sql_update = <<<str
            UPDATE crm_member_list SET first_name = '$fname', last_name = '$lname', birth_date = '$dob', mbr_title='$mbr_title' WHERE mbr_id = '$mbr_id' ;
str;
                $this->db20->statement($sql_update);
                $sql = <<<str
                UPDATE sls_customer_list SET first_name = '$fname', last_name = '$lname' WHERE mbr_id = '$mbr_id' ;
str;
                $this->db20->statement($sql);
            }

            $sql_updateTrans = <<<str
            UPDATE crm_member_transaction SET salesperson_id = '$staff_id' WHERE trans_id = '$trans_id' ;
str;
            $this->db20->statement($sql_updateTrans);
        }

        $post = base64_encode(json_encode($decoded_obj));

        print_r($post);
        header("Location: https://mss.challenger.sg/vcmembership/#/confirmation?d=" . $post);
        exit();
    }

    public function testUpdate()
    {
        $data = [];

        $mbr_id = 'V2200258VC';
        $item_id = "!MEMBER-NEW28";
        $trans_id = "OS20220529T0001";
        $url = 'https://chvc-api.sghachi.com/api/vc2/update_status/' . $mbr_id . "/" . $item_id . "/" . $trans_id;
//        echo $url;
//        die();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);

        print_r($result);

        return $this->apiSuccessResponse(1, 'Success', "OKK");
    }

    public function membershipPaymentCurl(Request $request)
    {
        $email = $request->email;
        $contact = $request->contact;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $item_id = $request->item_id;

        $renew_type = $request->type;

        $staff_id = $request->staff_id ? $request->staff_id : '';
        $promo_code = $request->promo ? $request->promo : '';
        $dob = $request->birth_date;
        $mbr_title = $request->mbr_title;


        $env = "dev";

        //Put Age range & Salutation here.

        $headers = array(
            'Content-Type: application/json'
        );

        if ($renew_type == 'signup') {
            //Will Generate New Mbr_id Here.
            $mbr_id = $request->signup_mbr_id;
        } else {
            $mbr_id = $request->mbr_id;
        }

        if ($this->ENVIRONMENT == "development") {
            $callback_url = 'https://chvc-api.sghachi.com/api/vc2/callback_vc_payment';
        } else {
            $callback_url = 'https://vc.api.valueclub.asia/api/vc2/callback_vc_payment';
        }

        // $callback_url = 'https://chvc-api.sghachi.com/api/vc2/callback_vc_payment';
        $payment_mode = "RPP";

        $trans_id = "";
        if ($item_id == '!MEMBER-NEW28' || $item_id == '!MEMBER-REN28') {
            $payment_mode = "CC";
            $trans_id = $this->vcmemberRepository->getNextInvoiceNo();

            //Insert To Temp Member
            $sql = <<<str
insert into oms_temp_member (txnRef,mbr_id,first_name,last_name,
                             email_addr,contact_num,mbr_pwd,amount,
                             txnDate,mid,payment,created_on,created_by,txnTime,status,
                             error_code,upgrade_id, dob, mbr_title, staff_id) values 
  (?,?,?,?,
   ?,?,?,?,
   current_timestamp ,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,
   ?,?,?,?,?)
str;
            $password = "";
            $amount = 28;
            $tstatus = "";
            $errorCode = "";
            $payment = 28;
            $mid = $this->mechantId;

            $this->db20->statement($sql, [$trans_id, $mbr_id, $first_name, $last_name,
                $email, $contact, $password, $amount,
                $mid, $payment, $item_id, $tstatus,
                $errorCode, $renew_type, $dob, $mbr_title, $staff_id]);

        } else {
            //Sub06 Here.
            $sql = <<<str
insert into oms_temp_member (txnRef,mbr_id,first_name,last_name,
                             email_addr,contact_num,mbr_pwd,amount,
                             txnDate,mid,payment,created_on,created_by,txnTime,status,
                             error_code,upgrade_id, dob, mbr_title, staff_id) values 
  (?,?,?,?,
   ?,?,?,?,
   current_timestamp ,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,
   ?,?,?,?,?)
str;
            $password = "";
            $amount = 6;
            $tstatus = "";
            $errorCode = "";
            $payment = 6;
            $mid = $this->mechantId;

            $this->db20->statement($sql, [$item_id, $mbr_id, $first_name, $last_name, $email,
                $contact, $password, $amount, $mid, $payment,
                $item_id, $tstatus, $errorCode,
                $renew_type, $dob, $mbr_title, $staff_id]);


        }

        $post_data = array(
            "coy_id" => "CTL",
            "mbr_id" => $mbr_id,
            "item_id" => $item_id,
            "pay_mode" => $payment_mode,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $email,
            "contact" => $contact,
            "callback_url" => $callback_url,
            "trans_id" => $trans_id,
            "renew_type" => $renew_type,
            "dob" => $dob,
            "mbr_title" => $mbr_title,
            "staff_id" => $staff_id,
            "promo_code" => $promo_code
        );
        $this->_logprocess("VC_" . $renew_type, "0", json_encode($post_data), $trans_id, $mbr_id);

//        print_r($post_data);
//        die();
        if ($this->ENVIRONMENT == "development") {
            $url = 'https://chvc-api.sghachi.com/api/contract/subscription?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';
        } else {
            $url = 'https://vc.api.valueclub.asia/api/contract/subscription?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';
        }
        // $url = 'https://chvc-api.sghachi.com/api/contract/subscription?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';
//        $url = 'http://localhost:8090/api/contract/subscription?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        print_r($result);
    }

    public function testCurl($email, $mbr_id, $contact)
    {
        $headers = array(
            'Content-Type: application/json'
        );

        $post_data = array(
            "coy_id" => "CTL",
            "env" => "dev",
            "mbr_id" => $mbr_id,
            "item_id" => "!MEMBER-SUB28",
            "first_name" => "Wong",
            "last_name" => "tester",
            "email" => $email,
            "contact" => $contact
        );
//        print_r($post_data);

        $url = 'https://chvc-api.sghachi.com/api/contract/subscription?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        print_r($result);

    }

    public function testredis()
    {
//        $redisKey = $this->ENVIRONMENT . "#Vc_pushNotify_time#";
//        $redisKey = $this->ENVIRONMENT . "#oneSignal_push#";
        $hkey = $this->_redis_prefix ;

        $datas = Redis::keys('*' . $hkey . '*');
        foreach ($datas as $data) {
            echo $data. "<br/>";
//            Redis::del($data);
        }
//            $list = Redis::get($data);

//        echo $redisKey;
//        $del = Redis::del($redisKey);
//        print_r($del);
//        die();
//
////        die();
//        $redisKey = $this->server_name . "#Vc2_Notification_redis#";
//        $del = Redis::del($redisKey);
//        print_r($del);
//        die();

//        echo $redisKey;
//        $items = Redis::hgetall($redisKey);
//        print_r($items);
//        die();

////        $id='113650';
////        $this->insertPushRedis($id);

        //Insert Redis.
//        $redisKey = $this->ENVIRONMENT . "#Vc_pushNotify_time#";
//        $lists = Redis::hgetall($redisKey);
//
//        $timeNow = date("Y-m-d H:i:s");
//        $lists[] = date("Y-m-d H:i:s", strtotime($timeNow));
//
//        $expiry_sec = strtotime('+2 hours', strtotime($lists[count($lists) - 1])) - strtotime($timeNow);
//		echo $diff;
//		$expiry_sec= 86400;

//        Redis::hMset($redisKey, $lists);
//        Redis::expire($redisKey, $expiry_sec);

    }

    public function testemail()
    {

        $mbr_id = 'V160000500';
        $this->_insertCompletedRow($mbr_id);

//        $rebates_process_id = 'BINGO_REBATE_ROW1';
//        $earn_rebate = 28;
//        $the_trans_id = 'A00001';
//        $the_trans_date = '2021-10-23';
//
//
//        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);
//

//        $email = 'wong.weiseng@challenger.sg';
//        $email_data = [
//            "r_fname" => "HIH"
//        ];
//        $this->_sendEmarsysEmail($this->emarsys_template_list["NSMEN_WELCOME"], $email, $email_data);
//
    }

    public function removeLoginRedis($esid, $token)
    {
        if ($token !== "hachi12345") {
            print_r("Access Denied");
            return;
        }
        $sql_select = <<<str
    select * from crm_member_list where mbr_id='$esid' or email_addr='$esid'
str;
        $user = $this->db20->select($sql_select);
        if (count($user) > 0) {
            $email_addr = $user[0]->email_addr;
            $mbr_id = trim($user[0]->mbr_id);

            $session = $this->vcmemberRepository->getLoginRedis($mbr_id, $this->chappsDb);
//            print_r($session);
//            die();

            if (isset($session["deviceid"])) {
                $this->db20->statement("UPDATE o2o_member_devices SET logout = 1,modified_on=NOW() WHERE mbr_id = ? and deviceid = ?", [$mbr_id, $session["deviceid"]]);
            }
            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
            Redis::del($hkey);

            $loginkey = $this->server_name . $this->_login_token_prefix . $mbr_id; //. $key;
            Redis::del($loginkey);

            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
            $this->clearAuroraMemberdata($mbr_id);
        }

        return $this->apiSuccessResponse(1, "Finish.", $mbr_id);
    }

    private function clearAuroraMemberdata($mbr_id)
    {
        $del_query = <<<str
          DELETE FROM "Members_datatype" where mbr_id='$mbr_id'               
str;
        $this->chapps->statement($del_query);
    }

    public function vcTest()
    {
//        $inv_id = $this->vcmemberRepository->getNextInvoiceNo();
        $mbr_id = $this->vcmemberRepository->generateNewMbrId('AP');

        echo $mbr_id;
    }

    public function Login(Request $request)
    {

        $postdata['user'] = $this->filterInput($request->user);
        $postdata['password'] = trim($request->password);
        $postdata['redirect_to'] = trim($request->redirect_to);
        $postdata['redirect_back'] = trim($request->redirect_back);
        $postdata['from'] = trim($request->from);
        $postdata['platform'] = $request->platform ? $request->platform : "Desktop";
        $postdata['pushId'] = $request->pushId ? $request->pushId : '';
        $postdata['uuid'] = $request->uuid ? $request->uuid : 'Desktop';
        $postdata['player_id'] = $request->playerId ? $request->playerId : '';
        $postdata['meta'] = $request->meta ? $request->meta : '';
        $postdata['invalidUser'] = 0;
        $postdata['invalidPassword'] = 0;
        $postdata['duplicateUser'] = 0;
        $postdata['suspendedUser'] = 0;
        $postdata["hasOldDevice"] = 0;

        $postdata["is_mobile_digit_pwd"] = 0;

        if (($result = $this->vcmemberRepository->login($postdata))) {


            if (is_int($result) && $result < 0) {
                $msg = '';

                if ($result == -4) {
                    $msg = "Username Or Password is Empty.";
                } elseif ($result == -1) {
                    $msg = "This account is locked.";
                } elseif ($result == -2) {
                    $msg = "Your account has been locked due to security reasons, please email valueclub@challenger.sg for further assistance.";
                } elseif ($result == -3) {
                    $msg = "This account is not activated.";
                } elseif ($result == -6) {
                    $msg = "Your account has expired.";
                }
                $data = [
                    'code' => 'AUT200',
                    'message' => $msg,
                    'result' => null,
                ];
            } else {

                $hashedPassword2 = bin2hex(openssl_digest(trim($postdata['password']), 'sha256',true));
                $chckPwd2 = $this->memberRepository->chckUpdateMbrPassword2($postdata['user'], $hashedPassword2);    
                $code = 'AUT000';
                $msg = "Success Login";

                $data = [
                    'code' => $code,
                    'message' => $msg,
                    'result' => $result,
                ];
                //-- Update o2o_member_devices
                $uuid = $postdata['uuid'];
                $mbr_id = $result["mbr_id"];
                $platform = $postdata['platform'];
                $meta = $postdata['meta'];

                $sql = <<<str
DO $$
    begin
        IF not EXISTS(SELECT * FROM o2o_member_devices WHERE deviceid='$uuid')
            then
        INSERT INTO o2o_member_devices (mbr_id, platform, deviceid, meta, created_on) VALUES ('$mbr_id','$platform','$uuid','$meta', current_timestamp);
        ELSE
            UPDATE o2o_member_devices SET mbr_id = '$mbr_id', meta = '$meta',modified_on=current_timestamp,logout=NULL WHERE deviceid='$uuid' ; 
        end if;
    end
$$;
str;
                $this->db20->statement($sql, []);

            }
        } else {
            $data = [
                'code' => 0,
                'message' => "Either your email or password is incorrect.",
                'result' => $result,
            ];
        }

        $data["invalidUser"] = $postdata['invalidUser'];
        $data["invalidPassword"] = $postdata['invalidPassword'];
        $data["duplicateUser"] = $postdata['duplicateUser'];
        $data["suspendedUser"] = $postdata["suspendedUser"];
        $data["hasOldDevice"] = $postdata["hasOldDevice"];

        $data["is_mobile_digit_pwd"] = $postdata["is_mobile_digit_pwd"];
        if ($data["is_mobile_digit_pwd"]) {
            $this->_logprocess('MOBILE_LOGIN_PWD', 0, 'OK', $result["mbr_id"], 'VcLogin');
        }

        $isPushTime = $this->checkIfBlockSql();
        if ($isPushTime) {
            $this->getMemberShipData($mbr_id, '', true);
        }

        return response()->json($data);

    }

    public function logout(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $device_id = isset($request->device_id) ? $request->device_id : '';

//        print_r($session);
//        die();
        if ($device_id) {
            $this->db20->statement("UPDATE o2o_member_devices SET logout = '1',modified_on= current_timestamp WHERE mbr_id = ? and deviceid = ?", [$mbr_id, $device_id]);
        }

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        $loginkey = $this->server_name . $this->_login_token_prefix . $mbr_id; //. $key;
        Redis::del($loginkey);

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        return $this->apiSuccessResponse(1, "Session successfully logout", $loginkey);
    }

    private function _logprocess($process_id, $status, $remarks, $mbr_id, $created_by = 'VcApp')
    {
        $coy_id = $this->CTL_COY_ID;

        $after_setting = array(
            'coy_id' => $coy_id,
            "process_id" => substr($process_id, 0, 30),
            "status" => substr($status, 0, 3),
            "remarks" => $remarks,
            "created_by" => substr($created_by, 0, 15),
            "mbr_id" => substr($mbr_id, 0, 20),
            "created_on" => Carbon::now()
        );

        $this->db20->table('o2o_log_live_processes')
            ->insert($after_setting);
    }

    private function filterInput($string)
    {
        if (is_null($string)) {
            return null;
        }
        return $this->filterstring(stripslashes(trim($string)));
    }

    private function filterstring($string)
    {
        $output = $string;
        $output = str_replace("'", "", $output);
        $output = str_replace(";", "", $output);
        //$output = str_replace("*", "", $output);
        //$output = str_ireplace("insert", "", $output);
        //$output = str_ireplace("update", "", $output);
        //$output = str_ireplace("delete", "", $output);
        //$output = str_ireplace("select", "", $output);
        //$output = str_ireplace("wait", "", $output);
        //$output = str_replace("truncate", "", $output);
        $output = str_replace("--", "", $output);
        $output = str_replace("=", "", $output);

        return $output;
    }

    protected function _logNewprocess($process_id, $trans_id, $invoice_id, $status, $remarks, $mbr_id, $email_addr, $created_by = 'VcApp', $modified_on = null)
    {
        $coy_id = $this->CTL_COY_ID;

        $after_setting = array(
            'coy_id' => $coy_id,
            "process_id" => substr($process_id, 0, 30),
            "trans_id" => $trans_id,
            "invoice_id" => $invoice_id,
            "status" => substr($status, 0, 3),
            "remarks" => substr($remarks, 0, 200),
            "mbr_id" => substr($mbr_id, 0, 20),
            "created_by" => substr($created_by, 0, 15),
            "created_on" => Carbon::now(),
            "modified_on" => $modified_on,
            "email_addr" => $email_addr
        );

        $this->db20->table('o2o_log_live_processes_new')
            ->insert($after_setting);
    }

    public function getNotificationInbox(Request $request)
    {
        $mbr_id = $this->filterInput($request->mbr_id);

        $lists = $this->vcmemberRepository->getNotificationInbox($mbr_id);
//
//        print_r($lists);
//        die();

        $redisKey = $this->server_name . "#Vc2_Notification_redis#";
        $redisDatas = Redis::hgetall($redisKey);

//		print_r($redisDatas);
//		die();

        $timeNow = date("Y-m-d H:i:s A");
        foreach ($redisDatas as $index => $data) {
            $array = null;
            $array = array_filter($lists, function ($var) use ($index) {
                return $var["id"] == $index;
            });

            if (!$array) {
                $redis = json_decode($data, 1);
                $redis["mbr_id"] = $mbr_id;

                $datetime = date("Y-m-d H:i:s A", strtotime($redis["datetime"]));

                if ($timeNow >= $datetime) {
                    array_pop($lists);
                    array_unshift($lists, $redis);
                }
            }
        }

        return $this->apiSuccessResponse(1, $mbr_id, $lists);
    }

    public function myMembershipGet($mbr_id, $hachipass = '')
    {
        if ($hachipass !== $this->X_authorization) {
            return $this->apiSuccessResponse(0, "Access Denied", []);
        }
        $data = $this->getMemberShipData($mbr_id, '', true);

        return $data;
    }

    public function myMembership(Request $request)
    {
        $mbr_id = $request->mbr_id ? $request->mbr_id : 'PUBLIC';
        $uuid = $request->uuid;

        $data = $this->getMemberShipData($mbr_id, $uuid);

        return response()->json($data);
    }

    public function GetmyMembershipLists($mbr_id)
    {

        $data = $this->getMemberShipData($mbr_id, '');

        return response()->json($data);
    }

    public function getMemberShipData($mbr_id, $uuid, $isRefresh = false)
    {
        $isPushTime = $this->checkIfBlockSql();
        if (!$isRefresh && $this->chappsDb) {
            $twoAgoDays = Carbon::now()->addDays(-2)->format("Y-m-d");
//            print_r($twoAgoDays);
            $query = <<<str
    SELECT * FROM "Members" WHERE mbr_id= ? and cast(modified_on as date) > '$twoAgoDays'
str;
            $result = $this->chapps->select($query, [$mbr_id]);
            if ($result) {
                $data = json_decode($result[0]->meta, true);
                return $data;
            } else if ($isPushTime) {
//                //When Check If Block Sql.
                return array("mbr_id" => $mbr_id, "id" => $mbr_id);
            }
        }


        $user = $this->refreshSessionInfo($mbr_id, $uuid);
//        print_r($user);
//        die();

        $mbr_type = $user["mbr_type"];
        $mbr_addr = rtrim($user['mbr_addr']);
        $delv_addr = rtrim($user['delv_addr']);

        $eff_from = isset($user["eff_from"]) ? $user["eff_from"] : '';
        $eff_to = isset($user["eff_to"]) ? $user["eff_to"] : '';

        $primary_detail = [];
        $delivery_detail = [];
        $res_mbr_addr = [];
        $res_delv_addr = [];
        if ($mbr_addr !== '') {
            $mbr_addr_detail = $this->addressRepository->getAddress($mbr_id, $mbr_addr);
            $res_mbr_addr = [
                'street_line1' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line1) : '',
                'street_line2' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line2) : '',
                'street_line3' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line3) : '',
                'street_line4' => isset($mbr_addr_detail) ? rtrim($mbr_addr_detail->street_line4) : '',
                'country_id' => isset($mbr_addr_detail) ? $mbr_addr_detail->country_id : '',
                'postal_code' => isset($mbr_addr_detail) ? $mbr_addr_detail->postal_code : ''
            ];
            $primary_detail = $this->formatAddress_Singapore($res_mbr_addr);
        }
        if ($delv_addr !== '') {
            $delv_addr_detail = $this->addressRepository->getAddress($mbr_id, $delv_addr);
            $res_delv_addr = [
                'street_line1' => rtrim($delv_addr_detail->street_line1 ?? ''),
                'street_line2' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line2) : '',
                'street_line3' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line3) : '',
                'street_line4' => isset($delv_addr_detail) ? rtrim($delv_addr_detail->street_line4) : '',
                'country_id' => isset($delv_addr_detail) ? $delv_addr_detail->country_id : '',
                'postal_code' => isset($delv_addr_detail) ? $delv_addr_detail->postal_code : ''
            ];
            $delivery_detail = $this->formatAddress_Singapore($res_delv_addr);
        }

        $has_ass = $this->vcmemberRepository->has_associate($mbr_id);
        $associate_lists = [];
        if ($has_ass) {
            $associate_lists = $this->vcmemberRepository->getAssociateMbrList($mbr_id);
        }

        $associate_fullname = '';
        if ($user["main_id"]) {
            $main_member = $this->vcmemberRepository->getMainMember($user["main_id"]);
            $associate_fullname = isset($main_member) ? $main_member->full_name : '';
        }


        $isHideAssociate = false;
        $mbr_type_noAssociate = [$this->member_type_list['ASSOCIATE'], $this->member_type_list['NSMEN'], $this->member_type_list['STUDENT'],
            $this->member_type_list['STUDENT_PLUS'], $this->member_type_list['B2B'], 'ASC'];
        if (in_array(trim($mbr_type), $mbr_type_noAssociate) || substr($mbr_type, 0, 3) == 'MST') {
            $isHideAssociate = true;
        }

        $vpoints0 = $user['points_accumulated'] - $user['points_reserved'] - $user['points_expired'] - $user['points_redeemed'];
        $vpoints = number_format($vpoints0 / 100, 2);

        $accumulated = number_format($user['points_accumulated'] / 100, 2);

        $redeemed = number_format($user['points_redeemed'] / 100, 2);

        $name = $user['first_name'];
        $name = $name . ($name ? ' ' : '') . $user['last_name'];

        $sinceDate = date_format(date_create($user['join_date']), 'm/Y');
        $expiry = date_format(date_create($user['exp_date']), 'd/m/Y');

        $expiry_long = date_format(date_create($user['exp_date']), 'Y-m-d H:i:s');

        $since2 = date_format(date_create($user['join_date']), 'd/m/Y');
        $lastRenewal = date_format(date_create($user['last_renewal']), 'd/m/Y');
        $expiry2 = date_format(date_create($user['exp_date']), 'd/m/Y');
        $dob = date_format(date_create($user["birth_date"]), 'd/m/Y');

        $points_expiring = $this->vcmemberRepository->getPointExpiringLists($mbr_id);
        $mvip_progress = 0;

        //New One.
        if (substr($mbr_type, 0, 1) === 'S') {
            $rebate_eliga = "1% + 1%";
            $member_month_type = "24 months";
        } else if ($mbr_type === 'M08') {
            $member_month_type = "8 months";
            $rebate_eliga = "N/A";
        } else if ($mbr_type === 'M18') {
            $member_month_type = "18 months";
            $rebate_eliga = "0.5%";
        } else if ($mbr_type === 'M28') {
            $member_month_type = "28 months";
            $rebate_eliga = "1% + 1%";
        } else if ($mbr_type === 'MVIP') {
            $member_month_type = "MVIP";
            $rebate_eliga = "1% + 1%";

        } else if (substr($mbr_type, 0, 3) === 'MST') {
            $sinceDate = Carbon::parse($user['join_date']);
            $expDate = Carbon::parse($user['exp_date']);
            $diff = $expDate->diff($sinceDate);
            $yearLeft = $diff->y;
            $monthsLeft = $diff->m;
            $rebate_eliga = "N/A";

            if ($yearLeft == 2 && $monthsLeft >= 3) {
                $member_month_type = "28 months";
            } else if ($yearLeft == 1 || ($yearLeft == 0 && $monthsLeft == 11)) {
                $member_month_type = "12 months";
            } else if ($monthsLeft <= 3) {
                $member_month_type = "3 months";
            } else {
                $member_month_type = "24 months";
            }

            if ($mbr_type == "MSTP") {
                $rebate_eliga = "1% + 1%";
            }

        } else {
            $member_month_type = "24 months";
            $rebate_eliga = "1% + 1%";
        }


        if ($mbr_type == 'MVIP') {
            $sql_progress = <<<str
select  coalesce(cast(SUM(item_qty * unit_price) as int),0) as mvip_sum   from crm_member_transaction where mbr_id= ?
and cast(trans_date as date)>='$eff_from'
and cast(trans_date as date)<='$eff_to'
and trans_type not in ('RD','OD','OS','AP','TP','HX','HR')

str;
//            print_r($sql_progress);
            $progress = $this->db20->select($sql_progress, [$mbr_id]);
            $mvip_progress = count($progress) > 0 ? $progress[0]->mvip_sum : 0;
//            print_r($progress);
        }


        $unread_msgs = $this->getUnreadNotification($mbr_id);
        $vouchercount = $this->getVouchersCount($mbr_id);
        $couponcount = 0;

//        print_r($primary);

        $data = [
            'coy_id' => $user['coy_id'],
            'mbr_id' => $mbr_id,
            "id" => $mbr_id,
            "user_id" => $mbr_id,
            'vamount' => $vpoints,
            'vpoints' => $vpoints0,
            "accumulated" => "$" . $accumulated,
            "available" => "$" . $vpoints,
            'name' => $name,
            'mbr_title' => rtrim($user['mbr_title']),
            "title" => $user['mbr_title'],
            'member_month_type' => $member_month_type,
            'rebate_eligability' => $rebate_eliga,
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            "firstname" => $user['first_name'],
            "lastname" => $user['last_name'],
            'birth_date' => $user['birth_date'],
            'dob' => $dob,
            'nationality_id' => rtrim($user['nationality_id']),
            'email_addr' => $user['email_addr'],
            'email' => $user["email_addr"],
            'contact_num' => $user['contact_num'],
            'contact' => $user['contact_num'],
            'last_login' => $user['last_login'],
            'primary_details' => $res_mbr_addr,
            'delivery_details' => $res_delv_addr,
            'send_info' => rtrim($user['send_info']),
            'send_type' => rtrim($user['send_type']),
            'status_level' => $user['status_level'],
            'vouchercount' => $vouchercount,
            'couponcount' => $couponcount,
            "unread_msgs" => $unread_msgs,
            'since' => $sinceDate,
            'expiry' => $expiry,
            "since2" => $since2,
            "expiry2" => $expiry2,
            "expiry_long" => $expiry_long,
            'join_date' => $since2,
            'exp_date' => $user['exp_date'],
            'last_renewal' => $lastRenewal,
            'points_accumulated' => $user['points_accumulated'],
            'points_reserved' => $user['points_reserved'],
            'points_redeemed' => $user['points_redeemed'],
            'redeem_ctl' => $redeemed,
            'points_expired' => $user['points_expired'],
            "points_expiring" => $points_expiring,
            'mbr_savings' => $user['mbr_savings'],
            "member_savings" => "$" . number_format($user['mbr_savings'], 2),
            'rebate_voucher' => rtrim($user['rebate_voucher']),
            'mbr_type' => $mbr_type,
            'main_id' => rtrim($user['main_id']),
            'has_associate' => $has_ass,
            'is_hideassociate' => $isHideAssociate,
            'associate_members' => $associate_lists,
            "associate_fullname" => $associate_fullname,
            'sub_ind1' => $user['sub_ind1'],
            'sub_ind2' => $user['sub_ind2'],
            'sub_date' => $user['sub_date'],
            "primary_addr" => $primary_detail,
            "delivery_addr" => $delivery_detail,
            'salesperson_id' => rtrim($user['salesperson_id']),
            'created_by' => rtrim($user['created_by']),
            'created_on' => $user['created_on'],
            'modified_by' => rtrim($user['modified_by']),
            'modified_on' => $user['modified_on'],
            'updated_on' => $user['updated_on'],
            'login_locked' => $user['login_locked'],
            "popup_message" => $this->messages["KEEP_LOGIN_SECURE"]["message"],
            'eff_from' => $eff_from,
            'eff_to' => $eff_to,
            "mvip_progress" => $mvip_progress
        ];
        ksort($data);

        $this->refreshAuroraMemberData($data, $mbr_id);

        return $data;
    }

    public function refreshAuroraMemberData($data, $mbr_id)
    {

        $item = array(
            "mbr_id" => $mbr_id,
            "meta" => json_encode($data)
        );

        $query = <<<str
        INSERT INTO "Members" (mbr_id, meta, modified_on)
        VALUES (?, ? , NOW())
        ON CONFLICT (mbr_id) DO UPDATE
          SET meta = ?, modified_on= NOW()
str;
        $this->chapps->statement($query, [$mbr_id, $item["meta"], $item["meta"]]);

        return true;
    }

    private function getVouchersCount($mbr_id)
    {

        $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
        $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');


        $coy_id = $this->CTL_COY_ID;

        $page_from = 0;
        $page_to = 10;

        $vouchers = $this->vcmemberRepository->getMyVoucher($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to);

        return count($vouchers);
    }

    private function getUnreadNotification($mbr_id)
    {
        $lists = $this->vcmemberRepository->getNotificationInbox($mbr_id);
//        print_r($lists);
        $unread = 0;
        if (count($lists) > 0) {
            foreach ($lists as $item) {
                $list = $this->vcmemberRepository->stdToArray($item);

                if ($list["status"] === 0) {
                    $unread++;
                }
            }
        }
        return $unread;
    }

    public function refreshSessionInfo($mbr_id, $uuid)
    {
        $member_type = $this->db20->table('crm_member_type')
            ->select('mbr_type', DB::raw('cast(eff_from as date) as eff_from'), DB::raw('cast(eff_to as date) as eff_to'))
            ->where('mbr_id', $mbr_id)
            ->whereRaw("eff_from <= current_timestamp and eff_to >= current_timestamp")
            ->orderBy('line_num', 'desc')
            ->first();

        $member_list = $this->db20->table('crm_member_list')
            ->select('*')
            ->where('mbr_id', $mbr_id)
            ->first();

        $member = json_decode(json_encode($member_list), true);

        if ($member_type) {
            $member["mbr_type"] = $member_type->mbr_type;
            $member["eff_from"] = $member_type->eff_from;
            $member["eff_to"] = $member_type->eff_to;
        }

//        print_r($member);
        return $member;
    }

    private function formatAddress_Singapore($result)
    {
        $address = trim($result["street_line1"]) . (empty(trim($result["street_line1"])) ? '' : ' ');
        $address = $address . (empty(trim($result["street_line2"])) ? '' : '#') . trim($result["street_line2"]);
        $address = $address . (empty(trim($result["street_line3"])) ? '' : '-') . trim($result["street_line3"]) . (empty(trim($result["street_line3"])) ? '' : ' ');
        $address = $address . trim($result["street_line4"]) . (empty(trim($result["street_line4"])) ? '' : ' ');
        $address = trim($address) . (empty(trim($result["country_id"])) ? '' : ', ') . trim($result["country_id"]) . (empty(trim($result["country_id"])) ? '' : ' ');
        $address = $address . trim($result["postal_code"]);
        return $address;
    }

    public function verifyUpdateDetail(Request $request)
    {
        return $this->apiSuccessResponse(0, "Failed.", []);
        die();

//        $mbr_id = $request->mbr_id;
//
//        $header_token = '25b5d3a2d556f6fca899c22901430aed';
//        $headers = array(
//            'X_AUTHORIZATION: ' . $header_token,
//            'Content-Type: application/json'
//        );
//
//        if ($this->ENVIRONMENT == "development") {
//            $url = 'https://chvoices-test.challenger.sg/api/vcmember/verify/' . $mbr_id;
//        } else {
//            $url = 'https://chvoices.challenger.sg/api/vcmember/verify/' . $mbr_id;
//        }
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_HTTPGET, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        $result = curl_exec($ch);
//        curl_close($ch);
//
//        if ($result) {
//            $response = json_decode($result, true);
//            if ($response["code"] == "0") {
//                return $this->apiSuccessResponse(1, "OK", $response);
//            } else {
//                return $this->apiSuccessResponse(0, "Account Already Updated.", $response);
//            }
//        }
//        return $this->apiSuccessResponse(0, "Failed.", []);
    }

    public function checkLoginSession(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $token = $request->token;
        $isTime = $this->checkIfBlockSql();
        if ($isTime) {
            return $this->apiSuccessResponse(1, "No Check", $token);
        }

        $session_token = $this->vcmemberRepository->getSessionToken($mbr_id);
        if (!$session_token) {
            return $this->apiSuccessResponse(-1, "Expired", []);
        }
        if ($session_token == $token) {
            return $this->apiSuccessResponse(1, "Pair", $session_token);
        } else {
            if (!$isTime) {
                return $this->apiSuccessResponse(0, '<p>We have detected an active session on another device.</p><p>For security reasons, we will terminate this session.</p>', $session_token);
            } else {
                return $this->apiSuccessResponse(1, "No Check", $session_token);
            }
        }
    }

    private function checkIfBlockSql()
    {
        $currentTime = date("Y-m-d H:i:s");
        $checkRedisKey = $this->ENVIRONMENT . "#Vc_pushNotify_time#";
        $checkPushTimes = Redis::hgetall($checkRedisKey);
//		print_r($checkPushTimes);
        $isTime = false;
        foreach ($checkPushTimes as $thetime) {
            $onehourLater = date("Y-m-d H:i:s", strtotime('+5 minutes', strtotime($thetime)));
            if ($currentTime > $thetime && $currentTime <= $onehourLater) {
                $isTime = true;
                break;
            }
        }
//        echo "res". $isTime;
        return $isTime;
    }

    public function CheckSess($mbr_id)
    {
        $isTime = $this->checkIfBlockSql();
        if ($isTime) {
            return $this->apiSuccessResponse(1, "Skipped. Valid Session", []);
        }

        $sessions = $this->vcmemberRepository->getLoginRedis($mbr_id, $this->chappsDb);
//        print_r($sessions);

        if ($sessions) {
            $email = $sessions['email_addr'];
            $mbr_type = $sessions["mbr_type"];
            if (trim($mbr_type) == "X") {
                return $this->apiSuccessResponse(0, "Suspended User", []);
            }


            if ($email == "" && !$isTime) {
                $query = <<<str
    DELETE FROM "Members" WHERE mbr_id= ? 
str;
                $this->chapps->statement($query, [$mbr_id]);
                return $this->apiSuccessResponse(0, "Session Expires", []);
            }

            $key = $this->tokenKeys['NOTIFY_FORGOT'] . $email;
            $data = array(
                "notify_forgot" => 0,
                "extra_data" => [],
                "email" => $email
            );
            $notifyFpass = $this->getRedisByKey($key);
            if ($notifyFpass) {
                $data["notify_forgot"] = 1;
                $data["extra_data"] = $notifyFpass;
                $data["email"] = $email;
            }

            //-- check if any push data
            $device_id = isset($sessions['deviceid']) ? $sessions['deviceid'] : '';
            if ($device_id) {
                $key = $this->server_name . trim($device_id) . $this->tokenKeys['NOPUSH_POSGETTOKEN'];
                $extraData = Redis::get($key);
                $edata = json_decode($extraData, TRUE);
                if ($extraData && !$edata['notified']) {
                    $data['pushdata'] = $extraData;
                    //-- marked redis that has already notified
                    $edata['notified'] = 1;
                    $edata = json_encode($edata);
                    Redis::set($key, $edata);
                    Redis::expire($key, $this->posTokenExpire);
                    //-- eof marked redis that has already notified
                }
                //-- eof check if any push data
                //-- check if any batch notify push data
                $key = $this->server_name . trim($device_id) . $this->tokenKeys['NOPUSH_BATCHNOTIFY'];
                $extraData = Redis::get($key);
                $edata = json_decode($extraData, TRUE);
                if ($extraData && !$edata['notified']) {
                    $data['notifypush'] = $extraData;
                    $edata['notified'] = 1;
                    $edata = json_encode($edata);
                    Redis::set($key, $edata);
                    Redis::expire($key, $this->posTokenExpire);
                }
            }
            //-- eof check if any batch notify push data

            if ($sessions) {
                return $this->apiSuccessResponse(1, "Valid Session", $data);
            } else {
                return $this->apiSuccessResponse(0, "Session Expires", $data);
            }
        } else {
            $hournow = date("H");
            $minnow = date("m");
            if (($hournow == 0 && $minnow >= 30) || $hournow == 1) {
                return $this->apiSuccessResponse(1, "Valid Session", $data = array(
                    "notify_forgot" => 0,
                    "extra_data" => [],
                    "email" => ''
                ));
            } else {
                return $this->apiSuccessResponse(0, "Session Expires", []);
            }
        }
    }

    private function getRedisByKey($key)
    {
        $key = $this->server_name . $key;
        return Redis::get($key);
    }

    public function getproduct(Request $request)
    {
        $item_id = $request->item_id;

        $environment = $this->ENVIRONMENT;
//        if($environment=='development') {

        $theurl = "https://www.challenger.sg/product/$item_id/details";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
        ));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $theurl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_hachi1 = json_decode($response, true);
        $response_hachi = $response_hachi1["data"];

//        print_r($response_hachi);
//        die();

        $descurl = "https://www.challenger.sg/product/$item_id/descriptions";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
        ));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $descurl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($ch);

        curl_close($ch);
        $response_pos81 = json_decode($response, true);
        $response_pos8 = $response_pos81["data"];

//        print_r($response_pos8);
//        die();

        //item_id
        $data = $response_hachi;

        $data["p3detail"] = $response_pos8;
        if (!isset($data["p3detail"]["OVERVIEW"]["body"])) {
            $data["p3detail"]["OVERVIEW"] = array(
                "body" => ""
            );
        }
        if (!isset($data["p3detail"]["SPECIFICATION"]["body"])) {
            $data["p3detail"]["SPECIFICATION"] = array(
                "body" => ""
            );
        }
        if (!isset($data["p3detail"]["VIDEO"]["body"])) {
            $data["p3detail"]["VIDEO"] = array(
                "body" => ""
            );
        }
        $data["prices"]["rebate_description"] = 'U.P. S$' . $response_hachi["prices"]["regular_price"];

//        }else {
//
//            $theurl = "https://2.hachi.tech/api/ProductDetails/" . $item_id . "?returns=PROMOPERIOD,PWP,FLB,LOCBASE,STOCKLEVEL,CHECKOUT,BOM,HAT,";
//
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
//            ));
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//            curl_setopt($ch, CURLOPT_URL, $theurl);
//
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//            $response = curl_exec($ch);
//            curl_close($ch);
//            $response_hachi = json_decode($response, true);
//
////        print_r($response_hachi);
////        die();
//
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
//            ));
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//            curl_setopt($ch, CURLOPT_URL, 'https://pos8.api.valueclub.asia/products/' . trim($item_id) . '?customer_group=MEMBER');
//
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//            $response = curl_exec($ch);
//            curl_close($ch);
//            $response_pos8 = json_decode($response, true);
//
//
//            //item_id
//            $data = array();
//            $data["item_id"] = $response_pos8['item_id'];
//            //item_type
//            $data['item_type'] = $response_pos8['item_type'];
//            //item_desc
//            $data['item_desc'] = $response_pos8['item_desc'];
//            //prices
//            $data['prices'] = $response_pos8['prices'];
//
//            $data["p3images"] = isset($response_hachi["p3images"]) ? $response_hachi["p3images"] : $response_hachi["p3images"];
//            $data["p3colors"] = $response_hachi["p3colors"];
//
//            $data["p3info"] = $response_hachi["p3info"];
//
//            $data["p3info"]["price_mbr"] = $response_pos8["prices"]["unit_price"];
//            $data["p3info"]["rebate_description"] = 'U.P. S$' . $response_pos8["prices"]["regular_price"];
//        }

        return $this->apiSuccessResponse(1, "Success", $data);
    }

    public function getproductstock(Request $request)
    {
        $item_id = $request->item_id;

        $postdata = array(
            "item_id" => $item_id
        );

        $url = $this->imsUrl . "vcapp/getproductstock";
        $stores = $this->curlImsApi($url, $postdata);
//        print_r($stores);
//        die();

        $theurl = "https://2.hachi.tech/api/ProductDetails/" . $item_id . "?returns=STOCKLEVEL";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
        ));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $theurl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($ch);
        curl_close($ch);
        $stocks = json_decode($response, true);

//        print_r($stocks);

        foreach ($stocks['stocklevel'] as $key => $value) {
            for ($i = 0; $i < count($stores); $i++) {
                if ($key == $stores[$i]['cname']) {
                    $stocks['stocklevel'][$key]['title'] = $stores[$i]['title'];
                    $stocks['stocklevel'][$key]['opening_hours'] = $stores[$i]['opening_hours'];
                    $stocks['stocklevel'][$key]['opening_hours2'] = $stores[$i]['opening_hours2'];
                    break;
                }
            }

            if ($stocks['stocklevel'][$key]['stock_level'] === 1 || $stocks['stocklevel'][$key]['stock_level'] === 2) {
                $stocks['stocklevel'][$key]['inv_desc'] = "Limited availability";
                $stocks['stocklevel'][$key]['color'] = "#FFA500";
            } else {
                $stocks['stocklevel'][$key]['inv_desc'] = "Available";
                $stocks['stocklevel'][$key]['color'] = "#3ca400";
            }
        }


        return $this->apiSuccessResponse(1, "Success", $stocks['stocklevel']);
    }


    public function getTnC()
    {
        $lists = $this->vcmemberRepository->getTnc();
        if (count($lists) === 0) {
            return $this->apiSuccessResponse(0, "no tnc", []);
        } else {
            return $this->apiSuccessResponse(1, "Success", $lists);
        }
    }


    public function getBrandLists()
    {

        $redisKeyLists = $this->ENVIRONMENT . "_serviceCenterLists#";
        $lists = Redis::get($redisKeyLists);
//        print_r($lists);
//        die();

        if (!$lists || $lists == '[]') {
            $redisKey = $this->ENVIRONMENT . "_serviceCenter#";
            $lists = Redis::hgetall($redisKey);
            $brand_lists = array();
            $count = 1;
            foreach ($lists as $brand => $list) {
                $character = substr($brand, 0, 1);
                if (is_numeric($character)) {
                    $is_numeric = 1;
                    $character = '#';
                } else {
                    $is_numeric = 0;
                }

                $data = array(
                    "rownum" => $count,
                    "id" => $brand,
                    "name" => $brand,
                    "is_top_menu" => '',
                    "svc_ctr_id" => '',
                    "is_numeric" => $is_numeric,
                    "character" => $character
                );
                $brand_lists[] = $data;
                $count++;
            }

            usort($brand_lists, function ($a, $b) {
                return strcmp($a["name"], $b["name"]);
            });
            Redis::set($redisKeyLists, json_encode($brand_lists));
            Redis::expire($redisKeyLists, $this->oneDays * 5);
//			echo "here";
        } else {
            $brand_lists = json_decode($lists, true);
        }

        return $this->apiSuccessResponse(1, "OK", $brand_lists);
    }

    public function getBrandServiceCentreLists(Request $request)
    {
        $brand_id = $request->brand_id;

        $lists = $this->vcmemberRepository->getBrandServiceCentres($brand_id);

        return $this->apiSuccessResponse(1, "OK", $lists);
    }

    public function FPassEmail($email)
    {
        if ($email) {
            $postdata['email'] = $email;
        }

        $postdata["mobile"] = "";
        $postdata["platform"] = "";
        $postdata["pushId"] = "";

        //-- Validation
        if (!trim($postdata['email'])) {
            return $this->apiSuccessResponse(0, "No username / email address has been specified", []);
        }
        if (!trim($postdata['mobile'])) {
            $postdata['mobile'] = "";
        }
        $data = $this->vcmemberRepository->isEmailValid($postdata['email']);
        if ($data["valid"] === false) {
            return $this->apiSuccessResponse(0, "Email does not exist or has already been expired", $this->messages["EMAIL_NOT_EXIST"]);
        } else if ($data["valid"] === -1) {
            return $this->apiSuccessResponse(0, "Account 's not activated", $this->messages["ASSO_ACCOUNT_NOT_ACTIVATE"]);
        }

        $keyToken = bin2hex(openssl_random_pseudo_bytes(20));
        Redis::set($keyToken, $email);  //20 seconds
        Redis::expire($keyToken, 172800);

//        echo $keyToken;

        $mbr_id = trim($data['data']->mbr_id);
        $mbr_token = trim($data['data']->member_token);
        $dataToken = array(
            "email" => $postdata['email'],
            "platform" => $postdata['platform'],
            "regid" => $postdata['pushId']
        );

        $token = $this->createToken($dataToken, 172800);
        $edata = array(
            "token" => $token,
            "email" => trim($postdata['email']),
            "mbr_id" => $mbr_id
        );
        $edata = base64_encode(json_encode($edata));

        $email_address = trim($postdata['email']);
        $email_data = array(
            "r_ctl_url" => '',
            "r_c_first_name" => trim($data['data']->first_name),
            "r_c_mbr_token" => $mbr_token,
            "r_ctl" => "",
            "r_c_appresetpw" => "",
            "r_c_resetpw" => ""
        );

        $email_data["c_resetpw"] = $keyToken;
        $ctl_webpage = ($this->ENVIRONMENT == "development") ? 'dev.ctl.sghachi.com/' : 'www.challenger.com.sg/';
//        'https://' .
        $paramsUrl = ["longUrl" => $ctl_webpage . 'login/reset_pwd1?code=' . $keyToken];
//        $curlResponseUrl = $this->shortenUrlCurl($paramsUrl);
//        $shortUrl = $curlResponseUrl;

        $email_data["r_ctl_url"] = $paramsUrl["longUrl"];
        $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["WEB_FORGOT_PASSWORD"], $email_address, $email_data);

        //-- eof Sent Email for forgot password version 2
        $status = $email_result['status_code'] === 200 ? 'Sent' : 'Fail';

        $result = array();
        if ($status === 'Sent') {
            $result["new_code"] = $keyToken;
            $result["url"] = $paramsUrl["longUrl"];
            $result["msg"] = $this->messages["EMAIL_SENT"]["message"];

            return $this->apiSuccessResponse(0, "An email has been sent to activate:", $result);
        } else {
            return $this->apiSuccessResponse(0, "Mail server busy:", $result);
        }

    }

    public function FPassGet($email0, $redirectLink)
    {
        if ($email0) {
            $postdata['email'] = $email0;
        }

        $postdata["mobile"] = "";
        $postdata["platform"] = "";
        $postdata["pushId"] = "";

        //-- Validation
        if (!trim($postdata['email'])) {
            return $this->apiSuccessResponse(0, "No username / email address has been specified", []);
        }
        if (!trim($postdata['mobile'])) {
            $postdata['mobile'] = "";
        }
        $data = $this->vcmemberRepository->isEmailValid($postdata['email']);
        if ($data["valid"] === false) {
            return $this->apiSuccessResponse(0, "Email does not exist or has already been expired", $this->messages["EMAIL_NOT_EXIST"]);
        } else if ($data["valid"] === -1) {
            return $this->apiSuccessResponse(0, "Account 's not activated", $this->messages["ASSO_ACCOUNT_NOT_ACTIVATE"]);
        }

        $mbr_id = trim($data['data']->mbr_id);
        $mbr_token = trim($data['data']->member_token);
        $dataToken = array(
            "email" => $postdata['email'],
            "platform" => $postdata['platform'],
            "regid" => $postdata['pushId']
        );

        $token = $this->createToken($dataToken, 172800);
        $edata = array(
            "token" => $token,
            "email" => trim($postdata['email']),
            "mbr_id" => $mbr_id
        );
        $edata = base64_encode(json_encode($edata));

        $email_address = trim($postdata['email']);
        $email_data = array(
            "r_ctl_url" => '',
            "r_c_first_name" => trim($data['data']->first_name),
            "r_c_mbr_token" => $mbr_token,
            "r_ctl" => "",
            "r_c_appresetpw" => "",
            "r_c_resetpw" => ""
        );

        $email_data["c_resetpw"] = $redirectLink;
        $ctl_webpage = ($this->ENVIRONMENT == "development") ? 'dev.ctl.sghachi.com/' : 'www.challenger.com.sg/';
//        'https://' .
        $paramsUrl = ["longUrl" => $ctl_webpage . 'login/reset_pwd1?code=' . $redirectLink];
//        $curlResponseUrl = $this->shortenUrlCurl($paramsUrl);
//        $shortUrl = $curlResponseUrl;

        $email_data["r_ctl_url"] = $paramsUrl["longUrl"];
        $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["WEB_FORGOT_PASSWORD"], $email_address, $email_data);

        //-- eof Sent Email for forgot password version 2
        $status = $email_result['status_code'] === 200 ? 'Sent' : 'Fail';

        if ($status === 'Sent') {
            return $this->apiSuccessResponse(0, "An email has been sent to activate:", $this->messages["EMAIL_SENT"]);
        } else {
            return $this->apiSuccessResponse(0, "Mail server busy:", "Mail server busy, please try again");
        }
    }

    public function FPass(Request $request)
    {

        $postdata = array();
        $postdata['email'] = trim($request->email);
        $postdata['password'] = trim($request->password);
        $postdata['mobile'] = trim($request->mobile);
        $postdata['platform'] = trim($request->platform);
        $postdata['pushId'] = trim($request->pushId);

        //-- Validation
        if (!trim($postdata['email'])) {
            return $this->apiSuccessResponse(0, "No username / email address has been specified", []);
        }
        if (!trim($postdata['mobile'])) {
            $postdata['mobile'] = "";
        }
        $data = $this->vcmemberRepository->isEmailValid($postdata['email']);
        if ($data["valid"] === false) {
            return $this->apiSuccessResponse(0, "Email does not exist or has already been expired", $this->messages["EMAIL_NOT_EXIST"]);
        } else if ($data["valid"] === -1) {
            return $this->apiSuccessResponse(0, "Account 's not activated", $this->messages["ASSO_ACCOUNT_NOT_ACTIVATE"]);
        }

        $mbr_id = trim($data['data']->mbr_id);
        $mbr_token = trim($data['data']->member_token);
        $dataToken = array(
            "email" => $postdata['email'],
            "platform" => $postdata['platform'],
            "regid" => $postdata['pushId']
        );

        $token = $this->createToken($dataToken, 172800);
        $edata = array(
            "token" => $token,
            "email" => trim($postdata['email']),
            "mbr_id" => $mbr_id
        );
        $edata = base64_encode(json_encode($edata));

        $email_address = $postdata['email'];
        $email_data = [
            "r_ctl_url" => '',
            "r_c_first_name" => trim($data['data']->first_name),
            "r_c_mbr_token" => $mbr_token,
            "r_ctl" => "",
            "r_c_appresetpw" => "",
            "r_c_resetpw" => ""
        ];

        if ($this->ENVIRONMENT == 'development') {
            $theUrl = $this->redirectUrl;
        } else {
            $theUrl = $this->redirectUrl;
        }

        $params = ["longUrl" => $theUrl . '/api/vc2/resetpw/' . $edata];
//        $curlResponseUrl = $this->shortenUrlCurl($params);
//        $shortUrl = $curlResponseUrl;

//        $email_data["r_ctl_url"] = $shortUrl;
        $email_data["r_ctl_url"] = $params["longUrl"];
        $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["VC_FORGETPASSWORD"], $email_address, $email_data);

        $status = $email_result['status_code'] === 200 ? 'Sent' : 'Fail';

        if ($status === 'Sent') {
            return $this->apiSuccessResponse(0, "An email has been sent to activate:", $this->messages["EMAIL_SENT"]);
        } else {
            return $this->apiSuccessResponse(0, "Mail server busy:", "Mail server busy, please try again");
        }
    }


    private function createToken($data = array(), $expire_seconds = 180, $tokenKey = "vctoken")
    {
        if ($tokenKey == "vctoken") {
            $token = strtoupper(uniqid());
            $key = $this->server_name . "$tokenKey#" . $token;
        } else {
            $key = $this->server_name . $tokenKey;
        }

        $data = json_encode($data);
        Redis::set($key, $data);
        Redis::expire($key, $expire_seconds); // 3 Minutes
        return $key;
    }


    public function resetpw($edata)
    {
        $data = json_decode(base64_decode($edata), true);
//        print_r($data);
//        die();
        $token = $data['token'];
//        $key = $this->server_name . "vctoken#" . $token;
        $data = Redis::get($token);

        if (!$data) {
            echo "<h1>Error</h1>";
            echo "<p>Token already expired.</p>";
            die();
        }
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/reset/$edata");
        } else {
            return redirect("https://www.challenger.com.sg/");
        }
    }


    public function validateRenewAndUpgrade(Request $request)
    {
        $mbr_id = $request->nric;

        $result = $this->vcmemberRepository->validateRenewAndUpgrade($mbr_id);

        return $this->apiSuccessResponse($result["code"], $result["msg"], $result);
    }

    public function ChPass(Request $request)
    {
        $mbr_id = trim(strtoupper($request->mbr_id));
        $email_addr = isset($request->email) ? trim($request->email) : '';

        $cur_pass = isset($request->cur_pass) ? $request->cur_pass : '';
        $new_pass = $request->new_pass;
        $token = isset($request->token) ? $request->token : '';

        //Fix when Email Addr Uppercase problem.
        $email_addr = strtolower($email_addr);

        if (!$email_addr && !$mbr_id && !$token) {
            return $this->apiSuccessResponse(0, "User Not Found.", []);
        }

        if ($token) {
            if (!$email_addr) {
                $email_addr = Redis::get($token);
                $email_addr = strtolower($email_addr);
            }
            if (!$email_addr) {
                return $this->apiSuccessResponse(0, "Email not found/ Token has expired.", []);
            }
            $this->vcmemberRepository->updateNewPassCtl($email_addr, $new_pass);

            //Remove CTL Token.
            Redis::del($token);

            return $this->apiSuccessResponse(1, 'Successful Updated.', []);
        } else {

            if ($cur_pass) {
                $validatePass = $this->vcmemberRepository->verifyCurPass($email_addr, $cur_pass);
                if (!$validatePass) {
                    return $this->apiSuccessResponse(0, "Current password entered is incorrect", []);
                }
            }

            $this->vcmemberRepository->updateNewPass($email_addr, $mbr_id, $new_pass);

            return $this->apiSuccessResponse(1, 'Successful.', []);
        }
    }

    public function Version(Request $request)
    {
        $result["force"] = false;
        $pwa = $request->pwa ? trim($request->pwa) : "";
        $current = $request->current ? trim($request->current) : "";


        if (!in_array($current, $this->appVersion)) {
            $result["data"] = $this->messages["OLDER_VERSION_APP"]["title"];
            return $this->apiSuccessResponse(0, $this->messages["OLDER_VERSION_APP"]["title"], $result);
        }

        $result["data"] = "Latest Version";

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function searchPostal(Request $request)
    {
        $postalCode = trim($request->postal_code);
        $preFix = substr($postalCode, 0, 3);
        $exist = false;
        $row_data = [];

        if ($this->isPostalNoZone($postalCode)) {
            $return = array(
                "code" => 0,
                "message" => "Postal Code Does Not Exist.",
                'result' => [],
            );
            return response()->json($return);
        }

        $redisKey = $this->ENVIRONMENT . "_postalCode_" . $preFix;
        $lists = Redis::hmget($redisKey, $postalCode)[0];

        if ($lists) {
            $address = json_decode($lists);
            $row1 = $address[1] && $address[2] ? $address[1] . " " . $address[2] : ($address[2] ? $address[2] : $address[1]);
            $row2 = $address[3] ? $address[3] : " ";
            $row_data = array(
                "addr_1" => $row1,
                "addr_2" => $row2,
                "street_name" => $address[2] . " " . $address[3],
                "block_no" => $address[1]
            );
            $exist = true;
        }

        $data = [
            'code' => ($exist ? 1 : 0),
            'message' => ($exist ? "Exist" : "Postal Code Does Not Exist."),
            'result' => $row_data,
        ];
        return response()->json($data);
    }

    private function isPostalNoZone($postal = null)
    {
        $found = false;
        $redisKey = $this->ENVIRONMENT . "_postalNoGoZone#";
        $lists = Redis::hmget($redisKey, $postal)[0];
        if ($lists) {
            $found = true;
        }
        return $found;
    }

    public function updateMemberSubInd(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $sub_ind1 = $request->sub_ind1;
        $sub_ind2 = $request->sub_ind2;

        $res = $this->vcmemberRepository->updateMemberSubStatus($mbr_id, $sub_ind1, $sub_ind2);

        return $this->apiSuccessResponse(1, "Success", $res);
    }

    public function updateMemberInfoCtl(Request $request)
    {
        $mbrId = trim($request->mbr_id);
        $input = array();

        $email = $request->email;
        $firstname = $request->firstname;

        $input["email"] = $email;
        $input["contact"] = $request->contact;
        $input["firstname"] = $request->firstname;
        $input["lastname"] = $request->lastname;
        $input["title"] = $request->title;
        $input['primary_details'] = $request->primary_details;
        $input['delivery_details'] = $request->delivery_details;


        $user = $this->memberRepository->update([
            'contact_num' => $input['contact'],
            'email_addr' => $input['email'],
            'mbr_title' => isset($input['title']) ? $input['title'] : '',
            'first_name' => $input['firstname'],
            'last_name' => isset($input['lastname']) ? $input['lastname'] : ''
        ], $mbrId, $attribute = 'mbr_id');
        if ($user) {
            $this->addressRepository->updateMemberInfo($mbrId, $input);

            //Clear MyMemberShip Redis
            $this->clearRedisFields($mbrId, '#MBRINFO#', $this->AUTOLOGIN_TOKEN, false);
            $this->updateOrInsertMbr($mbrId, $this->_accessPass);

            $email_data = [
                'r_fname' => ucwords(strtolower($firstname)),
            ];
            $this->_sendEmarsysEmail($this->emarsys_template_list["UPDATE_PROFILE"], $email, $email_data);

            $pd_addr_text = $this->formatAddress_Singapore($input['primary_details']);
            $dd_addr_text = $this->formatAddress_Singapore($input['delivery_details']);

            return $this->apiSuccessResponse(1, "Member Info, successfully updated",
                array("primary" => $pd_addr_text, "delivery" => $dd_addr_text));

        } else {
            return $this->errorWithCodeAndInfo(100501, 'Server problem, cannot save member info');
        }
    }


    public function updateMemberInfo(Request $request)
    {
        $mbrId = trim($request->mbr_id);
        $email = $request->email;
        $firstname = $request->firstname;

        $input = $request->only(['contact', 'email', 'title', 'firstname', 'lastname', 'primary_details', 'delivery_details']);

        $rules = [
            'contact' => 'required|numeric',
            'email' => 'required|email',
            'firstname' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(500, $errors);
        }
        if (strlen($input['contact']) < 8) {
            return $this->errorWithCodeAndInfo(500, 'Contact number at least 8 digital.');
        }
        if ($this->memberRepository->checkEmailUnique($mbrId, $input['email'])) {
            return $this->errorWithCodeAndInfo(500, 'Email address must unique.');
        }
        if ($this->memberRepository->checkPhoneUnique($mbrId, $input['contact'])) {
            return $this->errorWithCodeAndInfo(500, 'Phone number must unique.');
        }
        $user = $this->memberRepository->update([
            'contact_num' => $input['contact'],
            'email_addr' => $input['email'],
            'mbr_title' => isset($input['title']) ? $input['title'] : '',
            'first_name' => $input['firstname'],
            'last_name' => isset($input['lastname']) ? $input['lastname'] : ''
        ], $mbrId, $attribute = 'mbr_id');
        if ($user) {
            $this->addressRepository->updateMemberInfo($mbrId, $input);

            //Clear MyMemberShip Redis
            $this->clearRedisFields($mbrId, '#MBRINFO#', $this->AUTOLOGIN_TOKEN, false);
            $this->updateOrInsertMbr($mbrId, $this->_accessPass);


            $email_data = [
                'r_fname' => ucwords(strtolower($firstname)),
            ];
            $this->_sendEmarsysEmail($this->emarsys_template_list["UPDATE_PROFILE"], $email, $email_data);

            $pd_addr_text = $this->formatAddress_Singapore($input['primary_details']);
            $dd_addr_text = $this->formatAddress_Singapore($input['delivery_details']);

            return $this->apiSuccessResponse(1, "Member Info, successfully updated",
                array("primary" => $pd_addr_text, "delivery" => $dd_addr_text));

        } else {
            return $this->errorWithCodeAndInfo(100501, 'Server problem, cannot save member info');
        }
    }

    public function deletePushItem(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $pushid = trim($request->push_id);
        $deviceid = $request->device_id ? $request->device_id : 'DELETE';
        $platform = $request->platform ? trim($request->platform) : 'DELETE';

        $lists = $this->vcmemberRepository->deletePushItem($mbr_id, $pushid, $deviceid, $platform);
        return $this->apiSuccessResponse(1, "Success", $lists);
    }

    public function deleteAllNotifications(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $deviceid = $request->device_id ? $request->device_id : 'DELETE';
        $platform = $request->platform ? $request->platform : 'DELETE';

        $isTime = $this->checkIfBlockSql();
        if ($isTime) {
            return $this->apiSuccessResponse(0, "Unable to do the process, please try again 15 minutes later.", []);
        }

        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Member ID not found.", []);
        }

        $this->vcmemberRepository->deleteAllNotifications($mbr_id, $deviceid, $platform);

        return $this->apiSuccessResponse(1, "Success", []);
    }

    public function updateLatestPush(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $latest_id = $request->latest_id;

        $isTime = $this->checkIfBlockSql();
        if ($isTime) {
            return $this->apiSuccessResponse(0, "No Check.", []);
        }

        $keyType = '#notificationInbox#';
        $push_new_id = $this->vcmemberRepository->getLatestPushId();

        if ($push_new_id && $push_new_id !== 0 && (int)$push_new_id > $latest_id) {
            $this->vcmemberRepository->deleteTableMeta($mbr_id, $keyType);
            return $this->apiSuccessResponse(1, "Success Updated.", [$push_new_id, $latest_id]);
        }

        return $this->apiSuccessResponse(0, "It's Latest", []);
    }

    public function updateInboxReadStatus(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $get_id = $request->id;
        $platform = $request->platform ? $request->platform : 'Desktop';
        $pushid = $request->pushid ? $request->pushid : 'LOCAL';
        $status = $request->status;

        $removeKey = '#notificationInbox#';

        if (!$mbr_id || !$platform || !$get_id) {
            return $this->apiSuccessResponse(1, "Ok", []);
        }

        if ($get_id) {
            $redisKey = $this->ENVIRONMENT . "#Vc_pushNotification_all";
            $indexKey = $mbr_id . $platform . $get_id;
            $item = Redis::hMget($redisKey, $indexKey)[0];
            if ($item) {
                $item = unserialize($item);
                if ($item["status"] && $item["status"] === 3) {
                    return $this->apiSuccessResponse(1, "Ok", []);
                }
                $list["status"] = ($item["status"] !== 1) ? 2 : 3;
            } else {
                $list["status"] = ($status !== 1) ? 2 : 3;
            }

            $list["mbr_id"] = $mbr_id;
            $list["batch_id"] = $get_id;
            $list["pushId"] = $pushid;
            $list["platform"] = $platform;
            $list["date"] = date("Y-m-d H:i:s");
            $list["created_on"] = date("Y-m-d H:i:s");
            $list["created_by"] = 'App';

            $params = $this->vcmemberRepository->getPushStatusQuery($list);
            $array[$indexKey] = serialize($params);

            Redis::hMset($redisKey, $array);
            Redis::expire($redisKey, $this->threeDays);

            $this->vcmemberRepository->updateMetaTableStatus($list, $removeKey, false);

        }


        return $this->apiSuccessResponse(1, "Success", $get_id);
    }

    public function updatePushClickStatus(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $pushlist_id = $request->id;
        $platform = $request->platform;
        $status = $request->status;
        $pushid = $request->pushid;
        $removeKey = '#notificationInbox#';

        if (!$mbr_id || !$platform || !$pushlist_id) {
            return $this->apiSuccessResponse(1, "Ok", []);
        }

        if ($pushlist_id) {
            $redisKey = $this->ENVIRONMENT . "#Vc_pushNotification_all";
            $indexKey = $mbr_id . $platform . $pushlist_id;
//            $alls= Redis::hgetall($redisKey);
//            print_r($alls);
//            die();
            $lists = Redis::hMget($redisKey, $indexKey)[0];
            if ($lists) {
                $lists = unserialize($lists);
                $lists["mbr_id"] = $mbr_id;
                $lists["platform"] = $platform;
                $lists["created_on"] = date("Y-m-d H:i:s");
                $lists["status"] = $lists["status"] === 2 ? 3 : 1;
            } else {
                $lists = array();
                $lists["mbr_id"] = $mbr_id;
                $lists["platform"] = $platform;
                $lists["created_on"] = date("Y-m-d H:i:s");
                $lists["status"] = $status;
            }
            $lists["pushId"] = $pushid;
            $lists["batch_id"] = $pushlist_id;
            $lists["date"] = date("Y-m-d H:i:s");
            $lists["created_by"] = 'Batch';

            $params = $this->vcmemberRepository->getPushStatusQuery($lists);
            $array[$indexKey] = serialize($params);

            Redis::hMset($redisKey, $array);
            Redis::expire($redisKey, $this->threeDays);

            $this->vcmemberRepository->updateMetaTableStatus($lists, $removeKey, true);
            $code = 1;
        }

        return $this->apiSuccessResponse(1, "Success", []);
    }

    public function updatePushId(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $uuid = $request->uuid;
        $pushId = $request->pushId;

        $result = $this->db20->table('o2o_member_devices')
            ->select('meta')
            ->where('deviceid', $uuid)
            ->get();

        if (count($result)) {
            $meta = json_decode($result[0]->meta, TRUE);
            if ($pushId == "-1") {
                $pushId = "";
            }
            $meta["pushId"] = $pushId;
            $meta = json_encode($meta);

            $this->db20->table('o2o_member_devices')
                ->select('meta')
                ->where('deviceid', $uuid)
                ->update([
                    'meta' => $meta
                ]);

            return $this->apiSuccessResponse(1, "success", $meta);
        } else {
            return $this->apiSuccessResponse(1, "nodata", []);
        }

    }

    public function checkWishListValid(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $guid = $request->guid;

//        $redisKey = ENVIRONMENT . "#vc_wishlist2020#";

//        $lists = Redis::hMget($redisKey, $guid);
        $validate = false;
//        if (count($lists) > 0) {
//            $item = json_decode($lists[0], true);
//            $validate = true;
//            $the_mbr_id = $item["mbr_id"];
//            if ($mbr_id != $the_mbr_id) {
//                $validate = false;
//            }
//        }

        if (!$validate) {
            return $this->apiSuccessResponse(0, "Failed", []);
        } else {
            return $this->apiSuccessResponse(1, "Success", []);
        }
    }

    public function testPush($mbr_id = '')
    {
        $dateNow = "2022-04-28-9";
        $push_process_num_id = 'VCMAY22_PUSH_' . $dateNow;

        $message = "Wow, 10 stars! Congratulations, you've earned V$1! 🥳";
        $button = "Learn More";
        $text_title = "Congratulation!";
        $title = "Congratulation!";
        $long_message = "<p>Wow, 10 stars! <br/>Congratulations, you've earned V$1! 🥳</p>";


        $this->PushMay22OneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

//        $button = "Learn More";
//        $text_title = "Congratulation!";
//        $title = "Congratulation!";
//        $long_message = "<p>👍 You've earned 2 stars and 10 chances for the PS5 redemption draw. Increase your chances by shopping more!</p>";
//        $message = "👍 You've earned 2 stars and 10 chances for the PS5 redemption draw. Increase your chances by shopping more!";
//
//        $this->PushMay22OneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

    }

    public function testMay22($email = '')
    {
        $sql = <<<str
select * from crm_member_list where email_addr='$email'

str;
        $member = $this->db20->select($sql);
        if (count($member) === 0) {
            return $this->apiSuccessResponse(0, "MEMBER Not Found.", []);
        } else {
            $mbr_id = $member[0]->mbr_id;

            echo $mbr_id;

            $headers = array(
                'Content-Type: application/json',
                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
            );
            $ch = curl_init();

            $hachi_pass = $this->hachiPass;

            $curl_url = 'https://' . $this->server_name . '/api/vc2/VcMay2022Campaign' . '/' . $hachi_pass . '/' . $mbr_id . '/' . '0';
//            $curl_url = 'http://' . 'localhost:8090' . '/api/vc2/VcMay2022Campaign' . '/' . $hachi_pass . '/' . $mbr_id . '/' . '0';


            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $response = curl_exec($ch);
            curl_close($ch);

            print_r($response);

            return $this->apiSuccessResponse(1, "Updated.", []);
        }
    }

    //Daily Report To Display Give Me 5 Campaign Star Redemption.
    public function batchMay22StarDailyReport()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $todayDate = date("Y-m-d");
        $todayDateTitle = date("d M Y");
        $ytdDate = date("Y-m-d", strtotime("-1 day"));
        $ytdDateTitle = date("d M Y", strtotime("-1 day"));

        // Total V$ given via star redemption
        $sql_total_rebates = <<<str
select sum(amount_earned) / 10 as total_rebates FROM vc_may22_chance_list
where type = 'star' and (status_level = 1 or amount_earned > 0);
str;
        $total_rebates = $this->db20->select($sql_total_rebates, []);
        if (count($total_rebates) > 0) {
            $total_rebates = $total_rebates[0]->total_rebates;
        } else {
            return $this->apiSuccessResponse(0, "Unable to query for total rebates.", []);
        }

        // Total stars accumulated
        $sql_total_stars = <<<str
select sum(amount) as total_stars FROM vc_may22_chance_list
where type = 'star';
str;
        $total_stars = $this->db20->select($sql_total_stars, []);
        if (count($total_stars) > 0) {
            $total_stars = $total_stars[0]->total_stars;
        } else {
            return $this->apiSuccessResponse(0, "Unable to query for total stars rewarded.", []);
        }


        // Stars given for each transaction
        $sql_stars_txn_breakdown = <<<str
select item_info, sum(amount) as total_stars from vc_may22_chance_list
where type = 'star'
group by item_info;
str;
        $stars_txn_breakdown = $this->db20->select($sql_stars_txn_breakdown, []);

        // Total stars given yesterday
        $sql_stars_given_ytd = <<<str
select sum(amount) as total_stars from vc_may22_chance_list
where type = 'star' and ? <= created_on and created_on < ?;
str;
        $stars_given_ytd = $this->db20->select($sql_stars_given_ytd, [$ytdDate, $todayDate]);
        if (count($stars_given_ytd) > 0) {
            $stars_given_ytd = $stars_given_ytd[0]->total_stars ? $stars_given_ytd[0]->total_stars : 0;
        } else {
            return $this->apiSuccessResponse(0, "Unable to query for total stars rewarded.", []);
        }

        // Stars given for each transaction yesterday
        $sql_stars_txn_breakdown_ytd = <<<str
select item_info, sum(amount) as total_stars from vc_may22_chance_list
where type = 'star' and ? <= created_on and created_on < ?
group by item_info;
str;
        $stars_txn_breakdown_ytd = $this->db20->select($sql_stars_txn_breakdown_ytd, [$ytdDate, $todayDate]);

        $subject = 'May22 Campaign Daily Star Redemption Report (Date: ' . $todayDateTitle . ') ';

        $body = $subject;

        $body .= "<style>td{border:1px solid black !important;}</style>";

        $body .= "</br></br>";

        $body .= "Date: " . "6 - " . $ytdDateTitle;

        $body .= "<table style='border: 1px solid black;'>";

        $body .= "<tr><td>Total V$ given via star redemption</td><td>Total stars given up to date</td></tr>";

        $body .= "<tr><td>$total_rebates</td><td>$total_stars</td></tr>";

        $body .= "</table><br>";

        $body .= "Breakdown of stars given out for each kind of transaction/source:";

        $body .= "<table style='border: 1px solid black;'>";

        $body .= "<tr><td>Kind of Transaction/Source</td><td>Total stars given out</td></tr>";

        foreach ($stars_txn_breakdown as $txn_type) {
            $body .= "<tr><td>$txn_type->item_info</td><td>$txn_type->total_stars</td></tr>";
        }

        $body .= "</table>" . "<br/>";

        $body .= "Daily stars given:";

        $body .= "<table style='border: 1px solid black;'>";

        $body .= "<tr><td>Date</td><td>Stars given</td></tr>";

        $body .= "<tr><td>$ytdDateTitle</td><td>$stars_given_ytd</td></tr>";

        $body .= "</table>" . "<br/>";

        $body .= "<table style='border: 1px solid black;'>";

        $body .= "<tr><td>Kind of Transaction/Source</td><td>Stars given out on $ytdDateTitle</td></tr>";

        foreach ($stars_txn_breakdown_ytd as $txn_type) {
            $body .= "<tr><td>$txn_type->item_info</td><td>$txn_type->total_stars</td></tr>";
        }

        $body .= "</table>";

        $body .= "<br/><br/>";

        $body .= "Best Regards,<br/>MIS.<br/><br/>";

        //Make it Dynamic.
        $email_detail = $this->vcmemberRepository->getEmailListings("EMAIL_MAY22");
//         print_r($email_detail);
//         die();

        $emails = $email_detail["email"];
        $cc = $email_detail["cc"];

        if ($emails == '') {
            return $this->apiSuccessResponse(0, "No Email Recipient.", $email_detail);
        }

//         $emails = "wong.weiseng@challenger.sg";
//         $cc = '';
        // $emails = "chin.hockyang@challenger.sg";
        // $cc = '';

        $res1 = $this->vcmemberRepository->sendEmailByCtlMailer('ValueClub@challenger.sg', $emails, $body, $subject, $cc);

        return $this->apiSuccessResponse(1, "Sent", $res1);

    }

    //Daily Report To Display Give Me 5 Campaign Draw Chance Redemption.
    public function batchMay22ChanceDailyReport()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $todayDate = date("d M Y");
        $ytdDate = date('Y-m-d', strtotime("-1 days"));
        $yesterdayDate = date('d M Y', strtotime("-1 days"));
        // print_r($yesterdayDate);

        $sql_select_one_time = "
        select count(*), mbr_id FROM vc_may22_chance_list
            where type= 'chance' 
                and item_info = 'TRANSACTION' 
                and cast(created_on as date)< '$ytdDate'
            group by mbr_id
            having count(*) = 1;
        ";
        $ytd_one_time = $this->db20->select($sql_select_one_time, []);
        $one_time = count($ytd_one_time);
        // print_r($one_time);

        $sql_select_two_three_times = "
        select count(*), mbr_id FROM vc_may22_chance_list
            where type= 'chance' 
                and item_info = 'TRANSACTION' 
                and cast(created_on as date)< '$ytdDate'
            group by mbr_id
            having (count(*) = 2 or count(*) = 3);
        ";
        $ytd_two_three_times = $this->db20->select($sql_select_two_three_times, []);
        $two_three_times = count($ytd_two_three_times);
        // print_r($two_three_times);


        $sql_select_more_four_times = "
        select count(*), mbr_id FROM vc_may22_chance_list
            where type= 'chance' 
                and item_info = 'TRANSACTION' 
                and cast(created_on as date)< '$ytdDate'
            group by mbr_id
            having count(*) >= 4;
        ";
        $ytd_more_four_times = $this->db20->select($sql_select_more_four_times, []);
        $more_four_times = count($ytd_more_four_times);
        // print_r($more_four_times);

        $sql_select_chance_membership = "select count(distinct trans_id) FROM vc_may22_chance_list where type = 'chance' and item_info in ('MEMBERSHIP') and cast(created_on as date)< '$ytdDate'";
        $ytd_chance_times = $this->db20->select($sql_select_chance_membership, []);

        $sql_select_chance_itez = <<<str
select count(distinct trans_id) FROM vc_may22_chance_list where type = 'chance' and item_info='ITEZ' and cast(created_on as date)< '$ytdDate'
str;
        $ytd_chance_itez_times = $this->db20->select($sql_select_chance_itez, []);

        $chance_times = 0;
        if ($ytd_chance_times && isset($ytd_chance_times[0]->count)) {
            $chance_times = $ytd_chance_times[0]->count;
        }

        $chance_times_itez = 0;
        if ($ytd_chance_itez_times && isset($ytd_chance_itez_times[0]->count)) {
            $chance_times_itez = $ytd_chance_itez_times[0]->count;
        }
        // print_r($chance_times);

        $subject = 'Redemption Draw Chance Daily Report (Date: ' . $todayDate . ') ';

        $body = $subject;

        $body .= "<br/><style>td{border:1px solid orange !important;}</style>";

        $body .= "<table style='border: 1px solid orange;'>";

        $body .= "<tr><td>Date</td><td>1 time</td><td>2-3 times</td><td>4 or more times</td><td>Membership Sign Up</td><td>ITEZ Subscription</td></tr>" . "<br/>";

        $body .= "<tr><td>1 May - $yesterdayDate</td><td>$one_time</td><td>$two_three_times</td><td>$more_four_times</td><td>$chance_times</td><td>$chance_times_itez</td></tr>" . "<br/>";

        $body .= "</table>";

        $body .= "<br/><br/><br/>";

        $body .= "Best Regards,<br/>MIS.<br/><br/>";

        //Make it Dynamic.
        $email_detail = $this->vcmemberRepository->getEmailListings('EMAIL_MAY22');
        // print_r($email_detail);
        // die();

        $emails = $email_detail["email"];
        $cc = $email_detail["cc"];

        if ($emails == '') {
            return $this->apiSuccessResponse(0, "No Email Recipient.", $email_detail);
        }

        //Hardcoded. Enter relevant emails here.
        // $emails = 'ng.siqiang@challenger.sg';
        // $cc = '';

        $res1 = $this->vcmemberRepository->sendEmailByCtlMailer('ValueClub@challenger.sg', $emails, $body, $subject, $cc);

        return $this->apiSuccessResponse(1, "Sent", $res1);
    }

    public function batchBingoDailyReports()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $todayDate = date("d M Y");
        $ytdDate = date('Y-m-d', strtotime("-1 days"));
        $yesterdayDate = date('d M Y', strtotime("-1 days"));

        print_r($todayDate);

        //Get Yesterday 's Total number of numbers generated
        $sql_insert_ytd = <<<str
select distinct trans_id, status
from o2o_log_live_processes_new where process_id in ('VC_BINGO_NO')
and cast(created_on as date)='$ytdDate'
str;
        $yesterday_lists = $this->db20->select($sql_insert_ytd, []);
        $count_ytd_list = count($yesterday_lists);


        //Get YEsterday 's Total number of rebates given out
        $sql_rebate_ytd = <<<str
        select  distinct invoice_id
        from o2o_log_live_processes_new where process_id like 'BINGO_REBATE_%' and invoice_id!=''
        and cast(created_on as date)='$ytdDate'

str;
        $yesterday_rebates = $this->db20->select($sql_rebate_ytd, []);
        $count_ytd_rebate = count($yesterday_rebates);

        //Get Yesterday 's Number of people completed all row
        $yesterday_complete_sql = <<<str
        select distinct mbr_id, status
        from o2o_log_live_processes_new where process_id in ('VC_BINGO_COMPLETE')
        and cast(created_on as date)='$ytdDate'
str;

        $yesterday_completeds = $this->db20->select($yesterday_complete_sql, []);
        $count_ytd_completed = count($yesterday_completeds);

        //Get Total number of numbers generated
        $sql_total = <<<str
        select distinct trans_id, status
        from o2o_log_live_processes_new where process_id in ('VC_BINGO_NO')
str;
        $total_lists = $this->db20->select($sql_total, []);
        $count_total_list = count($total_lists);

        //Get Total number of rebates given out
        $sql_rebate_total = <<<str
       select distinct invoice_id
from o2o_log_live_processes_new where process_id like 'BINGO_REBATE_%' and invoice_id!=''
str;
        $total_rebates = $this->db20->select($sql_rebate_total, []);
        $count_total_rebates = count($total_rebates);

        //Get Total Of completed.
        $total_complete_sql = <<<str
        select distinct mbr_id, status
        from o2o_log_live_processes_new where process_id in ('VC_BINGO_COMPLETE')
str;

        $total_completeds = $this->db20->select($total_complete_sql, []);
        $count_total_completed = count($total_completeds);

        $subject = 'Bingo Contest 2021 Report (Date: ' . $todayDate . ') ';

        $body = $subject;

        $body .= "<br/><style>td{border:1px solid orange !important;}</style>";

        $body .= "<table style='border: 1px solid orange;'>";

        $body .= "<tr><td>Date</td><td>Number of balls generated</td><td>Total Number of rebates given out</td><td>Number of people completed all row.</td></tr>" . "<br/>";

        $body .= "<tr><td>$yesterdayDate</td><td>$count_ytd_list</td><td>$count_ytd_rebate</td><td>$count_ytd_completed</td></tr>" . "<br/>";

        $body .= "<tr><td>1 Nov - $todayDate</td><td>$count_total_list</td><td>$count_total_rebates</td><td>$count_total_completed</td></tr>" . "<br/>";

        $body .= "</table>";

        $body .= "<br/><br/><br/>";

        $body .= "Best Regards,<br/>MIS.<br/><br/>";

        //Make it Dynamic.
        $email_detail = $this->vcmemberRepository->getEmailListings('EMAIL_BINGO20');
//        print_r($email_detail);
//        die();

        $emails = $email_detail["email"];
        $cc = $email_detail["cc"];

        if ($emails == '') {
            return $this->apiSuccessResponse(0, "No Email Recipient.", $email_detail);
        }

        //Hardcoded. Enter relevant emails here.
//		$emails = 'wong.weiseng@challenger.sg';
//		$cc = '';

        $res1 = $this->vcmemberRepository->sendEmailByCtlMailer('ValueClub@challenger.sg', $emails, $body, $subject, $cc);

        return $this->apiSuccessResponse(1, "Sent", $res1);
    }


    public function getBingoLists(Request $request)
    {
        $mbr_id = $request->mbr_id;

        $keyType = '#Bingo#';
        //        //Get Current Result From Postgre If Exists.
        $sql_get = <<<str
	select * from "Members_datatype" where mbr_id= ? and keytype= ?
str;
        $current_result = $this->chapps->select($sql_get, [$mbr_id, $keyType]);
        if (count($current_result) > 0) {
            $cache_result = $this->vcmemberRepository->stdToArray($current_result[0]);
            $result = json_decode($cache_result["meta"], true);
        } else {
            //Generate Array.
            $headers = array(
                'Content-Type: application/json',
                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
            );
            $ch = curl_init();

            $hachi_pass = $this->hachiPass;

//            $this->server_name = 'localhost:8090';
            $curl_url = 'https://' . $this->server_name . '/api/vc2/VcBingoCampaign' . '/' . $hachi_pass . '/' . $mbr_id . '/' . '1';
//            print_r($curl_url);

            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $response = curl_exec($ch);
            curl_close($ch);

//            print_r($response);

            $current_result = $this->chapps->select($sql_get, [$mbr_id, $keyType]);
            if (count($current_result) > 0) {
                $cache_result = $this->vcmemberRepository->stdToArray($current_result[0]);
                $result = json_decode($cache_result["meta"], true);
            } else {
                $result = $this->getDefaultArray();
            }
        }

        $resp = $result;

        return $this->apiSuccessResponse(1, "OK", $resp);

    }

    public function batchCallBingoCampaign()
    {
        $sql = <<<str
select trans.mbr_id, trans_id
from crm_member_transaction trans
inner join crm_member_list member on member.coy_id= trans.coy_id and member.mbr_id= trans.mbr_id
where member.coy_id='CTL'
  and (member.mbr_type in ('M','M18', 'M28', 'MSTP', 'MVIP') or LEFT(member.mbr_type,1)='S')
and cast(trans_date as date) >= '2021-12-01'
 and trans.item_qty >= 1
 and trans_type not in ('SR','OS','HX','HR','OD','EX','AP','RD','EP')
  and trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_BINGO_JOB'
           ) limit 75;
str;
        $lists = $this->db20->select($sql, []);
//        print_r($lists);
//        die();

        $mbr_lists = array();
        foreach ($lists as $list) {
            $mbr_id = trim($list->mbr_id);
            $trans_id = trim($list->trans_id);

            if (in_array($mbr_id, $mbr_lists)) {
                echo "Repeat" . $mbr_id . "<br/>";
                continue;
            }

            $mbr_lists[] = $mbr_id;

            echo $mbr_id . "<br/>";

            //Generate Array.
            $headers = array(
                'Content-Type: application/json',
                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
            );
            $ch = curl_init();

            $hachi_pass = $this->hachiPass;

            $curl_url = 'https://' . $this->server_name . '/api/vc2/VcBingoCampaign' . '/' . $hachi_pass . '/' . $mbr_id;
//            echo $curl_url;

            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $response = curl_exec($ch);
            curl_close($ch);

            $this->_logNewprocess('VC_BINGO_JOB', $trans_id, $trans_id, '1', '', $mbr_id, '', 'Bingo2');
        }

        return $this->apiSuccessResponse(1, "Processed", count($lists));
    }

    public function batchCallCny2022Campaign()
    {
        $sql = <<<str
select trans.mbr_id, trans_id
from crm_member_transaction trans
inner join crm_member_list member on member.coy_id= trans.coy_id and member.mbr_id= trans.mbr_id
where member.coy_id='CTL'
  and (member.mbr_type in ('M','M18', 'M28', 'MSTP', 'MVIP') or LEFT(member.mbr_type,1)='S')
and cast(trans_date as date) >= '2022-01-25'
 and trans.item_qty >= 1
 and trans_type not in ('SR','OS','HX','HR','OD','EX','AP','RD','EP')
  and trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_CNY2022_JOB'
           ) limit 75;
str;
        $lists = $this->db20->select($sql, []);
//        print_r($lists);
//        die();

        $mbr_lists = array();
        foreach ($lists as $list) {
            $mbr_id = trim($list->mbr_id);
            $trans_id = trim($list->trans_id);

            if (in_array($mbr_id, $mbr_lists)) {
                echo "Repeat" . $mbr_id . "<br/>";
                continue;
            }

            $mbr_lists[] = $mbr_id;

            echo $mbr_id . "<br/>";

            //Generate Array.
            $headers = array(
                'Content-Type: application/json',
                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
            );
            $ch = curl_init();

            $hachi_pass = $this->hachiPass;

            $curl_url = 'https://' . $this->server_name . '/api/vc2/VcCny2022Campaign' . '/' . $hachi_pass . '/' . $mbr_id;
//            echo $curl_url;

            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $response = curl_exec($ch);
            curl_close($ch);

            $this->_logNewprocess('VC_CNY2022_JOB', $trans_id, $trans_id, '1', '', $mbr_id, '', 'Cny2022');
        }

        return $this->apiSuccessResponse(1, "Processed", count($lists));
    }


    public function batchCallMay22ITEZSubscription()
    {

        //Check If got Participate Brands.
        $url = $this->imsUrl . "vcapp/gettodayitezsubscription";

        $lists = $this->curlImsApi($url, array());
        print_r($lists);
        die();

        foreach ($lists as $list) {
            $invoice_id = trim($list["invoice_id"]);
            $invoice_date = $list["invoice_date"];

            $sql_validate = <<<str
select* from o2o_log_live_processes where process_id='VC_MAY2022_ITEZ' and mbr_id= ?
str;
            $check = $this->db20->select($sql_validate, [$invoice_id]);
            if (!$check) {

                //Insert to Chance.


                $this->_logprocess('VC_MAY2022_ITEZ', '1', $invoice_date, $invoice_id, 'VcApp2');

            }

        }
        return $this->apiSuccessResponse(1, "Processed", count($lists));
    }

    public function batchCallMay22GM5Campaign()
    {
        $dateNow = date("Y-m-d");
        $sql = <<<str
select trans.trans_id, trans.mbr_id
from crm_member_transaction trans
inner join crm_member_list member on member.coy_id= trans.coy_id and member.mbr_id= trans.mbr_id
where member.main_id ='' and
    (member.mbr_type in ('M','M18', 'M28', 'MSTP', 'MVIP') or LEFT(member.mbr_type,1)='S')
  and trans.coy_id='CTL'
and cast(trans.trans_date as date) >= '2022-05-06'
  and cast(trans.trans_date as date) = '$dateNow'
 and trans.trans_type not in ('SR','HX','HR','OD','EX','AP','RD','EP')
  and trans.trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_MAY2022_JOB'
           ) limit 75;
str;
        $mbr_lists = array();
        $lists = $this->db20->select($sql, []);
//        print_r($lists);
//        die();
        foreach ($lists as $index => $list) {
            $mbr_id = trim($list->mbr_id);
            $trans_id = trim($list->trans_id);

            if (in_array($mbr_id, $mbr_lists)) {
//                echo "Repeat" . $mbr_id . "<br/>";
                continue;
            }

            $mbr_lists[] = $mbr_id;
            echo $mbr_id . "<br/>";

            //Generate Array.
            $headers = array(
                'Content-Type: application/json',
                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
            );
            $ch = curl_init();

            $hachi_pass = $this->hachiPass;

            $curl_url = 'https://' . $this->server_name . '/api/vc2/VcMay2022Campaign' . '/' . $hachi_pass . '/' . $mbr_id;
            echo $curl_url . "<br/>";

            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            $response = curl_exec($ch);
            curl_close($ch);

            $this->_logNewprocess('VC_MAY2022_JOB', $trans_id, $trans_id, '1', '', $mbr_id, '', 'May2022');
        }
        return $this->apiSuccessResponse(1, "Processed", count($lists));
    }

    public function getMay22CatalogueDetail(Request $request)
    {
        $mbr_id = $request->mbr_id;
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Member Id not found, please try to login again.", []);
        }

        $sql = <<<str
select * from vc_may22_chance_list where mbr_id= ? and status_level >= 0 order by created_on desc
str;
        $lists = $this->db20->select($sql, [$mbr_id]);
        $result = array(
            "stars" => [],
            "chances" => [],
            "rebates" => 0
        );

        $total_earned = 0;
        foreach ($lists as $list) {
            $type = $list->type;
            $status_level = $list->status_level;
            $amount_earned = $list->amount_earned;

            $trans_id = isset($list->trans_id) ? $list->trans_id : '';
            if ($trans_id && $trans_id != '--') {
                $invoice_id = $trans_id;
                $sub = substr(trim($invoice_id), -3, 3);
                $signature = md5($invoice_id . '@Challenger.' . $sub);
                $url_link = "https://www.challenger.com.sg/invoice/trusted/" . $trans_id . "?ereceipt=y&signature=" . $signature;
            } else {
                $url_link = "";
            }
            $list->url_link = $url_link;

            if ($type == 'star') {
                $result["stars"][] = $list;

                if ($status_level >= 0 && $amount_earned > 0) {
                    $total_earned += $amount_earned;

                }
            } else if ($type == 'chance') {
                $result["chances"][] = $list;
            }


        }

        $result["total"] = $total_earned;
        $result["rebates"] = floor(($total_earned / 10));

//        $result["rebates"] = 2;

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function givemefiveAddStar($the_id = '')
    {
        if ($the_id == '') {
            return $this->apiSuccessResponse(0, 'Mbr ID is empty. Please Login Challenger.Sg.', []);
        }

//2. Check member type.
        $mbr_sql = <<<str
   select RTRIM(mbr_type) as mbr_type,RTRIM(main_id) as main_id, RTRIM(mbr_id) as mbr_id  from crm_member_list where mbr_id= ? or email_addr= ?
str;
        $member = $this->db20->select($mbr_sql, [$the_id, $the_id]);
        if (count($member) > 0) {
            $mbr_type = $member[0]->mbr_type;
            $the_mbr_id = $member[0]->mbr_id;
        } else {
            $mbr_type = '';
        }

//    print_r($mbr_type);
        if (!in_array($mbr_type, ['M', 'M18', 'M28', 'MSTP', 'MVIP']) && substr($mbr_type, 0, 1) != 'S') {
            return $this->apiSuccessResponse(0, "Mbr Type is not eligible for Give me Five Campaign.", []);
        }

        //3. Validate if today already redeemed how much.
        $dateNow = date("Y-m-d");
        $timeOneHourAgo = date("Y-m-d H:i:s", strtotime('-1 hour', time()));
//        $sql_validate = <<<str
//select to_char(created_on ,'YYYY-MM-DD HH:MI:ss') as created_time,
//       * from vc_may22_chance_list where cast(created_on as date)='$dateNow' and type='star' and mbr_id= ? and item_info='STAR HUNT' order by created_on desc;
//str;
//        $validate = $this->db20->select($sql_validate, [$the_mbr_id]);
//        if (count($validate) >= 3) {
//            return $this->apiSuccessResponse(0, "You have already reached today's star hunt limit.", []);
//        }

        $star_amount = 1;
        $star_type = 'star';
        $the_trans_id = '--';
        $remarks = '';

        $sql_chance_ins = <<<str
insert into vc_may22_chance_list (mbr_id, trans_id, type, amount, item_info, status_level, remarks, created_by, created_on, amount_earned)
    VALUES
('$the_mbr_id','$the_trans_id','$star_type', ? ,'STAR HUNT',0,'$remarks','VcApp2',NOW(), 0)
str;
        $this->db20->statement($sql_chance_ins, [$star_amount]);

        //Also check How Many Star Already got.
        $isGetRebate = false;
        $dateNow = date("Y-m-d");

        // Check If Collected 10 Star, Converted to V$1.
        $mbr_id = $the_mbr_id;
        $sql_verify = <<<str
select SUM(amount- amount_earned) as total_sum from vc_may22_chance_list where mbr_id= ? and type='star' and status_level=0
str;
        $has_amount = $this->db20->select($sql_verify, [$mbr_id]);
        $total_earned = $has_amount[0]->total_sum;
//        print_r($total_earned);
//        die();

        if ($total_earned >= 10) {
            //10. Got 10 Star already, converted to V$1 rebate.
            $sql_check = <<<str
select * from vc_may22_chance_list where mbr_id= ? and type='star' and status_level=0 order by id;
str;
            $collected_lists = $this->db20->select($sql_check, [$mbr_id]);

            $rebate_amount = 10;
            //11 Update vc_may22_chance_list status.
            foreach ($collected_lists as $collected_list) {
                if ($rebate_amount <= 0) {
                    break;
                }
                $id = $collected_list->id;
                $amount = $collected_list->amount;
                $amount_earned = $collected_list->amount_earned;

                $mbr_id = $collected_list->mbr_id;

                $amount_remaining = $amount - $amount_earned;

                if ($rebate_amount >= $amount_remaining) {
                    $amount_deduct = $amount_remaining;
                    $rebate_amount -= $amount_remaining;
                    $sql_upd_amount = <<<str
    update vc_may22_chance_list set amount_earned= amount_earned + ? , updated_on=NOW(), updated_by= ? , status_level=1, remarks= remarks || ' - Deducted.' where id=? 
str;
                    $this->db20->statement($sql_upd_amount, [$amount_deduct, $mbr_id, $id]);

                } else {
                    $amount_deduct = $rebate_amount;

                    if ($amount_deduct == $amount_remaining) {
                        $status = 1;
                        $the_remark = ' - Fully Deducted.';
                    } else {
                        $status = 0;
                        $the_remark = ' - Partial Deducted.';
                    }

                    $rebate_amount -= $amount;

                    $sql_upd_amount = <<<str
    update vc_may22_chance_list set amount_earned= amount_earned + ? , updated_on=NOW(), updated_by= ? ,status_level= ?, remarks= remarks || ? where id=? 
str;
                    $this->db20->statement($sql_upd_amount, [$amount_deduct, $mbr_id, $status, $the_remark, $id]);

                }


            }

            //12. Give Rebates.
            $rebate_amt = 1;
            $cur_date = date("Y-m-d");
            $rebates_process_id = 'MAY2022_REBATE_' . $cur_date;
            //Give Rebates Func.
            $this->May2022InsertRebate($mbr_id, $rebates_process_id, $rebate_amt, 'STAR', $cur_date);

            $isGetRebate = true;
        }

        //13. Send Push Notification.
        $push_process_num_id = 'VCMAY22_PUSH_' . $dateNow;
        if ($isGetRebate) {

            $message = "Wow, 10 stars! Congratulations, you've earned V$1! 🥳";
            $button = "Learn More";
            $text_title = "Congratulation!";
            $title = "Congratulation!";
            $long_message = "<p>Wow, 10 stars! <br/>Congratulations, you've earned V$1! 🥳</p>";

            $this->PushMay22OneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

        }

        return $this->apiSuccessResponse(1, "Star Added.", []);
    }

    public function VcMay2022Campaign($hachi_pass = '', $mbr_id = '', $isDisplay = '0')
    {
        if ($hachi_pass !== $this->hachiPass || $mbr_id == '') {
            return $this->apiSuccessResponse(0, 'Hachi Pass Incorrect or MBr ID is empty.', []);
        }

        $env = $this->ENVIRONMENT;

        $campaign_start_date = '2022-05-06';
        $campaign_last_date = '2022-06-07';
        //1. Check Date.
        $dateNow = date("Y-m-d");
//        print_r($dateNow);
        if ($env == 'production' && $dateNow < $campaign_start_date) {
            return $this->apiSuccessResponse(0, "Live Campaign is not Started.", []);
        }
        if ($dateNow >= $campaign_last_date) {
            return $this->apiSuccessResponse(0, "The Campaign has ended.", []);
        }

        //2. Check member type.
        $mbr_sql = <<<str
   select RTRIM(mbr_type) as mbr_type,RTRIM(main_id) as main_id  from crm_member_list where mbr_id= ?
str;
        $member = $this->db20->select($mbr_sql, [$mbr_id]);
        if (count($member) > 0) {
            $mbr_type = $member[0]->mbr_type;
        } else {
            $mbr_type = '';
        }

//    print_r($mbr_type);
        if (!in_array($mbr_type, ['M', 'M18', 'M28', 'MSTP', 'MVIP']) && substr($mbr_type, 0, 1) != 'S') {
            return $this->apiSuccessResponse(0, "Mbr Type is not eligible for Give me Five Campaign.", []);
        }

        //3. Check Today 's Member Transaction that more than $50.
        $sql = "  select a.* from (
       select  RTRIM(mbr_id) as mbr_id,
              RTRIM(trans.trans_id) as trans_id,
            string_agg(RTRIM(trans.item_id), ' | ') as all_item,
            MAX(trans.item_id) as item_id,
              MAX(trans.line_num) as line_num,
              MAX(cast(trans_date as timestamp)) as  trans_date,
              SUM(cast(
                      cast(item_qty as int) * cast(unit_price as decimal(10, 2)) as decimal(10, 2))) as total
       from crm_member_transaction trans
       where  mbr_id=  ?
           and cast(trans_date as date)>='2022-05-06'
           and cast(trans_date as date)<='$campaign_last_date'
            and cast(trans_date as date)='$dateNow'
        and trans.item_qty >= 1
         and trans_type not in ('SR','HX','HR','OD','EX','AP','RD','EP')
         and trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_MAY2022_JOB'
  and mbr_id= ?
           )
    and NOT EXISTS (
select 1 from o2o_log_live_processes_new logg where logg.process_id='VC_MAY2022_LOG'
  and logg.mbr_id= ?  and cast(logg.created_on as date)= '$dateNow'
         )
         and trans_id not in (
select trans_id from vc_may22_chance_list where mbr_id= ?
           )
       group by mbr_id,trans_id
   ) as a where a.total >= 50 or a.item_id in ('!MEMBER-NEW28','!MEMBER-REN28') limit 1";

        $transactions = $this->db20->select($sql, [$mbr_id, $mbr_id, $mbr_id, $mbr_id]);
        if ($isDisplay == '1') {
            print_r($transactions);
            die();
        }
//        print_r($transactions);
//        die();

        if (count($transactions) > 0) {
            //4, Check If Contain Membership.

            $the_trans_id = count($transactions) ? $transactions[0]->trans_id : '';
            $the_total = count($transactions) ? $transactions[0]->total : '';
            $the_trans_date = count($transactions) ? $transactions[0]->trans_date : '';
            $all_items = count($transactions) ? $transactions[0]->all_item : '';

            $hasMembership = false;
            $hasMoreThanTotal = false;
            if (strpos($all_items, '!MEMBER-') !== false) {
                //Contain Membership
                $hasMembership = true;
            }

            //Check If got Participate Brands.
            $url = $this->imsUrl . "vcapp/getproductdetailall";

            $post_data = array(
                "all_items" => $all_items,
                "item_id" => ""
            );

            $item_lists = $this->curlImsApi($url, $post_data);

            $hasParticipateBrand = false;
            $participate_brands = ['APPLE - APPLE',
                'APPLE - VALORE',
                'MULTIMEDIA - VALORE',
                'MULTIMEDIA - VALUECLUB EXCLUSIVE',
                'MULTIMEDIA - 1MORE',
                'MULTIMEDIA - AUDIO-TECHNICA',
                'MULTIMEDIA - CORSAIR',
                'MULTIMEDIA - CREATIVE',
                'MULTIMEDIA - EARFUN',
                'MULTIMEDIA - EDIFIER',
                'MULTIMEDIA - HYPERX',
                'MULTIMEDIA - JABRA',
                'MULTIMEDIA - JBL',
                'MULTIMEDIA - KLIPSCH',
                'MULTIMEDIA - LOGITECH',
                'MULTIMEDIA - MARSHALL',
                'MULTIMEDIA - RAZER',
                'MULTIMEDIA - SAMSUNG',
                'MULTIMEDIA - SENNHEISER',
                'MULTIMEDIA - SKULLCANDY',
                'MULTIMEDIA - SONICGEAR',
                'MULTIMEDIA - SONY',
                'MULTIMEDIA - STEELSERIES',
                'MULTIMEDIA - SUDIO',
                'MULTIMEDIA - TRIBIT',
                'MULTIMEDIA - VINNFIER',
                'WELLNESS - LIFETRONS',
                'WELLNESS - KOIZUMI',
                'WELLNESS - AUKEY',
                'WELLNESS - WISTECH',
                'DIGITALCAM - FUJIFILM',
                'DIGITALCAM - HOHEM',
                'ELECTRICAL - FELLOWES',
                'ACCESSORY - AGVA',
                'ACCESSORY - BELKIN',
                'APPLE - BELKIN',
                'ELECTRICAL - BELKIN',
                'ELECTRICAL - MAZER',
                'REDMONSTER',
                'ACCESSORY - INXUS',
                'ACCESSORY - NORTHBAYOU',
                'J5CREATE',
                'SUPERV',
                'ELECTRICAL - COOLER MASTER',
                'INPUT DEV - COOLER MASTER',
                'MITORI',
                'PHILIPS',
                'ELECTRICAL - VALORE',
                'ACCESSORY - VALORE',
                'INPUT DEV - VALORE',
                'ACCESSORY - VALUECLUB EXCLUSIVE',
                'ELECTRICAL - VALUECLUB EXCLUSIVE',
                'INPUT DEV - VALUECLUB EXCLUSIVE',
                'ELECTRICAL - OMARS',
                'ELECTRICAL - BASEUS',
                'ACCESSORY - BASEUS',
                'STORAGE - SEAGATE',
                'STORAGE - WD',
                'STORAGE - SANDISK',
                'STORAGE - TOSHIBA',
                'NOTEBOOK - AVITA',
                'NOTEBOOK - VAIO',
                'DESKTOP - MICROSOFT',
                'NOTEBOOK - MICROSOFT',
                'TABLET - MICROSOFT',
                'HUAWEI',
                'NOTEBOOK - ACER',
                'DESKTOP - ACER',
                'DESKTOP - ASUS',
                'TABLET - ASUS',
                'NOTEBOOK - ASUS',
                'TABLET - LENOVO',
                'NOTEBOOK - LENOVO',
                'DESKTOP - LENOVO',
                'DESKTOP - HP',
                'NOTEBOOK - HP',
                'TABLET - HP',
                'KOBO',
                'WACOM',
                'XP-PEN',
                'LOGITECH G',
                'RAZER',
                'CORSAIR',
                'GIFT CARD - MICROSOFT',
                'VIDEO GAMES - MICROSOFT',
                'VIDEO GAMES - MICROSOFT STUDIOS',
                'ARMAGGEDDON',
                'OTHER SVC - ASUS',
                'VIDEO GAMES - MARVO',
                'FELLOWES',
                'ACCESSORY - BELKIN',
                'ACCESSORY - ENERGIZER',
                'POWERPAC',
                'MORRIES',
                'MASTERPLUG',
                'ACCESSORY - SOUNDTEOH',
                'ELECTRICAL - SOUNDTEOH',
                'HOME & KITCHEN - SOUNDTEOH',
                'LED - SOUNDTEOH',
                'WELLNESS - SOUNDTEOH',
                'ACCESSORY - VALORE',
                'HOME & KITCHEN - VALORE',
                'ELECTRICAL - VALORE',
                'LED - VALORE',
                'ACCESSORY - VALUECLUB EXCLUSIVE',
                'HOME & KITCHEN - VALUECLUB EXCLUSIVE',
                'LED - VALUECLUB EXCLUSIVE',
                'INPUT DEV - LOGITECH',
                'ACCESSORY - LOGITECH G',
                'INPUT DEV - ALCATROZ',
                'INPUT DEV - MICROSOFT',
                'INPUT DEV - TARGUS',
                'MOBILE ACCESSORY - BELKIN',
                'TABLET - BELKIN',
                'MOBILE ACCESSORY - MAZER',
                'WEARABLE - AUKEY',
                'APPLE - AUKEY',
                'CYGNETT',
                'ENERGEA',
                'ELECTRICAL - ENERGIZER',
                'APPLE - ENERGIZER',
                'APPLE - SOUNDTEOH',
                'ACCESSORY - SOUNDTEOH',
                'UNIQ',
                'MOBILE ACCESSORY - VALORE',
                'MOBILE ACCESSORY - VALUECLUB EXCLUSIVE',
                'SMARTPHONE - VALUECLUB EXCLUSIVE',
                'APPLE - VALUECLUB EXCLUSIVE',
                'OMARS',
                'APPLE - BASEUS',
                'MOBILE ACCESSORY - BASEUS',
                'ACCESSORY - ASUS',
                'SAMSUNG',
                'XGIMI',
                'DATA COMM - GOOGLE',
                'DATA COMM - TP-LINK',
                'DATA COMM - D-LINK',
                'DATA COMM - LINKSYS',
                'DATA COMM - ASUS',
                'DATA COMM - NETGEAR',
                'CRICUT',
                'BROTHER',
                'SUPPLIES - HP',
                'PRINTING - HP',
                'PRINTING - EPSON',
                'SUPPLIES - EPSON',
                'SUPPLIES - CANON',
                'PRINTING - CANON',
                'SMARTPHONE - OPPO',
                'TABLET - OPPO',
                'REALME',
                'VIVO',
                'SMARTPHONE - XIAOMI',
                'TABLET - XIAOMI',
                'WEARABLE - XIAOMI',
                'LENOVO',
                'SOFTWARE - MICROSOFT',
                'VIDEO GAMES - MICROSOFT',
                'NORTON',
                'ELECOM',
                'ACCESSORY - POWERPAC',
                'LIVALL',
                'WEARABLE - AUKEY',
                'ELECTRICAL - AUKEY',
                'WEARABLE - GARMIN',
                'WELLNESS - GARMIN',
                'MOBILE ACCESSORY - GARMIN',
                'MOBVOI',
                'FITBIT',
                'KINGDOM'
            ];

            foreach ($item_lists as $item_list) {
                $category = trim($item_list["inv_dim2"]);
                $brand_id = trim($item_list["brand_id"]);

                $combines = $category . ' - ' . $brand_id;

                if (in_array($combines, $participate_brands) || in_array($brand_id, $participate_brands)) {
                    $hasParticipateBrand = true;
                    break;
                }

            }


            //Amount for Star.
            $trans_star = 2;
            $mbr_star = 2;
            $star_type = 'star';

            //Amount for Chance.
            $trans_chance = 10;
            $no_trans_chance = 1;
            $mbr_chance = 5;
            $chance_type = 'chance';

            $remarks = $the_total . ' - ' . $the_trans_date;

            //5. Insert Star Collection.
            if ($the_total >= 50) {
                $hasMoreThanTotal = true;
            }
            if ($hasMoreThanTotal && $hasParticipateBrand) {
                $sql_star_ins = <<<str
insert into vc_may22_chance_list (mbr_id, trans_id, type, amount, item_info, status_level, remarks, created_by, created_on, amount_earned)
    VALUES
('$mbr_id','$the_trans_id','$star_type', ? ,'TRANSACTION',0,'$remarks' ,'VcApp2',NOW(), 0)
str;
                $this->db20->statement($sql_star_ins, [$trans_star]);
            }


            //6. Insert Chance Collection.
            if ($hasMoreThanTotal) {
                if ($hasParticipateBrand) {
                    $chance_amount = $trans_chance;
                } else {
                    $chance_amount = $no_trans_chance;
                }
                $sql_chance_ins = <<<str
insert into vc_may22_chance_list (mbr_id, trans_id, type, amount, item_info, status_level, remarks, created_by, created_on, amount_earned)
    VALUES
('$mbr_id','$the_trans_id','$chance_type', ? ,'TRANSACTION',0,'$remarks','VcApp2',NOW(), 0)
str;
                $this->db20->statement($sql_chance_ins, [$chance_amount]);
            }

            //7. If got Membership, Insert Star/Chance too.
            if ($hasMembership) {
                $sql_star_mbrship = <<<str
insert into vc_may22_chance_list (mbr_id, trans_id, type, amount, item_info, status_level, remarks, created_by, created_on, amount_earned)
    VALUES
('$mbr_id','$the_trans_id','$star_type', ? ,'MEMBERSHIP',0,'$remarks' ,'VcApp2',NOW(), 0)
str;
                $this->db20->statement($sql_star_mbrship, [$mbr_star]);


                $sql_chance_mbrship = <<<str
insert into vc_may22_chance_list (mbr_id, trans_id, type, amount, item_info, status_level, remarks, created_by, created_on, amount_earned)
    VALUES
('$mbr_id','$the_trans_id','$chance_type', ? ,'MEMBERSHIP',0,'$remarks','VcApp2',NOW(), 0)
str;
                $this->db20->statement($sql_chance_mbrship, [$mbr_chance]);

            }

            $isGetRebate = false;

            //8. Insert Insert Log.
            $this->_logNewprocess('VC_MAY2022_LOG', $the_trans_id, $the_trans_date, 'OK', $the_total, $mbr_id, '', 'VcApp');

            //9. Check If Collected 10 Star, Converted to V$1.
            $sql_verify = <<<str
select SUM(amount- amount_earned) as total_sum from vc_may22_chance_list where mbr_id= ? and type='star' and status_level=0
str;
            $has_amount = $this->db20->select($sql_verify, [$mbr_id]);
            $total_earned = $has_amount[0]->total_sum;
            if ($total_earned >= 10) {
                //10. Got 10 Star already, converted to V$1 rebate.
                $sql_check = <<<str
select * from vc_may22_chance_list where mbr_id= ? and type='star' and status_level=0 order by id;
str;
                $collected_lists = $this->db20->select($sql_check, [$mbr_id]);

                $rebate_amount = 10;
                //11 Update vc_may22_chance_list status.
                foreach ($collected_lists as $collected_list) {
                    if ($rebate_amount <= 0) {
                        break;
                    }
                    $id = $collected_list->id;
                    $amount = $collected_list->amount;
                    $amount_earned = $collected_list->amount_earned;

                    $mbr_id = $collected_list->mbr_id;

                    $amount_remaining = $amount - $amount_earned;

                    if ($rebate_amount >= $amount_remaining) {
                        $amount_deduct = $amount_remaining;
                        $rebate_amount -= $amount_remaining;
                        $sql_upd_amount = <<<str
    update vc_may22_chance_list set amount_earned= amount_earned + ? , updated_on=NOW(), updated_by= ? , status_level=1, remarks= remarks || ' - Deducted.' where id=? 
str;
                        $this->db20->statement($sql_upd_amount, [$amount_deduct, $mbr_id, $id]);

                    } else {
                        $amount_deduct = $rebate_amount;

                        if ($amount_deduct == $amount_remaining) {
                            $status = 1;
                            $the_remark = ' - Fully Deducted.';
                        } else {
                            $status = 0;
                            $the_remark = ' - Partial Deducted.';
                        }

                        $rebate_amount -= $amount;

                        $sql_upd_amount = <<<str
    update vc_may22_chance_list set amount_earned= amount_earned + ? , updated_on=NOW(), updated_by= ? ,status_level= ?, remarks= remarks || ? where id=? 
str;
                        $this->db20->statement($sql_upd_amount, [$amount_deduct, $mbr_id, $status, $the_remark, $id]);

                    }


                }

                //12. Give Rebates.
                $rebate_amt = 1;
                $cur_date = date("Y-m-d");
                $rebates_process_id = 'MAY2022_REBATE_' . $cur_date;
                //Give Rebates Func.
                $this->May2022InsertRebate($mbr_id, $rebates_process_id, $rebate_amt, 'STAR', $cur_date);

                $isGetRebate = true;
            }

            //13. Send Push Notification.
            $push_process_num_id = 'VCMAY22_PUSH_' . $dateNow;
            if ($isGetRebate) {

                $message = "Wow, 10 stars! Congratulations, you've earned V$1! 🥳";
                $button = "Learn More";
                $text_title = "Congratulation!";
                $title = "Congratulation!";
                $long_message = "<p>Wow, 10 stars! <br/>Congratulations, you've earned V$1! 🥳</p>";

                $this->PushMay22OneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

            } else {
                if ($hasParticipateBrand) {
                    $button = "Learn More";
                    $text_title = "Congratulation!";
                    $title = "Congratulation!";
                    $long_message = "<p>👍 You've earned 2 stars and 10 chances for the PS5 redemption draw. Increase your chances by shopping more!</p>";
                    $message = "👍 You've earned 2 stars and 10 chances for the PS5 redemption draw. Increase your chances by shopping more!";


                } else if ($hasMembership) {
                    $button = "Learn More";
                    $text_title = "Congratulation!";
                    $title = "Congratulation!";
                    $long_message = "<p>👍 You've earned 5 chance for the PS5 redemption draw. Increase your chances by shopping more!</p>";
                    $message = "👍 You've earned 5 chance for the PS5 redemption draw. Increase your chances by shopping more!";

                } else {
                    $button = "Learn More";
                    $text_title = "Congratulation!";
                    $title = "Congratulation!";
                    $long_message = "<p>👍 You've earned 1 chance for the PS5 redemption draw. Increase your chances by shopping more!</p>";
                    $message = "👍 You've earned 1 chance for the PS5 redemption draw. Increase your chances by shopping more!";

                }

                $this->PushMay22OneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

            }

//            $this->_logNewprocess('VC_MAY2022_JOB', $the_trans_id, $the_trans_id, '1', '', $mbr_id, '', 'May2022');

            //LAST: Clear Log.
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        } else {
            $this->_logprocess('VC_MAY2022_EMPTY', '0', 'No Transaction | Transaction less than $50', $mbr_id, $mbr_id);
            return $this->apiSuccessResponse(0, 'No Transaction | Transaction less than $50', []);
        }

        return $this->apiSuccessResponse(0, "End", []);
    }

    private function PushMay22OneSignal($mbr_id, $process_id, $title, $message, $button = '', $text_title = '', $text_title_no = '', $short = false, $long_message = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes where process_id= ? and mbr_id = ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //Trigger the push to User.
            $page = '/give-me-five-page';
            $isModal = false;

            if ($long_message) {
                $desc_message = $long_message;
            } else {
                $desc_message = $message;
            }

//            $title = '';

            $datamsg = array(
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "tags" => array(
                    array("relation" => "=", "key" => "mbr_id", "value" => $mbr_id)
                ),
                "data" => array(
                    "title" => $title,
                    "message" => $desc_message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "modal" => $isModal,
                    "feature" => "page",
                    "page" => $page,
                    "pushdata" => 1,
                    "pushlist_id" => 9999,
                    "description" => $message,
                    "short" => $short,
                    "is_inner" => true
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            );

//        	print_r($datamsg);
            $response = $this->_curlOneSignalPushRedis($datamsg, $mbr_id, $process_id);
            print_r($response);
            $this->_logOneSignal(json_encode($response), $mbr_id, $process_id, 'VcApp');

        }
    }

    private function May2022InsertRebate($mbr_id, $process_id, $rebate_amt = 0, $trans_id = '', $trans_date = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and mbr_id = ?
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //No Process Id, Add Rebates.

            $this->_logNewprocess($process_id . '_INIT', $trans_id, '', 'OK',
                json_encode($trans_date), $mbr_id, $mbr_id, 'VcApp');

            $yearMonth = date("Ym");
            $coy_id = $this->CTL_COY_ID;
            $trans_prefix = 'HO_TYPETP' . $yearMonth;

            $inv_id_header = 'TP' . $yearMonth;

            $func = "points/award";
            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);

            $post_data = array(
                "mbr_id" => $mbr_id,
                "trans_id" => $ap_trans_id,
                "rebate_amount" => $rebate_amt,
                "rebate_description" => "ValueClub May 22 Give Me 5 Reward"
            );

            $rebate_res = $this->curlCHPos($func, $post_data);

            //Log Process (Update Rebates)
            $status = 'XOK';
            if (isset($rebate_res["status_code"])) {
                $status = 'OK';
            }
            $this->_logNewprocess($process_id, $trans_id, $ap_trans_id, $status, json_encode($rebate_res), $mbr_id, $mbr_id, 'VcApp');

        } else {
//            echo "<br/>" . 'Rebate Already Added: ' . $process_id;
//            echo "<br/>";
        }
        return;
    }


    public function getCnyScratchCards(Request $request)
    {
        $mbr_id = $request->mbr_id;
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Member Id not found, please try to login again.", []);
        }

        $sql = <<<str
select * from vc_cny_scratch_list where mbr_id = ?
and ((status_level=1) or (status_level= 2 and prize != 0))
order by status_level, created_on desc
str;

        $lists = $this->db20->select($sql, [$mbr_id]);

        return $this->apiSuccessResponse(1, "Processed", $lists);
    }

    public function updateCny2022Rebate(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $card_id = $request->card_id;
        $prize = $request->prize;

        $sql_check = <<<str
select * from vc_cny_scratch_list where mbr_id= ? and id=? and prize= ?
str;
        $item = $this->db20->select($sql_check, [$mbr_id, $card_id, $prize]);
        if ($item) {
            $list = $item[0];
            $status_level = $list->status_level;
            $prize = $list->prize;
            $the_mbr_id = $list->mbr_id;
            $trans_id = $list->trans_id;
            $trans_date = $list->trans_date;

            if ($status_level == 2) {
                $this->_logprocess('VC_CNY2022_ERROR', 'Status= 2 Already Updated.', $card_id, $mbr_id);

            } else if ($status_level == 1) {
                //Update Rebates if not 0.
                if ($prize != 0) {

                    $rebates_process_id = 'CNY2022_REBATE_' . $card_id;
                    //Give Rebates.
                    $this->Cny2022InsertRebate($mbr_id, $rebates_process_id, $prize, $trans_id, $trans_date);

                }

                $sql_upd = <<<str
update vc_cny_scratch_list set status_level=2, updated_on=NOW(), updated_by=? where mbr_id= ? and id= ?
str;
                $this->db20->statement($sql_upd, [$mbr_id, $the_mbr_id, $card_id]);

                $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
            } else {
                $this->_logprocess('VC_CNY2022_ERROR', 'Status= 0 ????', $card_id, $mbr_id);

            }
        } else {
            $this->_logprocess('VC_CNY2022_ERROR', 'Card NOT Found', $card_id . ' - ' . $prize, $mbr_id);

        }


        $sql = <<<str
select * from vc_cny_scratch_list where mbr_id = ?
and ((status_level=1) or (status_level= 2 and prize != 0))
order by status_level, created_on desc
str;

        $lists = $this->db20->select($sql, [$mbr_id]);

        return $this->apiSuccessResponse(1, "OK", $lists);
    }

    public function generateCnyScratchNumber()
    {
        $value = 188;
        $number = 1;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 88;
        $number = 5;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 28;
        $number = 10;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 8.88;
        $number = 50;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 6.88;
        $number = 100;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 2.88;
        $number = 200;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 1.88;
        $number = 800;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 0.88;
        $number = 1000;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 0.68;
        $number = 5000;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 0.28;
        $number = 5000;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        $value = 0;
        $number = 85000;
//        $number = 36845;

        for ($i = 0; $i < $number; $i++) {
            $sql = <<<str
insert into vc_cny_scratch_list (prize, status_level, added_on) VALUES
($value, 0, NOW())
str;
            $this->db20->statement($sql);
        }

        return $this->apiSuccessResponse(1, "Done", []);
    }


    public function VcCny2022Campaign($hachi_pass = '', $mbr_id = '', $isUpdate = '0')
    {
        if ($hachi_pass !== $this->hachiPass || $mbr_id == '') {
            return $this->apiSuccessResponse(0, 'Hachi Pass Incorrect or MBr ID is empty.', []);
        }

        $env = $this->ENVIRONMENT;

        $campaign_start_date = '2022-01-25';
        $campaign_last_date = '2022-02-16';

        //Check Date.
        $dateNow = date("Y-m-d");
        if ($env == 'production' && $dateNow < $campaign_start_date) {
            return $this->apiSuccessResponse(0, "Live Campaign is not Started.", []);
        }
        if ($dateNow >= $campaign_last_date) {
            return $this->apiSuccessResponse(0, "The Campaign has ended.", []);
        }

        //Count More than 200.
        $sql = <<<str
select 1 as count from vc_cny_scratch_list where status_level=0
str;
        $check_count = $this->db20->select($sql);
        if (count($check_count) === 0) {
            $this->_logprocess('VC_CNYS_ENDED', 'ok', '', 'VCAPP', '');
            return $this->apiSuccessResponse(0, "No More Scratch Card. The Campaign has ended.", []);
        }

        //2. Check member type.
        $mbr_sql = <<<str
   select RTRIM(mbr_type) as mbr_type,RTRIM(main_id) as main_id  from crm_member_list where mbr_id= ?
str;
        $member = $this->db20->select($mbr_sql, [$mbr_id]);
        if (count($member) > 0) {
            $mbr_type = $member[0]->mbr_type;
        } else {
            $mbr_type = '';
        }

//    print_r($mbr_type);
        if (!in_array($mbr_type, ['M', 'M18', 'M28', 'MSTP', 'MVIP']) && substr($mbr_type, 0, 1) != 'S') {
            return $this->apiSuccessResponse(0, "Mbr Type is not eligible for Bingo Contest.", []);
        }

//3. Check Today 's Member Transaction.
        $sql = <<<str
    select a.* from (
       select  mbr_id,
              RTRIM(trans.trans_id) as trans_id,
            MAX(loc_id) as loc_id,
            MAX(trans.item_id) as item_id,
              MAX(trans.line_num) as line_num,
              MAX(cast(trans_date as timestamp)) as  trans_date,
              SUM(cast(
                      cast(item_qty as int) * cast(unit_price as decimal(10, 2)) as decimal(10, 2))) as total
       from crm_member_transaction trans
       where  mbr_id= ?
           and cast(trans_date as date)>='2022-01-25'
           and cast(trans_date as date)<='$campaign_last_date'
        and trans.item_qty >= 1
         and trans_type not in ('SR','OS','HX','HR','OD','EX','AP','RD','EP')
         and trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_CNY2022_JOB'
  and mbr_id= ?
           )
        and trans_id not in (
select trans_id from vc_cny_scratch_list where mbr_id= ?
           )
       group by mbr_id,trans_id
   ) as a where a.total >= 88 limit 1
str;
        $transaction = $this->db20->select($sql, [$mbr_id, $mbr_id, $mbr_id]);
//        print_r($transaction);
//        die();

        if (count($transaction) > 0) {
            $the_trans_id = count($transaction) ? $transaction[0]->trans_id : '';
            $the_total = count($transaction) ? $transaction[0]->total : '';
            $the_trans_date = count($transaction) ? $transaction[0]->trans_date : '';

            //Get Scratch Number here.
            $sql = <<<str
select * from vc_cny_scratch_list where status_level=0 and mbr_id is null
str;
            $scratchs = $this->db20->select($sql);
            if (count($scratchs) > 0) {

                $total = count($scratchs);

                $rand_num = rand(0, $total - 1);

                $scratch_card = $scratchs[$rand_num];
                if ($scratch_card && $scratch_card->id) {
                    $card_id = $scratch_card->id;

                    $sql_upd = <<<str
update vc_cny_scratch_list set mbr_id= ?, trans_id= ?, trans_date=?,status_level= 1, created_on= NOW(), created_by= ? where id= ?
str;
                    $this->db20->statement($sql_upd, [$mbr_id, $the_trans_id, $the_trans_date, $mbr_id, $card_id]);

                    $this->_logNewprocess('VC_CNY2022_NO', $the_trans_id, $the_trans_date, $card_id, $the_total, $mbr_id, '', 'VcApp');

                    //Send a Push Notification.
                    $push_process_num_id = 'VC2022_PUSH_NO_' . $card_id;

                    $button = "Learn More";
                    $text_title = "Total worth of V$10,000 to be won!";
                    $title = "Total worth of V$10,000 to be won!";
                    $message = "Thank you for shopping with Challenger, here is your chance card. Wishing you prosperity & good fortune all year!";
                    $long_message = "<p>Thank you for shopping with Challenger, here is your chance card. Wishing you prosperity & good fortune all year!</p>";

                    $this->PushCnyOneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);

                    $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

                } else {
                    $this->_logprocess('VC_CNY2022_ERROR', '0', 'ERROR, CARD NOT FOUND', $mbr_id);
                    return $this->apiSuccessResponse(0, 'ERROR, CARD NOT FOUND.', []);
                }


            } else {
                $this->_logprocess('VC_CNY2022_ERROR', '0', 'ALL Scratch Cards finished. - ' . $dateNow, $mbr_id);
                return $this->apiSuccessResponse(0, 'ALL Scratch Cards finished.', []);
            }

        } else {
            $this->_logprocess('VC_CNY2022_EMPTY', '0', 'No Transaction | Transaction less than $88', $mbr_id, $mbr_id);
            return $this->apiSuccessResponse(0, 'No Transaction | Transaction less than $88', []);
        }

    }

    public function batchCnyDailyReports()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $todayDate = date("d M Y");
        $ytdDate = date('Y-m-d', strtotime("-1 days"));
        $yesterdayDate = date('d M Y', strtotime("-1 days"));

        print_r($todayDate);

        //Get Total number of Scratch Cards generated
        $sql_insert_ytd = <<<str
select * from vc_cny_scratch_list where status_level!=0
str;
        $scratch_lists = $this->db20->select($sql_insert_ytd, []);
        $count_total = count($scratch_lists);

        //Get YEsterday 's Total number of rebates given out
        $sql_ytd = <<<str
select * from vc_cny_scratch_list where status_level!=0 and cast(created_on as date)='$ytdDate'
str;
        $ytd_lists = $this->db20->select($sql_ytd, []);
        $count_ytd = count($ytd_lists);

        $ytd_try = 0;
        $ytd_028 = 0;
        $ytd_068 = 0;
        $ytd_088 = 0;
        $ytd_188 = 0;
        $ytd_288 = 0;
        $ytd_688 = 0;
        $ytd_888 = 0;
        $ytd_1800 = 0;
        $ytd_2800 = 0;
        $ytd_8800 = 0;
        $ytd_18800 = 0;

        $all_try = 0;
        $all_028 = 0;
        $all_068 = 0;
        $all_088 = 0;
        $all_188 = 0;
        $all_288 = 0;
        $all_688 = 0;
        $all_888 = 0;
        $all_1800 = 0;
        $all_2800 = 0;
        $all_8800 = 0;
        $all_18800 = 0;


        foreach ($scratch_lists as $scratch_list) {
            $prize = $scratch_list->prize;
            if ($prize == 0) {
                $all_try++;
            } else if ($prize == 0.28) {
                $all_028++;
            } else if ($prize == 0.68) {
                $all_068++;
            } else if ($prize == 0.88) {
                $all_088++;
            } else if ($prize == 1.88) {
                $all_188++;
            } else if ($prize == 2.88) {
                $all_288++;
            } else if ($prize == 6.88) {
                $all_688++;
            } else if ($prize == 8.88) {
                $all_888++;
            } else if ($prize == 18) {
                $all_1800++;
            } else if ($prize == 28) {
                $all_2800++;
            } else if ($prize == 88) {
                $all_8800++;
            } else if ($prize == 188) {
                $all_18800++;
            }
        }

        foreach ($ytd_lists as $ytd_list) {
            $prize = $ytd_list->prize;
            if ($prize == 0) {
                $ytd_try++;
            } else if ($prize == 0.28) {
                $ytd_028++;
            } else if ($prize == 0.68) {
                $ytd_068++;
            } else if ($prize == 0.88) {
                $ytd_088++;
            } else if ($prize == 1.88) {
                $ytd_188++;
            } else if ($prize == 2.88) {
                $ytd_288++;
            } else if ($prize == 6.88) {
                $ytd_688++;
            } else if ($prize == 8.88) {
                $ytd_888++;
            } else if ($prize == 18) {
                $ytd_1800++;
            } else if ($prize == 28) {
                $ytd_2800++;
            } else if ($prize == 88) {
                $ytd_8800++;
            } else if ($prize == 188) {
                $ytd_18800++;
            }
        }


        $subject = 'CNY Campaign 2022 Report (Date: ' . $todayDate . ') ';

        $body = $subject;

        $body .= "<br/><style>td{border:1px solid black !important;}</style>";

        $body .= "<table style='border: 1px solid black;'>";

        $body .= "<tr><td>Prizes</td><td>No of chance card issued out yesterday</td><td>Total issued</td></tr>" . "<br/>";

        $body .= "<tr><td>Better Luck Next Time</td><td>$count_ytd</td><td>$count_total</td></tr>" . "<br/>";

        $body .= "<tr><td>0.28</td><td>$ytd_028</td><td>$all_028</td></tr>";
        $body .= "<tr><td>0.68</td><td>$ytd_068</td><td>$all_068</td></tr>";
        $body .= "<tr><td>0.88</td><td>$ytd_088</td><td>$all_088</td></tr>";
        $body .= "<tr><td>1.88</td><td>$ytd_188</td><td>$all_188</td></tr>";
        $body .= "<tr><td>2.88</td><td>$ytd_288</td><td>$all_288</td></tr>";
        $body .= "<tr><td>6.88</td><td>$ytd_688</td><td>$all_688</td></tr>";
        $body .= "<tr><td>8.88</td><td>$ytd_888</td><td>$all_888</td></tr>";
        $body .= "<tr><td>18</td><td>$ytd_1800</td><td>$all_1800</td></tr>";
        $body .= "<tr><td>28</td><td>$ytd_2800</td><td>$all_2800</td></tr>";
        $body .= "<tr><td>88</td><td>$ytd_8800</td><td>$all_8800</td></tr>";
        $body .= "<tr><td>188</td><td>$ytd_18800</td><td>$all_18800</td></tr>" . "<br/>";

        $body .= "</table>";

        $body .= "<br/><br/><br/>";

        $body .= "Best Regards,<br/>MIS.<br/><br/>";

        //Make it Dynamic.
        $email_detail = $this->vcmemberRepository->getEmailListings('EMAIL_BINGO20');
//        print_r($email_detail);
//        die();

        $emails = $email_detail["email"];
        $cc = $email_detail["cc"];

        if ($emails == '') {
            return $this->apiSuccessResponse(0, "No Email Recipient.", $email_detail);
        }

        //Hardcoded. Enter relevant emails here.
//		$emails = 'wong.weiseng@challenger.sg';
//		$cc = '';

        $res1 = $this->vcmemberRepository->sendEmailByCtlMailer('ValueClub@challenger.sg', $emails, $body, $subject, $cc);

        return $this->apiSuccessResponse(1, "Sent", $res1);
    }

    public function VcBingoCampaign($hachi_pass = '', $mbr_id = '', $isUpdate = '0')
    {
        if ($hachi_pass !== $this->hachiPass || $mbr_id == '') {
            return $this->apiSuccessResponse(0, 'Hachi Pass Incorrect or MBr ID is empty.', []);
        }

        $env = $this->ENVIRONMENT;
//        echo $env;
//        die();

        $campaign_start_date = '2021-11-02';
        $campaign_last_date = '2021-12-31';

        //Check Date.
        $dateNow = date("Y-m-d");
        if ($env == 'production' && $dateNow < $campaign_start_date) {
            return $this->apiSuccessResponse(0, "Live Campaign is not Started.", []);
        }
        if ($dateNow >= $campaign_last_date) {
            return $this->apiSuccessResponse(0, "The Campaign has ended.", []);
        }

        //Count More than 200.
        $sql = <<<str
select invoice_id from o2o_log_live_processes_new where process_id like 'BINGO_REBATE_%' and invoice_id!=''
str;
        $check_count = $this->db20->select($sql);
        if (count($check_count) >= 200) {
            return $this->apiSuccessResponse(0, "Redeemed more than 200. The Campaign has ended.", []);
        }


        //2. Check member type.
        $mbr_sql = <<<str
   select RTRIM(mbr_type) as mbr_type,RTRIM(main_id) as main_id  from crm_member_list where mbr_id= ?
str;
        $member = $this->db20->select($mbr_sql, [$mbr_id]);
        if (count($member) > 0) {
            $mbr_type = $member[0]->mbr_type;
        } else {
            $mbr_type = '';
        }

//    print_r($mbr_type);
        if (!in_array($mbr_type, ['M', 'M18', 'M28', 'MSTP', 'MVIP']) && substr($mbr_type, 0, 1) != 'S') {
            return $this->apiSuccessResponse(0, "Mbr Type is not eligible for Bingo Contest.", []);
        }

        //3. Get From Table.
        $keyType = '#Bingo#';
        $sql_get = <<<str
	select * from "Members_datatype" where mbr_id= ? and keytype= ?
str;
        $current_result = $this->chapps->select($sql_get, [$mbr_id, $keyType]);
        if (count($current_result) > 0) {
            $cache_result = $this->vcmemberRepository->stdToArray($current_result[0]);
            $result = json_decode($cache_result["meta"], true);
        } else {
            //Default Array.
            $result = $this->getDefaultArray();
        }

//        print_r($result);
//        die();

        //3. Check Today 's Member Transaction.
        $sql = <<<str
    select a.* from (
       select  mbr_id,
              RTRIM(trans.trans_id) as trans_id,
            MAX(loc_id) as loc_id,
            MAX(trans.item_id) as item_id,
              MAX(trans.line_num) as line_num,
              MAX(cast(trans_date as date)) as                                                                        trans_date,
              SUM(cast(
                      cast(item_qty as int) * cast(unit_price as decimal(10, 2)) as decimal(10, 2))) as total
       from crm_member_transaction trans
       where  mbr_id= ?
           and cast(trans_date as date)>='2021-12-01'
           and cast(trans_date as date)<='$campaign_last_date'
        and trans.item_qty >= 1
         and trans_type not in ('SR','OS','HX','HR','OD','EX')
         and trans_id not in (
select trans_id from o2o_log_live_processes_new where process_id='VC_BINGO_NO'
  and mbr_id= ?
           )
       group by mbr_id,trans_id
   ) as a where a.total >= 50 limit 1
str;
        $transaction = $this->db20->select($sql, [$mbr_id, $mbr_id]);
//        print_r($transaction);
//        die();

        if (count($transaction) > 0 || $isUpdate) {
            $the_trans_id = count($transaction) ? $transaction[0]->trans_id : '';
            $the_total = count($transaction) ? $transaction[0]->total : '';
            $the_trans_date = count($transaction) ? $transaction[0]->trans_date : '';

            //Check Whether User purchase today. From o2o_log_processes_new
            $sql_log = <<<str
        select status, remarks, trans_id from o2o_log_live_processes_new where process_id='VC_BINGO_NO'
and mbr_id= ?  and invoice_id='$dateNow'
str;
            $logs = $this->db20->select($sql_log, [$mbr_id]);
            if (count($logs) === 0 || $isUpdate) {

                //Get Customer 's Board Number
                $board_lists = $this->_generatBoardNum($mbr_id);

                $num_arrays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];

                //Get All Numbers That Get Before.
                $sql_old = <<<str
 select * from o2o_log_live_processes_new where process_id='VC_BINGO_NO'
and mbr_id= ?  
str;
                //check db for number and make it into an array
                $numbers = $this->db20->select($sql_old, [$mbr_id]);
                $collected_nums = [];
                foreach ($numbers as $number) {
                    $old_number = (int)$number->status;
                    $collected_nums[] = $old_number;

                    $num_index = array_search($old_number, $num_arrays);
                    unset($num_arrays[$num_index]);
                }
                $num_arrays = array_values($num_arrays);
//                print_r($num_arrays);
//                print_r(count($num_arrays));

                //Generate a Rand Number between 0 to count(array).
                if (count($num_arrays) > 0) {
                    $new_index = rand(0, count($num_arrays) - 1);
                    $new_number = $num_arrays[$new_index];
                } else {
                    $new_number = -1;
                }

//                echo $new_number;
//                die();

                //Insert o2o_log_live_processes_new.
                if (count($transaction) > 0 && $new_number) {
                    $this->_logNewprocess('VC_BINGO_NO', $the_trans_id, $the_trans_date, $new_number, $the_total, $mbr_id, '', 'VcApp');
                    $collected_nums[] = $new_number;

                    //Send a Push Notification.
                    $push_process_num_id = 'BINGO_PUSH_NO_' . $new_number;

                    $button = "Learn More";
                    $text_title = "Congratulations!";
                    $title = "<img src='https://ctl-public-test.s3-ap-southeast-1.amazonaws.com/app/valueclub20/ios/assets/img/icon-congrats.png' /><br/> Congratulations!";
                    $message = "You 've unlocked $new_number Number Roll(s)";
                    $long_message = "<p>You 've unlocked <span class='bingo-win-number'>$new_number</span> Number Roll(s)</p>";

                    $this->PushBingoOneSignal($mbr_id, $push_process_num_id, $title, $message, $button, $text_title, 'Okay', true, $long_message);


                }

//                die();

                //For Testing.
//                $collected_nums[] = 16;
//                $collected_nums[] = 10;
//                $collected_nums[] = 18;
//                $collected_nums[] = 9;
//                $collected_nums[] = 22;

                sort($collected_nums);

                //Check For the Rows.
//                print_r($collected_nums);
//                die();
                $result["boards"] = $board_lists;
                $result["nums"] = $collected_nums;
                $genReturn = $this->_generatSuccessRow($collected_nums, $board_lists);

                $result["success"] = $genReturn["row"];
                $result["completed"] = $genReturn["count"];

                if ($result["completed"] == 12) {
                    $this->_insertCompletedRow($mbr_id);
                }

//                print_r($result);
//                die();

                //Send Push Notification.
                if (count($result["nums"]) >= 5) {
                    $earn_rebate = 28;
                    $button = "Learn More";
                    $text_title = "Congratulations!";
                    $title = "<img src='https://ctl-public-test.s3-ap-southeast-1.amazonaws.com/app/valueclub20/ios/assets/img/icon-congrats.png' /><br/> Congratulations!";
                    $message = "Congratulations! For successfully forming a line, you’ve won V$28";
                    $long_message = "<p>Congratulations! For successfully forming a line, you’ve won V$28</p>";


                    if ($result["success"]["row1"] === true) {
                        $push_process_id = 'BINGO_PUSH_ROW1';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_ROW1';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);

                    }
                    if ($result["success"]["row2"] === true) {
                        $push_process_id = 'BINGO_PUSH_ROW2';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_ROW2';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["row3"] === true) {
                        $push_process_id = 'BINGO_PUSH_ROW3';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_ROW3';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["row4"] === true) {
                        $push_process_id = 'BINGO_PUSH_ROW4';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_ROW4';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["row5"] === true) {
                        $push_process_id = 'BINGO_PUSH_ROW5';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_ROW5';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["col1"] === true) {
                        $push_process_id = 'BINGO_PUSH_COL1';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_COL1';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["col2"] === true) {
                        $push_process_id = 'BINGO_PUSH_COL2';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_COL2';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["col3"] === true) {
                        $push_process_id = 'BINGO_PUSH_COL3';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_COL3';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["col4"] === true) {
                        $push_process_id = 'BINGO_PUSH_COL4';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_COL4';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["col5"] === true) {
                        $push_process_id = 'BINGO_PUSH_COL5';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_COL5';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["dia1"] === true) {
                        $push_process_id = 'BINGO_PUSH_DIA1';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_DIA1';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                    if ($result["success"]["dia2"] === true) {
                        $push_process_id = 'BINGO_PUSH_DIA2';

                        $this->PushBingoOneSignal($mbr_id, $push_process_id, $title, $message, $button, $text_title, 'Okay', true);

                        $rebates_process_id = 'BINGO_REBATE_DIA2';
                        //Give Rebates.
                        $this->BingoInsertRebate($mbr_id, $rebates_process_id, $earn_rebate, $the_trans_id, $the_trans_date);


                    }
                }


                $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
                $keyType = '#Bingo#';

                $query = <<<str
        INSERT INTO "Members_datatype" (mbr_id, meta , keytype) 
        VALUES (?, ? ,?)
        ON CONFLICT (mbr_id,keytype) DO UPDATE 
          SET meta = ? 
str;
                $this->chapps->statement($query, [$mbr_id, json_encode($result), $keyType, json_encode($result)]);


            } else {
                $number = $logs[0]->status;
                $this->_logprocess('VC_BINGO_ERROR', '0', 'Already got Number' . '-' . $number . ' - ' . $dateNow, $mbr_id);

                return $this->apiSuccessResponse(0, 'Already Got Number Today.', $logs);
            }

        } else {
            $this->_logprocess('VC_BINGO_ERROR', '0', 'No Transaction.', $mbr_id);
            return $this->apiSuccessResponse(0, 'No Transaction Today.', []);
        }


        $resp = array(
            "trans_id" => $the_trans_id,
            "date" => $the_trans_date,
            "number" => $new_number
        );
        return $this->apiSuccessResponse(1, 'Finish', $resp);
    }

    private function _insertCompletedRow($mbr_id)
    {
        $dateNow = date("Y-m-d");
        $process_id = 'VC_BINGO_COMPLETE';
        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and mbr_id = ?
str;
        $success_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($success_log) === 0) {
            $this->_logNewprocess($process_id, '', $dateNow, 'OK', '', $mbr_id, '', '');
        }
    }

    private function Cny2022InsertRebate($mbr_id, $process_id, $rebate_amt = 0, $trans_id = '', $trans_date = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and mbr_id = ?
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //No Process Id, Add Rebates.

            $this->_logNewprocess($process_id . '_INIT', $trans_id, '', 'OK',
                json_encode($trans_date), $mbr_id, $mbr_id, 'VcApp');

            $yearMonth = date("Ym");
            $coy_id = $this->CTL_COY_ID;
            $trans_prefix = 'HO_TYPETP' . $yearMonth;

            $inv_id_header = 'TP' . $yearMonth;

            $func = "points/award";
            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);

            $post_data = array(
                "mbr_id" => $mbr_id,
                "trans_id" => $ap_trans_id,
                "rebate_amount" => $rebate_amt,
                "rebate_description" => "ValueClub CNY Card Scratch Reward"
            );

            $rebate_res = $this->curlCHPos($func, $post_data);

            //Log Process (Update Rebates)
            $status = 'XOK';
            if (isset($rebate_res["status_code"])) {
                $status = 'OK';
            }
            $this->_logNewprocess($process_id, $trans_id, $ap_trans_id, $status, json_encode($rebate_res), $mbr_id, $mbr_id, 'VcApp');

        } else {
//            echo "<br/>" . 'Rebate Already Added: ' . $process_id;
//            echo "<br/>";
        }
        return;
    }

    private function BingoInsertRebate($mbr_id, $process_id, $rebate_amt = 28, $trans_id = '', $trans_date = '')
    {

        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and mbr_id = ?
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //No Process Id, Add Rebates.

            $this->_logNewprocess($process_id . '_INIT', $trans_id, '', 'OK',
                json_encode($trans_date), $mbr_id, $mbr_id, 'VcApp');

            $yearMonth = date("Ym");
            $coy_id = $this->CTL_COY_ID;
            $trans_prefix = 'HO_TYPETP' . $yearMonth;

            $inv_id_header = 'TP' . $yearMonth;

            $func = "points/award";
            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'SMS', $coy_id);

            $post_data = array(
                "mbr_id" => $mbr_id,
                "trans_id" => $ap_trans_id,
                "rebate_amount" => $rebate_amt,
                "rebate_description" => "ValueClub Bingo Row Reward"
            );

            $rebate_res = $this->curlCHPos($func, $post_data);

            //Log Process (Update Rebates)
            $status = 'XOK';
            if (isset($rebate_res["status_code"])) {
                $status = 'OK';
            }
            $this->_logNewprocess($process_id, $trans_id, $ap_trans_id, $status, json_encode($rebate_res), $mbr_id, $mbr_id, 'VcApp');


        } else {
//            echo "<br/>" . 'Rebate Already Added: ' . $process_id;
            echo "<br/>";
        }
        return;
    }

    private function _generatBoardNum($mbr_id)
    {
        $process_id = 'VC_BINGO_BOARD';
        $sql_check = <<<str
	select * from o2o_log_live_processes where process_id= ? and mbr_id = ? and status= '1' 
str;
        $board_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($board_log) > 0) {
            $remarks = ($board_log[0] && isset($board_log[0]->remarks)) ? json_decode($board_log[0]->remarks) : [];
        } else {
            $remarks = [];
        }

        if (count($remarks) === 0) {
            $num_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
            shuffle($num_array);

            $this->_logprocess($process_id, '1', json_encode($num_array), $mbr_id, 'VcBingo');
            return $num_array;
        } else {
            return $remarks;
        }
    }

    private function PushCnyOneSignal($mbr_id, $process_id, $title, $message, $button = '', $text_title = '', $text_title_no = '', $short = false, $long_message = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes where process_id= ? and mbr_id = ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //Trigger the push to User.
            $page = '/cny-scratch-page';
            $isModal = true;

            if ($long_message) {
                $desc_message = $long_message;
            } else {
                $desc_message = $message;
            }

            $title = '';

            $datamsg = array(
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "tags" => array(
                    array("relation" => "=", "key" => "mbr_id", "value" => $mbr_id)
                ),
                "data" => array(
                    "title" => $title,
                    "message" => $message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "modal" => $isModal,
                    "feature" => "page",
                    "page" => $page,
                    "pushdata" => 1,
                    "pushlist_id" => 9999,
                    "description" => $desc_message,
                    "short" => $short
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            );

//        	print_r($datamsg);
            $response = $this->_curlOneSignalPushRedis($datamsg, $mbr_id, $process_id);
            print_r($response);
            $this->_logOneSignal(json_encode($response), $mbr_id, $process_id, 'VcApp');
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        } else {
//            echo "Already Sent: " . $process_id . "<br/>";
            echo "<br/>";
        }

    }


    private function PushBingoOneSignal($mbr_id, $process_id, $title, $message, $button = '', $text_title = '', $text_title_no = '', $short = false, $long_message = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes where process_id= ? and mbr_id = ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {
            //Trigger the push to User.
            $page = 'BingoPage';
            $isModal = true;

            if ($long_message) {
                $desc_message = $long_message;
            } else {
                $desc_message = $message;
            }

            $title = '';

            $datamsg = array(
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "tags" => array(
                    array("relation" => "=", "key" => "mbr_id", "value" => $mbr_id)
                ),
                "data" => array(
                    "title" => $title,
                    "message" => $message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "modal" => $isModal,
                    "feature" => "bingo",
                    "page" => $page,
                    "pushdata" => 1,
                    "pushlist_id" => 9999,
                    "description" => $desc_message,
                    "short" => $short
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            );

//        	print_r($datamsg);
            $response = $this->_curlOneSignalPushRedis($datamsg, $mbr_id, $process_id);
            print_r($response);
            $this->_logOneSignal(json_encode($response), $mbr_id, $process_id, 'VcApp');
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        } else {
//            echo "Already Sent: " . $process_id . "<br/>";
            echo "<br/>";
        }

    }


    private function _generatSuccessRow($collected_rows, $board_lists)
    {

        $row_count = array(
            "row1" => false,
            "row2" => false,
            "row3" => false,
            "row4" => false,
            "row5" => false,
            "col1" => false,
            "col2" => false,
            "col3" => false,
            "col4" => false,
            "col5" => false,
            "dia1" => false,
            "dia2" => false,
        );
        $count = 0;

        if (in_array($board_lists[0], $collected_rows) && in_array($board_lists[1], $collected_rows) && in_array($board_lists[2], $collected_rows) && in_array($board_lists[3], $collected_rows) && in_array($board_lists[4], $collected_rows)) {
            $row_count["row1"] = true;
            $count++;
        }
        if (in_array($board_lists[5], $collected_rows) && in_array($board_lists[6], $collected_rows) && in_array($board_lists[7], $collected_rows) && in_array($board_lists[8], $collected_rows) && in_array($board_lists[9], $collected_rows)) {
            $row_count["row2"] = true;
            $count++;
        }
        if (in_array($board_lists[10], $collected_rows) && in_array($board_lists[11], $collected_rows) && in_array($board_lists[12], $collected_rows) && in_array($board_lists[13], $collected_rows) && in_array($board_lists[14], $collected_rows)) {
            $row_count["row3"] = true;
            $count++;
        }
        if (in_array($board_lists[15], $collected_rows) && in_array($board_lists[16], $collected_rows) && in_array($board_lists[17], $collected_rows) && in_array($board_lists[18], $collected_rows) && in_array($board_lists[19], $collected_rows)) {
            $row_count["row4"] = true;
            $count++;
        }
        if (in_array($board_lists[20], $collected_rows) && in_array($board_lists[21], $collected_rows) && in_array($board_lists[22], $collected_rows) && in_array($board_lists[23], $collected_rows) && in_array($board_lists[24], $collected_rows)) {
            $row_count["row5"] = true;
            $count++;
        }

        if (in_array($board_lists[0], $collected_rows) && in_array($board_lists[5], $collected_rows) && in_array($board_lists[10], $collected_rows) && in_array($board_lists[15], $collected_rows) && in_array($board_lists[20], $collected_rows)) {
            $row_count["col1"] = true;
            $count++;
        }
        if (in_array($board_lists[1], $collected_rows) && in_array($board_lists[6], $collected_rows) && in_array($board_lists[11], $collected_rows) && in_array($board_lists[16], $collected_rows) && in_array($board_lists[21], $collected_rows)) {
            $row_count["col2"] = true;
            $count++;
        }
        if (in_array($board_lists[2], $collected_rows) && in_array($board_lists[7], $collected_rows) && in_array($board_lists[12], $collected_rows) && in_array($board_lists[17], $collected_rows) && in_array($board_lists[22], $collected_rows)) {
            $row_count["col3"] = true;
            $count++;
        }
        if (in_array($board_lists[3], $collected_rows) && in_array($board_lists[8], $collected_rows) && in_array($board_lists[13], $collected_rows) && in_array($board_lists[18], $collected_rows) && in_array($board_lists[23], $collected_rows)) {
            $row_count["col4"] = true;
            $count++;
        }
        if (in_array($board_lists[4], $collected_rows) && in_array($board_lists[9], $collected_rows) && in_array($board_lists[14], $collected_rows) && in_array($board_lists[19], $collected_rows) && in_array($board_lists[24], $collected_rows)) {
            $row_count["col5"] = true;
            $count++;
        }

        if (in_array($board_lists[0], $collected_rows) && in_array($board_lists[6], $collected_rows) && in_array($board_lists[12], $collected_rows) && in_array($board_lists[18], $collected_rows) && in_array($board_lists[24], $collected_rows)) {
            $row_count["dia1"] = true;
            $count++;
        }
        if (in_array($board_lists[4], $collected_rows) && in_array($board_lists[8], $collected_rows) && in_array($board_lists[12], $collected_rows) && in_array($board_lists[16], $collected_rows) && in_array($board_lists[20], $collected_rows)) {
            $row_count["dia2"] = true;
            $count++;
        }

        $resp = array();
        $resp["count"] = $count;
        $resp["row"] = $row_count;
        return $resp;
    }

    private function getDefaultArray()
    {
        $result = array();
        $result["nums"] = [];
        $result["boards"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
        $result["success"] = array(
            "row1" => false,
            "row2" => false,
            "row3" => false,
            "row4" => false,
            "row5" => false,
            "col1" => false,
            "col2" => false,
            "col3" => false,
            "col4" => false,
            "col5" => false,
            "dia1" => false,
            "dia2" => false,
        );
        $result["completed"] = 0;

        return $result;
    }

    public function getGamificationLists(Request $request)
    {
        $mbr_id = $request->mbr_id;

//        $keyType = '#Gamification20#';
//        //Get Current Result From Postgre If Exists.
//        $sql_get = <<<str
//	select * from "Members_datatype" where mbr_id= ? and keytype= ?
//str;
//        $current_result = $this->chapps->query($sql_get, [$mbr_id, $keyType])->row_array();
//        if ($current_result) {
//            $result = json_decode($current_result["meta"], true);
//        } else {
//            //Generate Array.
//            $headers = array(
//                'Content-Type: application/json',
//                'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'
//            );
//            $ch = curl_init();
//
//            curl_setopt($ch, CURLOPT_URL, 'https://' . $this->server_name . '/api/Vc2/VcGamificationCampaign');
//            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("mbr_id" => $mbr_id)));
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//            $response = curl_exec($ch);
//            curl_close($ch);
//
//            $current_result = $this->chapps->query($sql_get, [$mbr_id, $keyType])->row_array();
//            if ($current_result) {
//                $result = json_decode($current_result["meta"], true);
//            } else {
//                $result = $this->getDefaultArray();
//            }
//        }

        $resp["result"] = null;

        $imageUrl = "https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/app/valueclub20/ios/assets/";
        $resp["message"] = [
            array(
                "title" => "The Timekeeper",
                "title2" => "The<br/>Timekeeper",
                "desc" => "You're connected with everything around you.",
                "desc2" => "Purchase any Valore Smart Watch and Fitness Tracker.",
                "img" => $imageUrl . "img/icon-stub-1timekeeper.png"
            ),
            array(
                "title" => "The Illuminator",
                "title2" => "The<br/>Illuminator",
                "desc" => "You light up any room you walk into.",
                "desc2" => "Purchase any Valore Table Lamp.",
                "img" => $imageUrl . "img/icon-stub-2illuminator.png"
            ),
            array(
                "title" => "Gaming Gadgeteer",
                "title2" => "Gaming<br/>Gadgeteer",
                "desc" => "You dominate the playing field.",
                "desc2" => "Purchase any Valore Input Device.",
                "img" => $imageUrl . "img/icon-stub-3gaming.png"
            ),
            array(
                "title" => "Domestic Engineer",
                "title2" => "Domestic<br/>Engineer",
                "desc" => "You engineer your household to perfection.",
                "desc2" => "Purchase any Valore Air Purifier, Clock, Scale, Extension Cable or Wine Acessory.",
                "img" => $imageUrl . "img/icon-stub-4domestic.png"
            ),
            array(
                "title" => "The Powerhouse",
                "title2" => "The<br/>Powerhouse",
                "desc" => "You have great drive and influence everyone around you.",
                "desc2" => "Purchase any Valore Powerbank.",
                "img" => $imageUrl . "img/icon-stub-5powerhouse.png"
            ),
            array(
                "title" => "The Fixer Upper",
                "title2" => "The Fixer<br/>Upper",
                "desc" => "You are the true handyman and can fix almost everything.",
                "desc2" => "Purchase any ValueClub Exclusive/PLG Tools.",
                "img" => $imageUrl . "img/icon-stub-6fixer.png"
            ),
            array(
                "title" => "Seamlessly Sound",
                "title2" => "Seamlessly<br/>Sound",
                "desc" => "You are perfectly in sync with all that is around you.",
                "desc2" => "Purchase any ValueClub Wireless Earbuds.",
                "img" => $imageUrl . "img/icon-stub-7seamlessly.png"
            ),
            array(
                "title" => "The Hodophile",
                "title2" => "The<br/>Hodophile",
                "desc" => "You travel the world in luxury and comfort.",
                "desc2" => "Purchase any ValueClub Travel Accessory, Umbrella, USB Fan or Selfie Stick.",
                "img" => $imageUrl . "img/icon-stub-8hodophile.png"
            ),
            array(
                "title" => "Value Tycoon",
                "title2" => "Value<br/>Tycoon",
                "desc" => "You understand the power that lies in value.",
                "desc2" => "Spend a min. $100 on Valore, ValueClub Exclusive, and/or PLG products in a single receipt.",
                "img" => $imageUrl . "img/icon-stub-9valuetycoon.png"
            )
        ];

        return $this->apiSuccessResponse(1, "OK", $resp);
    }


    public function UpdateEmailDetail(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        //NO Need Update Anymore
        return $this->apiSuccessResponse(0, "Failed to update.", []);
        die();

//        $postdata = array();
//        $postdata["mbr_id"] = $mbr_id;
//        $postdata["email_addr"] = $request->email_addr;
//        $postdata["contact_no"] = $request->contact_no;
//
//        $token_sql = <<<str
//    SELECT set_desc, remarks FROM b2b_setting_list
//WHERE sys_id='API'
//AND set_code='CTLVALUECLUB_VERIFY' AND set_default='LIVE'
//str;
//        $token_query = $this->db->query($token_sql)->row_array();
//        if (!$token_query) {
//            return $this->apiSuccessResponse(0, "Token not Found", []);
//        }
//        $header_token = $token_query["remarks"];
//        $headers = array(
//            'X_AUTHORIZATION: ' . $header_token,
//            'Content-Type: application/json'
//        );
//
//        if ($this->ENVIRONMENT == "development") {
//            $url = 'https://chvoices-test.challenger.sg/api/vcmember/verify/' . $mbr_id;
//        } else {
//            $url = 'https://chvoices.challenger.sg/api/vcmember/verify/' . $mbr_id;
//        }
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//        $result = curl_exec($ch);
//        curl_close($ch);
//        if ($result) {
//            $response = json_decode($result, true);
//
//            if ($response["code"] == "2") {
//                return $this->apiSuccessResponse(1, "Success", $response);
//            } else if ($response["code"] == "1") {
//                return $this->apiSuccessResponse(1, $response["msg"], $response);
//            } else if ($response["code"] == "-1") {
//                return $this->apiSuccessResponse(0, $response["msg"], $response["msgparts"]);
//            }
//        }
//        return $this->apiSuccessResponse(0, "Failed to update.", $result);
    }

    private function _hachi20ApiCurl($url, $post_data, $has_header = true, $is_post = true, $return_data = true)
    {
        $headers = array(
            'Content-Type: application/json'
        );

        if ($has_header) {
            $headers[] = 'X_AUTHORIZATION: ' . $this->X_authorization;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($is_post) {
            curl_setopt($ch, CURLOPT_POST, true);
        } else {
            curl_setopt($ch, CURLOPT_POST, false);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($return_data) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        } else {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($is_post && $post_data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }

        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($result, true);
        if (!$return_data) {
            return true;
        }
        return $response;
    }

    public function getMemberPhoto(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $staff_id = $request->staff_id;

        $post_field = array(
            "mbr_id" => $mbr_id,
            "staffcode" => "NA",
            "func" => "getValueclubProfilephoto",
            "image" => []
        );

        $url = "https://kxrimfra0k.execute-api.ap-northeast-1.amazonaws.com/faceapi/faceapi";
        $headers = array(
            'x-api-key: RcvbIpBxDA5SwJX4N2nloa64aO15JCTdaCVJtHeq',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_field));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);

        //Get FaceId from Log.
        $result["face_id"] = $this->vcmemberRepository->getFaceIdFromLog($mbr_id);

        $this->_logprocess('VC_STAFF_GET_PHOTO', $result["code"], $result["face_id"], $mbr_id);

//        $result["msg"]= 'data:image/jpeg;base64,' .$result["msg"];
//        $img= 'data:image/jpeg;base64,' .$result["msg"];
//        $html=<<<str
//<img src="$img" />
//str;
//        echo $html;
//


        return $this->apiSuccessResponse(1, "Success", $result);

    }


    public function removeMemberPhoto(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $face_id = $request->face_id ? $request->face_id : '';

        if ($face_id == '') {
            //Get FaceId IF No Pass FaceID.
            $face_id = $this->vcmemberRepository->getFaceIdFromLog($mbr_id);

            $this->_logprocess('VC_STAFF_GET_ID', 'ok', $face_id, $mbr_id);
        }

        $post_fields = array(
            "staffcode" => "NA",
            "func" => "deleteValueclubFaceId",
            "collectionId" => "valueclub",
            "image" => [],
            "faces" => [$face_id]
        );

//        print_r($post_fields);

        $url = "https://kxrimfra0k.execute-api.ap-northeast-1.amazonaws.com/faceapi/faceapi";
        $headers = array(
            'x-api-key: RcvbIpBxDA5SwJX4N2nloa64aO15JCTdaCVJtHeq',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
        $result = curl_exec($ch);

//        print_r($result);

        curl_close($ch);
        $result = json_decode($result, TRUE);


        if ($result && isset($result["message"])) {
            return $this->apiSuccessResponse(0, $result["message"], $result);
        } else if ($result && isset($result["code"])) {
            $this->_logprocess('VC_STAFF_RM_PHOTO', $result["code"], $face_id, $mbr_id);
        }

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function uploadMemberPhoto(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $staff_id = $request->staff_id;
        $imgdata = $request->image;

        $post_fields = array(
            "mbr_id" => $mbr_id,
            "staffcode" => "NA",
            "func" => "registerValueclub",
            "force" => 1,
            "image" => [$imgdata],
        );

//        print_r($post_fields);


        $url = "https://kxrimfra0k.execute-api.ap-northeast-1.amazonaws.com/faceapi/faceapi";
        $headers = array(
            'x-api-key: RcvbIpBxDA5SwJX4N2nloa64aO15JCTdaCVJtHeq',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
        $response = curl_exec($ch);

//        print_r($response);
//        die();

        curl_close($ch);
        $result = json_decode($response, TRUE);

        if ($result && isset($result["message"])) {
            return $this->apiSuccessResponse(0, $result["message"], $result);
        } else if ($result && isset($result["code"])) {
            $this->_logprocess('VC_STAFF_ADD_PHOTO', $result["code"], $result["faceid"], $mbr_id);
        }

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function getAssociateMbr(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $lists = array();
        $results = $this->vcmemberRepository->getAssociateMbrList($mbr_id);
        for ($i = 0; $i < $this->associate_mbr_limit; $i++) {
            if ($results && isset($results[$i])) {
                array_push($lists, $results[$i]);
            } else {
                $data = array(
                    "email_addr" => '',
                    "full_name" => '',
                    "mbr_id" => '',
                    "status" => -1
                );
                array_push($lists, $data);
            }
        }

        return $this->apiSuccessResponse(1, "Success", $lists);

    }

    public function sendAssociateMbr(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $email = strtolower($request->email);
        $main_id = $mbr_id;

        $validateExp = $this->vcmemberRepository->checkMemberExpiry($main_id);
        if ($validateExp === false) {
            return $this->apiSuccessResponse(0, "Associate membership not applicable for expired ValueClub membership.", []);
        }

        $member = $this->vcmemberRepository->getById($main_id);
        $mbr_type = $member->mbr_type;

        //Staff is not allow to use this feature.
        if (substr($mbr_type, 0, 1) == 'S') {
            return $this->apiSuccessResponse(0, "Staff membership is unable to access this feature.", []);
        }

        if (substr($mbr_type, 0, 3) == "MST" || $mbr_type == "MNS") {
            return $this->apiSuccessResponse(0, "Associate membership not applicable for complimentary ValueClub membership.", []);
        }

        $checkEmail = $this->vcmemberRepository->isAssoEmailExist($email);
        if ($checkEmail) {
            if (!$checkEmail->status_level && $checkEmail->main_id) {
                return $this->apiSuccessResponse(0, "Pending associate member activation for this email (" . trim($email) . ").", []);
            } else {
                return $this->apiSuccessResponse(0, "This email exists in our system.", []);
            }
        }
        $checks = $this->vcmemberRepository->getAssociateMbrList($main_id);
        if (count($checks) >= $this->associate_mbr_limit) {
            return $this->apiSuccessResponse(0, "Sorry. Associate Members cannot more than " . $this->associate_mbr_limit, []);
        }

        //Get Main User
        $coy_id = 'CTL';

        $user = $this->vcmemberRepository->getAssoMainId($main_id, $coy_id);
        if (!$user) {
            return $this->apiSuccessResponse(0, "User is not found.", []);
        }

        //Generate Associate Mbr
        $pass1 = '@hachi!#@' . '123456';

        $insert_data['mbr_id'] = $this->vcmemberRepository->generateNewMbrId('AS');
        $mbr_id = $insert_data['mbr_id'];

        $insert_data['mbr_type'] = $this->member_type_list['ASSOCIATE'];
        $insert_data['status_level'] = 0;
        $insert_data['email_addr'] = $email;
        $insert_data['first_name'] = '';  // Hardcoded first
        $insert_data['last_name'] = ''; // Hardcoded first
        $insert_data['contact_num'] = '00000000'; // Hardcoded first
        $insert_data['birth_date'] = '1900-01-01';
        $insert_data['main_id'] = $user->mbr_id;
        $insert_data['exp_date'] = $user->exp_date;
        $insert_data['mbr_pwd'] = md5($pass1);
        $insert_data['login_locked'] = 'N';

        //Insert Log
        $remarks = json_encode($insert_data);
        $this->_logprocess('ASSONEW', 'OK', $remarks, $mbr_id, 'VcApp');

        if ($this->vcmemberRepository->addAccount($insert_data)) {
            //Send Email.
            $code = $this->_sendAssociateMemberEmail($mbr_id, $main_id, $email, $user->full_name);

            //Clear Redis
            $asso_expdate = date('Y-m-d 00:00:00', strtotime('+1 day', strtotime($insert_data['exp_date'])));
            $this->vcmemberRepository->insertMemberType($mbr_id, $main_id, $asso_expdate, $this->member_type_list['ASSOCIATE']);

            //Return Lists.
            $lists = array();
            $results = $this->vcmemberRepository->getAssociateMbrList($main_id);
            for ($i = 0; $i < $this->associate_mbr_limit; $i++) {
                if ($results && isset($results[$i])) {
                    array_push($lists, $results[$i]);
                } else {
                    $data = array(
                        "email_addr" => '',
                        "full_name" => '',
                        "mbr_id" => '',
                        "status" => -1
                    );
                    array_push($lists, $data);
                }
            }

            return $this->apiSuccessResponse(1, "OK", $lists);
        } else {
            return $this->apiSuccessResponse(0, 'Cannot register account registration failed, due to db error', []);
        }
    }


    private function _sendAssociateMemberEmail($mbr_id, $main_id, $email, $main_name)
    {
        $mbr_id = trim($mbr_id);

        $edata = array(
            "e" => $email,
            "m" => $mbr_id,
            "u" => $main_id,
            "f" => $main_name
        );
        $token = $this->createToken($edata, 25920000, "assomembership" . $mbr_id); // 72 hours //2020208 become 720 hours.
        $edata["t"] = $token;

        $edata = base64_encode(json_encode($edata));
        $url = $this->server_name . '/api/vc2/' . 'signup_associate/' . $edata;

        $this->_logprocess('Email_AssoMbr_Trigger', '-', $email . '-' . $main_name, $mbr_id, $main_id);


        $email_data = [
            "r_appurl" => $url,
            'r_mainmember' => $main_name
        ];
        $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["ASSOCIATE_INVITE"], $email, $email_data);

        return $url;
    }

    public function cancelAssoInvite(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $email = $request->email;
        $asso_id = $request->asso_id;

        //-- Validate if active or not active
        $data = $this->db20->table('crm_member_list')
            ->select('mbr_id')
            ->where([
                'email_addr' => $email,
                'status_level' => 0
            ])->first();

        if (!$data) {
            return $this->apiSuccessResponse(0, "Account has been generated.", []);
        }

//        //-- Remove the existing associate member invited
        DB::beginTransaction();
        try {

            $this->db20->table('crm_member_list')
                ->where([
                    'email_addr' => $email,
                    'status_level' => 0
                ])->delete();

            $this->db20->table('crm_member_type')
                ->where([
                    'mbr_id' => $asso_id
                ])->delete();

            $this->db20->table('o2o_log_live_processes_new')
                ->where([
                    'process_id' => 'ASSO_CODE',
                    'status' => '0',
                    'email_addr' => $email,
                    'trans_id' => $mbr_id,
                    'mbr_id' => $asso_id
                ])->update([
                    'status' => '-1',
                    'modified_by' => $mbr_id,
                    'modified_on' => Carbon::now()
                ]);

            DB::commit();

            return $this->apiSuccessResponse(1, "Successfully cancelled", []);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->apiSuccessResponse(0, "Request failed, please try again.", []);
        }

    }

    public function manualIssueVoucher($mbr_id)
    {
        $this->_issueSignUpVoucher($mbr_id, $this->member_type_list["STUDENT"], '', 'OS');

    }

    public function signupAssociateMbr(Request $request)
    {
        $email = $request->email;
        $asso_id = $request->asso_id;
        $mbr_id = $request->main_id;

        $last_name = $request->lastname;
        $first_name = $request->firstname;
        $contact_no = $request->contact;
        $dob = $request->dob;
        $password = $request->pass1;
        $confirm_pass = $request->pass2;

        $asso_code = $request->asso_code ? $request->asso_code : '';

        $validates = [$last_name, $first_name, $contact_no, $dob, $password, $confirm_pass];
        $msg = array();
        foreach ($validates as $validate) {
            if (!$validate) {
                array_push($msg, "Missing Info");
                return $this->apiSuccessResponse(0, $msg, $this->messages["SIGNUP_INCOMPLETE"]);
            }
        }

        if (strlen(trim($contact_no)) < 8) {
            array_push($msg, "Contact Minimum");
        } else if ($this->vcmemberRepository->isMemberContactExist(trim($contact_no))) {
            array_push($msg, "Contact Exist");
        } else if (!$this->vcmemberRepository->isContactNum($contact_no)) {
            array_push($msg, "Contact Invalid");
        }

        //-- compare password1 and password1 if thesame
        if ($password !== $confirm_pass) {
            array_push($msg, "Invalid Password");
        }

        if (!empty($msg)) {
            return $this->apiSuccessResponse(0, $msg, "error");
        }

        $result = $this->db20->table('crm_member_list')
            ->select('status_level')
            ->where([
                'email_addr' => $email,
                'main_id' => $mbr_id
            ])->first();

        if (!$result) {
            return $this->apiSuccessResponse(0, "Associate member not found.", []);
        } else if ($result->status_level == 1) {
            return $this->apiSuccessResponse(0, "This associate member has already activated.", []);
        };

        if ($dob == "Invalid date") {
            $dob = date("Y-m-d");
        }

        $sql = <<<str
UPDATE crm_member_list 
set first_name= ?, last_name= ?, birth_date= ? , contact_num = ? , join_date= current_timestamp,mbr_pwd= ? ,status_level = 1, modified_on=current_timestamp
WHERE email_addr = ? and main_id= ?
str;
        $this->db20->statement($sql,
            [$first_name, $last_name, $dob, $contact_no, md5($password), $email, $mbr_id]);

        $this->db20->statement("INSERT INTO  o2o_log_live_processes (coy_id,process_id,status,remarks,mbr_id,created_on) VALUES(?,?,?,?,?, current_timestamp )",
            ['CTL', 'ASSO_SIGNUP', 'OK', json_encode($validates), $asso_id]);

        if ($asso_code) {
            $sql_upd = <<<str
	update o2o_log_live_processes_new set status= 1, modified_on= current_timestamp, modified_by= 'Signup_asso' where mbr_id= ? and process_id='ASSO_CODE' and remarks= ? 
str;
            $this->db20->statement($sql_upd, [$mbr_id, $asso_code]);
        }


        $tokenKey = $this->server_name . "assomembership" . $asso_id;
        Redis::del($tokenKey);

        return $this->apiSuccessResponse(1, "Success", []);
    }

    public function signup_associate($edata)
    {
        $data = json_decode(base64_decode($edata), true);
//        print_r($data);
//        die();

        $token = $data["m"];
        $key = $this->server_name . "assomembership" . $token;
        $data = Redis::get($key);

        if (!$data) {
            return redirect("https://www.challenger.com.sg/message/2");

        }
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        return redirect("valueclub://app/signup_associate/$edata");
    }

    public function validateStaffCode(Request $request)
    {
        $staffcode = $request->staffcode;
        if (!$staffcode || !$this->vcmemberRepository->isStaffCodeValid($staffcode)) {
            return $this->apiSuccessResponse(0, "Invalid Staff Code", []);
        }
        return $this->apiSuccessResponse(1, "Success", []);
    }

    public function validateStuSignUp(Request $request)
    {
        $coy_id = $this->CTL_COY_ID;
        $msg = array();
        $emailinvalid = false;
        $contactinvalid = false;
        //-- check for existence

        $check_email = strtolower($request->email);

        $data = $this->db20
            ->table('crm_member_list')
            ->select('mbr_id', 'email_addr', 'contact_num')
            ->where('coy_id', $coy_id)
            ->whereRaw("LOWER(email_addr) = '$check_email'")
            ->orWhere('contact_num', $request->contact)
            ->get();

        //-- check for empty data

        $validates = [$request->firstname, $request->lastname, $request->email, $request->contact, $request->pass1, $request->pass2];
        foreach ($validates as $validate) {
            if (!$validate) {
                array_push($msg, "Missing Info");
                return $this->apiSuccessResponse(0, $msg, $this->messages["SIGNUP_INCOMPLETE"]);
            }
        }

        if (!$this->vcmemberRepository->isValidEmail(trim($request->email))) {
            array_push($msg, "Email Invalid");
            $emailinvalid = true;
        }

        if (count($data) > 1) {
            foreach ($data as $member) {
                if (strtoupper(trim($member["mbr_id"])) == strtoupper(trim($request->nric)) && !in_array("Member Exist", $msg)) {
                    array_push($msg, "Member Exist");
                }

                if (strtolower(trim($member["email_addr"])) == strtolower(trim($request->email)) && !$emailinvalid && !in_array("This email exists in our system.", $msg)) {
                    array_push($msg, "This email exists in our system.");
                }

                if ($member["contact_num"] == $request->contact && !in_array("Contact Exist", $msg)) {
                    array_push($msg, "Contact Exist");
                    $contactinvalid = true;
                }
            }
        } else if (count($data) === 1) {
            if (strtoupper(trim($data[0]->mbr_id)) == strtoupper(trim($request->nric))) {
                array_push($msg, "Member Exist");
            }
            if (strtolower(trim($data[0]->email_addr)) == strtolower(trim($request->email)) && !$emailinvalid) {
                array_push($msg, "This email exists in our system.");
            }
            if ($data[0]->contact_num == $request->contact) {
                array_push($msg, "Contact Exist");
                $contactinvalid = true;
            }
        }

        if (strlen(trim($request->contact)) < 8 && !$contactinvalid) {
            array_push($msg, "Contact Minimum");
        } else if (!$this->vcmemberRepository->isContactNum($request->contact) && !$contactinvalid) {
            array_push($msg, "Contact Invalid");
        }

        //-- compare password1 and password1 if thesame
        if ($request->pass1 !== $request->pass2) {
            array_push($msg, "Invalid Password");
        }

        if ($request->nric) {
            if (count($msg) == 0 && count($data) > 0) {
                array_push($msg, "Member Exist");
            }
        }

        //--
        if (empty($msg)) {
            return $this->apiSuccessResponse(1, "valid", []);
        } else {
            return $this->apiSuccessResponse(0, $msg, "error");
        }
    }

    public function regEduEmail(Request $request)
    {
        $email = $request->email;
        $staffcode = $request->staffcode;
        $last_name = $request->lastname;
        $first_name = $request->firstname;
        $contact = $request->contact;
        $dob = $request->dob;
        $pass1 = $request->pass1;
        $pass2 = $request->pass2;

        $refer_id = $request->referer_id ? $request->referer_id : '';

        //-- Validate Staff code
        if ($staffcode) {
            if (!$this->vcmemberRepository->isStaffCodeValid($staffcode)) {
                return $this->apiSuccessResponse(0, "Invalid Staff Code", []);
            }
        }

        if (!$this->vcmemberRepository->isEduEmailValid($email) && !$staffcode) {
            return $this->apiSuccessResponse(0, "Non edu.sg email requires staff code and student ID verification. Please approach our staff for further assistance.", []);
        }

        //-- Add the account
        $dob = Carbon::parse($dob)->format('Y-m-d');

        $insert_data['mbr_id'] = $this->vcmemberRepository->generateNewMbrId('ST');
        $mbr_id = $insert_data['mbr_id'];

        $insert_data['mbr_type'] = $this->member_type_list['STUDENT'];
        $insert_data['status_level'] = 0;
        $insert_data['email_addr'] = $email;
        $insert_data['first_name'] = $first_name;  // Hardcoded first
        $insert_data['last_name'] = $last_name; // Hardcoded first
        $insert_data['contact_num'] = $contact; // Hardcoded first
        $insert_data['birth_date'] = $dob;
        $insert_data['mbr_pwd'] = $pass1;


        $startDate = time();
        $insert_data['exp_date'] = date('Y-m-d 23:59:59', strtotime('+1 year', $startDate));

        //-- Student Membership Log
        $remarks = $insert_data;
        if ($staffcode) {
            $remarks["staffcode"] = substr($staffcode, strlen('hachi'));
        }
        $remarks = json_encode($remarks);
        //-- eof Student Membership Log
        $this->_logprocess('STUNEW', 'OK', $remarks, $mbr_id, 'VcApp');

        if ($this->vcmemberRepository->addAccount($insert_data)) {
            $exp_date = $insert_data['exp_date'];

            $this->vcmemberRepository->insertMemberType($mbr_id, $mbr_id, $exp_date, $this->member_type_list['STUDENT']);

            //Update Referrer IF Got
            if ($refer_id) {
                $this->db20
                    ->table('o2o_log_live_processes')
                    ->where([
                        'process_id' => 'STU_REFER_FRIEND',
                        'remarks' => $email,
                        'mbr_id' => $refer_id
                    ])->update([
                        'modified_by' => $mbr_id,
                        'modified_on' => Carbon::now(),
                        'status' => '1'
                    ]);
            }

            $this->sendEduMemberEmail($mbr_id, $pass1, $email, $first_name);
            return $this->apiSuccessResponse(1, "success", '<p>Please check your inbox to verify your email.</p>');
        } else {
            return $this->apiSuccessResponse(0, "fail", 'Cannot register account registration failed, due to db error');
        }

    }


    public function validateNSMenSignup(Request $request)
    {
        $coy_id = $this->CTL_COY_ID;
        $msg = array();
        $emailinvalid = false;
        $contactinvalid = false;
        //-- check for existence


        $check_email = strtolower($request->email);

        $data = $this->db20
            ->table('crm_member_list')
            ->select('mbr_id', 'email_addr', 'contact_num')
            ->where('coy_id', $coy_id)
            ->whereRaw("LOWER(email_addr)='$check_email'")
            ->orWhere('contact_num', $request->contact)
            ->get();

        //-- check for empty data

        $validates = [$request->firstname, $request->lastname, $request->email, $request->contact, $request->pass1, $request->pass2];
        foreach ($validates as $validate) {
            if (!$validate) {
                array_push($msg, "Missing Info");
                return $this->apiSuccessResponse(0, $msg, $this->messages["SIGNUP_INCOMPLETE"]);
            }
        }

        if (!$this->vcmemberRepository->isValidEmail(trim($request->email))) {
            array_push($msg, "Email Invalid");
            $emailinvalid = true;
        }

        if (count($data) > 1) {
            foreach ($data as $member) {
                if (strtoupper(trim($member["mbr_id"])) == strtoupper(trim($request->nric)) && !in_array("Member Exist", $msg)) {
                    array_push($msg, "Member Exist");
                }

                if (strtolower(trim($member["email_addr"])) == strtolower(trim($request->email)) && !$emailinvalid && !in_array("This email exists in our system.", $msg)) {
                    array_push($msg, "This email exists in our system.");
                }

                if ($member["contact_num"] == $request->contact && !in_array("Contact Exist", $msg)) {
                    array_push($msg, "Contact Exist");
                    $contactinvalid = true;
                }
            }
        } else if (count($data) === 1) {
            if (strtoupper(trim($data[0]->mbr_id)) == strtoupper(trim($request->nric))) {
                array_push($msg, "Member Exist");
            }
            if (strtolower(trim($data[0]->email_addr)) == strtolower(trim($request->email)) && !$emailinvalid) {
                array_push($msg, "This email exists in our system.");
            }
            if ($data[0]->contact_num == $request->contact) {
                array_push($msg, "Contact Exist");
                $contactinvalid = true;
            }
        }

        if (strlen(trim($request->contact)) < 8 && !$contactinvalid) {
            array_push($msg, "Contact Minimum");
        } else if (!$this->vcmemberRepository->isContactNum($request->contact) && !$contactinvalid) {
            array_push($msg, "Contact Invalid");
        }

        //-- compare password1 and password1 if thesame
        if ($request->pass1 !== $request->pass2) {
            array_push($msg, "Invalid Password");
        }

        if ($request->nric) {
            if (count($msg) == 0 && count($data) > 0) {
                array_push($msg, "Member Exist");
            }
        }

        //--
        if (empty($msg)) {
            return $this->apiSuccessResponse(1, "valid", []);
        } else {
            return $this->apiSuccessResponse(0, $msg, "error");
        }
    }

    public function signupNSFMember(Request $request)
    {
        $email = $request->email;
        $staffcode = $request->staffcode;
        $last_name = $request->lastname;
        $first_name = $request->firstname;
        $contact = $request->contact;
        $dob = $request->dob;
        $pass1 = $request->pass1;
        $pass2 = $request->pass2;

        $email = strtolower($email);

        //-- Validate Staff code
        if ($staffcode) {
            if (!$this->vcmemberRepository->isStaffCodeValid($staffcode)) {
                return $this->apiSuccessResponse(0, "Invalid Staff Code", []);
            }
        }

        //-- Add the account
        $dob = Carbon::parse($dob)->format('Y-m-d');

        $insert_data['mbr_id'] = $this->vcmemberRepository->generateNewMbrId('NS');
        $mbr_id = $insert_data['mbr_id'];

        $insert_data['mbr_type'] = $this->member_type_list['NSMEN'];
        $insert_data['status_level'] = 0;
        $insert_data['email_addr'] = $email;
        $insert_data['first_name'] = $first_name;  // Hardcoded first
        $insert_data['last_name'] = $last_name; // Hardcoded first
        $insert_data['contact_num'] = $contact; // Hardcoded first
        $insert_data['birth_date'] = $dob;
        $insert_data['mbr_pwd'] = $pass1;


        $startDate = time();
        $insert_data['exp_date'] = date('Y-m-d 23:59:59', strtotime('+1 year', $startDate));

        //-- Student Membership Log
        $remarks = $insert_data;
        if ($staffcode) {
            $remarks["staffcode"] = substr($staffcode, strlen('hachi'));
        }
        $remarks = json_encode($remarks);
        //-- eof Student Membership Log
        $this->_logprocess('NSFNEW', 'OK', $remarks, $mbr_id, 'VcApp');

        if ($this->vcmemberRepository->addAccount($insert_data)) {
            $exp_date = $insert_data['exp_date'];

            $this->vcmemberRepository->insertMemberType($mbr_id, $mbr_id, $exp_date, $this->member_type_list['NSMEN']);

            $this->sendNSFMemberEmail($mbr_id, $pass1, $email, $first_name);
            return $this->apiSuccessResponse(1, "success", '<p>Please check your inbox to verify your email.</p>');
        } else {
            return $this->apiSuccessResponse(0, "fail", 'Cannot register account registration failed, due to db error');
        }

    }

    private function sendNSFMemberEmail($mbr_id, $pass1, $email, $first_name)
    {
        $mbr_id = trim($mbr_id);

        $token = $this->createToken("nsfmembership", 259200); // 72 hours

        $edata = base64_encode(
            json_encode(
                array(
                    "u" => $mbr_id,
                    "p" => $pass1,
                    "token" => $token,
                    "e" => $email,
                    "f" => $first_name
                )
            ));

        $theUrl = $this->redirectUrl;
        $url = $theUrl . '/api/vc2/activateNSMember/' . $edata;

        // For test only
        if ($this->ENVIRONMENT == 'development') {
            if (strpos($email, 'denise.goh') !== false) {
                $email = "denise.goh@challenger.sg";
            } else if (strpos($email, 'weiseng') !== false) {
                $email = 'weiseng_101@hotmail.com';
            } else {
                $emailTests = ['mike.castillo1@edu.sg'];
                if (in_array($email, $emailTests)) {
                    $email = "castillo_mike_romeo@yahoo.com";
                }
            }
        }

        $this->_logprocess('EMAIL_NSF_TRIGGER', '1', $email, $mbr_id, $mbr_id);

        $email_data = [
            'r_firstname' => $first_name,
            'r_verifyemail' => $url,
        ];

        $send_result = $this->_sendEmarsysEmail($this->emarsys_template_list["NSMEN_SIGNUP_ACTIVATE"], $email, $email_data);

        $this->_logprocess('EMAIL_NSF_SUCCESS', '-', json_encode($send_result), $mbr_id);

    }

    public function activateMember($edata)
    {
//        CI1::load()->library('encryption');
//        CI1::encryption()->decrypt(base64_decode($edata))
        $edata = json_decode(base64_decode($edata), TRUE);
        $token = $edata['token'];

//        $key = $this->server_name . "vctoken#" . $token;
        $data = Redis::get($token);


        if (!$data) {
            redirect("https://www.challenger.com.sg/message/2");
            die();
        }
        if ($edata && $edata['u']) {
            $sql = "SELECT status_level FROM crm_member_list WHERE mbr_id = ?";
            $result = $this->db20->select($sql, [$edata["u"]]);
            if (count($result) == 0) {
                return redirect("https://www.challenger.com.sg/message/1");
            }
            if ($result[0]->status_level == 1) {
                return redirect("https://www.challenger.com.sg/message/1");
            };
        }

        $sql = "UPDATE crm_member_list set status_level = 1,modified_on= current_timestamp ,join_date= current_timestamp WHERE mbr_id = ?";
        $this->db20->statement($sql, [$edata["u"]]);

        $this->db20->statement("INSERT INTO  o2o_log_live_processes(coy_id,process_id,status,remarks,mbr_id,created_on) VALUES(?,?,?,?,?,current_timestamp)",
            ['CTL', 'STU_ACT_MBRSHIP', 'OK', '', $edata['u']]);

        $rm_result = $this->updateOrInsertMbr($edata['u'], $this->_accessPass);

        $mbr_id = $edata['u'];
        $this->_issueSignUpVoucher($mbr_id, $this->member_type_list["STUDENT"], '', 'OS');

        $edata = base64_encode(json_encode($edata));
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/autologin/$edata");
        } else {
            return redirect("https://www.challenger.com.sg/message/6");
        }
    }


    public function activateNSMember($edata)
    {
        $edata = json_decode(base64_decode($edata), TRUE);
        $token = $edata['token'];

//        $key = $this->server_name . "vctoken#" . $token;
        $data = Redis::get($token);


        if (!$data) {
            redirect("https://www.challenger.com.sg/message/2");
            die();
        }
        if ($edata && $edata['u']) {
            $sql = "SELECT status_level FROM crm_member_list WHERE mbr_id = ? and status_level != -1";
            $result = $this->db20->select($sql, [$edata["u"]]);
            if (count($result) == 0) {
                return redirect("https://www.challenger.com.sg/message/1");
            }
            if ($result[0]->status_level == 1) {
                return redirect("https://www.challenger.com.sg/message/1");
            };
        }

        $sql = "UPDATE crm_member_list set status_level = 1,modified_on= current_timestamp ,join_date= current_timestamp WHERE mbr_id = ?";
        $this->db20->statement($sql, [$edata["u"]]);

        $this->db20->statement("INSERT INTO  o2o_log_live_processes(coy_id,process_id,status,remarks,mbr_id,created_on) VALUES(?,?,?,?,?,current_timestamp)",
            ['CTL', 'NSMEN_ACT_MBRSHIP', 'OK', '', $edata['u']]);

        $this->updateOrInsertMbr($edata['u'], $this->_accessPass);

        //Send a Welcome Email to User.
        $email = $edata["e"];
        $first_name = $edata["f"];
        $email_data = [
            'r_fname' => $first_name,
        ];
        $this->_sendEmarsysEmail($this->emarsys_template_list["NSMEN_WELCOME"], $email, $email_data);


        //Remove Redis Token.
        Redis::del($token);

        $edata = base64_encode(json_encode($edata));
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/autologin/$edata");
        } else {
            return redirect("https://www.challenger.com.sg/message/6");
        }
    }


    private function sendEduMemberEmail($mbr_id, $pass1, $email, $first_name)
    {
        $mbr_id = trim($mbr_id);

//        CI1::load()->library('encryption');

        $token = $this->createToken("edumembership", 259200); // 72 hours

        $edata = base64_encode(
            json_encode(
                array(
                    "u" => $mbr_id,
                    "p" => $pass1,
                    "token" => $token,
                    "e" => $email
                )
            ));

        $theUrl = $this->redirectUrl;
        $url = $theUrl . '/api/vc2/activateMember/' . $edata;

        // For test only
        if ($this->ENVIRONMENT == 'development') {
            if (strpos($email, 'denise.goh') !== false) {
                $email = "denise.goh@challenger.sg";
            } else if (strpos($email, 'weiseng') !== false) {
                $email = 'weiseng_101@hotmail.com';
            } else {
                $emailTests = ['mike.castillo1@edu.sg'];
                if (in_array($email, $emailTests)) {
                    $email = "castillo_mike_romeo@yahoo.com";
                }
            }
        }

        $this->_logprocess('EmailStuMbrTrigger', '1', $email, $mbr_id, $mbr_id);

        $email_data = [
            'r_fname' => $first_name,
            'r_OrderDetailsURL' => $url
        ];

        $send_result = $this->_sendEmarsysEmail($this->emarsys_template_list["STUDENT_MEMBERSHIP"], $email, $email_data);

        $this->_logprocess('EmailStudentMbr', '-', json_encode($send_result), $mbr_id);
    }


    public function getProductReview(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $result = $this->vcmemberRepository->getProductReviewByMbrId($mbr_id);

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function ProductReviewPage($edata)
    {

        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            redirect("valueclub://app/productreview/$edata");
        } else {
            redirect("https://www.challenger.com.sg/message/4");
        }
    }

    public function submitProductReview(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $item_id = $request->item_id;
        $trans_id = $request->trans_id;
        $star_count = $request->star_count;
        $like_count = $request->like_count;
        $dislike_count = $request->dislike_count;


        $validate = $this->vcmemberRepository->getProductReviewItem($mbr_id, $item_id, $trans_id);

        if (!$validate) {
            return $this->apiSuccessResponse(0, "Item Not Found.", []);
        } else if ($validate[0]->status_level != 0) {
            return $this->apiSuccessResponse(0, "You have already reviewed this item.", []);
        }

        $this->vcmemberRepository->updateProductReviewStat(1, $star_count, $like_count, $dislike_count, $mbr_id, $item_id, $trans_id);

        //Log Process (Update Review)
        $coy_id = 'CTL';
        $earn_rebates = 100;
        $remarks = $trans_id . " | " . $item_id . " | " . $earn_rebates . " | " . $star_count . " | " . $like_count . " | " . $dislike_count;
        $this->_logprocess('PRODUCT_REVIEW', 'OK', $remarks, $mbr_id);

        $func = "points/award";
        $trans_id = $this->vcmemberRepository->getNextInvoiceNo();
        $post_data = array(
            "mbr_id" => $mbr_id,
            "trans_id" => $trans_id,
            "rebate_amount" => 1.00,
            "rebate_description" => "Product Review V1 Rebate"
        );

        $this->curlCHPos($func, $post_data);

        //Log Process (Update Rebates)
        $status = 'XOK';
        if (isset($rebates_result->result_id)) {
            $status = 'OK';
        }
        $this->_logprocess('PRODUCT_REVIEW_REBATES', $status, $remarks, $mbr_id);

        //Update Status to 2.
        $this->vcmemberRepository->updateProductReviewFinish($mbr_id, $item_id, $trans_id);


        $member = $this->vcmemberRepository->getReceiverUser($mbr_id);
        $email_address = $member->email_addr;
        $send_email_param = array(
            "r_FirstName" => $member->first_name
        );
        $this->_sendEmarsysEmail($this->emarsys_template_list["FINISH_PROD_REVIEW"], $email_address, $send_email_param);

        //Return List.
        $result = $this->vcmemberRepository->getProductReviewByMbrId($mbr_id);

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function getAllProductReview($item_list)
    {
        $key = $this->server_name . $this->productReview_key;
        if (!$item_list) {
            return $this->apiSuccessResponse(0, "Item ID is Empty.", []);
        }
        $item_ids = explode(",", $item_list);
//        print_r($item_ids);
        $result = array();
        foreach ($item_ids as $item_id) {
            $list = Redis::hMget($key, $item_id)[0];
            if ($list) {
                $data = json_decode($list, 1);
                $result[$item_id] = (float)$data["rating"];
            } else {
                $result[$item_id] = 0;
            }
        }

        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function getMemberItems($type = '')
    {
        $displaySql = "";
        if ($type == 'web') {
            $displaySql = " and is_display='1'";
        }
        $statusSql = "";
        if ($this->ENVIRONMENT == 'production') {
            $statusSql = " and status_level = 1";
        }

        $sql = <<<str
select
    sort_level as line_num,
    item_id as item_id,
    item_desc as item_desc,
    item_type as item_tye,
    unit_price as unit_price,
    item_group,
    status_level,
    image_url,
    desc_text
    from ims_item_list_member where item_id!=''
        $displaySql
        $statusSql
order by sort_level

str;
        $lists = $this->db20->select($sql);

        return $this->apiSuccessResponse(1, "Success", $lists);
    }

    public function getItemProductReview($item_id)
    {
        if (!$item_id) {
            return $this->apiSuccessResponse(0, "Item ID is Empty.", []);
        }

        $key = $this->server_name . $this->productReview_key;
        $result = Redis::hMget($key, $item_id)[0];
        if (!$result) {
            return $this->apiSuccessResponse(0, "No Rating.", []);
        } else {
            $result = json_decode($result, 1);
        }

        return $this->apiSuccessResponse(1, "Success", $result);
//        echo $result;
    }


    public function sendReferFriends(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $email_items = $request->email_lists;
        $email_lists = array();
        foreach ($email_items as $email_item) {
            $the_email = strtolower($email_item);
            $email_lists[] = $the_email;
        }


        $lists = $this->db20->table('crm_member_list')
            ->selectRaw('RTRIM(email_addr) as email_addr')
            ->whereIn('email_addr', $email_lists)
            ->where('status_level', '>=', 0)
            ->get();

        if ($lists) {
            $email_exist = '';
            foreach ($lists as $index => $list) {
                $emailItem = $list->email_addr;
                $email_exist .= $emailItem;
                if ($index !== count($lists) - 1) {
                    $email_exist .= ", ";
                }
            }

            if ($email_exist) {
                return $this->apiSuccessResponse(0, $email_exist . " already exist(s) in our system.", []);
            }
        }

        //Validate If Already Sent before.
        $list2 = $this->db20->table('o2o_log_live_processes')
            ->selectRaw('RTRIM(remarks) as email_addr')
            ->whereIn('remarks', $email_lists)
            ->where('mbr_id', $mbr_id)
            ->where('process_id', 'STU_REFER_FRIEND')
            ->get();


        if (count($list2) > 0) {
            $email_string = '';
            foreach ($list2 as $index => $list) {
                $email_string .= $list->email_addr;
                if ($index < count($list2) - 1) {
                    $email_string .= ', ';
                }
            }

            return $this->apiSuccessResponse(0, 'You have already sent the invitation to ' . $email_string . " already.", []);
        }

        $member = $this->vcmemberRepository->getMemberById($mbr_id);


        foreach ($email_lists as $email) {
            //Create Token.
            $edata = array(
                "e" => $email,
                "m" => $mbr_id
            );

            $token = $this->createToken($edata, 2592000, "vctoken"); // 30 days
            $edata["t"] = $token;

            $edata = base64_encode(json_encode($edata));
            $theUrl = $this->redirectUrl;

            $url = $theUrl . '/api/vc2/' . 'SignupStudentPage/' . $edata;

            //Send Refer Email to Them.
            $email_address = $email;
            $email_data = [
                'r_membername' => $member->first_name,
                'r_appurl' => $url,
            ];

            if ($this->ENVIRONMENT == 'development' && strpos($email, 'weiseng') !== false) {
                $email_address = 'wong.weiseng@challenger.sg';
            }

            $this->_sendEmarsysEmail($this->emarsys_template_list["SEND_REFER_FRIENDS"], $email_address, $email_data, '', true, 'STU_REFER_FRIEND');

            $this->_logprocess('STU_REFER_FRIEND', '0', $email, $mbr_id, 'VcApp');
        }

        return $this->apiSuccessResponse(1, "Success", $email_lists);
    }

    //Test
    public function signupStudentPage($edata)
    {
        $decodedata = json_decode(base64_decode($edata), 1);

        $token = $decodedata["t"];
        $key = $this->server_name . "vctoken#" . $token;
        $data = Redis::get($token);

//		print_r($data);
//		die();

        if (!$data) {
            return redirect("https://www.challenger.com.sg/message/2");
            die();
        }
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/signup_student/$edata");
        } else {
            return redirect("https://www.challenger.com.sg/message/6");
        }
    }

    public function validateRenew(Request $request)
    {
        $mbr_id = $request->nric;

        $validate_mbr = $this->vcmemberRepository->verifyNewMbrId($mbr_id);
        if (!$validate_mbr) {
            return $this->apiSuccessResponse(0, "Failed", "Your member information is still pending for processing. This would take about 1 day to process, you may try again tomorrow.");
        }

        //New Validation For Renew
        $res = $this->vcmemberRepository->isEligibleToRenew($mbr_id);
        if (!$res) {
            return $this->apiSuccessResponse(0, "Member is not eligable to renew now.", "Member is not eligable to renew now.");
        }

        $data = $this->db20
            ->table('crm_member_list')
            ->select(DB::raw("-DATE_PART('day', exp_date::timestamp - current_timestamp) as daydiff"), "mbr_type")
            ->where([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id
            ])->first();

        if (!$data) {
            return $this->apiSuccessResponse(0, "Member has already expired", "Member has already expired");
        } else if ($data && $data->daydiff > 90) {
            return $this->apiSuccessResponse(0, "Member has already expired", "Member has already expired");
        }


        return $this->apiSuccessResponse(1, "valid", []);
    }

    public function validateEmailContact(Request $request)
    {
        $email = $request->email;
        $contact = $request->contact;
        $coy_id = $this->CTL_COY_ID;

        if (!$email && !$contact) {
            return $this->apiSuccessResponse(3, "Invalid Email & Contact no.", "Invalid Email & Contact no.");
        }

        if (!$email) {
            return $this->apiSuccessResponse(0, "Invalid Email.", "Invalid Email.");
        }

        if (!$contact) {
            return $this->apiSuccessResponse(2, "Invalid Contact No.", "Invalid Contact No.");
        }

        $isCheckEmail = $this->vcmemberRepository->isValidEmail(trim($email));
        $isCheckContact = $this->vcmemberRepository->isContactNum($contact);

        if (!$isCheckEmail && !$isCheckContact) {
            return $this->apiSuccessResponse(3, "Invalid Email & Contact no.", "Invalid Email & Contact no.");
        }

        if (!$isCheckEmail) {
            return $this->apiSuccessResponse(0, "Invalid Email.", "Invalid Email.");
        }

        if (!$isCheckContact) {
            return $this->apiSuccessResponse(2, "Invalid Contact no.", "Invalid Contact no.");
        }

        $data = $this->db20->select("SELECT mbr_id,email_addr,contact_num FROM crm_member_list 
WHERE (LOWER(email_addr) = ? or RTRIM(contact_num) = ?) and coy_id = ?",
            [strtolower($email), $contact, $coy_id]);

        $isEmail = false;
        $isContact = false;

        if (count($data) > 0) {

            foreach ($data as $list) {

                if (strtolower(trim($list->email_addr)) == strtolower(trim($email)) && $list->contact_num == $contact) {
                    $isEmail = true;
                    $isContact = true;
                }

                if (strtolower(trim($list->email_addr)) == strtolower(trim($email))) {
                    $isEmail = true;
                }
                if ($list->contact_num == $contact) {
                    $isContact = true;
                }

            }


        }


        if ($isEmail && $isContact) {
            return $this->apiSuccessResponse(3, "Email & Contact no. already exist.", "Email & Contact no. already exist.");
        }

        if ($isEmail) {
            return $this->apiSuccessResponse(0, "Email already exist.", "Email already exist.");
        }
        if ($isContact) {
            return $this->apiSuccessResponse(2, "Contact already exist.", "Contact already exist.");
        }

        return $this->apiSuccessResponse(1, "Valid.", "Valid.");
    }

    //TODO::: add a checking here.
    public function validateSignup(Request $request)
    {
        $msg = array();
        $emailinvalid = false;
        $contactinvalid = false;

        $coy_id = $this->CTL_COY_ID;
        $nric = $request->nric ? $request->nric : '';
        $email = $request->email ? $request->email : '';
        $contact = $request->contact ? $request->contact : '';
        $firstname = $request->firstname ? $request->firstname : '';
        $lastname = $request->lastname ? $request->lastname : '';
        $pass1 = $request->pass1 ? $request->pass1 : '';
        $pass2 = $request->pass2 ? $request->pass2 : '';

        //-- check for existence
        if ($nric) {
            $data = $this->db20->select("SELECT mbr_id,email_addr,contact_num FROM crm_member_list 
WHERE (mbr_id = ? or LOWER(email_addr) = ? or RTRIM(contact_num) = ?) and coy_id = ?",
                [strtoupper($nric), strtolower($email), $contact, $coy_id]);
        } else {
            $data = $this->db20->select("SELECT mbr_id,email_addr,contact_num FROM crm_member_list 
WHERE (LOWER(email_addr) = ? or RTRIM(contact_num) = ?) and coy_id = ?",
                [strtolower($email), $contact, $coy_id]);
        }
        //-- check for empty data
        $validates = [$email, $contact, $firstname, $lastname, $pass1, $pass2];
        foreach ($validates as $validate) {
            if (!$validate) {
                array_push($msg, "Missing Info");
                return $this->apiSuccessResponse(0, $msg, $this->messages["SIGNUP_INCOMPLETE"]);
            }
        }

        if (!$this->vcmemberRepository->isValidEmail(trim($email))) {
            array_push($msg, "Email Invalid");
            $emailinvalid = true;
        }

        if (count($data) > 1) {
            foreach ($data as $member) {
                $member = $this->vcmemberRepository->stdToArray($member);

                if (strtoupper(trim($member["mbr_id"])) == strtoupper(trim($nric)) && !in_array("Member Exist", $msg)) {
                    array_push($msg, "Member Exist");
                }

                if (strtolower(trim($member["email_addr"])) == strtolower(trim($email)) && !$emailinvalid && !in_array("Email Exist", $msg)) {
                    array_push($msg, "Email Exist");
                }

                if ($member["contact_num"] == $contact && !in_array("Contact Exist", $msg)) {
                    array_push($msg, "Contact Exist");
                    $contactinvalid = true;
                }
            }
        } else if (count($data) === 1) {
            if (strtoupper(trim($data[0]->mbr_id)) == strtoupper(trim($nric))) {
                array_push($msg, "Member Exist");
            }
            if (strtolower(trim($data[0]->email_addr)) == strtolower(trim($email)) && !$emailinvalid) {
                array_push($msg, "Email Exist");
            }
            if ($data[0]->contact_num == $contact) {
                array_push($msg, "Contact Exist");
                $contactinvalid = true;
            }
        }

        if (strlen(trim($contact)) < 8 && !$contactinvalid) {
            array_push($msg, "Contact Minimum");
        } else if (!$this->vcmemberRepository->isContactNum($contact) && !$contactinvalid) {
            array_push($msg, "Contact Invalid");
        }

        //-- compare password1 and password1 if thesame
        if ($pass1 !== $pass2) {
            array_push($msg, "Invalid Password");
        }

        if ($nric) {
            if (count($msg) == 0 && count($data) > 0) {
                array_push($msg, "Member Exist");
            }
        }


        //--
        if (empty($msg)) {
            return $this->apiSuccessResponse(1, "valid", []);
        } else {
            return $this->apiSuccessResponse(0, $msg, "error");
        }
    }

    public function getVoteStaffInfo(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $staff_id = $request->staff_id;
        $trans_id = isset($request->trans_id) ? $request->trans_id : '';

        $list = $this->getStaffInfo($staff_id);
        if (!$list) {
            return $this->apiSuccessResponse(0, "Staff ID 1 not found.", []);
        }
        $list = $this->vcmemberRepository->stdToArray($list);

        if ($trans_id) {
            $validate = $this->vcmemberRepository->checkNonMemberTransaction($staff_id, $trans_id);
            if ($validate === -1) {
                return $this->apiSuccessResponse(0, "Receipt no. not found.", []);

            } else if ($validate === 0) {
                return $this->apiSuccessResponse(0, "You have voted already.", []);
            }
            $list["trans_id"] = $trans_id;
        } else {
            $validate = $this->vcmemberRepository->checkDailyTransaction($mbr_id, $staff_id);
            if ($validate === -1) {
                return $this->apiSuccessResponse(0, "You have not made any transactions today.", []);

            } else if ($validate === 0) {
                return $this->apiSuccessResponse(0, "You have voted already.", []);

            }
            $list["trans_id"] = $validate;
        }

        $url = "https://kxrimfra0k.execute-api.ap-northeast-1.amazonaws.com/faceapi/faceapi";
        $headers = array(
            'x-api-key: RcvbIpBxDA5SwJX4N2nloa64aO15JCTdaCVJtHeq',
            'Content-Type: application/json'
        );
        // note: staffcode and image is optional
        $fields = array(
            "staffcode" => $staff_id,
            "func" => "getprofilephoto",
            "image" => [],
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($result, true);
        if ($response["code"]) {
            $list["img_name"] = "data:image/jpeg;base64," . $response["msg"];
        } else {
            $list["img_name"] = '';
        }

        return $this->apiSuccessResponse(1, "OK", $list);
    }

    private function getStaffInfo($staff_id)
    {
        $url = $this->imsUrl . 'vcapp/getstaffinfo';

        $post_data = array(
            "staff_id" => $staff_id
        );

        $data = $this->curlImsApi($url, $post_data);

        return $data;
    }

    private function curlImsApi($url, $post_data)
    {
        $headers = array(
            'Authorization: Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);

//        print_r($result);
//        die();

        $response = json_decode($result, true);
        curl_close($ch);

//        print_r($response);
//        die();

        return $response["data"];
    }

    private function curlLogisticsApi($url, $post_data)
    {
        $headers = array(
            'Authorization: Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);
//        print_r($result);
//        die();

        $response = json_decode($result, true);
        curl_close($ch);

        return $response;
    }


    public function voteStaff(Request $request)
    {

        $staff_id = $request->staff_id;
        $loc_id = $request->loc_id;
        $mbr_id = $request->mbr_id ? $request->mbr_id : '';
        $trans_id = $request->trans_id ? $request->trans_id : '';


        $sql = <<<str
      insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','VOTE_STAFF', ? ,? , 1, ?, ?, ?, current_timestamp )
str;
        $this->db20->statement($sql, [$loc_id, $trans_id, $staff_id, $mbr_id, 'VcApp']);

        return $this->apiSuccessResponse(1, "Success", []);
    }

    public function addVoucher(Request $request)
    {
        $voucherid = $request->voucherid ? trim($request->voucherid) : '';
        $mbr_id = $request->mbr_id ? $request->mbr_id : '';

        if (substr($voucherid, 0, 5) == "POS2/") {
            $resp = $this->processQRCodePOS2($mbr_id, $voucherid);
            return $this->apiSuccessResponse($resp["code"], $resp["msg"], $resp["result"]);
        } else if (substr($voucherid, 0, 22) == "INCALL/RESERVE_REBATE/") {
            $resp2 = $this->processQRCodeIncall_ReserveRebate($mbr_id, $voucherid);
            return $this->apiSuccessResponse($resp2["code"], $resp2["msg"], $resp2["result"]);
        } else if (substr($voucherid, 0, 20) == "INCALL/AWARD_REBATE/") {
            $resp3 = $this->processQRCodeIncall_AwardRebate($mbr_id, $voucherid);
            return $this->apiSuccessResponse($resp3["code"], $resp3["msg"], $resp3["result"]);
        } else {
            $result = $this->vcApp_addVoucher($mbr_id, $voucherid);
            $code = isset($result["valid"]) ? $result["valid"] : null;
            $message = ($code !== null) ? $result["message"] : 'Failed';
            return $this->apiSuccessResponse($code, $message, []);
        }

        return $this->apiSuccessResponse(1, "Success", []);;
    }

    //TODO:: Put AddVoucher.
    private function vcApp_addVoucher($mbr_id, $voucherid)
    {
        $hasMember = $this->vcmemberRepository->isMemberExist($mbr_id);
        if ($hasMember === false) {
            return array(
                "valid" => 0,
                "message" => "Unverified Member ID. Please re-login to update your session."
            );
        }

        //Add Voucher.
//        $sql = <<<str
//INSERT INTO crm_voucher_list
//            (coy_id, coupon_id, coupon_serialno, promocart_id, mbr_id, status_level, issue_date, expiry_date,
//             voucher_amount, receipt_id1, created_by, created_on, modified_by, modified_on, [guid],utilize_date,loc_id)
//            VALUES
//('CTL', '$voucherid', 'coupon_serialno', '$voucherid', '$mbr_id', 0, current_timestamp, 'expDate', 0, '', 'Vc_Sp', current_timestamp,
//                    'VcApp', current_timestamp , NEWID(),null, '');
//str;
//
//
//        $this->clearRedisFields($mbr_id, '#myVouchers#', $this->AUTOLOGIN_TOKEN, false);

        return array(
            "valid" => 0,
            "message" => "Unable to Add Voucher now."
        );
    }


    public function processQRCodePOS2($mbr_id, $voucherid, $silent = FALSE)
    {
        $url = "https://pos8.api.valueclub.asia/api/valueclub/qr_login";
        $headers = array(
            'X-AUTH: daaa259760e5402367299b0fa2646e14',
            'Content-Type: application/json'
        );
        $data = [
            "mbr_id" => $mbr_id,
            "channel_code" => $voucherid
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);
        if (!$silent) {
            if (array_key_exists('code', $result)) {
                return array(
                    "code" => $result['code'],
                    //The Qr_login lambda server return 500 but login still works, so I have to put like this.
                    "msg" => ($result['msg']=='Member login fail') ?  "Success." : $result["msg"],
                    "result" => []);
            } else {
                return array(
                    "code" => 0,
                    "msg" => "Login fails, server response unknown",
                    "result" => []);
            }
        }
    }

    private function processQRCodeIncall_ReserveRebate($mbr_id, $data)
    {
        $data = explode("/", $data);
        if (count($data) !== 4) {
            return array(
                "code" => 0,
                "msg" => "Invalid Parameters",
                "result" => []);
        }
        $transId = $data[2];
        $rebateAmount = $data[3];
        //echo $transId  . "<br>" . $amount;
        $data = [
            "mbr_id" => $mbr_id,//"V183535200",
            "trans_id" => $transId,
            "rebate_amount" => $rebateAmount
        ];
        $result = $this->curlCHPos("reserved", $data);
        //$result = $this->curlCHPos("unreserved",$data);
        if ($result['status_code'] == 200) {
            // redis
            $params = ["connection_group" => "incall"];
            $redisKey = "incall_reserve_rebate#" . $transId;
            $data = json_encode($data);
            Redis::setex($redisKey, 720, $data); //12minutes = 720
            //$redis->setex($redisKey, 720, $rebateAmount);// 12minutes = 720
            return array(
                "code" => 1,
                "msg" => $result['info'],
                "result" => []);
        } else {
            return array(
                "code" => 0,
                "msg" => $result['message'],
                "result" => []);
        }
    }

    private function processQRCodeIncall_AwardRebate($mbr_id, $data)
    {
        $data = explode("/", $data);
        if (count($data) !== 4) {
            return array(
                "code" => 0,
                "msg" => "Invalid Parameters",
                "result" => []);
        }
        $transId = $data[2];

        $key = "incall_award_rebate#$transId";
        $redisData = Redis::get($key);
        $redisData = json_decode($redisData, TRUE);
        $redisData['customer_info']['id'] = $mbr_id;

        $result = $this->curlCHPos("transaction", $redisData);
        if ($result['status_code'] == 200) {
            // redis
            return array(
                "code" => 1,
                "msg" => $result['info'],
                "result" => []);

        } else {
            return array(
                "code" => 0,
                "msg" => $result['message'],
                "result" => []);
        }
    }

    private function curlCHPos($func, $data)
    {
        $url = "https://chvc-api.sghachi.com/";
        if ($this->ENVIRONMENT == 'production') {
            $url = "https://vc.api.valueclub.asia/";
        }
        $keycode = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
        $url = $url . "api/" . $func . "?key_code=" . $keycode;
        $headers = array(
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);

//        echo $url;
//        print_r($result);
        return $result;
    }

    public function GetMemberByMbrId(Request $request)
    {
        $mbr_id = trim($request->nric);

        $result = $this->db20->table('crm_member_list')
            ->select(DB::raw("CONCAT(first_name,' ',last_name) as full_name"), DB::raw("LTRIM(RTRIM(mbr_id)) as mbr_id"),
                DB::raw("CASE WHEN cast(exp_date as date) > cast(current_timestamp as date) THEN 0 ELSE 1 END expired"))
            ->where('coy_id', 'CTL')
            ->whereRaw("(mbr_id = '$mbr_id' or email_addr= '$mbr_id' or contact_num= '$mbr_id' )")
            ->first();

//        print_r($result);

        $result->valid = FALSE;
        if ($result->full_name) {
            $result->valid = true;
        }
        return $this->apiSuccessResponse(1, "success", $result);
    }


    public function initRebatePage($page, $mbrId)
    {
        $keyToken = bin2hex(openssl_random_pseudo_bytes(35));
        $keyToken = ["token" => $keyToken, "mbr_id" => $mbrId];
        $keyToken = base64_encode(json_encode($keyToken));
        Redis::setex("REBATESESSION" . $keyToken, 300, "yes"); // 5 minutes
        $keyToken = urlencode($keyToken);
        $mode = $this->ENVIRONMENT;
//        $url = "http://localhost:8101/$page?token=$keyToken";
        $url = "https://ctl-public-test.s3-ap-southeast-1.amazonaws.com/app/valueclub20Hosted/$mode/index.html?p=$page&token=$keyToken";
        return redirect($url);
    }

    public function my_membership(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $token = $request->token;
        $this->isRebateSessionValid($token);
        $data = $this->getMemberShipData($mbr_id, '');

        return response()->json($data);
    }

    private function isRebateSessionValid($token)
    {
        $isValidSession = Redis::get("REBATESESSION" . $token);
        if (!$isValidSession) {
            return $this->apiSuccessResponse(0, "Invalid session/session expired",
                array("code" => -1, "msg" => "Session expired. Please try again."));
        }
        return true;
    }

    public function donate_rebate(Request $request)
    { // use by version external link rebate redemption
        $token = $request->token;
        $this->isRebateSessionValid($token);
        $return = $this->DonateRebates($request);
        return $this->apiSuccessResponse(1, "Success", $return);
    }

    public function DonateRebates($request)
    {
        $mbr_id = trim($request->mbr_id);
        $username = trim($request->full_name);
        $points = trim($request->point);
        $token = $request->token;

        $result = $this->vcmemberRepository->donateRebates($mbr_id, $this->_donateRebatesMbrID, $points);
        if ($result["status"] == 1) {
            $this->_logprocess("VCDONATEREBATE", "OK", "$mbr_id#$points;", $mbr_id);
            $this->createToken(array("from_id" => $mbr_id, "user_name" => $username, "points" => $points), 1200, "vcdonaterebate_" . strtoupper(uniqid()));

            $this->clearRedisFields($mbr_id, '#myRedemption#', $this->AUTOLOGIN_TOKEN, false);
            $this->clearRedisFields($mbr_id, '#MBRINFO#', $this->AUTOLOGIN_TOKEN, false);

            $sender_user = $this->vcmemberRepository->getReceiverUser($mbr_id);
            $sender_vpoints = $this->vcmemberRepository->getAvailablePoints($mbr_id, $this->CTL_COY_ID);

            $email_address = $sender_user->email_addr;
            $email_data = array(
                "r_C_First_Name" => ucwords(strtolower($sender_user->first_name)),
                "r_c_point_xfer_val" => number_format($points / 100, 2),
                "r_c_rebate_value" => number_format($sender_vpoints / 100, 2)
            );
            $this->_sendEmarsysEmail($this->emarsys_template_list["SENDER_REBATE_DONATE"], $email_address, $email_data);

        }

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
        Redis::del("REBATESESSION" . $token);
        return $result;
    }

    public function transfer_point(Request $request)
    {
        $token = $request->token;
        $mbr_id = $request->mbr_id;
        $transfer_id = $request->transfer_id;
        if ($mbr_id == $transfer_id) {
            return $this->apiSuccessResponse(0, "You cannot transfer rebates to yourself.", []);
        }

        $this->isRebateSessionValid($token);
        $return = $this->transferpoint($request);

        return $this->apiSuccessResponse(1, "Success", $return);
    }

    public function get_member_byid(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        $token = $request->token;
        $this->isRebateSessionValid($token);
        $result = $this->GetMemberByMbrIdGet($request);
        return $this->apiSuccessResponse(1, "Success", $result);
    }

    private function GetMemberByMbrIdGet($request)
    {
        $nric = $request->nric;

        $mbr_id = trim($nric);
        $sql = <<<str
select CONCAT(first_name,' ',last_name) as full_name,LTRIM(RTRIM(mbr_id)) as mbr_id,
CASE WHEN cast(exp_date as date) > cast(current_timestamp as date) THEN 0 ELSE 1 END expired from crm_member_list
 WHERE (mbr_id = ? or email_addr= ?
            or contact_num= ?) and coy_id = 'CTL'
str;

        $resp = $this->db20->select($sql, [$mbr_id, $mbr_id, $mbr_id]);
        if (count($resp) === 0) {
            $result = array("valid" => false, "full_name" => "", "mbr_id" => "", "expired" => 0);
        } else {
            $result = $resp[0];
            $result->valid = FALSE;
            if ($result->full_name) {
                $result->valid = true;
            }
        }

        return $result;
    }


    public function transferpoint($request)
    {
        $data = array();
        $data["mbr_id"] = $request->mbr_id;
        $data["transfer_id"] = $request->transfer_id;
        $data["full_name"] = $request->full_name;
        $data["point"] = $request->point;
        $data["message"] = $request->message;
        $data["info"] = $request->info;
        $data["token"] = $request->token;


        $mbr_id = trim($data["mbr_id"]);
        $transfer_id = trim($data["transfer_id"]);
        $username = trim($data["full_name"]);
        $points = trim($data["point"]);
        $message = trim($data["message"]);
        $info = trim($data["info"]);
        $token = $data["token"];
        $result = $this->vcmemberRepository->transferPointsToVCMember($mbr_id, $transfer_id, $points);
        if ($result["status"] == 1) {
            $this->_logprocess("VCTRANSFERPOINT", "OK", "$mbr_id#$transfer_id;$points;$message", $mbr_id);
            $this->createToken(array("from_id" => $mbr_id, "user_name" => $username, "to_id" => $transfer_id, "points" => $points, "message" => $message, "info" => $info), 1200, "vctranspoint_" . strtoupper(uniqid()));

            $this->clearRedisFields($mbr_id, '#myRedemption#', $this->AUTOLOGIN_TOKEN, false);
            $this->clearRedisFields($mbr_id, '#MBRINFO#', $this->AUTOLOGIN_TOKEN, false);

            $sender_user = $this->vcmemberRepository->getReceiverUser($mbr_id);
            $receiver_user = $this->vcmemberRepository->getReceiverUser($transfer_id);
            $sender_vpoints = $this->vcmemberRepository->getAvailablePoints($mbr_id, $this->CTL_COY_ID);


            $sender_email = $sender_user->email_addr;
            $send_email_data = array(
                "r_C_First_Name" => ucwords(strtolower($sender_user->first_name)),
                "r_c_rev_Name" => ucwords(strtolower($receiver_user->first_name)),
                "r_c_point_xfer_date" => date("d M Y"),
                "r_c_point_xfer_val" => number_format($points / 100, 2),
                "r_c_rebate_value" => number_format($sender_vpoints / 100, 2),
                "r_c_trf_rebate_msg" => $message
            );

            $receiver_email = $receiver_user->email_addr;
            $receive_email_data = array(
                "r_C_First_Name" => ucwords(strtolower($receiver_user->first_name)),
                "r_C_sender_Name" => ucwords(strtolower($sender_user->first_name)),
                "r_c_point_revc_date" => date("d M Y"),
                "r_c_point_recv_val" => number_format($points / 100, 2),
                "r_c_rebate_value" => number_format($receiver_user->point_avail / 100, 2),
                "r_c_trf_rebate_msg" => $message
            );

            $this->_sendEmarsysEmail($this->emarsys_template_list["SENDER_REBATE_TRANSFER"], $sender_email, $send_email_data);
            $this->_sendEmarsysEmail($this->emarsys_template_list["RECEIVER_REBATE_TRANSFER"], $receiver_email, $receive_email_data);

        }
        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
        $this->updateOrInsertMbr($transfer_id, $this->_accessPass);
        Redis::del("REBATESESSION" . $token);

        return $result;
    }

    public function myPurchases(Request $request)
    {
//        ini_set("display_errors", 1);

        $mbr_id = $this->filterInput($request->mbr_id);
        if (!$mbr_id) {
            $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        $mbr_id2 = isset($request->mbr_id2) ? $request->mbr_id2 : '';
        if ($mbr_id2 != '') {
            $mbr_id = $mbr_id2;
        }

        $pageno_from = isset($request->pageno) ? (int)$request->pageno : 0;
        $tfrom = $request->tfrom ? $request->tfrom : '';

        $pageno_to = 5;
        if (!$pageno_from) {
            $pageno_from = 0;
        }

        $fdate = $request->fdate;
        $tdate = $request->tdate;
        if ($fdate && $tdate) {

        } else {
            $fdate = Carbon::now()->modify('-2 years')->format('Y-m-d');
            $tdate = Carbon::now()->format('Y-m-d');
        }

        $coy_id = 'CTL';

        $ctlInvType = ['PS', 'RF', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC', 'OS', 'EX', 'E1'];
        $hsgInvType = ['HI', 'HR', 'OS'];

        $transType = ['HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'HR', 'PU', 'ID', 'DZ', 'DU', 'IC', 'EX', 'E1'];
        if ($tfrom === 'CTL') {
            $transType = ['PS', 'RF', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC', 'OS', 'EX', 'E1'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI', 'HR', 'OS'];
        }

        $transactions = $this->vcmemberRepository->getTransSql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate, $ctlInvType, $hsgInvType, $tfrom, $coy_id);

        return response()->json($transactions, 200);
    }

    public function myPurchasesGet($mbr_id, $fdate = '', $tdate = '', $filter = '', $page_no = '')
    {
        if (!$mbr_id) {
            $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        if ($fdate && $tdate) {
        } else {
            $fdate = Carbon::now()->modify('-2 years')->format('Y-m-d');
            $tdate = Carbon::now()->format('Y-m-d');
        }

        $coy_id = 'CTL';
        $tfrom = $filter;

//        echo $page_no ."<br/>";

        if ($page_no || $page_no === '0') {
            $pageno_from = (int)$page_no;
            $pageno_to = (int)$pageno_from + 5;
        } else {
            $pageno_from = 0;
            $pageno_to = 99999;
        }

//        echo $pageno_from ."-". $pageno_to. "<br/>";

        $ctlInvType = ['PS', 'RF', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC', 'OS', 'EX', 'E1'];
        $hsgInvType = ['HI', 'HR'];

        $transType = ['HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'HR', 'PU', 'ID', 'DZ', 'DU', 'IC', 'EX', 'E1'];
        if ($tfrom === 'CTL') {
            $transType = ['PS', 'RF', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC', 'OS', 'EX', 'E1'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI', 'HR'];
        }

        $transactions = $this->vcmemberRepository->getTransSql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate, $ctlInvType, $hsgInvType, $tfrom, $coy_id);

//        echo count($transactions);

        return response()->json($transactions, 200);
    }

    public function myRedemptionGet($mbr_id = '', $fdate = '', $tdate = '', $page_no = '')
    {
        if (!$mbr_id) {
            $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        if ($fdate && $tdate) {

        } else {
            $fdate = Carbon::now()->modify('-2 years')->format('Y-m-d');
            $tdate = Carbon::now()->format('Y-m-d');
        }

        if ($page_no || $page_no === '0') {
            $pageno_from = (int)$page_no;
            $pageno_to = (int)$pageno_from + 5;
        } else {
            $pageno_from = 0;
            $pageno_to = 99999;
        }

        $coy_id = 'CTL';

        $redemptions = $this->vcmemberRepository->getRedemptionSql($mbr_id, $pageno_from, $pageno_to, $fdate, $tdate, $coy_id);

        return response()->json($redemptions, 200);
    }

    public function myRedemptions(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        $pageno_from = $request->pageno;
        $tfrom = $request->tfrom ? $request->tfrom : '';

        $pageno_to = $pageno_from + 5;
        if (!$pageno_from) {
            $pageno_from = 0;
            $pageno_to = 99999;
        }

        $fdate = $request->fdate;
        $tdate = $request->tdate;
        if ($fdate && $tdate) {

        } else {
            $fdate = Carbon::now()->modify('-2 years')->format('Y-m-d');
            $tdate = Carbon::now()->format('Y-m-d');
        }

        $coy_id = 'CTL';

        $redemptions = $this->vcmemberRepository->getRedemptionSql($mbr_id, $pageno_from, $pageno_to, $fdate, $tdate, $coy_id);

        return response()->json($redemptions, 200);
    }

    public function myWarranty(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        $pageno_from = $request->pageno;
        $tfrom = $request->tfrom ? $request->tfrom : '';

        $pageno_to = $pageno_from + 5;
        if (!$pageno_from) {
            $pageno_from = 0;
            $pageno_to = 99999;
        }

        $fdate = $request->fdate;
        $tdate = $request->tdate;


        $transType = ['HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'PU', 'HR', 'ID', 'DZ', 'DU', 'IC'];
        if ($tfrom === 'CTL') {
            $transType = ['PS', 'RF', 'OS', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI', 'HR'];
        }

        $warrantys = $this->getWarrantySql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate);

        return response()->json($warrantys, 200);
    }

    public function myDigitalDownload(Request $request)
    {
        $mbr_id = trim($request->mbr_id);
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        $pageno_from = $request->pageno;
        $tfrom = $request->tfrom ? $request->tfrom : '';

        $pageno_to = $pageno_from + 5;
        if (!$pageno_from) {
            $pageno_from = 0;
            $pageno_to = 99999;
        }


        $fdate = $request->fdate;
        $tdate = $request->tdate;


        $transType = ['HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'PU', 'HR', 'ID', 'DZ', 'DU', 'IC'];
        if ($tfrom === 'CTL') {
            $transType = ['PS', 'RF', 'OS', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI', 'HR'];
        }

        $digitals = $this->getDigitalDownloadSql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate);

        return response()->json($digitals, 200);
    }


    //Add All 4 API.
    public function myHistory2(Request $request)
    {
        ini_set("display_errors", 1);
        $mbr_id = $this->filterInput($request->mbr_id);
        if (!$mbr_id) {
            return $this->apiSuccessResponse(0, "Invalid Session, no member session is found", []);
        }

        $pageno_from = $this->filterInput($request->pageno);
        $tfrom = $request->tfrom ? $this->filterInput($request->tfrom) : '';

        $pageno_to = 5;
        if (!$pageno_from) {
            $pageno_from = 0;
        }


        $fdate = $this->filterInput($request->fdate);
        $tdate = Carbon::parse($request->tdate)->addDays(1)->format("Y-m-d");


        $coy_id = $this->CTL_COY_ID;

        $ctlInvType = ['PS', 'RF', 'EX', 'PU', 'ID', 'DZ', 'DU', 'VC', 'EX', 'E1'];
        $hsgInvType = ['HI', 'HR', 'OS'];

        $transType = ['HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'HR', 'PU', 'ID', 'DZ', 'DU', 'IC', 'EX', 'E1'];
        if ($tfrom === 'CTL') {
            $transType = ['PS', 'RF', 'VC', 'EX', 'PU', 'ID', 'DZ', 'DU', 'IC', 'EX', 'E1'];
        } else if ($tfrom === 'HSG') {
            $transType = ['HI', 'HR', 'OS'];
        }

        //Add To Member_datatype.

        $transactions = $this->vcmemberRepository->getTransSql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate, $ctlInvType, $hsgInvType, $tfrom, $coy_id);

        $warrantys = $this->getWarrantySql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate);

        $redemptions = $this->vcmemberRepository->getRedemptionSql($mbr_id, $pageno_from, $pageno_to, $fdate, $tdate, $coy_id);

        $digitals = $this->getDigitalDownloadSql($mbr_id, $pageno_from, $pageno_to, $transType, $fdate, $tdate);

        $returnJson = array(
            "trans" => $transactions,
            "warranty" => $warrantys,
            "redemptions" => $redemptions,
            "digitals" => $digitals
        );
        return response()->json($returnJson, 200);
    }

    public function getDigitalDownloadSql($mbr_id, $page_from, $page_to, $transType, $fdate, $tdate)
    {
        $redisKey = "#myDigitals#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate;
        $transactions_redis = $this->vcmemberRepository->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }

        $trans_lists = $this->db20->table('crm_member_transaction as a')
            ->select(DB::raw('DISTINCT(trans_id)'), 'trans_date', 'trans_type')
            ->where('mbr_id', $mbr_id)
            ->whereIn('trans_type', $transType)
            ->whereBetween("trans_date", [$fdate, $tdate])
            ->orderBy('trans_date', 'desc')
            ->get();


        $lists = array();
        if (count($trans_lists) > 0) {

            $url = $this->imsUrl . 'vcapp/getdigitaldownload';

            $post_data = array(
                "page_from" => 1,
                "page_to" => 6,
                "mbr_id" => $mbr_id,
                "trans_lists" => $trans_lists
            );

            $lists = $this->curlImsApi($url, $post_data);
        }

        $this->vcmemberRepository->IsPutRedis($redisKey, $lists, $mbr_id);

        return $lists;
    }

    public function getWarrantySql($mbr_id, $page_from, $page_to, $transType, $fdate, $tdate)
    {
        $redisKey = "#myWarranty#" . $mbr_id . (string)$page_from . (string)$page_to . $fdate . "_" . $tdate;
        $transactions_redis = $this->vcmemberRepository->CheckIsRedis($redisKey, $mbr_id);
        if ($transactions_redis !== null) {
            return $transactions_redis;
        }


        $trans_lists = $this->db20->table('crm_member_transaction as a')
            ->select(DB::raw('DISTINCT(trans_id)'), 'trans_date', 'trans_type')
            ->where('mbr_id', $mbr_id)
            ->whereIn('trans_type', $transType)
            ->whereRaw(" LEFT(trans_id, 3) != 'NEW'")
            ->whereBetween("trans_date", [$fdate, $tdate])
            ->whereRaw("a.item_id != '!MEMBER-HACON'")
            ->whereRaw(" a.item_id not like '#%'")
            ->whereRaw("(a.item_id not like '!%' or a.item_id LIKE '!MEMBER-%' or a.item_id LIKE '!EGIFT-%')")
            ->orderBy('trans_date', 'desc')
            ->get();


        $lists = array();
        if (count($trans_lists) > 0) {

            $url = $this->imsUrl . 'vcapp/getwarrantylists';

//            echo $url;

            $post_data = array(
                "page_from" => 1,
                "page_to" => 6,
                "mbr_id" => $mbr_id,
                "trans_lists" => $trans_lists
            );

            $lists = $this->curlImsApi($url, $post_data);

        }

        $this->vcmemberRepository->IsPutRedis($redisKey, $lists, $mbr_id);


        return $lists;
    }


    public function getRedeemItemLists(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $min_price = $request->min_price;
        $max_price = $request->max_price;


        $url = $this->imsUrl . "vcapp/getredeemlists";


        $post_data = array(
            "mbr_id" => $mbr_id,
            "min_price" => $min_price,
            "max_price" => $max_price
        );

        $datas = $this->curlImsApi($url, $post_data);

        if (!$datas) {
            $results = array();
        } else {
            $results = $datas;
        }

//        print_r($datas);

        return $this->apiSuccessResponse(1, "OK", $results);
    }

    public function getStarRedemptionStock(Request $request)
    {

        $item_id = $request->item_id;

        $postdata = array(
            "item_id" => $item_id
        );

        $url = $this->imsUrl . "vcapp/getproductstock";
        $stores = $this->curlImsApi($url, $postdata);
//        print_r($stores);
//        die();

        $url2 = $this->imsUrl . "vcapp/getstarredemptionstock";
        $items = $this->curlImsApi($url2, $postdata);

//        print_r($items);
//        die();

        foreach ($items as $key => $item) {

            $loc_s_id = $item["loc_id"];
            $loc_id = explode('-', $loc_s_id)[0];
            $qty_left = (int)$item["qty_on_hand"] - (int)$item["qty_reserved"];

            for ($i = 0; $i < count($stores); $i++) {
                if ($loc_id == $stores[$i]['cname']) {
                    $items[$key]['title'] = $stores[$i]['title'];
                    $items[$key]['opening_hours'] = $stores[$i]['opening_hours'];
                    $items[$key]['opening_hours2'] = $stores[$i]['opening_hours2'];
                    $items[$key]['loc_id'] = $loc_id;
                    break;
                }
            }

            $items[$key]['inv_desc'] = "Available";
            $items[$key]['color'] = "#3ca400";
        }


        return $this->apiSuccessResponse(1, "Success", $items);
    }

    public function doStarRedemptionSummary(Request $request)
    {
        $coy_id = $this->CTL_COY_ID;
        $mbr_id = $request->mbr_id;
        $item_id = trim($request->item_id);
        $item_desc = $request->item_desc;
        $redeem_price = (float)$request->redeem_price;
        $unit_price = $request->unit_price;
        $loc_id = $request->loc_id;

        $scl_loc_id = $request->scl_loc_id;
        $delv_mode = $request->delv_mode;
        $delv_address = $request->delv_address;

        $the_loc_id = 'HCL-S';
        $image_name = $request->image_name;
        $regular_price = $request->unit_price;


        $sql_cust = <<<str
	select first_name, last_name, points_accumulated, contact_num,
	RTRIM(email_addr) as email_addr from crm_member_list where mbr_id= ? and status_level = 1
str;
        $members = $this->db20->select($sql_cust, [$mbr_id]);
//        print_r($members);
        if (count($members) === 0) {
            return $this->apiSuccessResponse(0, "Sorry, you are not eligible to redeem the item.", []);
        }

        //Validate Item 1st.
        $url = $this->imsUrl . "vcapp/validateredeemitem";
        $post_data = array(
            "item_id" => $item_id,
            "redeem_price" => $redeem_price
        );
        $validates = $this->curlImsApi($url, $post_data);
//        print_r($datas);
//        die();

        if (count($validates) === 0) {
            return $this->apiSuccessResponse(0, "Sorry, you are not eligible to redeem the item now.", []);
        }
//        print_r($validates);
//        die();


        $redeem_float = ((float)$this->vcmemberRepository->getAvailablePoints($mbr_id, $coy_id)) / 100;
        if ($redeem_float < (float)$redeem_price) {
            return $this->apiSuccessResponse(0, "Insufficient rebates.", []);
        }

        $year = date("Y");
        $trans_type = 'OD';

//        $rr_trans_id = $this->vcmemberRepository->getNextTransIdNo($trans_type, 8);
//        $rr_trans_id= 'ODD00344';

        if ($delv_mode == 'SCL') {

            $cust = $this->vcmemberRepository->stdToArray($members[0]);

            $cust_name = substr($cust["first_name"] . ' ' . $cust["last_name"], 0, 30);
            $first_name = $cust["first_name"];
            $last_name = $cust["last_name"];
            $email_addr = $cust["email_addr"];
            $tel_code = $cust["contact_num"];

            $sql_addr = <<<str
	select * from coy_address_book where ref_id=? and addr_type='0-PRIMARY';
str;
            $addresses = $this->db20->select($sql_addr, [$mbr_id]);

            if (count($addresses) > 0) {
                $address = $this->vcmemberRepository->stdToArray($addresses[0]);

                $postal_code = $address["postal_code"] ? $address["postal_code"] : '';
                $street_line1 = $address["street_line1"] ? $address["street_line1"] : '';
                $street_line2 = $address["street_line2"] ? $address["street_line2"] : '';
                $street_line3 = $address["street_line3"] ? $address["street_line3"] : '';
                $street_line4 = $address["street_line4"] ? $address["street_line4"] : '';
            } else {
                $postal_code = '';
                $street_line1 = '';
                $street_line2 = '';
                $street_line3 = '';
                $street_line4 = '';
            }

        } else {

            $tel_code = $delv_address["contact_num"];

//            $street_line1 = $delv_address["floor_no"];
//            $street_line2 = $delv_address["unit_no"];
//            $street_line3 = $delv_address["addr_line1"];
//            $street_line4 = $delv_address["addr_line2"];

            $street_line1 = '#' . $delv_address["floor_no"] . '-' . $delv_address["unit_no"];
            $street_line2 = $delv_address["addr_line1"] . ', ' . $delv_address["addr_line2"];
//            $street_line3 = $delv_address["addr_line1"];
//            $street_line4 = $delv_address["addr_line2"];

            $first_name = $delv_address["first_name"];
            $last_name = $delv_address["last_name"];

            $cust_name = substr($first_name . ' ' . $last_name, 0, 30);

            $addr_type = $delv_address["addr_type"];
            $email_addr = $delv_address["email_addr"];
            $postal_code = $delv_address["postal_code"];

        }

//        $cust_po_id = $rr_trans_id;

        $discount_amt = (int)$unit_price - (int)$redeem_price;
        $trans_points = $redeem_price * -100;
        $rebates_total = $redeem_price * 100;
        $discount_perc = number_format(($discount_amt / $unit_price) * 100, 2);


        //Insert the Invoices.

//        $url_api = $this->imsUrl . "vcapp/insertstarredemption";
//        $post_data2 = array(
//            "item_id" => $item_id,
//            "item_desc" => $item_desc,
//            "discount_amt" => $discount_amt,
//            "discount_perc" => $discount_perc,
//            "the_loc_id" => $the_loc_id,
//            "scl_loc_id" => $scl_loc_id,
//            "unit_price" => $unit_price,
//            "rr_trans_id" => $rr_trans_id,
//            "trans_type" => $trans_type,
//            "mbr_id" => $mbr_id,
//            "tel_code" => $tel_code,
//            "cust_name" => $cust_name,
//            "email_addr" => $email_addr,
//            "street_line1" => $street_line1,
//            "street_line2" => $street_line2,
//            "street_line3" => $street_line3,
//            "street_line4" => $street_line4,
//            "postal_code" => $postal_code,
//            "delv_mode" => $delv_mode,
//            "cust_po_id" => $cust_po_id,
//            "first_name" => $first_name,
//            "last_name" => $last_name,
//            "redeem_price" => $redeem_price
//        );

        if ($this->ENVIRONMENT == 'development') {
            $url_api = "http://chappos-api.sghachi.com/orders/od/save";
        } else {
            $url_api = "https://pos8.api.valueclub.asia/orders/od/save";
        }

        $post_data2 = array(
            "mbr_id" => $mbr_id,
            "delv_mode" => 'STD',
            "delv_addr" => array(
                "street_line1" => $street_line1,
                "street_line2" => $street_line2,
                "postal_code" => $postal_code
            ),
            "items" => [
                array(
                    "item_id" => $item_id,
                    "item_desc" => $item_desc,
                    "regular_price" => $unit_price,
                    "unit_price" => $redeem_price,
                    "disc_percent" => $discount_perc,
                    "image_name" => $image_name,
                    "loc_id" => "HCL-S",
                    "qty_available" => "1.0000"
                )
            ],
            "staff_id" => "3561",
            "loc_id" => "HCL-S",
            "pos_id" => ""
        );

//        print_r($post_data2);
//        die();

        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
        );

//        echo $url_api;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_api);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data2));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
//        print_r($result);
//        die();

        $resp = json_decode($result, true);
        curl_close($ch);

//        print_r($resp);
//        die();

        if (!isset($resp) || !$resp) {
            return $this->apiSuccessResponse(0, "Sorry, you are not eligible to redeem this item.", []);
        } else {

            $rr_trans_id = $resp["invoice"];

//            echo "Ok";
//            echo $rr_trans_id;
//            die();

            //Deduct the Points.
//            $yearMonth = date("Ym");
//            $trans_prefix = 'ADJUSTAP' . $yearMonth;
//            $so_id_header = 'AP' . $yearMonth;
//
//            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header);
//
//            $func = "points/deduct";
//            $post_data = array(
//                "mbr_id" => $mbr_id,
//                "trans_id" => $ap_trans_id,
//                "rebate_amount" => -$redeem_price,
//                "rebate_description" => "Star Redemption Deduct Points: " . $rebates_total
//            );
//
//            $res = $this->curlCHPos($func, $post_data);
//            $this->_logprocess('SR_POINT_DEDUCT', $redeem_price, json_encode($res), $mbr_id, $ap_trans_id);


            //Reserve The Qty.
            $apiUrl = $this->imsUrl;

            $url_reserve = $apiUrl . "confirm_reserved";
            $doc_type = 'ORDER';
            $move_type = 'OD';

            $item = array();
            $item["item_id"] = trim($item_id);
            $item["line_num"] = 1;
            $item["qty_reserved"] = 1;
            $reserve_items[] = $item;

            $post_data1 = array(
                "coy_id" => $this->CTL_COY_ID,
                "loc_id" => 'HCL-S',
                "doc_type" => $doc_type,
                "trans_type" => $move_type,
                "trans_id" => $rr_trans_id,
                "modified_by" => $mbr_id,
                "items" => $reserve_items
            );
            $res1 = $this->curlLogisticsApi($url_reserve, $post_data1);

            $this->_logprocess('SR_RESERVED', '1', json_encode($res1), $rr_trans_id, $mbr_id);

            //Insert Crm_member_transaction.
//            $sql_item_desc = substr($item_desc, 0, 60);
//            $sql_trans = <<<str
//INSERT INTO crm_member_transaction
//				(coy_id, mbr_id, trans_id, line_num, trans_type, trans_date, loc_id, pos_id, item_id , item_desc  ,item_qty, regular_price, unit_price,
//					disc_percent, disc_amount, trans_points, salesperson_id, mbr_savings, created_by, created_on, modified_by, modified_on, updated_on)
//	VALUES ('CTL' , '$mbr_id', '$rr_trans_id' , 1 , '$trans_type', current_timestamp , '$scl_loc_id', '', '$item_id', '$sql_item_desc', 1, '$unit_price' ,'$unit_price',
//		'$discount_perc', '$discount_amt' , '0', '',0, 'VcApp' , NOW(),'VcApp' , NOW(), NOW());
//str;
//            $this->db20->statement($sql_trans, []);
//
//            $sql_rebate = <<<str
//		INSERT INTO crm_member_transaction
//				(coy_id, mbr_id, trans_id, line_num, trans_type, trans_date, loc_id, pos_id, item_id , item_desc  ,item_qty, regular_price, unit_price,
//					disc_percent, disc_amount, trans_points, salesperson_id, mbr_savings, created_by, created_on, modified_by, modified_on, updated_on)
//	VALUES ('CTL' , '$mbr_id', '$rr_trans_id' , 2 , '$trans_type', current_timestamp , '$scl_loc_id', '', '!VCH-STAR001', '$0.01 Star Rewards Voucher', '$rebates_total',  1,1,
//		0, 0 , '$trans_points', '',0, 'VcApp' , NOW(),'VcApp' , NOW(), NOW());
//str;
//            $this->db20->statement($sql_rebate, []);

        }


        //Clear all necessary redis/cache.
        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        $this->clearRedisFields($mbr_id, '#myRedemption#', $this->AUTOLOGIN_TOKEN, false);
        $this->clearRedisFields($mbr_id, "#myPurchases#", $this->AUTOLOGIN_TOKEN, false);

        //Return Value.

        $sub = substr(trim($rr_trans_id), -3, 3);
        $signature = md5($rr_trans_id . '@Challenger.' . $sub);
        $url_link = "https://www.challenger.com.sg/invoice/trusted/" . $rr_trans_id . "?ereceipt=y&signature=" . $signature;

        $resp = array(
            "trans_id" => $rr_trans_id,
            "url_link" => $url_link
        );
        return $this->apiSuccessResponse(1, "Ok.", $resp);

    }


    public function getRedeemHCLItemLists(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $rebates = $request->rebates;


        if ($this->ENVIRONMENT == 'development') {
            $url_api = "http://chappos-api.sghachi.com/star_redemption?loc_id=BF&mbr_id=" . $mbr_id . "&rebates=" . $rebates;
        } else {
            $url_api = "https://pos8.api.valueclub.asia/star_redemption?loc_id=HCL&mbr_id=" . $mbr_id . "&rebates=" . $rebates;
        }

        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3'
        );

//        echo $url_api;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_api);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
//        print_r($result);
//        die();

        $resp = json_decode($result, true);
        curl_close($ch);

        if (!isset($resp) || !isset($resp["data"])) {
            return $this->apiSuccessResponse(0, "Sorry, No item to redeem now.", []);
        } else {

            $lists = $resp["data"];

            $data = array();

            foreach ($lists as $list) {
                if ($list["item_desc"]) {
                    $data[] = $list;
                }
            }

            return $this->apiSuccessResponse(1, "Success", $data);
        }

    }

    public function starRedemptionPayment(Request $request)
    {
        $txn_id = $this->vcmemberRepository->getNextInvoiceNo();
        $key = $this->server_name . "#payment" . $txn_id;
        $date = Carbon::now();

        $data = array();
        $mbr_id = $request->mbr_id;
        $email_addr = $request->email_addr;

        $data["mbr_id"] = $mbr_id;
        $data["email"] = $email_addr;


        $amount = "8.00"; //"30.00";
        $amount_str = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);

//        return $mbrId;

        $data = array(
            "fn" => "sr",
            "transcode" => $txn_id,
            "description" => "Star Redemption Payment.",
            "amt" => $amount_str,
            "cardholderName" => "",
            "cardholderEmail" => $data["email"],
            "callbackUrl" => "api/vc2/callback_sr",
            "callbackUrl2" => "callback_sr",
            "data" => $data, // data submitted
            "txnDate" => $date->format('Y-m-d H:i:s'),
            "txnRef" => $txn_id,
            "mbr_id" => $data["mbr_id"],
            "status" => "", // to be replaced in callback
            "payment" => floatval($amount),
            "txnDatetime" => $date->format('Y-m-d H:i:s'),
            "txnTime" => $date->format('H:i:s'),
            "ClientType" => "vcapp", // new member
            "authenticity_token" => "",
            "code" => "",
            "mid" => "", // 2C2P / BRAINTREE etc.
            "email_addr" => $data["email"],
            "errorCode" => "",
            "paytype" => $this->payTypeDefault
        );

        $txn_id = $this->base64url_encode($txn_id);
        $data = base64_encode(json_encode($data));
        if ($this->redisSetAndLog($mbr_id, $key, $data, "SR")) {
            Redis::expire($key, 3600); // testing 5 minutes
            return $this->apiSuccessResponse(1, "success", array("paytype" => $this->payTypeDefault, "txn" => $txn_id));
        }

    }


    public function getInvoicePdf(Request $request)
    {
        $invoice_id = $request->input('tr');
        $token = $request->input('h');

        $pdfUrl = $this->pdfUrl;

        $url = "https://www.hachi.tech/invoice/trusted/";

//        echo $invoice_id;
        $sub = substr(trim($invoice_id), -3, 3);
        $signature = md5($invoice_id . '@Challenger.' . $sub);
        $invoice_url = $url . $invoice_id . "?signature=" . $signature;

        return redirect()->to($invoice_url);
    }

    public function myCoupons(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $fdate = isset($request->fdate) ? Carbon::parse($request->fdate)->format("Y-m-d") : '';
        $tdate = isset($request->tdate) ? Carbon::parse($request->tdate)->format("Y-m-d") : '';

        if (!$fdate || !$tdate) {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }


        $coy_id = $this->CTL_COY_ID;

        $page_from = isset($request->pageno) ? $request->pageno : 0;
        $page_to = ($page_from || $page_from === 0) ? ($page_from + 5) : 9999;

        $vouchers = $this->vcmemberRepository->getMyVoucher($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to);

        $e_pageno_from = isset($request->pageno) ? $request->pageno : 0;
        $e_pageno_to = ($e_pageno_from || $e_pageno_from === 0) ? ($e_pageno_from + 5) : 9999;

        $gifts = $this->vcmemberRepository->getMyGifts($mbr_id, $coy_id, $fdate, $tdate, $e_pageno_from, $e_pageno_to);

        $c_pageno_from = isset($request->pageno) ? $request->pageno : 0;
        $c_pageno_to = ($c_pageno_from || $c_pageno_from === 0) ? ($c_pageno_from + 5) : 9999;

        $credits = $this->vcmemberRepository->getMyCredits($mbr_id, $coy_id, $fdate, $tdate, $c_pageno_from, $c_pageno_to);

        $returnJson = array(
            "vouchers" => $vouchers,
            "gifts" => $gifts,
            "credits" => $credits
        );

        return response()->json($returnJson, 200);
    }

    public function myVouchersGet($mbr_id, $orderby = '', $desc = '', $fdate = '', $tdate = '')
    {
        if ($fdate && $tdate) {

        } else {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }

        $coy_id = $this->CTL_COY_ID;

        $c_pageno_from = 0;
        $c_pageno_to = 9999;

        $credits = $this->vcmemberRepository->getMyVoucher($mbr_id, $coy_id, $fdate, $tdate, $c_pageno_from, $c_pageno_to, $orderby, $desc);

        return response()->json($credits, 200);
    }

    public function myVouchers(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $fdate = isset($request->fdate) ? Carbon::parse($request->fdate)->format("Y-m-d") : '';
        $tdate = isset($request->tdate) ? Carbon::parse($request->tdate)->format("Y-m-d") : '';

        if (!$fdate || !$tdate) {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }

        $coy_id = $this->CTL_COY_ID;

        $c_pageno_from = isset($request->pageno) ? $request->pageno : 0;
        $c_pageno_to = ($c_pageno_from || $c_pageno_from === 0) ? ($c_pageno_from + 5) : 9999;

        $credits = $this->vcmemberRepository->getMyVoucher($mbr_id, $coy_id, $fdate, $tdate, $c_pageno_from, $c_pageno_to);

        return response()->json($credits, 200);
    }

    public function myeCredits(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $fdate = isset($request->fdate) ? Carbon::parse($request->fdate)->format("Y-m-d") : '';
        $tdate = isset($request->tdate) ? Carbon::parse($request->tdate)->format("Y-m-d") : '';

        if (!$fdate || !$tdate) {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }

        $coy_id = $this->CTL_COY_ID;

        $c_pageno_from = isset($request->pageno) ? $request->pageno : 0;
        $c_pageno_to = ($c_pageno_from || $c_pageno_from === 0) ? ($c_pageno_from + 5) : 9999;

        $credits = $this->vcmemberRepository->getMyCredits($mbr_id, $coy_id, $fdate, $tdate, $c_pageno_from, $c_pageno_to);

        return response()->json($credits, 200);
    }

    public function myeGift(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $fdate = isset($request->fdate) ? Carbon::parse($request->fdate)->format("Y-m-d") : '';
        $tdate = isset($request->tdate) ? Carbon::parse($request->tdate)->format("Y-m-d") : '';

        if (!$fdate || !$tdate) {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }

        $coy_id = $this->CTL_COY_ID;

        $e_pageno_from = isset($request->pageno) ? $request->pageno : 0;
        $e_pageno_to = ($e_pageno_from || $e_pageno_from === 0) ? ($e_pageno_from + 5) : 9999;


        $gifts = $this->vcmemberRepository->getMyGifts($mbr_id, $coy_id, $fdate, $tdate, $e_pageno_from, $e_pageno_to);

        return response()->json($gifts, 200);
    }

    private function _sendEmarsysEmail($template_id, $email_addr, $params, $trans_id = '', $return_response = false, $invoice_log_id = '')
    {
        if ($this->ENVIRONMENT) {

        }

        $url = $this->vcapiUrl . "api/emarsys/" . $template_id;
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->emarsys_authorization_bearer
        );

        $post_data = array(
            "source" => "hachi",
            "trans_id" => $trans_id,
            "email" => $email_addr,
            "data" => $params
        );

//        print_r($post_data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

        $result = curl_exec($ch);
//        print_r($result);

        curl_close($ch);

        $response = json_decode($result, true);
        return $response;
    }


    public function testEmarsys()
    {
        $template_id = $this->emarsys_template_list["UPDATE_PROFILE"];

        $url = "https://chvc-api.sghachi.com/api/emarsys/" . $template_id;
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->emarsys_authorization_bearer
        );

        $post_data = array(
            "source" => "hachi",
            "trans_id" => "",
            "email" => "wong.weiseng@challenger.sg",
            "data" => [
                "r_fname" => "WEI S"
            ]
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($result, true);
        return $response;
    }

    public function bitlyl($key)
    {
        $link = Redis::hget($this->hkey, $key);
        if ($link) {
            $url = base64_decode($link);
            return redirect($url);
        }
    }

    private function shortenUrlCurl($data)
    {
        $link = $data["longUrl"];
        $expire_seconds = 604800; // 7 days
        $key1 = md5($link);
        $uniq = uniqid();
        $key2 = substr(md5($link . $uniq), 0, 6);
        $hkey = $this->hkey;
        $cachedata = Redis::hget($hkey, $key1);
        if (!$cachedata) {
            $link = base64_encode($link);
            Redis::hset($hkey, $key1, $key2);
            Redis::hset($hkey, $key2, $link);
            Redis::expire($hkey, $expire_seconds);
        } else {
            $key2 = Redis::hget($hkey, $key1);
        }
        if ($this->ENVIRONMENT == 'development') {
            $url = $this->vcapiUrl . "api/vc2/bitlyl/$key2";
        } else {
            $url = $this->vcapiUrl . "api/vc2/bitlyl/$key2";
        }

        return $url;
    }

    public function getCouponList(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $lists = $this->vcmemberRepository->getCouponQuery($mbr_id);

        return $this->apiSuccessResponse(1, 'Success', $lists);
    }

    public function getInsertVoucher(Request $request)
    {
        $mbr_id = trim(strtoupper($request->user()->mbr_id));
        $voucher_id = trim($request->voucherid);

        //TODO:: insert Voucher.
        $result = $this->vcmemberRepository->validateAddVoucher($voucher_id, $mbr_id);

        $lists = $this->vcmemberRepository->getCouponQuery($mbr_id);

        $this->removeHashRedis($mbr_id);

        return $this->apiSuccessResponse(1, 'Success', $lists);
    }

    public function removeHashRedis($mbr_id)
    {
        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id;
        Redis::del($hkey);
    }

    public function getEasyInkLists(Request $request)
    {
        $mbr_id = trim($request->mbr_id);

        $devices = $this->getEasyInkDatas($mbr_id);

        if (!$devices) {
            return $this->apiSuccessResponse(1, "Success 1", []);
        }

//        print_r($devices);
//        die();

        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            return $this->apiSuccessResponse(0, "Unable to Get Access Token", "Unable to Get Access Token");
        }
        $access_token = $access_response["access_token"];

        $result = [];
        $index_key = trim($mbr_id);
        $sql = <<<str
    select * from "members_meta" where key= ? or key= ?
str;
        $hr_lists = $this->chapps->select($sql, [$index_key, $index_key . '-1']);
//        print_r($hr_lists);
//        die();

        if (count($hr_lists) > 0) {
            foreach ($hr_lists as $hr_list) {
                if ($hr_list) {
                    //Got hr id, get the subscribe lists.
                    $hr_id = $hr_list->meta;
                    $response = $this->getHpInkDeviceStatus($hr_id, $access_token);
//            print_r($response);
//            die();

                    $result["hr_id"] = isset($response["hp_sr_id"]) ? $response["hp_sr_id"] : '';
                    $result["status"] = 'Subscribed';


                    if (isset($response["printers"]) && count($response["printers"]) > 0) {
                        foreach ($response["printers"] as $ind => $printer) {
//                            $status = $printer["status"];
                            $modelNum = $printer["modelNumber"];
                            $serialNum = $printer["serialNumber"];


                            foreach ($devices as $index => $device) {
//                                print_r($device);
//                                echo "<br/>";
//                                print_r($modelNum);
//                                echo "-";
//                                print_r($serialNum);
                                if (trim($device["model_id"]) == $modelNum && trim($device["serial_no"]) == $serialNum) {
                                    $obj_merged = array_merge($device, $printer);
                                    $devices[$index] = $obj_merged;
                                }

                            }
                        }
                    }

                    $result["devices"] = $devices;

                }
            }
        } else {
            //Never Enroll any printer before.
            $result["devices"] = $devices;
            $result["hr_id"] = '';
            $result["status"] = 'Unsubscribe';
        }


        return $this->apiSuccessResponse(1, "Success", $result);
    }

    private function getHpInkDeviceStatus($hp_sr_id, $access_token)
    {
        $partnerId = $this->hpPartnerId;

        if ($this->ENVIRONMENT == "production") {
            $url = "https://acr.ext.hp.com/acr/v1/devices/$partnerId/$hp_sr_id";
        } else {
            $url = "https://acr-dev.ext.hp.com/acr/v1/devices/$partnerId/$hp_sr_id";
        }

//        echo $url;
        //For Testing
//        $url = "https://acr.ext.hp.com/acr/v1/devices/$partnerId/$hp_sr_id";

        $headers = array(
            'Authorization: Bearer ' . $access_token
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, 1);
        return count($response) > 0 ? $response[0] : array();
    }

    private function getEasyInkDatas($mbr_id)
    {
        if ($this->ENVIRONMENT == 'development') {
            $url = "http://chappos-api.sghachi.com/api/report/hpinklist";
        } else {
            $url = "https://pos8.api.valueclub.asia/api/report/hpinklist";
        }

        $item_lists = $this->vcmemberRepository->curlPosApi($url, [], false);
//        print_r($item_lists);

        $lists2 = $this->getEasyInkImsLists($mbr_id, $item_lists);

        $lists = $this->db20->table('crm_hp_printer_list')
            ->select('item_desc', 'item_id', DB::raw("case when subscribe_mode ='manual' then 'P2-Manual' else 'P2-Scanned' end as subscribe_mode",
                DB::raw("0 as line_num")),
                'invoice_id', 'model_id', DB::raw("case when status_level= 0 THEN '' ELSE cast(status_level as varchar(10)) END as status"), 'img_name',
                DB::raw("RTRIM(lot_id) as serial_no"), 'created_on', 'created_on as updatedAt')
            ->where('mbr_id', $mbr_id)
            ->where('status_level', '>=', '0')
            ->get();

        $lists = $this->vcmemberRepository->stdToArray($lists);

        $result = array_merge($lists2, $lists);
        usort($result, function ($a, $b) {
            return strcmp($b["created_on"], $a["created_on"]);
        });


        //Get Comp Item Id Lists
        foreach ($result as $index => $list) {
            $item_id = $list["item_id"];
            $comp_lists = array();
            foreach ($item_lists as $item_list) {
                if ($item_list["item_id"] == $item_id) {
                    $comp_lists[] = $item_list;
                }
            }
            $result[$index]["comp_lists"] = $comp_lists;
        }

        return $result;

    }

    private function getEasyInkImsLists($mbr_id, $item_lists)
    {
//        print_r($item_lists);
//        die();
        $item_strs = $this->formatItemUPCtoArray($item_lists);

//        return($item_strs);
//        die();

        $url = $this->imsUrl . "vcapp/geteasyinklists";

//        echo $url;

        $post_data = array(
            "mbr_id" => $mbr_id,
            "item_str" => $item_strs
        );

        $datas = $this->curlImsApi($url, $post_data);

        if (!$datas) {
            return array();
        }
        return $datas;
    }

    public function putHpInkSubscribeStatus(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $hr_id = $request->hr_id;
        $model_id = $request->model_id;
        $serial_no = $request->serial_no;
        $status_level = $request->status;

        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            return $this->apiSuccessResponse(0, "Failed", "Unable to Get Access Token");
        }
        $access_token = $access_response["access_token"];
        $partnerId = $this->hpPartnerId;

        if ($this->ENVIRONMENT == "production") {
            $url = "https://acr.ext.hp.com/acr/v1/devices/$partnerId/$hr_id/$serial_no";
        } else {
            $url = "https://acr-dev.ext.hp.com/acr/v1/devices/$partnerId/$hr_id/$serial_no";
        }

        $status = "unsubscribed";
        if ($status_level) {
            $status = "active";
        }

        $params = array(
            "status" => $status
        );
        $post_fields = json_encode($params);
        $headers = array(
            'Authorization: Bearer ' . $access_token,
            "Content-Type: application/json",
            'Content-Length: ' . strlen($post_fields)
        );

//        print_r($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, 1);
//        print_r($response);
//        die();
        if ($response["message"] || $response["success_message"]) {

            return $this->apiSuccessResponse(1, "Success", $params);
        }

        return $this->apiSuccessResponse(0, "Failed", []);
    }

    private function getInkDeviceToken()
    {
        if ($this->ENVIRONMENT == "production") {
            $url = 'https://acr.ext.hp.com/oauth/token';
            $apiKey = "NjVmZTUyMGQ5NzEwNDJiMmIzNjg1YTM0MzdhMjYzNzg6ZjBlOTg2YTQ3NzEzNGE1NTk3N2MxNzc5MWMxMWY2MjI=";
        } else {
            $url = 'https://acr-dev.ext.hp.com/oauth/token';
            $apiKey = "NzdjNmNkZGMxOThiMTFlOWFiMTRkNjYzYmQ4NzNkOTM6M2I3MmRkYmY2ZjVkNDNmYzgwZGIyMDZlN2JhZTFiNWU";
        }

//        echo $apiKey;
//        die();
        //For Testing.
//        $url = 'https://acr.ext.hp.com/oauth/token';
//        $apiKey = "NjVmZTUyMGQ5NzEwNDJiMmIzNjg1YTM0MzdhMjYzNzg6ZjBlOTg2YTQ3NzEzNGE1NTk3N2MxNzc5MWMxMWY2MjI=";

        $headers = array(
            'Authorization: Basic ' . $apiKey,
            'Content-Type: application/x-www-form-urlencoded'
        );
        $post_fields = "grant_type=client_credentials&scope=devices";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $result = curl_exec($ch);

        $response = json_decode($result, 1);
        curl_close($ch);

        return $response;
    }

    public function getHpSalesIdLists(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $invoice_id = $request->invoice_id;

        $lists = $this->getHpSalesPersonLists($mbr_id, $invoice_id);


        return $this->apiSuccessResponse(1, "Success", $lists);
    }

    private function getHpSalesPersonLists($mbr_id, $invoice_id)
    {
        $post_data = array(
            "mbr_id" => $mbr_id,
            "invoice_id" => $invoice_id
        );

        $url = $this->imsUrl . "vcapp/gethpsalesperson";

        $datas = $this->curlImsApi($url, $post_data);

        return $datas;
    }

    public function getAllEligiblePrinters(Request $request)
    {
        
        return $this->apiSuccessResponse(0, "No Printer Found.", []);
        
        //-- disable code feature below as requested by HP
//         $access_response = $this->getInkDeviceToken();
//         if (!isset($access_response["access_token"])) {
//             return $this->apiSuccessResponse(0, "Unable to Get Access Token", "Unable to Get Access Token");

//         }
//         $access_token = $access_response["access_token"];

//         $partnerId = $this->hpPartnerId;
//         if ($this->ENVIRONMENT == "production") {
//             $url = "https://acr.ext.hp.com/acr/v1/printers/$partnerId";
//         } else {
//             $url = "https://acr-dev.ext.hp.com/acr/v1/printers/$partnerId";
//         }

//         //For Testing.
// //		$url = "https://acr.ext.hp.com/acr/v1/printers/9898906fe5fb41a5a8171e334e8f9ced";

//         $headers = array(
//             'Authorization: Bearer ' . $access_token
//         );

//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL, $url);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//         curl_setopt($ch, CURLOPT_POST, false);
//         $result = curl_exec($ch);
//         curl_close($ch);
//         $response = json_decode($result, 1);

//         if (count($response) > 0) {
//             return $this->apiSuccessResponse(1, "Success", $response);
//         }
//         return $this->apiSuccessResponse(0, "No Printer Found.", []);

    }

    public function getEligiblePrinterByLists(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $printers = $request->printers;

        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            return $this->apiSuccessResponse(0, "Unable to Get Access Token", "Unable to Get Access Token");
        }
        $access_token = $access_response["access_token"];

        foreach ($printers as $printer) {
            $params["printers"][] = array(
                "modelNumber" => $printer["modelNumber"],
                "serialNumber" => $printer["serialNumber"],
                "printerDescription" => ""
            );
            $post_fields = json_encode($params);
        }

        $partnerId = $this->hpPartnerId;
        if ($this->ENVIRONMENT == "production") {
            $url = "https://acr.ext.hp.com/acr/v1/pdt/$partnerId";
        } else {
            $url = "https://acr-dev.ext.hp.com/acr/v1/pdt/$partnerId";
        }

        $headers = array(
            'Authorization: Bearer ' . $access_token,
            'Content-Type: application/json'
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $result = curl_exec($ch);
//		print_r($result);
        curl_close($ch);
        $response = json_decode($result, 1);

        if ($response && isset($response["message"])) {
            return $this->apiSuccessResponse(0, $response["message"], $response["message"], true);
        } else if ($response && isset($response["printers"])) {
            return $this->apiSuccessResponse(1, "OK", $response["printers"], true);
        }

        return $this->apiSuccessResponse(0, "No Response", []);
    }

    public function addEasyInkPrinter(Request $request)
    {

        $mbr_id = $request->mbr_id;
        $serial_num = $request->serial_num;
        $model_id = $request->model_id;

        $subscribe_mode = $request->subscribe_mode ? $request->subscribe_mode : '';

        $sql_validate = <<<str
	select 1 from crm_hp_printer_list where lot_id= ? and model_id= ?
str;
        $validate = $this->db20->select($sql_validate, [$serial_num, $model_id]);
        if ($validate) {
            return $this->apiSuccessResponse(0, "Printer has been added already.", []);
        }

        $item = $this->searchImsHpPrinter($model_id);
        $item_id = $item["item_id"];
        $item_desc = $item["item_desc"];
        $img_name = $item["img_name"];
        if (!$item_id) {
            return $this->apiSuccessResponse(0, "Printer is not found.", [$item_id]);
        }

        $sql_ins = <<<str
	    insert into crm_hp_printer_list (mbr_id, item_id, item_desc, invoice_id, lot_id, model_id, img_name, status_level, 
	                                     created_on, created_by, subscribe_mode) 
 VALUES (?, ?, ?, '', ? ,?, ?, 0, current_timestamp , ?, ?)
str;
        $res = $this->db20->statement($sql_ins, [$mbr_id, $item_id, $item_desc, $serial_num, $model_id, $img_name, $mbr_id, $subscribe_mode]);

        return $this->apiSuccessResponse(1, "Successfully Added.", [$item_id, $res]);
    }

    private function searchImsHpPrinter($model_id)
    {

        $url = $this->imsUrl . "vcapp/getimshpprinter";

        $post_data = array(
            "model_id" => $model_id
        );
        $datas = $this->curlImsApi($url, $post_data);

        return $datas;
    }

    public function loginKeyOverride(Request $request)
    {
        if (!$request->tagId || !$request->deviceId) {
            return $this->apiSuccessResponse(0, "POS Tag Key / DeviceId is not found/registered", []);
        }
        //- Verify Device Tag Id and return the POS number assigned
        $tagId = base64_decode($request->tagId);
        //$POS2 = 'POS2/UBIU9/#NFC';  // UBI e.g. posapi_dev.pos_device_list
        $POS2 = Redis::hget("POSKeys", $tagId);
        if (!$POS2) {
            return $this->apiSuccessResponse(0, "POS Tag Key is not found/registered", []);
        }
        $deviceId = '%' . $request->deviceId . '%';
        $sql = "SELECT mbr_id FROM o2o_member_devices WHERE meta like ? order by created_on desc limit 1";
        $result = $this->db20->select($sql, [$deviceId]);
        if (count($result) > 0) {
            $mbr_id = $result[0]->mbr_id;
            $pass = md5(rand());
            $key = $this->server_name . "loginKeyOverride#" . $mbr_id;
            Redis::setex($key, 30, $pass); // 30seconds
            $this->processQRCodePOS2($mbr_id, $POS2, TRUE);
            return $this->apiSuccessResponse(1, "success", ["mbr_id" => $mbr_id, "pass" => $pass]);
        } else {
            return $this->apiSuccessResponse(0, "fail", []);
        }
    }

    public function Upgrade(Request $request)
    {
        $data = array();

        $data["mbr_id"] = $request->mbr_id;
        $data["nric"] = $request->nric;
        $data["email"] = $request->email;
        $data["mbr_type"] = $request->mbr_type;
        $data["firstname"] = $request->firstname;
        $data["lastname"] = $request->lastname;

        $data["contact_num"] = isset($request->contact_num) ? $request->contact_num : '';

        $amount = '';

        $rebate_fee = $this->renewRebateFee;
        $amount_str = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);
        $rebate_fee_str = str_pad(str_replace(".", "", $rebate_fee), 12, '0', STR_PAD_LEFT);

        $txn_id = $this->vcmemberRepository->getNextInvoiceNo();
        $key = $this->server_name . "#payment" . $txn_id;
        $mbrId = $data["nric"];
        $date = Carbon::now();
        $data = array(
            "fn" => "upgrade",
            "transcode" => $txn_id,
            "description" => "Value Club Upgrade",
            "amt" => $amount_str,
            "rebate_amt" => $rebate_fee_str,
            "cardholderName" => "",
            "cardholderEmail" => $data["email"],
            "callbackUrl" => "",
            "callbackUrl2" => "callback_upgrade",
            "data" => $data,
            "txnDate" => $date->format('Y-m-d H:i:s'),
            "txnRef" => $txn_id,
            "mbr_id" => $mbrId,
            "status" => "", // to be replaced in callback
            "payment" => floatval($amount),
            "rebate_fee" => floatval($rebate_fee),
            "txnDatetime" => Carbon::now()->format('Y-m-d H:i:s'),
            "txnTime" => Carbon::now()->format('H:i:s'),
            "ClientType" => "vcapp", // new member
            "authenticity_token" => "",
            "code" => "",
            "mid" => "", // 2C2P / BRAINTREE etc.
            "email_addr" => $data["email"],
            "contact_num" => $data["contact_num"] ? $data["contact_num"] : "",
            "errorCode" => "",
            "paytype" => $this->payTypeDefault,
            "last_mbrtype" => $data["mbr_type"]
        );

//        CI1::encryption()->encrypt($txn_id)
        $txn_id = $this->base64url_encode($txn_id);
        $data = base64_encode(json_encode($data));
        if ($this->redisSetAndLog($mbrId, $key, $data, "UPGRADE")) {
            Redis::expire($key, 3600); // 5 minutes
            return $this->apiSuccessResponse(1, "success", array("paytype" => $this->payTypeDefault, "txn" => $txn_id));
        }
    }

    public function Renew(Request $request)
    {
        $data = array();

        $data["nric"] = $request->nric;
        $data["mbr_id"] = $request->mbr_id;
        $data["email"] = $request->email;
        $data["mbr_type"] = $request->mbr_type;
        $data["firstname"] = $request->firstname;
        $data["lastname"] = $request->lastname;

        $data["contact_num"] = isset($request->contact_num) ? $request->contact_num : '';

        $amount = '';

        $rebate_fee = $this->renewRebateFee;
        $amount_str = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);
        $rebate_fee_str = str_pad(str_replace(".", "", $rebate_fee), 12, '0', STR_PAD_LEFT);

        $txn_id = $this->vcmemberRepository->getNextInvoiceNo();
        $key = $this->server_name . "#payment" . $txn_id;
        $mbrId = $data["nric"];

        $data = array(
            "fn" => "renew",
            "transcode" => $txn_id,
            "description" => "Value Club Renew",
            "amt" => $amount_str,
            "rebate_amt" => $rebate_fee_str,
            "cardholderName" => "",
            "cardholderEmail" => $data["email"],
            "callbackUrl" => "",
            "callbackUrl2" => "callback_renew",
            "data" => $data,
            "txnDate" => Carbon::now()->format('Y-m-d H:i:s'),
            "txnRef" => $txn_id,
            "mbr_id" => $mbrId,
            "status" => "", // to be replaced in callback
            "payment" => floatval($amount),
            "rebate_fee" => floatval($rebate_fee),
            "txnDatetime" => Carbon::now()->format('Y-m-d H:i:s'),
            "txnTime" => Carbon::now()->format('H:i:s'),
            "ClientType" => "vcapp", // new member
            "authenticity_token" => "",
            "code" => "",
            "mid" => "", // 2C2P / BRAINTREE etc.
            "email_addr" => $data["email"],
            "contact_num" => $data["contact_num"] ? $data["contact_num"] : "",
            "errorCode" => "",
            "paytype" => $this->payTypeDefault,
            "last_mbrtype" => $data["mbr_type"]
        );

//        CI1::encryption()->encrypt($txn_id)
        $txn_id = $this->base64url_encode($txn_id);
        $data = base64_encode(json_encode($data));
        if ($this->redisSetAndLog($mbrId, $key, $data, "RENEW")) {
            Redis::expire($key, 3600); // 5 minutes
            return $this->apiSuccessResponse(1, "success", array("paytype" => $this->payTypeDefault, "txn" => $txn_id));
        }

    }

    public function Signup(Request $request)
    {

        $txn_id = $this->vcmemberRepository->getNextInvoiceNo();
        $key = $this->server_name . "#payment" . $txn_id;
        $date = Carbon::now();

        $data = array();
        $data["mbrshipType"] = $request->mbrshipType;
        $data["nric"] = $request->nric;
        $data["email"] = $request->email;
        $data["contact"] = $request->contact;
        $data["firstname"] = $request->firstname;
        $data["lastname"] = $request->lastname;
        $data["pass1"] = $request->pass1;
        $data["pass2"] = $request->pass2;
        $data["dob"] = $request->dob;
        $data["agree"] = $request->agree;
        $data["paytype"] = $request->paytype;

        if ($data["mbrshipType"]) {
            $mbrType = $data["mbrshipType"];
            $amount = $this->mbrShipFee[$mbrType];
            $amount_str = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);

        } //Will Not Use it.
        else {
            $amount = $this->signupFee; //"30.00";
            $amount_str = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);
        }

        if (!$data["nric"]) {
            $data["nric"] = $this->vcmemberRepository->generateNewMbrId('AP');
        }
        $mbrId = $data["nric"];

//        return $mbrId;

        $data = array(
            "fn" => "signup",
            "transcode" => $txn_id,
            "description" => "Value Club Sign Up",
            "amt" => $amount_str,
            "cardholderName" => "",
            "cardholderEmail" => $data["email"],
            "callbackUrl" => "api/vc2/callback_signup",
            "callbackUrl2" => "callback_signup",
            "data" => $data, // data submitted
            "txnDate" => $date->format('Y-m-d H:i:s'),
            "txnRef" => $txn_id,
            "mbr_id" => $mbrId,
            "status" => "", // to be replaced in callback
            "payment" => floatval($amount),
            "txnDatetime" => $date->format('Y-m-d H:i:s'),
            "txnTime" => $date->format('H:i:s'),
            "ClientType" => "vcapp", // new member
            "authenticity_token" => "",
            "code" => "",
            "mid" => "", // 2C2P / BRAINTREE etc.
            "email_addr" => $data["email"],
            "contact_num" => $data["contact"],
            "errorCode" => "",
            "paytype" => $this->payTypeDefault,
            "mbr_type" => $mbrType
        );


//        CI1::encryption()->encrypt($txn_id))
        $txn_id = $this->base64url_encode($txn_id);
        $data = base64_encode(json_encode($data));
        //if (Redis::set($key, $data)) {
        if ($this->redisSetAndLog($mbrId, $key, $data, "SIGNUP")) {
            //Redis::expire($key, 86400); // 1 day
            Redis::expire($key, 3600); // testing 5 minutes
            return $this->apiSuccessResponse(1, "success", array("paytype" => $this->payTypeDefault, "txn" => $txn_id));
        }
    }

    public function generateNextTransId()
    {
        $trans_id = $this->vcmemberRepository->getNextInvoiceNo();

        return $this->apiSuccessResponse(1, "Success", $trans_id);
    }

    public function generateCTLMbrId()
    {

        $backfix = 'CT';
        $mbr_id = $this->vcmemberRepository->generateNewMbrId($backfix);

        return $this->apiSuccessResponse(1, "Success", $mbr_id);
    }

    private function redisSetAndLog($mbrId, $key, $data, $type)
    {
        if (Redis::set($key, $data)) {
            $data = ["key" => $key, "data" => $data];
            $data = json_encode($data);
            $this->_logprocess("VC" . $type, "OK", $data, $mbrId);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    private function base64url_decode($data)
    {
        return base64_decode(strtr($data, '-_', '+/'));
    }

    public function Viewpay($txn_id)
    {
        $data = $this->getPaymentInRedis($txn_id);
        if (!$data) {
            return;
        }
        $d = base64_decode($data);

        //For Dev One.
        $view = $this->view_2c2p_new($data);
        return $view;
    }

    public function ViewpaySr($txn_id)
    {
        $data = $this->getPaymentInRedis($txn_id);
        if (!$data) {
            return;
        }
        $d = base64_decode($data);

        //For Dev One.
        $view = $this->view_2c2p_sr($data);
        return $view;
    }

    //Add Mbr_id/Contact/Email
    private function view_2c2p_new($data)
    {
        $server_name = $this->server_name;

        if ($this->ENVIRONMENT == "development") {
            $rebate_post_url = $this->vcapiUrl . "api/vc2/renew_viarebates";
            $post_url = $this->vcapiUrl . "api/vc2/postpay_2c2p";
            $paymentJS = 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/api/my2c2p.1.6.9.min.js';

            $checkUrl = $this->vcapiUrl . "api/vc2/checkOmsTempMember";
//            $checkUrl = 'http://localhost/' . "api/vc2/checkOmsTempMember";
        } else {
            $rebate_post_url = $this->vcapiUrl . "api/vc2/renew_viarebates";
            $post_url = $this->vcapiUrl . "api/vc2/postpay_2c2p";
            $paymentJS = 'https://t.2c2p.com/SecurePayment/api/my2c2p.1.6.9.min.js';

            $checkUrl = $this->vcapiUrl . "api/vc2/checkOmsTempMember";
        }
        $d = json_decode(base64_decode($data), TRUE);


        $key = $this->server_name . "#2C2P_STORE" . trim($d["mbr_id"]) . '*';
        $cards = Redis::keys($key);
        $mycards = [];
        foreach ($cards as $key => $card) {
            $mycard = Redis::get($card);
            $mycard = json_decode(base64_decode($mycard));
            array_push($mycards, $mycard);
        }
        $avail_rebate = $this->vcmemberRepository->getAvailablePoints(trim($d["mbr_id"]), $this->CTL_COY_ID);

//        $renew_fee = $this->renewRebateFee;
        $renew_fee = $this->mbrShipFee["!MEMBER-NEW08"];
        $renew_mbrtype = '!MEMBER-NEW08';

        $last_mbrtype = '';

        if ($d["fn"] == "renew") {
            $mbr_type = trim($d["last_mbrtype"]);

            if ($mbr_type === 'M08') {
                $last_mbrtype = '!MEMBER-NEW28';
                if ($avail_rebate >= $this->mbrShipFee_viaRebate["!MEMBER-NEW28"]) {
                    $renew_fee = $this->mbrShipFee_viaRebate["!MEMBER-NEW28"];
                } else {
                    $renew_fee = $this->mbrShipFee["!MEMBER-NEW28"];
                }
                $renew_fee_viarebate = $renew_fee;
                $renew_mbrtype = '!MEMBER-NEW28';
            } else if ($mbr_type === 'M18') {
                $last_mbrtype = '!MEMBER-NEW28';
                if ($avail_rebate >= $this->mbrShipFee_viaRebate["!MEMBER-NEW28"]) {
                    $renew_fee = $this->mbrShipFee_viaRebate["!MEMBER-NEW28"];
                } else {
                    $renew_fee = $this->mbrShipFee["!MEMBER-NEW28"];
                }
                $renew_fee_viarebate = $renew_fee;
                $renew_mbrtype = '!MEMBER-NEW28';
            } else {
                $last_mbrtype = '!MEMBER-NEW28';

                $renew_fee_viarebate = $this->mbrShipFee_viaRebate["!MEMBER-NEW28"];
                if ($avail_rebate >= $this->mbrShipFee_viaRebate["!MEMBER-NEW28"]) {
                    $renew_fee = $this->mbrShipFee_viaRebate["!MEMBER-NEW28"];
                } else {
                    $renew_fee = $this->mbrShipFee["!MEMBER-NEW28"];
                }
                $renew_mbrtype = '!MEMBER-NEW28';
            }
        } else if ($d["fn"] == "upgrade") {
            $mbr_type = trim($d["last_mbrtype"]);

            if ($mbr_type === 'M08') {
                $last_mbrtype = '!MEMBER-NEW28';
                $renew_fee = $this->mbrShipFee["!MEMBER-NEW28"];
                $renew_fee_viarebate = $renew_fee;
                $renew_mbrtype = '!MEMBER-NEW28';
            } else {
                $last_mbrtype = '!MEMBER-NEW28';
                $renew_fee = $this->mbrShipFee["!MEMBER-NEW28"];
                $renew_mbrtype = '!MEMBER-NEW28';
                $renew_fee_viarebate = $renew_fee;
            }
        } else {
            $mbr_type = '';
            $renew_fee_viarebate = 0;
        }


        $data = array(
            "invoiceData" => $data,
            "paymentJS" => $paymentJS,
            "post_url" => $post_url,
            "rebate_post_url" => $rebate_post_url,
            "mycards" => $mycards,
            'renew_total' => number_format($renew_fee, 2),
            "renew_fee_viarebate" => $renew_fee_viarebate,
            "renew_mbrtype" => $renew_mbrtype,
            'last_mbrtype' => $last_mbrtype,
            "avail_rebates" => number_format($avail_rebate / 100, 2),
            "callbackUrl" => $d["callbackUrl2"],
            "renew_min" => $this->mbrShipFee["!MEMBER-NEW08"],
            "mbr_type" => $mbr_type,
            "upgrade_type" => $d["fn"],
            "mbr_id" => $d["mbr_id"],
            "email_addr" => $d["email_addr"],
            "contact_num" => $d["contact_num"],
            "checkurl" => $checkUrl
        );

//        print_r($data);
//        die();
        return view("2c2p-mbrship2018", compact('data'));
    }

    public function view_2c2p_sr($data)
    {
        $server_name = $this->server_name;

        if ($this->ENVIRONMENT == "development") {
            $post_url = $this->vcapiUrl . "api/vc2/postpay_2c2p";
            $paymentJS = 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/api/my2c2p.1.6.9.min.js';
        } else {
            $post_url = $this->vcapiUrl . "api/vc2/postpay_2c2p";
            $paymentJS = 'https://t.2c2p.com/SecurePayment/api/my2c2p.1.6.9.min.js';
        }
        $d = json_decode(base64_decode($data), TRUE);

        $key = $this->server_name . "#2C2P_STORE" . trim($d["mbr_id"]) . '*';
        $cards = Redis::keys($key);
        $mycards = [];
        foreach ($cards as $key => $card) {
            $mycard = Redis::get($card);
            $mycard = json_decode(base64_decode($mycard));
            array_push($mycards, $mycard);
        }
        $avail_rebate = $this->vcmemberRepository->getAvailablePoints(trim($d["mbr_id"]), $this->CTL_COY_ID);

//        $renew_fee = $this->renewRebateFee;
        $renew_fee = "8.00";

        $data = array(
            "invoiceData" => $data,
            "paymentJS" => $paymentJS,
            "post_url" => $post_url,
            "mycards" => $mycards,
            'renew_total' => number_format($renew_fee, 2),
            "callbackUrl" => $d["callbackUrl2"]
        );

//        print_r($data);
//        die();
        return view("2c2p-sr", compact('data'));
    }

    private function getPaymentInRedis(&$txn_id)
    {
        $txn_id = $this->base64url_decode($txn_id);
        $key = $this->server_name . "#payment" . $txn_id;
        $data = Redis::get($key);
        return $data;
    }

    public function postpay_2c2p(Request $request)
    {
        //ini_set('display_errors', 1);
        $merchantID = $this->mechantId;
        $server_name = $this->server_name;
        if (strpos($server_name, 'lamb') !== false || strpos($server_name, 'staging') !== false ||
            $this->ENVIRONMENT == "development") {
//            $secretKey = 'zCwoDSs4kwRA'; // Challenger Testing
            $secretKey = 'YQ4pMi4Z0i20';
            $payment_mode_url = 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx';
        } else {
//            $secretKey = 'Z0bnBXm6kwXL'; // Production
            $secretKey = 'rqrORqdQV2Z2';
            $payment_mode_url = 'https://t.2c2p.com/SecurePayment/PaymentAuth.aspx';
        }

        $redisData = $request->invoiceData;
        $invoiceData = base64_decode($redisData);
        $invoiceData = json_decode($invoiceData, TRUE);
        $uniqueTransactionCode = $invoiceData["transcode"];
        $desc = $invoiceData["description"];
        $amt = $invoiceData["amt"];        //12 digit format
        $cardholderName = $request->cardholderName;
        //$cardholderEmail = $invoiceData["cardholderEmail"];
        $country = "SG";
        $currencyCode = "702";            //Ref: http://en.wikipedia.org/wiki/ISO_4217
        $timeStamp = time();
        $apiVersion = "9.9";
        $encryptedCardInfo = $request->encryptedCardInfo;                            //Retrieve encrypted credit card data
        $maskedCardNo = $request->maskedCardInfo;                                    //Masked card number (first 6 and last 4 digit of credit card number)
        $expMonth = $request->expMonthCardInfo;                                      //Card expiry month
        $expYear = $request->expYearCardInfo;

        //New Added.
        if ($invoiceData["fn"] == "renew" || $invoiceData["fn"] == "upgrade") {
            $send_amt = $_POST['renew_renewal'];
            $mbr_type = $_POST['renew_membertype'];
            $mbrship_amt = $this->mbrShipFee[$mbr_type];

            if ($mbrship_amt && $mbr_type) {
                $rebates_amount = 0.00;
                $rebate_point = str_pad(str_replace(".", "", number_format(0, 2)), 12, '0', STR_PAD_LEFT);

                $amount = number_format($mbrship_amt, 2);
                $amt = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);

                $invoiceData["rebate_payment"] = number_format($rebates_amount, 2);
                $invoiceData["rebate_amount"] = $rebate_point;
                $invoiceData["amt"] = $amt;
                $invoiceData["payment"] = $amount;
                $invoiceData["mbr_type"] = $mbr_type;
//            print_r($invoiceData);
//            die();
                $redisData = base64_encode(json_encode($invoiceData));
            } else if (!$mbr_type || $mbrship_amt != $send_amt) {
                $str = <<<str
    Membership Type is not found.
str;
                echo $str;
                die();
            }
        }

        //Card expiry Year
        $userDefined1 = array(
            "fn" => $invoiceData["fn"],
            "callbackUrl" => $invoiceData["callbackUrl"],
            "mbrId" => $invoiceData["mbr_id"]
        );
        $userDefined1 = base64_encode(json_encode($userDefined1));
        $storeCard = "Y";
        $storeCardUniqueID = "";
        if ($request->storeCardUniqueID) {
            $storeCard = "N";
            $storeCardUniqueID = $request->storeCardUniqueID;
        }
        $cardholderEmail = "";
        $userDefined3 = "app-" . $invoiceData["fn"];
        $userDefined2 = "";
        $userDefined5 = "app-" . $invoiceData["fn"];

        if ($this->ENVIRONMENT == 'development') {
            $userDefined4 = 'vcapp-stage';
        } else {
            $userDefined4 = 'vcapp';
        }

        //Construct payment request message
        $xml = <<<str
<PaymentRequest>
<timeStamp>$timeStamp</timeStamp>
<merchantID>$merchantID</merchantID>
<uniqueTransactionCode>$uniqueTransactionCode</uniqueTransactionCode>
<desc>$desc</desc>
<amt>$amt</amt>
<currencyCode>$currencyCode</currencyCode>  
<panCountry>$country</panCountry> 
<cardholderName>$cardholderName</cardholderName>   
<cardholderEmail>$cardholderEmail</cardholderEmail>   
<userDefined1>$userDefined1</userDefined1>
<userDefined2>$userDefined2</userDefined2>
<userDefined3>$userDefined3</userDefined3>
<userDefined4>$userDefined4</userDefined4>
<userDefined5>$userDefined5</userDefined5>
<storeCard>$storeCard</storeCard> 
<encCardData>$encryptedCardInfo</encCardData>
<storeCardUniqueID>$storeCardUniqueID</storeCardUniqueID>
</PaymentRequest>
str;
//        print_r($xml);
//        die();

        //3) Create inner payload
        $payload = base64_encode($xml);

        //4) Generate signature based on inner payload
        $signature = strtoupper(hash_hmac('sha256', $payload, $secretKey, false));

        //5) Create Outer XML
        $payloadXML = "<PaymentRequest>
            <version>$apiVersion</version>
            <payload>$payload</payload>
            <signature>$signature</signature>
            </PaymentRequest>";

        $payload = base64_encode($payloadXML);

        $key = $server_name . "#payment" . trim($uniqueTransactionCode);
        $result = Redis::set($key, $redisData);
        Redis::expire($key, 86400); // 1 day
        $html = <<<str
<form action='$payment_mode_url' method='POST' name='authForm'>    
    Please wait for a while. Do not close the browser or refresh the page.            
    <input type='hidden' name='paymentRequest' value='$payload'>
</form>
<script language="JavaScript">
    document.authForm.submit();
</script>
str;
        echo $html;
    }

    public function callback_upgrade($txn_id, $status)
    {
//        echo $txn_id. "/". $status;
//        die();

        $key = $this->server_name . "#payment" . trim($txn_id);
        $data = Redis::get($key);

        //echo "status: " . $status;  // NOTE:  A-success, AR-fail, F-fail
        $data = base64_decode($data);
        $data = json_decode($data, TRUE);

//        print_r($data["data"]);
//        die();

        $merchantID = $this->mechantId;
        $entry = $data["data"];
        $mbr_id = $entry["nric"];
        $txnRef = $data["transcode"];
        $payment = $data["payment"];
        $amount = $data["payment"];
        $last_mbrtype = $data["last_mbrtype"];

        $mbr_type = $data["mbr_type"];
        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $upgrade_type = "upgrade";

        //Renew with Rebates
        $rebates_payment = $data["rebate_payment"] ? $data["rebate_payment"] : 0;
        $rebates_amount = $data["rebate_amount"] ? $data["rebate_amount"] : 0;

        $txnDate = $data["txnDatetime"];
        $authenticity_token = $data["authenticity_token"];
        $tstatus = ($status == 'A' ? 's' : 'f');
        $errorCode = $status;
        $txnTime = $data["txnTime"];
        $email = $entry["email"];
        $first = $entry["firstname"];

        $this->db20->beginTransaction();

        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ? and coy_id='CTL'",
            [$txnRef]);
        if (count($result)) {
            echo "Transaction already exist";
            return FALSE;
        }

        $this->sp_renew_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus,
            $errorCode, $payment, $txnTime, $mbr_month_type, $upgrade_type, $email, '', $first);
//        die();

        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,amount,txnDate,
                          mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?,?,?,?,?)
str;
        $query_log = $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, '',
            $payment, $merchantID, $status, $errorlog, $mbr_id, 'upg', $tstatus, $this->vcmemberRepository->GUID(), $email, 'CC', 'callback_upgrade'));


        $this->db20->commit();
        Redis::del($key);

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        $response = array("fn" => $data["fn"], "status" => $tstatus);
        $response = json_encode($response);
        $js = <<<str
<script>
    parent.postMessage($response, "*");
</script>
str;
        if ($status == 'A') {
            //Send Email to New Signup User
            $send_email = $email;
            $email_data = array(
                "r_FirstName" => ucwords(strtolower($first)),
            );


            if ($this->ENVIRONMENT === 'development' && strpos($email, 'weiseng') !== false) {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            //Change to Upgrade Edm
            $email_template = $this->emarsys_template_list['UPGRADE_MBRSHIP'];

            //-- Set Email pending for processing.
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');

            //Purge the asso members
        } else {
            return "2C2P Transaction Failed";
        }

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        echo $js;

    }

    public function callback_renew($txn_id, $status)
    {
        $key = $this->server_name . "#payment" . trim($txn_id);
        $data = Redis::get($key);


        $data = base64_decode($data);
        $data = json_decode($data, TRUE);
//        print_r($data);
//        die();

        $merchantID = $this->mechantId;
        $entry = $data["data"];
        $mbr_id = $entry["nric"];
        $txnRef = $data["transcode"];
        $payment = $data["payment"];
        $amount = $data["payment"];
        $last_mbrtype = $data["last_mbrtype"];

        $mbr_type = $data["mbr_type"];
        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $upgrade_type = "renew";

        //Renew with Rebates
        $rebates_payment = $data["rebate_payment"] ? $data["rebate_payment"] : 0;
        $rebates_amount = $data["rebate_amount"] ? $data["rebate_amount"] : 0;

        $txnDate = $data["txnDatetime"];
        $authenticity_token = $data["authenticity_token"];
        $tstatus = ($status == 'A' ? 's' : 'f');
        $errorCode = $status;
        $txnTime = $data["txnTime"];
        $email = $entry["email"];
        $first = $entry["firstname"];


        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ? and coy_id='CTL'",
            [$txnRef]);
        if (count($result)) {
            echo "Transaction already exist";
            return FALSE;
        }

        $this->sp_renew_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $mbr_month_type, $upgrade_type, $email, '', $first);

        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,amount,txnDate,mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp ,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?,?,?,?,?)
str;
        $query_log = $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, '', $payment, $merchantID,
            $status, $errorlog, $mbr_id, 'ren', $tstatus, $this->vcmemberRepository->GUID(), $email, 'CC', 'callback_renew'));

        $this->db20->commit();
        Redis::del($key);

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        $response = array("fn" => $data["fn"], "status" => $tstatus);
        $response = json_encode($response);
        $js = <<<str
<script>
    parent.postMessage($response, "*");
</script>
str;
        if ($status == 'A') {
            //Send Email to New SIgnup User
            $send_email = $email;
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );

            if (strpos($email, 'weiseng992') !== false && $this->ENVIRONMENT === 'development') {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            $email_template = '';
            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['RENEW_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['RENEW_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['RENEW_28MONTHS'];
            }

            //-- Set Email pending for processing.
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');
        } else {
            return "2C2P Transaction Failed";
        }

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        echo $js;

    }

    public function member_signup(Request $request)
    {
        $merchantID = $this->mechantId;

        $first = $request->firstname;
        $last = $request->lastname;
        $mbr_id = $request->mbr_id;
        $email = $request->email;

        $dob = $request->dob ? $request->dob : '1900-01-01';
        $dob = date("Y-m-d", strtotime($dob));
        $dob = Carbon::createFromFormat('Y-m-d', $dob)->format('Y-m-d');
        $mbr_type = $request->mbr_type;

        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $contact = $request->contact;
        $password = md5($request->pass1);
        $txnRef = $request->transcode;
        $payment = $request->payment;
        $mid = $this->mechantId;
        $amount = $request->payment;
        $txnDate = $request->txnDatetime;
        $status = $request->status;
        $tstatus = ($request->status == 'A' ? 's' : 'f');
        $errorCode = $request->status;
        $txnTime = $request->txnTime;
        $email_addr = $request->email;
        $code = md5($mbr_id . $txnDate);


        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ?", [$txnRef]);
        if (count($result)) {
            return $this->apiSuccessResponse(0, "Failed", "Transaction: " . $txnRef . " already exist");
        }

        $sql = <<<str
insert into oms_temp_member (txnRef,mbr_id,first_name,last_name,email_addr,contact_num,mbr_pwd,amount,txnDate,mid,payment,
                             created_on,created_by,txnTime,status,error_code) values 
  (?,?,?,?,?,?,?,?,current_timestamp ,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?)
str;
        $this->db20->statement($sql, array($txnRef, $mbr_id, $first, $last, $email, $contact, $password, $amount, $mid, $payment, $mbr_id, $tstatus, $errorCode));

        $this->sp_new_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $email_addr, $code, $mbr_month_type,
            $first, $last, $contact, $dob, $password, $mbr_type);


        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,
                          amount,txnDate,mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,
                          payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp), ?,?,?,?,?,?)
str;
        $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, $last, $amount, $merchantID, $status, $errorlog, $mbr_id, 'new', $tstatus,
            $this->vcmemberRepository->GUID(), $email, 'CC', 'member_signup'));

        if ($status == 'A') {

            //Add Voucher.
            $this->_issueSignUpVoucher($mbr_id, $mbr_month_type, $txnRef, 'OS');

            //Send Email to New SIgnup User
            $send_email = $email_addr;
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );

            //For Testing Purpose.
            if (strpos($email_addr, 'weiseng') !== false && $this->ENVIRONMENT === 'development') {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            $email_template = '';
            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['SIGNUP_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['SIGNUP_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['SIGNUP_28MONTHS'];
            }

            //-- Set Email pending for processing
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');
        } else {
            return $this->apiSuccessResponse(0, 'Transaction Failed', []);
        }

        return $this->apiSuccessResponse(1, 'Success', $mbr_id);
    }

    public function member_renew(Request $request)
    {
        $merchantID = $this->mechantId;
        $mbr_id = $request->mbr_id;
        $txnRef = $request->transcode;
        $payment = $request->payment;
        $amount = $request->payment;
        $status = $request->status;

        $mbr_type = $request->mbr_type;
        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $upgrade_type = "renew";

        $txnDate = $request->txnDatetime;
        $tstatus = ($status == 'A' ? 's' : 'f');
        $errorCode = $status;
        $txnTime = $request->txnTime;
        $email = $request->email;
        $first = $request->firstname;


        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ? and coy_id='CTL'", [$txnRef]);
        if (count($result)) {
            return $this->apiSuccessResponse(0, "Failed", "Transaction: " . $txnRef . " already exist");
        }

        $this->sp_renew_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $mbr_month_type, $upgrade_type, $email, '', $first);

        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,amount,txnDate,mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp ,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?,?,?,?,?)
str;
        $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, '', $payment, $merchantID,
            $status, $errorlog, $mbr_id, 'ren', $tstatus, $this->vcmemberRepository->GUID(), $email, 'CC', 'member_renew'));


        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        if ($status == 'A') {
            //Send Email to New SIgnup User
            $send_email = $email;
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );

            if (strpos($email, 'weiseng') !== false && $this->ENVIRONMENT === 'development') {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            $email_template = '';
            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['RENEW_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['RENEW_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['RENEW_28MONTHS'];
            }

            //-- Set Email pending for processing.
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');
        } else {
            return $this->apiSuccessResponse(0, "Transaction Failed", "Transaction Failed");
        }

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);


        return $this->apiSuccessResponse(1, 'Success', $mbr_id);
    }

    public function member_upgrade(Request $request)
    {
        $status = $request->status;

        $merchantID = $this->mechantId;
        $mbr_id = $request->mbr_id;
        $txnRef = $request->transcode;
        $payment = $request->payment;
        $amount = $request->payment;

        $mbr_type = $request->mbr_type;
        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $upgrade_type = "upgrade";

        $txnDate = $request->txnDatetime;

        $tstatus = ($status == 'A' ? 's' : 'f');
        $errorCode = $status;
        $txnTime = $request->txnTime;
        $email = $request->email;
        $first = $request->firstname;

        $this->db20->beginTransaction();

        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ? and coy_id='CTL'",
            [$txnRef]);
        if (count($result)) {
            return $this->apiSuccessResponse(0, "Failed", "Transaction: " . $txnRef . " already exist");
        }

        $this->sp_renew_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $mbr_month_type, $upgrade_type, $email, '', $first);

        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,amount,txnDate,
                          mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?,?,?,?,?)
str;
        $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, '',
            $payment, $merchantID, $status, $errorlog, $mbr_id, 'upg', $tstatus, $this->vcmemberRepository->GUID(), $email, 'CC', 'member_upgrade'));


        $this->db20->commit();

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        if ($status == 'A') {
            //Send Email to New Signup User
            $send_email = $email;
            $email_data = array(
                "r_FirstName" => ucwords(strtolower($first)),
            );


            if ($this->ENVIRONMENT === 'development' && strpos($email, 'weiseng') !== false) {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            //Change to Upgrade Edm
            $email_template = $this->emarsys_template_list['UPGRADE_MBRSHIP'];

            //-- Set Email pending for processing.
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');

            //Purge the asso members
        } else {
            return $this->apiSuccessResponse(0, "Transaction Failed", "Transaction Failed");
        }

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        return $this->apiSuccessResponse(1, "Success", "Success");
    }

    public function checkOmsTempMember(Request $request)
    {

        $upgrade_type = $request->upgrade_type;
        $mbr_id = $request->mbr_id;
        $email = strtolower($request->email_addr);
        $contact = $request->contact_num;

        if ($upgrade_type == 'signup') {

            $sql = <<<str
select * from oms_temp_member where cast(txndate as date)=cast(NOW() as date) and
                            ( LOWER(email_addr)='$email' or contact_num='$contact')
str;
            $oms_member = $this->db20->select($sql);
            if ($oms_member && count($oms_member) > 0) {
                return $this->apiSuccessResponse(0, "Recurring transaction.",
                    "Your Account has already been created just now.");
            }

        } else {
            $sql2 = <<<str
select * from o2o_log_live_processes_new where process_id='VC_SIGNUP' and
    cast(created_on as date)=cast(NOW() as date) and
( LOWER(email_addr)='$email' or created_by='$contact')
str;
            $renew_member = $this->db20->select($sql2);
            if ($renew_member && count($renew_member) > 0) {
                return $this->apiSuccessResponse(0, "Recurring transaction.",
                    "Your Account has already been renewed/upgraded.");

            }

        }

        return $this->apiSuccessResponse(1, "OK", [$mbr_id, $email, $contact]);
    }

    public function callback_sr($txn_id, $status)
    {
        $key = $this->server_name . "#payment" . trim($txn_id);
//        echo $key;
        $data = Redis::get($key);

        $this->_logprocess("CALLBACK_SR", $status, $data, $txn_id);

        if (!$data) {
            echo "Token has Expired.";
            die();
        }

        $tstatus = ($status == 'A' ? 's' : 'f');

        Redis::del($key);

        $response = array("fn" => 'sr', "status" => $tstatus);
        $response = json_encode($response);
        $js = <<<str
<script> 
    parent.postMessage($response, "*");
</script>
str;

        echo $js;
    }

    public function callback_signup($txn_id, $status)
    {
        $key = $this->server_name . "#payment" . trim($txn_id);

        $data = Redis::get($key);
        if (!$data) {
            echo "Token has Expired.";
            die();
        }

        //echo "status: " . $status;  // NOTE:  A-success, AR-fail, F-fail
        $data = base64_decode($data);
        $data = json_decode($data, TRUE);

//        print_r($data);
//        die();

        $merchantID = $this->mechantId;

        $entry = $data["data"];

//        print_r($entry);
//        die();

        $first = $entry["firstname"];
        $last = $entry["lastname"];
        $mbr_id = $entry["nric"];
        $email = $entry["email"];
        $title = '';

        $dob = $entry["dob"] ? $entry["dob"] : '1900-01-01';
        $dob = Carbon::createFromFormat('Y-m-d', $dob)->format('Y-m-d');
        $mbr_type = $entry["mbrshipType"];

        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $contact = $entry["contact"];
        $password = md5($entry["pass1"]);
        $txnRef = $data["transcode"];
        $payment = $data["payment"];
        $mid = $this->mechantId;
        $amount = $data["payment"];
        $txnDate = $data["txnDatetime"];
        $authenticity_token = $data["authenticity_token"];
        $tstatus = ($status == 'A' ? 's' : 'f');
        $errorCode = $status;
        $txnTime = $data["txnTime"];
        $email_addr = $entry["email"];
        $code = md5($mbr_id . $txnDate);


        // Check first if it exist
        $result = $this->db20->select("SELECT mbr_id FROM crm_member_transaction WHERE trans_id = ?", [$txnRef]);
        if (count($result)) {
            return $this->apiSuccessResponse(0, "Failed", "Transaction: " . $txnRef . " already exist");
        }

        $sql = <<<str
insert into oms_temp_member (txnRef,mbr_id,first_name,last_name,email_addr,contact_num,mbr_pwd,amount,txnDate,mid,payment,
                             created_on,created_by,txnTime,status,error_code) values 
  (?,?,?,?,?,?,?,?,current_timestamp ,?,?,current_timestamp,?,cast(current_timestamp AS timestamp),?,?)
str;
        $query = $this->db20->statement($sql, array($txnRef, $mbr_id, $first, $last, $email, $contact, $password, $amount, $mid, $payment, $mbr_id, $tstatus, $errorCode));

        $this->sp_new_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $email_addr, $code, $mbr_month_type,
            $first, $last, $contact, $dob, $password, $mbr_type);


        //Insert Log to oms_2c2p_log.
        $errorlog = ($status == 'A') ? '00' : $errorCode;
        $sql_log = <<<str
insert into oms_2c2p_log (merchant_txn_ref,twoCtwoP_txn_ref,twoCtwoP_txn_resp_code,mbr_id,first_name,last_name,
                          amount,txnDate,mid,twoCtwoP_txn_status,error_code,created_on,created_by,txnTime,ttype,tstatus,guid,email_addr,
                          payment_mode,function_name) values 
  (?,?,?,?,?,?,?,current_timestamp,?,?,?,current_timestamp,?,cast(current_timestamp AS timestamp), ?,?,?,?,?,?)
str;
        $query_log = $this->db20->statement($sql_log, array($txnRef, '', $errorlog, $mbr_id, $first, $last, $amount, $merchantID, $status, $errorlog, $mbr_id, 'new', $tstatus, $this->vcmemberRepository->GUID(), $email, 'CC', 'callback_signup'));

        if ($status == 'A') {

            //Add Voucher.
            $this->_issueSignUpVoucher($mbr_id, $mbr_month_type, $txnRef, 'OS');

            //Send Email to New SIgnup User
            $send_email = $email_addr;
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );

            //For Testing Purpose.
            if (strpos($email_addr, 'weiseng') !== false && $this->ENVIRONMENT === 'development') {
                $send_email = 'wong.weiseng@challenger.sg';
            }

            $email_template = '';
            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['SIGNUP_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['SIGNUP_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['SIGNUP_28MONTHS'];
            }

            //-- Set Email pending for processing
            $this->_sendEmarsysEmail($email_template, $send_email, $email_data, '');
        } else {
            return "2C2P Transaction Failed";
        }


        $this->db20->commit();
        Redis::del($key);

        $response = array("fn" => $data["fn"], "status" => $tstatus);
        $response = json_encode($response);
        $js = <<<str
<script> 
    parent.postMessage($response, "*");
</script>
str;

        echo $js;

    }


    private function sp_renew_member_points($txnRef, $mbr_id, $amount, $payment, $mbr_month_type, $upgrade_type)
    {

        $coy_id = $this->CTL_COY_ID;

//        $this->db20->beginTransaction();
//        try {

        $member = $this->db20->table('crm_member_list')
            ->select(DB::raw('cast(exp_date as date) as the_exp_date'), 'exp_date', 'last_renewal', 'mbr_type', 'last_name', 'first_name', 'email_addr', 'contact_num')
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id
            ])->first();

        $trans = $this->db20->table('crm_member_transaction')
            ->select(DB::raw("count(mbr_id) as line_num"))
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id,
                'trans_id' => $txnRef
            ])->first();

        $types = $this->db20->table('crm_member_type')
            ->select(DB::raw("MAX(line_num) as line_num"))
            ->where([
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id
            ])->first();


        $type_num = $types ? $types->line_num + 1 : 1;
        $line_num = $trans ? $trans->line_num + 1 : 1;

        $dateNow = date("Y-m-d");
        $old_mbr_type = $member->mbr_type;
        $exp_date = $member->exp_date;
        $the_exp_date = $member->the_exp_date;
        $last_renewal = $member->last_renewal;
        $last_name = $member->last_name;
        $email = $member->email_addr;
        $contact = $member->contact_num;
        $first_name = $member->first_name;


        $this->_logNewprocess('VC_RENEW_POINT', $txnRef, $mbr_month_type, 'OK', $payment, $mbr_id, $upgrade_type, $amount);

        //Renew via point.
        if ($upgrade_type == 'renew') {

            if ($mbr_month_type == 'M08') {
                $start_exp_date = Carbon::now()->addMonth(8);
                $next_exp_date = Carbon::parse($exp_date)->addMonth(8);
                $item_id = "!MEMBER-REN08";
                $item_desc = '8-Month Membership Renewal (800 pts)';
                $amount = 8;
                $point_to_redeem = 8;
            } else if ($mbr_month_type == 'M18') {
                $start_exp_date = Carbon::now()->addMonth(18);
                $next_exp_date = Carbon::parse($exp_date)->addMonth(18);
                $item_id = "!MEMBER-REN18";
                $item_desc = '18-Month Membership Renewal (1800 pts)';
                $amount = 18;
                $point_to_redeem = 18;
            } else if ($mbr_month_type == 'M28') {
                $start_exp_date = Carbon::now()->addMonth(28);
                $next_exp_date = Carbon::parse($exp_date)->addMonth(28);
                $item_id = "!MEMBER-REN28";
                $item_desc = '28-Month Membership Renewal (2500 pts)';
                $amount = 25;
                $point_to_redeem = 25;
            } else {
                $start_exp_date = Carbon::now()->addMonth(24);
                $next_exp_date = Carbon::parse($exp_date)->addMonth(24);
                $item_id = "!MEMBER-REN";
                $item_desc = '24-Month Membership Renewal (2400 pts)';
                $amount = 24;
                $point_to_redeem = 24;
            }

            // TODO: YS (2022-03-17): Should use RD for redemption of points
            //Renew here.
            if ($the_exp_date > $dateNow) {
                //User still available.
                $eff_from = $exp_date;

                $use_exp_date = $next_exp_date;


                $sql_upd = <<<str
update crm_member_list set mbr_type = '$mbr_month_type', exp_date = '$use_exp_date' ,last_renewal = current_timestamp ,status_level = 1,
created_by = left('$mbr_id',15), modified_by = 'MBR_RENEW2',modified_on = current_timestamp ,updated_on = current_timestamp 
where coy_id='$coy_id' and  mbr_id = '$mbr_id' 
str;
                $this->db20->statement($sql_upd);

            } else {
                //User has expired.
                $use_exp_date = $start_exp_date;
                $eff_from = date("Y-m-d H:i:s");

                $sql_upd = <<<str
update crm_member_list set mbr_type = '$mbr_month_type' , exp_date = '$use_exp_date',last_renewal =  current_timestamp  ,status_level = 1,
        created_by = left('$mbr_id',15), modified_by = 'MBR_RENEW1',modified_on = current_timestamp  ,updated_on = current_timestamp 
where coy_id='$coy_id' and  mbr_id = '$mbr_id' 
str;
                $this->db20->statement($sql_upd);
            }

            //Insert Crm_member _transaction
            $sql_trans = <<<str
	insert into crm_member_transaction (coy_id,mbr_id,trans_id,line_num,trans_type,trans_date,loc_id,pos_id,
						item_id,item_desc,item_qty,regular_price,unit_price,disc_percent,disc_amount,trans_points,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) 
						values ('$coy_id','$mbr_id','$txnRef', '$line_num' ,'OS',current_timestamp,'OS','',
						'$item_id','$item_desc',1,'$amount','$amount',0,0,0,'',
						0,left('$mbr_id',15),current_timestamp ,'',current_timestamp,current_timestamp)
str;
            $this->db20->statement($sql_trans);


            //Deduct Points. here.
//            print_r($res);
            $yearMonth = date("Ym");
            $trans_prefix = 'ADJUSTAP' . $yearMonth;
            $so_id_header = 'AP' . $yearMonth;

            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header, 15, 'MEMBER', 'CRM', 'CTL');

            $func = "points/deduct";
            $post_data = array(
                "mbr_id" => $mbr_id,
                "trans_id" => $ap_trans_id,
                "rebate_amount" => -$point_to_redeem,
                "rebate_description" => "Renew " . $mbr_month_type . " Membership via REBATE."
            );

            $res = $this->curlCHPos($func, $post_data);
            $this->_logprocess('RENEW_POINT_DEDUCT', $point_to_redeem, json_encode($res), $mbr_id, $ap_trans_id);


            //Insert Crm Member Type.
            $sql_type = <<<str
insert into crm_member_type (coy_id, mbr_id, line_num, eff_from, eff_to, mbr_type, created_by, created_on, modified_by, modified_on) VALUES
								('$coy_id','$mbr_id','$type_num','$eff_from','$use_exp_date','$mbr_month_type','$mbr_id', current_timestamp , '$mbr_id', current_timestamp)
str;
            $this->db20->statement($sql_type);


            $this->_logNewprocess('VC_RENEW_POINT_OK', $txnRef, $mbr_month_type, 'OK', $payment, $mbr_id, $upgrade_type, $amount);


            //Insert OS Invoice.
            $url = $this->imsUrl . "vcapp/insertosinvoice";

            $post_fields = array(
                "type" => "renew_point",
                "trans_id" => $txnRef,
                "mbr_id" => $mbr_id,
                "mbr_type" => $mbr_month_type,
                "payment" => $payment,
                "trans_date" => date("Y-m-d H:i:s"),
                "contact_num" => $contact,
                "email_addr" => $email,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "item_id" => $item_id,
                "item_desc" => $item_desc
            );
            $this->_logprocess('VC_RENEWPOINT_DATA', $mbr_month_type, json_encode($post_fields), $mbr_id, $txnRef);

            $resp_os = $this->curlImsApi($url, $post_fields);
            $this->_logprocess('VC_RENEWPOINT_OS', $mbr_month_type, json_encode($resp_os), $mbr_id, $txnRef);


        } else if ($upgrade_type == 'upgrade') {

            if ($mbr_month_type == 'M08') {
                $start_exp_date = Carbon::now()->addMonth(8);
                $item_desc = '8-Month Membership Renewal (800 pts)';
                $item_id = "!MEMBER-REN08";
                $amount = 8;
                $point_to_redeem = 8;
            } else if ($mbr_month_type == 'M18') {
                $start_exp_date = Carbon::now()->addMonth(18);
                $item_id = "!MEMBER-REN18";
                $item_desc = '18-Month Membership Renewal (1800 pts)';
                $amount = 18;
                $point_to_redeem = 18;
            } else if ($mbr_month_type == 'M28') {
                $start_exp_date = Carbon::now()->addMonth(28);
                $item_desc = '28-Month Membership Renewal (2800 pts)';
                $item_id = "!MEMBER-REN28";
                $amount = 28;
                $point_to_redeem = 28;
            } else {
                $start_exp_date = Carbon::now()->addMonth(24);
                $item_desc = '24-Month Membership Renewal (2400 pts)';
                $item_id = "!MEMBER-REN";
                $amount = 24;
                $point_to_redeem = 24;
            }

            //Upgrade here.
            $sql_diff = <<<str
select SUM(years) as year_diff, SUM(months) as month_diff from (
                                          select DATE_PART('month', AGE(eff_to, current_timestamp)) AS months,
                                                 DATE_PART('year', AGE(eff_to, current_timestamp)) AS years
                                          from crm_member_type
                                          where coy_id='$coy_id' 
                                            and mbr_id = ?
                                            and eff_to >= '$dateNow'
                                      ) as a

str;

            $type_lists = $this->db20->select($sql_diff, [$mbr_id]);

            $month_diff = $type_lists[0]->month_diff;
            $year_diff = $type_lists[0]->year_diff;
            if ($year_diff > 0) {
                $month_diff = $month_diff + ($year_diff * 12);
            }
            if (!$month_diff) {
                $month_diff = 1;
            }

            //Update crm_member_list
            $sql_crm = <<<str
update crm_member_list set mbr_type = '$mbr_month_type', exp_date = '$start_exp_date' ,last_renewal = current_timestamp  ,status_level = 1,
modified_by = 'UPGRADE_1',modified_on = current_timestamp,updated_on =current_timestamp where coy_id='$coy_id' and  mbr_id = '$mbr_id'
str;
            $this->db20->statement($sql_crm);

            //Insert crm_member_transaction
            $sql_trans = <<<str
	insert into crm_member_transaction (coy_id,mbr_id,trans_id,line_num,trans_type,trans_date,loc_id,pos_id,
						item_id,item_desc,item_qty,regular_price,unit_price,disc_percent,disc_amount,trans_points,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) 
						values ('$coy_id','$mbr_id','$txnRef', '$line_num' ,'OS',current_timestamp,'OS','',
						'$item_id','$item_desc',1,'$amount','$amount',0,0,0,'',
						0,left('$mbr_id',15),current_timestamp ,'',current_timestamp,current_timestamp)
str;
            $this->db20->statement($sql_trans);


            $yearMonth = date("Ym");
            $trans_prefix = 'ADJUSTAP' . $yearMonth;
            $so_id_header = 'AP' . $yearMonth;

            $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header, 15, 'MEMBER', 'CRM', 'CTL');

            //Deduct Points. here.
            $func = "points/deduct";
            $post_data = array(
                "mbr_id" => $mbr_id,
                "trans_id" => $ap_trans_id,
                "rebate_amount" => -$point_to_redeem,
                "rebate_description" => $item_desc
            );

            $res = $this->curlCHPos($func, $post_data);
            $this->_logprocess('UPGRADE_POINT_DEDUCT', $point_to_redeem, json_encode($res), $mbr_id, $ap_trans_id);


            //Update Old Crm_member_type;
            $sql_upd_type = <<<str
update crm_member_type set eff_to= current_timestamp where (line_num < ? and eff_to > current_timestamp )and coy_id='$coy_id' and  mbr_id ='$mbr_id'
str;
            $this->db20->statement($sql_upd_type, [$type_num]);


            //Insert new crm_member_type
            $sql_ins_type = <<<str
				insert into crm_member_type (coy_id, mbr_id, line_num, eff_from, eff_to, mbr_type, created_by, created_on, modified_by, modified_on) VALUES
                ('$coy_id','$mbr_id', '$type_num', current_timestamp , '$start_exp_date','$mbr_month_type','$mbr_id', current_timestamp, 'upgrade', current_timestamp)
str;
            $this->db20->statement($sql_ins_type, []);

            //Refund here.
            if ($month_diff > 0) {
                $refund_point = $month_diff * 100;

                $point_list = $this->db20->table('crm_member_points')
                    ->where('coy_id', $coy_id)
                    ->where('mbr_id', $mbr_id)
                    ->whereRaw("CAST(exp_date AS DATE) = '$start_exp_date'")
                    ->first();

                if ($point_list) {
                    $point_line_num = $point_list->line_num;

                    $sql_upd_point = <<<str
UPDATE crm_member_points SET points_accumulated = points_accumulated + $refund_point , modified_by = '$mbr_id', modified_on = current_timestamp  
WHERE coy_id = '$coy_id' AND mbr_id = '$mbr_id' AND line_num = ?
str;
                    $this->db20->statement($sql_upd_point, [$point_line_num]);

                } else {

                    $points = $this->db20->table('crm_member_points')
                        ->select(DB::raw("MAX(line_num) as line_num"))
                        ->where([
                            'coy_id' => 'CTL',
                            'mbr_id' => $mbr_id
                        ])->first();

                    $point_line = $points ? (isset($points->line_num) ? $points->line_num + 1 : 1) : 1;

                    //Insert mbr_points
                    $sql_ins_points = <<<str
insert into crm_member_points (coy_id, mbr_id, line_num, exp_date, points_accumulated, points_redeemed, points_expired, created_by, created_on, modified_by, modified_on, updated_on) VALUES
										('$coy_id','$mbr_id', '$point_line', '$start_exp_date', $refund_point, 0,0,'$mbr_id', current_timestamp ,'$mbr_id', current_timestamp, current_timestamp)
str;
                    $this->db20->statement($sql_ins_points, []);

                }


                //Update member point accumulated.
                $sql_upd_mbr = <<<str
UPDATE crm_member_list SET points_accumulated = points_accumulated + ? , modified_by = 'Vc30' , modified_on = current_timestamp  WHERE coy_id = '$coy_id' AND mbr_id = '$mbr_id'
str;
                $this->db20->statement($sql_upd_mbr, [$refund_point]);

                $yearMonth = date("Ym");
                $trans_prefix = 'ADJUSTAP' . $yearMonth;
                $so_id_header = 'AP' . $yearMonth;

                $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header, 15, 'MEMBER', 'CRM', 'CTL');

                $AP_itemdesc = 'Membership upgrade to ' . $mbr_month_type . ' and refund ' . $month_diff . ' months via rebate';

                //Insert AP crm_member_transaction.
                $sql_ins_ap = <<<str
   insert into crm_member_transaction(coy_id, mbr_id, trans_id, trans_date, loc_id, pos_id, line_num, trans_type, item_id, item_desc, item_qty, regular_price, unit_price,
   trans_points, disc_percent,disc_amount,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) VALUES
     ('$coy_id', '$mbr_id', '$ap_trans_id', current_timestamp ,'' ,'' , 1, 'AP', '', '$AP_itemdesc', ? , 100,1,
      ? ,'0','0','','0', '$mbr_id', current_timestamp, 'Vc30', current_timestamp, current_timestamp )
str;
                $this->db20->statement($sql_ins_ap, [$month_diff, $refund_point]);

            }


            $this->_logNewprocess('VC_UPGRADE_POINT_OK', $txnRef, $mbr_month_type, 'OK', $payment, $mbr_id, $upgrade_type, $amount);

            //Insert OS Invoice.
            $url = $this->imsUrl . "vcapp/insertosinvoice";

            $post_fields = array(
                "type" => "upgrade_point",
                "trans_id" => $txnRef,
                "mbr_id" => $mbr_id,
                "mbr_type" => $mbr_month_type,
                "payment" => $payment,
                "trans_date" => date("Y-m-d H:i:s"),
                "contact_num" => $contact,
                "email_addr" => $email,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "item_id" => $item_id,
                "item_desc" => $item_desc
            );
            $this->_logprocess('VC_UPGPOINT_DATA', $mbr_month_type, json_encode($post_fields), $mbr_id, $txnRef);


            $resp_os = $this->curlImsApi($url, $post_fields);
            $this->_logprocess('VC_UPGPOINT_OS', $mbr_month_type, json_encode($resp_os), $mbr_id, $txnRef);

        }

//            $this->db20->commit();
        return;
//        } catch (\Exception $e) {
//            $this->db20->rollback();
//            $this->_logprocess('VC_RENEWPOINT_ERROR', 'XOK', json_encode($e), $mbr_id);
//            return;
//        }
    }


    private function sp_renew_member_purchase($txnRef, $mbr_id, $amount, $txnDate,
                                              $tstatus, $errorCode, $payment, $txnTime, $mbr_month_type, $upgrade_type, $email, $contact, $firstname)
    {
        $coy_id = $this->CTL_COY_ID;

        $this->_logNewprocess('VC_SIGNUP', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $upgrade_type, $amount, $txnDate);

        $this->db20->beginTransaction();
        try {
            $member = $this->db20->table('crm_member_list')
                ->select(DB::raw('cast(exp_date as date) as the_exp_date'), 'exp_date', 'last_renewal', 'mbr_type', 'last_name')
                ->where([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbr_id
                ])->first();

            $trans = $this->db20->table('crm_member_transaction')
                ->select(DB::raw("count(mbr_id) as line_num"))
                ->where([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbr_id,
                    'trans_id' => $txnRef
                ])->first();

            $types = $this->db20->table('crm_member_type')
                ->select(DB::raw("MAX(line_num) as line_num"))
                ->where([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbr_id
                ])->first();


            $type_num = $types ? $types->line_num + 1 : 1;
            $line_num = $trans ? $trans->line_num + 1 : 1;

            $dateNow = date("Y-m-d");
            $old_mbr_type = $member->mbr_type;
            $exp_date = $member->exp_date;
            $the_exp_date = $member->the_exp_date;
            $last_renewal = $member->last_renewal;
            $last_name = $member->last_name;


            if ($upgrade_type == 'renew') {
                if ($tstatus == 'f') {
                    //Failed
                    $this->_logNewprocess('VC_RENEW_FAIL', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $upgrade_type, $amount, $txnDate);


                } else if ($tstatus == 's') {
                    if ($mbr_month_type == 'M08') {
                        $start_exp_date = Carbon::now()->addMonth(8);
                        $next_exp_date = Carbon::parse($exp_date)->addMonth(8);
                        $item_id = "!MEMBER-REN08";
                        $item_desc = '8-Month Membership Renewal';
                        $amount = 8;
                    } else if ($mbr_month_type == 'M18') {
                        $start_exp_date = Carbon::now()->addMonth(18);
                        $next_exp_date = Carbon::parse($exp_date)->addMonth(18);
                        $item_id = "!MEMBER-REN18";
                        $item_desc = '18-Month Membership Renewal';
                        $amount = 18;
                    } else if ($mbr_month_type == 'M28') {
                        $start_exp_date = Carbon::now()->addMonth(28);
                        $next_exp_date = Carbon::parse($exp_date)->addMonth(28);
                        $item_id = "!MEMBER-REN28";
                        $item_desc = '28-Month Membership Renewal';
                        $amount = 28;
                    } else {
                        $start_exp_date = Carbon::now()->addMonth(24);
                        $next_exp_date = Carbon::parse($exp_date)->addMonth(24);
                        $item_id = "!MEMBER-REN";
                        $item_desc = '2-Year Membership Renewal';
                        $amount = 24;
                    }

                    //Renew here.
                    if ($the_exp_date > $dateNow) {
                        //User still available.
                        $eff_from = $exp_date;

                        $use_exp_date = $next_exp_date;


                        $sql_upd = <<<str
update crm_member_list set mbr_type = '$mbr_month_type', exp_date = '$use_exp_date' ,last_renewal = current_timestamp ,status_level = 1,
created_by = left('$mbr_id',15), modified_by = 'MBR_RENEW2',modified_on = current_timestamp ,updated_on = current_timestamp 
where coy_id='CTL' and  mbr_id = '$mbr_id' 
str;
                        $this->db20->statement($sql_upd);

                    } else {
                        //User has expired.
                        $use_exp_date = $start_exp_date;
                        $eff_from = date("Y-m-d H:i:s");

                        $sql_upd = <<<str
update crm_member_list set mbr_type = '$mbr_month_type' , exp_date = '$use_exp_date',last_renewal =  current_timestamp  ,status_level = 1,
        created_by = left('$mbr_id',15), modified_by = 'MBR_RENEW1',modified_on = current_timestamp  ,updated_on = current_timestamp 
where coy_id='CTL' and  mbr_id = '$mbr_id' 
str;
                        $this->db20->statement($sql_upd);
                    }

                    //Insert Crm_member _transaction
                    $sql_trans = <<<str
	insert into crm_member_transaction (coy_id,mbr_id,trans_id,line_num,trans_type,trans_date,loc_id,pos_id,
						item_id,item_desc,item_qty,regular_price,unit_price,disc_percent,disc_amount,trans_points,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) 
						values ('$coy_id','$mbr_id','$txnRef', '$line_num' ,'OS',current_timestamp,'OS','',
						'$item_id','$item_desc',1,'$amount','$amount',0,0,0,'',
						0,left('$mbr_id',15),current_timestamp ,'',current_timestamp,current_timestamp)
str;
                    $this->db20->statement($sql_trans);

                    //Insert Crm Member Type.
                    $sql_type = <<<str
insert into crm_member_type (coy_id, mbr_id, line_num, eff_from, eff_to, mbr_type, created_by, created_on, modified_by, modified_on) VALUES
								('$coy_id','$mbr_id','$type_num','$eff_from','$use_exp_date','$mbr_month_type','$mbr_id', current_timestamp , '$mbr_id', current_timestamp)
str;
                    $this->db20->statement($sql_type);


                    $this->_logNewprocess('VC_RENEW_OK', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $upgrade_type, $amount, $txnDate);

                    //Insert OS Invoice.
                    $url = $this->imsUrl . "vcapp/insertosinvoice";

                    $post_fields = array(
                        "type" => "renew",
                        "trans_id" => $txnRef,
                        "mbr_id" => $mbr_id,
                        "mbr_type" => $mbr_month_type,
                        "payment" => $payment,
                        "trans_date" => $txnDate,
                        "email_addr" => $email,
                        "contact_num" => $contact,
                        "first_name" => $firstname,
                        "last_name" => $last_name,
                        "item_id" => $item_id,
                        "item_desc" => $item_desc
                    );
                    $this->_logprocess('VC_RENEW_DATA', $mbr_month_type, json_encode($post_fields), $mbr_id, $txnRef);

                    $resp_os = $this->curlImsApi($url, $post_fields);
                    $this->_logprocess('VC_RENEW_OS', $mbr_month_type, json_encode($resp_os), $mbr_id, $txnRef);


                }
            } else if ($upgrade_type == 'upgrade') {
                if ($tstatus == 'f') {
                    //Failed
                    $this->_logNewprocess('VC_UPGRADE_FAIL', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $upgrade_type, $amount, $txnDate);


                } else if ($tstatus == 's') {
                    if ($mbr_month_type == 'M08') {
                        $start_exp_date = Carbon::now()->addMonth(8);
                        $item_desc = '8-Month Membership Renewal';
                        $item_id = "!MEMBER-REN08";
                        $amount = 8;
                    } else if ($mbr_month_type == 'M18') {
                        $start_exp_date = Carbon::now()->addMonth(18);
                        $item_id = "!MEMBER-REN18";
                        $item_desc = '18-Month Membership Renewal';
                        $amount = 18;
                    } else if ($mbr_month_type == 'M28') {
                        $start_exp_date = Carbon::now()->addMonth(28);
                        $item_desc = '28-Month Membership Renewal';
                        $item_id = "!MEMBER-REN28";
                        $amount = 28;
                    } else {
                        $start_exp_date = Carbon::now()->addMonth(24);
                        $item_desc = '2-Year Membership Renewal';
                        $item_id = "!MEMBER-REN";
                        $amount = 24;
                    }

                    //Upgrade here.
                    $sql_diff = <<<str
select SUM(years) as year_diff, SUM(months) as month_diff from (
                                          select DATE_PART('month', AGE(eff_to, current_timestamp)) AS months,
                                                 DATE_PART('year', AGE(eff_to, current_timestamp)) AS years
                                          from crm_member_type
                                          where mbr_id = ?
                                            and eff_to >= '$dateNow'
                                      ) as a

str;


                    $type_lists = $this->db20->select($sql_diff, [$mbr_id]);

                    $month_diff = $type_lists[0]->month_diff;
                    $year_diff = $type_lists[0]->year_diff;
                    if ($year_diff > 0) {
                        $month_diff = $month_diff + ($year_diff * 12);
                    }
                    if (!$month_diff) {
                        $month_diff = 1;
                    }

                    //Update crm_member_list
                    $sql_crm = <<<str
update crm_member_list set mbr_type = '$mbr_month_type', exp_date = '$start_exp_date' ,last_renewal = current_timestamp  ,status_level = 1,
modified_by = 'UPGRADE_1',modified_on = current_timestamp,updated_on =current_timestamp where coy_id='CTL' and  mbr_id = '$mbr_id'
str;
                    $this->db20->statement($sql_crm);

                    //Insert crm_member_transaction
                    $sql_trans = <<<str
	insert into crm_member_transaction (coy_id,mbr_id,trans_id,line_num,trans_type,trans_date,loc_id,pos_id,
						item_id,item_desc,item_qty,regular_price,unit_price,disc_percent,disc_amount,trans_points,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) 
						values ('$coy_id','$mbr_id','$txnRef', '$line_num' ,'OS',current_timestamp,'OS','',
						'$item_id','$item_desc',1,'$amount','$amount',0,0,0,'',
						0,left('$mbr_id',15),current_timestamp ,'',current_timestamp,current_timestamp)
str;
                    $this->db20->statement($sql_trans);


                    //Update Old Crm_member_type;
                    $sql_upd_type = <<<str
update crm_member_type set eff_to= current_timestamp where (line_num < ? and eff_to > current_timestamp )and coy_id='$coy_id' and  mbr_id ='$mbr_id'
str;
                    $this->db20->statement($sql_upd_type, [$type_num]);


                    //Insert new crm_member_type
                    $sql_ins_type = <<<str
				insert into crm_member_type (coy_id, mbr_id, line_num, eff_from, eff_to, mbr_type, created_by, created_on, modified_by, modified_on) VALUES
                ('$coy_id','$mbr_id', '$type_num', current_timestamp , '$start_exp_date','$mbr_month_type','$mbr_id', current_timestamp, 'upgrade', current_timestamp)
str;
                    $this->db20->statement($sql_ins_type, []);

                    //Refund here.
                    if ($month_diff > 0) {
                        $refund_point = $month_diff * 100;

                        $point_list = $this->db20->table('crm_member_points')
                            ->where('coy_id', $coy_id)
                            ->where('mbr_id', $mbr_id)
                            ->whereRaw("CAST(exp_date AS DATE) = '$start_exp_date'")
                            ->first();

                        if ($point_list) {
                            $point_line_num = $point_list->line_num;

                            $sql_upd_point = <<<str
UPDATE crm_member_points SET points_accumulated += ?, modified_by = '$mbr_id', modified_on = current_timestamp  
WHERE coy_id = '$coy_id' AND mbr_id = '$mbr_id' AND line_num = ?
str;
                            $this->db20->statement($sql_upd_point, [$refund_point, $point_line_num]);

                        } else {

                            $points = $this->db20->table('crm_member_points')
                                ->select(DB::raw("MAX(line_num) as line_num"))
                                ->where([
                                    'coy_id' => 'CTL',
                                    'mbr_id' => $mbr_id
                                ])->first();

                            $point_line = $points ? (isset($points->line_num) ? $points->line_num + 1 : 1) : 1;

                            //Insert mbr_points
                            $sql_ins_points = <<<str
insert into crm_member_points (coy_id, mbr_id, line_num, exp_date, points_accumulated, points_redeemed, points_expired, created_by, created_on, modified_by, modified_on, updated_on) VALUES
										('$coy_id','$mbr_id', '$point_line', '$start_exp_date', $refund_point, 0,0,'$mbr_id', current_timestamp ,'$mbr_id', current_timestamp, current_timestamp)
str;
                            $this->db20->statement($sql_ins_points, []);

                        }


                        //Update member point accumulated.
                        $sql_upd_mbr = <<<str
UPDATE crm_member_list SET points_accumulated = points_accumulated + ? , modified_by = 'Vc30' , modified_on = current_timestamp  WHERE coy_id = '$coy_id' AND mbr_id = '$mbr_id'
str;
                        $this->db20->statement($sql_upd_mbr, [$refund_point]);

                        $yearMonth = date("Ym");
                        $trans_prefix = 'ADJUSTAP' . $yearMonth;
                        $so_id_header = 'AP' . $yearMonth;

                        $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header, 15, 'MEMBER', 'CRM', 'CTL');

                        $AP_itemdesc = 'Membership upgrade to ' . $mbr_month_type . ' and refund ' . $month_diff . ' months via rebate';

                        //Insert AP crm_member_transaction.
                        $sql_ins_ap = <<<str
   insert into crm_member_transaction(coy_id, mbr_id, trans_id, trans_date, loc_id, pos_id, line_num, trans_type, item_id, item_desc, item_qty, regular_price, unit_price,
   trans_points, disc_percent,disc_amount,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) VALUES
     ('$coy_id', '$mbr_id', '$ap_trans_id', current_timestamp ,'' ,'' , 1, 'AP', '', '$AP_itemdesc', ? , 100,1,
      ? ,'0','0','','0', '$mbr_id', current_timestamp, 'Vc30', current_timestamp, current_timestamp )
str;
                        $this->db20->statement($sql_ins_ap, [$month_diff, $refund_point]);

                    }


                    $this->_logNewprocess('VC_UPGRADE_OK', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $upgrade_type, $amount, $txnDate);


                    //Insert OS Invoice.
                    $url = $this->imsUrl . "vcapp/insertosinvoice";

                    $post_fields = array(
                        "type" => "upgrade",
                        "trans_id" => $txnRef,
                        "mbr_id" => $mbr_id,
                        "mbr_type" => $mbr_month_type,
                        "payment" => $payment,
                        "trans_date" => $txnDate,
                        "contact_num" => $contact,
                        "email_addr" => $email,
                        "first_name" => $firstname,
                        "last_name" => $last_name,
                        "item_id" => $item_id,
                        "item_desc" => $item_desc
                    );
                    $this->_logprocess('VC_UPGRADE_DATA', $mbr_month_type, json_encode($post_fields), $mbr_id, $txnRef);

                    $resp_os = $this->curlImsApi($url, $post_fields);
                    $this->_logprocess('VC_UPGRADE_OS', $mbr_month_type, json_encode($resp_os), $mbr_id, $txnRef);


                }
            }

            $this->db20->commit();
            return;
        } catch (\Exception $e) {
            $this->db20->rollback();
            $this->_logprocess('VC_RENEWOLD_ERROR', 'XOK', json_encode($e), $mbr_id);
            return;
        }

        return;
    }

    public function member_renew_rebate(Request $request)
    {
        $mbr_id = $request->mbr_id;
        $txnRef = $request->transcode;
        $fn = $request->renew_type;

        $mbr_type = $request->mbr_type;
        if ($fn == "upgrade") {
            $renew_type = "upgrade";
            $rebate_renew_fee = (int)$this->mbrShipFee[$mbr_type] * 100;
        } else {
            $renew_type = "renew";
            $rebate_renew_fee = (int)$this->mbrShipFee_viaRebate[$mbr_type] * 100;
        }

//        print_r($rebate_renew_fee);
//        die();

        $email = $request->email;
        $first = $request->firstname;
        $renew_renewal = $request->payment;

        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);
        //New Rebate Amount Get.
        $rebate_fee = (int)$renew_renewal * 100;

        $amount = str_pad(str_replace(".", "", $rebate_fee), 12, '0', STR_PAD_LEFT);
        $payment = floatval($amount);

        if ((int)$rebate_fee !== $rebate_renew_fee) {
            return $this->apiSuccessResponse(0, "Failed", "Error while sign up. Please go back and try again.");
        }

        $rebate_avail = $this->vcmemberRepository->getAvailablePoints($mbr_id, $this->CTL_COY_ID);

        if ((int)$rebate_avail < $rebate_fee) {
            echo "Error while sign up. Please go back and try again.";
            die();
        }

        $this->sp_renew_member_points($txnRef, $mbr_id, $amount, $payment, $mbr_month_type, $renew_type);

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        $email_template = '';
        $send_email = $email;
        if ($fn == "upgrade") {
            $email_template = $this->emarsys_template_list['UPGRADE_MBRSHIP'];
            $email_data = array(
                "r_FirstName" => ucwords(strtolower($first)),
            );
        } else {

            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['RENEW_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['RENEW_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['RENEW_28MONTHS'];
            }
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );
        }

        if (strpos($send_email, 'weiseng') !== false && $this->ENVIRONMENT == 'development') {
            $send_email = 'wong.weiseng@challenger.sg';
        }

        //-- Set Email pending for processing.
        $this->_sendEmarsysEmail($email_template, $send_email, $email_data, $txnRef);

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        return $this->apiSuccessResponse(1, 'Success', $mbr_id);

    }

    public function renew_viaRebates(Request $request)
    {
        ini_set("display_errors", 1);

        $redisData = $request->invoiceData;
        $renew_renewal = $request->renew_renewal;
        $renew_mbrtype = $request->renew_mbrtype;

        $invoiceData = base64_decode($redisData);
        $invoiceData = json_decode($invoiceData, TRUE);

        $data = $invoiceData["data"];

//        print_r($data);
//        die();

        $email = $invoiceData['email_addr'];
        $first = $data['firstname'];
        $txnRef = $invoiceData['txnRef'];

        //New Rebate Amount Get.
        $rebate_fee = (int)$renew_renewal * 100;

        $amount = str_pad(str_replace(".", "", $rebate_fee), 12, '0', STR_PAD_LEFT);
        $payment = floatval($amount);
        $mbr_type = $renew_mbrtype;
        $fn = $invoiceData["fn"];

        if ($fn == "upgrade") {
            $renew_type = "upgrade";
            $rebate_renew_fee = (int)$this->mbrShipFee[$mbr_type] * 100;
        } else {
            $renew_type = "renew";
            $rebate_renew_fee = (int)$this->mbrShipFee_viaRebate[$mbr_type] * 100;
        }

        if ((int)$rebate_fee !== $rebate_renew_fee) {
            echo "Error while sign up. Please go back and try again.";
            die();
        }

        $mbr_month_type = $this->getMemberTypeByCardItemId($mbr_type);

        $mbr_id = $data['mbr_id'];
        $rebate_avail = $this->vcmemberRepository->getAvailablePoints($mbr_id, $this->CTL_COY_ID);

        if ((int)$rebate_avail < $rebate_fee) {
            echo "Error while sign up. Please go back and try again.";
            die();
        }

        $this->sp_renew_member_points($txnRef, $mbr_id, $amount, $payment, $mbr_month_type, $renew_type);

        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        Redis::del($hkey);

        $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        $email_template = '';
        $send_email = $email;
        if ($fn == "upgrade") {
            $email_template = $this->emarsys_template_list['UPGRADE_MBRSHIP'];
            $email_data = array(
                "r_FirstName" => ucwords(strtolower($first)),
            );
        } else {

            if ($mbr_month_type === 'M08') {
                $email_template = $this->emarsys_template_list['RENEW_8MONTHS'];
            } else if ($mbr_month_type === 'M18') {
                $email_template = $this->emarsys_template_list['RENEW_18MONTHS'];
            } else if ($mbr_month_type === 'M28') {
                $email_template = $this->emarsys_template_list['RENEW_28MONTHS'];
            }
            $email_data = array(
                "r_c_first_name" => ucwords(strtolower($first)),
            );
        }

        if (strpos($send_email, 'weiseng') !== false && $this->ENVIRONMENT == 'development') {
            $send_email = 'wong.weiseng@challenger.sg';
        }

//        echo $email_template;

        //-- Set Email pending for processing.
        $this->_sendEmarsysEmail($email_template, $send_email, $email_data, $txnRef);

        $response = array("fn" => $fn, "status" => 's');
        $response = json_encode($response);

        $js = <<<str
<script>
    parent.postMessage($response, "*");
</script>
str;
        echo $js;

    }

    public function testtrans()
    {
//        echo $this->ENVIRONMENT;
        $txnRef = 'OS202104000T007';
        $mbr_id = 'V16000T007';
        $amount = 8;
        $payment = 8;
        $txnDate = '2021-04-28 14:05:05';
        $tstatus = 's';
        $email_addr = 'weiseng107@challenger.sg';
        $errorCode = '';
        $code = '123';
        $txnTime = "13:03:03";
        $mbr_month_type = 'M08';
        $mbr_type = "";
        $first = 'tester';
        $last = 'wong';
        $dob = '1900-01-01';
        $password = 'qweqwe';
        $contact = "91823337";

        $this->sp_new_member_purchase($txnRef, $mbr_id, $amount, $txnDate, $tstatus, $errorCode, $payment, $txnTime, $email_addr, $code, $mbr_month_type,
            $first, $last, $contact, $dob, $password, $mbr_type);
    }

    public function manualRefundRebate(Request $request)
    {

        $start_exp_date = date("Y-m-d");
        $coy_id = 'CTL';
        $mbr_id = $request->mbr_id;

        $month_diff = 8;
        $refund_point = $month_diff * 100;
        $mbr_month_type = 'M28';

//        //Update member point accumulated.
//        $sql_upd_mbr = <<<str
//UPDATE crm_member_list SET points_accumulated = points_accumulated + ? , modified_by = 'Vc30' , modified_on = current_timestamp  WHERE coy_id = '$coy_id' AND mbr_id = '$mbr_id'
//str;
//        $this->db20->statement($sql_upd_mbr, [$refund_point]);

        $yearMonth = date("Ym");
        $trans_prefix = 'ADJUSTAP' . $yearMonth;
        $so_id_header = 'AP' . $yearMonth;

        $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $so_id_header, 15, 'MEMBER', 'CRM', 'CTL');

        $AP_itemdesc = 'Membership upgrade to ' . $mbr_month_type . ' and refund ' . $month_diff . ' months via rebate';

        //Insert AP crm_member_transaction.
        $sql_ins_ap = <<<str
   insert into crm_member_transaction(coy_id, mbr_id, trans_id, trans_date, loc_id, pos_id, line_num, trans_type, item_id, item_desc, item_qty, regular_price, unit_price,
   trans_points, disc_percent,disc_amount,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) VALUES
     ('$coy_id', '$mbr_id', '$ap_trans_id', current_timestamp ,'' ,'' , 1, 'AP', '', '$AP_itemdesc', ? , 100,1,
      ? ,'0','0','','0', '$mbr_id', current_timestamp, 'Vc30', current_timestamp, current_timestamp )
str;
        $this->db20->statement($sql_ins_ap, [$month_diff, $refund_point]);


    }


    private function sp_new_member_purchase($txnRef, $mbr_id, $amount, $txnDate,
                                            $tstatus, $errorCode, $payment, $txnTime, $email_addr, $code, $mbr_month_type,
                                            $first_name, $last_name, $contact, $dob, $pass1, $mbr_type)
    {
        $this->_logNewprocess('VC_SIGNUP', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $email_addr, $contact, $txnDate);

        $this->db20->beginTransaction();
        try {

            if ($tstatus == 'f') {
                //Failed
                $this->_logNewprocess('VC_SIGNUP_FAIL', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $email_addr, $contact, $txnDate);


            } else if ($tstatus == 's') {
                if ($mbr_month_type == 'M08') {
                    $exp_date = Carbon::now()->addMonth(8);
                    $item_desc = '8-Month Membership';
                    $amount = 8;
                } else if ($mbr_month_type == 'M18') {
                    $exp_date = Carbon::now()->addMonth(18);
                    $item_desc = '18-Month Membership';
                    $amount = 18;
                } else if ($mbr_month_type == 'M28') {
                    $exp_date = Carbon::now()->addMonth(28);
                    $item_desc = '28-Month Membership';
                    $amount = 28;
                } else {
                    $exp_date = Carbon::now()->addMonth(24);
                    $item_desc = '2-Year Membership';
                    $amount = 24;
                }


                //Insert Member.
                $insert_data['coy_id'] = $this->CTL_COY_ID;
                $insert_data['mbr_id'] = $mbr_id;
                $insert_data['mbr_type'] = $mbr_month_type;
                $insert_data['status_level'] = 1;
                $insert_data['email_addr'] = $email_addr;
                $insert_data['first_name'] = $first_name;  // Hardcoded first
                $insert_data['last_name'] = $last_name; // Hardcoded first
                $insert_data['contact_num'] = $contact; // Hardcoded first
                $insert_data['birth_date'] = $dob;
                $insert_data['main_id'] = '';
                $insert_data['exp_date'] = $exp_date;
                $insert_data['mbr_pwd'] = $pass1;
                $insert_data['login_locked'] = 'N';

                $this->vcmemberRepository->addAccount($insert_data);

                //Insert mbr Type. (Line Num 1.)
                $this->vcmemberRepository->insertMemberType($mbr_id, $mbr_id, $exp_date, $mbr_month_type);


                //Insert crm_member_transaction.
                $sql_trans = <<<str
insert into crm_member_transaction (coy_id,mbr_id,trans_id,line_num,trans_type,trans_date,loc_id,pos_id,
						item_id,item_desc,item_qty,regular_price,unit_price,disc_percent,disc_amount,trans_points,salesperson_id,
						mbr_savings,created_by,created_on,modified_by,modified_on,updated_on) 
						values ('CTL','$mbr_id','$txnRef',1,'OS',current_timestamp,'OS','',
						'$mbr_type', '$item_desc',1,'$amount','$amount',0,0,0,'',
						0,left('$mbr_id',15),current_timestamp ,left('$mbr_id',15),current_timestamp ,current_timestamp);
str;
                $this->db20->statement($sql_trans);

                $this->_logNewprocess('VC_SIGNUP_OK', $txnRef, $mbr_month_type, $tstatus, $errorCode . "-" . $payment . "-" . $txnTime, $mbr_id, $email_addr, $contact, $txnDate);


                $url = $this->imsUrl . "vcapp/insertosinvoice";

                $post_fields = array(
                    "type" => "signup",
                    "trans_id" => $txnRef,
                    "mbr_id" => $mbr_id,
                    "mbr_type" => $mbr_month_type,
                    "payment" => $payment,
                    "trans_date" => $txnDate,
                    "first_name" => $first_name,
                    "contact_num" => $contact,
                    "email_addr" => $email_addr,
                    "last_name" => $last_name,
                    "item_id" => $mbr_type,
                    "item_desc" => $item_desc
                );
//                print_r($post_fields);
                $this->_logprocess('VC_SIGNUP_DATA', $mbr_month_type, json_encode($post_fields), $mbr_id, $txnRef);

                $resp_os = $this->curlImsApi($url, $post_fields);
//                print_r($resp_os);

                $this->_logprocess('VC_SIGNUP_OS', $mbr_month_type, json_encode($resp_os), $mbr_id, $txnRef);

            }

            $this->db20->commit();

            return;
        } catch (\Exception $e) {
            $this->db20->rollback();
            $this->_logprocess('VC_SIGNUP_ERROR', 'XOK', json_encode($e), $mbr_id);
            return;
        }

    }

    private function _issueSignUpVoucher($mbr_id, $mbr_type, $trans_id = '', $trans_type = '')
    {
        if ($this->ENVIRONMENT == 'development') {
            $server_name = "https://chvc-api.sghachi.com/";
        } else {
            $server_name = "https://vc.api.valueclub.asia/";
        }

        $api = "api/coupon/welcomepack";

        $key_code = "?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";

        $url = $server_name . $api . $key_code;

//		echo $url;

        $post_data = array(
            "mbr_id" => $mbr_id,
            "mbr_type" => $mbr_type,
            "trans_id" => $trans_id,
            "trans_type" => $trans_type
        );
        $headers = array(
            "Content-Type: application/json",
            'Authorization: Bearer ' . 'aaaaaa'
        );

//		print_r($post_data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, TRUE);

//		print_r($response);
//		die();

        return $response;
    }

    public function getMemberTypeByCardItemId($itemId)
    {
        $itemId = strtoupper(trim($itemId));

        if (in_array($itemId, ['!MEMBER-NEW08', '!MEMBER-RR08', '!MEMBER-REN08', '!MEMBER-UPG08'], true)) {
            return 'M08';
        }

        if (in_array($itemId, ['!MEMBER-NEW18', '!MEMBER-RR18', '!MEMBER-REN18', '!MEMBER-UPG18'], true)) {
            return 'M18';
        }

        if (in_array($itemId, ['!MEMBER-NEW28', '!MEMBER-RR28', '!MEMBER-REN28', '!MEMBER-UPG28'], true)) {
            return 'M28';
        }

        if ($itemId === '!MEMBER-HACHI') {
            return 'M';
        }

        return '';
    }


    public function subscribeNewHpInkDevice(Request $request)
    {

        $mbr_id = $request->mbr_id;
        $serial_no = $request->serial_no ? trim($request->serial_no) : '';
        $model_id = $request->model_id;
        $invoice_id = $request->invoice_id ? $request->invoice_id : '';

        $sales_id = $request->sales_id ? $request->sales_id : '';

        $subscribe_type = $request->subscribe_type ? $request->subscribe_type : '';

        //HARDCODE, change it when go LIVE.
        $is_warrantypromo = true;

        $validate_mbr = $this->vcmemberRepository->verifyNewMbrId($mbr_id);
        if (!$validate_mbr) {
            return $this->apiSuccessResponse(0, "Your member information is still pending for processing. This would take about 1 day to process, you may try again tomorrow.", []);
        }

        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            return $this->apiSuccessResponse(0, "Unable to Get Access Token", []);
        }
        $access_token = $access_response["access_token"];
//        print_r($access_token);
        $partnerId = $this->hpPartnerId;

        $index_key = trim($mbr_id);
        $sql_get = <<<str
    select * from "members_meta" where key= ?
str;
        $hr_detail = $this->chapps->select($sql_get, [$index_key]);

        $region = "Outlet";
        if ($invoice_id) {
            $trans_sql = <<<str
    select RTRIM(trans_type) as trans_type, 
    CONCAT(cast(trans_date as date), 'T', LEFT(cast(cast(trans_date as time) as varchar(20)),8) , '.000Z') as trans_date
    from crm_member_transaction where trans_id = ?
str;
            $invoice_lists = $this->db20->select($trans_sql, [$invoice_id]);
            if (in_array($invoice_lists[0]->trans_type, ['HI', 'HR'])) {
                $region = "Hachi";
            } else {
                $region = "Outlet";
            }

            if (count($invoice_lists) > 0) {
                $purchase_date = $invoice_lists[0]->trans_date;
            } else {
                $purchase_date = date("Y-m-d") . 'T' . date("H:i:s") . '.000Z';
            }

        } else {
            $printer_sql = <<<str
	select *, 
	 CONCAT(cast(created_on as date), 'T', LEFT(cast(cast(created_on as time) as varchar(20)),8) , '.000Z') as created_date
	from crm_hp_printer_list where mbr_id=? and model_id=? and lot_id=?
str;
            $printer_list = $this->db20->select($printer_sql, [$mbr_id, $model_id, $serial_no]);
//			print_r($printer_list);
            if (count($printer_list) > 0) {
                $purchase_date = $printer_list[0]->created_date;
            } else {
                $purchase_date = date("Y-m-d") . 'T' . date("H:i:s") . '.000Z';
            }
        }


        //Check ALL API Logs.
        if ($sales_id) {
            $sql = <<<str
      insert into o2o_log_live_processes (coy_id, process_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_INSERT_LOG', 'OK', ?, ?, ?, current_timestamp )
str;
            $this->db20->statement($sql, [$sales_id . " | " . $serial_no . " |" . $subscribe_type, $mbr_id, $invoice_id]);
        } else {
            $sql = <<<str
      insert into o2o_log_live_processes (coy_id, process_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_INSERT_LOG', 'OK', ?, ?, ?, current_timestamp)
str;
            $this->db20->statement($sql, ['' . " | " . $serial_no . " |" . $subscribe_type, $mbr_id, $invoice_id]);
        }

        //Get Promo Warranty Info
        $has_warrantystart = false;
        if ($is_warrantypromo && substr($subscribe_type, 0, 3) == 'P1-') {
            $printerWarrantyInfo = $this->getHpWarrantyPromo($model_id);
//            print_r($printerWarrantyInfo);
            if ($printerWarrantyInfo) {

                $warrantyStart = $printerWarrantyInfo["warrantyStartDate"];
                $warrantyEnd = $printerWarrantyInfo["warrantyEndDate"];

                $warrantyStartDate = date("Y-m-d", strtotime($warrantyStart));
                $warrantyEndDate = date("Y-m-d", strtotime($warrantyEnd));

                $dateNow = date("Y-m-d");
                if ($dateNow >= $warrantyStartDate && $dateNow <= $warrantyEndDate) {
                    $customer_sql = <<<str
	select
    coalesce(member.first_name,'-') as first_name,
    coalesce(member.last_name,'-') as last_name,
    RTRIM(member.email_addr) as email_addr,
    cast(address.postal_code as varchar(12)) as postal_code,
    CONCAT(RTRIM(address.street_line1) , ', ' , RTRIM(address.street_line4)) as street_line1,
    CONCAT('#', RTRIM(address.street_line2) , '-' , RTRIM(address.street_line3)) as street_line2
from crm_member_list member
left join coy_address_book address on address.ref_id= member.mbr_id and address.coy_id = member.coy_id and address.addr_type='0-PRIMARY'
where member.coy_id='CTL' and member.mbr_id= ? and member.status_level=1
str;
                    $customer_info = $this->db20->select($customer_sql, [$mbr_id]);
//                    print_r($customer_info);
                    if (count($customer_info) > 0) {
                        $customer_info = $this->vcmemberRepository->stdToArray($customer_info[0]);

                        $has_warrantystart = true;

                        $cust_first_name = $customer_info["first_name"] ? str_replace(" ", "", $customer_info["first_name"]) : 'VcUser';
                        $cust_last_name = $customer_info["last_name"] ? str_replace(" ", "", $customer_info["last_name"]) : 'VcUser';
                        $cust_postal_code = isset($customer_info["postal_code"]) ? (int)$customer_info["postal_code"] : $customer_info["postal_code"];
                        $cust_email_addr = $customer_info["email_addr"] ? $customer_info["email_addr"] : '-';
                        $cust_street = $customer_info["street_line1"] ? $customer_info["street_line1"] : '-';
                        $cust_building = $customer_info["street_line2"] ? $customer_info["street_line2"] : '-';
                    }
                }
            }
        }


        //Unable to return value when already subscribed.
        if ($hr_detail) {

            //Add New Device to hr id.
            $hr_id = $hr_detail[0]->meta;

            if ($this->ENVIRONMENT == "production") {
                $url = "https://acr.ext.hp.com/acr/v1/devices/$partnerId";
            } else {
                $url = "https://acr-dev.ext.hp.com/acr/v1/devices/$partnerId";
            }

            $params = array(
                "hp_sr_id" => $hr_id,
                "printers" => [],
                "customerConsent" => "true"
            );

            $params["printers"][] = array(
                "modelNumber" => $model_id,
                "serialNumber" => $serial_no,
                "region" => $region,
            );
            if ($subscribe_type) {
                $params["printers"][0]["subscriptionMode"] = $subscribe_type;
            } else {
                $params["printers"][0]["subscriptionMode"] = 'P1-Store';
            }

            //New HP Warranty Promo.
            if ($is_warrantypromo) {
                $params["printers"][0]["purchaseDate"] = $purchase_date;
            }
            if ($has_warrantystart) {

                $params["email"] = $cust_email_addr;
                $params["firstName"] = $cust_first_name;
                $params["lastName"] = $cust_last_name;

                if ($cust_postal_code) {
                    $addressParams = array(
                        "street" => $cust_street,
                        "building" => $cust_building,
                        "postalCode" => $cust_postal_code
                    );
                    $params["customerAddress"] = $addressParams;
                }
            }

//			print_r($params);
//			die();

            $post_fields = json_encode($params);

            $headers = array(
                'Authorization: Bearer ' . $access_token,
                "Content-Type: application/json",
                'Content-Length: ' . strlen($post_fields)
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, 1);

            //SEND EDM HERE
//            print_r($response);

            if ($response && (isset($response["success_message"]) || isset($response["message"]))) {
                if ($response["message"] && (strpos($response["message"], "eligible") !== false || strpos($response["message"], "Invalid") !== false ||
                        strpos($response["message"], "exists") !== false || strpos($response["message"], "failed") !== false || strpos($response["message"], "serialNumber") !== false)) {
                    $this->_logprocess('HP_SUBSCRIBE_ERROR', 0, $serial_no . $response["message"], $mbr_id, 'VcApp');
                    return $this->apiSuccessResponse(0, $response["message"], []);
                }

                $this->addPrinterVoucherLog($invoice_id, $mbr_id, $serial_no, $model_id, $sales_id);
                return $this->apiSuccessResponse(1, "Success", []);
            } else if ($response && isset($response["Error"])) {
                $this->_logprocess('HP_SUBSCRIBE_ERROR', 0, $serial_no . json_encode($response), $mbr_id, 'VcApp');
                return $this->apiSuccessResponse(0, $response["Error"], $response["Error"]);
            }
        } else {
            //Subsbribe new hr id & devices.
            if ($this->ENVIRONMENT == "production") {
                $url = "https://acr.ext.hp.com/acr/v1/devices";
            } else {
                $url = "https://acr-dev.ext.hp.com/acr/v1/devices";
            }

            //For Testing.
//            $url = "https://acr.ext.hp.com/acr/v1/devices";

            $params = array(
                "partnerId" => $partnerId,
                "printers" => [],
                "customerConsent" => "true"
            );

            $params["printers"][] = array(
                "modelNumber" => $model_id,
                "serialNumber" => $serial_no,
                "region" => $region,
            );
            //Get Subscribe type.
            if ($subscribe_type) {
                $params["printers"][0]["subscriptionMode"] = $subscribe_type;
            } else {
                $params["printers"][0]["subscriptionMode"] = 'P1-Store';
            }

            //New HP Warranty Promo.
            if ($is_warrantypromo) {
                $params["printers"][0]["purchaseDate"] = $purchase_date;
            }
            if ($has_warrantystart) {

                $params["email"] = $cust_email_addr;
                $params["firstName"] = $cust_first_name;
                $params["lastName"] = $cust_last_name;

                if ($cust_postal_code) {
                    $addressParams = array(
                        "street" => $cust_street,
                        "building" => $cust_building,
                        "postalCode" => $cust_postal_code
                    );
                    $params["customerAddress"] = $addressParams;
                }
            }


//			print_r($params);
//			die();

            $post_fields = json_encode($params);
            $headers = array(
                'Authorization: Bearer ' . $access_token,
                "Content-Type: application/json",
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            $result = curl_exec($ch);
            curl_close($ch);


            $response = json_decode($result, 1);

//            print_r($response);

            if ($response && isset($response["hp_sr_id"])) {
                $hr_id = $response["hp_sr_id"];

                //Insert into Postgre.
                $query = <<<str
        INSERT INTO "members_meta" (key, meta) VALUES (?, ?)
str;
                $insert_res = $this->chapps->statement($query, [$index_key, $hr_id]);

                $this->_logprocess('HPINK_USERID', 1, $hr_id, $mbr_id, 'VcApp2');

                $this->addPrinterVoucherLog($invoice_id, $mbr_id, $serial_no, $model_id, $sales_id);
                return $this->apiSuccessResponse(1, "Success", $hr_id);
            } else if ($response && isset($response["message"])) {
                if ((strpos($response["message"], "eligible") !== false || strpos($response["message"], "Invalid") !== false || strpos($response["message"], "exists") !== false || strpos($response["message"], "failed") !== false) || strpos($response["message"], "serialNumber") !== false) {
                    $this->_logprocess('HP_SUBSCRIBE_ERROR', 0, $serial_no . $response["message"], $mbr_id, 'VcApp');
                    return $this->apiSuccessResponse(0, $response["message"], []);
                }
                //echo "continue";
                $this->addPrinterVoucherLog($invoice_id, $mbr_id, $serial_no, $model_id, $sales_id);
                return $this->apiSuccessResponse(1, $response["message"], $response["message"]);
            } else if ($response && isset($response["Error"])) {
                $this->_logprocess('HP_SUBSCRIBE_ERROR', 0, $serial_no . json_encode($response), $mbr_id, 'VcApp');
                return $this->apiSuccessResponse(0, $response["Error"], $response["Error"]);
            }
        }
    }

    private function addPrinterVoucherLog($invoice_id, $mbr_id, $serial_no, $model_id, $sales_id)
    {
        //Disable it. (Call it when callback of Printer Activation.)
//        $add_voucher_sql = <<<str
//    EXEC spGetHpAutoInkCoupon ?,?,?
//str;
//        $res = $this->db->query($add_voucher_sql, [$mbr_id, $invoice_id, $serial_no]);

        $sql = <<<str
      insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_SUBSCRIBED_PRINTER', ? ,? , 'OK', ?, ?, ?, current_timestamp )
str;
        $this->db20->statement($sql, [$serial_no, $invoice_id, $model_id, $mbr_id, $mbr_id]);

        if ($sales_id) {
            $sql = <<<str
      insert into o2o_log_live_processes (coy_id, process_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_SUBSCRIBED_SALESID', 'OK', ?, ?, ?, current_timestamp)
str;
            $this->db20->statement($sql, [$sales_id, $mbr_id, $invoice_id]);
        }

        return;
    }

    public function getHpWarrantyPromo($model_id = '')
    {
        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            echo "Unable to Get Access Token";
            die();
        }
        $access_token = $access_response["access_token"];
//        print_r($access_token);
//        die();

        $partnerId = $this->hpPartnerId;

        if ($this->ENVIRONMENT == "production") {
            $url = "https://acr.ext.hp.com/acr/v1/warrantypromo/$partnerId";
        } else {
            $url = "https://acr-dev.ext.hp.com/acr/v1/warrantypromo/$partnerId";
        }

        $headers = array(
            'Authorization: Bearer ' . $access_token,
            "Content-Type: application/json",
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $response = json_decode($result, 1);

        curl_close($ch);


        $printer_info = null;
        foreach ($response as $index => $printer) {
            $printer_model_id = $printer["printerModelNumber"];
//			echo $printer_model_id. "<br/>";

            if ($printer_model_id == $model_id) {
                $printer_info = $printer;
                break;
            }
        }

//		print_r($printer_info);
        return $printer_info;
    }


    public function getScheduleItem(Request $request)
    {
        $email_id = $request->email_id;

        if (!$email_id) {
            return $this->errorWithInfo("Email ID: " . $email_id . " not found.");
        }

        $lists = $this->db20
            ->table('o2o_nid_reschedule_list')
            ->where('email_id', $email_id)
            ->get();

        return $this->successWithData($lists);
    }

    public function updateEmailSchedule(Request $request)
    {
        $email_id = $request->email_id;
        $timeslot = $request->timeslot;
        $reschedule_date = $request->reschedule_date;
        $remarks = $request->remarks;

        if (!$email_id) {
            return $this->errorWithInfo("Email ID: " . $email_id . " not found.");
        }

        $upd = $this->db20
            ->table('o2o_nid_reschedule_list')
            ->where('email_id', $email_id)
            ->update([
                'reschedule_date' => $reschedule_date,
                'timeslot' => $timeslot,
                'status_level' => 1,
                'remarks' => $remarks
            ]);

        return $this->successWithData($upd);
    }

    public function sendUpdateProfileEmail(Request $request)
    {
        $firstname = $request->firstname;
        $email = $request->email;
        if (!$email) {
            return $this->apiSuccessResponse(0, "Email is empty.", []);
        }

        $send_email = $email;
        $email_data = [
            'fname' => ucwords(strtolower($firstname)),
        ];

        $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["SUCCESS_UPDATE_PROFILE"], $send_email, $email_data);

        return $this->apiSuccessResponse(1, "Email has been sent.", $email_result);
    }

    public function resetPassReturn($email0)
    {
        $postdata['email'] = $email0;

        if (!trim($postdata['email'])) {
            return $this->apiSuccessResponse(0, "No username / email address has been specified", []);
        }

        $data = $this->vcmemberRepository->isEmailValid($postdata['email']);
        if (!$data["valid"]) {
            return $this->apiSuccessResponse(0, "Email does not exist or has already been expired", $this->messages["EMAIL_NOT_EXIST"]);
        }
        $first_name = trim($data['data']->first_name);
        $mbr_token = trim($data['data']->member_token);

        $send_email = $postdata["email"];
        $email_data = array(
            "r_ctl" => "CTL",
            "r_c_first_name" => $first_name,
            "r_c_mbr_token" => $mbr_token,
            "r_c_sub_ind1" => "",
            "r_c_sub_ind2" => ""
        );

        $this->_sendEmarsysEmail($this->emarsys_template_list["RESET_PASSWORD_RETURN"], $send_email, $email_data);
        return $this->apiSuccessResponse(1, "An email has been sent.", $this->messages["EMAIL_SENT"]);
    }

    public function updateOrInsertMbr($mbr_id = '', $loginPass = '', $returnJson = 0, $isForce = false)
    {
        if ($loginPass !== $this->_accessPass) {
            return null;
        }
//        echo $mbr_id;
//        die();
        //Dont update when push notification
        $isTime = $this->checkIfBlockSql();
        if ($isTime && !$isForce) {
            return null;
        }

        if ($mbr_id) {
            $session = $this->myMembershipGet($mbr_id, $this->X_authorization);
            $item = array(
                "mbr_id" => $mbr_id,
                "meta" => json_encode($session)
            );

            $query = <<<str
        INSERT INTO "Members" (mbr_id, meta, modified_on)
        VALUES (?, ? , NOW())
        ON CONFLICT (mbr_id) DO UPDATE
          SET meta = ?, modified_on= NOW()
str;
            $insert_res = $this->chapps->statement($query, [$item["mbr_id"], $item["meta"], $item["meta"]]);
//            $insert_res = false;

            if (!$insert_res) {
                return null;
            }
            if ($returnJson) {
                return response()->json($item["mbr_id"] . " insert: " . $insert_res);

            }
            return response()->json($item["mbr_id"] . " insert: " . $insert_res);
        } else {
            if ($returnJson) {
                return null;

            }
            return null;
        }
    }

    //TODO:: Change to vc.api live. (Currently No More.)
    public function HpPrinterVoucher(Request $request)
    {
        $serial_no = $request->serial_no;
        $model_id = $request->model_id;

        //Save Log
        $this->_logprocess('HP_VOUCHER_API_LOG', '1', $serial_no, $model_id, 'VcApp');

        $url = $this->imsUrl . "vcapp/getlotidinvoice";

        $post_data = array(
            "lot_id" => $serial_no,
            "model_id" => $model_id
        );
//        echo $url;
//        print_r($post_data);

        $invoice_item = $this->curlImsApi($url, $post_data);
//        print_r($invoice_item);
//        die();

        if (isset($invoice_item) && count($invoice_item) === 0) {
            return $this->apiSuccessResponse(0, "Invoice is not found.", []);
        }

        $coy_id = $this->CTL_COY_ID;
        $invoice_id = trim($invoice_item[0]["trans_id"]);
        $item_line_num = $invoice_item[0]["line_num"];


        $sql_search = <<<str
select trans.item_id, RTRIM(trans.mbr_id) as mbr_id,
item.inv_dim3,
              case when item.inv_dim3 like '%CLR%' THEN  1
              when item.inv_dim3 like '%LSR%' THEN  1
              when item.inv_dim3 like '%INKJET%' THEN 2
              END as printer_type,
RTRIM(trans.trans_id) as trans_id
from crm_member_transaction as trans
inner join ims_item_list as item on item.item_id = trans.item_id and item.is_active='Y'
 where trans.coy_id='$coy_id' and trans.trans_id=? and trans.line_num= ?
           and item.supp_item_id=?  or item.model_id= ?
 UNION ALL
 select printer.item_id, RTRIM(printer.mbr_id) as mbr_id,
item.inv_dim3,
              case when item.inv_dim3 like '%CLR%' THEN  1
              when item.inv_dim3 like '%LSR%' THEN  1
              when item.inv_dim3 like '%INKJET%' THEN 2
              END as printer_type,
'-' as trans_id
from crm_hp_printer_list as printer
inner join ims_item_list as item on item.item_id = printer.item_id and item.is_active='Y'
 where (item.supp_item_id=?  or item.model_id= ?) and printer.model_id=? and printer.lot_id=?
str;

        $invoice_res = $this->db20->select($sql_search, [
            $invoice_id, $item_line_num, $model_id, $model_id,
            $model_id, $model_id, $model_id, $serial_no]);

//        print_r($invoice_res);
//        die();
        if (count($invoice_res) === 0) {
            return $this->apiSuccessResponse(0, "Invoice is not found.", []);
        }

        $mbr_id = $invoice_res[0]->mbr_id;
        $invoice_id = $invoice_res[0]->trans_id;
        $printer_type = $invoice_res[0]->printer_type;

//		print_r($invoice_id);

        if ($invoice_id) {

            $resp = $this->spGetHpAutoInkCoupon($mbr_id, $invoice_id, $serial_no, $printer_type);
            if (isset($resp["code"]) && $resp["code"] === 0) {
                $this->_logprocess('HP_VOUCHER_API_ERROR', $printer_type, $serial_no . $resp["message"], $model_id, $mbr_id);
                return $this->apiSuccessResponse(0, "Voucher is not found.", []);
            }


            //New Voucher Add Log
            $sql = <<<str
DO $$
    begin
        IF NOT EXISTS(SELECT 1 FROM o2o_log_live_processes_new WHERE process_id = 'HP_SUBSCRIBED_VOUCHER' 
                                                                 AND mbr_id = '$mbr_id' AND invoice_id = '$invoice_id' AND trans_id = '$serial_no')
         then
          insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status,
                                                  remarks, mbr_id, created_by, created_on) VALUES
          ('CTL','HP_SUBSCRIBED_VOUCHER', '$serial_no' ,'$invoice_id' , 'OK', '$model_id', '$mbr_id', '$mbr_id', current_timestamp);
        end if;
    end;
$$;
   
str;
            $this->db20->statement($sql, []);

            $this->clearRedisFields($mbr_id, '#myVouchers#', $this->AUTOLOGIN_TOKEN, false);

            return $this->apiSuccessResponse(1, "Voucher Added.", [
                "serial_no" => $serial_no,
                "model_id" => $model_id,
                "printer_type" => $printer_type
            ]);
        } else {
            return $this->apiSuccessResponse(0, "Invoice is not found.", []);
        }
    }

    public function clearHashRedis($mbr_id)
    {
        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        echo $hkey;

        $result = Redis::del($hkey);

        return $this->apiSuccessResponse(1, "Clear.", $result);
    }

    public function batchDeleteAllMemberCache($from_date = '')
    {
        if (!$from_date) {
            $from_date = date("Y-m-d", strtotime("-1 days"));
        }

        print_r($from_date . "<br/>");

        $sql = <<<str
  select distinct mbr_id from (
                     select RTRIM(mbr_id) as mbr_id
                     from crm_member_transaction
                     where cast(trans_date as date) = '$from_date' 
                   ) as a where mbr_id !=''
str;
        $mbr_lists = $this->db20->select($sql, []);
        $mbr_str = "";

        $count = 0;
        $total_count = count($mbr_lists);
        foreach ($mbr_lists as $index => $mbr_item) {
            $mbr_list = $this->vcmemberRepository->stdToArray($mbr_item);

            $mbr_id = $mbr_list["mbr_id"];
            $mbr_str .= "'" . $mbr_id . "'";

            if ($index != count($mbr_lists) - 1) {
                $mbr_str .= ',';
            }

            $thekey = $this->server_name . $this->_redis_prefix . $mbr_id;
            $res = Redis::del($thekey);
            $count += $res;
        }

        $query = <<<str
    DELETE FROM "Members" where mbr_id in ($mbr_str)
str;
//        echo $query;

        $result = $this->chapps->statement($query);
        print_r($result);

        $this->_logprocess('BATCH_CLEAR_MEMBER_CACHE', count($mbr_lists), $from_date, '', 'VcApp2');

        echo json_encode("<br/>Cleared " . $total_count . " Vc Aurora.<br/>" . "End. " . $count . " Vc Redis deleted.");
    }

    private function spGetHpAutoInkCoupon($mbr_id, $invoice_id, $serial_no, $printer_type)
    {
        $todayDate = date("Y-m-d");
        if ($todayDate > '2021-05-01') {
            return array(
                "message" => "After May 1 2021",
                "code" => 0
            );
        }

        if ($printer_type != 1 && $printer_type != 2) {
            return array(
                "message" => "Printer Type Not 1/2",
                "code" => 0
            );
        }

        //Add here if new voucher.
        $voucher_lists = ['!WP-HP3'];


        $sql_log = <<<str
SELECT 1   FROM o2o_log_live_processes_new
                  WHERE process_id = 'HP_SUBSCRIBED_VOUCHER'
                    AND mbr_id = ?
                    AND invoice_id = ?
                    AND trans_id = ?
str;

        $log_check = $this->db20->select($sql_log, [$mbr_id, $invoice_id, $serial_no]);
        if (count($log_check) > 0) {
            return array(
                "message" => "Voucher already added.",
                "code" => 0
            );
        }

        foreach ($voucher_lists as $voucher_id) {

            $isFound = false;
            while (!$isFound) {
                $code_id = 'HP3-' . $this->generateRandomString(5);
                $sql_voucher_check = <<<str
  SELECT 1
                    FROM crm_voucher_list
                    WHERE coupon_serialno = ?
str;
                $voucher_validate = $this->db20->select($sql_voucher_check, [$code_id]);
                if (count($voucher_validate) === 0) {
                    $isFound = true;
                }
            }

            //Add here if new voucher.
            if ($voucher_id == '!WP-HP3') {
                $exp_date = Carbon::now()->addMonth(6);
            }

            $coy_id = $this->CTL_COY_ID;
            $guid = $this->vcmemberRepository->GUID();

            //Insert Voucher.
            $sql_ins = <<<str
 INSERT INTO crm_voucher_list
                    (coy_id, coupon_id, coupon_serialno, promocart_id, mbr_id, status_level, issue_date, sale_date,
                     utilize_date, expiry_date, voucher_amount, receipt_id1, created_by, created_on, modified_by,
                     modified_on, guid)
                    VALUES (?, ? , ?, ?, ?, 0, NOW(), NOW(), null, ?, 0,
                            ?, 'HpInkV3', NOW(), 'HpInkV2', NOW(), ?)
str;
            $this->db20->statement($sql_ins,
                [$coy_id, $voucher_id, $code_id, $voucher_id, $mbr_id, $exp_date,
                    $invoice_id, $guid]);

        }

        return array(
            "code" => 1,
            "message" => "OK"
        );

    }

    private function generateRandomString($length = 5)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function clearAllVcRedis()
    {
        $count = 0;
        $hkey = $this->_redis_prefix;
        $ks = Redis::keys("*" . $hkey . "*");
        if (count($ks) > 0) {
            foreach ($ks as $k) {
                $res = Redis::del($k);
                $count += $res;
            }
        }
        return "End" . $count;
    }


    public function clearRedisFields($mbr_id, $keyword, $auth, $isUrl = true)
    {
        if ($auth !== $this->X_authorization) {
            if ($isUrl) {
                echo "Access Denied";
            }
            return;
        }
        $keyword = urldecode($keyword);
        $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
        $lists = Redis::hKeys($hkey);
        $delList = array();
        foreach ($lists as $list) {
            if (strpos($list, $keyword) !== false) {
                array_push($delList, $list);
            }
        }
        $result = array();
        if ($delList) {
            $result = Redis::hDel($hkey, $delList);
        }
        if ($keyword == "#MBRINFO#" && $this->chappsDb) {
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
        }

        if ($isUrl) {
            print_r(count($delList));
            return;
        }
        return $result;
    }

    //OTHERS.
    public function insertEmarsysPush(Request $request)
    {
        $post = array();
        $post["title"] = $request->title;
        $post["ttos"] = $request->ttos;
        $post["url"] = $request->url;
        $post["ttos"] = $request->ttos;
        $post["body"] = isset($request->body) ? $request->body : '';
        $post["feature"] = isset($request->feature) ? $request->feature : '';
        $post["link_url"] = isset($request->link_url) ? $request->link_url : '';
        $post["mbr_id"] = isset($request->mbr_id) ? $request->mbr_id : '';

        $title = $post['title'];
        $message = $post['body'];
        $url = $post['url'];
        $ttos = $post['ttos'];
        $date = new Carbon();
        $type = substr($post['title'] . $date->format('ymdHis'), 0, 25);

        if ($post['mbr_id']) {
            $mbr_id = $post['mbr_id']; //'V183535200;'; // mike
            $pushtype = 'private';
        } else {
            $mbr_id = "";
            $pushtype = 'public';
        }
        $feature = $post["feature"] ? $post["feature"] : 'notification';
        $link_url = $post["link_url"] ? $post["link_url"] : '';
        $text_title = $title;

        $sql_no = <<<str
select MAX(id) as id from o2o_push_notify;
str;
        $numlist = $this->db20->select($sql_no);
        $newid = (count($numlist) > 0 && isset($numlist[0]->id)) ? (int)$numlist[0]->id + 1 : 1;

        $sql = <<<str
INSERT INTO o2o_push_notify 
(id, datetime,type,title,message,feature,url,require_login,pushtype,mbr_id, link_url, status,created_by,created_on) 
VALUES 
(?, ?,?,?,?,?,?,'Y',?,?,?,1,'VCAPP2', current_timestamp )
str;

        $this->db20->statement($sql, [$newid, $ttos, $type, $title, $message, $feature, $url, $pushtype, $mbr_id, $link_url]);

        $id = $newid;
        if ($pushtype == 'public') {
            $this->insertPushRedis($id);
            $this->insertPublicPushList($id, $ttos);

            $this->PushOneSignalAll('PUSH_ALL', $id, $feature, $ttos,
                $title, $message, $text_title, 'Learn More', 'Ok', $url, $link_url);

        } else {
            $keyType = '#notificationInbox#';
            $this->vcmemberRepository->deleteTableMeta($mbr_id, $keyType);

            $this->PushOneSignalCommon($mbr_id, 'PUSH_PRIVATE', $id, $feature, $ttos,
                $title, $message, $text_title, 'Learn More', 'Ok', $mbr_id, $url, $link_url);

        }
        return $this->apiSuccessResponse(1, "success", strval($id));
    }

    private function PushOneSignalAll($process_id, $trans_id, $feature, $thetime, $title, $message, $text_title = '', $button = '', $text_title_no = 'Ok', $url = '', $link_url = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and trans_id= ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $trans_id]);
        if (count($push_log) === 0) {
            $datamsg = array(
                "included_segments" => "All",
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "data" => array(
                    "title" => $title,
                    "message" => $message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "feature" => $feature,
                    "url" => $url,
                    "pushdata" => 1,
                    "pushlist_id" => $trans_id,
                    "description" => $message,
                    "datetime" => $thetime
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1,
                "send_after" => $thetime . " UTC+08:00",
            );

            if ($link_url == 'system') {
                $datamsg["data"]["key_windowsystem"] = true;
            }

//            print_r($datamsg);
//        	die();

            $response = $this->_curlOneSignalPushRedis($datamsg, 'PUBLIC', $process_id);
            $this->_logOneSignalNew(json_encode($datamsg), 'PUBLIC', $process_id, 'VcApp', $trans_id, '', '');
            return true;
        }
    }


    private function PushOneSignalCommon($mbr_id, $process_id, $trans_id, $feature, $thetime, $title, $message, $text_title = '', $button = '', $text_title_no = 'Ok', $email_addr = '', $url = '', $link_url = '')
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes_new where process_id= ? and mbr_id = ? and trans_id= ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id, $trans_id]);
        if (count($push_log) === 0) {
            $datamsg = array(
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "tags" => array(
                    array("relation" => "=", "key" => "mbr_id", "value" => $mbr_id)
                ),
                "data" => array(
                    "title" => $title,
                    "message" => $message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "feature" => $feature,
                    "url" => $url,
                    "pushdata" => 1,
                    "pushlist_id" => $trans_id,
                    "description" => $message,
                    "datetime" => $thetime
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1,
                "send_after" => $thetime . " UTC+08:00",
            );

            if ($link_url == 'system') {
                $datamsg["data"]["key_windowsystem"] = true;
            }

//            print_r($datamsg);
//        	die();

            $response = $this->_curlOneSignalPushRedis($datamsg, $mbr_id, $process_id);
            $this->_logOneSignalNew(json_encode($datamsg), $mbr_id, $process_id, 'VcApp', $trans_id, '', $email_addr);
            return true;
        }
    }


    private function insertPushRedis($id)
    {
        $array = array();
        $sql = <<<str

select  notify.id,notify.datetime,
        cast (notify.datetime as date) as thedate,
cast(notify.datetime as timestamp) as thetime,
        notify.type,notify.title,notify.message,notify.require_login,
notify.feature,notify.url, notify.pushtype,notify.updated_on as push_date,'' as mbr_id,
'' as platform,0 as status ,
notify.long_message, notify.image_url, CASE WHEN coalesce(notify.link_text,'')='' THEN 'Ok' ELSE notify.link_text END as link_text,
        coalesce(notify.link_url,'') as link_url
from o2o_push_notify notify
where notify.id= ? limit 1
str;
        $list = $this->db20->select($sql, [$id]);

        if (count($list) > 0) {
            $pushitem = $list[0];
            $item = $this->vcmemberRepository->stdToArray($pushitem);

            $redisKey = $this->server_name . "#Vc2_Notification_redis#";

            $array[$id] = json_encode($item);
            Redis::hMset($redisKey, $array);
            Redis::expire($redisKey, $this->fiveDays);
        }
    }

    //Test redis & Push
    private function insertPublicPushList($id, $ttos)
    {
        //Insert Redis.
        $redisKey = $this->ENVIRONMENT . "#Vc_pushNotify_time#";
        $lists = Redis::hgetall($redisKey);

        $timeNow = date("Y-m-d H:i:s");
        $lists[] = date("Y-m-d H:i:s", strtotime($ttos));

        $expiry_sec = strtotime('+2 hours', strtotime($lists[count($lists) - 1])) - strtotime($timeNow);
//		echo $diff;
//		$expiry_sec= 86400;

        Redis::hMset($redisKey, $lists);
        Redis::expire($redisKey, $expiry_sec);

        return;
    }

    //TODO:: test Live.
    public function autologin_hachi(Request $request)
    {
        $post = array();
        $post['access_token'] = $request->access_token;
        $email_addr = $request->email;
        $mbr_id = $request->mbr_id;

        if ($post['access_token'] !== $this->X_authorization || (!$mbr_id && !$email_addr)) {
            return $this->apiSuccessResponse(0, "Invalid Token", []);
        }

        $keyToken = bin2hex(openssl_random_pseudo_bytes(20));
        Redis::setex($keyToken, 30, $email_addr);  //20 seconds

        $password = "*"; // not applicable since it is token based
        $result = $this->autologin($email_addr, $password, $mbr_id, $this->AUTOLOGIN_TOKEN);
        $result["sectoken"] = $keyToken;

        return $this->apiSuccessResponse(1, "success", $result);
    }

    private function autologin($email, $password, $mbr_id, $token)
    {
        if ($token !== $this->X_authorization) {
            return array(
                "result" => false
            );
        }

        event(new UserLogin($mbr_id, $email));

        return array(
            "result" => true
        );
    }

    public function secureurl($token, $url)
    {
        if ($email = Redis::get($token)) {
            $resp = $this->userlogin($email, "*", $this->AUTOLOGIN_TOKEN);
            $url = base64_decode($url);
            header('Location: ' . $url, true, "303");
            die();
        } else {
            $this->apiSuccessResponse(0, "Access Denied, invalid token", []);
        }
    }

    public function setCtlLoginToken(Request $request)
    {
        $email = $request->email;
        if (!$email) {
            return $this->apiSuccessResponse(0, "Invalid Token", '');
        }

        $keyToken = bin2hex(openssl_random_pseudo_bytes(20));
        Redis::set($keyToken, $email);  //20 seconds
        Redis::expire($keyToken, 172800);

//        CI1::session()->set_userdata("nozendesk", 1);
        return $this->apiSuccessResponse(1, "success", $keyToken);
    }

    public function setCtlLoginTokenGet($email)
    {
        if (!$email) {
            return $this->apiSuccessResponse(0, "Invalid Token", '');
        }

        $keyToken = bin2hex(openssl_random_pseudo_bytes(20));
        Redis::set($keyToken, $email);  //20 seconds
        Redis::expire($keyToken, 172800);

//        CI1::session()->set_userdata("nozendesk", 1);
        return $this->apiSuccessResponse(1, "success", $keyToken);
    }

    public function getCtlLoginToken(Request $request)
    {
        $token = $request->token;
        if (!$token) {
            return $this->apiSuccessResponse(0, "Invalid Token", '');
        }

        $tokenKey = Redis::get($token);
        if (!$tokenKey) {
            return $this->apiSuccessResponse(0, "Token Expired.", []);
        }
        return $this->apiSuccessResponse(1, "success", $tokenKey);
    }

    public function getCtlLoginTokenGet($token)
    {
        if (!$token) {
            return $this->apiSuccessResponse(0, "Invalid Token", '');
        }

        $tokenKey = Redis::get($token);
        if (!$tokenKey) {
            return $this->apiSuccessResponse(0, "Token Expired.", []);
        }
        return $this->apiSuccessResponse(1, "success", $tokenKey);
    }

    //BATCHJOB
    public function batchSendStudentMbrship(Request $request)
    {
        ini_set('display_errors', 1);
        ini_set("memory_limit", -1);

        $todayDate = date("Y-m-d");
        $twoMonthDate = date('Y-m-d', strtotime("-120 days"));; // St

        $keyLock = $this->server_name . "#batchSendStudentMbrshipV3_LOCK";
        $this->lockProcess($keyLock, 900);

        $sql_temp = <<<str
 select *  from (
                      SELECT a.cust_id, SUM(a.total) as total
                      FROM (
                                SELECT trans.mbr_id      AS cust_id
                                    , sum(trans.item_qty * trans.unit_price) AS total
                               FROM crm_member_transaction trans
                                        INNER JOIN crm_member_list member on trans.mbr_id = member.mbr_id and trans.coy_id= member.coy_id
                               WHERE trans.coy_id = 'CTL'
                                 AND member.mbr_type = 'MST'
                                 and cast(member.exp_date AS DATE) > cast(current_timestamp AS DATE)
                               GROUP BY trans.mbr_id
                           ) a
                      WHERE NOT EXISTS(
                              SELECT 1
                              FROM o2o_log_live_processes c
                              WHERE mbr_id = a.cust_id
                                AND process_id = 'STUEXTEND'
                          )
                      GROUP BY a.cust_id
                  ) as b where b.total >= 100 limit 100;
str;
        $students = $this->db20->select($sql_temp, []);
//       print_r($students);
//       die();

        foreach ($students as $student) {
            $stu_id = $student->cust_id;

            $sql_off = <<<str
update crm_member_type set eff_to = current_timestamp , modified_on= current_timestamp where mbr_type='MST' and mbr_id in ('$stu_id');
str;
            $this->db20->statement($sql_off, []);

            $sql_ins_type = <<<str
insert into crm_member_type (coy_id, mbr_id, line_num, eff_from ,eff_to, mbr_type, created_on, created_by, modified_on, modified_by) VALUES
('CTL' , '$stu_id',
coalesce((select MAX(line_num)+1 from crm_member_type where mbr_id= '$stu_id'),1),
current_timestamp,
coalesce((select coalesce(cast( CONCAT(cast(eff_from + INTERVAL '28 months' as date) , ' 23:59:59') as timestamp), cast(current_timestamp + INTERVAL '16 months' as timestamp))
from crm_member_type where mbr_id= '$stu_id' and mbr_type='MST' order by line_num desc limit 1), cast(current_timestamp + INTERVAL '28 months' as timestamp)),
'MSTP', current_timestamp,
'$stu_id',
      current_timestamp, 'VcApp_V3');
str;
            $this->db20->statement($sql_ins_type, []);

            $sql_upd_member = <<<str
UPDATE crm_member_list
SET exp_date = coalesce((select cast( CONCAT(cast(eff_from + INTERVAL '28 months' as date) , ' 23:59:59') as timestamp)
from crm_member_type where mbr_id= crm_member_list.mbr_id  and mbr_type='MST' order by line_num desc limit 1),
    exp_date + INTERVAL '28 months' )  , modified_on=current_timestamp,
    mbr_type= 'MSTP'
WHERE coy_id='CTL' and mbr_id= '$stu_id'
str;
            $this->db20->statement($sql_upd_member, []);

            $sql_log = <<<str
INSERT INTO  o2o_log_live_processes (coy_id,process_id,status,remarks,mbr_id,created_on,created_by,modified_by, modified_on) VALUES
('CTL','STUEXTEND', 'OK', 'Student Expiry Extend','$stu_id',current_timestamp,'VcApp_V3','VcApp_new'
     ,current_timestamp);
str;
            $this->db20->statement($sql_log, []);

        }

        $sql = <<<str
SELECT DISTINCT b.email_addr,b.mbr_id, b.first_name FROM  crm_member_list b
where mbr_id in (select mbr_id from o2o_log_live_processes where process_id='STUEXTEND' and cast(created_on as date)='$todayDate')
and mbr_id not in (select mbr_id from o2o_log_live_processes where process_id='STUWELCOME' and cast(created_on as date)='$todayDate')
str;
        $lists = $this->db20->select($sql, []);

//        print_r($lists);
//        die();

        for ($i = 0; $i < count($lists); $i++) {
            $mbr_id = trim($lists[$i]->mbr_id);
            $email_addr = $lists[$i]->email_addr;
            $first_name = $lists[$i]->first_name ? $lists[$i]->first_name : '';

            echo $email_addr . $mbr_id . "<br/>";

            $dataToken = array(
                "m" => $mbr_id,
                "e" => $email_addr
            );
            $token = $this->createToken($dataToken, 604800, 'vctoken');

            $params = array(
                "t" => $token,
                "m" => $mbr_id,
                "e" => $email_addr
            );
            $edata = $this->base64url_encode(json_encode($params));

            $send_email = trim($email_addr);
            $email_data = [
                'r_C_First_Name' => $first_name,
            ];
            //Will Send New Template.
            if ($this->ENVIRONMENT == 'production') {
                $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["STU_UPT_MBRSHIP_PLUS"], $send_email, $email_data, '');
            } else {
                $email_result = 'DEVELOPMENT';
            }

            $this->_logprocess("STUWELCOME", "OK", json_encode($email_result), $mbr_id, 'VcAppV3');

            //-- Clear Cache
            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id;
            Redis::del($hkey);

            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

        }

        echo "done, processed: " . count($lists);
        $this->unlockProcessing($keyLock);

    }

    public function batchEmailStudent()
    {
        $keyLock = $this->server_name . "#batchEmailStudent_LOCK";
        $this->lockProcess($keyLock, 900); // Run every N seconds, this only applies in production mode

        $sql = <<<str

select email_addr,
       to_char(exp_date, 'DD Mon YYYY') as exp_date,
       first_name,
       last_name,
       a.mbr_id,
       a.purchased
from (
         select c.mbr_id, (100 - SUM(coalesce(a.item_qty, 0) * coalesce(a.unit_price, 0))) as purchased
         from crm_member_list c
                  LEFT JOIN crm_member_transaction as a on a.coy_id = 'CTL' and a.mbr_id = c.mbr_id
         WHERE c.mbr_type = 'MST'
           and NOT EXISTS
             (SELECT * FROM o2o_log_live_processes cc WHERE c.mbr_id = cc.mbr_id and process_id = 'STUEMAILUPDATE')
           and cast(NOW() + Interval '30 days' as date) >= cast(c.exp_date as date)
           and cast(NOW() as date) >= cast(c.exp_date as date)
           and c.status_level = 1
         GROUP BY c.mbr_id
         HAVING SUM(coalesce(a.item_qty, 0) * coalesce(a.unit_price, 0)) < 100
     ) as a
         INNER JOIN crm_member_list as list on list.mbr_id = a.mbr_id;

str;

        $results = $this->db20->select($sql, []);
//        print_r($results);
//        die();

        //-- Send Email to the result from query above
        foreach ($results as $index => $result) {

            $student_data = $this->vcmemberRepository->stdToArray($result);

            $mbr_id = trim($student_data["mbr_id"]);
            $email = trim($student_data["email_addr"]);

            //For Test
//            $email = 'wong.weiseng@challenger.sg';
//            $mbr_id = 'V160000500';

            $email_data = [
                'r_c_date_to_convert' => $student_data["exp_date"],
                'r_C_First_Name' => trim($student_data["first_name"]),
                'r_C_point_toearn' => number_format($student_data["purchased"], 2)
            ];
            $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["STU_UNLOCK_REMINDER"], $email, $email_data);
            $this->_logprocess("STUEMAILUPDATE", "OK", json_encode($email_result), $mbr_id);

            //-- Clear Cache
            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id;
            Redis::del($hkey);

        }

        $this->unlockProcessing($keyLock);

        return $this->apiSuccessResponse(1, "Success", "done, processed: " . count($results));
    }

    public function batchUpdateNotificationAurora()
    {
        if ($this->checkIfBlockSql()) {
            return $this->apiSuccessResponse(0, 'Push Now', []);
        }

        $keyLock = $this->server_name . "#batchUpdateNotificationAurora_LOCK";
        $this->lockProcess($keyLock);

        $sql = <<<str
SELECT a.id,a.batch_id, a.mbr_id FROM o2o_push_log a
WHERE exists(
    SELECT * FROM o2o_push_notify b
    WHERE cast(NOW() as date) BETWEEN cast(b.datetime as date)
        and cast(b.datetime + Interval '2 days' as date) and a.batch_id = b.id)
        and coalesce(a.mbr_id,'') != '' and a.status = 1
limit 50;
str;
        $results = $this->db20->select($sql, []);
        foreach ($results as $index => $result) {
            $list = $this->vcmemberRepository->stdToArray($result);

            $mbr_id = $list['mbr_id'];
            $Id = $list['id'];
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
            $this->db20->statement("UPDATE o2o_push_log SET status = 2 WHERE id = '$Id'");
            echo $mbr_id . "<br>";

        }
        $this->unlockProcessing($keyLock);

        $this->_logprocess("VC_PUSHLOG_UPD", "OK", count($results), 'VC');

        return $this->apiSuccessResponse(1, 'OK', count($results));
    }

    public function batchCeaseAssoMember()
    {
        ini_set('display_errors', 1);
        ini_set("memory_limit", -1);

        $amount = 150;

        $coy_id = $this->CTL_COY_ID;
        $sql_select = <<<str
    select mbr_id from crm_member_list where coy_id='$coy_id' and mbr_type='MAS'
 and cast(exp_date as timestamp) < cast(NOW() as timestamp) and main_id !='' limit $amount
str;
        $lists = $this->db20->select($sql_select);
//        print_r($lists);
//        die();

        foreach ($lists as $index => $list_std) {
            $list = $this->vcmemberRepository->stdToArray($list_std);

            $mbr_id = $list["mbr_id"];
            $user_sql = <<<str
    select email_addr,contact_num,mbr_id,main_id,first_name,
      to_char(exp_date ,'DD/MM/YYYY') as exp_date2 from crm_member_list where coy_id= '$coy_id' and mbr_id= ?
str;
            $users = $this->db20->select($user_sql, [$mbr_id]);

            $user = isset($users[0]) ? $users[0] : array();
//            print_r($user);
//            die();
            if ($user) {
                $user = $this->vcmemberRepository->stdToArray($user);

                $email_addr = trim($user["email_addr"]);
                $the_mbr_id = trim($user["mbr_id"]);
                $main_id = trim($user["main_id"]);
                if ($email_addr) {
                    $first_name = trim($user["first_name"]);

//                    if ($this->ENVIRONMENT == "development") {
//                        $email_addr = 'wong.weiseng@challenger.sg';
//                    }

                    $sender_email = $email_addr;
                    $email_data = array(
                        "r_c_first_name" => ucwords(strtolower($first_name)),
                        "r_date_of_terminated" => $user["exp_date2"]
                    );
//                    print_r($email_data);
//                    die();

                    if ($this->ENVIRONMENT == 'production') {
                        $email_template = $this->emarsys_template_list["EXPIRED_ASSOCIATE"];
                        $email_result = $this->_sendEmarsysEmail($email_template, $sender_email, $email_data);
                    } else {
                        $email_result = 'DEVELOPMENT';
                    }

                    $remarks = array(
                        "email_addr" => $email_addr,
                        "result" => $email_result
                    );
                    $this->db20->statement("INSERT INTO  o2o_log_live_processes (coy_id,process_id,status,remarks,mbr_id,created_on,created_by) VALUES(?,?,?,?,?,NOW(),?)",
                        ['CTL', 'EXPIRED_ASSOMBR', 'OK', json_encode($remarks), $the_mbr_id, $main_id]);

                    $arrWhere = [
                        'coy_id' => $coy_id,
                        'mbr_id' => $the_mbr_id,
                    ];

                    $updData = [
                        'email_addr' => 'CEASED',
                        "contact_num" => "CEASED",
                        "mbr_pwd" => "",
                        'main_id' => '',
                        'mbr_type' => '',
                        'status_level' => -1,
                        'modified_on' => date('Y-m-d H:i:s'),
                        'modified_by' => $main_id,
                    ];

                    $this->db20->table('crm_member_list')
                        ->where($arrWhere)
                        ->update($updData);

                    $hkey = $this->server_name . $this->_redis_prefix . $the_mbr_id; //. $key;
                    Redis::del($hkey);

                    $del_query = <<<str
          DELETE FROM "Members" where mbr_id='$the_mbr_id' 
str;
                    $this->chapps->statement($del_query);

                    //Update Main Member.
                    $this->updateOrInsertMbr($main_id, $this->_accessPass);

                    print_r($mbr_id . " associate member purged.<br/>");
                }
            }
        }
        echo "Finish. " . count($lists) . " associate members removed.<br/>";
    }

    //Purge Normal Member.
    public function batchCeaseExpMember($limit = 1000)
    {
        ini_set('display_errors', 1);
        ini_set("memory_limit", -1);

        $amount = $limit;
        $sixMonth = date('Y-m-d', strtotime('-6 months'));
        $todayDate = date("Y-m-d");

        $coy_id = $this->CTL_COY_ID;
        $sql_select = <<<str
    select RTRIM(mbr_id) as mbr_id, RTRIM(first_name) as first_name, RTRIM(last_name) as last_name, 
           RTRIM(contact_num) as contact_num, 
           RTRIM(email_addr) as email_addr
    from crm_member_list where coy_id='$coy_id'
                               and email_addr != ''
                               and email_addr != 'CEASED'
                               and email_addr != 'NEW'
                           and mbr_type in ('MST','MSTP','M','M08','M18','M28')
 and cast(exp_date as timestamp) < '$sixMonth' limit $amount
str;
//        echo $sql_select;

        $lists = $this->db20->select($sql_select);
        foreach ($lists as $index => $list_std) {
            $list = $this->vcmemberRepository->stdToArray($list_std);

            $mbr_id = $list["mbr_id"];
            $email_addr = $list["email_addr"];
            $contact_num = $list["contact_num"];
            $last_name = $list["last_name"];
            $first_name = $list["first_name"];

//            echo $mbr_id. "<br/>";

            $this->_logNewprocess('EXPIRED_MBR', $todayDate, $contact_num, '1', $first_name . ' | ' . $last_name,
                $mbr_id, $email_addr, 'VcApp2');

            $arrWhere = [
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id
            ];

            $updData = [
                'created_by' => 'VcApp2',
                'modified_by' => substr($contact_num, 0, 10),
                'last_name' => '',
                'email_addr' => 'CEASED',
                "contact_num" => 'CEASED',
                "mbr_pwd" => "",
                'main_id' => '',
                'mbr_type' => '',
                'status_level' => -1,
                'modified_on' => date('Y-m-d H:i:s'),
            ];

            $this->db20->table('crm_member_list')
                ->where($arrWhere)
                ->update($updData);


            $hkey = $this->server_name . $this->_redis_prefix . $mbr_id; //. $key;
            Redis::del($hkey);

            $del_query = <<<str
          DELETE FROM "Members" where mbr_id='$mbr_id' 
str;
            $this->chapps->statement($del_query);

            //Update Main Member.
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

            print_r($mbr_id . " member purged.<br/>");
        }

        $this->apiSuccessResponse(1, "Finish. " . count($lists) . " members purged.<br/>", []);

    }

    private function lockProcess($key, $expireSeconds = 0)
    {
        if ($this->ENVIRONMENT == "development") {
            $expireSeconds = 0;
        }
        $key_exist = Redis::get($key);
        if ($key_exist) {
            return $this->apiSuccessResponse(0, "There is a job running, please wait", null);
        }

        Redis::setex($key, 1200, "lock"); // 20 minutes

        if ($expireSeconds > 0) {
            $keyLast = $key . "_EXP";
            $expireTiming = Redis::get($keyLast);
            if (!$expireTiming) {
                $date = new Carbon();
                $expireTiming = intval($date->format('ymdHis')) + $expireSeconds;
                Redis::set($keyLast, $expireTiming);
            } else {
                $date = new Carbon();
                $expireTiming = intval($expireTiming);
                if (intval($date->format('ymdHis')) < $expireTiming) {
                    $this->unlockProcessing($key);
                    return $this->apiSuccessResponse(0, "Next run will be on " . $expireTiming, null);
                } else {
                    $expireTiming = intval($date->format('ymdHis')) + $expireSeconds;
                    Redis::set($keyLast, $expireTiming);
                }
            }
        }
    }

    private function unlockProcessing($key)
    {
        //ini_set('display_errors', 1);
        Redis::del($key);
    }

    public function batchRedisServiceCenter($hachipass = '')
    {
        ini_set("display_errors", 1);
        if ($hachipass !== 'hachi1234') {
            return;
        }

        $url = $this->imsUrl . 'vcapp/getbrandlistings';

        $post_data = array();

        $lists = $this->curlImsApi($url, $post_data);

        $redisKey = $this->ENVIRONMENT . "_serviceCenter#";

        $array = array();

        $res = $this->delServiceCenterRedis('', $this->X_authorization);

        foreach ($lists as $list) {
            $brand_name = $list["brand_name"];
            $cate_name = $list["category_name"];


            if ($cate_name != "-") {
                $array[$brand_name][$cate_name][] = $list;
            }
        }

        foreach ($array as $key => $data) {
            $array[$key] = json_encode($array[$key]);
        }

        Redis::hMset($redisKey, $array);
        echo json_encode("Redis Generated.");
    }

    public function delServiceCenterRedis($brand = '', $access_token = '')
    {
        if ($access_token !== $this->X_authorization) {
            return;
        }
        ini_set("display_errors", 1);
        $redisKey = $this->ENVIRONMENT . "_serviceCenter#";

        if (!$brand) {

            $redisKeyLists = $this->ENVIRONMENT . "_serviceCenterLists#";

            $res = Redis::del($redisKeyLists);
            $res2 = Redis::del($redisKey);

            return ("Removed: " . $res . $res2);

        } else {
            $res2 = Redis::hdel($redisKey, $brand);

            $redisKeyLists = $this->ENVIRONMENT . "_serviceCenterLists#";
            $res = Redis::del($redisKeyLists);
            return ("Removed: " . $res . $res2);

        }

    }

    public function saveMemberDevices($token=''){
        if ($token !== $this->X_authorization) {
            return $this->apiSuccessResponse(0, 'Access Denied.', []);
            die();
        }
        $lists = array();
        $amount = 0;
        $fileNo = 0;
        if (($handle = fopen('files/user_may.csv', "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($count === 0) {
                    $count++ ;
                   continue;
                }
                if ($count != 0) {


                    $platform_type = isset($data[2]) ? $data[2] : 'iOS';
                    if(trim($platform_type)=='iOS'){
                        $platform= "iOS";
                    }else{
                        $platform= "Android";
                    }

                    $uuid= isset($data[6]) ? $data[6] : '';
                    $push_id= isset($data[11]) ? $data[11] : '';
                    $date= isset($data[5]) ? $data[5]: '';
                    $last_date = isset($data[8]) ? $data[8] : '';
                    $mbr_meta= isset($data[14]) ? json_decode($data[14],true): '';

                    $mbr_id= isset($mbr_meta["mbr_id"]) ? $mbr_meta["mbr_id"] : '';
                    $app_version= isset($mbr_meta["app_version"]) ? $mbr_meta["app_version"] : '';

                    if($mbr_id && $uuid){
                        $fileNo++;

                        $meta = json_encode(array(
                            "pushId" => $push_id,
                            "platform" => $platform,
                            "appVersion" => $app_version,
                            "manufacturer" => $platform_type
                        ));

                        $sql = <<<str
DO $$
    begin
        IF not EXISTS(SELECT * FROM o2o_member_devices WHERE deviceid='$uuid')
            then
        INSERT INTO o2o_member_devices (mbr_id, platform, deviceid, meta, created_on) VALUES ('$mbr_id','$platform','$uuid','$meta', '$last_date');
        ELSE
            UPDATE o2o_member_devices SET mbr_id = '$mbr_id', meta = '$meta',modified_on='$last_date',logout=NULL WHERE deviceid='$uuid' ; 
        end if;
    end
$$;
str;
                        $this->db20->statement($sql, []);

                    }else{
//                        print_r($data);
                    }

                }
                $count++ ;
            }

            fclose($handle);
        }

        return response()->json([1, "Total: " . $count, "Member: " . $fileNo]);
    }

    public function savePostalCode($token = '', $function = 0)
    {
        ini_set("display_errors", 1);
        if ($token !== $this->X_authorization) {
            return $this->apiSuccessResponse(0, 'Access Denied.', []);
            die();
        }

        $lists = array();
        $amount = 0;
        $fileNo = 0;
        if (($handle = fopen('files/sgpostal_new.csv', "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($count === 0) {
                    $count++;
                }
                $objKey = str_pad(trim($data[0]), 6, "0", STR_PAD_LEFT);
                $preThree = substr($objKey, 0, 3);

//                echo $preThree;

                $lists[$preThree][$objKey] = json_encode($data);
                $amount++;

                if ($amount >= 50000) {
                    //Ok Enough, Lets put it into Redis.
                    foreach ($lists as $prefix => $list) {
                        $redisKey = $this->ENVIRONMENT . "_postalCode_" . $prefix;
                        Redis::hmset($redisKey, $list);
                        $fileNo++;
                    }
                    $amount = 0;
                    $lists = array();
                }
                $count++;
            }

            if ($amount >= 0) {
                //put it into Redis.
                foreach ($lists as $prefix => $list) {
                    $redisKey = $this->ENVIRONMENT . "_postalCode_" . $prefix;
                    Redis::hmset($redisKey, $list);
                    $fileNo++;
                }
            }

            fclose($handle);
        }


        //Save Postal No Go Zone
        $list2 = array();
        $count2 = 0;
        if (file_exists('files/sgpostal_verify.csv')) {
            $count2 = 0;
            if (($handle2 = fopen('files/sgpostal_verify.csv', "r")) !== FALSE) {
                while (($data2 = fgetcsv($handle2, 1000, ",")) !== FALSE) {
                    $objKey2 = str_pad(trim($data2[0]), 6, "0", STR_PAD_LEFT);
                    $list2[$objKey2] = json_encode($data2);
                }
                $redisNoGo = $this->ENVIRONMENT . "_postalNoGoZone#";
                Redis::del($redisNoGo);
                Redis::hmset($redisNoGo, $list2);
                $count2++;
                fclose($handle2);
            }
        }

        if ($function == "1") {
            return "";
        }

        return response()->json([1, "Finish" . $count, $fileNo, $count2]);
    }

    //TODO:: test.
    public function batchSendProductReview()
    {
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');

        $date = date("md");
        $today = date("Y-m-d");
        $lists = array();
        $fileName = "productreview" . date("Y-m-d") . ".csv";
        $file_name = $this->s3Config["bucketFullpath"] . $fileName;

        $count = 0;
        if (($handle = fopen($file_name, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($count === 0) {
                    $count++;
                    continue;
                }
                $invoice_id = trim($data[0]);
                $mbr_id = trim($data[2]);
                $item_id = trim($data[1]);
                $item_id = str_pad($item_id, 13, "0", STR_PAD_LEFT);

                $first_name = trim($data[3]);
                $email_addr = trim($data[4]);
                $trans_date = trim($data[5]);
                $item_desc = trim($data[6]);
                $img_name = trim($data[7]);
                $rebate_amount = $data[8];
                $store_loc = trim($data[9]);

                if (!$mbr_id) {
                    continue;
                }

                $list = array(
                    "mbr_id" => $mbr_id,
                    "invoice_id" => $invoice_id,
                    "item_id" => $item_id,
                    "first_name" => $first_name,
                    "email_addr" => $email_addr,
                    "trans_date" => $trans_date,
                    "item_desc" => $item_desc,
                    "img_name" => $img_name,
                    "rebate_amt" => $rebate_amount,
                    "store_loc" => $store_loc
                );

                $lists[] = $list;
                $count++;
            }
        }

//        print_r(count($lists));
//        print_r($lists);
//        die();

        //Arhieve CSV Data in S3
        if ($count > 0) {
            $endfileName = "productreview" . date("Y-m-d") . "_Completed" . ".csv";
            $oldfilePath = $this->s3Config["bucketSubpath"] . $endfileName;
            $newfilePath = $this->s3Config["bucketSubpath"] . $fileName;

            $awsAccessKey = $this->s3Config["awsAccessKey"];
            $awsSecretKey = $this->s3Config["awsSecretKey"];

            $s3 = new S3();
            $s3::setAuth($awsAccessKey, $awsSecretKey);
            $s3::copyObject('ctl-public-test', $newfilePath, 'ctl-public-test', $oldfilePath);
        }

        foreach ($lists as $index => $list) {
            //Insert into Product_review Aurora.
            $product = $this->vcmemberRepository->getProductReview($list["mbr_id"], $list["invoice_id"], $list["item_id"]);
            $remarks = $list["mbr_id"] . "_" . $list["email_addr"] . "_" . $list["item_id"] . "_" . $list["invoice_id"] . "<br/>";

            if (!$product) {
                $sql_insert = <<<str
    INSERT INTO "Product_review" (trans_id, item_id, mbr_id, img_name, item_desc, star_count, like_count, dislike_count, status_level, created_on, created_by)
            VALUES (?,?,?,?,?,0,0,0,0,NOW(),'Batch$date')
str;
                $insert_res = $this->chapps->statement($sql_insert, [$list["invoice_id"], $list["item_id"], $list["mbr_id"], $list["img_name"], $list["item_desc"]]);
//               print_r($insert_res);
                if (!$insert_res) {
                    $this->_logprocess('BATCH_INSERT_PRODREVIEW_FAIL', 0, json_encode($product), $list["mbr_id"]);
                    echo "Failed To Insert: " . $remarks;
                    continue;
                } else {
                    echo "OK: " . $remarks;
                }
            } else {
                echo "Already Inserted: " . $remarks;
                $this->_logprocess('BATCH_PRODRE_ALRDY', 1, $remarks, $list["mbr_id"]);
                continue;
            }

            //Send Email.
            $data = array(
                "m" => $list["mbr_id"],
                "t" => $list["trans_id"],
                "i" => $list["item_id"]
            );
            $edata = base64_encode(json_encode($data));
            $img_url = $this->IMAGE_URL . 'assets/images/product_images/';

            $send_email = $list["email_addr"];
            //TODO:: change.
            $theUrl = $this->redirectUrl;

            $send_email_param = array(
                "r_FirstName" => $list["first_name"],
                "r_item_id" => $list["item_id"],
                "r_ProductImage" => $img_url . $list["img_name"],
                "r_ProductName" => $list["item_desc"],
                "r_ProductURL" => $theUrl . "/api/vc2/ProductReviewPage/" . $edata,
                "r_rebate_amount" => number_format($list["rebate_amt"] / 100, 2),
                "r_StoreName" => $list["store_loc"],
                "r_TransactionDate" => $list["trans_date"]
            );
//            print_r($send_email_param);

//TODO:: add template
            $email_result = $this->_sendEmarsysEmail($this->emarsys_template_list["EMAIL_PROD_REVIEW"], $send_email, $send_email_param);

//            print_r($email_result);
            //For Testing Edm.

            //Log.

            $this->_logprocess('BATCH_INSERT_PRODREVIEW', 1, $list["mbr_id"] . " | " . $list["invoice_id"] . " | " . $list["item_id"] . " | " . $list["email_addr"], $list["mbr_id"]);
        }

    }

    //TODO:: update query & STd.
    public function sendProductReviewEDM()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $keyLastsendProductReviewEDM = "keyLastsendProductReviewEDM";
        $lastDate = Redis::get($keyLastsendProductReviewEDM);


        if (!$lastDate) {
            $lastDate = date('Y-m-d', strtotime("-14 days"));; // Start October 1st, 2018
        }

        //Testing
//        $lastDate = '2019-01-24'; // debugg

        $keyLock = $this->server_name . "#sendProductReviewEDM_LOCK";
//		$this->unlockProcessing($keyLock);
        $this->lockProcess($keyLock);

        $sql = <<<str
  SELECT 
RTRIM(a.trans_id) as trans_id,
RTRIM(a.mbr_id) as mbr_id,
RTRIM(a.item_id) as item_id,
member.first_name as first_name,
member.email_addr,
cast(a.trans_date as date) trans_date,
a.item_desc
,ISNULL((select TOP(1) detail_image_name from o2o_product_detail_image where item_id= a.item_id order by created_on desc),'') as img_name,
ISNULL((select TOP(1) rebate.rebate_amt from (
select TOP(1) mbr_points as rebate_amt from pos_transaction_list where trans_id= a.trans_id and mbr_id= a.mbr_id
UNION
select TOP(1) total_points_earned as rebate_amt from o2o_transaction_list where invoice_no= a.trans_id and mbr_id= a.mbr_id 
) as rebate) , 0) as rebate_amt,
ISNULL((select TOP(1) title from o2o_store_location where store_id= a.loc_id),'Hachi.tech') as store_loc
 FROM crm_member_transaction a
 inner join crm_member_list as member on member.mbr_id = a.mbr_id
WHERE a.coy_id='CTL' and  a.item_id in (
select item_id FROM (
			SELECT item_id,status_level,item_desc
			FROM ims_item_list b
			WHERE (
				b.brand_id like 'APPLE%' or
				 b.brand_id like 'LENOVO%' or
				 b.brand_id like 'ASUS%'
			)
			UNION
			SELECT item_id,status_level,item_desc
            FROM ims_item_list b
            WHERE (
                (b.brand_id like 'MICROSOFT%' or
				b.brand_id like 'LOGITECH%') and
				(b.inv_dim3 like '%MOUSE%' or
				b.inv_dim3 like '%KEYBOARD%')
			)
			UNION
			SELECT item_id,status_level,item_desc
            FROM ims_item_list b
            WHERE (
                (b.brand_id like 'DJI%' or
				b.brand_id like 'PARROT%') AND
				b.inv_dim3= 'DRONE' AND
				b.inv_dim4 not like 'ADD-ON%'
			)
			UNION
			SELECT item_id,status_level,item_desc
            FROM ims_item_list b
            WHERE (
                (b.brand_id like 'TP-LINK%' or
				b.brand_id like 'D-LINK%' or
				b.brand_id like 'LINKSYS%' or
				b.brand_id like 'NETGEAR%') AND
				b.inv_dim3 ='MESH ROUTER'
			)
			UNION
			SELECT item_id,status_level,item_desc
			FROM ims_item_list b
			WHERE (
				(b.brand_id like 'ARLO%' or
				b.brand_id like 'D-LINK%' or
				b.brand_id like 'NETGEAR%') AND
				b.inv_dim3 ='IP CAM'
			)
			UNION
			SELECT item_id,status_level,item_desc
			FROM ims_item_list b
			WHERE (
				(b.brand_id like '%VALORE%' or
				b.brand_id like 'REDMONSTER%' or
				b.brand_id like 'MAZER%' or
				b.brand_id like 'IWALK%') AND
				b.inv_dim3= 'BATTERY-POWERBANK'
			)
) as a where a.status_level != -1 and
(	a.item_desc not like 'PIT-%' and
	a.item_desc not like 'FLB-%' and
	a.item_desc not like 'FLB %' and
	a.item_desc not like 'XX%' and
	a.item_desc not like 'X-%' and
	a.item_desc not like 'Auction-%' and
	a.item_desc not like 'Auction %' and
	a.item_desc not like 'Pre-order%'
)
) and a.trans_id!=''
and cast(a.trans_date as date) = '$lastDate' and datediff(DAY,a.trans_date,getdate())>9
and trans_type not in ('HX','HR','RF','EX') and item_qty > 0
and NOT EXISTS(
    SELECT c.cust_po_code
    FROM sms_invoice_list c
    WHERE c.cust_po_code = a.trans_id and cast(a.trans_date as date) <=cast(c.invoice_date as date)  and c.inv_type  in ('HX','HR')
)
and NOT EXISTS(
    SELECT d.ref_id
    FROM pos_transaction_list d
    WHERE d.ref_id = a.trans_id and cast(a.trans_date as date) <=cast(d.trans_date as date)  and d.trans_type in ('RF','EX')
)
and NOT EXISTS(
    SELECT e.cust_po_code
    FROM sms_invoice_list e
    WHERE e.cust_po_code = a.trans_id and cast(a.trans_date as date) <=cast(e.invoice_date as date) and  e.inv_type in ('RF','EX')
)
ORDER BY a.trans_date desc
str;
        $result = $this->db20->select($sql, []);

        if (count($result) > 0) {
            $csv = "InvoiceID,ItemID,Member,FirstName,EmailAddr,TransDate,ItemDesc,ImageName,rebate_amt,storeLoc";
            for ($i = 0; $i < count($result); $i++) {
//                $item_id = "=\"" . $result[$i]["item_id"] . "\"";
                $first_name = "\"" . str_replace('"', " ", $result[$i]["first_name"]) . "\"";
                $item_desc = "\"" . str_replace('"', " ", $result[$i]["item_desc"]) . "\"";
                $store_loc = "\"" . str_replace('"', " ", $result[$i]["store_loc"]) . "\"";

//                $invoice_id=  $result[$i]['trans_id'];
//                $mbr_id= $result[$i]['mbr_id'];

                $csv = $csv . "\r\n" . $result[$i]['trans_id'] . ',' . $result[$i]["item_id"] . ',' . $result[$i]['mbr_id'] . ',' . $first_name . ',' .
                    $result[$i]['email_addr'] . ',' . $result[$i]['trans_date'] .
                    ',' . $item_desc . ',' .
                    $result[$i]['img_name'] . ',' . $result[$i]['rebate_amt'] . ',' . $store_loc;
            }
            $date = new Carbon();
            $this->uploadS3($csv, "productreview" . $date->format('Y-m-d') . ".csv", "text/csv");
            $lastDate = $result[0]['trans_date'];
            Redis::set($keyLastsendProductReviewEDM, $lastDate);
        }

        $this->unlockProcessing($keyLock);
        echo "sendProductReviewEDM-done count: " . count($result);
    }

    //Upload S3 Func?
    private function uploadS3($fileData, $fileName, $fileType)
    {
        //-- $fileData: data string
        //-- $fileName: assets/namecard/img.png -> Filename
        //-- $fileType: image/png ->File Type

//        $filePath = $this->s3Config["bucketSubpath"] . $fileName;
//        $awsAccessKey = $this->s3Config["awsAccessKey"];
//        $awsSecretKey = $this->s3Config["awsSecretKey"];
//        $s3 = new S3();
//        $s3::setAuth($awsAccessKey, $awsSecretKey);
//        $res = $s3::putObject($fileData, "ctl-public-test", $filePath, S3::ACL_PUBLIC_READ, array(), array('Content-Type' => $fileType));

//        return $res;
        return true;
    }

    private function formatItemListstoArray($item_lists)
    {
        $list_msg = '';
        foreach ($item_lists as $index => $list) {
            $list_msg .= "'" . $list["comp_id"] . "'";

            if ($index !== count($item_lists) - 1) {
                $list_msg .= ',';
            }
        }
        return $list_msg;
    }

    private function formatItemUPCtoArray($item_lists)
    {
        $list_msg = '';
        foreach ($item_lists as $index => $list) {
            $list_msg .= "'" . $list["item_id"] . "'";

            if ($index !== count($item_lists) - 1) {
                $list_msg .= ',';
            }
        }
        return $list_msg;
    }

    private function formatListstoArray($mbr_lists)
    {
        $list_msg = '';
        foreach ($mbr_lists as $index => $list) {
            $list_msg .= "'" . $list->remarks . "'";

            if ($index !== count($mbr_lists) - 1) {
                $list_msg .= ',';
            }
        }

        return $list_msg;
    }

    private function formatListstoMbrArray($mbr_lists)
    {
        $list_msg = '';
        foreach ($mbr_lists as $index => $list) {
            $list_msg .= "'" . $list->mbr_id . "'";

            if ($index !== count($mbr_lists) - 1) {
                $list_msg .= ',';
            }
        }

        return $list_msg;
    }

    //Access Token for Order Api.
    private function getInkDeviceTokenOrder()
    {
        if ($this->ENVIRONMENT == "production") {
            $url = 'https://acr.ext.hp.com/oauth/token';
            $apiKey = "MzU3ODU3NTgyMDYxNGM4ZGE2OTYxMjUzM2M1YjAzNzE6NGVjMTFhYWE1OGIxNDQyOGE3MDZkM2M1MGVjZTBlZDM=";
        } else {
            $url = 'https://acr-dev.ext.hp.com/oauth/token';
            $apiKey = "ZjExY2FiYzAxOThiMTFlOWFiMTRkNjYzYmQ4NzNkOTM6MDJiOGIxYzYxOThjMTFlOWFiMTRkNjYzYmQ4NzNkOTM=";
        }
//        echo $url;

        $headers = array(
            'Authorization: Basic ' . $apiKey,
            'Content-Type: application/x-www-form-urlencoded'
        );
        $post_fields = "grant_type=client_credentials&scope=orders";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, 1);

//        print_r($response);
        return $response;
    }

    //To Push Hp Ink LOI Reminder via ValueCLub.
    private function PushHpLoiOneSignal($mbr_id, $process_id, $title, $message, $page = '', $button = '', $text_title = '', $text_title_no = '', $short = false)
    {
        $sql_check = <<<str
	select 1 from o2o_log_live_processes where process_id= ? and mbr_id = ? and status= 'OK'
str;
        $push_log = $this->db20->select($sql_check, [$process_id, $mbr_id]);
        if (count($push_log) === 0) {

            //Insert the Private Push Notify.
            $date = new Carbon();
            $post = array(
                "title" => $title,
                "body" => $message,
                "url" => "",
                "ttos" => date("Y-m-d H:i:s A"),
                "type" => $title . $date->format('ymdHis'),
                "mbr_id" => $mbr_id,
                "feature" => "page",
                "link_url" => $page
            );
            $the_id = $this->insertPrivatePush($post);

            //Trigger the push to User.
            $datamsg = array(
                "app_id" => $this->_oneSignal_AppID,
                "contents" => array("en" => $message),
                "headings" => array("en" => $text_title),
                "tags" => array(
                    array("relation" => "=", "key" => "mbr_id", "value" => $mbr_id)
                ),
                "data" => array(
                    "title" => $title,
                    "message" => $message,
                    "actionbutton" => $button,
                    "closebutton" => $text_title_no,
                    "feature" => "page",
                    "page" => $page,
                    "pushdata" => 1,
                    "pushlist_id" => $the_id,
                    "description" => $message,
                    "short" => $short
                ),
                "android_group" => "vc-app",
                "content_available" => true,
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            );

//        	print_r($datamsg);
            $response = $this->_curlOneSignalPushRedis($datamsg, $mbr_id, $process_id);
            print_r($response);
            $this->_logOneSignal(json_encode($response), $mbr_id, $process_id, 'VcApp');
            $this->updateOrInsertMbr($mbr_id, $this->_accessPass);

            $keyType = '#notificationInbox#';
            $this->vcmemberRepository->deleteTableMeta($mbr_id, $keyType);
        } else {
            echo "Already Sent: " . $process_id . "<br/>";
        }
    }

    private function insertPrivatePush($post)
    {
        $title = $post['title'];
        $message = $post['body'];
        $url = $post['url'];
        $ttos = $post['ttos'];
        $date = new Carbon();
        $type = $post['title'] . $date->format('ymdHis');

        if ($post['mbr_id']) {
            $mbr_id = $post['mbr_id']; //'V183535200;'; // mike
            $pushtype = 'private';
        } else {
            $mbr_id = "";
            $pushtype = 'public';
        }
        $feature = $post["feature"] ? $post["feature"] : 'notification';
        $link_url = $post["link_url"] ? $post["link_url"] : '';


        $this->db20->beginTransaction();
        try {
            $sql_no = <<<str
select MAX(id) as id from o2o_push_notify;
str;
            $numlist = $this->db20->select($sql_no);
            $id = (count($numlist) > 0 && isset($numlist[0]->id)) ? (int)$numlist[0]->id + 1 : 1;


            $sql = <<<str
INSERT INTO o2o_push_notify 
(id, datetime,type,title,message,feature,url,require_login,pushtype,mbr_id, link_url, status,created_by,created_on) 
VALUES 
(?, ?,?,?,?,?,?,'Y',?,?,?,1,'VCAPP', current_timestamp )
str;

            $this->db20->statement($sql, [$id, $ttos, $type, $title, $message, $feature, $url, $pushtype, $mbr_id, $link_url]);
//            $result = $this->db20->select("SELECT max(id)id FROM o2o_push_notify");
//
//            $id = $result[0]->id;

            $this->db20->commit();
            return $id;
        } catch (\Exception $e) {
            $this->db20->rollback();
            return 0;
        }
    }

    private function _curlOneSignalPushRedis($data, $mbr_id = '', $keyname = '')
    {
        $redisKey = $this->ENVIRONMENT . "#oneSignal_push#";

//        echo $redisKey;

        $array = array();
        $key = $mbr_id . $keyname;

        $array[$key] = json_encode($data);

        Redis::hmSet($redisKey, $array);
        Redis::expire($redisKey, $this->threeDays);

        return 'OK20';
    }

    public function batchOneSignalPushRedis($hachipass = '')
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $redisKey = $this->ENVIRONMENT . "#oneSignal_push#";
        $lists = Redis::hgetall($redisKey);
//		print_r($lists);
//		die();
        if (count($lists) == 0) {
            return $this->apiSuccessResponse(1, "No Redis Data.", []);
        }

        foreach ($lists as $index => $list) {
            $data = json_decode($list, true);
//			print_r($data);

            $url = $this->oneSignal["URL_NOTIFICATION"]; //"https://onesignal.com/api/v1/notifications";
            $headers = array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $this->oneSignal["APIKEY_BASIC"] //Mjk5ZWNiYTQtOWVlYi00MzAxLWFmZjMtYWI4NGZlYzFjNmMx'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $result = curl_exec($ch);
            curl_close($ch);

//            print_r($result);

        }

        $this->_logprocess('PUSHREDIS_SUCC', 'ok', count($lists), '', '');

        Redis::del($redisKey);
        return $this->apiSuccessResponse(1, "FINISH", count($lists));
    }

    protected function _logOneSignal($result, $mbr_id, $process_id = "VCTOKEN-ONESIGNAL", $created_by = 'VCTOKEN')
    {
        $this->_logprocess($process_id, 'OK', $result, $mbr_id, $created_by);
    }

    protected function _logOneSignalNew($result, $mbr_id, $process_id = "VCTOKEN-ONESIGNAL", $created_by = 'VCTOKEN', $trans_id = '', $invoice_id = '', $email_addr = '')
    {
        $this->_logNewprocess($process_id, $trans_id, $invoice_id, 'OK', $result, $mbr_id, $email_addr, $created_by);
    }

    //TODO:: deploy live.
    public function batchSaveResetPwdLists()
    {
        ini_set('display_errors', 1);
        ini_set("memory_limit", -1);


        $keyLock = $this->server_name . "#batchSaveResetPwdLists_LOCK";
//        $this->unlockProcessing($keyLock);

        $redisKey = $this->server_name . '#resetPwdBatchLists';
        $twoDayAgo = date('Y-m-d', strtotime('-2 days'));
//        print_r($twoDayAgo);
//        die();

        $this->lockProcess($keyLock, 900);

        $sql = <<<str
    select distinct list.mbr_id, email_addr, first_name
from o2o_log_live_processes as process
inner join crm_member_list as list on list.mbr_id= process.mbr_id
where process.process_id = 'MOBILE_LOGIN_PWD'
    and cast(process.status as int) < 1
     and list.pwd_changed < process.created_on
     and cast(process.created_on as date) >='2021-07-21'
     and cast(process.created_on as date)  < '$twoDayAgo'
    and not exists (
        select 1 from o2o_log_live_processes logg where logg.process_id='VC_INFORM_CHANGEPWD' and logg.mbr_id= list.mbr_id
    )
str;
        $result = $this->db20->select($sql, []);
//        print_r($result);
//        die();

        foreach ($result as $item) {
            $list = $this->vcmemberRepository->stdToArray($item);

            $indexKey = trim($list["mbr_id"]);

            $array[$indexKey] = serialize($list);
            Redis::hMset($redisKey, $array);

        }

        Redis::expire($redisKey, $this->oneWeek);

        $this->unlockProcessing($keyLock);
        echo json_encode(count($result) . "  records inserted into redis.");
    }

    //TODO:: modified this. (Url + change pass not working.)
    public function batchSendResetPwdEmail($amount = 1000)
    {
        ini_set('display_errors', 1);
        ini_set("memory_limit", -1);

        $keyLock = $this->server_name . "#batchSendResetPwdEmail_LOCK";
        $this->lockProcess($keyLock, 900);

        $todayDate = date("Y-m-d");

        $redisKey = $this->server_name . '#resetPwdBatchLists';
        $datas = Redis::hgetall($redisKey);
//        print_r($datas);
//        die();

        $count = 0;
        if (count($datas) > 0) {
            foreach ($datas as $index => $data) {
                $list = unserialize($data);

                $email_addr = trim($list["email_addr"]);
                $mbr_id = trim($list["mbr_id"]);
                $first_name = ucwords(strtolower($list["first_name"]));

                //For Testing.
//                $email_addr= 'weiseng_101@hotmail.com';
//                $mbr_id= 'V160000500';

                echo $email_addr . " - " . $mbr_id . "<br/>";


                $dataToken = array(
                    "email" => $email_addr,
                    "platform" => 'Android',
                    "regid" => ''
                );

                $token = $this->createToken($dataToken, 86400 * 3);
                $edata = array(
                    "token" => $token,
                    "email" => trim($email_addr),
                    "mbr_id" => $mbr_id
                );
                $edata = base64_encode(json_encode($edata));

                $send_email = $email_addr;
                $email_data = array(
                    "r_FirstName" => $first_name,
                    "r_c_resetpw" => "",
                    "r_ctl_url" => ""
                );

                if ($this->ENVIRONMENT == 'development') {
                    $theUrl = 'hachi.tech';
                } else {
                    $theUrl = 'hachi.tech';
                }

                $params = ["longUrl" => $theUrl . '/' . 'api/vc2/resetpw/' . $edata];
//                $curlResponse = $this->shortenUrlCurl($params);
//                $shortUrl = $curlResponse;
//                echo $shortUrl;

                $email_data["r_c_resetpw"] = $params["longUrl"];
                $email_data["r_ctl_url"] = $params["longUrl"];

                $res = $this->_sendEmarsysEmail($this->emarsys_template_list["PASSWORD_RESET_MOBILE"], $send_email, $email_data);
//                print_r($res);

                $this->_logprocess('VC_INFORM_CHANGEPWD', '1', $todayDate, $mbr_id);

                $sql_update = <<<str
    update o2o_log_live_processes set status= cast(cast(status as int) + 1 as varchar(2)), modified_on= NOW() where process_id = 'MOBILE_LOGIN_PWD' and mbr_id= ? 
str;

                $this->db20->statement($sql_update, [$mbr_id]);
                Redis::hDel($redisKey, $index);

                $count++;

                if ($count >= $amount) {
                    break;
                }
            }
        } else {
            $this->unlockProcessing($keyLock);
            return response()->json("No record in redis.");
        }

        $this->unlockProcessing($keyLock);
        return response()->json($count . " emails sent.");
    }

    public function getResendLinkSTU($email_address)
    {
        $mbr_info = $this->db20->select("SELECT * FROM crm_member_list WHERE LOWER(email_addr) = LOWER(?)", [$email_address]);
//        print_r($mbr_info);
//        die();

        $digits = 5;
        $pass = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        $pass0 = $pass;
        $pass = md5($pass);
        $mbr_id = trim($mbr_info[0]->mbr_id);

        if (trim(substr($mbr_info[0]->mbr_type, 0, 3)) != 'MST') {
            return $this->apiSuccessResponse(0, "NOT STUDENT EMAIL", []);
        }
        $this->db20->statement("Update crm_member_list SET mbr_pwd = ? WHERE mbr_id = ?", [$pass, $mbr_id]);
        $email = trim($mbr_info[0]->email_addr);
        $first_name = trim($mbr_info[0]->first_name);
        $token = $this->createToken("edumembership", 86400); // 24 hour
        $edata = $this->base64url_encode(
            json_encode(
                array(
                    "u" => $mbr_id,
                    "p" => $pass,
                    "token" => $token,
                    "e" => $mbr_info[0]->email_addr
                )
            ));
        $url = 'https://chstaff.challenger.sg/api/vc2/activateMember/' . $edata;
        echo "Password: " . $pass0 . "<br>";
        echo $url;
    }

    //No Need Anymore.
    public function batchHpReissueVoucher()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $access_response = $this->getInkDeviceToken();
        if (!isset($access_response["access_token"])) {
            echo "Unable to Get Access Token";
            die();
        }
        $access_token = $access_response["access_token"];
        $partnerId = $this->hpPartnerId;

        $twoWeekAgoDate = date("Y-m-d", strtotime("-2 weeks"));
        echo $twoWeekAgoDate . "<br/>";


        if ($this->ENVIRONMENT == "production") {
            $url = "https://acr.ext.hp.com/acr/v1/devices/$partnerId?createdAt=$twoWeekAgoDate&limit=500";
        } else {
            $url = "https://acr-dev.ext.hp.com/acr/v1/devices/$partnerId?createdAt=$twoWeekAgoDate&limit=500";
        }
        echo $url . "<br/>";


        $headers = array(
            'Authorization: Bearer ' . $access_token,
            "Content-Type: application/json",
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $responses = json_decode($result, 1);
//		print_r($responses);
//		die();

        $count = 0;
        $activeCount = 0;
        foreach ($responses as $response) {
            $printers = $response["printers"];

            foreach ($printers as $printer) {
                $subscribeStatus = $printer["status"];
                if ($subscribeStatus == 'active') {
                    $activeCount++;

                    $modelNumber = $printer["modelNumber"];
                    $serialNumber = $printer["serialNumber"];
                    $subscribeDate = count($printer["subscriptionDates"]) ? $printer["subscriptionDates"][0]["subscribedAt"] : '';

                    $sql_check = <<<str
	select * from o2o_log_live_processes_new where process_id='HP_SUBSCRIBED_VOUCHER' and remarks=? and trans_id=?
str;
                    $check_res = $this->db20->select($sql_check, [$modelNumber, $serialNumber]);

                    if (count($check_res) === 0) {
                        echo $modelNumber . " _ " . $serialNumber . " _ " . $subscribeDate;
                        echo "<br/>";
                        $count++;

                        //Reissue Here.
                        if ($this->ENVIRONMENT == 'production') {
                            $url_curl = 'https://chstaff.challenger.sg/api/vc2/HpPrinterVoucher';
                        } else {
                            $url_curl = 'https://chvc-api.sghachi.com/api/vc2/HpPrinterVoucher';
                        }

                        $X_AUTHORI = $this->hpAccess_token;
                        $headers = array(
                            'X_AUTHORIZATION: ' . $X_AUTHORI,
                            "Content-Type: application/json",
                        );

                        $post_fields = array(
                            "serial_no" => $serialNumber,
                            "model_id" => $modelNumber
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url_curl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        $post_result = json_decode($result, 1);
//						print_r($post_result);

                        $status = $post_result["code"] ? $post_result["code"] : 'XOK';
                        $this->_logprocess('HP_VOUCHER_REISSUE', $status, $serialNumber, $modelNumber, 'VcApp');

                    }
                }
            }

        }

        return $this->apiSuccessResponse(1, "Success", array(
            "Actives" => $activeCount,
            "Reissue" => $count
        ));
    }

    public function batchHpInkInsert($from_date = '')
    {
        ini_set("memory_limit", -1);
        ini_set("display_errors", 1);

        if (!$from_date) {
            $from_date = date("Y-m-d", strtotime("-1 days"));
        }

        print_r($from_date . "<br/>");
//        die();

        $sql_mbr = <<<str
select remarks, mbr_id from o2o_log_live_processes
where process_id='HPINK_USERID'
str;
        $mbr_lists = $this->db20->select($sql_mbr, []);
        $mbr_msg = $this->formatListstoMbrArray($mbr_lists);

        if ($this->ENVIRONMENT == 'development') {
            $url = "http://chappos-api.sghachi.com/api/report/hpinklist";
        } else {
            $url = "https://pos8.api.valueclub.asia/api/report/hpinklist";
        }

        $item_lists = $this->vcmemberRepository->curlPosApi($url, [], false);
        $item_comp_lists = $this->formatItemListstoArray($item_lists);

//        print_r($item_comp_lists);
//        die();

        $sql = <<<str
select
    a.model_id as model_id,
    MAX(a.serial_num) as serial_num,
    MAX(a.printer_model_id) as printer_model_id,
    MAX(a.printer_trans_id) as printer_trans_id,
    a.trans_date,
    a.item_id,
    a.mbr_id,
    a.line_num,
    a.loc_id,
       MAX(a.item_desc) as item_desc,
       a.trans_id,
       cast(MAX(a.item_qty) as int) as qty
from (
                  select item.model_id,
                         processnew.trans_id                                      as serial_num,
                         processnew.remarks                                       as printer_model_id,
                         processnew.invoice_id                                    as printer_trans_id,
                         CONCAT(cast(trans.trans_date as date),'T', cast(trans.trans_date as time),'Z') as trans_date,
                         trans.item_id,
                         trans.mbr_id,
                         trans.line_num,
                         trans.loc_id,
                         trans.item_desc,
                         trans.trans_id,
                         trans.item_qty
                  from crm_member_transaction as trans
                           inner join ims_item_list as item on trans.item_id = item.item_id and item.status_level = '1'
                           inner join o2o_log_live_processes_new as processnew
                                      on processnew.process_id = 'HP_SUBSCRIBED_PRINTER' and
                                         processnew.mbr_id = trans.mbr_id
                      		and cast(processnew.created_on as date)<='$from_date'
                           inner join ims_item_list as item2 on item2.supp_item_id = processnew.remarks
                  where trans.trans_type in ('HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'HR', 'PU', 'ID', 'DZ', 'DU')
                    and not EXISTS(select 1
                                   from o2o_log_live_processes_new as process
                                   where process_id = 'HP_INK_INSERTORDER'
                                     and process.invoice_id = trans.trans_id
                                     and process.mbr_id = trans.mbr_id
                                     and process.remarks = item.model_id)
                    and trans.item_id in ($item_comp_lists)
                    and cast(trans.trans_date as date) >= cast(processnew.created_on as date)
					and trans.mbr_id in ($mbr_msg)
                    and cast(trans.trans_date as date) = '$from_date'
              ) as a group by a.model_id, a.trans_date, a.item_id, a.mbr_id, a.line_num, a.trans_id, a.loc_id

str;

//        echo $sql;
        echo $this->ENVIRONMENT;

        $lists = $this->db20->select($sql, []);
//        print_r($lists);
//        die();

        if (count($lists) > 0) {
            if ($this->ENVIRONMENT == "production") {
                $url = "https://acr.ext.hp.com/acr/v1/orders";
            } else {
                $url = "https://acr-dev.ext.hp.com/acr/v1/orders";
            }
            echo $url . "<br/>";

            $access_response = $this->getInkDeviceTokenOrder();
            if (!isset($access_response["access_token"])) {
                $this->_logprocess('HP_INK_INSERT_FAILED', 0, $from_date, 'ACCESSTOKEN', 'batchHpInkInsert');
                print_r("Unable to Get Access Token");
            }
            $access_token = $access_response["access_token"];
            $partnerId = $this->hpPartnerId;

            foreach ($lists as $index => $item) {
                $list = $this->vcmemberRepository->stdToArray($item);

                $mbr_id = trim($list["mbr_id"]);

                $trans_id = trim($list["trans_id"]);
                $serialNum = $list["serial_num"];
                $printer_model_id = $list["printer_model_id"];
                $ink_model_id = trim($list["model_id"]);
                $trans_date = $list["trans_date"];
                $qty = $list["qty"];

                $hr_lists = array_filter($mbr_lists, function ($var) use ($mbr_id) {
                    return $var->mbr_id == $mbr_id;
                });
                foreach ($hr_lists as $hrlist) {
                    $hr_id = $hrlist->remarks;
                }

                //Need add Trans ID and Trans Date.
                $params = array(
                    "hp_sr_id" => $hr_id,
                    "partnerId" => $partnerId,
                    "modelNumber" => $printer_model_id,
                    "serialNumber" => $serialNum,
                    "transactionId" => $trans_id,
                    "transactionDate" => $trans_date,
                    "consumables" => []
                );
                $params["consumables"] = [array(
                    "suppliesPartNumber" => $ink_model_id
                )];

                print_r($params);
                echo "<br/>";

                $post_fields = json_encode($params);

                $headers = array(
                    'Authorization: Bearer ' . $access_token,
                    "Content-Type: application/json"
                );

//                print_r($post_fields);
//                die();

                for ($i = 0; $i < $qty; $i++) {

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($result, 1);

                    print_r($response);

                    if ($response && isset($response["status"]) && isset($response["hp_sr_id"])) {
                        echo $mbr_id . "_" . $trans_id . "_" . $ink_model_id . "_" . "---OK" . "<br/>";
                        //Insert Success Log
                        $sql = <<<str
      insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_INK_INSERTORDER', ? ,? , '$i', ?, ?, ?, cast(? as date) )
str;
                        $this->db20->statement($sql, [$serialNum, $trans_id, $ink_model_id, $mbr_id, $response["status"], $from_date]);
                    } else {
                        echo $mbr_id . "_" . $trans_id . "_" . $ink_model_id . "_" . "---FAILED";
                        $this->_logprocess('HP_INK_INSERT_FAILED', '$i',
                            $from_date . json_encode($response), $mbr_id, 'batchHpInkInsert');
                    }

                }

            }

        } else {
            echo "No Data: " . $from_date . "<br/>";
        }

        $this->_logprocess('HP_INK_INSERT_SUCCCESS', 1, $from_date, count($lists), 'batchHpInkInsert');
        echo("done. processed: " . count($lists));
    }

    public function batchImportToOneSignal()
    {
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');


        $sql_select = <<<str
SELECT
mbr_id,platform, meta,
       CASE WHEN platform='Android' THEN 1 ELSE 0 END device_type,
       cast(id as varchar(50)) as id2
FROM o2o_member_devices WHERE platform IN ('Android','iOS')
                          and export is null
                          and logout is null
                         and meta not like '%"pushId":""%'
order by modified_on desc limit 100;
str;

        $result = $this->db20->select($sql_select);
//        print_r($result);
//        die();

        foreach ($result as $res) {
            $member = $this->vcmemberRepository->stdToArray($res);

            $id = $member["id2"];
            $meta = json_decode($member["meta"], TRUE);

            if (isset($meta["pushId"])) {
                $device_type = $member["device_type"];
                $mbr_id = $member["mbr_id"];

                $data = array(
                    "app_id" => $this->oneSignal["APP_ID"], //"fb230403-0130-415e-9bb8-3bad87f3d7a6",
                    "device_type" => $device_type,
                    "identifier" => $meta["pushId"],
                    "tags" => array(
                        "mbr_id" => $mbr_id,
                        "type" => substr($mbr_id, 0, 3)
                    )
                );
                $response = $this->_curlOneSignalImport($data);
                if ($response['success']) {
                    $this->db20->statement("UPDATE o2o_member_devices SET export='1' WHERE id = ?", [$id]);
                }
            }

        }

        $this->_logprocess('BATCH_IMPORT_ONESIGNAL', count($result), 'OK', '');
        return $this->apiSuccessResponse(1, 'SUCCESS', "done");
    }

    private function _curlOneSignalImport($data)
    {
        $url = $this->oneSignal["URL_PLAYERS"]; //"https://onesignal.com/api/v1/players";
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic ' . $this->oneSignal["APIKEY_BASIC"]
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, TRUE);
    }

    public function batchHpPrintersOrder($from_date = '')
    {
        ini_set("memory_limit", -1);
        ini_set("display_errors", 1);
        if (!$from_date) {
            $from_date = date("Y-m-d", strtotime("-1 days"));
        }

        print_r($from_date . "<br/>");


        if ($this->ENVIRONMENT == 'development') {
            $url = "http://chappos-api.sghachi.com/api/report/hpinklist";
        } else {
            $url = "https://pos8.api.valueclub.asia/api/report/hpinklist";
        }

        $item_lists = $this->vcmemberRepository->curlPosApi($url, [], false);
        $item_strs = $this->formatItemUPCtoArray($item_lists);


        $sql = <<<str
	  select RTRIM(trans.trans_id) as trans_id, RTRIM(trans.mbr_id) as mbr_id, 
	         trans.item_desc,
         RTRIM(trans.item_id) as item_id, RTRIM(item.supp_item_id) as model_id,
          CONCAT(cast(trans.trans_date as date),'T', cast(trans.trans_date as time),'Z') as trans_date ,
                           case when trans.trans_type in ('HI','HR') THEN 'Online' else 'In-store' end as type
                    from crm_member_transaction as trans
                             inner join ims_item_list as item on trans.item_id = item.item_id and item.status_level = 1
                    where trans.trans_type in ('HI', 'PS', 'OS', 'RF', 'VC', 'EX', 'HR', 'PU', 'ID', 'DZ', 'DU')
                      and trans.item_id in ($item_strs)
                      and cast(trans.trans_date as date) = '$from_date'
                      and not EXISTS(select 1
                                     from o2o_log_live_processes_new as process
                                     where process_id = 'HP_PRINTER_ORDER'
                                       and process.invoice_id = trans.trans_id
                                       and process.mbr_id = trans.mbr_id
                                       and process.trans_id = item.supp_item_id)
str;

        $lists = $this->db20->select($sql, []);
//		print_r($lists);
//		die();

        echo $this->ENVIRONMENT;

        if (count($lists) > 0) {
            if ($this->ENVIRONMENT == "production") {
                $url = "https://acr.ext.hp.com/acr/v1/printerOrders";
            } else {
                $url = "https://acr-dev.ext.hp.com/acr/v1/printerOrders";
            }
            echo $url . "<br/>";

            $access_response = $this->getInkDeviceTokenOrder();
            if (!isset($access_response["access_token"])) {
                echo "Unable to Get Access Token";
                $this->_logprocess('HP_PRINTER_INS_FAILED', 0, $from_date, 'ACCESSTOKEN', 'batchHpPrintersOrder');
                die();
            }
            $access_token = $access_response["access_token"];
            $partnerId = $this->hpPartnerId;
//            print_r($access_token);
//            die();

            $params = array(
                "partnerId" => $partnerId,
                "transactionDate" => '',
                "printers" => []
            );

            $order_lists = [];

            foreach ($lists as $index => $item) {
                $list = $this->vcmemberRepository->stdToArray($item);

                $model_id = $list["model_id"];
                $mode = $list["type"];

                $trans_date = $list["trans_date"];
                if ($index === 0) {
                    $params["transactionDate"] = $trans_date;
                }

                $indexFound = array_search($model_id, array_column($order_lists, 'modelNumber'));

                if ($indexFound > -1) {
                    $order_lists[$indexFound]["count"]++;
                } else {
                    $array = array(
                        "modelNumber" => $model_id,
                        "purchaseMode" => $mode,
                        "count" => 1
                    );
                    $order_lists[] = $array;
                }

            }

            $params["printers"] = $order_lists;

            print_r($params);
//			die();

            $post_fields = json_encode($params);

            $headers = array(
                'Authorization: Bearer ' . $access_token,
                "Content-Type: application/json"
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, 1);

            print_r($response);

            if ($response && isset($response["message"])) {
                foreach ($lists as $item) {
                    $list = $this->vcmemberRepository->stdToArray($item);

                    $mbr_id = $list["mbr_id"];
                    $trans_id = $list["trans_id"];
                    $model_id = $list["model_id"];

                    echo $mbr_id . "_" . $trans_id . "_" . $model_id . "_" . "---OK" . "<br/>";
                    //Insert Success Log
                    $sql = <<<str
      insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status, remarks, mbr_id, created_by, created_on) VALUES
      ('CTL','HP_PRINTER_ORDER', ? ,? , 'OK', ?, ?, ?, cast( ? as timestamp))
str;
                    $this->db20->statement($sql, [$model_id, $trans_id, $response["message"], $mbr_id, $mbr_id, $from_date]);
                }

            } else {
                foreach ($lists as $item) {
                    $list = $this->vcmemberRepository->stdToArray($item);

                    $mbr_id = $list["mbr_id"];
                    $trans_id = $list["trans_id"];
                    $model_id = $list["model_id"];

                    echo $mbr_id . "_" . $trans_id . "_" . $model_id . "_" . "---FAILED";
                }

                //Insert Success Log
                $this->_logprocess('HP_PRINTER_ORDER_FAILED', 0, $from_date . json_encode($response), $mbr_id, 'batchHpPrintersOrder');
            }

        } else {
            echo "No Data: " . $from_date . "<br/>";
        }

        $this->_logprocess('HP_PRINTER_INS_SUCCCESS', 1, $from_date, count($lists), 'batchHpPrintersOrder');
        echo json_encode("done. processed: " . count($lists));
    }

    public function manualRunLoiReminder()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        $LoiPushStart = true;
        $todayDate = date("Y-m-d");

        $sql = <<<str
select * from o2o_log_live_processes where process_id='HP_LOI_LOGS'
and cast(created_on as date)='$todayDate' and
 (remarks like '%"LOI"%' or remarks like '%"VLOI"%')
str;
        $lists = $this->db20->select($sql);

        $count = 0;

        foreach ($lists as $iix => $item) {
            $list = $this->vcmemberRepository->stdToArray($item);

            $remarks = json_decode($list["remarks"], true);

//            print_r($remarks);
//            die();
            $printer = $remarks['printers'];
//            print_r($printer);
//            die();

            $hasSendVcPush = false;

//            foreach ($printers as $ind => $printer) {
            $consumables = $printer["consumables"];
            $serial_number = $printer["serialNumber"];

            $process_id_check = 'LOW_INK_REMIND_' . $serial_number;
            $sql_check = <<<str
select * from o2o_log_live_processes where process_id = '$process_id_check'
and cast(created_on as date)='$todayDate'
str;
            $check_log = $this->db20->select($sql_check);
            if (count($check_log) > 0) {
                echo "Skip :" . $serial_number . "<br/>";
                continue;
            }

            echo $serial_number . "<br/>";

            $ink_message = '';
            $ink_message2 = '';
            $ink_message3 = '';
            $ink_message4 = '';
            $ink_message5 = '';
            foreach ($consumables as $consumable) {
//					print_r($consumables);

                $ink_status = $consumable["status"];
                $color_code = $consumable["ColorCode"];

                if (in_array($ink_status, ['VLOI', 'LOI'])) {
                    $ink_text = '';
                    $ink_color = '';
                    $loi_color = '';

                    if ($ink_status == 'VLOI') {
                        $loi_color = $color_code;
                        $ink_text = 'Very Low on Ink';

                        if ($color_code == "C") {
                            $ink_color = "Cyan";
                            $ink_message = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "M") {
                            $ink_color = "Magenta";
                            $ink_message2 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "K") {
                            $ink_color = "Black";
                            $ink_message3 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "Y") {
                            $ink_color = "Yellow";
                            $ink_message4 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "CMY") {
                            $ink_color = "Tri-Color";
                            $ink_message5 = $ink_color . " - " . $ink_text;
                        }

                    } else if ($ink_status == 'LOI') {
                        $ink_text = 'Low on Ink';
                        $loi_color = $color_code;

                        if ($color_code == "C") {
                            $ink_color = "Cyan";
                            $ink_message = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "M") {
                            $ink_color = "Magenta";
                            $ink_message2 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "K") {
                            $ink_color = "Black";
                            $ink_message3 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "Y") {
                            $ink_color = "Yellow";
                            $ink_message4 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "CMY") {
                            $ink_color = "Tri-Color";
                            $ink_message5 = $ink_color . " - " . $ink_text;
                        }
                    }

                }
            }

            if ($ink_message != '' || $ink_message2 != '' || $ink_message3 != '' || $ink_message4 != '' || $ink_message5 != '') {

                $detail = $this->vcmemberRepository->getMemberBySerialNumber($serial_number, $loi_color);
                print_r($detail);
//                    die();
                if (isset($detail["user"]["email_addr"]) && isset($detail["user"]["mbr_id"])) {

                    $member = $detail["user"];

                    $email_addr = $member["email_addr"] ? $member["email_addr"] : '';
                    $mbr_id = $member["mbr_id"] ? $member["mbr_id"] : '';
                    $first_name = isset($member["first_name"]) ? $member["first_name"] : '';
                    $comp_id = $detail["comp_id"] ? trim($detail["comp_id"]) : '';
                    $item_desc = $detail["item_desc"] ? $detail["item_desc"] : '';

                    echo $serial_number;
                    echo "<br/>";
                    echo $email_addr;
                    echo "<br/><br/>";

                    $send_email = trim($email_addr);
                    $email_data = [
                        'r_PrinterName' => $item_desc,
                        'r_FirstName' => $first_name,
                        'r_ColourLow' => $ink_message,
                        'r_Colour2Low' => $ink_message2,
                        'r_Colour3Low' => $ink_message3,
                        'r_Colour4Low' => $ink_message4,
                        'r_Colour5Low' => $ink_message5,
                        "r_url" => 'www.hachi.tech/products/detail/' . $comp_id
                    ];
                    $resp = $this->_sendEmarsysEmail($this->emarsys_template_list["LOW_INK_REMINDER"], $send_email, $email_data);

                    $this->_logprocess('LOW_INK_REMIND_' . $serial_number, '1', json_encode($resp), $mbr_id);
                    $count++;

                    //Add Push Notification Here. Only send 1 time per day.
                    if ($LoiPushStart && !$hasSendVcPush) {

                        $action_button = "Okay";
                        $text_title = "Low On Ink";
                        $title = "Low On Ink.";
                        $message = "Your printer is low on ink. Tap to find out now.";

                        $page = '/easy-ink';

                        $date = date("Y-m-d");
                        $push_process_id = 'HP_LOI_PUSH_' . $date;

                        $this->PushHpLoiOneSignal($mbr_id, $push_process_id, $title, $message, $page, $action_button, $text_title, 'Close', false);

                        $hasSendVcPush = true;
                    }

                } else {
                    $this->_logprocess('LOI_FAILED', '1', 'User/Trans ID Not Found.', $serial_number);
                }
            } else {
                $this->_logprocess('LOI_FAILED', '1', "Serial Num: " . $serial_number . ", no VLOI / LOI ink.", $serial_number);
            }

        }

        return $this->apiSuccessResponse(1, "OK", count($lists));
    }

    public function batchLoiDeviceReminder()
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        //Set To False If Not Go Live.
        $todayDate = date("Y-m-d");
        $LoiPushStart = true;
        $isRedis = false;
        $redisKey = $this->ENVIRONMENT . '#EasyInkLOI';
        $redisData = Redis::get($redisKey);

        if ($redisData) {
            $response = unserialize($redisData);
            $isRedis = true;
            echo "Using Redis" . "<br/>";
//            print_r($response);
//            die();
        } else {
            echo "Get cUrl" . "<br/>";

            $access_response = $this->getInkDeviceToken();
            if (!isset($access_response["access_token"])) {
                return $this->apiSuccessResponse(0, "Unable to Get Access Token", []);
            }
            $access_token = $access_response["access_token"];
            $partnerId = $this->hpPartnerId;

            if ($this->ENVIRONMENT == "production") {
                $url = "https://acr.ext.hp.com/acr/v1/devices/loi/$partnerId";
            } else {
                $url = "https://acr-dev.ext.hp.com/acr/v1/devices/loi/$partnerId";
            }

            $headers = array(
                'Authorization: Bearer ' . $access_token,
                "Content-Type: application/json",
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);

//        print_r($result);
//        die();

            $response = json_decode($result, 1);
//            print_r($response);

            if ($response) {
                Redis::set($redisKey, serialize($response));
                Redis::expire($redisKey, 50000);
                $isRedis = true;
            }

        }

//        print_r($response);
//        die()

        $count = 0;
//        foreach ($response as $index => $loidevice) {
        foreach ($response["loiDevices"] as $index => $loidevice) {

            $this->_logprocess('HP_LOI_LOGS', 1, json_encode($loidevice), '');

//            $printers = $loidevice['printers'];
            $printer = $loidevice['printers'];
//            print_r($printer);
//            die();

            $hasSendVcPush = false;

//            foreach ($printers as $ind => $printer) {
            $consumables = $printer["consumables"];
            $serial_number = $printer["serialNumber"];

            $process_id_check = 'LOW_INK_REMIND_' . $serial_number;
            $sql_check = <<<str
select * from o2o_log_live_processes where process_id = '$process_id_check'
and cast(created_on as date)='$todayDate'
str;
            $check_log = $this->db20->select($sql_check);
            if (count($check_log) > 0) {
                $this->_logprocess('LOI_FAILED', '1', "Today Already Sent.", $serial_number);
                continue;
            }

            echo $serial_number . "<br/>";

            $ink_message = '';
            $ink_message2 = '';
            $ink_message3 = '';
            $ink_message4 = '';
            $ink_message5 = '';
            foreach ($consumables as $consumable) {
//					print_r($consumables);

                $ink_status = $consumable["status"];
                $color_code = $consumable["ColorCode"];

                if (in_array($ink_status, ['VLOI', 'LOI'])) {
                    $ink_text = '';
                    $ink_color = '';
                    $loi_color = '';

                    if ($ink_status == 'VLOI') {
                        $loi_color = $color_code;
                        $ink_text = 'Very Low on Ink';

                        if ($color_code == "C") {
                            $ink_color = "Cyan";
                            $ink_message = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "M") {
                            $ink_color = "Magenta";
                            $ink_message2 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "K") {
                            $ink_color = "Black";
                            $ink_message3 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "Y") {
                            $ink_color = "Yellow";
                            $ink_message4 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "CMY") {
                            $ink_color = "Tri-Color";
                            $ink_message5 = $ink_color . " - " . $ink_text;
                        }

                    } else if ($ink_status == 'LOI') {
                        $ink_text = 'Low on Ink';
                        $loi_color = $color_code;

                        if ($color_code == "C") {
                            $ink_color = "Cyan";
                            $ink_message = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "M") {
                            $ink_color = "Magenta";
                            $ink_message2 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "K") {
                            $ink_color = "Black";
                            $ink_message3 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "Y") {
                            $ink_color = "Yellow";
                            $ink_message4 = $ink_color . " - " . $ink_text;
                        } else if ($color_code == "CMY") {
                            $ink_color = "Tri-Color";
                            $ink_message5 = $ink_color . " - " . $ink_text;
                        }
                    }

                }
            }

            if ($ink_message != '' || $ink_message2 != '' || $ink_message3 != '' || $ink_message4 != '' || $ink_message5 != '') {

                $detail = $this->vcmemberRepository->getMemberBySerialNumber($serial_number, $loi_color);
//                    print_r($detail);
//                    die();
                if (isset($detail["user"]["email_addr"]) && isset($detail["user"]["mbr_id"])) {

                    $member = $detail["user"];

                    $email_addr = $member["email_addr"] ? $member["email_addr"] : '';
                    $mbr_id = $member["mbr_id"] ? $member["mbr_id"] : '';
                    $first_name = isset($member["first_name"]) ? $member["first_name"] : '';
                    $comp_id = $detail["comp_id"] ? trim($detail["comp_id"]) : '';
                    $item_desc = $detail["item_desc"] ? $detail["item_desc"] : '';

//                    echo $serial_number;
//                    echo "<br/>";
//                    echo $email_addr;
//                    echo "<br/><br/>";

                    $send_email = trim($email_addr);
                    $email_data = [
                        'r_PrinterName' => $item_desc,
                        'r_FirstName' => $first_name,
                        'r_ColourLow' => $ink_message,
                        'r_Colour2Low' => $ink_message2,
                        'r_Colour3Low' => $ink_message3,
                        'r_Colour4Low' => $ink_message4,
                        'r_Colour5Low' => $ink_message5,
                        "r_url" => 'www.hachi.tech/products/detail/' . $comp_id
                    ];
                    $resp = $this->_sendEmarsysEmail($this->emarsys_template_list["LOW_INK_REMINDER"], $send_email, $email_data);

                    $this->_logprocess('LOW_INK_REMIND_' . $serial_number, '1', json_encode($resp), $mbr_id);
                    $count++;

                    //Add Push Notification Here. Only send 1 time per day.
                    if ($LoiPushStart && !$hasSendVcPush) {

                        $action_button = "Okay";
                        $text_title = "Low On Ink";
                        $title = "Low On Ink.";
                        $message = "Your printer is low on ink. Tap to find out now.";

                        $page = '/easy-ink';

                        $date = date("Y-m-d");
                        $push_process_id = 'HP_LOI_PUSH_' . $date;

                        $this->PushHpLoiOneSignal($mbr_id, $push_process_id, $title, $message, $page, $action_button, $text_title, 'Close', false);

                        $hasSendVcPush = true;
                    }

                } else {
                    $this->_logprocess('LOI_FAILED', '1', 'User/Trans ID Not Found.', $serial_number);
                }

            } else {
                $this->_logprocess('LOI_FAILED', '1', "Serial Num: " . $serial_number . ", no VLOI / LOI ink.", $serial_number);
            }

        }

        if ($isRedis) {
            Redis::del($redisKey);
        }

        return $this->apiSuccessResponse(1, "Ok", $count);
    }

    public function batchHpSendReminderToSubscribe($date = '')
    {

        $sql_lists = <<<str
	select 
 distinct  item.trans_id ,member.mbr_id, member.first_name, member.last_name, member.email_addr
from crm_member_transaction item
inner join ims_item_list ims on ims.item_id = item.item_id
inner join crm_member_list as member on member.mbr_id= item.mbr_id
left join sms_invoice_lot as lot on lot.invoice_id= item.trans_id and lot.line_num=item.line_num
left join pos_transaction_lot as lot2 on lot2.trans_id= item.trans_id and lot2.line_num=item.line_num
left join o2o_log_live_processes_new as process on process.process_id='HP_SUBSCRIBED_PRINTER' and process.invoice_id= item.trans_id
where item.trans_type in (
'HI','PS','OS','VC','PU','ID','DZ','DU','EX')
and (lot.lot_id is not null or lot2.lot_id is not null)
and ((ims.item_id in (select item_id from ims_item_comp_id where ref_type='HPINK')))
and process.trans_id is null
and cast(item.trans_date as datetime) >= DATEADD(HOUR, 12, DATEADD(dd, DATEDIFF(dd, 1, '$date'), 0))
and cast(item.trans_date as datetime) <= DATEADD(HOUR,12,CONVERT(VARCHAR(10), '$date',110))

str;

    }

    //TODO:: add to live.
    public function batchHpSendInactiveReminder()
    {
        ini_set("display_errors", 1);

// 		$thedate = date("Y-m-d");
// 		echo "Date: " . $thedate . "<br/>";

//        $json_datas = $this->getJSONHPInactivePrinter();

        $redisKey = $this->ENVIRONMENT . '_inactivePrinters';
        $json_datas = Redis::get($redisKey);
        $json_datas = unserialize($json_datas);
        print_r(count($json_datas));
//        die();

//        print_r($redisKey);
//        Redis::set($redisKey, serialize($json_datas));
//        Redis::expire($redisKey, 100000);

        //For Testing Check.
//		echo $count;
//		print_r(count($json_datas));
//        print_r($json_datas);
//        die();

        $count = 0;
        if (count($json_datas) > 0) {
            foreach ($json_datas as $index => $json) {
                $hr_id = $json["hp_sr_id"];
                $printers = $json["printers"];

                $sql = <<<str
select * from "members_meta" where meta= ?
str;
                $chappsmember = $this->chapps->select($sql, [$hr_id]);
                if ($chappsmember && count($chappsmember) > 0) {
                    $list = $this->vcmemberRepository->stdToArray($chappsmember[0]);

                    $mbr_id = $list["key"];
//                    print_r($mbr_id);
//                    $mbr_id= 'V160000500';

                    $sql_member = <<<str
select first_name,last_name,mbr_id,email_addr from crm_member_list where mbr_id=?
str;
                    $member_select = $this->db20->select($sql_member, [$mbr_id]);


                    if ($member_select && count($member_select) > 0) {
                        $member = $this->vcmemberRepository->stdToArray($member_select[0]);

                        $first_name = $member["first_name"] ? $member["first_name"] : $member["last_name"];
                        $mbr_id = trim($member["mbr_id"]);
                        $email_addr = $member["email_addr"];

//                        print_r($member);
//                        print_r($email_addr);
//                        die();

                        if (!$member || !$email_addr) {
                            echo "Unable to get Email for: " . $mbr_id . " - " . $hr_id . " <br/>";
                            $this->_logprocess('HP_SEND_INACTIVE_REMIND_ERROR', 0, "Unable to get Email: " . $hr_id, $mbr_id, 'VcApp');
                        } else {
                            foreach ($printers as $printer) {
                                $setupOption = $printer["setupOption"];
                                $serial_num = $printer["serialNumber"];

                                $sql_check = <<<str
select * from o2o_log_live_processes_new where process_id='HP_SEND_INACTIVE_REMIND' and trans_id= ? and mbr_id=? and status = ?
str;
                                $check = $this->db20->select($sql_check, [$serial_num, $mbr_id, $setupOption]);

                                //TODO:: check, Invalid request payload
                                $template_id = $this->emarsys_template_list["HP_ACTIVATION_NEW"];

                                if (count($check) === 0) {
                                    echo "Ready: " . $email_addr . " - " . $mbr_id . " - " . $first_name . " - " . $hr_id . "<br/>";

                                    $email = $email_addr;
                                    $email_data = array(
                                        'r_C_First_Name' => $first_name
                                    );

                                    //For Testing Only.
//                                    $email = 'wong.weiseng@challenger.sg';

//								print_r($email_data);
//								echo "<br/><br/>";

                                    $sql = <<<str
  insert into o2o_log_live_processes_new (coy_id, process_id, trans_id, invoice_id, status, remarks, mbr_id, created_by, created_on) VALUES
  ('CTL','HP_SEND_INACTIVE_REMIND', ? ,? , ?, ?, ?, ?, NOW())
str;
                                    $this->db20->statement($sql, [$serial_num, $email_addr, $setupOption, $hr_id, $mbr_id, 'VcApp']);

                                    $this->_sendEmarsysEmail($template_id, $email, $email_data);
                                    $count++;

                                } else if ($check) {
                                    echo "Already Sent: " . $email_addr . " - " . $mbr_id . " - " . $first_name . " - " . $hr_id . "<br/>";
                                    $this->_logprocess('HP_SEND_INACTIVE_REMIND_ERROR', 0, "Already Sent: " . $email_addr . " - " . $hr_id, $mbr_id, 'VcApp');
                                } else {
                                    echo "WRONG setupOption: " . $email_addr . " - " . $mbr_id . " - " . $first_name . " - " . $hr_id . "<br/>";
                                    $this->_logprocess('HP_SEND_INACTIVE_REMIND_ERROR', 0, "WRONG setupOption: " . $email_addr, $mbr_id, 'VcApp');
                                }
                            }
                        }
                    }
                } else {
                    echo "Unable to find HR_ID: " . $hr_id . " <br/>";
                    $this->_logprocess('HP_SEND_INACTIVE_REMIND_ERROR', 0, "Unable to find HR_ID: " . $hr_id, '', 'VcApp');
                }

                //For Testing.
                if ($index === 0) {
                    break;
                }

            }

            return $this->apiSuccessResponse(1, "Success. " . $count . " emails sent.", []);
        } else {
            $this->_logprocess('HP_SEND_INACTIVE_REMIND_ERROR', 0, 'No Data / Empty Json File: ', '', 'VcApp');
            return $this->apiSuccessResponse(0, 'No Data.', []);
        }

    }

    private function getJSONHPInactivePrinter()
    {
        $access_response = $this->getInkDeviceToken();
        $access_token = $access_response["access_token"];
        $partnerId = $this->hpPartnerId;
//		 if (ENVIRONMENT == "production") {
//		 	$url = "https://acr.ext.hp.com/acr/v1/devices/inactive/$partnerId";
//		 } else {
//		 	$url = "https://acr-dev.ext.hp.com/acr/v1/devices/inactive/$partnerId";
//		 }
//		 echo $url;
        $url = "https://acr.ext.hp.com/acr/v1/devices/inactive/$partnerId";
        $headers = array(
            'Authorization: Bearer ' . $access_token,
            "Content-Type: application/json",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);
        return $result;
        //echo $result[0]['hp_sr_id'];
        //print_r($result);

    }

    public function batchPushClickStatus()
    {
        ini_set('memory_limit', '-1');
        ini_set("display_errors", 1);
        $isPushTime = $this->checkIfBlockSql();
        if ($isPushTime) {
            return $this->apiSuccessResponse(0, "Push Notification now.", null);
        }

        $keyLock = $this->server_name . "#batchPushClickStatus_LOCK";
        ///Uncomment this if need.
        $this->lockProcess($keyLock);

        $redisKey = $this->ENVIRONMENT . "#Vc_pushNotification_all";
        $lists = Redis::hgetall($redisKey);
//        print_r($lists);
//        die();

        if (!$lists) {
            $this->unlockProcessing($keyLock);
            return $this->apiSuccessResponse(0, "No Batch Job.", null);
        }

        $result = ["success" => 0, "fail" => 0];
        foreach ($lists as $index => $list) {
            $param = unserialize($list);

            $sql = $param["sql"];
            $params = $param["params"];

            if ($sql) {

                $res = $this->db20->statement($sql, $params);

                Redis::hDel($redisKey, $index);
                $result["success"] = $result["success"] + 1;


            }
        }

        $temp_new_id = $this->vcmemberRepository->getLatestPushLogId();
        $result["latest_id"] = $temp_new_id;

        if ($temp_new_id) {
            $this->vcmemberRepository->updatePushOption($temp_new_id, 'latest_push_id');
        }

        $this->unlockProcessing($keyLock);
        return $this->apiSuccessResponse(1, "Success", $result);
    }

    public function batchSendStarRedemptionReport()
    {
        ini_set("display_errors", 1);

        $ytdDate = date("Y-m-d", strtotime("-1 day"));
        $todayDateTitle = date("d M Y");
        $ytdDateTitle = date("d M Y", strtotime("-1 day"));

//        $ytdDate= '2021-07-11';
//        print_r($ytdDate);

        $url = $this->imsUrl . 'vcapp/getytdredeemitems';
//        echo $url;

        $post_data = array(
            "date" => $ytdDate
        );

        $items = $this->curlImsApi($url, $post_data);

//        print_r($items);
//        die();

        $body = <<<str
<style>
    td{
        border: 1px solid grey;
        padding: 2px 4px;
    }
</style>
str;

        $subject = 'Star Redemption Daily report (Date: ' . $todayDateTitle . ") ";
        if (count($items) === 0) {
            $body .= "<b>NO</b> item redeemed yesterday. ( <b>" . $ytdDateTitle . " </b>)" . "<br/><br/>";
        } else {
            $qty = 0;
            foreach ($items as $index => $list) {
                $item = $this->vcmemberRepository->stdToArray($list);
                $qty += (int)$item["total_qty"];

            }


            $body .= "<b>" . $qty . "</b> item(s) redeemed yesterday. ( <b>" . $ytdDateTitle . "</b> )" . "<br/><br/>";

            $body .= "<table style='border: 1px solid orange;'>";
            $body .= "<thead><td>Date of Redemption</td><td>Item ID</td><td>Item Description</td><td>No. of Redemption</td><td>Price.</td><td>Location.</td> </b>" . "</td></thead>";

            foreach ($items as $index => $list) {

                $item = $this->vcmemberRepository->stdToArray($list);
//                print_r($item);
//                die();

                $price = number_format($item["unit_price"], 2);
                $loc_id = $item["loc_id"] ? $item["loc_id"] : '';
                $location = explode("-", $loc_id)[0];

                $body .= "<tr><td>" . $item["the_invoice_date"] . "</td><td>" .
                    $item["item_id"] . "</td><td>" . $item["item_desc"] . "</td><td><b>" . (int)$item["total_qty"] . "</b></td><td>" . $price . "</td><td>" . $location . "</td></tr>";


            }

            $body .= "</table>";
            $body .= "<br/><br/>";

        }

        $email_detail = $this->vcmemberRepository->getEmailListings("EMAIL_STAR22");

        $emails = $email_detail["email"];
        $cc = $email_detail["cc"];

//        $emails = 'wong.weiseng@challenger.sg';
//        $cc = '';

        $res = $this->vcmemberRepository->sendEmailByCtlMailer('cherps@challenger.sg', $emails, $body, $subject, $cc);
        $this->_logprocess('SR_DAILY_REPORT', '1', json_encode($res), $ytdDate, 'VcApi');


        return $this->apiSuccessResponse(1, 'Ok' . $ytdDate, $res);
    }

    public function batchUpdateProductReview($hachiPass = '')
    {
        ini_set("display_error", 1);
        if ($hachiPass !== $this->_accessPass) {
            return $this->apiSuccessResponse(0, "Permission Denied.", []);
        }
        $key = $this->server_name . $this->productReview_key;
        $dateKey = $this->server_name . $this->reviewLastUpdate;

        $redisDate = Redis::get($dateKey);

        $last_update = '1900-01-01';
        if ($redisDate) {
            $last_update = unserialize($redisDate);
        }

//        echo $last_update;
//        die();

        $sql = <<<str
select *
from (
         SELECT RTRIM(item_id)                         AS item_id,
                CAST(AVG(star_count) AS DECIMAL(5, 2)) AS rating,
                SUM(like_count)                        AS like_count,
                SUM(dislike_count)                     AS dislike_count,
                MAX(cast(modified_on as date))                       AS modified_on
         FROM "Product_review"
         WHERE item_id IN (SELECT DISTINCT item_id
                           FROM "Product_review"
                           WHERE cast(modified_on as date) >= '$last_update'
     ) and status_level > 0
GROUP BY item_id
    ) as a
order by a.modified_on desc
str;
//        echo $sql;
        $lists = $this->chapps->select($sql, []);
//        print_r($lists);
//        die();

        $update_count = 0;

        if ($last_update == '1900-01-01') {
            Redis::del($key);
        }

        if (count($lists) > 0) {
            $redisData = Redis::hgetall($key);
            foreach ($lists as $item) {
                $list = $this->vcmemberRepository->stdToArray($item);

                $list["rating_raw"] = number_format($list["rating"], 2);
                $list["rating"] = round((float)$list["rating"]);
                $list["like_count"] = round((float)$list["like_count"]);
                $list["dislike_count"] = round((float)$list["dislike_count"]);
                $item_id = $list["item_id"];
                $redisData[$item_id] = json_encode($list);
                $update_count++;
            }
            Redis::hMset($key, $redisData);
            Redis::expire($key, $this->threeDays);

            $last_update = $lists[0]->modified_on;
            Redis::set($dateKey, serialize($last_update));
            Redis::expire($dateKey, $this->threeDays);
        }

//        echo $key;

        return $this->apiSuccessResponse(1, "Success" . $update_count . " " . $last_update, []);
    }

    public function deleteBatchRedis($pass = '')
    {
        if ($pass !== $this->_accessPass) {
            die();
        }
        $mbrListkey = $this->server_name . "#batchUpdateMembers_Lists";
        $mbrCountKey = $this->server_name . "#batchUpdateMembers_Count";
        $dateCountKey = $this->server_name . "#batchUpdateMembers_Date";
        $keyLock = $this->server_name . "#batchUpdateMembers_Postgres";
        $keylock2 = $this->server_name . "#batchUpdateMembers_Postgres_EXP";

        $key = Redis::del($keyLock);
        $result1 = Redis::del($mbrCountKey);
        $result2 = Redis::del($mbrListkey);
        $result3 = Redis::del($dateCountKey);
        $result4 = Redis::del($keylock2);
        echo "Del: Count" . $result1 . ", MBr:" . $result2 . ", Date:" . $result3 . $key . $result4 . "<br/>";

    }

    public function batchUpdateMembers($limit = 50)
    {
        ini_set("display_errors", 1);
        ini_set('memory_limit', '-1');

        if ($this->checkIfBlockSql()) {
            return $this->apiSuccessResponse(0, 'Stopped 1st while pushing', []);
        }

        $mbrListkey = $this->server_name . "#batchUpdateMembers_Lists";
        $mbrCountKey = $this->server_name . "#batchUpdateMembers_Count";
        $dateCountKey = $this->server_name . "#batchUpdateMembers_Date";


        $keyLock = $this->server_name . "#batchUpdateMembers_Postgres";
//        $this->unlockProcessing($keyLock);
        $this->lockProcess($keyLock, $limit * 2);

        $list_data = Redis::get($mbrListkey);
        $mbr_count_data = Redis::get($mbrCountKey);
        $date_data = Redis::get($dateCountKey);


        if ($list_data && $mbr_count_data && $date_data) {
            echo "Getting from Saved Redis:<br/>";
            $mbr_lists = unserialize($list_data);
            $mbr_count = unserialize($mbr_count_data);
            $date_lists = unserialize($date_data);

            echo "Data COUNT:" . count($mbr_lists) . "<br/>";
            echo "Start FROM:" . $mbr_count . "<br/>";
            echo "Date: ";
//            print_r($date_lists);
//            die();
            echo "<br/>";
        } else {
            echo "Getting from DB:<br/>";

            $last_modifieds = $this->chapps->select('SELECT * FROM "Modifiedon" ');
            $last_modified_on = array();
            foreach ($last_modifieds as $last_modified_item) {
                $last_modified = $this->vcmemberRepository->stdToArray($last_modified_item);

                $last_modified_on[trim($last_modified["tablename"])] = $last_modified["last_modified_on"];
            }

            $sql_mbr = <<<str
 select * from (
		(select mbr_id, cast(modified_on as timestamp) as modified_on,'o2o_member_devices' as tablename
		from o2o_member_devices where modified_on >  ? limit $limit)
        UNION
        (select mbr_id, cast(modified_on as timestamp) as modified_on,'crm_member_list' as tablename
		from crm_member_list where modified_on > ? limit $limit)
        UNION
	    (select ref_id as mbr_id, cast(modified_on as timestamp) as modified_on,'coy_address_book' as tablename
		from coy_address_book  where modified_on > ? limit $limit)
	    UNION
	    (select main_id as mbr_id, cast(modified_on as timestamp) as modified_on,'crm_member_list' as tablename
		from crm_member_list where main_id!='' and modified_on > ? limit $limit)
	    UNION
		(select mbr_id as mbr_id, cast(modified_on as timestamp) as modified_on,'crm_member_points' as tablename
		from crm_member_points where modified_on > ? limit $limit)
		UNION
		(select mbr_id as mbr_id, cast(created_on as timestamp) as modified_on,'o2o_push_log' as tablename
		from o2o_push_log where created_on >  ? limit $limit)
		UNION
		(select mbr_id as mbr_id, cast(modified_on as timestamp) as modified_on,'crm_member_transaction' as tablename
		from crm_member_transaction where modified_on > ? limit $limit)
		UNION
		(select mbr_id as mbr_id, cast(modified_on as timestamp) as modified_on,'crm_voucher_list' as tablename
		from crm_voucher_list where modified_on > ? limit $limit)
	) as a order by tablename,modified_on desc;
str;
            $sql_mbr_lists = $this->db20->select($sql_mbr,
                [$last_modified_on["o2o_member_devices"], $last_modified_on["crm_member_list"], $last_modified_on["coy_address_book"],
                    $last_modified_on["crm_member_list"], $last_modified_on["crm_member_points"],
                    $last_modified_on["o2o_push_log"], $last_modified_on["crm_member_transaction"],
                    $last_modified_on["crm_voucher_list"]]);

//            print_r($this->db->last_query());

            echo "Data GET:" . count($sql_mbr_lists);
            echo "<br/>";
//            print_r($sql_mbr_lists);
//            die();

            $date_lists = array();
            $mbr_lists = array();
            $table_key = '';
            foreach ($sql_mbr_lists as $index => $sql_mbr_item) {
                $sql_mbr_list = $this->vcmemberRepository->stdToArray($sql_mbr_item);

                $mbr_id = trim($sql_mbr_list["mbr_id"]);
                $modified_on = $sql_mbr_list["modified_on"];
                $table_name = $sql_mbr_list["tablename"];
                if (!$table_key || $table_key !== $table_name) {
                    $table_key = $table_name;
                    $table = array(
                        "tablename" => $table_name,
                        "last_modified_on" => $modified_on
                    );
                    $date_lists[] = $table;
                }
                if (in_array($mbr_id, $mbr_lists)) {
                    continue;
                }
                array_push($mbr_lists, $mbr_id);
            }


//            print_r($mbr_lists);
//            print_r($date_lists);
//            die();

            //Save Into Redis.
            Redis::set($dateCountKey, serialize($date_lists));
            Redis::set($mbrListkey, serialize($mbr_lists));

            $mbr_count = 0;
            echo "Start Update From Count: 0<br/>";
        }

        if (!$mbr_lists || !$date_lists) {
            $this->unlockProcessing($keyLock);
            $result1 = Redis::del($mbrCountKey);
            $result2 = Redis::del($mbrListkey);
            $result3 = Redis::del($dateCountKey);
            echo "Del: Count" . $result1 . ", MBr:" . $result2 . ", Date:" . $result3 . "<br/>";
            $this->_logprocess('POSTGRE_UPDATE_NODATA', '0', "No Data: " . date("Y-m-d H:i:s"), 'Vc-Batch', 'Vc-Batch');
            die();
        }

        $start = $mbr_count;
        $max_count = count($mbr_lists) - 1;
        for ($i = $start; $i < $start + $limit; $i++) {
            $mbr_id = $i <= $max_count ? $mbr_lists[$i] : '';
            if ($mbr_id) {
                $result = $this->updateOrInsertMbr($mbr_id, $this->_accessPass);
                if (!$result) {
                    $this->_logprocess('POSTGRE_UPDATEFAIL_MBR', '0', "Failed " . $mbr_id, $mbr_id, 'Vc-Batch');
                } else {
//                    print_r($result);
//                    echo "<br/>";
                }
            } else {
                echo "Break<br/>";
                break;
            }
        }
        $mbr_count += $limit;

        print_r("Updated Mbr List Count: ");
        print_r(count($mbr_lists));
        echo "<br/>";

        //Update Modified On Date. UPDATE EVERYTIME.
        foreach ($date_lists as $date_list) {
            $query = <<<str
            INSERT INTO "Modifiedon" (tablename, last_modified_on) 
            VALUES (?, ?)
            ON CONFLICT (tablename) DO UPDATE 
              SET last_modified_on = ?
str;
            $this->chapps->statement($query,
                [$date_list["tablename"], $date_list["last_modified_on"], $date_list["last_modified_on"]]);
        }


        echo "After Updated:" . $mbr_count . "<br/>";
        echo "Max Count: " . $max_count . "<br/>";
        if ($mbr_count > $max_count) {
            echo "Clear Redis<br/>";

            $result1 = Redis::del($mbrCountKey);
            $result2 = Redis::del($mbrListkey);
            $result3 = Redis::del($dateCountKey);
            echo "Del: Count" . $result1 . ", MBr:" . $result2 . ", Date:" . $result3 . "<br/>";
        } else {
            echo "Count Change to:" . $mbr_count . "<br/>";
            if ($mbr_count <= $max_count) {
                echo "Next Mbr ID: " . $mbr_lists[$mbr_count] . "<br/>";
            }
            Redis::set($mbrCountKey, serialize($mbr_count));
        }

//        if ($this->chapps->trans_status() === FALSE) {
//            $this->unlockProcessing($keyLock);
//            $this->_logprocess('POSTGRE_UPDATEFAIL_DATE', '0', "Failed Update ModefiedOn: " . date("Y-m-d H:i:s"), 'Vc-Batch', 'Vc-Batch');
//            die();
//        }

        echo "Finished";
        $this->unlockProcessing($keyLock);
    }


    //Manual JOb.
    public function manualSendPushToEmail($token = '')
    {
        ini_set("display_errors", 1);
        ini_set("memory_limit", -1);

        if ($token !== $this->hachiPass) {
            return $this->apiSuccessResponse(0, 'Access Denied.', []);
        }

        $mbr_lists = array();
        $mbr_list2s = array();
        if (($handle = fopen('files/mbr_sales12.csv', "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($count === 0) {
                    $count++;
                }

                $the_mbr_id = $data[0];
//                $email = $data[1];
//                print_r($email);
//                break;

//                $sql_member = <<<str
//    select * from crm_member_list where email_addr= '$email'
//str;
//                $member = $this->db20->select($sql_member, []);
//                if ($member) {
//                    $mbr_id = $member[0]->mbr_id;
//
//                    $mbr_lists[] = $mbr_id;
//
//                }
                $mbr_lists[] = $the_mbr_id;

                $count++;
            }

            fclose($handle);
        }

        $mbr_txt = "";
        foreach ($mbr_lists as $index => $mbr_list) {
            $mbr_txt .= "'" . $mbr_list . "'";
            if ($index !== count($mbr_lists) - 1) {
                $mbr_txt .= ", ";
            }
        }

//        print_r($mbr_txt);

        $sql_del = <<<str
delete from o2o_push_notify where  url='https://www.hachi.tech/search?query=Jabra%20Elite%2085T'
and mbr_id in ($mbr_txt)
str;
        $this->db20->statement($sql_del);

//        die();

//        $sql_member=<<<str
//select mbr_id from o2o_push_notify where url='https://www.hachi.tech/search?query=Jabra%20Elite%2085T' order by id desc
//str;
//        $pushes= $this->db20->select($sql_member);
//        $mbr_exists=[];
//        foreach($pushes as $push){
//            $mbr_id= $push->mbr_id;
//            $mbr_exists[]= $mbr_id;
//        }
//
//
//        foreach($mbr_list2s as $mbr_list2){
//            if(!in_array($mbr_list2, $mbr_exists)){
//                $mbr_lists[]= $mbr_list2;
//            }
//        }

//        print_r($mbr_lists);
//        die();

        foreach ($mbr_lists as $index => $mbr_id) {
            echo $index . "<br/>";
            echo $mbr_id . "<br/>";
            if ($mbr_id) {
                $url = "https://chstaff.challenger.sg/api/vc2/insertEmarsysPush";

                $post_fields = array(
                    "page" => "",
                    "title" => "Upgrade your audio experience with the Jabra Elite 85t now and enjoy $20 OFF specially for you!",
                    "body" => "Check out with code '20OFF85T' for discount valid till 30 June only! Tap to shop now.",
                    "url" => "https://www.hachi.tech/search?query=Jabra%20Elite%2085T",
                    "ttos" => "2021-06-26 10:00:00",
                    "feature" => "notification",
                    "mbr_id" => $mbr_id,
                    "link_url" => ""
                );

                $headers = array(
                    'X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG',
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_fields));
                $result = curl_exec($ch);

                print_r($result);
                echo "ok";

                curl_close($ch);
                $result = json_decode($result, TRUE);

//                print_r($result);

            }

            $index++;
//            if($index === 2){
//                break;
//            }
        }

        return response()->json(['OK', $count]);
    }

    public function manualInsertStarRedemptionList($insert = '')
    {
        ini_set("display_errors", 1);

        $fileDirectory = 'files/redemption_lists(0712).csv';


        $item_lists = [];
        if (($handle = fopen($fileDirectory, "r")) !== FALSE) {
            $line = 0;
            $count = 0;
            $line_num = 1;
            //Forloop whole .csv file and import to db
            while (($data = fgetcsv($handle, 0, ',', '"', '"')) !== FALSE) {
                if ($count === 0) {
                    $count++;
                    continue;
                }
//				print_r($data);

                $item = array(
                    "line_num" => $line_num,
                    "item_desc" => $data[0],
                    "item_id" => str_pad($data[1], 13, '0', STR_PAD_LEFT),
                    "mbr_price" => str_replace(",", "", str_replace("$", "", $data[3])),
                    "unit_price" => str_replace(",", "", str_replace("$", "", $data[2])),
                    "discount_label" => $data[4],
                    "rebate_required" => str_replace(",", "", str_replace("$", "", $data[5])),
                    "qty" => $data[6],
                    "image_name" => ''
                );


                $item_lists[] = $item;

                $line_num++;
                $count++;
                $line++;
            }
        }

        $invoice_str = "";

//		print_r($item_lists);
//		die();

        foreach ($item_lists as $index => $item) {
            $item_id = $item["item_id"];

            $api_url = "https://pos8.api.valueclub.asia/products/$item_id?loc_id=BF&customer_group=MEMBER";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer amXgnuZPGE9DxOKq1S40RuI1947esWaT',
                "Content-Type: application/json"
            ));

            curl_setopt($ch, CURLOPT_URL, $api_url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = curl_exec($ch);

//			print_r($response);

            $resp = json_decode($response, true);

            $image_file = $resp["image_name"];

            $file_name = explode("product_images_thumb/", $image_file)[1];
//			print_r($file_name);

            $item_lists[$index]["image_name"] = $file_name;
//			break;

        }
//
//		print_r($item_lists);
//		die();

        foreach ($item_lists as $index => $item) {

            $sql2 = <<<str
	insert into crm_redemption_list (coy_id, line_num, item_id, item_desc, points_required, eff_from, eff_to, image_name,
                                 created_by, created_on, modified_by, modified_on, updated_on, loc_id, discount_amt, unit_price)
                                 VALUES ('CTL',?,?,?,?,current_timestamp ,'2021-12-31',?, 'ws',current_timestamp,'',current_timestamp,current_timestamp,?,?,?)
str;
            $this->db20->statement($sql2, [$item["line_num"], $item["item_id"], $item["item_desc"], $item["rebate_required"] * 100,
                $item["image_name"], 'BF', $item["discount_label"], (float)$item["unit_price"]
            ]);

        }

//insert into ims_inv_physical
//        if ($insert == '1') {
//            foreach ($item_lists as $index => $item) {
//                $qty = $item["qty"] ? $item["qty"] : 1;
//                $item_id = $item["item_id"];
//
//                $sql3 = <<<str
//insert into ims_inv_physical (coy_id, item_id, loc_id, line_num, trans_date,
//  qty_on_hand, qty_reserved, unit_cost, supp_csg, initial_date,
//   ref_id, ref_rev, ref_num, created_by, created_on, modified_by,
//                              modified_on, updated_on) VALUES
//('CTL',?, 'BF-S',1,current_timestamp, ?, 0, 0, 'N', current_timestamp,'LIVE',1,1,'ws', current_timestamp,
// 'ws',current_timestamp, current_timestamp)
//
//str;
//                $this->db20->query($sql3, [$item_id, $qty]);
//
//            }
//        }

        $this->apiSuccessResponse(1, "Success", count($item_lists));
    }

    public function openvcapp()
    {
        $edata = base64_encode(json_encode(array(
            "e" => "",
            "p" => ""
        )));
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/autologin/$edata");
        } else {
            return redirect("https://www.challenger.sg/valueclub-app");
        }
    }

    public function vcAppQrLogin($code)
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
        if ($mobile) {
            return redirect("valueclub://app/loginQr/$code");
        } else {
            return redirect("https://www.challenger.sg/valueclub-app");
        }
    }

    public function odInvoiceRefund(Request $request)
    {
        $invoice_id = $request->invoice_id;
        $mbr_id = $request->mbr_id;

        $select_sql = "
            select * from crm_member_transaction where mbr_id ='$mbr_id' and trans_id= '$invoice_id' and trans_type='RD'
        ";

        $result = $this->db20->select($select_sql);

        $trans_points = $result && $result[0] ? $result[0]->trans_points : 0;
        $rebate_amt = abs($trans_points / 100);
        // print_r($rebate_amt);

        // Refund points
        $yearMonth = date("Ym");
        $coy_id = $this->CTL_COY_ID;
        $trans_prefix = 'ADJUSTAP' . $yearMonth;

        $inv_id_header = 'AP' . $yearMonth;

        $func = "points/award";
        $ap_trans_id = $this->vcmemberRepository->generateNextNum($trans_prefix, $inv_id_header, 15, 'MEMBER', 'CRM', $coy_id);

        $post_data = array(
            "mbr_id" => $mbr_id,
            "trans_id" => $ap_trans_id,
            "rebate_amount" => $rebate_amt,
            "rebate_description" => "Refund OD Invoice $invoice_id"
        );

        $rebate_res = $this->curlCHPos($func, $post_data);

        //Log Process (Update Rebates)
        $status = 'XOK';
        if (isset($rebate_res["status_code"])) {
            $status = 'OK';
        }

        $this->_logNewprocess('CHL_CANCEL_OD_INV', $invoice_id, $ap_trans_id, $status, json_encode($rebate_res), $mbr_id, $mbr_id, 'VcApp');

        return $this->apiSuccessResponse(1, "Success", $rebate_res);
    }


    //-- Eventbridge Batch Job Testing.
    public function testBatchJob()
    {
        $subject = "Test EventBridge";

        $body = <<<str
        <p>Batch Job Migration Testing</p>
str;
        $emails = "chin.hockyang@challenger.sg";
        $cc = "";

        $res1 = $this->vcmemberRepository->sendEmailByCtlMailer('ValueClub@challenger.sg', $emails, $body, $subject, $cc);

        return $this->apiSuccessResponse(1, "Success", $res1);
    }

}