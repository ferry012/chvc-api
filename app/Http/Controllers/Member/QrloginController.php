<?php

namespace App\Http\Controllers\Member;

//use App\Events\DataOperation;
//use App\Facades\Member\Voucher\Voucher;
//use App\Http\Controllers\Result;
//use App\Models\User;
//use Carbon\Carbon;
//use function GuzzleHttp\default_ca_bundle;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Log;
//use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Str;

class QrloginController extends Controller
{

    public function __construct()
    {

    }

    /**
     * @api {get} /qr/login QR Login
     * @apiGroup 1.QR LOGIN
     *
     * @apiParam {string} channel_code Channel Code
     * @apiParam {string} mbr_id Member ID
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "code": "1",
     *      "msg": "Member Login successful",
     *      "data": {
     *          "mbr_id": "V1111111ZZ"
     *      }
     *  }
     */
    public function qr_login(){

        $data = json_decode(file_get_contents('php://input'), true);

        $code_sep = strpos($data['channel_code'],'#');
        $body['code'] = substr($data['channel_code'],0,$code_sep);
        $body['channel'] = substr($data['channel_code'],$code_sep+1);
        $body['mbr'] = (isset($data['mbr_id'])) ? trim($data['mbr_id']) : 'STAFF_'.trim($data['staff_id']);
        $body['endpoint'] = 'jlugnxb26f.execute-api.ap-southeast-1.amazonaws.com/prod';

        $endpoint = "https://cajb3d5jy5.execute-api.ap-southeast-1.amazonaws.com/prod_v1/chpos-ws";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($body)
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        if (isset($content_data['code']) && $content_data['code']==1) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Member login successful'; //$content_data['msg'];
            $return_data['data'] = array('mbr_id'=>$content_data['mbr']);
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = (isset($content_data['code'])) ? $content_data['msg'] : 'Member login fail';
        }
        echo json_encode($return_data);

    }

    /**
     * @api {get} /qr/init Get QR Code
     * @apiGroup 1.QR LOGIN
     *
     * @apiParam {string} loc_id Location ID
     * @apiParam {string} pos_id Source ID (Point-of-Sale ID or Staff ID as an identifier)
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "code": "1",
     *      "msg": "Connection Init",
     *      "channel_code": "POS2/ABC/EFG#XYZ"
     *  }
     */
    public function init_qr(){

        $data = json_decode(file_get_contents('php://input'), true);
        $data['loc_id'] = $data['loc_id'] ?? 'UBI';
        $data['pos_id'] = $data['pos_id'] ?? 'U9';
        $fullcode = md5($data['loc_id'].$data['pos_id']);

        $body['action'] = 'INITIATE';
        $body['code'] = 'POS2/' . strtoupper(substr(trim($data['loc_id']),0,3)) . strtoupper(substr(trim($data['pos_id']),0,3)) . '/'.substr($fullcode,-10);
        $body['channel'] = substr($fullcode,0,16);

        $endpoint = "https://cajb3d5jy5.execute-api.ap-southeast-1.amazonaws.com/prod_v1/chpos-ws";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($body)
        ]);

        $content = $response->getBody()->getContents();
//        $content_data = json_decode($content,true);
//        echo json_encode($content_data);
        echo $content;

    }

    /**
     * @api {get} /qr/check Check QR Login
     * @apiGroup 1.QR LOGIN
     *
     * @apiParam {string} channel_code Channel Code
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "code": "1",
     *      "msg": "Member Login",
     *      "mbr_id": "V1111111ZZ"
     *  }
     */
    public function check_qr(){

        $data = json_decode(file_get_contents('php://input'), true);

        $body['action'] = 'QUERY';
        $code_sep = strpos($data['channel_code'],'#');
        $body['code'] = substr($data['channel_code'],0,$code_sep);
        $body['channel'] = substr($data['channel_code'],$code_sep+1);

        $endpoint = "https://cajb3d5jy5.execute-api.ap-southeast-1.amazonaws.com/prod_v1/chpos-ws";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($body)
        ]);

        $content = $response->getBody()->getContents();
//        $content_data = json_decode($content,true);
//        echo json_encode($content_data);
        echo $content;

    }
}