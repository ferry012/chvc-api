<?php

namespace App\Http\Controllers\Member\Voucher;

use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    use Result;

    private $voucherRepository;

    public function __construct(
        VoucherRepository $voucherRepository
    )
    {
        $this->voucherRepository = $voucherRepository;
    }

    /**
     * @api {get} /api/member/voucher/myvouchers/:fdate/:tdate My Vouchers
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} fdate 2018-01-01, default is last month
     * @apiParam {string} tdate 2018-12-31, default is today
     *
     * @apiSuccessExample Return all voucher data
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "trans_id": "",
     *              "status_level": 1,
     *              "issue_date": "2018-08-08 12:03:06",
     *              "coupon_serialno": "PCNB8-8DQ0WO",
     *              "issue_type": "eVoucher",
     *              "expiry_date": "2018-08-08 12:03:06",
     *              "receipt_id2": "HIA16174",
     *              "mbr_id": "S9222398A",
     *              "status": "Redeemed",
     *              "valid_status": 0,
     *              "qty_reserved": 1
     *          },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Get voucher error
     * HTTP/1.1 Get voucher error
     *  {
     *      "status": "error",
     *      "status_code": 400,
     *      "message": "Something wrong to get voucher"
     *  }
     **/
    public function myVouchers($fdate, $tdate)
    {
        $now = Carbon::now()->startOfDay();
        $fdate = isset($fdate) ? $fdate : $now->subMonth(1);
        $tdate = isset($tdate) ? $tdate : $now;
        $fdate = explode("-", $fdate);
        $fdate = Carbon::create($fdate[0], $fdate[1], $fdate[2], 0, 0, 0);
        $tdate = explode("-", $tdate);
        $tdate = Carbon::create($tdate[0], $tdate[1], $tdate[2], 0, 0, 0);
        $mbrId = trim(request()->user()->mbr_id, " ");
        $data = Voucher::myVouchers($mbrId, $fdate, $tdate);
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('Something wrong to get voucher');
        }

    }


    /**
     * @api {get} /api/member/voucher/myegift/:fdate/:tdate EGift
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} fdate 2018-01-01
     * @apiParam {string} tdate 2018-12-31
     *
     * @apiSuccessExample Return all egift data
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "valid_until": "2018-09-13 23:59:00",
     *              "amount": "$10.00",
     *              "coupon_serialno": "BFB1VE-00025836",
     *              "status": "Expired"
     *          },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Get eGift error
     * HTTP/1.1 Get eGift error
     *  {
     *      "status": "error",
     *      "status_code": 400,
     *      "message": "Something wrong to get eGift"
     *  }
     **/
    public function myeGift($fdate, $tdate)
    {
        $now = Carbon::now()->startOfDay();
        $fdate = isset($fdate) ? $fdate : $now->subMonth(1);
        $tdate = isset($tdate) ? $tdate : $now;
        $fdate = explode("-", $fdate);
        $fdate = Carbon::create($fdate[0], $fdate[1], $fdate[2], 0, 0, 0);
        $tdate = explode("-", $tdate);
        $tdate = Carbon::create($tdate[0], $tdate[1], $tdate[2], 0, 0, 0);
        $mbrId = trim(request()->user()->mbr_id, " ");
        $data = Voucher::myeGift($mbrId, $fdate, $tdate);
        if ($data) {
            return $this->successWithData($data);
        } else {
            return $this->errorWithInfo('Something wrong to get eGift');
        }
    }



    public function myVouchersLists(Request $request)
    {
        $mbr_id = trim(strtoupper($request->user()->mbr_id));

        $fdate = isset($request->fdate) ?  Carbon::parse($request->fdate)->format("Y-m-d") : '';
        $tdate = isset($request->tdate) ? Carbon::parse($request->tdate)->format("Y-m-d") : '';

        if (!$fdate || !$tdate) {
            $fdate = Carbon::now()->modify('-6 months')->format('Y-m-d');
            $tdate = Carbon::now()->modify('+2 years')->format('Y-m-d');
        }

//        echo $fdate;
//        echo $tdate;


        $coy_id = 'CTL';

        $page_from = isset($request->pageno) ? $request->pageno : 0;
        $page_to = ($page_from || $page_from === 0) ? ($page_from + 5) : 9999;

        $vouchers = Voucher::myVouchers($mbr_id, $coy_id, $fdate, $tdate, $page_from, $page_to);

        return response()->json($vouchers, 200);
    }



}
