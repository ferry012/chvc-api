<?php

namespace App\Http\Controllers\Member;

use App\Facades\Member\Voucher\Voucher;
use App\Http\Controllers\Result;
use App\Jobs\ActivateMember;
use App\Jobs\RegisterEmarsysUser;
use App\Jobs\RenewMember;
use App\Jobs\UpgradeMember;
use App\Models\Member\Points\Points;
use App\Models\Member\Transactions\Transaction;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\CorporateListRepository;
use App\Repositories\Member\MemberMetaRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Points\RebateReservedRepository;
use App\Repositories\Member\RebateClaimRepository;
use App\Repositories\Member\SysTransListRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\CreditReservedRepository;
use App\Repositories\Member\Voucher\VoucherIssueDataRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use Carbon\Upgrade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Services\EmarSys\EmarsysServiceV2;

class TransactionController extends Controller
{
    use Result;
    private $memberRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $voucherRepository;
    private $pointsExpiryRepository;
    private $memberTypeRepository;
    private $rebateClaimRepository;
    private $memberMetaRepository;
    private $voucherIssueDataRepository;
    private $addressRepository;
    private $creditReservedRepository;
    private $rebateReservedRepository;
    private $sysTransListRepository;
    private $dbconnMssqlCherps;
    private $emarsysService;

    private $key_code = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                TransactionRepository $transactionRepository,
                                VoucherRepository $voucherRepository,
                                PointsExpiryRepository $pointsExpiryRepository,
                                MemberTypeRepository $memberTypeRepository,
                                RebateClaimRepository $rebateClaimRepository,
                                MemberMetaRepository $memberMetaRepository,
                                AddressRepository $addressRepository,
                                CreditReservedRepository $creditReservedRepository,
                                RebateReservedRepository $rebateReservedRepository,
                                SysTransListRepository $sysTransListRepository,
                                VoucherIssueDataRepository $voucherIssueDataRepository,
                                EmarsysServiceV2 $emarsysService)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->voucherRepository = $voucherRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->rebateClaimRepository = $rebateClaimRepository;
        $this->memberMetaRepository = $memberMetaRepository;
        $this->voucherIssueDataRepository = $voucherIssueDataRepository;
        $this->addressRepository = $addressRepository;
        $this->creditReservedRepository = $creditReservedRepository;
        $this->rebateReservedRepository = $rebateReservedRepository;
        $this->sysTransListRepository = $sysTransListRepository;
        $this->emarsysService = $emarsysService;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    private function getRebateTierFromType($mbr_type) { 
        $check_mbr_type = (substr($mbr_type,0,1) == 'S') ? 'S' : $mbr_type;
        $rebate_table = $this->dbconnMssqlCherps->table('ims_item_list_member')->where('item_type', $check_mbr_type)->first();
        return ($rebate_table) ? $rebate_table->rebate_tier : 0;
    }

    /**
     * @api {post} /api/membership/transaction Save Transaction
     * @apiVersion 1.0.0
     * @apiGroup 2.TRANSACTION
     *
     * @apiParam {string} loc_id Location ID
     * @apiParam {string} [pos_id] Pos ID
     * @apiParam {string} [rebates_earned] Rebates Earned
     * @apiParam {string} invoice_num Invoice Number(transaction id)
     * @apiParam {string} trans_type Transaction Type
     * @apiParam {string} trans_date Transaction Date
     * @apiParam {string} [cashier_id] Cashier ID. Option, default is ''.
     * @apiParam {string} [cashier_name] Cashier Name. Option, default is ''
     * @apiParam {string} [calculate_points] Whether calculate points. Option 'Y' or 'N', default is 'N'
     * @apiParam {Object[]} customer_info Customer Information
     * @apiParam {Object} customer_info.id Customer ID
     * @apiParam {Object[]} items Items
     * @apiParam {Object} items.line_num Items Line Number
     * @apiParam {Object} items.item_id Items ID
     * @apiParam {Object} items.item_desc Items Description
     * @apiParam {Object} items.qty_ordered Items Ordered quantity
     * @apiParam {Object} items.unit_price Items Unit Price
     * @apiParam {Object} [items.unit_discount] Items Unit Discount. Options default 0
     * @apiParam {Object} items.regular_price Items Regular Price
     * @apiParam {Object} items.total_price Items Total Price
     * @apiParam {Object} [items.trans_point] Items Transaction Points. Options default 0
     * @apiParam {Object} [items.mbr_savings] Items Member Savings. Options default 0
     * @apiParam {Object[]} [coupons] Coupons
     * @apiParam {Object} coupons.coupon_id Coupon ID
     * @apiParam {Object} coupons.coupon_name Coupon Name
     * @apiParam {Object} coupons.coupon_code Coupon Code
     * @apiParam {Object} coupons.amount Coupon Amount
     * @apiParam {Object[]} payments Payments
     * @apiParam {Object} payments.pay_mode Payment Mode
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "loc_id": "BF",
     *      "pos_id": "B9",
     *      "rebates_earned": "83.49",
     *      "invoice_num": "B9B00064",
     *      "trans_type": "PS",
     *      "trans_date": "2020-03-31 21:22:44",
     *      "cashier_id": "3810",
     *      "customer_info": {
     *          "id": "V100000100"
     *      },
     *      "items": [
     *      {
     *          "line_num": 1,
     *          "item_id": "!MEMBER-UPG18",
     *          "item_desc": "XXLogitech 920-008617/8 Slim Folio for iPad 9.7",
     *          "qty_ordered": 1,
     *          "unit_price": "18.00",
     *          "unit_discount": "0.00",
     *          "regular_price": "18.00",
     *          "total_price": "18.00",
     *          "mbr_savings": 66,
     *          "trans_point": 77,
     *      },],
     *      "payments": [
     *      {
     *          "pay_mode": "CASH",
     *          "info": "",
     *          "approval_code": null,
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "CARD",
     *          "info": "[UOB_AMEX/*****7522]",
     *          "approval_code": "423910",
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "REBATE",
     *          "info": "",
     *          "approval_code": null,
     *          "trans_amount": "6.68"
     *      },
     *      {
     *          "pay_mode": "EGIFT",
     *          "coupon_serialno": "MEM-OTEPECB"
     *      },
     *      {
     *          "pay_mode": "ECREDIT",
     *          "coupon_serialno": "VCH-CR000000491"
     *      }],
     *      "coupons": [
     *          {
     *              "coupon_id": "!WPHP38INK",
     *              "coupon_name": "$38 off HP Ink Cartridges with min spend $38 ",
     *              "coupon_code": "HP-AIRPVA8D657",
     *              "amount": null
     *          },
     *      ]
     *  }
     *
     * @apiSuccessExample Transaction success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful save transaction",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Reserved rebate error
     * HTTP/1.1 reserved rebate error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, your reserved rebate is overdue or unactivated, please try again"
     *  }
     *
     * @apiErrorExample Transaction error
     * HTTP/1.1 transaction error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Whoops! There was a problem processing your transaction. Please try again."
     *  }
     *
     */
    public function transaction()
    {
        /*
         * Validate
         */
        $input = request()->only(['loc_id', 'pos_id', 'trans_type', 'trans_date', 'invoice_num', 'trans_num', 'cashier_id', 'calculate_points',
            'items', 'customer_info', 'payments', 'rebates_earned', 'total_savings', 'trans_date', 'coupons']);
        $rules = [
            'loc_id' => 'required',
            'trans_date' => 'required',
            'trans_type' => 'required',
            'invoice_num' => 'required',
            'items.*' => 'required',
            'customer_info.id' => 'required',
//            'rebates_earned' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        /*
         * Define variable
         */
        $now = new Carbon($input['trans_date']);
        $coy_id = 'CTL';
        $mbr_id = trim($input['customer_info']['id']);
        $member = $this->memberRepository->findByMbrId($mbr_id);
        if (empty($member)) {
            return $this->errorWithCodeAndInfo(500, "Sorry, member id is invalid.");
        }
        /*
         * Check if calculate_points is Y
         */
        if (isset($input['calculate_points']) && $input['calculate_points'] === 'Y') {
            $points_tier = $this->getRebateTierFromType( $this->memberTypeRepository->getPosMemberType($mbr_id) );
        }
        $loc_id = $input['loc_id'];
        $pos_id = isset($input['pos_id']) ? $input['pos_id'] : " ";
        $trans_type = rtrim($input['trans_type']);
        $trans_id = $input['invoice_num'];
        $inv_num = isset($input['trans_num']) ? rtrim($input['trans_num']) : $trans_id;
        $create_by = isset($input['cashier_id']) ? $input['cashier_id'] : '';
        $payments = $input['payments'];
        $items = $input['items'];
        $coupons = isset($input['coupons']) ? $input['coupons'] : [];
        $reserved_points = 0;
        if ($trans_type === 'DM' || $trans_type === 'DI') {
            if (in_array('REBATE', array_column($payments, "pay_mode"))) {
                $reserved_points = $this->_member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by);
                if (!$reserved_points) {
                    return $this->errorWithInfo("Sorry, your reserved rebate is overdue or unactivated, please try again");
                }
                $points_redeemed = $reserved_points ? $reserved_points : 0;
                $member_redeemed = $points_redeemed;
                $this->_member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, 0, $coy_id, 0, $create_by);
                return $this->successWithInfo("DM or DI type will not process and rebate payment deduct.");
            }
            return $this->successWithInfo("DM or DI type will not process.");
        }

        $this->dbconnMssqlCherps->beginTransaction();
        try {
            /*
             * Save transaction
             */
            $total_points = 0;
            $total_savings = 0;
            foreach ($items as $item) {
                $item_id = $item['item_id'];
                $item_qty = $item['qty_ordered'];
                $salesperson_id = isset($item['salesperson_id']) ? substr($item['salesperson_id'], 0, 15) : '';
                $mbr_savings = isset($item['mbr_savings']) ? number_format((float)str_replace(',', '', $item['mbr_savings']), 2, ".", "") : 0;
                $line_num = $item['line_num'];
                $disc_amount = isset($item['unit_discount']) ? $item['unit_discount'] : 0;
                $item_desc = strlen($item['item_desc']) > 60 ? substr($item['item_desc'],0,60) : $item['item_desc'];
                $unit_price = number_format((float)str_replace(',', '', $item['unit_price']), '2', '.', "");

                if (isset($input['calculate_points']) && $input['calculate_points'] === 'Y') {
                    $item_points = $this->calculate_points($points_tier, $unit_price, $item_qty);
                } else {
                    $item_points = isset($item['trans_point']) ? (float)$item['trans_point'] : 0;
                }

                $total_points = $total_points + $item_points;
                $total_savings = $total_savings + $mbr_savings;
                if (Transaction::where(['mbr_id' => $mbr_id, 'trans_id' => $trans_id, 'coy_id' => 'CTL', 'line_num' => $line_num])->exists()) {
                    return $this->errorWithInfo("Sorry, " . $item_id . ", line number is " . $line_num . " in " . $trans_id . " has been saved.");
                }
                $data_transaction = [
                    'coy_id' => $coy_id,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
                    'trans_type' => $trans_type,
                    'trans_date' => $now,
                    'loc_id' => $loc_id,
                    'line_num' => $line_num,
                    'pos_id' => $pos_id,
                    'item_id' => $item_id,
                    'item_desc' => $item_desc,
                    'item_qty' => $item_qty,
                    'regular_price' => number_format((float)str_replace(',', '', $item['regular_price']), '2', '.', ""),
                    'unit_price' => $unit_price,
                    'disc_percent' => 0,
                    'disc_amount' => $disc_amount,
                    'trans_points' => $item_points,
                    'salesperson_id' => $salesperson_id,
                    'mbr_savings' => $mbr_savings,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->transactionRepository->create($data_transaction);
                /*
                 * Check if member item
                 */
                if (strpos($item_id, '!MEMBER') !== false) {
                    // Create member type record
                    $member_type = $this->dbconnMssqlCherps->table('ims_item_list_member')->where(['item_id' => $item_id, 'status_level' => 1])->first();
                    $mbr_type = trim($member_type->item_type);
                    $member_refund_rebate = $this->memberRepository->findByMbrId($mbr_id);
                    // Check if refund member
                    if ($item_qty < 0) {
                        $rtn_del_mbr = $this->memberTypeRepository->returnMember($mbr_id);
                        $rtn_line_num = $rtn_del_mbr[0];
                        $rtn_trans_date = $rtn_del_mbr[1];
                        $rtn_mbr = $this->dbconnMssqlCherps->table('crm_member_type')->where(['mbr_id' => $mbr_id, 'line_num' => $rtn_line_num - 1]);
                        if (!$rtn_mbr->exists()) {
                            $this->dbconnMssqlCherps->table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                                'status_level' => -1,
                                'exp_date' => $now,
                                'last_renewal' => $now,
                                'modified_on' => $now
                            ]);
                        }
                        else {
                            $rtn_mbr_type = rtrim($rtn_mbr->first()->mbr_type);
                            $rtn_duration_month = $this->dbconnMssqlCherps->table('ims_item_list_member')->where('item_type', $rtn_mbr_type)->first()->month_of_duration;
                            $rtn_eff_from = $rtn_mbr->first()->eff_from;
                            $rtn_eff_to = Carbon::parse($rtn_eff_from)->addMonth($rtn_duration_month);
                            $rtn_mbr->update(['eff_to' => $rtn_eff_to, 'modified_on' => $now]);
                            $rtn_status = $rtn_eff_to->gt(Carbon::now()) ? 1 : -1;

                            $this->dbconnMssqlCherps->table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                                'status_level' => $rtn_status,
                                'exp_date' => $rtn_eff_to,
                                'last_renewal' => $now,
                                'mbr_type' => $rtn_mbr_type,
                                'modified_on' => $now
                            ]);
                        }
                        \App\Models\Member\Voucher\Voucher::whereDate('trans_date', Carbon::parse($rtn_trans_date)->toDateString())->where('coupon_serialno', 'LIKE', 'MEM-'.'%')->delete();
                    }
                    else {
                        if (str_contains($item_id, '!MEMBER-NEW')) {
                            $this->dispatch(new ActivateMember($mbr_type, $mbr_id, $trans_id));
                        } elseif (str_contains($item_id, '!MEMBER-REN')) {
                            $this->dispatch(new RenewMember($mbr_type, $mbr_id, $trans_id));
                        } elseif (str_contains($item_id, '!MEMBER-UPG')) {
                            $this->dispatch(new UpgradeMember($mbr_type, $mbr_id, $trans_id, $loc_id, $pos_id, $salesperson_id));
                        }
                        // Register email with emarsys
//                        $this->emarsysService->registerUser($member_refund_rebate->email_addr);
                        $this->dispatch(new RegisterEmarsysUser($member_refund_rebate->email_addr));

                    }
                }
            }

            /*
             * Save coupon
             */
            foreach ($coupons as $coupon) {
                $coupon_id = $coupon['coupon_id'];
                $coupon_code = $coupon['coupon_code'];
                $coupon_amount = $coupon['amount'];
                if (!is_null($coupon_code)) {
                    $voucher = $this->dbconnMssqlCherps->table('crm_voucher_list')->where([
                        'coy_id' => 'CTL',
                        'coupon_id' => $coupon_id,
                        'coupon_serialno' => $coupon_code,
                        'mbr_id' => $mbr_id
                    ]);
                    if ($voucher->exists()) {
                        $voucher->update([
                            'receipt_id2' => $trans_id,
                            'status_level' => 1,
                            'utilize_date' => $now
                        ]);
                    } else {
                        $this->voucherRepository->create([
                            'coy_id' => 'CTL',
                            'coupon_id' => $coupon_id,
                            'coupon_serialno' => $coupon_code,
                            'promocart_id' => $coupon_id,
                            'mbr_id' => $mbr_id,
                            'trans_id' => '',
                            'trans_date' => $now,
                            'ho_ref' => '',
                            'ho_date' => $now,
                            'issue_date' => $now,
                            'sale_date' => $now,
                            'utilize_date' => $now,
                            'expiry_date' => $now,
                            'print_date' => $now,
                            'loc_id' => $loc_id,
                            'receipt_id1' => $trans_id,
                            'receipt_id2' => $trans_id,
                            'voucher_amount' => $coupon_amount,
                            'redeemed_amount' => 0,
                            'expired_amount' => 0,
                            'issue_type' => '',
                            'issue_reason' => '',
                            'status_level' => 1,
                            'created_by' => $create_by,
                            'created_on' => $now,
                            'modified_by' => $create_by,
                            'modified_on' => $now,
                        ]);
                    }
                }
            }

            /*
             * Update CRM Member Points
             */
            if (in_array('REBATE', array_column($payments, "pay_mode"))) {
                $reserved_points = $this->_member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by);
                if (!$reserved_points) {
                    return $this->errorWithInfo("Sorry, your reserved rebate is overdue or unactivated, please try again");
                }
            }
            $points_redeemed = $reserved_points ? $reserved_points : 0;
            $member_redeemed = $points_redeemed;

            /*
             * Update Ecredits & Egifts
             */
            if (in_array('ECREDIT', array_column($payments, "pay_mode")) || in_array('EGIFT', array_column($payments, "pay_mode")) || in_array('PAYVCH', array_column($payments, "pay_mode"))) {
                foreach ($payments as $p) {
                    $p_mode = $p['pay_mode'];
                    $trans_amount = $p['trans_amount'];
                    if ($trans_amount > 0) {
                        if ($p_mode === 'ECREDIT' || $p_mode === 'EGIFT' || $p_mode == 'PAYVCH') {
                            $coupon_no = isset($p['coupon_serialno']) ? $p['coupon_serialno'] : '';
                            if ($coupon_no === '') {
                                return $this->errorWithCodeAndInfo(500, 'If pay_mode is ECREDIT, EGIFT or PAY_VCH, coupon_serialno required');
                            }
                            $this->creditReservedRepository->unreserve($trans_id);
//                            if (!$res) {
//                                return $this->errorWithCodeAndInfo(500, 'Sorry, You should reserve eGifts of eCredits first.');
//                            }
                            if ($this->voucherRepository->checkGiftExists($coupon_no)) {
                                $this->voucherRepository->updateGift($coupon_no, $trans_id);
                            } else {
                                return $this->errorWithCodeAndInfo(500, 'Sorry, eGifts, eCredits or Voucher invalid.');
                            }
                        }
                    }
                }
            }

            /*
             * Save payment
             */
            if (is_array($input['payments']) && count($input['payments'])>0) {
                $payment_data = [];
                $pdi = 1;
                foreach ($input['payments'] as $payment) {
                    $trans_ref = explode('/', substr($payment['info'], 1, -1));
                    $payment_data[] = [
                        'coy_id' => $coy_id,
                        'mbr_id' => $mbr_id,
                        'trans_id' => $trans_id,
                        'line_num' => $pdi,
                        'pay_mode' => substr($payment['pay_mode'], 0, 60),
                        'trans_ref1' => (isset($trans_ref[0])) ? substr($trans_ref[0], 0, 60) : '',
                        'trans_ref2' => (isset($trans_ref[1])) ? substr($trans_ref[1], 0, 60) : '',
                        'trans_ref3' => (isset($trans_ref[2])) ? substr($trans_ref[2], 0, 60) : '',
                        'trans_amount' => number_format((float)str_replace(',', '', $payment['trans_amount']), '2', '.', ""),
                        'created_by' => $create_by,
                        'created_on' => $now,
                        'modified_by' => $create_by,
                        'modified_on' => $now,
                    ];
                    $pdi++;
                }
                $this->dbconnMssqlCherps->table('crm_member_trans_payment')->insert($payment_data);
            }

            $this->_member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, $total_points, $coy_id, $total_savings, $create_by);
            $this->memberRepository->delete_same_member($member->email_addr, $member->contact_num);
        } catch (\Exception $e) {
            Log::error($e);
            $this->dbconnMssqlCherps->rollBack();
            $info = [
                'id' => $input['invoice_num'],
                'name' => $input['customer_info']['id'],
                'type' => false,
                'desc' => $e,
                'route_name' => 'pos.transaction'
            ];
//            event(new DataOperation($info));
            return $this->errorWithInfo("Whoops! There was a problem processing your transaction. Please try again. (CODE: PCT3222)");
        }
        $this->dbconnMssqlCherps->commit();
        /*
         * Save log
         */
//        $suc_info = [
//            'id' => $trans_id,
//            'name' => $mbr_id,
//            'type' => true,
//            'desc' => $mbr_id . ' successfully save ' . $trans_id . '. And increase ' . $total_points . ' points. At location ' . $pos_id . ' of ' . $loc_id . '. Transaction log is ' . json_encode($input),
//            'route_name' => 'pos.transaction'
//        ];
//        event(new DataOperation($suc_info));
        return $this->successWithInfo("Successful save transaction");
    }

    public function _member_points($member, $points_redeemed, $mbr_id, $now, $member_redeemed, $total_points, $coy_id, $total_savings, $create_by)
    {
        $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
        if (rtrim($member->mbr_type, " ") == 'MAS') {
            if ($points_redeemed > 0) {
                //$redeemed = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                $redeemed = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ?",['CTL', $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                foreach ($redeemed->get() as $k => $v) {
                    if ($points_redeemed > 0) {
                        $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed'], 2, '.', '');
                        if ($remain_point > $points_redeemed) {
                            //Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",['CTL', $mbr_id, $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = 0;
                        } else {
                            //Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",['CTL', $mbr_id, $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = number_format($points_redeemed - $remain_point, 2, '.', '');
                        }
                    }
                }
                $member->increment('points_redeemed', $member_redeemed);
            }
            $main_id = trim($member->main_id);
            //$points = Points::where(['coy_id' => 'CTL', 'mbr_id' => $main_id, 'exp_date' => $exp_date]);
            $points = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and exp_date = ?",['CTL', $main_id, $exp_date]);
            $main_member = $this->memberRepository->findByMbrId($main_id);
            if ($points->exists()) {
                $points->increment('points_accumulated', $total_points, ['modified_by' => $create_by, 'modified_on' => $now]);
            }
            else {
                $data_points = [
                    'coy_id' => $coy_id,
                    'mbr_id' => trim($main_id),
                    'exp_date' => $exp_date,
                    'points_accumulated' => $total_points,
                    'points_redeemed' => $points_redeemed,
                    'points_expired' => 0,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->pointsRepository->create($data_points);
            }
            $main_member->increment('points_accumulated', $total_points);
            $main_member->increment('mbr_savings', $total_savings, ['modified_by' => $create_by, 'modified_on' => $now]);
        }
        else {
            //$points = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
            $points = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and exp_date = ?",['CTL', $mbr_id, $exp_date]);
            if ($points_redeemed > 0) {
                //$redeemed = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                $redeemed = Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ?",['CTL', $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc');
                foreach ($redeemed->get() as $k => $v) {
                    if ($points_redeemed > 0) {
                        $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed'], 2, '.', '');
                        if ($remain_point > $points_redeemed) {
                            //Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",['CTL', $mbr_id, $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = 0;
                        } else {
                            //Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            Points::whereRaw("trim(coy_id) = ? and trim(mbr_id) = ? and line_num = ?",['CTL', $mbr_id, $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                            $points_redeemed = number_format($points_redeemed - $remain_point, 2, '.', '');
                        }
                    }
                }
            }
            if ($points->exists()) {
                $points->increment('points_accumulated', $total_points, ['modified_by' => $create_by, 'modified_on' => $now]);
            } else {
                $data_points = [
                    'coy_id' => $coy_id,
                    'mbr_id' => trim($mbr_id),
                    'exp_date' => $exp_date,
                    'points_accumulated' => $total_points,
                    'points_redeemed' => $points_redeemed,
                    'points_expired' => 0,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ];
                $this->pointsRepository->create($data_points);
            }
            $member->increment('points_accumulated', $total_points);
            $member->increment('points_redeemed', $member_redeemed);
            $member->increment('mbr_savings', $total_savings, ['modified_by' => $create_by, 'modified_on' => $now]);
        }
    }

    public function _member_rd_transaction($payments, $trans_id, $inv_num, $mbr_id, $coy_id, $now, $loc_id, $pos_id, $create_by)
    {
        $rebate_amount = $payments[array_search('REBATE', array_column($payments, "pay_mode"))]['trans_amount'];
        if ($rebate_amount <= 0) {
            $reserved_points = $rebate_amount * 100;
        } else {
            $trans_reserved = $this->rebateReservedRepository->get_rebate_amount($inv_num, $mbr_id);
            if ($trans_reserved) {
                $reserved_points = $trans_reserved->rebate_amount * 100;
                // Delete unreserved transaction
                $this->rebateReservedRepository->unreserve($inv_num);
            } else {
                return false;
            }
        }
        $max_line = Transaction::where('coy_id', 'CTL')->where('mbr_id', $mbr_id)->where('trans_id', $trans_id)
            ->max('line_num');
        $this->transactionRepository->create([
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => 'RD',
            'trans_date' => $now,
            'line_num' => $max_line ? $max_line + 1 : 1,
            'loc_id' => $loc_id,
            'pos_id' => $pos_id,
            'item_id' => '!VCH-STAR001',
            'item_desc' => 'Rebates Redemption',
            'item_qty' => $reserved_points,
            'regular_price' => 0.01,
            'unit_price' => 0.01,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => -1 * $reserved_points,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => $create_by,
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
            'updated_on' => $now,
        ]);
        return $reserved_points;
    }

    public function _associate_member($main_id, $old_type, $new_type, $eff_to, $now, $create_by)
    {
        $eff_from = Carbon::parse($now)->startOfDay();
        $ass_member = $this->memberRepository->get_associate($main_id);
        if (strcasecmp($old_type, $new_type) > 0) {
            $exp_date = $now;
            $this->memberRepository->set_associate_expiry($main_id, $exp_date, $now);
            foreach ($ass_member as $a) {
                $this->memberTypeRepository->update_ass_mbr($a->mbr_id, $exp_date, $now);
            }
        }
        else {
            foreach ($ass_member as $a) {
                $this->memberTypeRepository->update_ass_mbr($a->mbr_id, Carbon::parse($now)->startOfDay()->subSecond(1), $now);
                $this->memberTypeRepository->create([
                    'coy_id' => 'CTL',
                    'mbr_id' => $a->mbr_id,
                    'eff_from' => $eff_from,
                    'eff_to' => $eff_to,
                    'mbr_type' => 'MAS',
                    'created_by' => $main_id,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                ]);
            }
            $this->memberRepository->update_associate($main_id, $eff_to, $now);
        }
    }

    public function calculate_points($points_tier, $unit_price, $item_qty)
    {
        $points = floor($unit_price * (int)$item_qty * (float)$points_tier);
        return $points;
    }

    public function _generate_coupon_no()
    {
        $year = date('y');
        $trans_prefix = 'C' . $year;
        $is_exists = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => 'CTL', 'sys_id' => 'VC', 'trans_prefix' => $trans_prefix])->exists();
        if (!$is_exists) {
            $this->dbconnMssqlCherps->table('sys_trans_list')->insert([
                'coy_id' => 'CTL',
                'sys_id' => 'VC',
                'trans_prefix' => $trans_prefix,
                'next_num' => 0,
                'sys_date' => Carbon::now()]);
        }
        $next_num = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => 'CTL', 'sys_id' => 'VC', 'trans_prefix' => $trans_prefix]);
        $coupon_no = $trans_prefix . sprintf("%012s", $next_num->first()->next_num);
        $next_num->increment('next_num', 1);
        return $coupon_no;
    }

    public function get_transaction_item($trans_id, $item_id)
    {
        $data = Transaction::where(['trans_id'=>$trans_id, 'item_id'=>$item_id])->first();
//        $data = $this->transactionRepository->get_transaction_item($trans_id, $item_id);
        return $this->successWithData($data);
    }

    public function _issue_coupon($mbrId, $mbrType, $transId, $transDate)
    {
        $now = Carbon::now()->toDateTimeString();
        $id = $this->voucherIssueDataRepository->getCouponId($mbrType, $transDate);
        $data = [];
        foreach ($id as $v) {
            if ($mbrType === 'MST') {
                $exp_date = Carbon::today()->endOfDay()->addDays((int)$v->days_to_expiry)->toDateTimeString();
                $transId = '';
            } else {
                $exp_date = Carbon::today()->endOfDay()->addDays((int)$v->days_to_expiry)->toDateTimeString();
            }
            if ($mbrType === 'MST') {
                $is_exists = \App\Models\Member\Voucher\Voucher::where([
                    'mbr_id' => $mbrId,
                    'coupon_id' => $v->coupon_id
                ])->exists();
            } else {
                $is_exists = \App\Models\Member\Voucher\Voucher::where([
                    'mbr_id' => $mbrId,
                    'receipt_id1' => $transId,
                    'coupon_id' => $v->coupon_id
                ])->exists();
            }
            if (!$is_exists) {
                if ($mbrType === 'MST') {
                    $dt_format = Carbon::now()->format('yM');
                    $serial = 'STU' . strtoupper($dt_format) . '-' . strtoupper(Str::Random(6));
                } else {
                    $serial = $this->_quick_random(7);
                }
                $data[] = array(
                    'coy_id' => 'CTL',
                    'coupon_id' => $v->coupon_id,
                    'coupon_serialno' => $serial,
                    'promocart_id' => $v->coupon_id,
                    'mbr_id' => $mbrId,
                    'status_level' => 0,
                    'issue_date' => $now,
                    'sale_date' => $now,
                    'utilize_date' => '1900-01-01 00:00:00',
                    'expiry_date' => $exp_date,
                    'voucher_amount' => 0,
                    'receipt_id1' => $transId,
                    'created_by' => 'hachi',
                    'created_on' => $now,
                    'modified_by' => 'hachi',
                    'modified_on' => $now
                );
            }
        }
        \App\Models\Member\Voucher\Voucher::insert($data);
//        ($data);
        return true;
    }

    public function _quick_random($len)
    {
        do {
            //generate a random string using Laravel's str_random helper
            $serial = strtoupper('MEM-' . Str::Random($len));
        } //check if already exists and if it does, try again
        while (\App\Models\Member\Voucher\Voucher::where('coupon_serialno', $serial)->first());
        return $serial;
    }

    /**
     * @api {get} /api/membership/activate/{mbr_type}/{mbr_id}/{trans_id} Member Activate
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} mbr_type Member Type, should be 08 or 28
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Activate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_activate($mbr_type, $mbr_id, $trans_id)
    {
        $mbr_type = rtrim($mbr_type);
        $mbr_id = rtrim($mbr_id);
        $trans_id = rtrim($trans_id);
        $member = $this->memberRepository->check_by_mbr_id($mbr_id);
        if (!$member) {
            return $this->errorWithInfo("Invalid member");
        }
        if (!in_array($mbr_type, ['08', '28'])) {
            return $this->errorWithInfo("Member type should be 08 or 28");
        }
        if ($this->transactionRepository->check_transaction($trans_id)) {
            return $this->errorWithInfo("Trans_id already exists");
        }
        $item_id = $mbr_type == '08' ? '!MEMBER-NEW08' : '!MEMBER-NEW28';
        $this->_membership($mbr_id, $item_id, $trans_id);
        return $this->successWithInfo("Activate successfully");
    }

    /**
     * @api {get} /api/membership/renew/{mbr_type}/{mbr_id}/{trans_id} Member Renew
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} mbr_type Member Type, should be 08 or 28
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Activate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_renew($mbr_type, $mbr_id, $trans_id)
    {
        $mbr_type = rtrim($mbr_type);
        $mbr_id = rtrim($mbr_id);
        $trans_id = rtrim($trans_id);
        $member = $this->memberRepository->check_by_mbr_id($mbr_id);
        if (!$member) {
            return $this->errorWithInfo("Invalid member");
        }
        if (!in_array($mbr_type, ['08', '28'])) {
            return $this->errorWithInfo("Member type should be 08 or 28");
        }
        if ($this->transactionRepository->check_transaction($trans_id)) {
            return $this->errorWithInfo("Trans_id already exists");
        }
        $item_id = $mbr_type == '08' ? '!MEMBER-REN08' : '!MEMBER-REN28';
        $this->_membership($mbr_id, $item_id, $trans_id);
        return $this->successWithInfo("Renew successfully");
    }

    public function member_upgrade($mbr_type, $mbr_id, $trans_id, $loc_id, $pos_id, $salesperson_id)
    {
        $mbr_type = rtrim($mbr_type);
        $mbr_id = rtrim($mbr_id);
        $trans_id = rtrim($trans_id);
        $member = $this->memberRepository->check_by_mbr_id($mbr_id);
        if (!$member) {
            return $this->errorWithInfo("Invalid member");
        }
        if (!in_array($mbr_type, ['08', '28'])) {
            return $this->errorWithInfo("Member type should be 08 or 28");
        }
        if ($this->transactionRepository->check_transaction($trans_id)) {
            return $this->errorWithInfo("Trans_id already exists");
        }
        $item_id = $mbr_type == '08' ? '!MEMBER-REN08' : '!MEMBER-REN28';
        $this->_membership($mbr_id, $item_id, $trans_id, $loc_id, $pos_id, $salesperson_id);
        return $this->successWithInfo("Upgrade successfully");
    }

    /**
     * @api {get} /api/membership/expire/{mbr_id} Member Deactivate
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} mbr_id Member ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Deactivate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_deactivate($mbr_id)
    {
        $mbr_id = rtrim($mbr_id);
        $member = $this->memberRepository->check_by_mbr_id($mbr_id);
        if (!$member) {
            return $this->errorWithInfo("Invalid member");
        }
        $now = Carbon::now()->toDateTimeString();
        $rtn_del_mbr = $this->memberTypeRepository->returnMember($mbr_id);
        $rtn_line_num = $rtn_del_mbr[0];
        $rtn_trans_date = $rtn_del_mbr[1];
        $rtn_mbr = DB::table('crm_member_type')->where(['mbr_id' => $mbr_id, 'line_num' => $rtn_line_num - 1]);
        if (!$rtn_mbr->exists()) {
            DB::table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                'status_level' => -1,
                'exp_date' => $now,
                'last_renewal' => $now,
                'modified_on' => $now
            ]);
        }
        else {
            $rtn_mbr_type = rtrim($rtn_mbr->first()->mbr_type);
            $rtn_duration_month = DB::table('ims_item_list_member')->where('item_type', $rtn_mbr_type)->first()->month_of_duration;
            $rtn_eff_from = $rtn_mbr->first()->eff_from;
            $rtn_eff_to = Carbon::parse($rtn_eff_from)->addMonth($rtn_duration_month);
            $rtn_mbr->update(['eff_to' => $rtn_eff_to, 'modified_on' => $now]);
            $rtn_status = $rtn_eff_to->gt(Carbon::now()) ? 1 : -1;

            DB::table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                'status_level' => $rtn_status,
                'exp_date' => $rtn_eff_to,
                'last_renewal' => $now,
                'mbr_type' => $rtn_mbr_type,
                'modified_on' => $now
            ]);
        }
        \App\Models\Member\Voucher\Voucher::whereDate('trans_date', Carbon::parse($rtn_trans_date)->toDateString())
            ->where('coupon_serialno', 'LIKE', 'MEM-'.'%')
            ->where('mbr_id', $mbr_id)
            ->delete();
        $trans_id = $this->sysTransListRepository->generate_trans_id();
        $mbr_trans = $this->transactionRepository->get_mbr_trans($mbr_id);
        $data_transaction = [
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => '',
            'trans_date' => $now,
            'loc_id' => '',
            'line_num' => 1,
            'pos_id' => '',
            'item_id' => $mbr_trans->item_id,
            'item_desc' => isset($mbr_trans->item_desc) ? $mbr_trans->item_desc : '',
            'item_qty' => -1,
            'regular_price' => $mbr_trans->regular_price,
            'unit_price' => $mbr_trans->unit_price,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => 0,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => 'VC',
            'created_on' => $now,
            'modified_by' => 'VC',
            'modified_on' => $now,
            'updated_on' => $now,
        ];
        $this->transactionRepository->create($data_transaction);
        return $this->successWithInfo("Deactivate successfully");
    }

    public function _membership($mbr_id, $item_id, $trans_id, $loc_id = '', $pos_id = '', $salesperson_id = '')
    {
        $now = Carbon::now();
        $create_by = 'VC';
        $coy_id = 'CTL';
        $member_type = DB::table('ims_item_list_member')->where(['item_id' => $item_id, 'status_level' => 1])->first();
        $mbr_type = trim($member_type->item_type);
        $member = $this->memberRepository->findByMbrId($mbr_id);

        if ($member_type->is_from_today == 1) {
            $eff_from = $now->toDateString();
        } else {
            $getExpiryDate = $this->memberTypeRepository->getExpiryDate($mbr_id, $now);
            $eff_from = !is_null($getExpiryDate) ? Carbon::parse($getExpiryDate->eff_to)->addSeconds(1)->toDateTimeString() : $now->toDateString();
        }
        $format_eff_from = new Carbon($eff_from);
        $eff_to = $format_eff_from->addMonth($member_type->month_of_duration)->subSeconds(1)->toDateTimeString();
        // Associate member
        if (strpos($item_id, '!MEMBER-NEW') === false && $this->memberRepository->has_associate($mbr_id)) {
            $old_type = rtrim($member->mbr_type);
            $this->_associate_member($mbr_id, $old_type, $mbr_type, $eff_to, $now, $create_by);
        }
        // exec o2o_sp_do_transaction_do_issue_member_coupon
        $this->_issue_coupon($mbr_id, $mbr_type, $trans_id, $now);
        $data_member_type = [
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'eff_from' => $eff_from,
            'eff_to' => $eff_to,
            'mbr_type' => $mbr_type,
            'created_by' => substr($trans_id,0,15),
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
        ];
        $this->memberTypeRepository->create($data_member_type);
        // Update previous record eff_to and refund rebate
        if ($member_type->need_update == 1) {
            $data_update_eff_to = ['eff_to' => Carbon::parse($now)->startOfDay()->subSecond(1)->toDateTimeString(), 'modified_on' => $now];
            // not work
            $ystd = Carbon::parse($now)->subDay(1);
            $refund_rebate = $this->memberTypeRepository->refundRebate($mbr_id, $mbr_type, $member_type->month_of_duration, $ystd->startOfDay(), $data_update_eff_to);
//                            $this->memberTypeRepository->updateEffTo($mbr_id, $data_update_eff_to, $mbr_type, $ystd);
//                            $max_line_mbr = Transaction::where('coy_id', 'CTL')->where('mbr_id', $mbr_id)->where('trans_id', $trans_id)
//                                ->max('line_num');
            if ($refund_rebate != 0) {
                $max_line_mbr = end($items)['line_num'];
                $this->transactionRepository->create([
                    'coy_id' => $coy_id,
                    'mbr_id' => $mbr_id,
                    'trans_id' => $trans_id,
                    'trans_type' => 'AP',
                    'trans_date' => $now,
                    'loc_id' => $loc_id,
                    'line_num' => $max_line_mbr ? $max_line_mbr + 1 : 1,
                    'pos_id' => $pos_id,
                    'item_id' => '',
                    'item_desc' => 'Membership refund via rebate',
                    'item_qty' => $refund_rebate,
                    'regular_price' => 1,
                    'unit_price' => 1,
                    'disc_percent' => 0,
                    'disc_amount' => 0,
                    'trans_points' => $refund_rebate,
                    'salesperson_id' => $salesperson_id,
                    'mbr_savings' => 0,
                    'created_by' => $create_by,
                    'created_on' => $now,
                    'modified_by' => $create_by,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ]);
//                $total_points = $total_points + $refund_rebate;
            }
        }
        /*
         * Update member list expired_date and mbr_type
         */
        $last_renewal = $member->last_renewal;
        $last_expiry = $member->exp_date;
        $claim_entitled = $member->mbr_type;
        $this->memberRepository->update(['status_level' => 1, 'exp_date' => $eff_to, 'mbr_type' => $mbr_type, 'updated_on' => $now, 'last_renewal' => $now], $mbr_id, "mbr_id");
        // Update crm_rebate_claim
        if ($member_type->need_update_claim == 1 && $last_expiry > $now) {
            $this->rebateClaimRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id,
                'last_renewal' => $last_renewal,
                'last_expiry' => $last_expiry,
                'claim_entitled' => 'N',
                'claim_expiry' => Carbon::parse($last_expiry)->addDays(90)->toDateTimeString(),
                'rebate_voucher' => '',
                'mbr_savings' => 0,
                'last_mbrtype' => $claim_entitled,
                'created_by' => $create_by,
                'created_on' => $now,
                'modified_by' => $create_by,
                'modified_on' => $now,
                'updated_on' => $now
            ]);
        }
        $data_transaction = [
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => '',
            'trans_date' => $now,
            'loc_id' => '',
            'line_num' => 1,
            'pos_id' => '',
            'item_id' => $item_id,
            'item_desc' => $member_type->item_desc,
            'item_qty' => 1,
            'regular_price' => $member_type->unit_price,
            'unit_price' => $member_type->unit_price,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => 0,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => $create_by,
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
            'updated_on' => $now,
        ];
        $this->transactionRepository->create($data_transaction);
        $this->dispatch(new RegisterEmarsysUser($member->email_addr));
    }
}
