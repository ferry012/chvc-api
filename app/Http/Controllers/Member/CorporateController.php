<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Mailstream\EmarsysController;
use App\Http\Controllers\Result;
use App\Models\Log\LogWork;
use App\Models\User;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\CorporateListRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\SysTransListRepository;
use App\Services\EmarSys\EmarsysServiceV2;
use App\Services\IMS\Cherps2Service;
use App\Support\Collection;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CorporateController extends Controller
{
    use Result;
    private $memberRepository;
    private $pointsRepository;
    private $memberTypeRepository;
    private $addressRepository;
    private $corporateListRepository;
    private $cherps2Service;
    private $sysTransListRepository;
    private $emarsysServiceV2;
    private $emarsysController;
    private $dbconnMssqlCherps;

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                MemberTypeRepository $memberTypeRepository,
                                AddressRepository $addressRepository,
                                CorporateListRepository $corporateListRepository,
                                SysTransListRepository $sysTransListRepository,
                                EmarsysServiceV2 $emarsysServiceV2,
                                EmarsysController $emarsysController,
                                Cherps2Service $cherps2Service)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->addressRepository = $addressRepository;
        $this->corporateListRepository = $corporateListRepository;
        $this->cherps2Service = $cherps2Service;
        $this->sysTransListRepository = $sysTransListRepository;
        $this->emarsysServiceV2 = $emarsysServiceV2;
        $this->emarsysController = $emarsysController;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    /**
     * @api {post} /api/update_member Update Member
     * @apiVersion 1.0.0
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} [contact] Contact number
     * @apiParam {string} [email] Email address
     * @apiParam {string} [old_pwd] Old Password
     * @apiParam {string} [mbr_pwd] New Password
     * @apiParam {string} [status_level] Status Level, should be 0 or 1.
     * @apiParam {string} [first_name] First name
     * @apiParam {string} [last_name] Last name
     * @apiParam {string} [birth_date] Birth Date
     * @apiParam {string} [sub_ind1] Sub Ind1, 'Y' or 'N'
     * @apiParam {string} [sub_ind2] Sub Ind2, 'Y' or 'N'
     * @apiParam {string} [updated_by] Modified By
     * @apiParam {string} [role] Role, only 'CORP_STAFF' or 'CORP_PURCH'
     * @apiParam {string} [exp_date] Role, only 'CORP'
     * @apiParam {Object[]} [primary_details] Primary Details
     * @apiParam {Object} primary_details.postal_code Postal code
     * @apiParam {Object} primary_details.street_line1 Address line 1
     * @apiParam {Object} primary_details.street_line2 Address line 2
     * @apiParam {Object} primary_details.street_line3 Floor
     * @apiParam {Object} primary_details.street_line4 Unit
     * @apiParam {Object} primary_details.country_id Country Id
     * @apiParam {Object[]} [delivery_details] Delivery Details
     * @apiParam {Object} delivery_details.postal_code Postal code
     * @apiParam {Object} delivery_details.street_line1 Address line 1
     * @apiParam {Object} delivery_details.street_line2 Address line 2
     * @apiParam {Object} delivery_details.street_line3 Floor
     * @apiParam {Object} delivery_details.street_line4 Unit
     * @apiParam {Object} delivery_details.country_id Country Id
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V2100038AS",
     *      "contact": "81431879",
     *      "email": "test@test.com",
     *      "mbr_pwd": "23411",
     *      "first_name": "xx",
     *      "last_name": "xxxxx",
     *      "status_level": 0,
     *      "updated_by": "3811",
     *      "role": "CORP_PURCH"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successfully updated",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample role error
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Associate role must be 'CORP_STAFF' or 'CORP_PURCH'."
     *  }
     *
     * @apiErrorExample role error
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Only CORP_STAFF and CORP_PURCH can change role."
     *  }
     *
     */
    public function update_member()
    {
        $input = request()->only(['mbr_id', 'contact', 'email', 'first_name', 'status_level', 'last_name', 'birth_date', 'old_pwd', 'mbr_pwd', 'role', 'updated_by','exp_date', 'sub_ind1', 'sub_ind2', 'primary_details', 'delivery_details']);
        $rules = [
            'mbr_id' => 'required',
            'contact' => 'numeric|nullable',
            'email' => 'email|nullable',
            'status_level' => ['nullable', Rule::in([0,1])],
            'sub_ind1' => ['nullable', Rule::in(['Y','N'])],
            'sub_ind2' => ['nullable', Rule::in(['Y','N'])],
            'first_name' => 'max:40',
            'last_name' => 'max:40',
            'mbr_pwd' => 'min:4|nullable',
            'old_pwd' => 'min:4|nullable',
            'updated_by' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $this->dbconnMssqlCherps->beginTransaction();
        try {
            $mbr_id = rtrim($input['mbr_id']);
            $member = $this->memberRepository->findByMbrId($mbr_id);
            if ($member === null) {
                return $this->errorWithInfo("Invalid member");
            }
            if (isset($input['contact'])) {
                if (strlen($input['contact'])<8) {
                    return $this->errorWithInfo('Contact number at least 8 digital.');
                }
                if ($this->memberRepository->checkPhoneUnique($mbr_id, $input['contact'])) {
                    return $this->errorWithInfo('Phone number must unique.');
                }
                $contact = $input['contact'];
            } else {
                $contact = $member->contact_num;
            }
            if (isset($input['email'])) {
                if ($this->memberRepository->checkEmailUnique($mbr_id, $input['email'])) {
                    return $this->errorWithInfo('Email address must unique.');
                }
                $email = $input['email'];
            } else {
                $email = $member->email_addr;
            }
            $now = Carbon::now()->toDateTimeString();
            $first_name = isset($input['first_name']) ? $input['first_name'] : $member->first_name;
            $last_name = isset($input['last_name']) ? $input['last_name'] : $member->last_name;
            $birth_date = isset($input['birth_date']) ? $input['birth_date'] : $member->birth_date;
            $mbr_pwd = isset($input['mbr_pwd']) ? md5(rtrim($input['mbr_pwd'])) : $member->mbr_pwd;
            $mbr_pwd2 = isset($input['mbr_pwd']) ? bin2hex(openssl_digest(trim($input['mbr_pwd']), 'sha256',true)): null;
            $sub_ind1 = isset($input['sub_ind1']) ? $input['sub_ind1'] : $member->sub_ind1;
            $sub_ind2 = isset($input['sub_ind2']) ? $input['sub_ind2'].'YYYY' : $member->sub_ind2;
            $mbr_addr = $member->mbr_addr;
            $delv_addr = $member->delv_addr;
            $user = $input['updated_by'];
            $mbr_type = rtrim($member->mbr_type);
            $status_level = $member->status_level;
            $exp_date = $member->exp_date;
            $last_renewal = $member->last_renewal;
            $main_id = rtrim($member->main_id);
            if (isset($input['old_pwd']) && !empty(rtrim($input['old_pwd']))) {
                $old_pwd = $input['old_pwd'];
                if (md5($old_pwd) !== $member->mbr_pwd && bin2hex(openssl_digest(trim($old_pwd), 'sha256',true))!== $member->mbr_pwd2) {
                    return $this->errorWithInfo("Current password is incorrect.");
                }
            }
            if (isset($input['exp_date'])) {
                if ($mbr_type !== 'CORP') {
                    return $this->errorWithInfo("Only Corporate Admin can change exp_date");
                }
                $check_date = Carbon::parse($input['exp_date'])->gte(Carbon::now());
                $status_level = $check_date ? 1 : 0;
                $exp_date = Carbon::parse($input['exp_date'])->toDateTimeString();
                $associate_mbrs = $this->memberRepository->update_corporate_associate($mbr_id, $exp_date, $status_level, $user);
                if ($check_date) {
                    $this->memberTypeRepository->update_member_exp_date($mbr_id, $user, $now);
                    $this->memberTypeRepository->create([
                        'coy_id' => 'CTL',
                        'mbr_id' => $mbr_id,
                        'eff_from' => $now,
                        'eff_to' => $exp_date,
                        'mbr_type' => $mbr_type,
                        'created_by' => $user,
                        'created_on' => $now,
                        'modified_by' => $user,
                        'modified_on' => $now
                    ]);
                } else {
                    $this->memberTypeRepository->update_member_exp_date($mbr_id, $user, $exp_date);
                }
                foreach ($associate_mbrs as $v) {
                    if ($check_date) {
                        $this->memberTypeRepository->update_member_exp_date(rtrim($v['mbr_id']), $user, $now);
                        $this->memberTypeRepository->create([
                            'coy_id' => 'CTL',
                            'mbr_id' => $v['mbr_id'],
                            'eff_from' => $now,
                            'eff_to' => $exp_date,
                            'mbr_type' => $v['mbr_type'],
                            'created_by' => $user,
                            'created_on' => $now,
                            'modified_by' => $user,
                            'modified_on' => $now
                        ]);
                    } else {
                        $this->memberTypeRepository->update_member_exp_date(rtrim($v['mbr_id']), $user, $exp_date);
                    }
                    $this->memberTypeRepository->update_member_exp_date(rtrim($v['mbr_id']), $user, $exp_date);
                }
                $last_renewal = $now;
            }
            if (isset($input['role'])) {
                if (!in_array(rtrim($input['role']), ['CORP_STAFF', 'CORP_PURCH'])) {
                    return $this->errorWithInfo("Associate role must be 'CORP_STAFF' or 'CORP_PURCH'.");
                }
                if (!in_array($mbr_type, ['CORP_STAFF', 'CORP_PURCH'])) {
                    return $this->errorWithInfo("Only CORP_STAFF and CORP_PURCH can change role.");
                }
                $this->memberTypeRepository->update_member_type($mbr_id, $mbr_type, $input['role'], $user);
                $mbr_type = rtrim($input['role']);
            }
            if (isset($input['status_level'])) {
                if (substr($mbr_type, 0, 5) !== 'CORP_') {
                    return $this->errorWithInfo("Only corp staff and purchase member can change status level");
                }
                $new_status_level = $input['status_level'];
                if ($status_level != 1 && $new_status_level == 1) {
                    $main_member_exp_date = $this->memberRepository->findByMbrId($main_id)->exp_date;
                    $status_level = $new_status_level;
                    $exp_date = $main_member_exp_date;
                    $last_renewal = $now;
                    $this->memberTypeRepository->create([
                        'coy_id' => 'CTL',
                        'mbr_id' => $mbr_id,
                        'eff_from' => $now,
                        'eff_to' => $exp_date,
                        'mbr_type' => $mbr_type,
                        'created_by' => $user,
                        'created_on' => $now,
                        'modified_by' => $user,
                        'modified_on' => $now
                    ]);
                }
                if ($status_level == 1 && $new_status_level == 0) {
                    $exp_date = $now;
                    $status_level = -1;
                    $last_renewal = $now;
                    $this->memberTypeRepository->update_member_exp_date($mbr_id, $user, $exp_date);
                }
            }
            if (isset($input['primary_details']) && !empty($input['primary_details'])) {
                $mbr_addr = '0-PRIMARY';
            }
            if (isset($input['delivery_details']) && !empty($input['delivery_details'])) {
                $delv_addr = '2-DELIVERY';
            }
            $this->memberRepository->update([
                'contact_num' => $contact,
                'email_addr'  => $email,
                'first_name'  => $first_name,
                'last_name'   => $last_name,
                'birth_date'  => $birth_date,
                'status_level'=> $status_level,
                'exp_date'    => $exp_date,
                'last_renewal'=> $last_renewal,
                //'mbr_pwd'     => $mbr_pwd,
                'mbr_pwd2'     => $mbr_pwd2,
                'mbr_addr'    => $mbr_addr,
                'delv_addr'   => $delv_addr,
                'mbr_type'    => $mbr_type,
                'sub_ind1'    => $sub_ind1,
                'sub_ind2'    => $sub_ind2,
                'modified_on'  => $now,
                'modified_by'  => $user
            ], $mbr_id, $attribute = 'mbr_id');
            $this->addressRepository->updateMemberInfo($mbr_id, $input);
        } catch (\Exception $e) {
            $this->dbconnMssqlCherps->rollBack();
            return $this->errorWithInfo("Whoops! There was a problem processing your transaction. Please try again.");
        }
        $this->dbconnMssqlCherps->commit();
        return $this->successWithInfo("Successfully updated");
    }


    /**
     * @api {get} /api/corporate/all_cust Customer List
     * @apiGroup 7.CORPORATE
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "coy_id": "CBD",
     *          "cust_id": "AA0007",
     *          "cust_type": "T",
     *          "cust_name": "ASIA ASG LLP",
     *          "cust_addr": "0-PRIMARY",
     *          "email_addr": "",
     *          "tel_code": "90606565",
     *          "mbr_id": "V2100096H4",
     *          "first_name": "TEST",
     *          "last_name": "TEST",
     *          "coy_name": "TEST",
     *          "coy_roc": "DSDFWKJEHER",
     *          "coy_size": "1-30",
     *          "exp_date": "2023-01-01 23:59:59"
     *      },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     */
    public function all_cust(Request $request)
    {

        $skip = $request->get("skip") ?? 0;
        $take = $request->get("take") ?? 20;
        $find = $request->get("find") ?? "";

        $sqlWhere = ($find != "") ? " WHERE " : "";
        $sql = "select trim(m.mbr_id) mbr_id, 
                    c.coy_name,c.coy_roc,c.coy_size,c.cust_id,c.cust_type,c.cust_name,
                        c.contact_num, c.street_line1,c.street_line2, c.country_id,c.postal_code, 
                    m.first_name, m.last_name, m.contact_num mbr_contact_num, m.email_addr mbr_email_addr,
                    m.points_accumulated, m.points_redeemed, m.points_expired, m.points_reserved,
                    m.mbr_type,m.status_level,
                    m.join_date,m.last_renewal,m.exp_date
                from crm_corporate_list c
                join crm_member_list m ON c.coy_id=m.coy_id and c.mbr_id=m.mbr_id $sqlWhere
                order by c.created_on desc limit $take offset $skip";
        $cust = DB::select($sql);
        return $this->successWithData($cust);

//        $register = $this->corporateListRepository->get_all_corp();
//        if (count($register) !== 0) {
//            $custs_id = array(
//                "body" => json_encode([
//                    "cust" => array_column($register->toArray(), 'cust_id')
//                ])
//            );
//            $all = $this->cherps2Service->all_cust($custs_id);
//            dd($all);
//            foreach ($register as $r) {
//                $mbr_id = rtrim($r['mbr_id']);
//                $member = $this->memberRepository->findByMbrId($mbr_id);
//                $key = array_search($r['cust_id'], array_column($all, 'cust_id'));
//                $all[$key]['first_name'] = $member->first_name;
//                $all[$key]['last_name'] = $member->last_name;
//                $all[$key]['email_addr'] = $member->email_addr;
//                $all[$key]['tel_code'] = $member->contact_num;
//                $all[$key]['mbr_id'] = $mbr_id;
//                $all[$key]['coy_name'] = $r['coy_name'];
//                $all[$key]['coy_roc'] = $r['coy_roc'];
//                $all[$key]['coy_size'] = $r['coy_size'];
//                $all[$key]['exp_date'] = $member->exp_date;
////                array_unshift($all, $all[$key]);
////                unset($all[$key]);
//            }
////            $all = array_values($all);
//            return $this->successWithData($all);
//        } else {
//            return $this->successWithData([]);
//        }

    }

    /**
     * @api {post} /api/corporate_register Corporate Register
     * @apiVersion 1.0.0
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} cust_id Cust Id
     * @apiParam {string} [company_name] Company Name
     * @apiParam {string} [company_roc] Company Roc
     * @apiParam {string} [company_size] Company Size
     * @apiParam {string} contact Contact number
     * @apiParam {string} email Email address
     * @apiParam {string} mbr_pwd Password
     * @apiParam {string} [first_name] Option First name
     * @apiParam {string} [last_name] Option Last name
     * @apiParam {string} [birth_date] Option Birth Date
     * @apiParam {string} [created_by] Option Created By
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "cust_id": "CP0016",
     *      "company_name": "TEST",
     *      "company_roc": "DSDFWKJEHER",
     *      "company_size": "1-30",
     *      "contact": "81431879",
     *      "email": "test@test.com",
     *      "first_name": "xx",
     *      "last_name": "xxxxx"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "mbr_id": "V1900001B4",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "join_date": "2018-04-26 11:39:11.28",
     *          "exp_date": "2020-04-26 23:59:59",
     *          "mbr_type": "CORP"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function corporate_register()
    {
        $input = request()->only(['cust_id', 'coy_name','coy_roc','coy_size','contact', 'email', 'first_name', 'last_name', 'birth_date', 'mbr_pwd', 'created_by']);
        $now = Carbon::now()->toDateTimeString();
        $email = rtrim($input['email']);
        $contact = rtrim($input['contact']);
        $member = $this->memberRepository->check_member_exists($email, $contact);
        if (!empty($member)) {
            return $this->errorWithInfo("Membership exists in our system.");
            return $this->successWithData([
                'mbr_id' => rtrim($member->mbr_id),
                'first_name' => $member->first_name,
                'last_name' => $member->last_name,
                'email_addr' => $member->email_addr,
                'contact_num' => $member->contact_num,
                'join_date' => $member->join_date,
                'exp_date' => $member->exp_date,
                'mbr_type' => rtrim($member->mbr_type),
                'status' => $member->status_level,
                'message' => 'Already Registered'
            ]);
        }

        $rules = [
            'contact' => 'required|numeric|min:8|unique:pgsql.crm_member_list,contact_num',
            'email' => 'required|email|unique:pgsql.crm_member_list,email_addr',
            'first_name' => 'max:40',
            'last_name' => 'max:40',
//            'mbr_pwd' => 'required|min:4',
            'cust_id' => 'required',
//            'company_roc' => 'required',
//            'company_size' => 'required'
        ];
        $message = [
            'contact.required' => 'Contact required.',
            'contact.unique' => 'This contact number exists in our system.',
            'contact.numeric' => 'Contact number must be a number.',
            'contact.min' => 'Contact number must consist of minimum 8 digits.',
            'email.required' => 'Email address required.',
            'email.email' => 'Email address must comply with the rules.',
            'email.unique' => 'This email address exists in our system.',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $cust_id = rtrim($input['cust_id']);
        $cust_info = $this->cherps2Service->get_cust($cust_id);
        if (empty($cust_info)){
            return $this->errorWithInfo("Corporate ID invalid.");
        }
        $cust_exists = $this->corporateListRepository->check_cust_exists($cust_id);
        if ($cust_exists) {
            return $this->errorWithInfo("Corporate ID was registered.");
        }

        $source = 'CS';
        $mbr_type = 'CORP';
        $mbr_id = $this->sysTransListRepository->generate_mbr_id($source);
        $default_date = new \DateTime('1900-01-01T00:00:00');
        $endOfDay = Carbon::now()->endOfDay()->addYear()->toDateTimeString();
        $first_name = $input['first_name'] ? $input['first_name'] : '';
        $last_name = $input['last_name'] ? $input['last_name'] : '';
        $birth_date = isset($input['birth_date']) ? Carbon::parse($input['birth_date']) : $default_date;
        $mbr_pwd_raw = isset($input['mbr_pwd']) ? $input['mbr_pwd'] : substr($contact, -4);
        $mbr_pwd = md5($mbr_pwd_raw);
        $company_name = $cust_info['cust_name'];
        $company_roc = isset($input['coy_roc']) ? $input['coy_roc'] : '';
        $company_size = isset($input['coy_size']) ? $input['coy_size'] : '';
        $created_by = isset($input['created_by']) ? $input['created_by'] : 'VCAPI';
        $mbr_addr = '';
        if (!empty(rtrim($cust_info['cust_addr']))) {
            $mbr_addr = rtrim($cust_info['cust_addr']);
            $dd = $this->addressRepository->getCustAddress($cust_id, $mbr_addr);
            $this->addressRepository->create([
                'coy_id'    => 'CTL',
                'ref_type'  => 'MEMBER',
                'addr_type' => $mbr_addr,
                'ref_id'    => $mbr_id,
                'postal_code'=> isset($dd['postal_code']) ? $dd['postal_code'] : '',
                'street_line1'=> isset($dd['street_line1']) ? $dd['street_line1'] : '',
                'street_line2'=> isset($dd['street_line2']) ? $dd['street_line2'] : '',
                'street_line3'=> isset($dd['street_line3']) ? $dd['street_line3'] : '',
                'street_line4'=> isset($dd['street_line4']) ? $dd['street_line4'] : '',
                'country_id'=> isset($dd['country_id']) ? $dd['country_id'] : '',
                'city_name'=> isset($dd['city_name']) ? $dd['city_name'] : '',
                'state_name'=> isset($dd['state_name']) ? $dd['state_name'] : '',
                'addr_format'=> isset($dd['addr_format']) ? $dd['addr_format'] : '',
                'addr_text'=> isset($dd['addr_text']) ? $dd['addr_text'] : '',
                'created_by'=> $created_by,
                'modified_by'=> $created_by,
                'created_on'=> $now,
                'updated_on'=> $now,
                'modified_on'=> $now,
            ]);
        }
        $this->corporateListRepository->saveModel([
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'cust_id' => $cust_id,
            'coy_name' => $company_name,
            'coy_roc' => $company_roc,
            'coy_size' => $company_size,
            'cust_type' => $cust_info['cust_type'],
            'cust_name' => $cust_info['cust_name'],
            'created_by'=> $created_by,
            'modified_by'=> $created_by,
            'created_on' => $now,
            'modified_on' => $now
        ]);
        $this->memberTypeRepository->create([
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'eff_from' => $now,
            'eff_to' => $endOfDay,
            'mbr_type' => $mbr_type,
            'created_by'=> $created_by,
            'modified_by'=> $created_by,
            'created_on' => $now,
            'modified_on' => $now
        ]);
        $res = $this->_register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, 1, $created_by, '', $mbr_addr);
        if ($res) {
            $res_data = [
                'mbr_id' => $mbr_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email_addr' => $email,
                'contact_num' => $contact,
                'join_date' => $now,
                'exp_date' => $endOfDay,
                'mbr_type' => $mbr_type,
                'status' => 1,
                'message' => 'Success'
            ];
            $this->emarsysServiceV2->registerUserEmail($email);
            $dataset = [
                "email" => $email,
                "r_FirstName" => $first_name,
                "r_RegName" => $first_name . " " . $last_name,
                "r_RegEmail" => $email,
                "r_RegPhone" => $contact,
                "r_Password" => $mbr_pwd_raw
            ];
            $this->emarsysController->do_mailstream('CORPORATE_MEMBERSHIP', '', $email, '', 'hachi', $dataset);
            return $this->successWithData($res_data);
        } else {
            return $this->errorWithInfo('Server problem, cannot register member.');
        }
    }

    /**
     * @api {get} /api/latest_register/{main_id} Latest Register
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} main_id Main Member Id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "mbr_id": "966048869",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "mbr_type": "M",
     *          "status": "Activate",
     *          "message": "Already Registered."
     *      },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function latest_register($main_id)
    {
        $main_id = rtrim($main_id);
        $corp_exists = $this->memberRepository->check_corporate_member($main_id);
        if (!$corp_exists) {
            return $this->errorWithInfo("Main Member invalid.");
        }
        $data = LogWork::where(['mbr_id'=>$main_id, 'route_name'=>'pos.corporate_associate_register'])->first();
        if ($data) {
            $data = json_decode($data->desc);
        } else {
            $data = [];
        }
        return $this->successWithData($data);
    }

    /**
     * @api {post} /api/corporate_associate_register Corporate Associate Register
     * @apiVersion 1.0.0
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} main_id Main Member ID
     * @apiParam {Object[]} associate Associate Member
     * @apiParam {Object} associate.contact Contact number
     * @apiParam {Object} associate.email Email address
     * @apiParam {Object} [associate.first_name] Option First name
     * @apiParam {Object} [associate.last_name] Option Last name
     * @apiParam {Object} associate.role Role, 'CORP_STAFF' or 'CORP_PURCH'
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "main_id": "V2100014H4",
     *      "associate": [{
     *          "contact": "81431879",
     *          "email": "test@test.com",
     *          "first_name": "xx",
     *          "last_name": "xxxxx",
     *          "role": "CORP_STAFF"
     *      },]
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "mbr_id": "V1900001B4",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "join_date": "2018-04-26 11:39:11.28",
     *          "exp_date": "2020-04-26 23:59:59",
     *          "mbr_type": "M28",
     *          "status": 1,
     *          "message": "Member Exists" //'Email Exists', 'Contact Exists', 'Success Register', 'Unknown Error'
     *      },],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Main Member Invalid
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Main Member invalid."
     *  }
     *
     */
    public function corporate_associate_register()
    {
        $input = request()->only(['main_id', 'associate']);
        $rules = [
            'main_id' => 'required',
            'associate.*.contact' => 'required|numeric|min:8',
            'associate.*.email' => 'required|email',
            'associate.*.first_name' => 'max:40',
            'associate.*.last_name' => 'max:40',
            'associate.*.role' => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $main_id = rtrim($input['main_id']);
        $corp_exists = $this->memberRepository->check_corporate_member($main_id);
        if (!$corp_exists) {
            return $this->errorWithInfo("Main Member invalid.");
        }
        $now = Carbon::now()->toDateTimeString();
        $default_date = new \DateTime('1900-01-01T00:00:00');
        $associate = $input['associate'];
        $main_mbr = $this->memberRepository->findByMbrId($main_id);
        $endOfDay = $main_mbr->exp_date;
        $email_domain = explode('@',$main_mbr->email_addr)[1];
        $all = [];
        foreach ($associate as $a) {
            $contact = $a['contact'];
            $email = $a['email'];
            $first_name = $a['first_name'] ? $a['first_name'] : '';
            $last_name = $a['last_name'] ? $a['last_name'] : '';
            if (explode('@', $email)[1] !== $email_domain) {
                $all[] = [
                    'mbr_id' => '',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => '',
                    'exp_date' => '',
                    'mbr_type' => '',
                    'status' => 'Error',
                    'message' => 'Not Organization Domain.'
                ];
                continue;
            }
            $mbr_type = $a['role'];
            if (!in_array($mbr_type, ['CORP_STAFF', 'CORP_PURCH'])) {
                return $this->errorWithInfo("Associate role must be 'CORP_STAFF' or 'CORP_PURCH'.");
            }
            $mbr_exists = $this->memberRepository->check_associate_member_exists($email, $contact);
            if ($mbr_exists) {
                $exists_member = $this->memberRepository->findByEmailAndContact($email, $contact);
                $all[] = [
                    'mbr_id' => rtrim($exists_member['mbr_id']),
                    'first_name' => $exists_member['first_name'],
                    'last_name' => $exists_member['last_name'],
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => $exists_member['join_date'],
                    'exp_date' => $exists_member['exp_date'],
                    'mbr_type' => rtrim($exists_member['mbr_type']),
                    'status' => 'Error',
                    'message' => 'Already Registered.'
                ];
                continue;
            }
            $birth_date = isset($a['birth_date']) ? Carbon::parse($a['birth_date']) : $default_date;
            $mbr_pwd_raw = isset($a['mbr_pwd']) ? $a['mbr_pwd'] : substr($contact, -4);
            $mbr_pwd = md5($mbr_pwd_raw);
            $check_email = $this->memberRepository->check_email_unique($email);
            if ($check_email) {
                $all[] = [
                    'mbr_id' => '',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => '',
                    'exp_date' => '',
                    'mbr_type' => '',
                    'status' => 'Error',
                    'message' => 'Email belongs to existing member'
                ];
                continue;
            }
            $check_contact = $this->memberRepository->check_contact_unique($contact);
            if ($check_contact) {
                $all[] = [
                    'mbr_id' => '',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => '',
                    'exp_date' => '',
                    'mbr_type' => '',
                    'status' => 'Error',
                    'message' => 'Contact belongs to existing member'
                ];
                continue;
            }
            $mbr_id = $this->sysTransListRepository->generate_mbr_id('AS');
            $this->memberTypeRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id,
                'eff_from' => $now,
                'eff_to' => $endOfDay,
                'mbr_type' => $mbr_type,
                'modified_by' => $main_id,
                'created_by' => $main_id,
                'created_on' => $now,
                'modified_on' => $now
            ]);
            $res = $this->_register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, 1, $main_id, $main_id);
            if ($res) {
                $all[] = [
                    'mbr_id' => $mbr_id,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => $now,
                    'exp_date' => $endOfDay,
                    'mbr_type' => $mbr_type,
                    'status' => 'Success',
                    'message' => 'Successfully Registered'
                ];
                $this->emarsysServiceV2->registerUserEmail($email);
                $dataset = [
                    "email" => $email,
                    "r_FirstName" => $first_name,
                    "r_RegName" => $first_name . " " . $last_name,
                    "r_RegEmail" => $email,
                    "r_RegPhone" => $contact,
                    "r_Password" => $mbr_pwd_raw
                ];
                $this->emarsysController->do_mailstream('CORPORATE_MEMBERSHIP', '', $email, '', 'hachi', $dataset);
            } else {
                $all[] = [
                    'mbr_id' => '',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email_addr' => $email,
                    'contact_num' => $contact,
                    'join_date' => '',
                    'exp_date' => '',
                    'mbr_type' => '',
                    'status' => 'Error',
                    'message' => 'Unknown Error'
                ];
            }
        }
        array_multisort(array_column($all, 'status'), SORT_ASC, $all);

        $this->_save_log($main_id, $all);
        return $this->successWithData($all);
    }

    /**
     * @api {get} /api/get_corporate_associate/{main_id} Corporate Associate
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} main_id Main Member Id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [{
     *          "mbr_id": "966048869",
     *          "first_name": "xxx",
     *          "last_name": "xxx",
     *          "email_addr": "xxx@xxx.com",
     *          "contact_num": "81431879",
     *          "mbr_type": "M",
     *      },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function get_corporate_associate($main_id)
    {
        $main_id = rtrim($main_id);
        $corp_exists = $this->memberRepository->check_corporate_member($main_id);
        if (!$corp_exists) {
            return $this->errorWithInfo("Main Member invalid.");
        }
        $data = $this->memberRepository->get_corporate_associate($main_id);
        return $this->successWithData($data);
    }

    /**
     * @api {get} /api/corporate/profile/{mbrId} Corporate Profile
     * @apiGroup 7.CORPORATE
     *
     * @apiParam {string} mbrId Member id
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "coy_id": "CTL",
     *          "cust_id": "AA0006",
     *          "cust_type": null,
     *          "cust_name": null,
     *          "tel_code": "787013142",
     *          "email_addr": "test_cole2393@gmail.com",
     *          "mbr_id": "V2100134H4",
     *          "coy_name": "PCCONSULTANCY APAC",
     *          "first_name": "TESasd11",
     *          "last_name": "asdfas",
     *          "coy_name": "ASICS ASIA PTE LTD",
     *          "coy_roc": "DSD22FWKJEHER",
     *          "coy_size": "1-30",
     *          "exp_date": "2022-03-15 23:59:59",
     *          "primary_details": {
     *              "street_line1": "8 Cross Road #24-05",
     *              "street_line2": "PWC Building",
     *              "street_line3": "",
     *              "street_line4": "",
     *              "country_id": "SG",
     *              "postal_code": ""
     *          },
     *          "delivery_details": []
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Member or key code is null
     * HTTP/1.1 Member or key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Member or key code is null"
     *  }
     *
     * @apiErrorExample Can not find member
     * HTTP/1.1 Can not find member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Can not find member"
     *  }
     *
     * @apiErrorExample Sorry, you are not officially member.
     * HTTP/1.1 Sorry, you are not officially member.
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not officially member."
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     */
    public function corporateProfile($mbrId)
    {
        $mbrId = rtrim($mbrId);
        if (empty($mbrId)) {
            return $this->errorWithCodeAndInfo('500', 'Member ID is null.');
        }
        if (strlen(rtrim($mbrId, " ")) < 8) {
            return $this->errorWithCodeAndInfo('500', 'No member found');
        }
        $member = $this->memberRepository->findByMbrId($mbrId);
        if ($member === null) {
            return $this->errorWithInfo("Invalid member");
        }
        $mbr_type = rtrim($member->mbr_type);
        if (!in_array($mbr_type, ['CORP', 'CORP_STAFF', 'CORP_PURCH'])) {
            return $this->errorWithInfo("Only CORP member access this api.");
        }
        if (rtrim($member->main_id) !== '') {
            $mbrId = rtrim($member->main_id);
        }
        $corporate = $this->corporateListRepository->check_mbr_exists($mbrId);
        if (!$corporate) {
            return $this->errorWithInfo("This member is not corporate membership.");
        }
        $primary_addr = [];
        $delivery_addr = [];
        if (!empty(rtrim($member->mbr_addr))) {
            $primary = $this->addressRepository->getAddress($mbrId, $member->mbr_addr);
            $primary_addr = [
                'street_line1' => isset($primary['street_line1']) ? rtrim($primary['street_line1']) : '',
                'street_line2' => isset($primary['street_line2']) ? rtrim($primary['street_line2']) : '',
                'street_line3' => isset($primary['street_line3']) ? rtrim($primary['street_line3']) : '',
                'street_line4' => isset($primary['street_line4']) ? rtrim($primary['street_line4']) : '',
                'country_id'   => isset($primary['country_id']) ? $primary['country_id'] : '',
                'postal_code'  => isset($primary['postal_code']) ? $primary['postal_code'] : ''
            ];
        }
        if (!empty(rtrim($member->delv_addr))) {
            $delivery = $this->addressRepository->getAddress($mbrId, $member->delv_addr);
            $delivery_addr = [
                'street_line1' => isset($delivery['street_line1']) ? rtrim($delivery['street_line1']) : '',
                'street_line2' => isset($delivery['street_line2']) ? rtrim($delivery['street_line2']) : '',
                'street_line3' => isset($delivery['street_line3']) ? rtrim($delivery['street_line3']) : '',
                'street_line4' => isset($delivery['street_line4']) ? rtrim($delivery['street_line4']) : '',
                'country_id'   => isset($delivery['country_id']) ? $delivery['country_id'] : '',
                'postal_code'  => isset($delivery['postal_code']) ? $delivery['postal_code'] : ''
            ];
        }

        if ($corporate) {
            return $this->successWithData([
                'coy_id' => $member->coy_id,
                'cust_id' => $corporate->cust_id,
                'cust_type' => $corporate->cust_type,
                'cust_name' => $corporate->cust_name,
                'tel_code' => $member->contact_num,
                'email_addr' => $member->email_addr,
                'mbr_id' => $mbrId,
                'first_name' => $member->first_name,
                'last_name' => $member->last_name,
                'coy_name' => $corporate->coy_name,
                'coy_roc' => $corporate->coy_roc,
                'coy_size' => $corporate->coy_size,
                'exp_date' => $member->exp_date,
                'primary_details' => $primary_addr,
                'delivery_details' => $delivery_addr
            ]);
        }
        else {
            return $this->successWithData([]);
        }
    }

    public function _status_level($status)
    {
        if ($status == 1) {
            return 'Active';
        } elseif ($status == 0) {
            return 'Inactive';
        } elseif ($status == -1) {
            return 'Expire';
        } elseif ($status == -9) {
            return 'Pre-register';
        } else {
            return $status;
        }
    }

    public function _register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, $status_level, $created_by='', $main_id='', $addr = '')
    {
        $now = Carbon::now()->toDateTimeString();

        $data = [
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'mbr_title' => '',
            'first_name' => $first_name,
            'last_name' => $last_name,
            'birth_date' => $birth_date,
            'nationality_id' => '',
            'email_addr' => trim(strtolower($email)),
            'contact_num' => $contact,
            'mbr_pwd' => $mbr_pwd,
            'pwd_changed' => $default_date,
            'last_login' => $default_date,
            'mbr_addr' => $addr,
            'delv_addr' => '',
            'send_info' => '',
            'send_type' => '',
            'status_level' => $status_level,
            'join_date' => $now,
            'exp_date' => $endOfDay,
            'last_renewal' => $default_date,
            'points_accumulated' => 0,
            'points_reserved' => 0,
            'points_redeemed' => 0,
            'points_expired' => 0,
            'mbr_savings' => 0,
            'rebate_voucher' => '',
            'mbr_type' => $mbr_type,
            'main_id' => $main_id,
            'sub_ind1' => 'Y',
            'sub_ind2' => 'YYYYY',
            'sub_date' => $now,
            'created_by' => $created_by,
            'created_on' => $now,
            'modified_by' => $created_by,
            'modified_on' => $now,
            'updated_on' => $now,
            'login_locked' => 'N'
        ];
        $res = User::insert($data);
        return $res;
    }

    public function _save_log($mbr_id, $all)
    {
        $now = Carbon::now()->toDateTimeString();
        $register_log = LogWork::where(['mbr_id'=>$mbr_id, 'route_name'=>'pos.corporate_associate_register']);
        if ($register_log->exists()) {
            $register_log->update([
                'desc' => json_encode($all),
                'modified_by' => $mbr_id,
                'modified_on' => $now
            ]);
        } else {
            $this->log($mbr_id, 'pos.corporate_associate_register', json_encode($all), $mbr_id);
        }
    }
}