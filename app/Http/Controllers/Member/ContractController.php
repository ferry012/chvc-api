<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Result;

use App\Jobs\RegisterEmarsysUser;
use App\Repositories\Member\Contract\AgreementListRepository;
use App\Repositories\Member\Contract\AgreementTransRepository;
use App\Repositories\Member\Contract\CustomerListRepository;
use App\Repositories\Member\Contract\ItemListRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\RebateClaimRepository;
use App\Repositories\Member\SysTransListRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Services\IMS\Cherps2Service;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;

class ContractController extends Controller
{
    use Result;
    private $posController;
    private $memberRepository;
    private $memberTypeRepository;
    private $transactionRepository;
    private $rebateClaimRepository;
    private $itemListRepository;
    private $agreementTransRepository;
    private $agreementListRepository;
    private $customerListRepository;
    private $sysTransListRepository;
    private $cherps2Service;
    private $dbconnMssqlCherps;

    public function __construct(MemberRepository $memberRepository,
                                MemberTypeRepository $memberTypeRepository,
                                TransactionRepository $transactionRepository,
                                RebateClaimRepository $rebateClaimRepository,
                                PosController $posController,
                                AgreementTransRepository $agreementTransRepository,
                                AgreementListRepository $agreementListRepository,
                                CustomerListRepository $customerListRepository,
                                ItemListRepository $itemListRepository,
                                SysTransListRepository $sysTransListRepository,
                                Cherps2Service $cherps2Service
                                )
    {
        $this->memberRepository = $memberRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->transactionRepository = $transactionRepository;
        $this->rebateClaimRepository = $rebateClaimRepository;
        $this->posController = $posController;
        $this->agreementTransRepository = $agreementTransRepository;
        $this->agreementListRepository = $agreementListRepository;
        $this->customerListRepository = $customerListRepository;
        $this->itemListRepository = $itemListRepository;
        $this->sysTransListRepository = $sysTransListRepository;
        $this->cherps2Service = $cherps2Service;
        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    /**
     * @api {post} /api/contract/customer Create customer
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contact_num Contact number
     * @apiParam {string} email_addr Email address
     * @apiParam {string} first_name First name
     * @apiParam {string} last_name Last name
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "contact": "81431879",
     *      "email": "test@test.com",
     *      "first_name": "xx",
     *      "last_name": "xxxxx"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "cust_id": "xxx",
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function customer_register($data = [])
    {
        if($data)
        {
            $input = $data;
        }
        else
        {
            $input = request()->only(['first_name','last_name','email', 'contact']);
        }

        $now = Carbon::now()->toDateTimeString();
        $email = rtrim($input['email']);
        $contact = rtrim($input['contact']);
        $member = $this->customerListRepository->check_customer_exists($email, $contact);
        if (!empty($member)) {
            return $this->errorWithInfo("Customer exists in our system.");
        }

        $rules = [
            'contact' => 'required|numeric|min:8|unique:pgsql.sls_customer_list,contact_num',
            'email' => 'required|email|unique:pgsql.sls_customer_list,email_addr',
            'first_name' => 'max:40',
            'last_name' => 'max:40',
        ];
        $message = [
            'contact.required' => 'Contact required.',
            'contact.unique' => 'This contact number exists in our system.',
            'contact.numeric' => 'Contact number must be a number.',
            'contact.min' => 'Contact number must consist of minimum 8 digits.',
            'email.required' => 'Email address required.',
            'email.email' => 'Email address must comply with the rules.',
            'email.unique' => 'This email address exists in our system.',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $first_name = $input['first_name'];
        $last_name = $input['last_name'];
        $cust_data = array(
            "body" => json_encode([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'contact_num' => $contact,
                'email_addr' => $email
            ])
        );
        $cust_id = $this->cherps2Service->create_cust($cust_data);
        $mbr_id = $input["mbr_id"] ?: $this->sysTransListRepository->generate_mbr_id("00");
        $this->customerListRepository->create([
            'cust_id' => $cust_id,
            'mbr_id' => $mbr_id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email_addr' => $email,
            'contact_num' => $contact,
            'created_by' => 'VC',
            'created_on' => $now,
            'modified_by' => 'VC',
            'modified_on' => $now
        ]);
        return $this->successWithData(['cust_id'=>$cust_id]);
    }

    /**
     * @api {get} /api/contract/customer/{mbr_id} Get Customer
     * @apiGroup 8.Contract
     *
     * @apiParam {string} mbr_id Cust ID or mbr_id or email_addr or contact_number
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "cust_info": {
     *              "cust_id": "LL0007",
     *              "mbr_id": "V214630300",
     *              "first_name": "leon",
     *              "last_name": "lee",
     *              "email_addr": "li.liangze@challenger.sg",
     *              "contact_num": "81431879",
     *              "created_by": "VC",
     *              "created_on": "2021-10-14 14:48:04",
     *              "modified_by": "VC",
     *              "modified_on": "2021-10-14 14:48:04"
     *           },
     *          "agreement_list": []
     *      }
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     */
    public function get_customer($mbr_id)
    {
        $cust = $this->customerListRepository->get_cust($mbr_id);
        if (is_null($cust)) {
            return $this->errorWithInfo("Customer ID or Member ID or Email or Contact number is invalid");
        }
        $cust_id = $cust->cust_id;
        $agreement = $this->agreementListRepository->get_list($cust_id);
        return $this->successWithData([
            'cust_info' => $cust,
            'agreement_list' => $agreement
        ]);
    }

    /**
     * @api {post} /api/contract/create Create Contract
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} cust_id Customer ID
     * @apiParam {string} item_id Item ID
     * @apiParam {string} contract_start Contract Start
     * @apiParam {string} [contract_end] Default is contract_period
     * @apiParam {string} [contract_amount] Default is item amount
     * @apiParam {string} [cycle_period] Default is item cycle_period
     * @apiParam {string} [ref_id] Default is ''
     * @apiParam {string} [created_by] Default is 'VC'
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "cust_id": "LL0007",
     *      "item_id": "!MEMBER-SUB28",
     *      "contract_start": "2021-10-20 00:00:00",
     *      "ref_sys": "VC"
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "contract_id": "CC2110000010",
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function create_contract($data = [])
    {
        if($data)
        {
            $input = $data;
        }
        else
        {
            $input = request()->only(['cust_id','item_id','contract_start', 'ref_sys', 'contract_end', 'contract_amount', 'cycle_period', 'ref_id', 'created_by']);
        }
        
        $rules = [
            'cust_id' => 'required',
            'item_id' => 'required',
            'contract_start' => 'required',
            'ref_sys' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $cust_id = $input['cust_id'];
        if (!$this->customerListRepository->check_exist($cust_id)) {
            return $this->errorWithInfo("Customer ID invalid");
        }
        $item_id = $input['item_id'];
        $item = $this->itemListRepository->get_item($item_id);
        if (is_null($item)) {
            return $this->errorWithInfo("Item ID invalid");
        }
        $contract_start = $input['contract_start'];
        $ref_sys = $input['ref_sys'];
        $now = Carbon::now()->toDateTimeString();
        //TODO $item->contract_period is -1
        $contract_end = isset($input['contract_end']) ? Carbon::parse($input['contract_end'])->toDateTimeString() : Carbon::parse($contract_start)->addMonths($item->cycle_period);
        $contract_amount = isset($input['contract_amount']) ? $input['contract_amount'] : $item->contract_amount;
        $cycle_period = isset($input['cycle_period']) ? $input['cycle_period'] : $item->cycle_period;
        $ref_id = isset($input['ref_id']) ? $input['ref_id'] : '';
        $created_by = isset($input['created_by']) ? $input['created_by'] : 'VC';
        $contract_id = $this->sysTransListRepository->generate_contract_id();
        $this->agreementListRepository->create([
            'contract_id' => $contract_id,
            'cust_id' => $cust_id,
            'item_id' => $item_id,
            'status_level' => 0,
            'contract_start' => $contract_start,
            'contract_end' => $contract_end,
            'contract_amount' => $contract_amount,
            'cycle_num' => 0,
            'cycle_date' => $contract_start,
            'cycle_period' => $cycle_period,
            'ref_sys' => $ref_sys,
            'ref_id' => $ref_id,
            // TODO
            'item_data' => json_encode([]),
            'contract_data' => json_encode([]),
            'created_by' => $created_by,
            'created_on' => $now,
            'modified_by' => $created_by,
            'modified_on' => $now
        ]);
        $creation_url = $item->creation_url;
        if ($creation_url != "") {
            // TODO
            $this->_vc_member($creation_url, $contract_id);
//            call_user_func('_vc_member', array($creation_url, $contract_id));
        }
        return $this->successWithData(['contract_id' => $contract_id]);
    }

    /**
     * @api {post} /api/contract/pay Pay Contract
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     * @apiParam {string} trans_id Trans ID
     * @apiParam {string} trans_date Trans Date
     * @apiParam {string} trans_amount Trans Amount
     * @apiParam {string} [created_by] Default is 'VC'
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "contract_id": "CC2110000010",
     *      "trans_id": "TEST0001",
     *      "trans_date": "2021-10-20 00:00:00",
     *      "trans_amount": 28
     *  }
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Payment Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function pay_contract($data = [])
    {
        $input = request()->only(['contract_id','trans_id','trans_date', 'trans_amount', 'created_by']);

        if($data)
        {
            if(strlen(trim($data->refNumber)) <= 11)
            {
                $sub_refNumber = substr(trim($data->refNumber), 0, 11);
                $data->refNumber = $sub_refNumber;
            }

            $input = [
                'contract_id' => trim($data->refNumber),
                'trans_id' => $data->tranRef,
                'trans_date' => $data->dateTime,
                'trans_amount' => $data->amt
            ];
        }
        else
        {
            $input = request()->only(['contract_id','trans_id','trans_date', 'trans_amount', 'created_by']);
        }

        $input = (array)$input;

        $rules = [
            'contract_id' => 'required',
            'trans_id' => 'required',
            'trans_date' => 'required',
            'trans_amount' => 'required'
        ];
        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();

            if($data)
            {
                return $errors;
            }
            else
            {
                return $this->errorWithCodeAndInfo(422, $errors);
            }
        }

        $contract_id = rtrim($input['contract_id']);

        $contract = $this->agreementListRepository->get_contract($contract_id);

        if (is_null($contract)) 
        {
            if($data)
            {
                return "Invalid Contract ID";
            }
            else
            {
                return $this->errorWithInfo("Invalid Contract ID");
            }
        }
        else
        {
            $trans_id = $input['trans_id'];

            $trans_date = $input['trans_date'];
            $day = substr($trans_date, 0, 2);
            $month = substr($trans_date, 2, 2);
            $year = substr($trans_date, 4, 2);
            $hour = substr($trans_date, 6, 2);
            $minute = substr($trans_date, 8, 2);
            $second = substr($trans_date, 10, 2);
            $trans_date = '20'.$year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.$second;

            $trans_amount = (float)$input['trans_amount'] / (10 ** 2);
            $user = isset($input['created_by']) ? $input['created_by'] : 'VC';
            $item_id = $contract->item_id;

            $cycle_num = $contract->cycle_num;
            $cycle_date = Carbon::parse($contract->cycle_date)->addMonths($contract->cycle_period);
            
            $result = $this->_pay_contract($contract_id, $cycle_num, $trans_date, $trans_id, $trans_amount, $item_id, $cycle_date, $user);

            if($result == 404)
            {
                if($data)
                {
                    return "Not Found";
                }
                else
                {
                    return $this->errorWithInfo("Not Found");
                }
            }
            else
            {
                $response_url = trim($data->userDefined4);
                $get_contract = $this->get_contract($contract_id, $response_url);

                if($get_contract == "Invalid contract")
                {
                    //Payment made but invalid contract details
                    if($data)
                    {
                        return "Payment made but invalid contract";
                    }
                    else
                    {
                        return $this->errorWithInfo("Payment made but invalid contract");
                    }
                }
                else
                {
                    return $get_contract;
                }
            }
        }  
    }

    public function _pay_contract($contract_id, $cycle_num, $trans_date, $trans_id, $trans_amount, $item_id, $cycle_date, $user)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->agreementTransRepository->create([
            'contract_id' => $contract_id,
            'cycle_num' => $cycle_num,
            'retry_num' => 0,
            'trans_date' => $trans_date,
            'trans_id' => $trans_id,
            'trans_amount' => $trans_amount,
            'custom_data' => json_encode([]),
            'created_by' => $user,
            'created_on' => $now,
            'modified_by' => $user,
            'modified_on' => $now
        ]);
        $new_cycle_num = $cycle_num + 1;
        $this->agreementListRepository->update_pay($contract_id, $new_cycle_num, $cycle_date, $user);
        $item = $this->itemListRepository->get_item($item_id);
        $activation_url = $item->activation_url;
        $renewal_url = $item->renewal_url;
        $extend_url = $contract_id . '/' . $trans_id;

        if ($new_cycle_num == 1 && $activation_url != "") {
            return $this->_vc_member($activation_url, $extend_url);
            //call_user_func('_vc_member', array($activation_url, $extend_url));
        }
        if ($new_cycle_num > 1 && $renewal_url != "") {
            return $this->_vc_member($renewal_url, $extend_url);
            //call_user_func('_vc_member', array($renewal_url, $extend_url));
        }
    }

    /**
     * @api {get} /api/contract/{contract_id}/cancel Cancel Contract
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Cancel Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function cancel_contract($contract_id)
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        $now = Carbon::now()->toDateTimeString();
        // For sls_agreement_list
        $new_status = $contract->status_level == 2 ? 3 : -1;
        $this->agreementListRepository->cancel_contract($contract_id, $new_status);
        // For sls_agreement_trans
        $this->agreementTransRepository->create([
            'contract_id' => $contract_id,
            'cycle_num' => $contract->cycle_num,
            'retry_num' => 0,
            'trans_date' => $now,
            'trans_id' => 'CANCEL',
            'trans_amount' => 0,
            'custom_data' => json_encode([]),
            'created_by' => 'VC',
            'created_on' => $now,
            'modified_by' => 'VC',
            'modified_on' => $now
        ]);
        return $this->successWithInfo("Cancel Successful");
    }

    /**
     * @api {get} /api/contract/{contract_id} Get Contract
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Get Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "list": {
     *              "contract_id": "CC2110000010",
     *              "cust_id": "LL0007",
     *              "item_id": "!MEMBER-SUB28",
     *              "status_level": 2,
     *              "contract_start": "2021-10-20 00:00:00",
     *              "contract_end": "2024-02-20 00:00:00",
     *              "contract_amount": "28.00000000",
     *              "cycle_num": 1,
     *              "cycle_date": "2024-02-20 00:00:00",
     *              "cycle_period": "28.00",
     *              "ref_sys": "VC",
     *              "ref_id": "",
     *              "item_data": "[]",
     *              "contract_data": "[]",
     *              "created_by": "VC",
     *              "created_on": "2021-10-14 14:48:04",
     *              "modified_by": "VC",
     *              "modified_on": "2021-10-14 14:48:04"
     *           },
     *          "trans": [{
     *              "contract_id": "CC2110000010",
     *              "cycle_num": 0,
     *              "retry_num": 0,
     *              "trans_date": "2021-10-20 00:00:00",
     *              "trans_id": "TEST0001",
     *              "trans_amount": "28.00000000",
     *              "custom_data": "[]",
     *              "created_by": "3810",
     *              "created_on": "2021-10-20 15:30:48",
     *              "modified_by": "3810",
     *              "modified_on": "2021-10-20 15:30:48"
     *          }]
     *      }
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     */
    public function get_contract($contract_id = '', $response_url = '')
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);

        if (is_null($contract))
        {
            if(trim($response_url) !== '')
            {
                return "Invalid contract";
            }
            else{
                return $this->errorWithInfo("Invalid contract");
            }
        }
        else
        {
            $trans = $this->agreementTransRepository->get_trans($contract_id);

            if(trim($response_url) !== '')
            {
                $response_data = [
                    'list' => $contract,
                    'trans' => $trans
                ];

                return $response_data;
            }
            else{
                return $this->successWithData([
                    'list' => $contract,
                    'trans' => $trans
                ]);
            }
        }
    }

    /**
     * @api {get} /api/contract/{contract_id}/check Check Contract
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Check Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function check_contract($contract_id)
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }

        $now = Carbon::now()->toDateTimeString();

        // TODO check payment gateway
        //Call epayment inquiry rpp api
        $url = env("EPAYM_URL").'rpp_maintenance';

        $data = [
            "refNumber" => $contract_id,
            "processType" => "I"
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
                
        $result = json_decode($result, TRUE);

        $status = $result['error'];
        $reason = $result['messages']['data'];

        echo "<pre>";
        var_dump($result);
        echo "</pre>";
        die();

        $payment = false;

        if ($payment) {
            $cycle_num = 0;
            $trans_date = $now;
            $trans_id = '';
            $trans_amount = 0;
            $item_id = '';
            $cycle_date = $now;
            $user = 'VC';
            $this->_pay_contract($contract_id, $cycle_num, $trans_date, $trans_id, $trans_amount, $item_id, $cycle_date, $user);
            return $this->successWithInfo("Check payment successful.");
        } else {
            $cycle_num = $contract->cycle_num;
            $item_id = $contract->item_id;
            $item = $this->itemListRepository->get_item($item_id);
            $retry_num = $this->agreementTransRepository->get_max_trans($contract_id, $cycle_num, $item->retry_max);
            if ($retry_num) {
                $this->agreementTransRepository->create([
                    'contract_id' => $contract_id,
                    'cycle_num' => $cycle_num,
                    'retry_num' => $retry_num,
                    'trans_date' => $now,
                    'trans_id' => 'FAILED',
                    'trans_amount' => 0,
                    'custom_data' => json_encode([]),
                    'created_by' => 'VC',
                    'created_on' => $now,
                    'modified_by' => 'VC',
                    'modified_on' => $now
                ]);
            } else {
                $this->agreementListRepository->cancel_contract($contract_id, 4);
                $deactivation_url = $item->deactivation_url;
                if ($deactivation_url != '') {
                    $this->_vc_member($deactivation_url, $contract_id);
//                    call_user_func('_vc_member', array($deactivation_url, $contract_id));
                }
            }
            return $this->successWithInfo("Check no payment");
        }
    }

    /**
     * @api {get} /api/contract/{contract_id}/terminate Terminate Contract
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Terminate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function terminate_contract($contract_id)
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        $status = $contract->status_level;
        if ($status != 3) {
            return $this->errorWithInfo("Contract status_level should be 3");
        }
        $this->agreementListRepository->cancel_contract($contract_id, 4);
        $item = $this->itemListRepository->get_item($contract->item_id);
        $deactivation_url = $item->deactivation_url;

        if ($deactivation_url != '') {
            $result = $this->_vc_member($deactivation_url, $contract_id);

            if($result['status'] != "error")
            {
                //Call epayment cancel rpp api
                
                $url = env("EPAYM_URL").'rpp_maintenance';

                $data = [
                    "refNumber" => $contract_id,
                    "processType" => "C"
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                $result = curl_exec($ch);
                curl_close($ch);
                
                $result = json_decode($result, TRUE);

                $status = $result['error'];
                $reason = $result['messages']['data'];

                if($status == "404")
                {
                    return $this->errorWithInfo("Terminate failed - ".$reason);
                }
                else
                {
                    return $this->successWithInfo("Terminate successfully");
                }
            }
//            call_user_func('_vc_member', array($deactivation_url, $contract_id));
        }
        else
        {
            return $this->errorWithInfo("Terminate failed - no deactivation url");
        }
        //return $this->successWithInfo("Terminate successfully");
    }

    public function _vc_member($action_url, $extend_url)
    {
        $key_code = "?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
        $url = $action_url . $extend_url . $key_code;

        $client = new \GuzzleHttp\Client();
        try {
            $response = [
                'errorCode' => $client->get($url),
                'status' => 'success'
            ];

            return $response;

        } catch (BadResponseException $e) {
            $response = [
                'errorCode' => $e->getCode(),
                'status' => 'error'
            ];

            return $response;
        }
    }

    /**
     * @api {get} /api/contract/partner/valueclub/register/{contract_id} Member Register
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Register Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_register($contract_id)
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        $customer = $this->customerListRepository->get_cust_by_cust($contract->cust_id);
        if (is_null($customer)) {
            return $this->errorWithInfo("Invalid customer");
        }
        $contact = $customer->contact_num;
        $mbr_id = $customer->mbr_id;
        $mbr_pwd = md5(substr($contact, -4));
        $default_date = '1900-01-01 00:00:00';
        if ($this->memberRepository->check_by_mbr_id($mbr_id)) {
            return $this->successWithInfo("Member already exists");
        }
        $this->posController->_register_member($mbr_id, $mbr_pwd, $default_date, Carbon::now()->endOfDay()->toDateTimeString(), $customer->first_name, $customer->last_name, $customer->email_addr, $contact, 'M', $default_date, -9, 'VC', '');
        return $this->successWithInfo("Success register");
    }

    /**
     * @api {get} /api/contract/partner/valueclub/activate/{mbr_type}/{contract_id}/{trans_id} Member Activate
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     * @apiParam {string} mbr_type Member Type, should be 08 or 28
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Activate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_activate($mbr_type, $contract_id, $trans_id)
    {
        $mbr_type = rtrim($mbr_type);
        $contract_id = rtrim($contract_id);
        $trans_id = rtrim($trans_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        if (!in_array($mbr_type, ['06','08', '28'])) {
            return $this->errorWithInfo("Member type should be 06, 08 or 28");
        }
        $customer = $this->customerListRepository->get_cust_by_cust($contract->cust_id);
        if (is_null($customer)) {
            return $this->errorWithInfo("Invalid customer");
        }
        if ($this->transactionRepository->check_transaction($trans_id)) {
            return $this->errorWithInfo("Trans_id already exists");
        }
        $mbr_id = $customer->mbr_id;
        if (!$this->memberRepository->check_by_mbr_id($mbr_id)) {
            $contact = $customer->contact_num;
            $mbr_pwd = md5(substr($contact, -4));
            $default_date = '1900-01-01 00:00:00';
            $this->posController->_register_member($mbr_id, $mbr_pwd, $default_date, Carbon::now()->endOfDay()->toDateTimeString(), $customer->first_name, $customer->last_name, $customer->email_addr, $contact, 'M', $default_date, -9, 'VC', '');
        }
        $item_id = $mbr_type == '08' ? '!MEMBER-SUB08' : '!MEMBER-SUB06';
        $this->_membership($mbr_id, $item_id, $trans_id);
        return $this->successWithInfo("Activate successfully");
    }

    /**
     * @api {get} /api/contract/partner/valueclub/renew/{mbr_type}/{contract_id}/{trans_id} Member Renew
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     * @apiParam {string} mbr_type Member Type, should be 08 or 28
     * @apiParam {string} trans_id Transaction ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Activate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_renew($mbr_type, $contract_id, $trans_id)
    {
        $mbr_type = rtrim($mbr_type);
        $contract_id = rtrim($contract_id);
        $trans_id = rtrim($trans_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        if (!in_array($mbr_type, ['08', '28'])) {
            return $this->errorWithInfo("Member type should be 08 or 28");
        }
        $customer = $this->customerListRepository->get_cust_by_cust($contract->cust_id);
        if (is_null($customer)) {
            return $this->errorWithInfo("Invalid customer");
        }
        if ($this->transactionRepository->check_transaction($trans_id)) {
            return $this->errorWithInfo("Trans_id already exists");
        }
        $mbr_id = $customer->mbr_id;
        $item_id = $mbr_type == '08' ? '!MEMBER-REN08' : '!MEMBER-REN28';
        $this->_membership($mbr_id, $item_id, $trans_id);
        return $this->successWithInfo("Renew successfully");
    }

    /**
     * @api {get} /api/contract/partner/valueclub/expire/{contract_id} Member Deactivate
     * @apiVersion 1.0.0
     * @apiGroup 8.Contract
     *
     * @apiParam {string} contract_id Contract ID
     *
     * @apiSuccessExample Register success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Deactivate Successful",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Key code invalid
     * HTTP/1.1 Key code invalid
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Key code invalid"
     *  }
     *
     * @apiErrorExample Member Server error
     * HTTP/1.1 server error
     *  {
     *      "status": "error",
     *      "status_code": 0500101,
     *      "message": "Server problem, cannot register member."
     *  }
     *
     */
    public function member_deactivate($contract_id)
    {
        $contract_id = rtrim($contract_id);
        $contract = $this->agreementListRepository->get_contract($contract_id);
        if (is_null($contract)) {
            return $this->errorWithInfo("Invalid contract");
        }
        $customer = $this->customerListRepository->get_cust_by_cust($contract->cust_id);
        if (is_null($customer)) {
            return $this->errorWithInfo("Invalid customer");
        }
        $mbr_id = $customer->mbr_id;
        $now = Carbon::now()->toDateTimeString();
        $rtn_del_mbr = $this->memberTypeRepository->returnMember($mbr_id);
        $rtn_line_num = $rtn_del_mbr[0];
        $rtn_trans_date = $rtn_del_mbr[1];
        $rtn_mbr = DB::table('crm_member_type')->where(['mbr_id' => $mbr_id, 'line_num' => $rtn_line_num - 1]);
        if (!$rtn_mbr->exists()) {
            DB::table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                'status_level' => -1,
                'exp_date' => $now,
                'last_renewal' => $now,
                'modified_on' => $now
            ]);
        }
        else {
            $rtn_mbr_type = rtrim($rtn_mbr->first()->mbr_type);
            $rtn_duration_month = DB::table('ims_item_list_member')->where('item_type', $rtn_mbr_type)->first()->month_of_duration;
            $rtn_eff_from = $rtn_mbr->first()->eff_from;
            $rtn_eff_to = Carbon::parse($rtn_eff_from)->addMonth($rtn_duration_month);
            $rtn_mbr->update(['eff_to' => $rtn_eff_to, 'modified_on' => $now]);
            $rtn_status = $rtn_eff_to->gt(Carbon::now()) ? 1 : -1;

            DB::table('crm_member_list')->where('mbr_id', $mbr_id)->update([
                'status_level' => $rtn_status,
                'exp_date' => $rtn_eff_to,
                'last_renewal' => $now,
                'mbr_type' => $rtn_mbr_type,
                'modified_on' => $now
            ]);
        }
        \App\Models\Member\Voucher\Voucher::whereDate('trans_date', Carbon::parse($rtn_trans_date)->toDateString())
            ->where('coupon_serialno', 'LIKE', 'MEM-'.'%')
            ->where('mbr_id', $mbr_id)
            ->delete();
        $trans_id = $this->sysTransListRepository->generate_trans_id();
        $mbr_trans = $this->transactionRepository->get_mbr_trans($mbr_id);
        $data_transaction = [
            'coy_id' => 'CTL',
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => '',
            'trans_date' => $now,
            'loc_id' => '',
            'line_num' => 1,
            'pos_id' => '',
            'item_id' => $mbr_trans->item_id,
            'item_desc' => isset($mbr_trans->item_desc) ? $mbr_trans->item_desc : '',
            'item_qty' => -1,
            'regular_price' => $mbr_trans->regular_price,
            'unit_price' => $mbr_trans->unit_price,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => 0,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => 'VC',
            'created_on' => $now,
            'modified_by' => 'VC',
            'modified_on' => $now,
            'updated_on' => $now,
        ];
        $this->transactionRepository->create($data_transaction);
        return $this->successWithInfo("Deactivate successfully");
    }

    public function update_status($mbr_id, $item_id, $trans_id){
        $this->_membership($mbr_id, $item_id, $trans_id);
        return $this->successWithInfo("Updated successfully");
    }

    public function _membership($mbr_id, $item_id, $trans_id)
    {
        $now = Carbon::now();
        $create_by = 'VC';
        $coy_id = 'CTL';
        $member_type = DB::table('ims_item_list_member')->where(['item_id' => $item_id, 'status_level' => 1])->first();
        $mbr_type = trim($member_type->item_type);
        $member = $this->memberRepository->findByMbrId($mbr_id);

        if ($member_type->is_from_today == 1) {
            $eff_from = $now->toDateString();
        } else {
            $getExpiryDate = $this->memberTypeRepository->getExpiryDate($mbr_id, $now);
            $eff_from = !is_null($getExpiryDate) ? Carbon::parse($getExpiryDate->eff_to)->addSeconds(1)->toDateTimeString() : $now->toDateString();
        }
        $format_eff_from = new Carbon($eff_from);
        $eff_to = $format_eff_from->addMonth($member_type->month_of_duration)->subSeconds(1)->toDateTimeString();
        // Associate member
        if (strpos($item_id, '!MEMBER-NEW') === false && $this->memberRepository->has_associate($mbr_id)) {
            $old_type = rtrim($member->mbr_type);
            $this->posController->_associate_member($mbr_id, $old_type, $mbr_type, $eff_to, $now, $create_by);
        }
        // exec o2o_sp_do_transaction_do_issue_member_coupon
        $this->posController->_issue_coupon($mbr_id, $mbr_type, $trans_id, $now);
        $data_member_type = [
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'eff_from' => $eff_from,
            'eff_to' => $eff_to,
            'mbr_type' => $mbr_type,
            'created_by' => substr($trans_id,0,15),
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
        ];
        $this->memberTypeRepository->create($data_member_type);
        /*
         * Update member list expired_date and mbr_type
         */
        $last_renewal = $member->last_renewal;
        $last_expiry = $member->exp_date;
        $claim_entitled = $member->mbr_type;
        $this->memberRepository->update(['status_level' => 1, 'exp_date' => $eff_to, 'mbr_type' => $mbr_type, 'updated_on' => $now, 'last_renewal' => $now], $mbr_id, "mbr_id");
        // Update crm_rebate_claim
        if ($member_type->need_update_claim == 1 && $last_expiry > $now) {
            $this->rebateClaimRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id,
                'last_renewal' => $last_renewal,
                'last_expiry' => $last_expiry,
                'claim_entitled' => 'N',
                'claim_expiry' => Carbon::parse($last_expiry)->addDays(90)->toDateTimeString(),
                'rebate_voucher' => '',
                'mbr_savings' => 0,
                'last_mbrtype' => $claim_entitled,
                'created_by' => $create_by,
                'created_on' => $now,
                'modified_by' => $create_by,
                'modified_on' => $now,
                'updated_on' => $now
            ]);
        }
        $data_transaction = [
            'coy_id' => $coy_id,
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'trans_type' => '',
            'trans_date' => $now,
            'loc_id' => '',
            'line_num' => 1,
            'pos_id' => '',
            'item_id' => $item_id,
            'item_desc' => $member_type->item_desc,
            'item_qty' => 1,
            'regular_price' => $member_type->unit_price,
            'unit_price' => $member_type->unit_price,
            'disc_percent' => 0,
            'disc_amount' => 0,
            'trans_points' => 0,
            'salesperson_id' => '',
            'mbr_savings' => 0,
            'created_by' => $create_by,
            'created_on' => $now,
            'modified_by' => $create_by,
            'modified_on' => $now,
            'updated_on' => $now,
        ];
        $this->transactionRepository->create($data_transaction);
        $this->dispatch(new RegisterEmarsysUser($member->email_addr));
    }

    public function customer_subscription()
    {
        $post_data = request()->only(['coy_id', 'pay_mode', 'mbr_id', 'item_id', 'first_name', 'last_name', 'email', 'contact', 'callback_url', "trans_id","renew_type"]);

        /*$post_data = [
            'coy_id' => "CTL",
            'pay_mode' => "RPP", //rpp or cc
            'mbr_id' => 'V22000642',
            'item_id' => '!MEMBER-SUB28',
            'first_name' => 'TestABC',
            'last_name' => 'LastABC',
            'email' => 'testabc@hotmail.sg',
            'contact' => '85134503',
            'callback_url' => 'https://chvc-api.sghachi.com/api/vc2/callback_vc_payment',
            'trans_id' => 'TESTtransid'
        ];*/

        $post_data['gateway_name'] = "S2C2P";
        $post_data['pay_mode'] = strtoupper($post_data['pay_mode']);

        $url = env("EPAYM_URL").'epaym_twoctwop';

        //normal cc payment; signup !MEMBER-NEW28; renew !MEMBER-REN28
        if($post_data['pay_mode'] == 'CC' && (trim($post_data['item_id']) == '!MEMBER-NEW28' || trim($post_data['item_id']) == '!MEMBER-REN28'))
        {
            //Trigger 2c2p payment mode
            $post_data['pay_url'] = env("APP_PAY_URL").'/api/contract/process_non_subscription';


            //Trigger Payment - sls_agreement_trans
            $amount = number_format((float)28, 2, '.', '');
            $amount = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);

            $data = [
                "payment_description" => "",
                'member_id' => $post_data['mbr_id'],
                'order_id' => $post_data['trans_id'],
                'amount' => $amount,
                'promoStr' => null,
                'cardTokenStr' => "",
                'storeCard' => "N"
            ];

            $data_extra = [
                "recurring" => "N"
            ];
        }
        else
        {
            $post_data['pay_url'] = env("APP_PAY_URL").'/api/contract/process_subscription';

            //Check if it is existing customer - sls_customer_list
            $check_cust = $this->get_customer($post_data['mbr_id']);
            $check_cust_data = $check_cust->getData();
            $check_cust_status = $check_cust_data->status_code;

            if($check_cust_status == 404)
            {
                //Customer does not exist
                //Create customer - sls_customer_list
                $reg_cust = $this->customer_register($post_data);
                $check_reg_cust = $reg_cust->getData();

                if($check_reg_cust->status_code == 200)
                {
                    //Customer created
                    $cust_id = trim($check_reg_cust->data->cust_id);
                    $post_data['cust_id'] = $cust_id;
                }
                else
                {
                    return $this->errorWithInfo($check_reg_cust->message);
                }
            }

            if (!array_key_exists('cust_id', $post_data)) {
                $post_data['cust_id'] = trim($check_cust_data->data->cust_info->cust_id);
            }

            //Create contract - sls_agreement_list
            $post_data['contract_start'] = Carbon::now()->toDateTimeString();
            $post_data['ref_sys'] = 'VC';

            $contract_result = $this->create_contract($post_data);
            $check_contract_result = $contract_result->getData();

            if($check_contract_result->status_code == 200)
            {
                $contract_id = $check_contract_result->data->contract_id;

                //Get contract details
                $contract_details =$this->get_contract($contract_id);
                $contract_details = $contract_details->getData()->data;
                $contract_info = $contract_details->list;
                $contract_trans = $contract_details->trans;
            }

            if($check_contract_result->status_code == 404)
            {
                $data = [
                    'status'  => 'F',
                    'error_code' => '404',
                    'message' => "Subscription Failed - ".$check_contract_result->message
                ];            
    
                $encrypted_data = base64_encode(json_encode($data));
        
                return redirect()->to($post_data['callback_url'].'?data='.$encrypted_data);
            }

            //Trigger Payment - sls_agreement_trans
            $amount = number_format((float)$contract_info->contract_amount, 2, '.', '');
            $amount = str_pad(str_replace(".", "", $amount), 12, '0', STR_PAD_LEFT);

            $data = [
                "payment_description" => "",
                'member_id' => $contract_info->cust_id,
                'order_id' => $contract_info->contract_id,
                'amount' => $amount,
                'promoStr' => null,
                'cardTokenStr' => "",
                'storeCard' => "N"
            ];

            $data_extra = [
                "recurring" => "Y",
                "invoicePrefix" =>"",
                "recurringAmount" => $amount,
                "allowAccumulate" => "N",
                "maxAccumulateAmt" => "",
                "recurringInterval" => 1, //30 * intval($contract_info->cycle_period),
                "recurringCount" => 0,
                "chargeNextDate" => Carbon::now()->addDays(1)->format('dmY'),//Carbon::now()->addMonths(intval($contract_info->cycle_period))->format('dmY'),
            ];
        }

        $data = array_merge($post_data, $data);
        $data = array_merge($data, $data_extra);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
            
        $result = json_decode($result, TRUE);

        $data = $result['messages']['data'];
        $url = $result['messages']['url'];

        return redirect()->to($url.'?data='.$data);
    }
    
    public function process_subscription()
    {
        $data = json_decode(base64_decode($_GET['data']));

        $paym_status = $data->messages->paym_status;
        $callback_url = $data->messages->callback_url;
        $data = $data->messages->json_response;
        $failReason = trim($data->failReason);

        /*//Payment success
        if($paym_status == 'S')
        {
            $result = $this->pay_contract($data);

            if(is_array($result))
            {
                $contract = $result['list'];
                $trans = $result['trans'];

                return $this->successWithData([
                    'message' => "Payment successful",
                    'list' => $contract,
                    'trans' => $trans
                ]);
            }
            elseif($result == "Invalid Contract ID" || $result == "Not Found" || $result == "Payment made but invalid contract")
            {
                return $this->errorWithInfo($result);
            }
            else
            {
                return $this->errorWithCodeAndInfo(422, $result);
            }
        }
        else
        {
            return $this->errorWithInfo("Payment failed - ".$failReason);
        }*/


        //Payment success
        if($paym_status == 'S')
        {
            $result = $this->pay_contract($data);

            if(is_array($result))
            {
                $contract = $result['list'];
                $trans = $result['trans'];

                $data = [
                    'status'  => 'S',
                    'message' => "Payment successful",
                    'list' => $contract,
                    'trans' => $trans
                ];
            }
            elseif($result == "Invalid Contract ID" || $result == "Not Found" || $result == "Payment made but invalid contract")
            {
                $data = [
                    'status'  => 'F',
                    'error_code' => '404',
                    'message' => "Payment Failed - ".$result,
                ];
            }
            else
            {
                $data = [
                    'status'  => 'F',
                    'error_code' => '422',
                    'message' => "Payment Failed - ".$result
                ];
            }
        }
        else
        {
            $data = [
                'status'  => 'F',
                'error_code' => '422',
                'message' => "Payment Failed - ".$failReason
            ];
        }

        $encrypted_data = base64_encode(json_encode($data));

        return redirect()->to($callback_url.'?data='.$encrypted_data);
    }

    public function process_non_subscription()
    {
        $data = json_decode(base64_decode($_GET['data']));

        $paym_status = $data->messages->paym_status;
        $callback_url = $data->messages->callback_url;
        $data = $data->messages->json_response;
        $failReason = trim($data->failReason);
        //Payment success
        if($paym_status == 'S')
        {
            $data = [
                'status'  => 'S',
                'message' => "Payment successful",
                'response' => $data
            ];
        }
        else
        {
            $data = [
                'status'  => 'F',
                'error_code' => $data->respCode,
                'message' => "Payment Failed - ".$data->failReason
            ];            
        }

        $encrypted_data = base64_encode(json_encode($data));

        return redirect()->to($callback_url.'?data='.$encrypted_data);
    }
}