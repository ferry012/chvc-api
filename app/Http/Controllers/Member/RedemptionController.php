<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Result;
use App\Models\Member\Points\Points;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Redemption\RedemptionRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RedemptionController extends Controller
{
    use Result;
    private $memberRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $voucherRepository;
    private $addressRepository;
    private $redemptionRepository;
    private $pointsExpiryRepository;
    private $dbconnMssqlCherps;

    private $key_code = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                TransactionRepository $transactionRepository,
                                VoucherRepository $voucherRepository,
                                AddressRepository $addressRepository,
                                RedemptionRepository $redemptionRepository,
                                PointsExpiryRepository $pointsExpiryRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->voucherRepository = $voucherRepository;
        $this->addressRepository = $addressRepository;
        $this->redemptionRepository = $redemptionRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    /**
     * @api {post} /api/redemption/items Get Redemption Items
     * @apiGroup 4.REDEMPTION
     *
     * @apiParam {number} min_price Min Price
     * @apiParam {number} max_price Max Price
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "min_price": 20,
     *      "max_price": 40
     *  }
     *
     * @apiSuccessExample Issue Success
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "loc_name": "Bugis Junction (Flagship Store)",
     *              "img_name": "3f0fe9f597602b4a39b3f48e75c356fe.png",
     *              "discount_value": "-70%",
     *              "redeem_price": 29.7,
     *              "coy_id": "CTL",
     *              "line_num": 94,
     *              "item_id": "0745883775293  ",
     *              "item_desc": "Belkin Boostup Wireless Charging Stand 10W (White)",
     *              "points_required": 2970,
     *              "eff_from": "2020-11-04 14:55:43",
     *              "eff_to": "2021-11-04 14:55:43",
     *              "image_name": "3f0fe9f597602b4a39b3f48e75c356fe.png",
     *              "created_by": "ws             ",
     *              "created_on": "2020-11-04 14:55:43",
     *              "modified_by": "               ",
     *              "modified_on": "2020-11-04 14:55:43",
     *              "updated_on": "2020-11-04 14:55:43",
     *              "loc_id": "BF",
     *              "discount_amt": "70%",
     *              "unit_price": 79
     *          },
     *      ],
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample key code
     * HTTP/1.1 key code is null
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "key code is invalid"
     *  }
     *
     */
    public function getRedeemItemLists()
    {
        $input = request()->only('min_price', 'max_price');
        $rules = [
            'min_price' => 'required|numeric',
            'max_price' => 'required|numeric'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        if ($input['min_price'] > $input['max_price']) {
            return $this->errorWithInfo("Min price should less than max price");
        }
        $data = $this->redemptionRepository->getRedemption($input['min_price']*100, $input['max_price']*100);
        return $this->successWithData($data);
    }

    /**
     * @api {post} /api/redemption/transaction Post Redemption
     * @apiGroup 4.REDEMPTION
     *
     * @apiParam {string} mbr_id Member ID
     * @apiParam {string} item_id Item ID
     * @apiParam {string} item_desc Item Desc
     * @apiParam {string} redeem_price Redeem Price
     * @apiParam {unit_price} item_id Unit Price
     * @apiParam {loc_id} item_id Loc ID
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "mbr_id": "V183535400",
     *      "item_id": "8888880156688",
     *      "item_desc": "ValueClub Exclusive Clarity Air True Wireless Earbuds (Grey)",
     *      "redeem_price": "20.97",
     *      "unit_price": "100",
     *      "loc_id": "BF"
     *  }
     *
     * @apiSuccessExample Issue Success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Successful save redemption",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample invalid member
     * HTTP/1.1 invalid member
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, you are not eligible to redeem the item."
     *  }
     *
     * @apiErrorExample Insufficient rebates
     * HTTP/1.1 Insufficient rebates
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Insufficient rebates."
     *  }
     *
     * @apiErrorExample invalid item
     * HTTP/1.1 invalid item
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Sorry, the item is not valid now."
     *  }
     *
     */
    public function doStarRedemptionSummary()
    {
        $input = request()->only('mbr_id', 'item_id', 'item_desc', 'redeem_price', 'unit_price', 'loc_id');
        $rules = [
            'mbr_id' => 'required',
            'item_id' => 'required',
            'item_desc' => 'required',
            'redeem_price' => 'required',
            'unit_price' => 'required',
            'loc_id' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }

        $coy_id = 'CTL';
        $now = Carbon::now();
        $mbr_id = $input["mbr_id"];
        $item_id = trim($input["item_id"]);
        $item_desc = $input["item_desc"];
        $redeem_price = (float)$input["redeem_price"];
        $unit_price = $input["unit_price"];
        $loc_id = $input["loc_id"];

        $pos_id = '0';
        $trans_type = 'OS';
        $create_by = 'VcApp';
        $discount_amt = (int)$unit_price - (int)$redeem_price;
        $trans_points = $redeem_price * (-100);
        $rebates_total = $redeem_price * 100;
        $discount_perc = number_format(($discount_amt / $unit_price) * 100, 2);

        $member = $this->memberRepository->valid_member($mbr_id);
        if (empty($member)) {
            return $this->errorWithInfo("Sorry, you are not eligible to redeem the item.");
        }
        $beginning_rebate = $member->points_remaining;
        if ($beginning_rebate < $redeem_price*100) {
            return $this->errorWithInfo("Insufficient rebates.");
        }
        $first_name = $member["first_name"];
        $last_name = $member["last_name"];
        $cust_name = substr($first_name . ' ' . $last_name, 0, 30);
        $email_addr = $member["email_addr"];
        $tel_code = $member["contact_num"];

        $address = $this->addressRepository->getAddress($mbr_id, '0-PRIMARY');
        $postal_code = $address["postal_code"] ? $address["postal_code"] : '';
        $street_line1 = $address["street_line1"] ? $address["street_line1"] : '';
        $street_line2 = $address["street_line2"] ? $address["street_line2"] : '';
        $street_line3 = $address["street_line3"] ? $address["street_line3"] : '';
        $street_line4 = $address["street_line4"] ? $address["street_line4"] : '';

        $redemption_exists = $this->redemptionRepository->check_redemption($item_id, $rebates_total);
        if (!$redemption_exists) {
            $this->errorWithInfo("Sorry, the item is not valid now.");
        }

        $this->dbconnMssqlCherps->beginTransaction();
        try {
            $trans_id = $this->_generate_trans_id();
            /*
             * Save transaction
             */
            $os_transaction = [
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id,
                'trans_id' => $trans_id,
                'trans_type' => $trans_type,
                'trans_date' => $now,
                'loc_id' => $loc_id,
                'line_num' => 1,
                'pos_id' => $pos_id,
                'item_id' => $item_id,
                'item_desc' => $item_desc,
                'item_qty' => 1,
                'regular_price' => $unit_price,
                'unit_price' => $unit_price,
                'disc_percent' => $discount_perc,
                'disc_amount' => $discount_amt,
                'trans_points' => 0,
                'salesperson_id' => '',
                'mbr_savings' => 0,
                'created_by' => $create_by,
                'created_on' => $now,
                'modified_by' => $create_by,
                'modified_on' => $now,
                'updated_on' => $now,
            ];
            $this->transactionRepository->create($os_transaction);
            $rd_transaction = [
                'coy_id' => $coy_id,
                'mbr_id' => $mbr_id,
                'trans_id' => $trans_id,
                'trans_type' => 'RD',
                'trans_date' => $now,
                'loc_id' => $loc_id,
                'line_num' => 2,
                'pos_id' => $pos_id,
                'item_id' => '!VCH-STAR001',
                'item_desc' => '$0.01 Star Rewards Voucher',
                'item_qty' => 1,
                'regular_price' => $rebates_total,
                'unit_price' => 1,
                'disc_percent' => 1,
                'disc_amount' => 0,
                'trans_points' => $trans_points,
                'salesperson_id' => '',
                'mbr_savings' => 0,
                'created_by' => $create_by,
                'created_on' => $now,
                'modified_by' => $create_by,
                'modified_on' => $now,
                'updated_on' => $now,
            ];
            $this->transactionRepository->create($rd_transaction);
            /*
             * Update CRM Member Points
             */
            $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
            $points_redeemed = $rebates_total;

            $redeemed = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id])->where('exp_date', '>', $now)->orderBy('exp_date', 'asc')->get();
            foreach ($redeemed as $k => $v) {
                if ($points_redeemed > 0) {
                    $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed'], 2, '.', '');
                    if ($remain_point > $points_redeemed) {
                        Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $points_redeemed, ['modified_by' => $create_by, 'modified_on' => $now]);
                        $points_redeemed = 0;
                    } else {
                        Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'line_num' => $v['line_num']])->increment('points_redeemed', $remain_point, ['modified_by' => $create_by, 'modified_on' => $now]);
                        $points_redeemed = number_format($points_redeemed - $remain_point, 2, '.', '');
                    }
                }
            }
            $member->increment('points_redeemed', $rebates_total, ['modified_by' => $create_by, 'modified_on' => $now]);
        } catch (\Exception $e) {
            $this->dbconnMssqlCherps->rollBack();
            return $this->errorWithInfo("Whoops! There was a problem processing your transaction. Please try again.");
        }
        $this->dbconnMssqlCherps->commit();
        $params = array(
            'trans_id' => $trans_id,
            'mbr_id' => $mbr_id,
            'unit_price' => $unit_price,
            'redeem_price' => $redeem_price,
            'discount_amt' => $discount_amt,
            'rebates_total' => $rebates_total,
            'trans_points' => $trans_points,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'tel_code' => $tel_code,
            'email_addr' => $email_addr,
            'cust_name' => $cust_name,
            'postal_code' => $postal_code,
            'street_line1' => $street_line1,
            'street_line2' => $street_line2,
            'street_line3' => $street_line3,
            'street_line4' => $street_line4,
            'item_id' => $item_id,
            'item_desc' => $item_desc,
            'discount_perc' => $discount_perc
        );
        return $this->successWithInfo("Successful save redemption");
    }

    public function _generate_trans_id()
    {
        $year = date('y');
        $chars = 'CDEFGHIJKLMNOPQRSTUVWXYZ';
        $char = substr($chars, (int)$year - 20, 1);
        $trans_prefix = 'INVOS'.$char;
        $is_exists = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => 'CTL', 'sys_id' => 'SMS', 'trans_prefix'=>$trans_prefix])->exists();
        if (!$is_exists) {
            $this->dbconnMssqlCherps->table('sys_trans_list')->insert([
                'coy_id' => 'CTL',
                'sys_id' => 'SMS',
                'trans_prefix'=>$trans_prefix,
                'next_num' => 1,
                'sys_date' => Carbon::now()
            ]);
        }
        $next_num = $this->dbconnMssqlCherps->table('sys_trans_list')->where(['coy_id' => 'CTL', 'sys_id' => 'SMS', 'trans_prefix'=>$trans_prefix]);
        $invoice_id = 'OS'. $char . sprintf("%05s", $next_num->first()->next_num);
        $next_num->increment('next_num', 1);
        return $invoice_id;
    }
}