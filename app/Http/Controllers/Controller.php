<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use Result;


    protected $emarsysMailMapping =
        [
            "associate_invites_v2" => array(
                "mailid" => 5070067,
                "mail_stream_name" => "2019_38_associate_invites_v2"
            ),
            "ctl_password_changed" => array(
                "mailid" => 5070081,
                "mail_stream_name" => "2019_39_ctl_password_changed"
            ),
            "google_play_card" => array(
                "mailid" => 5605539,
                "mail_stream_name" => "2019_40_google_play_card"
            ),
            "VC_Generic_OTP" => array(
                "mailid" => 5070316,
                "mail_stream_name" => "2019_41_VC_Generic_Template"
            ),
            "student_to_valueclub" => array(
                "mailid" => 5070365,
                "mail_stream_name" => "2019_42_student_to_valueclub"
            ),
            "VC_web_reset_password" => array(
                "mailid" => 5070376,
                "mail_stream_name" => "2019_43_VC_web_reset_password"
            ),
            "ctl_forgot_password" => array(
                "mailid" => 5074245,
                "mail_stream_name" => "2019_44_ctl_forgot_password"
            ),
            "product_review_submit" => array(
                "mailid" => 5074250,
                "mail_stream_name" => "2019_45_product_review_submit"
            ),
            "VC_donate_rebate" => array(
                "mailid" => 5074254,
                "mail_stream_name" => "2019_46_VC_donate_rebate"
            ),
            "verification_email" => array(
                "mailid" => 5074266,
                "mail_stream_name" => "2019_47_verification_email"
            ),
            "sender_rebate_transfer" => array(
                "mailid" => 5074287,
                "mail_stream_name" => "2019_48_Sender_rebates_transfer"
            ),
            "recipient_rebate_received" => array(
                "mailid" => 5074290,
                "mail_stream_name" => "2019_49_receipient_rebates_received"
            ),
            "student_unlock_reminder" => array(
                "mailid" => 5074293,
                "mail_stream_name" => "2019_50_student_unlock_reminder"
            ),
            "VC_soon_to_expire" => array(
                "mailid" => 5074297,
                "mail_stream_name" => "2019_51_VC_soon_to_expire"
            ),
            "product_review" => array(
                "mailid" => 5074804,
                "mail_stream_name" => "2019_52_product_review"
            ),
            "Hachi_Samsungdoorlock" => array(
                "mailid" => 5074807,
                "mail_stream_name" => "2019_53_Hachi_Samsungdoorlock"
            ),
            "nsmen_welcome" => array(
                "mailid" => 5074956,
                "mail_stream_name" => "2019_54_nsmen_welcome"
            ),
            "nsmen_verify_email" => array(
                "mailid" => 5074820,
                "mail_stream_name" => "2019_55_nsmen_verify_email"
            ),
            "ctl_profile_updated" => array(
                "mailid" => 5074822,
                "mail_stream_name" => "2019_56_ctl_profile_updated"
            ),
            "ctl_welcome_renew" => array(
                "mailid" => 5074830,
                "mail_stream_name" => "2019_57_ctl_welcome_renew"
            ),
            "associate_invites_reminder" => array(
                "mailid" => 5074835,
                "mail_stream_name" => "2019_58_associate_invites_reminder"
            ),
            "associate_invites" => array(
                "mailid" => 5074839,
                "mail_stream_name" => "2019_59_associate_invites"
            ),
            "updated_profile" => array(
                "mailid" => 4833446,
                "mail_stream_name" => "2019_8_update_profile"
            ),
            "VC_8mths_mbrship" => array(
                "mailid" => 4985423,
                "mail_stream_name" => "2019_29_VC_8mths_mbrship"
            ),
            "8mth_renewal" => array(
                "mailid" => 4985436,
                "mail_stream_name" => "2019_30_8mth_renewal"
            ),
            "VC_18m_28m_welcome" => array(
                "mailid" => 5010658,
                "mail_stream_name" => "2019_32_VC_18m_28m_welcome"
            ),
            "18_28_mth_renewal" => array(
                "mailid" => 5010659,
                "mail_stream_name" => "2019_33_18_28_mth_renewal"
            ),
            "terminate_associate_mbrs" => array(
                "mailid" => 5010660,
                "mail_stream_name" => "2019_34_terminate_associate_mbrs"
            ),
            "VC_expire_8mth_tier" => array(
                "mailid" => 5029320,
                "mail_stream_name" => "2019_35_VC_expire_8mth_tier"
            ),
            "VC_18mth_to_expire" => array(
                "mailid" => 5029348,
                "mail_stream_name" => "2019_36_VC_18mth_to_expire"
            ),
            "VC_28mth_to_expire" => array(
                "mailid" => 5029338,
                "mail_stream_name" => "2019_37_VC_28mth_to_expire"
            ),
            "change_password" => array(
                "mailid" => 4833443,
                "mail_stream_name" => "change_password"
            ),
            "upgrade_membership" => array(
                "mailid" => 5176453,
                "mail_stream_name" => "2019_ValueClub_membership_Upgrade"
            ),
            "low_ink_reminder" => array(
                "mailid" => 5583982,
                "mail_stream_name" => "low_ink_reminder"
            ),
            "update_stu_membership" => array(
                "mailid" => 5523980,
                "mail_stream_name" => "update_stu_membership"
            ),
            "cease_membership" => [
                "mailid" => 5629637,
                "mail_stream_name" => "cease_membership",
            ],
            "password_reset_mobile" => [
                "mailid" => 5648988,
                "mail_stream_name" => "password_reset_mobile"
            ],
            "reset_password_otp" => [
                "mailid" => 5690249,
                "mail_stream_name" => "reset_password_otp"
            ],
            "hp_not_subscribe1" => [
                "mailid" => 5965695,
                "mail_stream_name" => "hp_not_subscribe1"
            ],
            "hp_not_subscribe2" => [
                "mailid" => 5947052,
                "mail_stream_name" => "hp_not_subscribe2"
            ],
            "hp_activation_new" => [
                "mailid" => 6589911,
                "mail_stream_name" => "hp_activation_new"
            ],
            "refer_new_mst_friend" => [
                "mailid" => 6094094,
                "mail_stream_name" => "refer_new_mst_friend"
            ],
            "upgrade_student_plus_success" => [
                "mailid" => 6086797,
                "mail_stream_name" => "upgrade_student_plus_success"
            ],
            "email_pcnb_campaign" => [
                "mailid" => 6671530,
                "mail_stream_name" => "email_pcnb_campaign"
            ],
            "hp_subscribe_eligi_printer" => [
                "mailid" => 7038708,
                "mail_stream_name" => "hp_subscribe_eligi_printer"
            ],
            "hp_utilize_38_voucher" => [
                "mailid" => 7081382,
                "mail_stream_name" => "hp_utilize_38_voucher"
            ],
            "hp_utilize_88_voucher" => [
                "mailid" => 7081454,
                "mail_stream_name" => "hp_utilize_88_voucher"
            ],
            "wishlist2020_tier1"=> [
                "mailid" => 8357412,
                "mail_stream_name" => "wishlist2020_tier1"
            ],
            "wishlist2020_tier2"=> [
                "mailid" => 8368300,
                "mail_stream_name" => "wishlist2020_tier2"
            ],
            "wishlist2020_tier3"=> [
                "mailid" => 8368607,
                "mail_stream_name" => "wishlist2020_tier3"
            ],

        ];



}
