<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Result;
use App\Http\Proxy\TokenProxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    protected $proxy;

    public function __construct(TokenProxy $proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * @api {post} /api/resetpwd User Reset Password
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} old_password
     * @apiParam {string} new_password
     *
     * @apiSuccessExample {json} Reset password success
     * HTTP/1.1 200 OK
     *  [{
     *      "status": "Reset password success",
     *      "status_code": 200,
     *      "message": "A new password has been generated"
     *  }]
     *
     * @apiErrorExample {json} Validator failed
     * HTTP/1.1 423 Validator failed
     *  [{
     *      "status": "Reset password failed",
     *      "status_code": 100301,
     *      "message": "The new password must be at least 8 characters."
     *  }]
     *
     * @apiErrorExample {json} Password incorrect
     * HTTP/1.1 423 Old password incorrect
     *  [{
     *      "status": "Reset password failed",
     *      "status_code": 100302,
     *      "message": "The old password is incorrect."
     *  }]
     *
     * @apiErrorExample {json} Server issue
     * HTTP/1.1 423 Server issue
     *  [{
     *      "status": "Reset password failed",
     *      "status_code": 100303,
     *      "message": "There was an error saving your new password."
     *  }]
     */
    public function resetPassword(Request $request){
        $messages = [
            'required' => ':attribute can not be empty!',
            'email' => 'Please input the correct username',
            'min:8'  => 'Please set a password greater than 8 digits',
        ];
        $validator = Validator::make($request->all(),[
            'old_password' => 'required|min:8',
            'new_password' => 'required|min:8',
        ],$messages);
        if($validator->fails()){
            $errors = $validator->errors()->first();
            return response()->json(['status'=>'Reset Password Failed', 'status_code'=>100301,'message'=>$errors ]);
        }
        return $this->proxy->resetPassword(request('old_password'),request('new_password'));
    }
}
