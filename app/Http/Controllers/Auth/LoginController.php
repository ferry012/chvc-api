<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Proxy\TokenProxy;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $proxy;



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TokenProxy $proxy)
    {
        $this->middleware('guest')->except('logout');
        $this->proxy = $proxy;
    }

    /**
     * @api {post} /api/login User Login
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} member_login Optional member id, contact number or email address
     * @apiParam {string} member_pwd
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "member_login": "xxxxxxxxx",
     *      "member_pwd": "123456",
     *  }
     *
     * @apiSuccessExample Login success
     * HTTP/1.1 200 OK
     *  {
     *      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJS",
     *      "refresh_token": "def502004210b06920de6c4140a",
     *      "expires_in": "2020-12-18 13:00:49", // Expire time
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Login fail
     * HTTP/1.1 421 Username or password is wrong
     *  {
     *      "status": "login error",
     *      "status_code": 100101,
     *      "message": "Username or password is wrong"
     *  }
     *
     * @apiErrorExample Login locked
     * HTTP/1.1 421 Login locked
     *  {
     *      "status": "login error",
     *      "status_code": 100102,
     *      "message": "Login locked"
     *  }
     *
     * @apiErrorExample Member forbid
     * HTTP/1.1 421 Member forbid
     *  {
     *      "status": "login error",
     *      "status_code": 100103,
     *      "message": "Member type is forbid"
     *  }
     *
     * @apiErrorExample Member inactivated
     * HTTP/1.1 421 Member inactivated
     *  {
     *      "status": "login error",
     *      "status_code": 100104,
     *      "message": "Member status is inactivated"
     *  }
     *
     * @apiErrorExample Invalid User
     * HTTP/1.1 421 Invalid User
     *  {
     *      "status": "login error",
     *      "status_code": 100105,
     *      "message": "Invalid User"
     *  }
     *
     * @apiErrorExample Duplicate User
     * HTTP/1.1 421 Duplicate User
     *  {
     *      "status": "login error",
     *      "status_code": 100106,
     *      "message": "Duplicate User"
     *  }
     *
     * @apiErrorExample Member Expiry
     * HTTP/1.1 421 Member Expiry
     *  {
     *      "status": "login error",
     *      "status_code": 100107,
     *      "message": "Member Expiry"
     *  }
     */

    public function login()
    {
        $user = request('user');
        $password = request('password');

        $response= $this->proxy->login($user, $password);




        return $response;
    }

//    private function login($user, $password){
//        $response= $this->proxy->login($user, $password);
//
//        return $response;
//    }

    /**
     * @api {post} /api/logout User Logout
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     *
     * @apiSuccessExample Logout success
     * HTTP/1.1 200 OK
     *  {
     *      "status": "success",
     *      "status_code": 200,
     *      "message": "logout success"
     *  }
     *
     * @apiErrorExample Logout error
     * HTTP/1.1 404 Logout error
     *  {
     *      "status": "login error",
     *      "status_code": 100201,
     *      "message": "You do not login."
     *  }
     *
     */
    public function logout()
    {
        return $this->proxy->logout();
    }

    /**
     * @api {post} /api/token/refresh Token Refresh
     * @apiVersion 1.0.0
     * @apiGroup VC-APP
     *
     * @apiParam {string} refresh_token Refresh Token
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "refresh_token": "xxxxxxxxx" // No Need Auth Header
     *  }
     *
     * @apiSuccessExample Refresh success
     * HTTP/1.1 200 OK
     *  {
     *      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJS",
     *      "refresh_token": "def502004210b06920de6c4140a",
     *      "expires_in": "2020-12-18 13:00:49", // Expire time
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Refresh fail
     * HTTP/1.1 401 No auth
     *  {
     *      "status": "login error",
     *      "status_code": 100401,
     *      "message": "Credentials not match"
     *  }
     */
    public function refresh()
    {
        $token = request('refresh_token');
        return $this->proxy->refresh($token);
    }
}