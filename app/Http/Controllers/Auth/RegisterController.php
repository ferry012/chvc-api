<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Result;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Repositories\Member\MemberRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use Result;
    private $memberRepository;
    private $dbconnMssqlCherps;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MemberRepository $memberRepository)
    {
//        $this->middleware('guest');
        $this->memberRepository = $memberRepository;

        $this->dbconnMssqlCherps = DB::connection('pgsql');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function associate_register()
    {
        $input = request()->only(['contact', 'email', 'first_name', 'last_name', 'birth_date', 'mbr_pwd']);
        $contact = $input['contact'];
        $email = $input['email'];
        $member = $this->memberRepository->check_associate_member_exists($email, $contact);
        if (!empty($member)) {
            if ($member === 2) {
                return $this->errorWithInfo("Pending associate member activation for this email (" . trim($email) . ").");
            } else if ($member === 3) {
                return $this->errorWithInfo("You're already a member.");
            }
        }
        $rules = [
            'contact' => 'required|numeric|min:8|unique:pgsql.crm_member_list,contact_num',
            'email' => 'required|email|unique:pgsql.crm_member_list,email_addr',
            'first_name' => 'max:40',
            'last_name' => 'max:40',
        ];
        $message = [
            'contact.required' => 'Contact required.',
            'contact.unique' => 'This contact number exists in our system.',
            'contact.numeric' => 'Contact number must be a number.',
            'contact.min' => 'Contact number must consist of minimum 8 digits.',
            'email.required' => 'Email address required.',
            'email.email' => 'Email address must comply with the rules.',
            'email.unique' => 'This email address exists in our system.',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator);
            return $this->errorWithCodeAndInfo(422, $errors);
        }
        $now = Carbon::now()->toDateTimeString();
        $main_id = rtrim(request()->user()->mbr_id);
        $main_mbr = $this->memberRepository->findByMbrId($main_id);
        if (empty($main_mbr)) {
            return $this->errorWithInfo("Main Member invalid.");
        }
        $main_mbr_type = rtrim($main_mbr->mbr_type);
        $member_item = DB::connection('pgsql')->table('ims_item_list_member')->where(['item_id' => $main_mbr_type, 'status_level' => 1])->first();
        $associate_limit = $member_item->associate_limit;
        $associate_count = $this->memberRepository->count_associate($main_id);
        if ($associate_limit === 0) {
            return $this->errorWithInfo("Membership of '".$member_item->item_desc."' is unable to access this feature.");
        }
        if ($associate_limit < $associate_count) {
            return $this->errorWithInfo("Sorry. Associate Members cannot more than " . (string)$associate_limit);
        }

        $mbr_id = $this->_generate_mbr_id('AS');
        $default_date = new \DateTime('1900-01-01T00:00:00');
//        $endOfDay = Carbon::now()->endOfDay()->addYear(1)->toDateTimeString();
        $endOfDay = $main_mbr->exp_date;
        $first_name = $input['first_name'] ? $input['first_name'] : '';
        $last_name = $input['last_name'] ? $input['last_name'] : '';
        $mbr_type = 'MAS';
        $birth_date = isset($input['birth_date']) ? Carbon::parse($input['birth_date']) : $default_date;
        $mbr_pwd = isset($input['mbr_pwd']) ? md5($input['mbr_pwd']) : md5(substr($contact, -4));
        $res = $this->_register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, 0, 'VcLogin', $main_id);
        if ($res) {
            $res_data = [
                'mbr_id' => $mbr_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email_addr' => $email,
                'contact_num' => $contact,
                'join_date' => $now,
                'exp_date' => $endOfDay,
                'mbr_type' => $mbr_type
            ];
            return $this->successWithData($res_data);
        } else {
            return $this->errorWithInfo('Server problem, cannot register member.');
        }
    }

    public function _register_member($mbr_id, $mbr_pwd, $default_date, $endOfDay, $first_name, $last_name, $email, $contact, $mbr_type, $birth_date, $status_level, $created_by='', $main_id='')
    {
        $now = Carbon::now()->toDateTimeString();

        $data = [];
        $data['coy_id'] = 'CTL';
        $data['mbr_id'] = $mbr_id;
        $data['mbr_title'] = '';
        $data['first_name'] = $first_name;
        $data['last_name'] = $last_name;
        $data['birth_date'] = $birth_date;
        $data['nationality_id'] = '';
        $data['email_addr'] = trim(strtolower($email));
        $data['contact_num'] = $contact;
        $data['mbr_pwd'] = $mbr_pwd;
        $data['pwd_changed'] = $default_date;
        $data['last_login'] = $default_date;
        $data['mbr_addr'] = '';
        $data['delv_addr'] = '';
        $data['send_info'] = '';
        $data['send_type'] = '';
        $data['status_level'] = $status_level;
        $data['join_date'] = $now;
        $data['exp_date'] = $endOfDay;
        $data['last_renewal'] = $default_date;
        $data['points_accumulated'] = 0;
        $data['points_reserved'] = 0;
        $data['points_redeemed'] = 0;
        $data['points_expired'] = 0;
        $data['mbr_savings'] = 0;
        $data['rebate_voucher'] = '';
        $data['mbr_type'] = $mbr_type;
        $data['main_id'] = $main_id;
        $data['sub_ind1'] = 'Y';
        $data['sub_ind2'] = 'YYYYY';
        $data['sub_date'] = $now;
        $data['created_by'] = $created_by;
        $data['created_on'] = $now;
        $data['modified_by'] = $created_by;
        $data['modified_on'] = $now;
        $data['updated_on'] = $now;
        $data['login_locked'] = 'N';
        $res = $this->memberRepository->saveModel($data);
        return $res;
    }

    public function _generate_mbr_id($source)
    {
        $coy_id = 'CTL';
        $sys_id = 'CRM';
        $doc_type = 'MEMBER';
        $mbr = $this->dbconnMssqlCherps->select('exec [sp_SysNewDocId] @CoyId = ?, @SysId=?, @DocType=?,@DocPrefix=?, @DocDate=?, @DocId=?, @isPrint =?', array($coy_id, $sys_id, $doc_type, $source, '', '', 1));
        return $mbr[0]->doc_id;
    }


}
