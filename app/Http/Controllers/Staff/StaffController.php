<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Result;
use App\Models\Member\MemberType;
use App\Models\User;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    use Result;

    private $memberRepository;
    private $memberTypeRepository;
    private $key_code = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';

    public function __construct(MemberRepository $memberRepository, MemberTypeRepository $memberTypeRepository){
        $this->memberRepository = $memberRepository;
        $this->memberTypeRepository = $memberTypeRepository;
    }

    /**
     * @api {post} /api/staff/convert/{mbrId}/{staffId} Convert Staff
     * @apiVersion 1.0.0
     * @apiGroup 7.STAFF
     *
     * @apiParam {string} mbrId member ID, Email Addr or Contact Number
     * @apiParam {string} StaffId staff ID
     *
     * @apiSuccessExample Convert success
     * HTTP/1.1 200 OK
     *  {
     *      "message": "Success convert",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Convert error
     * HTTP/1.1
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Error update member type."
     *  }
     *
     */
    public function convert($mbrId, $staffId)
    {
        $now = Carbon::now()->toDateTimeString();
        $eff_from = Carbon::today()->startOfDay()->toDateTimeString();
        $eff_to = Carbon::now()->addYear(3)->endOfYear()->toDateTimeString();
        $member = $this->memberRepository->convert_staff($mbrId, $staffId, $eff_to);
        if (!empty($member)) {
            $mbr_id = rtrim($member->mbr_id);
            $line_num = MemberType::where('mbr_id', $mbr_id)->max('line_num');
            if ($line_num) {
                $next_line = $line_num + 1;
                $member_type = MemberType::where(['mbr_id'=>$mbr_id,'line_num'=>$line_num])->first();
                $date_to = $member_type->eff_to;
                if (Carbon::now()->lt(Carbon::parse($date_to))) {
                    $this->memberTypeRepository->update_eff_to($mbr_id, $line_num, $now);
                }
            } else {
                $next_line = 1;
            }

            $re = $this->memberTypeRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id,
                'line_num' => $next_line,
                'eff_from' => $eff_from,
                'eff_to' => $eff_to,
                'mbr_type' => 'S'.$staffId,
                'created_by' => 'CONVERT',
                'created_on' => $now,
                'modified_by' => 'CONVERT',
                'modified_on' => $now,
            ]);
            if ($re) {
                return $this->successWithData('Success convert.');
            } else {
                return $this->errorWithCodeAndInfo('500', 'Error update member type.');
            }
        } else {
            return $this->errorWithCodeAndInfo('500', 'Error update member list.');
        }
    }

    /**
     * @api {post} /api/staff/terminate/{mbrId}/{staffId} Terminate Staff
     * @apiVersion 1.0.0
     * @apiGroup 7.STAFF
     *
     * @apiParam {string} mbrId member ID, Email Addr or Contact Number
     * @apiParam {string} StaffId staff ID
     *
     * @apiSuccessExample Terminate success
     * HTTP/1.1 200 OK
     *  {
     *      "message": "Success terminate",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Terminate error
     * HTTP/1.1
     *  {
     *      "status": "error",
     *      "status_code": 500,
     *      "message": "Error terminate."
     *  }
     *
     */
    public function terminate($mbrId, $staffId)
    {
        $now = Carbon::now();
        $res = User::orWhere('mbr_id', $mbrId)->orWhere('contact_num', $mbrId)->orWhere('email_addr', $mbrId)->where('mbr_type', 'S'.$staffId)->where('status_level', '<>', -1)->update([
            'status_level' => -1,
            'exp_date' => $now
        ]);
        if ($res) {
            return $this->successWithData('Success terminate.');
        } else {
            return $this->errorWithCodeAndInfo('500', 'Error terminate.');
        }
    }

}
