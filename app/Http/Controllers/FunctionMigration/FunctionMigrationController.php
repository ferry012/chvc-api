<?php

namespace App\Http\Controllers\FunctionMigration;

use App\Http\Controllers\Result;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\RebateClaimRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FunctionMigrationController extends Controller
{
    use Result;
    private $memberRepository;
    private $pointsRepository;
    private $voucherRepository;
    private $pointsExpiryRepository;
    private $memberTypeRepository;
    private $rebateClaimRepository;
    private $transactionRepository;
    private $key_code = 'iOiJKV1QiLCwWSJhbGciOiJSUz';

    public function __construct(MemberRepository $memberRepository,
                                PointsRepository $pointsRepository,
                                VoucherRepository $voucherRepository,
                                PointsExpiryRepository $pointsExpiryRepository,
                                MemberTypeRepository $memberTypeRepository,
                                RebateClaimRepository $rebateClaimRepository,
                                TransactionRepository $transactionRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
        $this->voucherRepository = $voucherRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->rebateClaimRepository = $rebateClaimRepository;
        $this->transactionRepository = $transactionRepository;
        set_time_limit(8000000);
    }

    public function check_date($date)
    {
        if ($date == ''){
            $from = Carbon::yesterday()->toDateString();
            $to = Carbon::today()->toDateString();
        } else {
            $from = Carbon::parse($date)->toDateString();
            $to = Carbon::parse($date)->addDay(1)->toDateString();
        }
        $datetime = array($from,$to);
        return $datetime;
    }

    public function setExpired($date = '')
    {
        $datetime = $this->check_date($date);
        $res = $this->memberRepository->setExpired($datetime[0], $datetime[1]);
        if ($res) {
            return $this->successWithInfo('Success set member status expired');
        } else {
            return $this->errorWithInfo('Error set member status expired.');
        }
    }

    public function deleteNoPay($date = '')
    {
        $datetime = $this->check_date($date);
        $res = $this->memberRepository->deleteNoPay($datetime[0], $datetime[1]);
        if ($res == 1) {
            return $this->successWithInfo('Success delete member not pay');
        } else {
            return $this->successWithInfo('Find no member not pay.');
        }
    }

    public function rebateClaimRenewed($date = '')
    {
        $datetime = $this->check_date($date);

        $rebates = $this->rebateClaimRepository->getRebateClaim($datetime[0],$datetime[1]);
        foreach ($rebates as $r) {
            $mbr_id = rtrim($r->mbr_id);
            $member = $this->memberRepository->findByMbrId($mbr_id);
            $mbr_savings = isset($member->mbr_savings) ? $member->mbr_savings : 100;
            if ($mbr_savings < 50 && (rtrim($r->last_mbrtype) === 'M' || rtrim($r->last_mbrtype) === 'M28')) {
                $this->rebateClaimRepository->updateRebateClaimRenewed($mbr_id,$r->line_num,$mbr_savings,'Y');
            } else {
                $this->rebateClaimRepository->updateRebateClaimRenewed($mbr_id,$r->line_num,$mbr_savings,'N');
            }
            $this->memberRepository->setSavingsZero($mbr_id);
        }
        return $this->successWithInfo('Success update rebate claim renewed.');
    }

    public function rebateClaimNotRenewed($date = '')
    {
        $datetime = $this->check_date($date);

        $now = Carbon::now()->toDateTimeString();
        $members = $this->memberRepository->getExpired($datetime[0],$datetime[1]);
        foreach ($members as $member) {
            $mbr_id = rtrim($member->mbr_id);
            $last_expiry = $member->exp_date;
            $mbr_savings = $member->mbr_savings;
            $claim_entitled = $mbr_savings < 50 && (rtrim($member->mbr_type) === 'M' || rtrim($member->mbr_type) === 'M28') ? 'Y' : 'N';
//                if ($mbr_savings < 50 && ())
            $is_exists = $this->rebateClaimRepository->isExists($mbr_id, $member->exp_date);
            if (!$is_exists) {
                $this->rebateClaimRepository->create([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbr_id,
                    'last_renewal' => $member->last_renewal,
                    'last_expiry' => $member->exp_date,
                    'claim_entitled' => $claim_entitled,
                    'claim_expiry' => Carbon::parse($last_expiry)->addDays(90)->toDateTimeString(),
                    'rebate_voucher' => '',
                    'mbr_savings' => $mbr_savings,
                    'last_mbrtype' => rtrim($member->mbr_type),
                    'created_by' => 'MIS',
                    'created_on' => $now,
                    'modified_by' => 'MIS',
                    'modified_on' => $now,
                    'updated_on' => $now
                ]);
                $this->memberRepository->setSavingsZero($mbr_id);
            }
        }
        return $this->successWithInfo('Success update rebate claim not renewed.');
    }

    public function clearAllExpiredPoints($date = '')
    {
        if ($date == ''){
            $from = Carbon::today()->subDays(90)->toDateTimeString();
            $to = Carbon::today()->subDays(89)->toDateTimeString();
        } else {
            $from = Carbon::parse($date)->subDays(90)->toDateTimeString();
            $to = Carbon::parse($date)->subDays(89)->toDateTimeString();
        }

        $members = $this->memberRepository->getExpired($from, $to);
        foreach ($members as $member) {
            $mbr_id = rtrim($member->mbr_id);
            $points_accumulated = $member->points_accumulated;
            $points_redeemed = $member->points_redeemed;
            $points_expired = $member->points_expired;
            $mbr_savings = $member->mbr_savings;
            $this->clearExpiredPoints($mbr_id, $points_accumulated,$points_redeemed,$points_expired,$mbr_savings);
        }
        return $this->successWithInfo('Success clear expired points.');
    }

    // Can not call twice one day
    public function approvalRebateClaim($date='')
    {
        $datetime = $this->check_date($date);

        $rebates = $this->rebateClaimRepository->needClaim($datetime[0], $datetime[1]);
        if (!$rebates->isEmpty()) {
            $now = Carbon::now()->toDateTimeString();
            foreach ($rebates as $rebate) {
                $mbr_id = rtrim($rebate->mbr_id);
                $last_expiry = $rebate->last_expiry;
                $last_renewal = $rebate->last_renewal;
                $trans_savings = $this->transactionRepository->checkTransSaving($mbr_id, $last_renewal, $last_expiry);
                if ($trans_savings > 50) {
                    continue;
                }
                $voucher_id = $this->getTransId('RV');
                // crm_voucher_list
                $this->voucherRepository->create([
                    'coy_id' => 'CTL',
                    'coupon_id' => '!VCH-REBATE',
                    'coupon_serialno' => $voucher_id,
                    'promocart_id' => '!VCH-REBATE',
                    'mbr_id' => $mbr_id,
                    'trans_id' => '',
                    'trans_date' => $now,
                    'ho_ref' => '',
                    'ho_date' => $now,
                    'issue_date' => $now,
                    'sale_date' => $now,
                    'utilize_date' => '1900-01-01 00:00:00',
                    'expiry_date' => Carbon::now()->addDays(90)->toDateTimeString(),
                    'print_date' => $now,
                    'loc_id' => 'UBI',
                    'receipt_id1' => $voucher_id,
                    'receipt_id2' => '',
                    'voucher_amount' => 40,
                    'redeemed_amount' => 0,
                    'expired_amount' => 0,
                    'issue_type' => 'eVoucher',
                    'issue_reason' => 'REBATE CLAIM',
                    'status_level' => 0,
                    'created_by' => 'MIS',
                    'created_on' => $now,
                    'modified_by' => 'MIS',
                    'modified_on' => $now
                ]);

                // crm_member_transaction
                $this->transactionRepository->create([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbr_id,
                    'trans_id' => $voucher_id,
                    'line_num' => 1,
                    'trans_type' => 'RD',
                    'trans_date' => $now,
                    'loc_id' => 'UBI',
                    'pos_id' => '',
                    'item_id' => '!VCH-REBATE',
                    'item_desc' => '$40 Rebate e-voucher',
                    'item_qty' => 1,
                    'regular_price' => 0,
                    'unit_price' => 0,
                    'disc_percent' => 0,
                    'disc_amount' => 0,
                    'trans_points' => 0,
                    'salesperson_id' => 'MIS',
                    'mbr_savings' => 0,
                    'created_by' => 'MIS',
                    'created_on' => $now,
                    'modified_by' => 'MIS',
                    'modified_on' => $now,
                    'updated_on' => $now,
                ]);

                // crm_rebate_claim
                $this->rebateClaimRepository->approvalRebateClaim($mbr_id,$rebate->line_num);

                // clear points
                $member = $this->memberRepository->findByMbrId($mbr_id);
                $points_accumulated = $member->points_accumulated;
                $points_redeemed = $member->points_redeemed;
                $points_expired = $member->points_expired;
                $mbr_savings = $member->mbr_savings;
                if ($member->exp_date == $last_expiry) {
                    $this->clearExpiredPoints($mbr_id, $points_accumulated,$points_redeemed,$points_expired,$mbr_savings);
                }
                else {
                    $remained_points = $this->transactionRepository->calculatePoints($mbr_id, $last_expiry);
                    $reduce_points = $points_accumulated-$points_redeemed-$points_expired-$remained_points;
                    // crm_member_transaction
                    $item_desc = 'Accum:'.$points_accumulated.'; Redeem:'.$points_redeemed.'; Expired:'.$points_expired.'; Savings:'.$mbr_savings;
                    if ($remained_points > 0) {
                        $ep_trans_id = $this->getTransId('EP');
                        $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ep_trans_id, 'EP',$item_desc,$reduce_points);
                    }
                    // crm_member_list
                    $member->increment('points_expired', $reduce_points);
                    // crm_member_points
                    $exp_points = $this->pointsRepository->approvalRebateClaim($mbr_id);
                    foreach ($exp_points as $k=>$v) {
                        if ($reduce_points > 0) {
                            $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed']-$v['points_expired'], 2, '.', '');
                            if ($remain_point > $reduce_points) {
                                $this->pointsRepository->addExpiredPoints($mbr_id,$v['line_num'],$reduce_points);
                                $reduce_points = 0;
                            } else {
                                $this->pointsRepository->addExpiredPoints($mbr_id,$v['line_num'],$remain_point);
                                $reduce_points = number_format($reduce_points - $remain_point, 2, '.', '');
                            }
                        }
                    }
                }
            }
            return $this->successWithInfo('Success approval rebate.');
        }
        else {
            return $this->successWithInfo('No rebate to claim.');
        }


    }

    public function processExpirePoints($date='')
    {
        $datetime = $this->check_date($date);
        $from = $datetime[0];
        $to = $datetime[1];
//        $from ='2021-06-30';
//        $to = '2021-07-01';
        $now = Carbon::now();
        $year = $now->year;
        if (Carbon::parse($to)->isSameDay(Carbon::parse((string)($year-1).'-12-31')) || Carbon::parse($to)->isSameDay(Carbon::parse($year.'-07-01'))) {
            $points = DB::select("SELECT * FROM
                        (SELECT (points_accumulated-points_redeemed-points_expired) as expired, * FROM crm_member_points WHERE exp_date >=:dateFrom and exp_date <=:dateTo) as t
                        WHERE expired > 0", ['dateFrom'=>$from, 'dateTo'=>$to]);
            foreach ($points as $point) {
                // crm_member_transaction
                $mbr_id = rtrim($point->mbr_id);
                $points_expired = $point->expired;
                // Points Expired on 2017-02-02 -> -612.00
                $item_desc = 'Points Expired on ' . Carbon::now()->toDateString() . ' -> ' . -$points_expired;

                // TODO: trans_id
                $ep_trans_id = $this->getTransId('EP');
                $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ep_trans_id, 'EP',$item_desc,-$points_expired);
                // crm_member_list
                $this->memberRepository->addExpiredPoints($mbr_id,$points_expired);
                // crm_member_points
                $this->pointsRepository->addExpiredPoints($mbr_id,$point->line_num,$points_expired);
            }
            return $this->successWithInfo('Success process expired points.');
        }
        else {
            return $this->errorWithInfo("Sorry, this api only can use at Jan 1st and Jul 1st.");
        }
    }

    public function getTransId($trans_type)
    {
        $date = Carbon::now()->format('Ym');
        if ($trans_type === 'AP') {
            $prefix = 'ADJUSTAP'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'AP' . $date . sprintf("%07d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'AP' . $date . sprintf("%07d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTAP'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RD') {
            $prefix = 'ADJUSTRD'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'RDUBI' . $date . sprintf("%04d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'RDUBI' . $date . sprintf("%04d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTRD'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RV') {
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','REBATE');
            $next_num = $sys_list->first()->next_num;
            $trans_id = 'RV' . sprintf("%08s", $next_num);
            $sys_list->update([
                'next_num'=>$next_num+1
            ]);
        }
        else {
            $prefix = 'ADJUSTEPUBI'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'EPUB' . $date . sprintf("%05d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'EPUB' . $date . sprintf("%05d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTEPUBI'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        return $trans_id;
    }

    public function clearExpiredPoints($mbr_id, $points_accumulated, $points_redeemed, $points_expired, $mbr_savings)
    {
        $now = Carbon::now();
        // crm_member_transaction
        if ($points_accumulated !== 0) {
            $item_desc = 'Accum:'.$points_accumulated.'; Redeem:'.$points_redeemed.'; Expired:'.$points_expired.'; Savings:'.$mbr_savings;
            $item_desc = substr($item_desc, 0, 60);
            if ($points_accumulated > 0) {
                $ap_trans_id = $this->getTransId('AP');
                $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ap_trans_id, 'AP',$item_desc,-$points_accumulated);
            }
            if ($points_redeemed > 0) {
                $rd_trans_id = $this->getTransId('RD');
                $this->transactionRepository->createPointsTransaction($now,$mbr_id,$rd_trans_id, 'RD',$item_desc,$points_redeemed);
            }
            if ($points_expired > 0) {
                $ep_trans_id = $this->getTransId('EP');
                $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ep_trans_id, 'EP',$item_desc,$points_expired);
            }

            // TODO: YS: Temp solution to record what really expired at end of membership
            if ($points_redeemed - $points_accumulated){
                $em_trans_id = $ap_trans_id;
                DB::connection('pgsql')->table('crm_member_transaction_ep')
                    ->insert([
                        'coy_id' => 'CTL',
                        'mbr_id' => $mbr_id,
                        'trans_id' => $em_trans_id,
                        'trans_type' => 'EP',
                        'trans_date' => $now,
                        'loc_id' => '',
                        'line_num' => 1,
                        'pos_id' => '',
                        'item_id' => '',
                        'item_desc' => substr($item_desc,0,60),
                        'item_qty' => 1,
                        'regular_price' => 0,
                        'unit_price' => 0,
                        'disc_percent' => 0,
                        'disc_amount' => 0,
                        'trans_points' => $points_redeemed - $points_accumulated,
                        'salesperson_id' => 'VCLUB',
                        'mbr_savings' => 0,
                        'created_by' => 'VCLUB',
                        'created_on' => $now,
                        'modified_by' => 'VCLUB',
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ]);
            }


            // crm_member_points
            $this->pointsRepository->clearPoints($mbr_id);
            // crm_member_list
            $this->memberRepository->reducePoints($mbr_id, 0, 0, 0, 0);
        }
    }
}
