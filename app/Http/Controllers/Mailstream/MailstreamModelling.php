<?php

namespace App\Http\Controllers\Mailstream;

use App\Services\EmarSys\EmarsysServiceV2;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Result;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\User;

use App\Services\PosapiService;

class MailstreamModelling extends Controller
{
    public function __construct(
        PosapiService $posapiService
    ){
        $this->posapiService = $posapiService;
        $this->IMSAPI = env('IMSAPI_URL');
    }

    public function invokeModel($model, $trans_id, $email, $type) {

        // chrooms emails, $type = $email_id

        if ($model == 'ORDERDELAY')
            return $this->orderDelay_coupon($type, $email);

        if ($model == 'REFUND_ORIGINAL' || $model == 'REFUND_ECREDITS')
            return $this->refundProcessedCS($model, $type, $email);

        if ($model == 'PARTIAL_SCL')
            return $this->partialCollection($type, $email);

        // if ($model == 'ORDERCANCEL')
        //     return $this->orderCancelled_cs($type, $email);
        
        if ($model == 'ARRIVE_OUTLET')
            return $this->orderArriveOutlet($type, $email);



        // cherps email
        // $type = refund amount (if original)
        if ($model == 'REFUND_P_ORIGINAL' || $model == 'REFUND_P_ECREDITS')
            return $this->refundProcessed($model, $trans_id, $email, $type);

        if ($model == 'ORDERCANCEL')
            return $this->orderCancelled($trans_id, $email);

        // logistic emails

        if ($model == 'COLLECT')  //$type = loc_id
            return $this->orderCollection($trans_id, $email, $type);

        if ($model == 'DISPATCH_NID' || $model == 'DISPATCH' || $model == 'DISPATCH_SPP')
            return $this->orderDispatch($model, $trans_id, $email);
        
        if ($model == 'REMINDER')  //$type = days
            return $this->orderReminder($trans_id, $email, $type);
        
        /*
         * Just ensure that the name use in table crm_emarsys_list.data_model is same as below function name (in lower case).
         *  i.e. data_model = 'INVOICING'. Below should have "private function invoicing($trans_id,$email)
         */

        $method = strtolower($model);
        if (method_exists($this, $method)) {
            return $this->$method($trans_id, $email);
        }
    }


    public function ___buildItemTable($title, $items) {

        if (count($items) == 0)
            return '';

        $ntable = '<h4 align="left" style="margin-bottom: 5px;">' . $title . '&nbsp;</h4>
                            <table cellpadding="10" width="100%">
                                <thead>
                                    <tr style="text-align:center;background-color:#F1F1F1;">
                                        <th style="text-align:left;font-size:12px">Item</th>
                                        <th style="font-size:12px">Unit Price</th>
                                        <th style="font-size:12px">Quantity</th>
                                        <th style="text-align:right;font-size:12px">Price Total</th>
                                    </tr>
                                </thead>
                                <tbody>';

        foreach($items as $itm) {
            $ntable .= '<tr style="text-align:center;">
                        <td style="text-align:left;" class="tabletext"><div style="display:inline-flex">'
                            . htmlentities(trim($itm['item_desc'])) . '<br>'
                            . '[Item ID: ' . trim($itm['item_id']) . ']</div></td>
                        <td class="tabletext">$' . $itm['unit_price'] . '</td>
                        <td class="tabletext">' . $itm['qty_ordered'] . '</td>
                        <td class="tabletext"><strong>$' . $itm['total_price'] . '</strong></td>
                    </tr>';
        }

        $ntable .= '</tbody>
            </table>';

        return $ntable;
    }

    public function ___get_email_item_lines($email_id) {
        $itemlines = DB::connection('pgsql')->select("select cei.item_line_num
                    from o2o_cs_email_item cei 
                    left join o2o_cs_email_list cel 
                    on cel.email_id=cei.email_id and cel.invoice_id=cei.invoice_id 
                    where cel.email_id=? 
                    and cel.email_sent='0' 
                    ", [$email_id]);

        return $itemlines;
    }

    private function invoicing($trans_id, $email) {

        $d = $this->posapiService->get_invoice($trans_id);

        $url_path = env("HACHIWEB_URL") . 'invoice/' . $d['invoice_key'] ;
        $url = '<a href="' . $url_path . '">';
        $query_url = ''; // Only use by mail id 12820?

        $d_payments = '';
        foreach($d['transactions'] as $p) {
            if ($p['success'] == true)
                $d_payments .= $p['pay_mode'] . ' $' . number_format($p['trans_amount'], 2) . '<br>';
        }

        $discount = '';
        $delivery_charge = '';

        $item_groups = [];
        foreach($d['items'] as $item) {
            $ditm = (isset($item['delv_method']) && $item['delv_method'] != '') ? $item['delv_method'] : 'NIL';
            $item_groups[$ditm][] = $item;
        }

        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);

        $item_groups['SCL'] = $item_groups['SCL'] ?? [];
        $col_table = $this->___buildItemTable('Self Collection', $item_groups['SCL']);

        $item_groups['STD'] = $item_groups['STD'] ?? [];
        $std_table = $this->___buildItemTable('Standard Shipping', $item_groups['STD']);

        $item_groups['SDD'] = $item_groups['SDD'] ?? [];
        $exp_table = $this->___buildItemTable('Express Shipping', $item_groups['SDD']);

        $d_addr = explode("\n", $d['addresses']['billing']['addr_text']);
        $d_billing = [
            'coy_name'      => $d['addresses']['billing']['recipient_name'],
            'contact_num'   => $d['addresses']['billing']['phone_num'],
            'addr_1'        => $d_addr[0],
            'addr_2'        => $d_addr[1],
            'addr_3'        => $d['addresses']['billing']['city_name'] .' '. $d['addresses']['billing']['postal_code']
        ];

        $d_shipping = '';
        if (isset($d['addresses']['shipping']['addr_text'])) {
            $d_shipping = '<td class="bodytext4" style="word-wrap: break-word;text-align:left;vertical-align: top;font-family: sans-serif;font-size: 15px;color: #333;font-style: normal;">
                        <h4 width="50" style="text-align:left; margin-bottom: 5px;">Shipping Address</h4>'
                . '<span style="color:#666;font-weight: 400">' . $d['addresses']['shipping']['recipient_name']
                . '<br>' . nl2br($d['addresses']['shipping']['addr_text'])
                . '<br>' . $d['addresses']['shipping']['phone_num']
                . '</span> </td>';
        }

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_order_no' => $d['invoice_num'],
                    'r_url' => $url,
                    'r_query' => $query_url,
                    'r_email' => $email,
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),

                    'r_billingname' => strtoupper($d_billing['coy_name']),
                    'r_contactnum' => $d_billing['contact_num'],
                    'r_billingaddress1' => $d_billing['addr_1'],
                    'r_billingaddress2' => $d_billing['addr_2'],
                    'r_billingaddress3' => $d_billing['addr_3'],
                    'shipping_info' => $d_shipping,

                    'r_pts_earned' => 'V$' . $d['rebates_earned'],
                    'r_purchase_total' => '$' . $d['subtotal'],
                    'r_gst_amt' => '$' . $d['tax_amount'],
                    'r_grand_total' => '$' . $d['total_amount'],

                    'r_payments' => $d_payments,
                    'r_discount' => $discount ?? '',
                    'r_delvCharge' => $delivery_charge ?? '',

                    'r_delvStandardTable' => $std_table,
                    'r_delvCollectionTable' => $col_table,
                    'r_delvNoShipTable' => $nsp_table,
                    'r_delvExpTable' => $exp_table
                ]
            ]
        ];

        return $email_data;
    }

    private function orderDelay_coupon($email_id, $email) {

        $email_data =  DB::connection('pgsql')->select("select invoice_id from o2o_cs_email_list where email_id=:email_id", ['email_id'=>$email_id]);
        $trans_id = $email_data[0]->invoice_id;

        $itemlines = $this->___get_email_item_lines($email_id);

        $d = $this->posapiService->get_invoice($trans_id);

        // print_r($d);

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $item_groups = [];
        foreach($itemlines as $line) {
            foreach($d['items'] as $item) {
                if($item['line_num'] == $line->item_line_num){
                    $ditm = (isset($item['delv_method']) && $item['delv_method'] != '') ? $item['delv_method'] : 'NIL';
                    $item_groups[$ditm][] = $item;
                }
            }
        }

        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);

        $item_groups['SCL'] = $item_groups['SCL'] ?? [];
        $col_table = $this->___buildItemTable('Self Collection', $item_groups['SCL']);

        $item_groups['STD'] = $item_groups['STD'] ?? [];
        $std_table = $this->___buildItemTable('Standard Shipping', $item_groups['STD']);

        $item_groups['SDD'] = $item_groups['SDD'] ?? [];
        $exp_table = $this->___buildItemTable('Same Day Shipping', $item_groups['SDD']);

        $d_addr = explode("\n", $d['addresses']['billing']['addr_text']);
        $d_billing = [
            'coy_name'      => $d['addresses']['billing']['recipient_name'],
            'contact_num'   => $d['addresses']['billing']['phone_num']
        ];

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_order_no' => $d['invoice_num'],
                    'r_url' => $url,
                    'r_email' => $email,
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_billingname' => strtoupper($d_billing['coy_name']),
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_contactnum' => $d_billing['contact_num'],
                    'r_billingaddress' => nl2br($d['addresses']['billing']['addr_text']),
                    'r_delvStandardTable' => $std_table,
                    'r_delvCollectionTable' => $col_table,
                    'r_delvNoShipTable' => $nsp_table,
                    'r_delvExpTable' => $exp_table
                ]
            ]
        ];


        return $email_data;
    }

    public function ___get_store($id){
        $apiurl= "https://pos.api.valueclub.asia/api/function/store_location";

        $authorization = 'Bearer fw6sl9LBIQJIYwRJse5DanMH4OTwtbIm';

        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $authorization],
            'http_errors' => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $product = json_decode($response->getBody()->getContents(),true);

        return $product['data'][$id];

    
    }

    private function orderArriveOutlet($email_id, $email){

        $email_data =  DB::connection('pgsql')->select("select invoice_id,info from o2o_cs_email_list where email_id=:email_id", ['email_id'=>$email_id]);
        $store_id = trim($email_data[0]->info);
        $trans_id = trim($email_data[0]->invoice_id);

        $store_data = $this->___get_store($store_id);

        
        $d = $this->posapiService->get_invoice($trans_id);
        $d_billing = [
            'coy_name'      => $d['addresses']['billing']['recipient_name'],
            'contact_num'   => $d['addresses']['billing']['phone_num']
        ];
        
        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_outlet' => $store_data['shop_name'],
                    'r_inv_url' => $url,
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_contactnum' => $d_billing['contact_num'],
                    'r_email' => $email,
                    'r_shippingname' => $d['addresses']['shipping']['recipient_name'],
                    'r_shippingaddress' => $d['addresses']['shipping']['addr_text']
                ]
            ]
        ];

        return $email_data;
    }

    private function refundProcessedCS($model, $email_id, $email) {
        if(trim($model) == 'REFUND_ORIGINAL'){
            $refund_mode = 'Original Mode of Payment/ Others';
        }else{
            $refund_mode = 'eCredits';
        }
        $email_data =  DB::connection('pgsql')->select("select invoice_id from o2o_cs_email_list where email_id=:email_id", ['email_id'=>$email_id]);
        $trans_id = $email_data[0]->invoice_id;

        $itemlines = $this->___get_email_item_lines($email_id);

        $d = $this->posapiService->get_invoice($trans_id);

        $item_groups = [];
        foreach($itemlines as $line) {
            foreach($d['items'] as $item) {
                if($item['line_num'] == $line->item_line_num){
                    $item_groups['NIL'][] = $item;
                }
            }
        }

        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_delvNoShipTable' => $nsp_table,
                    'r_refund_mode' => $refund_mode,
                    'r_delvStandardTable' => '',
                    'r_delvCollectionTable' => '',
                    'r_delvExpTable' => ''
                ]
            ]
        ];


        return $email_data;
    }


    private function refundProcessed($model, $trans_id, $email, $r_amount) {
        if(trim($model) == 'REFUND_P_ORIGINAL'){
            $refund_mode = 'Original Mode of Payment/ Others';
            $amt = explode(",", $r_amount);
            $refund_amt = '$' . number_format($amt[0], 2);
            $refund_rbt = 'V$' . number_format($amt[1], 2);
        }else{
            $refund_mode = 'eCredits';
            $refund_amt = ''; 
            $refund_rbt = '';
        }
    
        $d = $this->posapiService->get_invoice($trans_id);


        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_url' => $url,
                    'r_delvStandardTable' => '',
                    'r_delvCollectionTable' => '',
                    'r_delvNoShipTable' => $nsp_table,
                    'r_delvExpTable' => '',
                    'r_refund_mode' => $refund_mode,
                    'r_refund_amount' => $refund_amt,
                    'r_refund_rebates' => $refund_rbt
                ]
            ]
        ];

        return $email_data;
    }


    private function partialCollection($email_id, $email) {
        $email_data =  DB::connection('pgsql')->select("select invoice_id from o2o_cs_email_list where email_id=:email_id", ['email_id'=>$email_id]);
        $trans_id = $email_data[0]->invoice_id;

        $itemlines = $this->___get_email_item_lines($email_id);

        $d = $this->posapiService->get_invoice($trans_id);

        $store_id = '';
        $item_groups = [];
        foreach($itemlines as $line) {
            foreach($d['items'] as $item) {
                if($item['line_num'] == $line->item_line_num){
                    $store_id = $item['loc_id'];
                    $item_groups['SCL'][] = $item;
                }
            }
        }


        $store_data = $this->___get_store($store_id);

        $store_name = $store_data['shop_name'];
        $store_address = $store_data['street_line1'].'<br>'.$store_data['street_line2'];

        $html_items = '';
        foreach($item_groups['SCL'] as $item) {
            $html_items .= '<tr style="text-align:center;">
                        <td style="text-align:left;" class="tabletext">' . htmlentities(trim($item['item_desc'])) . '<br>[Item ID: ' . trim($item['item_id']) . ']'
                        . ' </td>
                        <td class="tabletext">' . $store_name . '</td>
                        <td class="tabletext">$' . $item['unit_price'] . '</td>
                        <td class="tabletext">' . $item['qty_ordered'] . '</td>
                        <td class="tabletext"><strong>$' . $item['total_price'] . '</strong></td>
                     </tr>';
        }

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_storeaddress' => $store_address,
                    'r_storename' => $store_name,
                    'r_items' => $html_items,
                    'r_fdcode' => '',
                    'r_url' => $url,
                    'r_email' => $email
                ]
            ]
        ];

        return $email_data;
    }


    public function ___get_ready_items($trans_id, $delv_mode, $loc){
        // $endpoint = 'https://ims.api.valueclub.asia/api/order/';
        $endpoint = $this->IMSAPI.'api/order/';
        // $this->IMSAPI
        $authorization = 'Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ';

        if($delv_mode == 'SCL'){
            $apiurl = $endpoint . 'order_collection/'. $trans_id .'/'.$loc;
        }elseif($delv_mode == 'DISPATCH_NID'){
            $apiurl = $endpoint . 'dispatch_nid/'. $trans_id;
        }else{
            $apiurl = $endpoint . 'dispatch_items/'. $trans_id;
        }
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $authorization],
            'http_errors' => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $product = json_decode($response->getBody()->getContents(),true);

        return $product['data'];

    }

    private function orderCollection($trans_id, $email, $loc) {
        $items = $this->___get_ready_items($trans_id, 'SCL', $loc);

        $d = $this->posapiService->get_invoice($trans_id);


        $store_data = $this->___get_store($loc);

        $store_name = $store_data['shop_name'];
        $store_address = $store_data['street_line1'].'<br>'.$store_data['street_line2'];
        
        $html_items = '';
        foreach($items as $item) {
            $unit_price = number_format((float)$item['unit_price'], 2);
            $item['total_price'] = number_format((float)$item['qty_ordered']*(float)$item['unit_price'], 2);
           
            $html_items .= '<tr style="text-align:center;">
                        <td style="text-align:left;" class="tabletext">' . htmlentities(trim($item['item_desc'])) . '<br>[Item ID: ' . trim($item['item_id']) . ']'
                        . ' </td>
                        <td class="tabletext">' . $store_name . '</td>
                        <td class="tabletext">$' . $unit_price . '</td>
                        <td class="tabletext">' . $item['qty_ordered'] . '</td>
                        <td class="tabletext"><strong>$' . $item['total_price'] . '</strong></td>
                     </tr>';
        }

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';


        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_storeaddress' => $store_address,
                    'r_storename' => $store_name,
                    'r_items' => $html_items,
                    'r_fdcode' => '',
                    'r_url' => $url,
                    'r_email' => $email
                ]
            ]
        ];
        return $email_data;
    }

    public function orderReminder($trans_id, $email, $daysLoc){

        $daysLoc = explode(",", $daysLoc);
        $days = $daysLoc[0];
        $loc = $daysLoc[1];

        if ($days == 10) {
            $reminder_text = "We noticed that you have not collected your order.<br>
                    Please proceed to selected store to collect your order with the required documents.<br><br>
                    (Please ignore this email if have done so)";
        } elseif ($days == 14) {
            $reminder_text = "We noticed that you have not collected your order as shown below. This order can still be collected at 
                    your selected Challenger store within the next <span style='color:red'>10 days.</span><br><br>
                    Please note that for orders not collected after given deadline, Hachi.tech reserves the right to render 
                    the order voided and disposed off.<br>
                    Thank you for your understanding and we hope to serve you again soon.<br><br>
                    (Please ignore this email if have done so)";
        }

        $items = $this->___get_ready_items($trans_id, 'SCL', $loc);

        $d = $this->posapiService->get_invoice($trans_id);


        $store_data = $this->___get_store($loc);

        $store_name = $store_data['shop_name'];
        $store_address = $store_data['street_line1'].'<br>'.$store_data['street_line2'];
        
        $html_items = '';
        foreach($items as $item) {
            $unit_price = number_format((float)$item['unit_price'], 2);
            $item['total_price'] = number_format((float)$item['qty_ordered']*(float)$item['unit_price'], 2);
            
            $html_items .= '<tr style="text-align:center;">
                        <td style="text-align:left;" class="tabletext">' . htmlentities(trim($item['item_desc'])) . '<br>[Item ID: ' . trim($item['item_id']) . ']'
                        . ' </td>
                        <td class="tabletext">' . $store_name . '</td>
                        <td class="tabletext">$' . $unit_price . '</td>
                        <td class="tabletext">' . $item['qty_ordered'] . '</td>
                        <td class="tabletext"><strong>$' . $item['total_price'] . '</strong></td>
                     </tr>';
        }

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_storeaddress' => $store_address,
                    'r_storename' => $store_name,
                    'r_items' => $html_items,
                    'r_fdcode' => '',
                    'r_url' => $url,
                    'r_email' => $email,
                    'r_reminder_text' => $reminder_text
                ]
            ]
        ];

        return $email_data;
    }

    public function __get_delivery_time($trans_id){

        // $this->endpoint = 'https://ims.api.valueclub.asia/api/order/';
        $this->endpoint = $this->IMSAPI.'api/order/';
        
        $this->authorization = 'Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ';

        $apiurl = $this->endpoint . 'delivery_time/'. $trans_id;
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->authorization],
            'http_errors' => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $product = json_decode($response->getBody()->getContents(),true);

        return $product['data'];
    }


    private function orderDispatch($model, $trans_id, $email) {
        $items = $this->___get_ready_items($trans_id, $model, '');

        $d = $this->posapiService->get_invoice($trans_id);

        $html_items = '';
        foreach($items as $item) {
            $unit_price = number_format((float)$item['unit_price'], 2);
            $item['total_price'] = number_format((float)$item['qty_ordered']*(float)$item['unit_price'], 2);
            
            $html_items .= '<tr style="text-align:center;">
                        <td style="text-align:left;" class="tabletext">' . htmlentities(trim($item['item_desc'])) . '<br>[Item ID: ' . trim($item['item_id']) . ']'
                        . ' </td>
                        <td class="tabletext">$' . $unit_price . '</td>
                        <td class="tabletext">' . $item['qty_ordered'] . '</td>
                        <td class="tabletext"><strong>$' . $item['total_price'] . '</strong></td>
                     </tr>';
        }

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';
 
        $delv_date = '';
        $time_slot = '';
        if($model == 'DISPATCH_NID'){
            $delv_time = $this->__get_delivery_time($trans_id);
            $delv_date = date("d/m/Y", strtotime($delv_time['delivery_date']));
            $delv_window = $delv_time['delivery_window'];
            if ($delv_window == '-1') {
                $time_slot = '0900 - 2200';
            } elseif ($delv_window == '0') {
                $time_slot = '0900 - 1200';
            } elseif ($delv_window == '1') {
                $time_slot = '1200 - 1500';
            } elseif ($delv_window == '2') {
                $time_slot = '1500 - 1800';
            } elseif ($delv_window == '3') {
                $time_slot = '1800 - 2200';
            } elseif ($delv_window == '4') {
                $time_slot = '0900 - 1400';
            } elseif ($delv_window == '5') {
                $time_slot = '1400 - 1800';
            } elseif ($delv_window == '7') {
                $time_slot = '1200 - 1800';
            } elseif ($delv_window == '8') {
                $time_slot = '1100 - 1300';
            }
        }

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_shippingname' => $d['addresses']['shipping']['recipient_name'],
                    'r_contactnum' => $d['addresses']['shipping']['phone_num'],
                    'r_shippingaddress' => $d['addresses']['shipping']['addr_text'],
                    'r_delv_date' => $delv_date,
                    'r_delv_time' => $time_slot,
                    'r_items' => $html_items,
                    'r_url' => $url,
                    'r_email' => $email
                ]
            ]
        ];

        return $email_data;
    }


    private function orderCancelled_cs($email_id, $email) {

        $email_data =  DB::connection('pgsql')->select("select invoice_id from o2o_cs_email_list where email_id=:email_id", ['email_id'=>$email_id]);
        $trans_id = $email_data[0]->invoice_id;

        $itemlines = $this->___get_email_item_lines($email_id);

        $d = $this->posapiService->get_invoice($trans_id);

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $item_groups = [];
        foreach($itemlines as $line) {
            foreach($d['items'] as $item) {
                if($item['line_num'] == $line->item_line_num){
                    $ditm = (isset($item['delv_method']) && $item['delv_method'] != '') ? $item['delv_method'] : 'NIL';
                    $item_groups[$ditm][] = $item;
                }
            }
        }

        $d_payments = '';
        foreach($d['transactions'] as $p) {
            if ($p['success'] == true)
                $d_payments .= $p['pay_mode'] . ' $' . number_format($p['trans_amount'], 2) . '<br>';
        }

        $discount = '';
        $delivery_charge = '';

        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);

        $item_groups['SCL'] = $item_groups['SCL'] ?? [];
        $col_table = $this->___buildItemTable('Self Collection', $item_groups['SCL']);

        $item_groups['STD'] = $item_groups['STD'] ?? [];
        $std_table = $this->___buildItemTable('Standard Shipping', $item_groups['STD']);

        $item_groups['SDD'] = $item_groups['SDD'] ?? [];
        $exp_table = $this->___buildItemTable('Express Shipping', $item_groups['SDD']);

        $d_addr = explode("\n", $d['addresses']['billing']['addr_text']);
        $d_billing = [
            'coy_name'      => $d['addresses']['billing']['recipient_name'],
            'contact_num'   => $d['addresses']['billing']['phone_num'],
            'addr_1'        => $d_addr[0],
            'addr_2'        => $d_addr[1],
            'addr_3'        => $d['addresses']['billing']['city_name'] .' '. $d['addresses']['billing']['postal_code']
        ];

        $d_shipping = '<td class="bodytext4" style="word-wrap: break-word;text-align:left;vertical-align: top;font-family: sans-serif;font-size: 15px;color: #333;font-style: normal;">
                        <h4 width="50" style="text-align:left; margin-bottom: 5px;">Shipping Address</h4>'
                    . '<span style="color:#666;font-weight: 400">' . $d['addresses']['shipping']['recipient_name']
                    . '<br>' . nl2br($d['addresses']['shipping']['addr_text'])
                    . '<br>' . $d['addresses']['shipping']['phone_num']
                    . '</span> </td>';

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [

                    'r_firstname' => (isset($d['customer_info'])) ? $d['customer_info']['name'] : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_billingname' => strtoupper($d_billing['coy_name']),
                    'r_contactnum' => $d_billing['contact_num'],
                    'r_billingaddress' =>  $d['addresses']['billing']['addr_text'],
                    'r_url' => $url,
                    'r_email' => $email,
                    'r_pts_earned' => 'V$' . $d['rebates_earned'],
                    'r_purchase_total' => '$' . $d['subtotal'],
                    'r_gst_amt' => '$' . $d['tax_amount'],
                    'r_grand_total' => '$' . $d['total_amount'],
                    'r_delvCharge' => $delivery_charge ?? '',
                    'r_discount' => $discount ?? '',
                    'r_payments' => $d_payments,
                    'r_delvStandardTable' => $std_table,
                    'r_delvCollectionTable' => $col_table,
                    'r_delvNoShipTable' => $nsp_table,
                    'r_delvExpTable' => $exp_table
                ]
            ]
        ];

        return $email_data;
    }

    private function orderCancelled($trans_id, $email) {

        $d = $this->posapiService->get_invoice($trans_id);

        $url = '<a href="https://www.hachi.tech/invoice/external/'.$trans_id.'">';

        $item_groups = [];
        foreach($d['items'] as $item) {
            $item_groups['NIL'][] = $item;
        }

        $item_groups['NIL'] = $item_groups['NIL'] ?? [];
        $nsp_table = $this->___buildItemTable('', $item_groups['NIL']);


        $d_addr = explode("\n", $d['addresses']['billing']['addr_text']);
        $d_billing = [
            'coy_name'      => $d['addresses']['billing']['recipient_name'],
            'contact_num'   => $d['addresses']['billing']['phone_num'],
            'addr_1'        => $d_addr[0],
            'addr_2'        => $d_addr[1],
            'addr_3'        => $d['addresses']['billing']['city_name'] .' '. $d['addresses']['billing']['postal_code']
        ];

        $email_data = [
            'key_id' => '3',
            'external_id' => $email,
            'data' => [
                'global' => [
                    'r_firstname' => (isset($d['customer_info'])) ? htmlspecialchars($d['customer_info']['name']) : '',
                    'r_order_no' => $d['invoice_num'],
                    'r_transactiondate' => date("d M Y", strtotime($d['trans_date'])),
                    'r_billingname' => strtoupper($d_billing['coy_name']),
                    'r_contactnum' => $d_billing['contact_num'],
                    'r_billingaddress' =>  $d['addresses']['billing']['addr_text'],
                    'r_url' => $url,
                    'r_email' => $email,
                    'r_delvStandardTable' => '',
                    'r_delvCollectionTable' => '',
                    'r_delvNoShipTable' => $nsp_table,
                    'r_delvExpTable' => ''
                ]
            ]
        ];

        return $email_data;

    }
}
