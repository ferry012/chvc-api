<?php

namespace App\Http\Controllers\Mailstream;

use App\Services\EmarSys\EmarsysServiceV2;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Result;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Mailstream\MailstreamModelling;
use App\Services\CtlmailerService;
use Illuminate\Support\Facades\Redis;

class EmarsysController extends Controller
{
    use Result;

    private $dbconn, $emarsysService, $msModelling, $ctlmailerService;

    public function __construct(
            EmarsysServiceV2 $emarsysService,
            MailstreamModelling $msModelling,
            CtlmailerService $ctlmailerService
    ){
        $this->dbconn = DB::connection('pgsql');

        $this->emarsysService = $emarsysService;
        $this->msModelling = $msModelling;
        $this->ctlmailerService = $ctlmailerService;
    }


    /**
     * @api {post} /api/mailstream/{name} Send Mailstream
     * @apiVersion 1.0.0
     * @apiGroup EMAIL_STREAM
     *
     * @apiParam {string} name Mailstream ID set up in Database
     * @apiParam {string} source Mandatory Source name for identification
     * @apiParam {string} trans_id Mandatory Trans ID
     * @apiParam {string} email Mandatory Customer email
     * @apiParam {string} contactnum Mandatory Customer contact
     * @apiParam {string} data Data set defined by Mailstream
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "source": "hachi",
     *      "trans_id": "HID10182",
     *      "email":"yongsheng@challenger.sg",
     *      "contactnum": "96211467",
     *      "data": {
     *          "r_name":"customer"
     *      }
     *  }
     *
     * @apiSuccessExample Success
     * HTTP/1.1 200 OK
     *  {
     *      "status": "success",
     *      "status_code": 200
     *      "data": {"activity_id":"ABCDE","response":"OK"},
     *  }
     *
     * @apiDescription Mailstream {name} must be set up in crm_emarsys_list.
     *      Parameters will be
     */

    public function __invoke($mailstreamId){

        if (!(request()->has(['source','trans_id','email']))) {
            return $this->errorWithCodeAndInfo(422, "Missing required fields for API");
        }

        $trans_id = request()->post('trans_id');
        $email = request()->post('email');
        $type = request()->post('type') ? request()->post('type') : '';
        $source = request()->post('source') ? request()->post('source') : '';
        $dataset = null;

        return $this->do_mailstream($mailstreamId, $trans_id, $email, $type, $source);

    }

    public function ___update_cs_email_status($email_id)
	{
        $this->db20 = DB::connection('pgsql');
        $sql_upd_type = "update o2o_cs_email_list SET email_sent = 1 WHERE email_id = ?";
        $this->db20->statement($sql_upd_type, [$email_id]);
        return TRUE;
	}

    public function do_mailstream($mailstreamId, $trans_id, $email, $type, $source, $dataset = null){

        $ms = $this->_get_mailstream($mailstreamId);
        if (!$ms){
            return $this->errorWithCodeAndInfo(404, 'NOT FOUND');
        }

        // Prep $dataset
        if ($dataset === null) {
            $dataset = request()->post('data');
        }

        $logData = [
            'activity_id'   => bin2hex(time() . random_bytes(22)),
            'activity_date' => date('Y-m-d H:i:s'),
            'source_id'     => $source,
            'trans_id'      => $trans_id,
            'mailstream_id' => $ms->mailstream_id,
            'request_email' => $email,
        ];

        if ($ms->service_partner == 'EMARSYS') {
            if (trim($ms->data_model) != '') {
                $email_template = $this->msModelling->invokeModel($ms->data_model, $trans_id, $email, $type);
            }
            else {
                // No modelling is defined, will expect data pass in
                if ($dataset === null) {
                    return $this->errorWithCodeAndInfo(422, "Missing required fields for Mapping");
                }
                $email_template = $ms->data_template;
            }
            $user_result = $this->emarsysService->registerUserEmail($email);
            $email_result = $this->emarsysService->sendEmail($ms->service_acct, $ms->service_method, $ms->service_uri, $email_template, $ms->data_model, $dataset);

            if ($ms->email_platform == 'CHROOMS') {
                $this->___update_cs_email_status($type);
            }

            $logData['request_svc_acct'] = $email_result['client'];
            $logData['request_endpoint'] = $email_result['endpoint'];
            $logData['request_data'] = $email_result['request'];
            $logData['response_data'] = $email_result['response'];
            $logData['response_status'] = $email_result['response']['replyCode'] ?? '';
            $logData['response_message'] = $email_result['response']['replyText'] ?? '';
            $this->_log_mailstream($logData);
        }
        else if ($ms->service_partner == 'SMS') {
            $logData['response_status'] = 0;
            $logData['response_message'] = 'Warning - SMS service will not provide response. Please refer to log.';
        }
        else {
            $logData['request_svc_acct'] = '';
            $logData['request_endpoint'] = '';
            $logData['request_data'] = json_encode($dataset);
            $logData['response_data'] = '';
            $logData['response_status'] = -1;
            $logData['response_message'] = 'NO CONTENT';
            $this->_log_mailstream($logData);

            $logData = null;
        }

        if (isset($ms->sms) && !empty($ms->sms)) {

            $subject = $ms->sms;

            // Phone number can be passed in either as first-level param or within data param
            $sms_number = '';
            $sms_number = (isset($dataset['contactnum'])) ? $dataset['contactnum'] : $sms_number;
            $sms_number = (isset($dataset['r_contactnum'])) ? $dataset['r_contactnum'] : $sms_number;
            $sms_number = (request()->has(['contactnum'])) ? request()->post('contactnum') : $sms_number;
            $dataset['contactnum'] = $sms_number; // For search & replace later

            if (!empty($sms_number)) {

                $dataset1 = request()->post();
                unset($dataset1['data']);
                $full_dataset = array_merge($dataset, $dataset1);

                // Search & replace
                foreach ($full_dataset as $dk => $ds) {
                    $s = '{' . $dk . '}';
                    $r = $ds;
                    $subject = str_replace($s, $r, $subject);
                }

                // Send SMS
                $sms = $this->ctlmailerService->send_sms($sms_number, $subject);

                // Log
                $logData2 = [
                    'activity_id' => bin2hex(time() . random_bytes(22)),
                    'activity_date' => date('Y-m-d H:i:s'),
                    'source_id' => 'MAILST_SMS',
                    'trans_id' => $trans_id,
                    'mailstream_id' => $ms->mailstream_id,
                    'request_email' => $email,
                    'request_svc_acct' => 'ctlmailerService',
                    'request_endpoint' => 'https://ctlmailer.api.valueclub.asia/',
                    'request_data' => json_encode(['recipients' => $sms_number, 'subject' => $subject]),
                    'response_data' => json_encode($sms),
                    'response_status' => $sms->code,
                    'response_message' => $sms->message
                ];
                $this->_log_mailstream($logData2);

                if ($sms->code == 1) {
                    $logData['activity_id'] = (isset($logData['activity_id'])) ? $logData['activity_id'] : $logData2['activity_id'];
                    $logData['response_status'] = (isset($logData['response_status'])) ? $logData['response_status'] : 1;
                    $logData['response_message'] = (isset($logData['response_message'])) ? $logData['response_message'] . ' - SMS SENT' : 'SMS SENT';
                }

            }
            else {
                // Log
                $logData2 = [
                    'activity_id' => bin2hex(time() . random_bytes(22)),
                    'activity_date' => date('Y-m-d H:i:s'),
                    'source_id' => 'MAILST_SMS',
                    'trans_id' => $trans_id,
                    'mailstream_id' => $ms->mailstream_id,
                    'request_email' => $email,
                    'request_svc_acct' => 'ctlmailerService',
                    'request_endpoint' => 'https://ctlmailer.api.valueclub.asia/',
                    'request_data' => '',
                    'response_data' => '',
                    'response_status' => '',
                    'response_message' => 'Missing phone number'
                ];
                $this->_log_mailstream($logData2);
            }
        }

        if (isset($logData['response_status'])) {
            if ($logData['response_status'] !== 0) {
                $res_data = [
                    'activity_id' => $logData['activity_id'],
                    'response' => $logData['response_message']
                ];
                return $this->errorWithCodeAndInfo(202, $res_data);
            }
            else {
                $res_data = [
                    'activity_id' => $logData['activity_id'],
                    'response' => $logData['response_message']
                ];
                return $this->successWithData($res_data);
            }
        }
        else {
            return $this->errorWithCodeAndInfo(204, 'NO CONTENT');
        }
    }

    private function _get_mailstream($mailstream){
        return $this->dbconn->table('crm_emarsys_list')->where('mailstream_id', $mailstream)->first();
    }

    private function _log_mailstream($data){

        if (is_array($data['request_data']))
            $data['request_data'] = json_encode($data['request_data']);
        if (is_array($data['response_data']))
            $data['response_data'] = json_encode($data['response_data']);

        $data2['activity_id'] = $data['activity_id'] ?? bin2hex(time() . random_bytes(22));
        $data2['activity_date'] = $data['activity_data'] ?? date('Y-m-d H:i:s');
        $data2['ref_activity_id'] = $data['ref_activity_id'] ?? '';
        $data2['source_id'] = substr($data['source_id'], 0,10);
        $data2['trans_id'] = substr($data['trans_id'], 0,15);
        $data2['mailstream_id'] = $data['mailstream_id'];
        $data2['request_svc_acct'] = substr($data['request_svc_acct'],0,20) ?? '';
        $data2['request_endpoint'] = substr($data['request_endpoint'],0,100) ?? '';
        $data2['request_email'] = substr($data['request_email'],0,64) ?? '';
        $data2['request_data'] = substr($data['request_data'],0,4000) ?? '';
        $data2['response_status'] = substr($data['response_status'], 0,32) ?? '';
        $data2['response_date'] = $data['response_date'] ?? date('Y-m-d H:i:s');
        $data2['response_data'] = substr($data['response_data'], 0,4000) ?? '';

        $this->dbconn->table('crm_emarsys_log')->insert($data2);

    }

    public function registerEmail(){
        $email = request()->post('email');
        $email_result = $this->emarsysService->registerUserEmail($email);
        return $this->successWithData($email_result);
    }

    public function invoke_job(){
        // Get 1 job from crm_emarsys_job with member details
        $sql = "select a.id, a.job_sys, a.job_type, a.udf_data1,a.udf_data2,a.udf_data3,
                       b.mbr_id,b.mbr_type,b.first_name,b.last_name,b.birth_date,b.email_addr,b.contact_num,b.exp_date,
                       b.mbr_savings,
                       case when (b.points_accumulated - b.points_reserved - b.points_redeemed - b.points_expired) > 0 then (b.points_accumulated - b.points_reserved - b.points_redeemed - b.points_expired)/100 else 0 end rebates
                from crm_api_job a 
                left join crm_member_list b on a.mbr_id=b.mbr_id
                where a.job_date <= ? and a.status_level=0
                order by a.job_date limit 1 ";
        $job = $this->dbconn->select($sql, [date('Y-m-d H:i:s')]);

        if (count($job)) {
            if ($job[0]->job_sys == 'MAILSTREAM') {

                // For mailstream that needs to be send via job, should use below basic standard params
                $dataset = [
                    "email" => $job[0]->email_addr,

                    "r_c_first_name" => $job[0]->first_name,
                    "r_fname" => $job[0]->first_name,

                    "r_FirstName" => $job[0]->first_name,
                    "r_LastName" => $job[0]->last_name,
                    "r_ExpDate" => $job[0]->exp_date,
                    "r_Rebates" => $job[0]->rebates,
                    "r_Savings" => $job[0]->mbr_savings,
                    "r_MbrType" => $job[0]->mbr_type
                ];

                // Other special params, should use udf_data1 to add
                $dataparams = explode(',',strtolower($job[0]->udf_data1));
                if (in_array('r_orderdetailsurl',$dataparams)){
                    $token = "chstaff.challenger.sg" . "vctoken#" . strtoupper(uniqid());
                    Redis::set($token, json_encode("edumembership"));
                    Redis::expire($token, 259200);

                    $edata = base64_encode(
                        json_encode([
                            "u" => $job[0]->mbr_id,
                            "p" => '',
                            "token" => $token,
                            "e" => $job[0]->email_addr
                        ])
                    );
                    $dataset["r_OrderDetailsURL"] = 'vc.challenger.sg/api/vc2/activateMember/' . $edata;;
                }

                $log = $this->do_mailstream($job[0]->job_type, $job[0]->mbr_id, $job[0]->email_addr, '', 'emarsys-job', $dataset);
            }

            $sql = "update crm_api_job set status_level=1,modified_by='JOB',modified_on=current_timestamp where id = ?";
            $this->dbconn->update($sql, [$job[0]->id]);

            $res_data = [
                'code'  => 1,
                'job_id' => $job[0]->id,
                'response' => $log
            ];
            return $this->successWithData($res_data);
        }

        // No pending
        $res_data = [
            'code' => 1,
            'response' => ["msg"=>"No scheduled job pending"]
        ];
        return $this->successWithData($res_data);
    }

}
