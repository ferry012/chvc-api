<?php

namespace App\Http\Controllers\Admin\Member;

use App\Http\Controllers\Result;
use App\Repositories\Member\Points\PointsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    use Result;
    private $memberPointsRepository;

    public function __construct(PointsRepository $memberPointsRepository)
    {
        $this->memberPointsRepository = $memberPointsRepository;
    }

    public function index()
    {
        $points = $this->memberPointsRepository->getQuery(15);
        if ($points) {
            return $this->successWithData($points);
        } else {
            return $this->errorWithInfo('Point get error');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id, $lineNum = null)
    {
        $points = $this->memberPointsRepository->showAdmin($id, $lineNum);
        if ($points) {
            return $this->successWithData($points);
        } else {
            return $this->errorWithInfo('There is no record in our database.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id, $lineNum)
    {
        $points = $this->memberPointsRepository->deleteAdmin($id, $lineNum);
        if ($points) {
            return $this->success();
        } else {
            return $this->errorWithInfo('Point delete error.');
        }
    }
}
