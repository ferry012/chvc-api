<?php

namespace App\Http\Controllers\Admin\Member;

use App\Http\Controllers\Result;
use App\Repositories\Member\Transaction\TransactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    use Result;
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function index()
    {
        $transaction = $this->transactionRepository->getQuery(15);
        if ($transaction) {
            return $this->successWithData($transaction);
        } else {
            return $this->errorWithInfo('Transaction get error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id, $transId, $lineNum = null)
    {
        $transaction = $this->transactionRepository->showAdmin($id, $transId, $lineNum);
        if ($transaction) {
            return $this->successWithData($transaction);
        } else {
            return $this->errorWithInfo('There is no transaction in our database.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id, $transId, $lineNum)
    {
        $transaction = $this->transactionRepository->deleteAdmin($id, $transId, $lineNum);
        if ($transaction) {
            return $this->success();
        } else {
            return $this->errorWithInfo('Transaction delete error.');
        }
    }
}
