<?php

namespace App\Http\Controllers\Admin\Member;

use App\Http\Controllers\Result;
use App\Repositories\Member\Voucher\VoucherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoucherController extends Controller
{
    use Result;
    private $voucherRepository;

    public function __construct(VoucherRepository $voucherRepository)
    {
        $this->voucherRepository = $voucherRepository;
    }

    public function index()
    {
        $voucher = $this->voucherRepository->getQuery(15);
        if ($voucher) {
            return $this->successWithData($voucher);
        } else {
            return $this->errorWithInfo('Voucher get error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id, $couponId = null)
    {
        $voucher = $this->voucherRepository->showAdmin($id, $couponId);
        if ($voucher) {
            return $this->successWithData($voucher);
        } else {
            return $this->errorWithInfo('There is no record in our database.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id, $couponId)
    {
        $voucher = $this->voucherRepository->deleteAdmin($id, $couponId);
        if ($voucher) {
            return $this->success();
        } else {
            return $this->errorWithInfo('Voucher delete error.');
        }
    }
}
