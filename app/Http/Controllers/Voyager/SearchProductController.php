<?php

namespace App\Http\Controllers\Voyager;

use Carbon\Carbon;
use CHG\Voyager\Http\Controllers\VoyagerBaseController;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use CHG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Schema;

class SearchProductController extends VoyagerBaseController
{

    public function __construct(){
    }
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.index")) {
            $view = "voyager::$slug.index";
        }

        return Voyager::view($view, compact(
            'search'
        ));
    }

    public function getallinventory($id, Request $request) {
        $url = 'https://ims.api.valueclub.asia/api/qty_available/CTL/' . $id;
        $headers = array(
            'Authorization: Bearer Mu0qAldeQV5knglVQs3BEpZztCKfyL9Dhu3RsUYQ',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_POST, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);

        return $result;
    }
}
