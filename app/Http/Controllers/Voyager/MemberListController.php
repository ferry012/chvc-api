<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Result;
use App\Models\Member\Points\Points;
use App\Repositories\Member\AddressRepository;
use App\Repositories\Member\MemberMetaRepository;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsExpiryRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use App\Services\POS\PosService;
use Carbon\Carbon;
use CHG\Voyager\Http\Controllers\VoyagerBaseController;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use CHG\Voyager\Database\Schema\SchemaManager;
use CHG\Voyager\Events\BreadDataAdded;
use CHG\Voyager\Events\BreadDataDeleted;
use CHG\Voyager\Events\BreadDataRestored;
use CHG\Voyager\Events\BreadDataUpdated;
use CHG\Voyager\Events\BreadImagesDeleted;
use CHG\Voyager\Facades\Voyager;
use CHG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Modules\Hachi\Entities\Promo\PromoItemUploadModel;

class MemberListController extends VoyagerBaseController
{
    use BreadRelationshipParser;
    use Result;

    private $addressRepository;
    private $memberRepository;
    private $memberTypeRepository;
    private $pointsRepository;
    private $transactionRepository;
    private $pointsExpiryRepository;
    private $voucherRepository;
    private $memberMetaRepository;
    private $posService;
    private $ENVIRONMENT;
    private $posApiUrl;
    private $vc8ApiUrl;

    public function __construct(AddressRepository $addressRepository,
                                MemberRepository $memberRepository,
                                MemberTypeRepository $memberTypeRepository,
                                PointsRepository $pointsRepository,
                                TransactionRepository $transactionRepository,
                                PointsExpiryRepository $pointsExpiryRepository,
                                MemberMetaRepository $memberMetaRepository,
                                PosService $posService,
                                VoucherRepository $voucherRepository)
    {
        $this->addressRepository = $addressRepository;
        $this->memberRepository = $memberRepository;
        $this->memberTypeRepository = $memberTypeRepository;
        $this->pointsRepository = $pointsRepository;
        $this->transactionRepository = $transactionRepository;
        $this->pointsExpiryRepository = $pointsExpiryRepository;
        $this->memberMetaRepository = $memberMetaRepository;
        $this->voucherRepository = $voucherRepository;
        $this->posService = $posService;
        $prod_mode = env("APP_ENV");
        if (strpos($_SERVER['REQUEST_URI'], 'mode=prod') !== false) {
            $prod_mode = 'production';
        }

        if($prod_mode == 'production'){
            $this->ENVIRONMENT = 'production';
            $this->posApiUrl = 'https://pos.api.valueclub.asia';
            $this->vc8ApiUrl = 'https://vc8.api.valueclub.asia';
        }else if($prod_mode == 'local'){
            $this->ENVIRONMENT = 'development';
            $this->posApiUrl = 'https://chappos-api.sghachi.com';
            $this->vc8ApiUrl = 'https://chvc-api.sghachi.com';
        }else{
            $this->ENVIRONMENT = 'development';
            $this->posApiUrl = 'https://chappos-api.sghachi.com';
            $this->vc8ApiUrl = 'https://chvc-api.sghachi.com';
        }
    }



    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************


    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s')];

        $searchNames = [];
//        if ($dataType->server_side) {
//            $searchable = Schema::getColumnListing('crm_member_list');
//            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
//            foreach ($searchable as $key => $value) {
//                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
//                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
//            }
//        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) ) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '') {
                $search_value = '%'.$search->value.'%';
                $query->whereRaw('mbr_id ilike ? or first_name ilike ? or last_name ilike ? or email_addr ilike ? or contact_num ilike ? or mbr_type ilike ?',
                                    [$search_value,$search_value,$search_value,$search_value,$search_value, $search_value]);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::connection('pgsql')->table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->details->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                if ($action != "CHG\Voyager\Actions\DeleteAction"){
                    $action = new $action($dataType, $dataTypeContent->first());

                    if ($action->shouldActionDisplayOnDataType()) {
                        $actions[] = $action;
                    }
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************

    public function show(Request $request, $id)
    {
        return redirect()->to("member-list/" . $id . "/edit");

        /*
        $slug = $this->getSlug($request);

        $now = Carbon::now();

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::connection('pgsql')->table($dataType->name)->where('id', $id)->first();
        }

        if (rtrim($dataTypeContent['mbr_addr']) != '') {
            $address = DB::connection('pgsql')->select('SELECT b.postal_code, rtrim(b.street_line1) street_line1, rtrim(b.street_line2) street_line2, rtrim(b.street_line3) street_line3, rtrim(b.street_line4) street_line4, b.country_id FROM crm_member_list a JOIN coy_address_book b ON a.mbr_id = b.ref_id AND (CASE WHEN a.mbr_addr=\'\' THEN \'0-PRIMARY\' ELSE a.mbr_addr END) = b.addr_type WHERE a.mbr_id = ?', [$id]);
        } else {
            $address = '';
        }

        // Check permission
        $this->authorize('read', $dataTypeContent);

        if (rtrim($dataTypeContent['mbr_addr']) != '') {
            $delv_addr = DB::connection('pgsql')->select('SELECT b.postal_code, rtrim(b.street_line1) street_line1, rtrim(b.street_line2) street_line2, rtrim(b.street_line3) street_line3, rtrim(b.street_line4) street_line4, b.country_id FROM crm_member_list a JOIN coy_address_book b ON a.mbr_id = b.ref_id AND (CASE WHEN a.delv_addr=\'\' THEN \'2-DELIVERY\' ELSE a.delv_addr END) = b.addr_type WHERE a.mbr_id = ?', [$id]);
        }else {
            $delv_addr = '';
        }

        $mbr_points = DB::connection('pgsql')->table('crm_member_points')->selectRaw('points_accumulated,points_redeemed,points_expired, exp_date')->where('mbr_id', $id)->orderBy('exp_date', 'desc')->get();
        $mbr_type = DB::connection('pgsql')->table('crm_member_type')->selectRaw('eff_from, eff_to, rtrim(mbr_type) mbr_type')->orderBy('eff_to', 'desc')->where('mbr_id',$id)->get();
        $mbr_voucher = DB::connection('pgsql')->table('crm_voucher_list')->selectRaw('coupon_id, expiry_date')->where(['coy_id'=>'CTL', 'mbr_id'=>$id, 'status_level'=>0])->where('expiry_date', '>', $now)->orderBy('created_on', 'desc')->get();
        $mbr_trans = DB::connection('pgsql')->table('crm_member_transaction')->select('trans_id', 'trans_type', 'trans_date', 'loc_id', 'pos_id', 'item_id', 'item_desc', 'item_qty', 'regular_price', 'unit_price','disc_percent','disc_amount','trans_points','mbr_savings','salesperson_id')->where(['mbr_id'=>$id, 'coy_id'=>'CTL'])->orderBy('trans_date', 'desc')->get();
        $mbr_trans_his = DB::connection('pgsql')->table('crm_member_trans_history')->select('trans_id', 'trans_type', 'trans_date', 'loc_id', 'pos_id', 'item_id', 'item_desc', 'item_qty', 'regular_price', 'unit_price','disc_percent','disc_amount','trans_points','mbr_savings','salesperson_id')->where(['mbr_id'=>$id, 'coy_id'=>'CTL'])->orderBy('trans_date', 'desc')->get();
        $mbr_transaction = $mbr_trans->merge($mbr_trans_his)->all();
        $rebate_claim = DB::connection('pgsql')->table('crm_rebate_claim')->selectRaw('last_renewal, last_expiry, claim_expiry, claim_entitled, mbr_savings, line_num')->whereRaw('mbr_id=? and coy_id =\'CTL\' and (rtrim(last_mbrtype)=\'M\' or rtrim(last_mbrtype)=\'M28\')',[$id])->orderBy('last_expiry', 'desc')->first();

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'address', 'delv_addr', 'mbr_type', 'mbr_points', 'mbr_transaction', 'mbr_voucher', 'rebate_claim'));
        */
    }

    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $now = Carbon::now();

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::connection('pgsql')->table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        $address = DB::connection('pgsql')->select('SELECT b.postal_code, rtrim(b.street_line1) street_line1, rtrim(b.street_line2) street_line2, rtrim(b.street_line3) street_line3, rtrim(b.street_line4) street_line4, b.country_id FROM crm_member_list a JOIN coy_address_book b ON a.mbr_id = b.ref_id AND (CASE WHEN a.mbr_addr=\'\' THEN \'0-PRIMARY\' ELSE a.mbr_addr END) = b.addr_type WHERE a.mbr_id = ?', [$id]);
//        if (rtrim($dataTypeContent['mbr_addr']) != '') {
//            $address = DB::connection('pgsql')->select('SELECT b.postal_code, rtrim(b.street_line1) street_line1, rtrim(b.street_line2) street_line2, rtrim(b.street_line3) street_line3, rtrim(b.street_line4) street_line4, b.country_id FROM crm_member_list a JOIN coy_address_book b ON a.mbr_id = b.ref_id AND (CASE WHEN a.mbr_addr=\'\' THEN \'0-PRIMARY\' ELSE a.mbr_addr END) = b.addr_type WHERE a.mbr_id = ?', [$id]);
//        } else {
//            $address = '';
//        }

        if (rtrim($dataTypeContent['mbr_addr']) != '') {
            $delv_addr = DB::connection('pgsql')->select('SELECT b.postal_code, rtrim(b.street_line1) street_line1, rtrim(b.street_line2) street_line2, rtrim(b.street_line3) street_line3, rtrim(b.street_line4) street_line4, b.country_id FROM crm_member_list a JOIN coy_address_book b ON a.mbr_id = b.ref_id AND (CASE WHEN a.delv_addr=\'\' THEN \'2-DELIVERY\' ELSE a.delv_addr END) = b.addr_type WHERE a.mbr_id = ?', [$id]);
        }else {
            $delv_addr = '';
        }

        $pos_login = DB::connection('pgsql')->table('crm_pos_login_log')->selectRaw('pos_id,source_count')->where('mbr_id', $id)->orderBy('pos_id', 'asc')->get();
        $mbr_points = DB::connection('pgsql')->table('crm_member_points')->selectRaw('points_accumulated,points_redeemed,points_expired, exp_date')->where('mbr_id', $id)->orderBy('exp_date', 'desc')->get();
        $mbr_type = DB::connection('pgsql')->table('crm_member_type')->selectRaw('eff_from, eff_to, rtrim(mbr_type) mbr_type')->orderBy('eff_to', 'desc')->where('mbr_id',$id)->get();
        $mbr_voucher = DB::connection('pgsql')->table('crm_voucher_list')->selectRaw('coupon_id, issue_date, expiry_date')->where(['coy_id'=>'CTL', 'mbr_id'=>$id, 'status_level'=>0])->where('expiry_date', '>', $now)->orderBy('expiry_date', 'asc')->get();
        $mbr_trans = DB::connection('pgsql')->table('crm_member_transaction')->select('trans_id', 'trans_type', 'trans_date', 'loc_id', 'pos_id', 'item_id', 'item_desc', 'item_qty', 'regular_price', 'unit_price','disc_percent','disc_amount','trans_points','mbr_savings','salesperson_id')->where(['mbr_id'=>$id, 'coy_id'=>'CTL'])->orderBy('trans_date', 'desc')->get();
        $mbr_trans_his = DB::connection('pgsql')->table('crm_member_trans_history')->select('trans_id', 'trans_type', 'trans_date', 'loc_id', 'pos_id', 'item_id', 'item_desc', 'item_qty', 'regular_price', 'unit_price','disc_percent','disc_amount','trans_points','mbr_savings','salesperson_id')->where(['mbr_id'=>$id, 'coy_id'=>'CTL'])->orderBy('trans_date', 'desc')->get();
        $mbr_transaction = $mbr_trans->merge($mbr_trans_his)->all();
        $asc_members = DB::connection('pgsql')->table('crm_member_list')->selectRaw('mbr_id,first_name,last_name,contact_num,email_addr,status_level,join_date')->where('main_id', $id)->orderBy('exp_date', 'desc')->get();
        $points_exp_date = $this->pointsExpiryRepository->getAllExpiryDate();
        $reminder = $this->memberMetaRepository->getRemainder($id);

        $rebate_claim = DB::connection('pgsql')->table('crm_rebate_claim')->selectRaw('last_renewal, last_expiry, claim_expiry, claim_entitled, mbr_savings, line_num')->whereRaw('mbr_id=? and coy_id =\'CTL\' and (rtrim(last_mbrtype)=\'M\' or rtrim(last_mbrtype)=\'M28\')',[$id])->orderBy('last_expiry', 'desc')->first();
//        if ($rebate_claim->exists()){
//            $rebate_claim = $rebate_claim->first();
//        } else {
//            $rebate_claim = '';
//        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'address', 'delv_addr', 'mbr_type', 'mbr_points', 'mbr_transaction', 'mbr_voucher', 'rebate_claim', 'asc_members', 'pos_login', 'points_exp_date', 'reminder'));
//        return $this->log2dynamoDB($id);
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        //for manual upload transaction
        //upload_transaction
//        $input = request()->all();
//        if(isset($input['upload_transaction'])){
//            return back()->with([
//                'message'    => 'success pressed on manual upload',
//                'alert-type' => 'success',
//            ]);
//        }else{
//            return back()->with([
//                'message'    => 'failed',
//                'alert-type' => 'error',
//            ]);
//        }

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        $id = rtrim($id);




        $input = request()->only(['contact_num', 'email_addr', 'first_name', 'last_name','status_level', 'birth_date', 'sub_ind1', 'sub_ind2', 'primary_details', 'delivery_details', 'mbr_pwd', 'anonymous', 'trans_id', 'upload_transaction']);
        $rules = [
            'contact_num'   => 'required|numeric',
            'email_addr'     => 'required|email',
            'first_name' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return back()->with([
                'message'    => $errors,
                'alert-type' => 'error',
            ]);
        }
//        if (strlen($input['contact_num'])<8) {
//            return back()->with([
//                'message'    => 'Contact number must be at least 8 digit',
//                'alert-type' => 'error',
//            ]);
//        }
        if ($this->memberRepository->checkEmailUnique($id, $input['email_addr'])) {
            return back()->with([
                'message'    => 'Email must be unique',
                'alert-type' => 'error',
            ]);
        }
        if ($this->memberRepository->checkPhoneUnique($id, $input['contact_num'])) {
            return back()->with([
                'message'    => 'Contact number must be unique',
                'alert-type' => 'error',
            ]);
        }

        $now = date("Y-m-d H:i:s");
        DB::connection('pgsql')->beginTransaction();
        try {
            $user_id = Voyager::getId() ?? '';

            if(isset($input['upload_transaction'])){
                if(!isset($input['trans_id'])){
                    return back()->with([
                        'message'    => 'Warming! Receipt ID is needed!',
                        'alert-type' => 'error',
                    ]);
                }
                $trans_id = $input['trans_id'];
                $sys_list = DB::connection('pgsql')->table('crm_member_transaction')
                    ->where(['coy_id'=>'CTL','mbr_id'=>$id,'trans_id'=>$trans_id]);

                if ($sys_list->exists()) {
                    $message = "Warming: ".$trans_id." this transaction already existed";
                    $alert_type = "error";
                }else{
                    $url = $this->posApiUrl."/orders/".$trans_id."/reprint_receipt?token=01HQPlbhcsJGdURriLvoa4fh0Meu62Qf&exclude_voided=1";

                    $headers[] = "Accept-Encoding:gzip,deflate";
                    $headers[] = "Content-Type:application/json";
                    $headers[] = "Connection:Keep-Alive";

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => $headers
                    ));

                    $result = curl_exec($curl);
                    $output = json_decode($result,true);

                    if(isset($output["error"]["message"]) && $output["error"]["message"] =="Resource not found"){
                        $message = $output["error"]["message"];
                        $alert_type = "error";
                    }else{
                        if($output['customer_info'] === null){
                            $output['customer_info']['id'] = $id;
                        }
                        //$url = "https://vc8.api.valueclub.asia/api/transaction?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";
                        $url = $this->vc8ApiUrl."/api/transaction?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl";

                        $headers[] = "Accept-Encoding:gzip,deflate";
                        $headers[] = "Content-Type:application/json";
                        $headers[] = "Connection:Keep-Alive";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($output));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                        $response = curl_exec($ch);

                        if($response){
                            $res = json_decode($response, true);
                            $message = $res['info'] ?? $res['message'] ?? $response;
                            $alert_type = $res["status"];
                        }else{
                            $message = 'POST: error in curl';
                            $alert_type = 'error';
                        }

                        //insert query result in meta table
                        $vclub_db = DB::connection('pgsql');
                        $list['mbr_id'] = $id;
                        $list['meta_key'] = 'uploadTransaction';
                        $list['meta_value']="message=".$message."; trans_id=".$trans_id;
                        $list['created_by']=$user_id;
                        $list['created_on']=date("Y-m-d h:i:sa");
                        $list['modified_by']=$user_id;
                        $list['modified_on']=date("Y-m-d h:i:sa");
                        $vclub_db->table('crm_member_meta')->insert($list);

                        curl_close($ch);
                    }
                }

                return back()->with([
                    'message'    => $message,
                    'alert-type' => $alert_type,
                ]);
            }else {
                $sub_ind1 = isset($input['sub_ind1']) && $input['sub_ind1'] === 'on' ? 'Y' : 'N';
                $sub_ind2 = isset($input['sub_ind2']) && $input['sub_ind2'] === 'on' ? 'YYYYY' : 'NYYYY';
                if (isset($input['anonymous'])) { //set member to anonymous
                    $contact_num = base64_encode($input['contact_num']);
                    $email_addr = base64_encode($input['email_addr'] . date("Y-m-d h:i:sa"));
                    $first_name = base64_encode($input['first_name']);
                    $last_name = base64_encode($input['last_name']);
                    $birth_date = date('Y-m-d', strtotime($input['birth_date']));

                    $vclub_db = DB::connection('pgsql');
                    $list['mbr_id'] = $id;
                    $list['meta_key'] = 'anonymousMember';
                    $list['meta_value'] = "first_name=" . $first_name . "; last_name=" . $last_name . "; email_addr=" . $email_addr . "; 
                contact_num=" . $contact_num . "; birth_date=" . $birth_date;
                    $list['created_by'] = $user_id;
                    $list['created_on'] = date("Y-m-d h:i:sa");
                    $list['modified_by'] = $user_id;
                    $list['modified_on'] = date("Y-m-d h:i:sa");
                    $vclub_db->table('crm_member_meta')->insert($list);
                    //take partial data insert to crm_member_list
                    $contact_num = substr($contact_num, 0, 5);
                    $email_addr = substr($email_addr, -30) . "@email";
                    $first_name = substr($first_name, 0, 5);
                    $last_name = substr($last_name, 0, 5);
                    $birth_date = date('Y-01-01', strtotime($input['birth_date']));

                    //remove main id
                    $this->memberRepository->update([
                        'main_id' => ''
                    ], $id, $attribute = 'mbr_id');
                } else {
                    $contact_num = $input['contact_num'];
                    $email_addr = $input['email_addr'];
                    $first_name = $input['first_name'];
                    $last_name = isset($input['last_name']) ? $input['last_name'] : '';
                    $birth_date = date('Y-m-d', strtotime($input['birth_date']));
                }
                if (isset($input['mbr_pwd']) && !empty($input['mbr_pwd'])) {
                    $this->memberRepository->update([
                        'contact_num' => $contact_num,
                        'email_addr' => $email_addr,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'mbr_pwd' => md5($input['mbr_pwd']),
                        'pwd_changed' => $now,
                        'birth_date' => $birth_date,
                        'mbr_addr' => '0-PRIMARY',
                        'delv_addr' => '2-DELIVERY',
                        'sub_ind1' => $sub_ind1,
                        'sub_ind2' => $sub_ind2,
                        'modified_on' => $now,
                        'modified_by' => $user_id
                    ], $id, $attribute = 'mbr_id');
                } else {
                    $this->memberRepository->update([
                        'contact_num' => $contact_num,
                        'email_addr' => $email_addr,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        //'status_level'=> $input['status_level'],
                        'birth_date' => $birth_date,
                        'mbr_addr' => '0-PRIMARY',
                        'delv_addr' => '2-DELIVERY',
                        'sub_ind1' => $sub_ind1,
                        'sub_ind2' => $sub_ind2,
                        'modified_on' => $now,
                        'modified_by' => $user_id
                    ], $id, $attribute = 'mbr_id');
                }

                $this->addressRepository->updateMemberInfo($id, $input);
                DB::connection('pgsql')->commit();
            }
        } catch (\Exception $e) {
            DB::connection('pgsql')->rollBack();
            return back()->with([
                'message'    => 'Error: '.$e->getMessage(),
                'alert-type' => 'error',
            ]);
        }

        return back()->with([
            'message'    => "Success update member",
            'alert-type' => 'success',
        ]);
    }

    public function updateMemberType(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit_more', app($dataType->model_name));

        $input = request()->only(['mbr_id', 'mbr_type', 'status_level', 'exp_date']);
        $rules = [
            'mbr_type'   => 'required',
            'status_level'     => 'required',
            'exp_date' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return back()->with([
                'message'    => $errors,
                'alert-type' => 'error',
            ]);
        }
        $now = Carbon::now()->toDateTimeString();
        $today = Carbon::today()->toDateTimeString();
        $yst = Carbon::now()->subDay(1);
        $id = $input['mbr_id'];
        $mbr_type = $input['mbr_type'];
        if (($mbr_type === 'MST' || $mbr_type === 'MSTP') && $input['status_level'] === "1") {
            $email = $this->memberRepository->findByMbrId($id)->email_addr;
            $this->_getMSTUrl($email);
        }
        $exp_date = Carbon::parse($input['exp_date'] . '23:59:59')->toDateTimeString();
        DB::connection('pgsql')->beginTransaction();
        try {
            $user_id = Voyager::getId() ?? '';
            $this->memberRepository->update([
                'mbr_type' => $input['mbr_type'],
                'status_level'  => $input['status_level'],
                'exp_date'  => $input['exp_date'],
                'modified_on'  => $now,
                'modified_by' => $user_id
            ], $id, $attribute = 'mbr_id');

            $this->memberTypeRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $id,
                'eff_from' => $today,
                'eff_to' => $exp_date,
                'mbr_type' => $mbr_type,
                'created_by' => $user_id,
                'created_on' => $now,
                'modified_by' => $user_id,
                'modified_on' => $now
            ]);

            $data_update_eff_to = ['eff_to' => $now, 'modified_on' => $now];
            $this->memberTypeRepository->updateEffTo($id, $data_update_eff_to, $mbr_type, $yst);

            DB::connection('pgsql')->commit();
        } catch (\Exception $e) {
            DB::connection('pgsql')->rollBack();
            return back()->with([
                'message'    => 'Error: '.$e->getMessage(),
                'alert-type' => 'error',
            ]);
        }

        return back()->with([
            'message'    => "Success update member",
            'alert-type' => 'success',
        ]);
    }

    public function _getMSTUrl($email)
    {
        $url = "https://chstaff.challenger.sg/api/vc2/getResendLinkSTU/".$email;
//        $url = 'https://www.hachi.tech/api/vc2/getResendLinkSTU/' . $email;
        $header = array('X_AUTHORIZATION'=>'NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, array('headers'=>$header));
        $response = $request->getBody()->getContents();
        $mst_url = explode("<br>", $response)[1];

        $client->get($mst_url);
    }

    public function resetPassword($email)
    {
//        https://chstaff.challenger.sg/api/vc2/FPassEmail
        $url = 'https://chstaff.challenger.sg/api/vc2/FPassEmail/' . $email;
//        $url = 'https://www.hachi.tech/api/member2/password-reset?coy=CTL&email=' . $email;
        $header = array('X_AUTHORIZATION'=>'ZD-567c7aaf990628d7d16e3c50026d24fa','Content-type'=>'application/json, text/plain');
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, array('headers'=>$header));
        $response = json_decode($request->getBody()->getContents(), true);
        $url = $response["result"]["url"];
        $code = $response["result"]["new_code"];
        return $this->successWithData([
            'code' => $code,
            'url' => $url
        ]);
    }

    public function points_award(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit_more', app($dataType->model_name));

        $input = request()->only(['mbr_id', 'points_amount', 'points_description', 'trans_ref', 'exp_date']);
        $rules = [
            'mbr_id' => 'required',
            'points_description' => 'required',
            'points_amount' => 'required|numeric',
            'exp_date' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return back()->with([
                'message'    => $errors,
                'alert-type' => 'error',
            ]);
        }
        $mbr_id = $input['mbr_id'];
        $award_points = $input['points_amount'] * 100;
        $rebate_desc = $input['points_description'];
        $trans_id = $input['trans_ref'];
        $exp_date = $input['exp_date'];

        if ($input['points_amount']<0) {
            $award_error = $this->_points_deduct($mbr_id,$award_points,$rebate_desc,$trans_id, $exp_date);
        }
        else {
            $award_error = $this->_points_award($mbr_id,$award_points,$rebate_desc,$trans_id, $exp_date);
        }

        if ($award_error=='') {
            return back()->with([
                'message' => "Success save awards",
                'alert-type' => 'success',
            ]);
        }
        else {
            return back()->with([
                'message'    => $award_error,
                'alert-type' => 'error',
            ]);
        }
    }

    private function _points_award($mbr_id,$award_points,$rebate_desc,$trans_id, $exp_date){
        $now = Carbon::now();
        $user = Voyager::getId();

        if ($trans_id=='') {
            $trans_id = $this->getTransId('AP');
            $trans_line_num = 1;
        }
        else {
            $trans_list = DB::connection('pgsql')->table('crm_member_transaction')
                                ->select( DB::raw('count(*) as line_num') )
                                ->where(['trans_id'=>$trans_id, 'mbr_id'=>$mbr_id, 'coy_id'=>'CTL'])->get();
            $trans_line_num = $trans_list[0]->line_num + 1;
        }

        DB::connection('pgsql')->beginTransaction();
        try {
            // create transaction
            $this->transactionRepository->create([
                'coy_id' => 'CTL',
                'mbr_id' => $mbr_id,
                'trans_id' => $trans_id,
                'trans_type' => 'AP',
                'trans_date' => $now,
                'loc_id' => '',
                'line_num' => $trans_line_num,
                'pos_id' => '',
                'item_id' => '!VCH-STAR-VCCMS',
                'item_desc' => $rebate_desc,
                'item_qty' => 0,
                'regular_price' => 0,
                'unit_price' => 0,
                'disc_percent' => 0,
                'disc_amount' => 0,
                'trans_points' => $award_points,
                'salesperson_id' => $user,
                'mbr_savings' => 0,
                'created_by' => $user,
                'created_on' => $now,
                'modified_by' => $user,
                'modified_on' => $now,
                'updated_on' => $now,
            ]);
            // create/update crm_member_points and update crm_member_list
//            $exp_date = $this->pointsExpiryRepository->getNextExpiryDate();
            $member = $this->memberRepository->findByMbrId($mbr_id);
            if (rtrim($member->mbr_type, " ") == 'MAS') {
                $main_id = $member->main_id;
                $points = Points::where(['coy_id' => 'CTL', 'mbr_id' => $main_id, 'exp_date' => $exp_date]);
                $main_member = $this->memberRepository->findByMbrId($main_id);
                if ($points->exists()) {
                    $points->increment('points_accumulated', $award_points);
                }
                else {
                    $data_points = [
                        'coy_id' => 'CTL',
                        'mbr_id' => $main_id,
                        'exp_date' => $exp_date,
                        'points_accumulated' => $award_points,
                        'points_redeemed' => 0,
                        'points_expired' => 0,
                        'created_by' => $user,
                        'created_on' => $now,
                        'modified_by' => $user,
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ];
                    $this->pointsRepository->create($data_points);
                }
                $main_member->increment('points_accumulated', $award_points);
            }
            else {
                $points = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
                if ($points->exists()) {
                    $points->increment('points_accumulated', $award_points);
                } else {
                    $data_points = [
                        'coy_id' => 'CTL',
                        'mbr_id' => $mbr_id,
                        'exp_date' => $exp_date,
                        'points_accumulated' => $award_points,
                        'points_redeemed' => 0,
                        'points_expired' => 0,
                        'created_by' => $user,
                        'created_on' => $now,
                        'modified_by' => $user,
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ];
                    $this->pointsRepository->create($data_points);
                }
                $member->increment('points_accumulated', $award_points);
            }

        }
        catch (\Exception $e) {
            //Log::error($e);
            DB::connection('pgsql')->rollBack();

            return 'Error: '.$e->getMessage(); // Fail
        }
        DB::connection('pgsql')->commit();

        return ''; // Success
    }

    private function _points_deduct($mbr_id,$award_points,$rebate_desc,$trans_id, $exp_date) {
        $now = Carbon::now();
        $user = Voyager::getId();

        if ($trans_id=='') {
            $trans_id = $this->getTransId('AP');
            $trans_line_num = 1;
        }
        else {
            $trans_list = DB::connection('pgsql')->table('crm_member_transaction')
                ->select( DB::raw('count(*) as line_num') )
                ->where(['trans_id'=>$trans_id, 'mbr_id'=>$mbr_id, 'coy_id'=>'CTL'])->get();
            $trans_line_num = $trans_list[0]->line_num + 1;
        }

        DB::connection('pgsql')->beginTransaction();
        try {
            // create/update crm_member_points and update crm_member_list
            $member = $this->memberRepository->findByMbrId($mbr_id);

            $points = Points::where(['coy_id' => 'CTL', 'mbr_id' => $mbr_id, 'exp_date' => $exp_date]);
            if ($points->exists()) {
                if ($points->first()->points_accumulated > abs($award_points)) {
                    // create transaction
                    $this->transactionRepository->create([
                        'coy_id' => 'CTL',
                        'mbr_id' => $mbr_id,
                        'trans_id' => $trans_id,
                        'trans_type' => 'AP',
                        'trans_date' => $now,
                        'loc_id' => '',
                        'line_num' => $trans_line_num,
                        'pos_id' => '',
                        'item_id' => '!VCH-STAR-VCCMS',
                        'item_desc' => $rebate_desc,
                        'item_qty' => 0,
                        'regular_price' => 0,
                        'unit_price' => 0,
                        'disc_percent' => 0,
                        'disc_amount' => 0,
                        'trans_points' => $award_points,
                        'salesperson_id' => $user,
                        'mbr_savings' => 0,
                        'created_by' => $user,
                        'created_on' => $now,
                        'modified_by' => $user,
                        'modified_on' => $now,
                        'updated_on' => $now,
                    ]);
                    $points->increment('points_accumulated', $award_points);
                    $member->increment('points_accumulated', $award_points);
                } else {
                    return 'Reduction rebate(S$'.(string)(abs($award_points)/100).') should less than accumulated(S$'.(string)($points->first()->points_accumulated / 100).').';
                }
            } else {
                return 'Reduction rebate should less than accumulated.';
            }
        }
        catch (\Exception $e) {
            //Log::error($e);
            DB::connection('pgsql')->rollBack();

            return 'Error: '.$e->getMessage();
        }
        DB::connection('pgsql')->commit();

        return '';
    }

    public function getTransId($trans_type)
    {
        $date = Carbon::now()->format('Ym');
        if ($trans_type === 'AP') {
            $prefix = 'ADJUSTAP'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'AP' . $date . sprintf("%07d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'AP' . $date . sprintf("%07d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTAP'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RD') {
            $prefix = 'ADJUSTRD'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'RDUBI' . $date . sprintf("%04d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'RDUBI' . $date . sprintf("%04d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTRD'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RV') {
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','REBATE');
            $next_num = $sys_list->first()->next_num;
            $trans_id = 'RV' . sprintf("%08s", $next_num);
            $sys_list->update([
                'next_num'=>$next_num+1
            ]);
        }
        else {
            $prefix = 'ADJUSTEPUBI'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'EPUB' . $date . sprintf("%05d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'EPUB' . $date . sprintf("%05d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTEPUBI'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        return $trans_id;
    }

    // Voucher
    public function showMemberVoucher(Request $request, $id)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = 'voucher-list';

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit_more_voucher', app("App\Models\User"));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*')->where('mbr_id', $id);
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        $transaction = $this->transactionRepository->getMemberTransaction($id);

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'orderBy',
            'sortOrder',
            'id',
            'transaction'
        ));
    }

    public function editMemberVoucher(Request $request, $id, $couponId)
    {
        $slug = 'voucher-list';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

//        if (strlen($dataType->model_name) != 0) {
//            $model = app($dataType->model_name);
//
//            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
//            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
//                $model = $model->withTrashed();
//            }
//            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
//                $model = $model->{$dataType->scope}();
//            }
//            $dataTypeContent = call_user_func([$model, 'findOrFail'], [$id, $couponId]);
//        } else {
            // If Model doest exist, get data from table name
        $dataTypeContent = $this->voucherRepository->editVoucher($id, $couponId);
//        }
//        dd($dataTypeContent);

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit_more_voucher', app("App\Models\User"));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function updateMemberVoucher(Request $request, $id, $couponId)
    {
        $slug = 'voucher-list';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        // Check permission
        $this->authorize('edit_more_voucher', app("App\Models\User"));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();

        $expiry_date = date('Y-m-d 23:59:59', strtotime(request()->expiry_date)); // Carbon::parse(request()->expiry_date . ' 23:59:59');

        $res = $this->voucherRepository->updateMemberVoucher($id, $couponId, $expiry_date);

        if ($res) {
            $redirect = redirect()->route("voyager.member-list.show_member_voucher", ['id'=>$id]);

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        }
        else {
            return back()->with([
                'message'    => "Error update voucher",
                'alert-type' => 'error',
            ]);
        }


    }

    // Member Meta Reminder
    public function reminder(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit_more', app($dataType->model_name));

        $input = request()->only(['mbr_id', 'meta_value']);
        $rules = [
            'mbr_id'   => 'required',
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return back()->with([
                'message'    => $errors,
                'alert-type' => 'error',
            ]);
        }
        $meta_value = rtrim($input['meta_value']);
        $mbr_id = $input['mbr_id'];
        $user_id = Voyager::getId() ?? '';
        $res = $this->memberMetaRepository->updateOrInsertReminder($mbr_id, $meta_value, $user_id);
        if ($res) {
            return back()->with([
                'message'    => "Success update reminder",
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => "Error update reminder",
                'alert-type' => 'error',
            ]);
        }
    }

    // Member Transaction Details

    public function showTransaction($mbr_id, $trans_id)
    {
//        dd('test');
        $trans_id = strtoupper($trans_id);

        $trans = $this->posService->get_transaction($trans_id);
//        dd($trans);
        if ($trans == 404) {
            return back()->with([
                'message'    => 'Can not find transaction record.',
                'alert-type' => 'error',
            ]);
        }
        if ($trans['order_status_level']==0) {
            $trans['order_status'] = 'Suspended';
        } else if ($trans['order_status_level']==-1) {
            $trans['order_status'] = 'Voided';
        } else {
            $trans['order_status'] = 'Paid';
        }

        $payments_data = $trans['transactions'];
        $payments = [];
        $payment_cash = 0;
//        dd($payments_data);
        foreach($payments_data as $payment){
            if ($payment['pay_mode'] == 'CASH' || $payment['pay_mode'] == 'CASH_CHANGE_DUE' || $payment['pay_mode'] == 'CASH_ROUNDING_ADJ') {
                $payment_cash += $payment['trans_amount'];
            }
            else {
                $trans_desc = '';
                if (!empty($payment['custom_data'])) {
                    $trans_desc = isset($payment['custom_data']['terminal']) ? $payment['custom_data']['terminal']['card_number'] : '';
                    $trans_desc.= ' '. $payment['approval_code'] .' '. $payment['transaction_reference_id'];
                }
                $payments[] = [
                    'pay_desc'      => $payment['pay_mode'] . ' ' . $payment['pay_type'],
                    'trans_desc'    => $trans_desc,
                    'trans_amount'  => $payment['trans_amount'],
                    'data'          => $payment
                ];
            }
        }
        if ($payment_cash > 0) {
            $payments[] = [
                'pay_desc'      => 'CASH',
                'trans_desc'    => '',
                'trans_amount'  => $payment_cash,
                'data'          => []
            ];
        }

        $dataTypeContent = $trans;
//        $dataTypeContent['items'][0]['lot_id'] = '';
        $dataTypeContent['transactions'] = $payments;

        // Deposit need more info from another API
//        if ( $trans['order_type'] =="DM") {
//            $receipt = $this->posTransactionService->receiptReprint($trans_id);
//            $dataTypeContent['deposit'] = 'Customer: ' . $receipt['deposit']['name'].' ('.$receipt['deposit']['contact'].')';
//            if (isset($receipt['deposit']['items'])) {
//                foreach ($receipt['deposit']['items'] as $itm){
//                    $dataTypeContent['deposit'] .= '<br>- ' . $itm;
//                }
//            }
//        }

//        $dataTypeContent['tax_invoice'] = OrderAddress::where('order_id',$order_id)->where('address_type','CASH_INV')->first();

        $view = 'voyager::transaction-list.read';
//        dd('JAY123',$dataTypeContent);
//        dd($view);
        return Voyager::view($view, compact( 'dataTypeContent'));
    }

    public function uploadTransaction(Request $request, $id){
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        $id = rtrim($id);
        $input = request()->only(['trans_id']);
        $rules = [
            'trans_id' => 'required'
        ];
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors($validator)->first();
            return back()->with([
                'message'    => $errors,
                'alert-type' => 'error',
            ]);
        }

        $now = date("Y-m-d H:i:s");
        DB::connection('pgsql')->beginTransaction();
        try {
            $user_id = Voyager::getId() ?? '';
            $trans_id = $input['trans_id'];
            $message = "";
            $alert_type = "";

            $sys_list = DB::connection('pgsql')->table('crm_member_transaction')
                ->where(['coy_id'=>'CTL','mbr_id'=>$id,'trans_id'=>$trans_id]);
            if ($sys_list->exists()) {
                $message = "Warming: ".$trans_id." this transaction already existed";
                $alert_type = "error";
            }else{
                //call POS API - GET transaction
                //call vclub API - POST transaction
                return back()->with([
                    'message'    => 'call success',
                    'alert-type' => 'success',
                ]);
            }

            //insert query result in meta table
            $vclub_db = DB::connection('pgsql');
            $list['mbr_id'] = $id;
            $list['meta_key'] = 'uploadTransaction';
            $list['meta_value']="message=".$message."; trans_id=".$trans_id;
            $list['created_by']=$user_id;
            $list['created_on']=date("Y-m-d h:i:sa");
            $list['modified_by']=$user_id;
            $list['modified_on']=date("Y-m-d h:i:sa");
            $vclub_db->table('crm_member_meta')->insert($list);

            return back()->with([
                'message'    => $message,
                'alert-type' => $alert_type,
            ]);

//            DB::connection('pgsql')->commit();
        } catch (\Exception $e) {
            DB::connection('pgsql')->rollBack();
            return back()->with([
                'message'    => 'Error: '.$e->getMessage(),
                'alert-type' => 'error',
            ]);
        }

//        return back()->with([
//            'message'    => "Success uploaded transaction",
//            'alert-type' => 'success',
//        ]);
    }

    private function log2dynamoDB($id)
    {
        $mbr_id = $id;
        $staff_id = Voyager::getId();
//        $credentials = new Aws\Credentials\Credentials('key', 'secret');

        return $staff_id;
    }


    public function showMvipSpending(Request $request){

        return "hello world";
    }

}
