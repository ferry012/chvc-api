<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Member\RebateClaim;
use App\Repositories\Member\MemberRepository;
use App\Repositories\Member\MemberTypeRepository;
use App\Repositories\Member\Points\PointsRepository;
use App\Repositories\Member\Transaction\TransactionRepository;
use App\Repositories\Member\Voucher\VoucherRepository;
use Carbon\Carbon;
use CHG\Voyager\Http\Controllers\VoyagerBaseController;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use CHG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Schema;

class RebateClaimController extends VoyagerBaseController
{

    private $transactionRepository;
    private $voucherRepository;
    private $memberRepository;
    private $pointsRepository;

    public function __construct(TransactionRepository $transactionRepository,
                                VoucherRepository $voucherRepository,
                                MemberRepository $memberRepository,
                                PointsRepository $pointsRepository)
    {
        $this->transactionRepository = $transactionRepository;
        $this->voucherRepository = $voucherRepository;
        $this->memberRepository = $memberRepository;
        $this->pointsRepository = $pointsRepository;
    }
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = Schema::connection('pgsql')->getColumnListing('crm_rebate_claim');
//            $searchable = array_diff($searchable, ['coy_id', 'line_num', 'rebate_voucher', 'created_by', 'created_on', 'modified_by', 'modified_on', 'updated_on']);
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $displayName = $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name');
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->details->scope && $dataType->details->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->details->scope))) {
                $query = $model->{$dataType->details->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) ) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'iLIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }
            else {
                $now = Carbon::now();
                $query->where('claim_expiry', '>', $now)->where('claim_entitled', 'R');
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::connection('pgsql')->table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->details->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                if ($action != "CHG\Voyager\Actions\DeleteAction"){
                    $action = new $action($dataType, $dataTypeContent->first());

                    if ($action->shouldActionDisplayOnDataType()) {
                        $actions[] = $action;
                    }
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
//        if (Voyager::canOrFail('rebate')) {
//            $showCheckboxColumn = true;
//        } else {
//            foreach ($actions as $action) {
//                if (method_exists($action, 'massAction')) {
//                    $showCheckboxColumn = true;
//                }
//            }
//        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function claim()
    {
        $input = request()->only('id', 'line');
        $id = $input['id'];
        $line = $input['line'];
        $claim = RebateClaim::where(['mbr_id'=>$id, 'line_num'=>$line]);
        if ($claim->exists()){
//            $last_expiry = $claim->first()->last_expiry;
            $last_expiry = Carbon::now()->addDays(7);
            $claim->update([
                'claim_entitled' => 'R',
                'claim_expiry' => $last_expiry,
                'modified_on' => Carbon::now()->toDateTimeString(),
            ]);
            return back()->with([
                'message'    => 'Success submit rebate claim',
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => 'Can not find rebate record.',
                'alert-type' => 'error',
            ]);
        }
    }

    public function claim_approval()
    {
        $input = request()->only('id', 'line');
        $id = $input['id'];
        $line = $input['line'];
        $claim = RebateClaim::where(['mbr_id'=>$id, 'line_num'=>$line]);
        if ($claim->exists()){
            $claim_expiry = $claim->first()->claim_expiry;
            $last_expiry = $claim->first()->last_expiry;
            if (Carbon::now() > Carbon::parse($claim_expiry)->addDays(7)){
                return back()->with([
                    'message'    => 'Sorry, this claim has expired.',
                    'alert-type' => 'error',
                ]);
            }
            DB::connection('pgsql')->beginTransaction();
            try {
                $user_id = Voyager::getId() ?? '';
                $claim->update([
                    'claim_entitled' => 'A',
                    'modified_on' => Carbon::now()->toDateTimeString(),
                    'modified_by' => $user_id
                ]);

                $next_num = DB::connection('pgsql')->table('sys_trans_list')->where('trans_prefix', 'REBATE')->first()->next_num;
                $voucher_id = 'RV' . sprintf("%08s", $next_num);
                DB::connection('pgsql')->table('sys_trans_list')->where('trans_prefix', 'REBATE')->update([
                    'next_num' => (int)$next_num + 1
                ]);
                // crm_voucher_list
                $now = Carbon::now()->toDateTimeString();
                $this->voucherRepository->saveModel([
                    'coy_id' => 'CTL',
                    'coupon_id' => '!VCH-REBATE',
                    'coupon_serialno' => $voucher_id,
                    'promocart_id' => '!VCH-REBATE',
                    'mbr_id' => $id,
                    'trans_id' => '',
                    'trans_date' => $now,
                    'ho_ref' => '',
                    'ho_date' => $now,
                    'issue_date' => $now,
                    'sale_date' => $now,
                    'utilize_date' => $now,
                    'expiry_date' => Carbon::now()->endOfDay()->addDays(90)->toDateTimeString(),
                    'print_date' => $now,
                    'loc_id' => 'UBI',
                    'receipt_id1' => $voucher_id,
                    'receipt_id2' => '',
                    'voucher_amount' => 40,
                    'redeemed_amount' => 0,
                    'expired_amount' => 0,
                    'issue_type' => 'eVoucher',
                    'issue_reason' => 'REBATE CLAIM',
                    'status_level' => 0,
                    'created_by' => $user_id,
                    'created_on' => $now,
                    'modified_by' => $user_id,
                    'modified_on' => $now
                ]);

                // crm_member_transaction
                $this->transactionRepository->saveModel([
                    'coy_id' => 'CTL',
                    'mbr_id' => $id,
                    'trans_id' => $voucher_id,
                    'line_num' => 1,
                    'trans_type' => 'RD',
                    'trans_date' => $now,
                    'loc_id' => 'UBI',
                    'pos_id' => '',
                    'item_id' => '!VCH-REBATE',
                    'item_desc' => '$40 Rebate e-voucher',
                    'item_qty' => 1,
                    'regular_price' => 0,
                    'unit_price' => 0,
                    'disc_percent' => 0,
                    'disc_amount' => 0,
                    'trans_points' => 0,
                    'salesperson_id' => $user_id,
                    'mbr_savings' => 0,
                    'created_by' => $user_id,
                    'created_on' => $now,
                    'modified_by' => $user_id,
                    'modified_on' => $now,
                    'updated_on' => $now,
                ]);

                // clear points
                $member = $this->memberRepository->findByMbrId($id);
                $points_accumulated = $member->points_accumulated;
                $points_redeemed = $member->points_redeemed;
                $points_expired = $member->points_expired;
                $mbr_savings = $member->mbr_savings;
                if ($member->exp_date == $last_expiry) {
                    $this->clearExpiredPoints($id, $points_accumulated,$points_redeemed,$points_expired,$mbr_savings);
                }
                else {
                    $remained_points = $this->transactionRepository->calculatePoints($id, $last_expiry);
                    $reduce_points = $points_accumulated-$points_redeemed-$points_expired-$remained_points;
                    // crm_member_transaction
                    $item_desc = 'Accum:'.$points_accumulated.'; Redeem:'.$points_redeemed.'; Expired:'.$points_expired.'; Savings:'.$mbr_savings;
                    if ($remained_points > 0) {
                        $ep_trans_id = $this->getTransId('EP');
                        $this->transactionRepository->createPointsTransaction($now,$id,$ep_trans_id, 'EP',$item_desc,$reduce_points);
                    }
                    // crm_member_list
                    $member->increment('points_expired', $reduce_points);
                    // crm_member_points
                    $exp_points = $this->pointsRepository->approvalRebateClaim($id);
                    foreach ($exp_points as $k=>$v) {
                        if ($reduce_points > 0) {
                            $remain_point = (int)number_format($v['points_accumulated'] - $v['points_redeemed']-$v['points_expired'], 2, '.', '');
                            if ($remain_point > $reduce_points) {
                                $this->pointsRepository->addExpiredPoints($id,$v['line_num'],$reduce_points);
                                $reduce_points = 0;
                            } else {
                                $this->pointsRepository->addExpiredPoints($id,$v['line_num'],$remain_point);
                                $reduce_points = number_format($reduce_points - $remain_point, 2, '.', '');
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                DB::connection('pgsql')->rollBack();
                return back()->with([
                    'message'    => 'Error: '.$e->getMessage(),
                    'alert-type' => 'error',
                ]);
            }
            DB::connection('pgsql')->commit();
            return redirect()->route("voyager.rebate-claim.index")->with([
                'message'    => 'Success approval rebate claim',
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => 'Can not find rebate record.',
                'alert-type' => 'error',
            ]);
        }

    }

    public function claim_reject()
    {
        $input = request()->only('id', 'line');
        $id = $input['id'];
        $line = $input['line'];
        $claim = RebateClaim::where(['mbr_id'=>$id, 'line_num'=>$line]);
        if ($claim->exists()){
            $now = Carbon::now();
            $claim_expiry = $claim->first()->claim_expiry;
            if ($now > Carbon::parse($claim_expiry)->addDays(7)){
                return back()->with([
                    'message'    => 'Sorry, this claim has expired.',
                    'alert-type' => 'error',
                ]);
            }
            $claim->update([
                'claim_entitled' => 'Z',
                'modified_on' => Carbon::now()->toDateTimeString(),
            ]);
            return back()->with([
                'message'    => 'Success reject rebate claim',
                'alert-type' => 'success',
            ]);
        } else {
            return back()->with([
                'message'    => 'Can not find rebate record.',
                'alert-type' => 'error',
            ]);
        }

    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::connection('pgsql')->table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted'));
    }

    /**
     * Order BREAD items.
     *
     * @param string $table
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (!isset($dataType->order_column) || !isset($dataType->order_display_column)) {
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::bread.ordering_not_set'),
                    'alert-type' => 'error',
                ]);
        }

        $model = app($dataType->model_name);
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $model = $model->withTrashed();
        }
        $results = $model->orderBy($dataType->order_column, $dataType->order_direction)->get();

        $display_column = $dataType->order_display_column;

        $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->whereField($display_column)->first();

        $view = 'voyager::bread.order';

        if (view()->exists("voyager::$slug.order")) {
            $view = "voyager::$slug.order";
        }

        return Voyager::view($view, compact(
            'dataType',
            'display_column',
            'dataRow',
            'results'
        ));
    }

    public function update_order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $model = app($dataType->model_name);

        $order = json_decode($request->input('order'));
        $column = $dataType->order_column;
        foreach ($order as $key => $item) {
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $i = $model->withTrashed()->findOrFail($item->id);
            } else {
                $i = $model->findOrFail($item->id);
            }
            $i->$column = ($key + 1);
            $i->save();
        }
    }

    public function action(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $action = new $request->action($dataType, null);

        return $action->massAction(explode(',', $request->ids), $request->headers->get('referer'));
    }

    public function clearExpiredPoints($mbr_id, $points_accumulated, $points_redeemed, $points_expired, $mbr_savings)
    {
        $now = Carbon::now();
        // crm_member_transaction
        $item_desc = 'Accum:'.$points_accumulated.'; Redeem:'.$points_redeemed.'; Expired:'.$points_expired.'; Savings:'.$mbr_savings;

        if ($points_accumulated > 0) {
            $ap_trans_id = $this->getTransId('AP');
            $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ap_trans_id, 'AP',$item_desc,-$points_accumulated);
        }
        if ($points_redeemed > 0) {
            $rd_trans_id = $this->getTransId('RD');
            $this->transactionRepository->createPointsTransaction($now,$mbr_id,$rd_trans_id, 'RD',$item_desc,$points_redeemed);
        }
        if ($points_expired > 0) {
            $ep_trans_id = $this->getTransId('EP');
            $this->transactionRepository->createPointsTransaction($now,$mbr_id,$ep_trans_id, 'EP',$item_desc,$points_expired);
        }
        // crm_member_points
        $this->pointsRepository->clearPoints($mbr_id);
        // crm_member_list
        $this->memberRepository->reducePoints($mbr_id, 0, 0, 0, 0);
    }

    public function getTransId($trans_type)
    {
        $date = Carbon::now()->format('Ym');
        if ($trans_type === 'AP') {
            $prefix = 'ADJUSTAP'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'AP' . $date . sprintf("%07d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'AP' . $date . sprintf("%07d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTAP'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RD') {
            $prefix = 'ADJUSTRD'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'RDUBI' . $date . sprintf("%04d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'RDUBI' . $date . sprintf("%04d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTRD'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        elseif ($trans_type === 'RV') {
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','REBATE');
            $next_num = $sys_list->first()->next_num;
            $trans_id = 'RV' . sprintf("%08s", $next_num);
            $sys_list->update([
                'next_num'=>$next_num+1
            ]);
        }
        else {
            $prefix = 'ADJUSTEPUBI'.$date;
            $sys_list = DB::connection('pgsql')->table('sys_trans_list')
                ->where(['coy_id'=>'CTL','sys_id'=>'CRM'])->where('trans_prefix','like',$prefix.'%');
            if ($sys_list->exists()) {
                $trans_prefix = $sys_list->first()->next_num;
                $trans_id = 'EPUB' . $date . sprintf("%05d", $trans_prefix);
                $sys_list->update([
                    'next_num'=>$trans_prefix+1
                ]);
            } else {
                $trans_id = 'EPUB' . $date . sprintf("%05d", 1);
                DB::connection('pgsql')->table('sys_trans_list')
                    ->insert([
                        'coy_id'=>'CTL',
                        'sys_id'=>'CRM',
                        'trans_prefix'=>'ADJUSTEPUBI'.$date,
                        'next_num'=>2,
                        'sys_date'=>Carbon::now()->toDateTimeString()
                    ]);
            }
        }
        return $trans_id;
    }
}
