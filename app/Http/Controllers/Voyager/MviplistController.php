<?php

namespace App\Http\Controllers\Voyager;

use Carbon\Carbon;
use CHG\Voyager\Http\Controllers\VoyagerBaseController;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use CHG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Schema;

class MviplistController extends VoyagerBaseController
{

    public function __construct(){
    }
    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

    public function index(Request $request)
    {
        //coy_id	mbr_id	mbr_title	first_name	last_name	birth_date	nationality_id	email_addr	contact_num	last_login	status_level	join_date	exp_date	last_renewal	mbr_type	points_accumulated	points_reserved	points_redeemed	points_expired	mbr_savings	is_mvip	mvip_eff_from	mvip_eff_to	spending_since_mvip	mvip_eligibility	is_mvip_eligible	spending_past_year	spending_to_mvip
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $query = DB::connection('pgsql')->table('v_mbr_mvip')
            ->selectRaw('mbr_id, first_name, last_name, DATE(exp_date), mvip_eff_from,mvip_eff_to, spending_since_mvip,spending_past_year')
            ->where("mbr_type",'MVIP')->get();
        $search = (object) ['value' => $request->get('s')];
        if ($search->value != '') {
            $search_value = '%'.$search->value.'%';
            $query = DB::connection('pgsql')->table('v_mbr_mvip')
                ->selectRaw('mbr_id, first_name, last_name, DATE(exp_date),mvip_eff_from,mvip_eff_to, spending_since_mvip,spending_past_year')
                ->whereRaw("mbr_type = 'MVIP' and (first_name ilike '$search_value' or last_name ilike '$search_value'or mbr_id ilike '$search_value' or email_addr ilike '$search_value' or contact_num ilike '$search_value')")->get();
        }

        
        return Voyager::view('voyager::mvip-list.browse', compact(
            'query',
            'search'
        ));
    }

}
