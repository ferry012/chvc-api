<?php

namespace App\Http\Controllers\Voyager;

use App\Exports\PointsExport;
use App\Facades\Member\Points\Points;
use App\Http\Controllers\Result;
use App\Models\Member\Points\PointsSummary;
use App\Repositories\Member\Points\PointsSummaryRepository;
use Carbon\Carbon;
use CHG\Voyager\Http\Controllers\VoyagerBaseController;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use CHG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Validator;
use Excel;

class MemberPointsSummaryController extends VoyagerBaseController
{
    private $pointsSummaryRepository;
    use Result;

    public function __construct(PointsSummaryRepository $pointsSummaryRepository)
    {
        $this->pointsSummaryRepository = $pointsSummaryRepository;
    }


    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************


    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $s = $request->get('s');

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (Carbon::today()->month === 1) {
            $from = Carbon::today()->subYear()->firstOfYear()->toDateTimeString();
            $to = Carbon::today()->startOfYear()->toDateTimeString();
        } else {
            $from = Carbon::today()->startOfYear()->toDateTimeString();
            $to = Carbon::today()->subMonth()->endOfMonth()->toDateTimeString();
        }
        $search = (object) [
            'from' => isset($s['from']) ? Carbon::parse($s['from'])->startOfDay()->toDateTimeString() : $from,
            'to' => isset($s['to']) ? Carbon::parse($s['to'])->endOfDay()->toDateTimeString() : $to,
        ];
        if (isset($s)) {
            $from = Carbon::parse($from)->startOfDay()->toDateTimeString();
            $to = Carbon::parse($to)->endOfDay()->toDateTimeString();
            $model = app($dataType->model_name);

            $is_exists = $model->where(['date_start'=>$from, 'closing_date'=>$to])->exists();
            if (!$is_exists) {
                Points::pointsSummary($from, $to);
            }
            $dataTypeContent = PointsSummary::select('*')->where(['date_start'=>$from, 'closing_date'=>$to])->orderBy('line_num', 'ASC')->get();

        } else {
            $dataTypeContent = [];
            $search->from = $from;
            $search->to = $to;
        }

        // Check if server side pagination is enabled
        $isServerSide = 0;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'search',
            'isServerSide'
        ));
    }

    public function downloadFile(Request $request)
    {
        $s = $request->get('s');

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (Carbon::today()->month === 1) {
            $from = Carbon::today()->subYear()->firstOfYear();
            $to = Carbon::today()->startOfYear();
        } else {
            $from = Carbon::today()->startOfYear();
            $to = Carbon::today()->subMonth()->endOfMonth();
        }
        $from = isset($s['from']) ? Carbon::parse($s['from'])->startOfDay() : $from;
        $to = isset($s['to']) ? Carbon::parse($s['to'])->endOfDay() : $to;
        if ($from->lt($to)) {
            $format_from = $from->copy()->format('Ymd');
            $format_to = $to->copy()->format('Ymd');
            $from = $from->toDateTimeString();
            $to = $to->toDateTimeString();
            $res = $this->pointsSummaryRepository->checkExists($from, $to);
            if (!$res) {
                Points::pointsSummary($from, $to);
            }
            $file_name = 'Points_Summary_' . $format_from . '_' . $format_to;
            return \Excel::download(new PointsExport($from,$to), $file_name .'.xlsx');
        }
        else {
            return back()->with([
                'message'    => 'From Date must less than To Date',
                'alert-type' => 'error',
            ]);
        }
    }

    public function sendFileEmail()
    {
        if (Carbon::today()->month === 1) {
            $from = Carbon::today()->subYear()->firstOfYear();
            $to = Carbon::today()->startOfYear();
        } else {
            $from = Carbon::today()->startOfYear();
            $to = Carbon::today()->subMonth()->endOfMonth();
        }
        $format_from = $from->copy()->format('Ymd');
        $format_to = $to->copy()->format('Ymd');
        $from = $from->toDateTimeString();
        $to = $to->toDateTimeString();
        $res = $this->pointsSummaryRepository->checkExists($from, $to);
        if (!$res) {
            Points::pointsSummary($from, $to);
        }
        $file_name = 'Points_Summary_' . $format_from . '_' . $format_to;
        \Excel::store(new PointsExport($from, $to), $file_name .'.xlsx', 'public');
        $this->send_email($file_name, url('/') . "/storage/" . $file_name .'.xlsx');
        return $this->successWithInfo("Success send email");
    }

    public function send_email($subject, $file)
    {
        $options = array(
            "body" => json_encode([
                "subject" => $subject,
                "body" => "Hi Hau Jie, \n\nPlease refer attached.",
                "recipients" => "hau.jie@challenger.sg",
                "copy_recipients" => "yongsheng@challenger.sg;sebastine@challenger.sg;li.liangze@challenger.sg",
                "file_url" => $file
            ])
        );
        $options['headers']['Authorization'] = '8f413c39b4d1b4e7c584943df22e7a8c';
        $options['headers']['Content-Type'] = 'application/json';
        $endpoint = "https://ctlmailer.api.valueclub.asia/send";
        $client = new \GuzzleHttp\Client();
        try {
            $client->post($endpoint, $options);
        } catch (BadResponseException $e) {
            $e->getCode();
        }
    }

}
