# Exception coding

<style>
table th:first-of-type {
    width: 200px;
}
</style>
### Error code format description

| 01 | 003 | 01 |
|-----------|-----------|-----------|
|Login|Rest password API|Specific error|


### Login

| Error Code | Description |
|---------|----------------------|
| 100101 | Login: Username or password is wrong |
| 100102 | Login: Login locked |
| 100103 | Login: Member type is forbid |
| 100104 | Login: Member status is inactivated |
| 100201 | Logout: You do not login |
| 100301 | Reset Password: Validator failed |
| 100302 | Reset Password: Password incorrect |
| 100304 | Reset Password: Server breakdown |
| 100501 | Update Info: Server breakdown |
| 100502 | Update Info: Validate error|

