---
---
## Authorization

All API must be authenticated. Please request for Key code from Challenger Software Team for development.

<style>
table th:first-of-type {
    width: 200px;
}
</style>
### **URL AUTH:**

```shell
http://{api_url}?key_code={key}
````

### **HEADER AUTH:**

```shell
curl -X GET http://localhost/api/validate \
--header 'Authorization:Bearer {key}' \
--data '{"mbr_id":"V00000000"}'
````
