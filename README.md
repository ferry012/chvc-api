
# VC API & CMS
Mission critical backend API for Valueclub Membership system,
powering the Valueclub App, Challenger POS & websites and micro-sites.

Project is developed on PHP Laravel
*****

### Getting started on local development
Create .env file for Laravel environment (or copy from stage.env)
> cp ./docker/test.env ./.env

To build the docker project on your local machine:
> docker build -t chvcapi .

Run the container locally.
Use [bind mount](https://docs.docker.com/storage/bind-mounts/) to map local directory into the container:
On exit, container will remove automatically because we use --rm flag
> docker run -v $(pwd):/var/www/html -p 8090:80 --rm chvcapi

The container is now running with your local files from this directory.
Changes made to your local files will be immediately reflect on the container.

Before you can load the website from the container, libraries (i.e. composer or npm) must be run in this directory.

> composer install --ignore-platform-reqs \
> php -d memory_limit=512M /usr/local/bin/composer update

For this project, we've made some changes to the default Auth libraries. 
Copy and replace following the following files:
> cp ./docker/UserRepository.php ./vendor/laravel/passport/src/Bridge/ \
> cp ./docker/SessionGuard.php ./vendor/laravel/framework/src/Illuminate/Auth/ \
> cp ./docker/AuthenticatesUsers.php ./vendor/laravel/framework/src/Illuminate/Foundation/Auth/ \
> cp ./docker/Gate.php ./vendor/laravel/framework/src/Illuminate/Auth/Access/ \
> cp ./docker/login.blade.php ./vendor/chg/voyager/resources/views/login.blade.php \
> cp ./docker/password.ttf ./public/fonts/password.ttf

Update your local .env file:
1. Set the `APP_URL` to your localhost URL (take note to remove https)
2. Update `FORCE_SSL` to `FALSE`

Open browser to http://localhost:9000 to preview this site
*****

### Development & Github practice
Github Development Branch: **dev**

Do not commit the following files: \
composer.lock \
public\*

Always check before committing following files: \
Dockerfile
composer.json
docker\*

--

[//]: # (To Kill Docker Container)

[//]: # ()
[//]: # (> docker container ls)

[//]: # (> docker rm -f <container-name>)

[//]: # ()
[//]: # (OR )

[//]: # ()
[//]: # (> docker ps -a)


# Mike v1
docker login -u AWS -p $(aws ecr get-login-password --region ap-southeast-1 --profile default_challenger) 923763705594.dkr.ecr.ap-southeast-1.amazonaws.com 
docker pull 923763705594.dkr.ecr.ap-southeast-1.amazonaws.com/chvcapi:stage931
docker run -d -p 8082:80 -v $(pwd):/var/www/html:rw --name mychvcapi 923763705594.dkr.ecr.ap-southeast-1.amazonaws.com/chvcapi:stage931
docker exec -it <containerID> /bin/bash
rm -rf composer.lock && composer update
tail -f cat /var/log/apache2/access.log
apt-get install nano
nano /etc/apache2/sites-enabled/000-default.conf
Header set Access-Control-Allow-Origin "*"
apache2ctl restart

tail -f cat /var/log/apache2/error.log

