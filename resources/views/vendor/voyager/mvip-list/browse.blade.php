@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-people"></i> MVIP List
    </h1>
@stop

@section('page_header_actions')

@stop

@section('content')
<div class="page-content container-fluid">
    <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                    <div style="padding:0px 15px">
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <div class="input-group col-md-12">
                                        <input id="searchProdVal" type="text" class="form-control" placeholder="Search by ID / Email / Contact Num / Name" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>

{{--                            <div class="panel-body">--}}
{{--                                <input type="submit" name="downloadFile"--}}
{{--                                       class="btn pull-right btn-primary" value="Download CSV"--}}
{{--                                />--}}
{{--                            </div>--}}

                        <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>MVIP Expiry Date</th>
                                    <th>MVIP From</th>
                                    <th>MVIP To</th>
                                    <th>MVIP Spending Since</th>
                                    <th>MVIP Spending Last Year</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                   if(sizeof($query)!=0){
                                    foreach($query as $search_array){
                                        echo "<tr>";
                                        foreach($search_array as $search_result){
                                            echo "<td>".$search_result."</td>";
                                        }
                                        echo "<td></td></tr>";
                                    }
                                   }
                                   else{
                                       echo "<tr><td colspan='8'>No results found</td></tr>";
                                   }
                                    
                                ?>
                                
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>



 
            
    
@stop

@section('javascript')
    <!-- DataTables --> 
@stop
