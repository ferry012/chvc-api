<?php
//$userRights = Session::get('rights');
//$allow_edit = (in_array('OM', $userRights) || in_array('MIS_ADM', $userRights)) ? true : false;
//$adm_rights = (in_array('MIS_ADM', $userRights)) ? true : false;
?>

@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' Member Transaction')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-file-text"></i> {{ __('voyager::generic.viewing') }} Member Transaction &nbsp;

        <a href="{{ url()->previous() }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-8">

                <!-- form start -->
                <div class="invoice-box">
                    <table cellpadding="0" cellspacing="0">

                        <tr class="information">
                            <td colspan="4">
                                <table>
                                    <tr>
                                        <td align="left">
                                            <p>
                                                <strong>Receipt No:</strong> {{ $dataTypeContent['order_num'] }} ( {{ $dataTypeContent['order_type'] }} )<br>
                                                Store: {{ $dataTypeContent['loc_id'] }} {{ $dataTypeContent['pos_id'] }}<br>
                                                Date: {{ $dataTypeContent['trans_date'] }}<br>
                                                Cashier: {{ $dataTypeContent['cashier_id'] }}<br>
                                                @if($dataTypeContent['order_ref_id']!='')
                                                    Ref: {{ $dataTypeContent['order_ref_id'] }}
                                                @endif
                                            </p>
                                            @if($dataTypeContent['order_status_level']>=1)
                                                <span class="label label-success" style="font-size:14px"><strong>{{ $dataTypeContent['order_status'] }}</strong></span>
                                            @else
                                                <span class="label label-default" style="font-size:14px"><strong>{{ $dataTypeContent['order_status'] }}</strong></span>
                                            @endif
                                        </td>
                                        <td align="center"> </td>
                                        <td align="right">
                                            @if (!empty($dataTypeContent['customer_id']))
                                                <div style="width:300px;text-align: left">
                                                    Customer ID: <a href="http://localhost:8083/link-vc?s%5Bname%5D={{ $dataTypeContent['customer_id'] }}">{{ $dataTypeContent['customer_id'] }}</a><br>
                                                    Name: {{ $dataTypeContent['customer_name'] }}<br>
                                                    Contact: {{ $dataTypeContent['customer_phone'] }}<br>
                                                    Email: {{ $dataTypeContent['customer_email'] }}<br>
                                                    @if($dataTypeContent['order_status_level']>=1)
                                                        Member Savings: S$ {{ $dataTypeContent['total_savings'] }}<br>
                                                        Rebates Earned: S$ {{ number_format((float)$dataTypeContent['rebates_earned'], 2, '.', ',') }}
                                                    @endif
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="heading">
                            <td width="65%" align="left">Item List</td>
                            <td width="5%" align="center">Qty</td>
                            <td width="15%" align="right">Unit Price</td>
                            <td width="15%" align="right">Amount</td>
                        </tr>

                        @foreach($dataTypeContent['items'] as $index => $item)
                            <tr class="item">
                                <td align="left">
                                    {{ $item['item_id'] }}<br>
                                    <strong>{{ $item['item_desc'] }}</strong><br>
                                    @if($item['lot_id'] !== '' && (!empty($item['lot_id']) || $item['item_type']=='I'))
                                        <span class="label label-default" style="background-color: transparent; width:100px; display:inline-flex;">Serial Number:</span>
                                        @foreach($item['lot_id'] as $lot)
                                            <span class="label label-default">{{ $lot }}</span>
                                        @endforeach
                                        <br>
                                    @endif

                                    <span class="label label-default" style="background-color: transparent; width:100px; display:inline-flex;">Sales Person:</span>
                                    <br>

                                    @if($item['item_id']=='$DEP_MANUAL')
                                        <?php echo (isset($dataTypeContent['deposit'])) ? $dataTypeContent['deposit'] : ''; ?>
                                        <br>
                                    @endif

                                    <div style="padding-left:5px">
                                        @if($item['parent_id']>0)
                                            <span class="label label-primary">PWP</span>
                                        @endif
                                        @if(isset($item['custom_data']['price_edited']))
                                            @if($item['custom_data']['price_edited']==1&& $dataTypeContent['order_status_level']>=1 && $dataTypeContent['order_type']=='PS')
                                                @if(isset($item->price_edited_approval) && $item->price_edited_approval!='')
                                                    <span class="label label-primary">{{ 'by '.$item['price_edited_approval'] }}</span>
                                                @else
                                                    <span class="label label-primary">Price overwrite</span>
                                                @endif
                                            @endif
                                        @endif
                                        @if(isset($item['custom_data']->promo_id))
                                            <span class="label label-default">{{ $item['custom_data']->promo_id }}</span>
                                        @endif
                                        @if($item['lot_id'] !== '')
                                                @if( strpos($item['item_desc'],' ESD')>1 && count($item['lot_id'])==0 && $adm_rights )
                                                    <a href="{{ url('pos-transaction/show/'.$dataTypeContent['order_num'].'/esd/'.$item['id']) }}" class="label label-default">Rectify ESD</a>
                                                @endif
                                        @endif
                                    </div>

                                    @if(isset($_GET['debug']))
                                        <pre><?php print_r($item); ?></pre>
                                    @endif
                                </td>
                                <td align="center">
                                    {{ $item['qty_ordered'] }}
                                    @if($item['qty_refunded']>0)
                                        <span class="text-smaller">(Refunded)</span>
                                    @endif
                                </td>
                                <td align="right">
                                    {{ $item['unit_price'] }}<br>
                                    @if( ( $item['unit_price']) != $item['regular_price'] )
                                        <span class="text-smaller">RP: {{ $item['regular_price'] }}</span>
                                    @endif
                                </td>
                                <td align="right">
                                    {{ number_format( str_replace(',', '',$item['unit_price'])*$item['qty_ordered'] , 2, '.', ',') }}<br>
                                </td>
                            </tr>
                        @endforeach

                        <tr><td>&nbsp;</td></tr>

                        @if($dataTypeContent['order_status_level']>=1)
                            <tr class="heading">
                                <td align="left">Payment List</td><td></td><td></td><td align="right">Amount</td>
                            </tr>

                            @foreach($dataTypeContent['transactions'] as $trans)
                                <tr class="item">
                                    <td align="left">
                                        {{ $trans['pay_desc'] }}<br>
                                        <span class="text-smaller">{{ $trans['trans_desc'] }}</span>

                                        @if(isset($_GET['debug']))
                                            <pre><?php print_r($trans['data']); ?></pre>
                                        @endif
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td align="right" class="text-nowrap">
                                        <p>S$ {{ $trans['trans_amount'] }}</p>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="2"></td>
                                <td align="right">
                                    <span class="text-nowrap">Total Amount:</span>
                                </td>
                                <td align="right">
                                    <span class="text-nowrap">S$ {{ $dataTypeContent['total_amount'] }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td align="right">
                                    <span class="text-nowrap">Inclusive GST:</span>
                                </td>
                                <td align="right">
                                    <span class="text-nowrap">S$ {{ $dataTypeContent['tax_amount'] }}</span>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>

            </div>
            <div class="col-md-4">

                @if($dataTypeContent['order_status_level']>=1)
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> Download Receipt/Invoice</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" style="max-height: 300px; overflow: auto">
                            <a class="btn pull-right btn-dark" href="https://www.challenger.com.sg/mytransaction/chrooms/{{ $dataTypeContent['order_num'] }}" target="_blank" title="Download" class="btn btn-sm clr-lightblue" style="font-size:12px">
                                <i class="voyager-download"></i> e-Receipt
                            </a>
                        </div>
                    </div>

                @endif

                @if (isset($_GET['debug']))
                    <pre>
                    Order Content (RAW)
                        <?php print_r($dataTypeContent); ?>
                </pre>
                @endif
            </div>
        </div>
    </div>

@stop

@section('javascript')
    <script>
        $(document).ready(function () {

            $(function () {
            });

            $('[id^=edit_sales_]').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            $('[id^=edit_sn_]').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            $("#retail-delivery-btn").click(function () {
                @if(isset($dataTypeContent['custom_data']->retail_delivery))
                    alert('Retail delivery created, cannot create new delivery.');
                    return;
                @endif
                if ($("#inv_name").val()=='' || $("#inv_addr").val()=='') {
                    alert('Please enter customer name & address to proceed');
                }
                else {
                    $("#delv_name").val( $("#inv_name").val() );
                    $("#delv_addr").val( $("#inv_addr").val() );
                    $('#retail_delivery').modal('show');
                }
            });

            $("#retail_delivery_form").on('submit', function(e){
                e.preventDefault();
                if ($('div.checkbox-group.required :checkbox:checked').length == 0) {
                    alert('Please select item(s) to deliver')
                }
                else {
                    this.submit();
                }
            })

        })
    </script>
@stop

@section('css')

    <style>
        .text-smaller {
            font-size: 90%;
        }

        .invoice-box {
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        #delv_item_ul {
            columns: 1;
            border: 1px solid #CCC;
            margin-top: 0px;
            padding: 10px 10px 10px 30px;
            list-style: none;
            width: 100%;
        }
        #delv_item_ul > li {
            margin-bottom: 0px;
        }
        #delv_item_ul > li > input {
            margin-top: 12px;
        }
        #delv_item_ul > li > label {
            background-color: #FFF;
            border: 0px;
        }
    </style>
@stop