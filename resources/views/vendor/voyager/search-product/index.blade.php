@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-hook"></i> Search Products
    </h1>
@stop

@section('page_header_actions')

@stop

@section('content')
    <div class="page-content container-fluid">
        @if (request()->has('message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-{{ request('message_type', 'info') }}">
                        <p>{{ request('message') }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <div style="padding:0px 15px">
                            <form method="get" class="form-search" onsubmit="return searchProd();">
                                <div id="search-input">
                                    <div class="input-group col-md-12">
                                        <input id="searchProdVal" type="text" class="form-control" placeholder="Search by Item ID / Description" value="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div id="prodLoading" style="text-align: center;width: 100%"><h1 style="color:#ccc">Searching...</h1></div>

                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th width="20%">ID</th>
                                <th width="45%">Description</th>
                                <th width="10%">Regular Price</th>
                                <th width="10%">Member Price</th>
                                <th width="15%" class="actions">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- Single delete modal --}}
        <div class="modal fade" tabindex="-1" id="info_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Product Details</h4>
                    </div>
                    <div class="modal-body" id="info_modal_body">
                        <table id="infoTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th width="10%">ID</th>
                                <th width="90%"></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        let thisDataTable;
        let thisInfoTable;

        $(document).ready(function () {
            thisDataTable = $('#dataTable').DataTable({
                "order": [],
                "searching": false,
                "paging": false,
            });

            thisInfoTable = $('#infoTable').DataTable({
                "order": [],
                "searching": false,
                "paging": false,
                "language": {
                    "emptyTable": "Loading..."
                }
            });

            $("#prodLoading").hide();
        });

        function fxPrice(id){
            $('#info_modal').modal('show');
            thisInfoTable.clear().draw();

            let ajaxGet = {
                url: 'https://pos.api.valueclub.asia/products/'+id+'?settings=1&online_settings=1&pwp_group=1&customer_group=MEMBER',
                headers: {'Authorization':'bearer UqrLJ6OOKayf0ie2QH34iVGPiuCkJiPW'}
            }
            $.get(ajaxGet, function(data){
                thisInfoTable.row.add(['Item ID', data.item_id]);
                thisInfoTable.row.add(['Description', data.item_desc]);
                thisInfoTable.row.add(['Brand', data.settings.dimensions.brand_id]);
                thisInfoTable.row.add(['Category', data.settings.dimensions.inv_dim1 + ' / ' + data.settings.dimensions.inv_dim2 + ' / ' + data.settings.dimensions.inv_dim3 + ' / ' + data.settings.dimensions.inv_dim4]);
                thisInfoTable.row.add(['',''])
                thisInfoTable.row.add(['Regular Price', data.prices.regular_price.toFixed(2)])
                thisInfoTable.row.add(['Member Price', data.prices.mbr_price.toFixed(2)])
                thisInfoTable.row.add(['Promo Price', data.prices.promo_price.toFixed(2)])
                thisInfoTable.row.add(['Promo ID', data.prices.promo_id + ' ('+data.prices.promo_ind+')'])
                thisInfoTable.draw();
            });
        }

        function fxInventory(id){
            $('#info_modal').modal('show');
            thisInfoTable.clear().draw();

            let ajaxGet = {
                url: 'search-product/getallinventory/'+id+''
            }
            $.get(ajaxGet, function(data){
                for(var i=0; i < data.data.length; i++) {
                    thisInfoTable.row.add([
                        data.data[i].loc_id,
                        data.data[i].qty_available
                    ]);
                }

                if (data.data.length == 0) {
                    thisInfoTable.row.add(['','Sold Out']);
                }
                thisInfoTable.draw();
            });
        }

        function searchProd() {
            $("#prodLoading").show();
            thisDataTable.clear().draw();

            let search = $("#searchProdVal").val();

            let ajaxGet = {
                url: 'https://pos.api.valueclub.asia/products/search/' +search+ '?customer_group=MEMBER&limit=100',
                headers: {'Authorization':'bearer UqrLJ6OOKayf0ie2QH34iVGPiuCkJiPW'}
            }
            $.get(ajaxGet, function(data){
                for(var i=0; i < data.length; i++) {
                    thisDataTable.row.add([
                        data[i].item_id,
                        data[i].item_desc,
                        data[i].prices.regular_price.toFixed(2),
                        data[i].prices.unit_price.toFixed(2),
                        '<a href="javascript:fxPrice(\''+data[i].item_id+'\');" >Prices</a> | <a href="javascript:fxInventory(\''+data[i].item_id+'\');" >Inventory</a>',
                    ]);
                }
                thisDataTable.draw();
                $("#prodLoading").hide();
            });

            return false;
        }
    </script>
@stop
