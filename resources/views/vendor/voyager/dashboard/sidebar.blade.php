<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                        <?php /* $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                            <img src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                            <img src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                        */ ?>
                        <img src="{{ env('APP_URL') . '/image/applogo.png' }}">
                    </div>
                    <div class="title">{{Voyager::setting('admin.title', 'VOYAGER')}}</div>
                </a>
            </div><!-- .navbar-header -->


            @if(env("APP_ENV")!='production')
            <div class="panel widget center bgimage" style="background-image:url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('images/bg.jpg') ) }}); background-size: cover; background-position: 0px;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <?php /*
                    <img src="{{ voyager_asset('images/captain-avatar.png') }}" class="avatar" alt="{{ \CHG\Voyager\Facades\Voyager::getName() }} avatar">
                    <h4>{{ ucwords(\CHG\Voyager\Facades\Voyager::getName()) }}</h4>
                    <p>{{ \CHG\Voyager\Facades\Voyager::getId() }}</p>

                    <a href="{{ route('voyager.profile') }}" class="btn btn-primary">{{ __('voyager::generic.profile') }}</a>
                    <div style="clear:both"></div>
                    */ ?>
                    <img src="{{ env('APP_URL') . '/image/test-avatar.png' }}" class="avatar" alt="test avatar" style="background-color: #FFF">
                    <h4 style="font-weight:bold; color: #FF9629">Test Environment</h4>
                    <div style="clear:both"></div>
                </div>
            </div>
            @endif

        </div>

        {!! menu('admin', 'admin_menu') !!}
    </nav>
</div>
