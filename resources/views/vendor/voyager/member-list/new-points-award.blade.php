<!-- !!! Add form action below -->
<form role="form" action="{{ route('voyager.member-list.points_award') }}" method="POST">
    <div class="modal fade modal-danger modal-relationships" id="new_member_points_award">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Member Points Adjustment</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 10px">
                            Award/deduct points from member account.
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Description:</p>
                            <input required type="text" maxlength="59" class="form-control" name="points_description">
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Amount ($):</p>
                            <input required type="number" class="form-control" name="points_amount" step="0.01">
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Expiry Date:</p>
                            <select class="relationship_type select2 select2-container" name="exp_date">
                                @foreach($points_exp_date as $p)
                                    <option value="{{ $p }}">{{ $p }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Trans Ref:</p>
                            <input type="text" class="form-control" name="trans_ref" placeholder="Optional">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="mbr_id" value="{{ $dataTypeContent['mbr_id'] }}">
                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                        <button class="btn btn-danger btn-relationship" id="award_points"><i class="voyager-double-up"></i>
                            <span>Update</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</form>
