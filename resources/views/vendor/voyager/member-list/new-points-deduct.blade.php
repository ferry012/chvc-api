<!-- !!! Add form action below -->
<form role="form" action="{{ route('voyager.member-list.points_deduct') }}" method="POST">
    <div class="modal fade modal-danger modal-relationships" id="new_member_points_deduct">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Member Points Deduct </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Points Amount:</p>
                            <input type="number" class="form-control" min="1"
                                   name="points_amount">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="mbr_id" value="{{ $dataTypeContent['mbr_id'] }}">
                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                        <button class="btn btn-danger btn-relationship"><i class="voyager-double-down"></i>
                            <span>Deduct</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</form>
