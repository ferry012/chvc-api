@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }} &nbsp;

        @if(\CHG\Voyager\Facades\Voyager::can('member_edit'))
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endif

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="row">
                    <div class="col-md-8">
                        <!-- Promo List -->
                        <div class="panel">

                            <!-- Basic Info -->
                            <div class="panel-heading">
                                <h3 class="panel-title panel-icon"><i class="voyager-person"></i> Member Info</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-up" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Member ID</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="{{ __('generic_name') }}"
                                               value="{{ rtrim($dataTypeContent['mbr_id']) }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Status</label>
                                        <input type="text" class="form-control" readonly value="@if($dataTypeContent['status_level'] == 1){{ "Active" }}@elseif($dataTypeContent['status_level'] == 0){{ "Inactive" }}@elseif($dataTypeContent['status_level'] == -1){{ "Expire" }}@elseif($dataTypeContent['status_level'] == -9){{ "Pre-register" }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_singular">First Name</label>
                                        <input type="text" class="form-control" name="" readonly placeholder="First Name"
                                               value="{{ $dataTypeContent['first_name'] }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">Last Name</label>
                                        <input type="text" class="form-control"
                                               name="" readonly
                                               placeholder="Last Name"
                                               value="{{ $dataTypeContent['last_name'] }}">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_singular">Email Address</label>
                                        <input required type="text" class="form-control"
                                               name="" readonly
                                               placeholder="Email Address"
                                               value="{{ $dataTypeContent['email_addr'] }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">Contact Number</label>
                                        <input type="text" class="form-control"
                                               name="" readonly
                                               placeholder="Contact Number"
                                               value="{{ $dataTypeContent['contact_num'] }}">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">ValueClub E-statements</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="Your rebate balance, account expiry and savings to date."></span><br>
                                        <?php $checked = (isset($dataTypeContent['sub_ind1']) && $dataTypeContent['sub_ind1'] == 'Y'); ?>
                                        <input type="text"  readonly
                                               class="form-control"
                                               value="@if(isset($dataTypeContent['sub_ind1']) && $dataTypeContent['sub_ind1'] == 'Y') {{ 'Yes' }}@else{{'No'}}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">ValueClub exclusive promotions</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="Latest offers, new gadgets and pre-orders!"></span><br>
                                        <?php $checked = (isset($dataTypeContent['sub_ind2']) && substr($dataTypeContent['sub_ind2'], 0, 1) == 'Y'); ?>
                                        <input type="text" readonly
                                               class="form-control"
                                               value="@if(isset($dataTypeContent['sub_ind2']) && substr($dataTypeContent['sub_ind2'], 0, 1) == 'Y'){{'Yes'}}@else{{'No'}}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Password Change</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Password Change"
                                               value="{{ $dataTypeContent['pwd_changed'] }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Last Login</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Last Login"
                                               value="{{ $dataTypeContent['last_login'] }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Member Address -->
                            <div class="panel-heading">
                                <h3 class="panel-title">Member Address</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Postal Code</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. 596741"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Postal Code"
                                               value="@if(!empty($address)){{ $address[0]->postal_code }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line1</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. BLK 50"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line1"
                                               value="@if(!empty($address)){{ $address[0]->street_line1 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line2</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. TOH TUCK ROAD"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line2"
                                               value="@if(!empty($address)){{ $address[0]->street_line2 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line3</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. #09-06"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line3"
                                               value="@if(!empty($address)){{ $address[0]->street_line3 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line4</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. Pte Ltd"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line4"
                                               value="@if(!empty($address)){{ $address[0]->street_line4 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Country Id</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. SG"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Country Id"
                                               value="@if(!empty($address)){{ $address[0]->country_id }}@endif">
                                    </div>
                                </div>
                            </div>
                            <!-- Deliver Address -->
                            <div class="panel-heading">
                                <h3 class="panel-title">Deliver Address</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Postal Code</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. 596741"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Postal Code"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->postal_code }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line1</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. BLK 50"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line1"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line1 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line2</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. TOH TUCK ROAD"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line2"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line2 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line3</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. #09-06"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line3"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line3 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line4</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. Pte Ltd"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Street Line4"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line4 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Country Id</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. SG"></span>
                                        <input type="text" class="form-control" readonly
                                               placeholder="Country Id"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->country_id }}@endif">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                <div class="col-md-4">
                    <!-- ### Rebate Claim ### -->
                    <div class="panel panel-bordered panel-dark">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Rebate Claim</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row fake-table-hd">
                                <div class="col-xs-4">Last Renewal</div>
                                <div class="col-xs-3">Last Expiry</div>
                                <div class="col-xs-3">Claim Expiry</div>
                                <div class="col-xs-2">Savings</div>
                            </div>
                            <div id="bread-items">
                                <div class="row row-dd" style="padding-top:10px;">
                                    <div class="col-xs-4" style="margin-bottom: 0">
                                        <p><strong>@if(!empty($rebate_claim)){{ $rebate_claim->last_renewal }}@endif</strong></p>
                                    </div>

                                    <div class="col-xs-3" style="margin-bottom: 0">
                                        <p><strong>@if(!empty($rebate_claim)){{ $rebate_claim->last_expiry }}@endif</strong></p>
                                    </div>
                                    <div class="col-xs-3" style="margin-bottom: 0">
                                        <p><strong>@if(!empty($rebate_claim)){{ $rebate_claim->claim_expiry }}@endif</strong></p>
                                    </div>
                                    <div class="col-xs-2" style="margin-bottom: 0">
                                        <p><strong>@if(!empty($rebate_claim)){{ $rebate_claim->mbr_savings }}@endif</strong></p>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'Y' && \Carbon\Carbon::now() < \Carbon\Carbon::parse($rebate_claim->last_expiry)->addDays(31) && \Carbon\Carbon::now() > \Carbon\Carbon::parse($rebate_claim->last_expiry)->addDays(1))
                                <form method="post" action="{{ route('voyager.rebate-claim.claim') }}" style="display:inline">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn pull-right btn-primary">Claim</button>
                                    <input type="hidden" name="line" value="{{ $rebate_claim->line_num }}">
                                    <input type="hidden" name="id" value="{{$dataTypeContent['mbr_id']}}" class="selected_ids">
                                </form>

                            @elseif(empty($rebate_claim))
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Claim</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'A')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Approved</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'R')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Waiting Approved...</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'S')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Expired</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'Z')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Rejected</button>
                            @else
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Claim</button>
                            @endif
                        </div>
                    </div>

                    <!-- ### Mbr Type ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Member Type</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row fake-table-hd">
                                <div class="col-xs-5">Effective From</div>
                                <div class="col-xs-5">Effective To</div>
                                <div class="col-xs-2">Type</div>
                            </div>
                            <div id="bread-items">
                                @if(!empty($mbr_type))
                                    @foreach($mbr_type as $type)
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-5" style="margin-bottom: 0">
                                                <p><strong>{{ $type->eff_from }}</strong></p>
                                            </div>

                                            <div class="col-xs-5" style="margin-bottom: 0">
                                                <p><strong>{{ $type->eff_to }}</strong></p>
                                            </div>
                                            <div class="col-xs-2" style="margin-bottom: 0">
                                                <p><strong>{{ $type->mbr_type }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- ### Mbr Points ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Member Points</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row fake-table-hd">
                                <div class="col-xs-4">Expiry Date</div>
                                <div class="col-xs-4">Accumulated</div>
                                <div class="col-xs-2">Redeemed</div>
                                <div class="col-xs-2">Expired</div>
                            </div>
                            <div id="bread-items">
                                @if(!empty($mbr_points))
                                    @foreach($mbr_points as $point)
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-4" style="padding-left: 30px;margin-bottom: 0">
                                                <p><strong>{{ $point->exp_date }}</strong></p>
                                            </div>
                                            <div class="col-xs-4" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_accumulated }}</strong></p>
                                            </div>

                                            <div class="col-xs-2" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_redeemed }}</strong></p>
                                            </div>
                                            <div class="col-xs-2" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_expired }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- ### Mbr Voucher ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> Member Voucher</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row fake-table-hd">
                                <div class="col-xs-6">Coupon Id</div>
                                <div class="col-xs-6">Expiry Date</div>
                            </div>
                            <div id="bread-items">
                                @if(!empty($mbr_voucher))
                                    @foreach($mbr_voucher as $voucher)
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-6" style="padding-left: 30px;margin-bottom: 0">
                                                <p><strong>{{ $voucher->coupon_id }}</strong></p>
                                            </div>

                                            <div class="col-xs-6" style="margin-bottom: 0">
                                                <p><strong>{{ $voucher->expiry_date }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- ### Mbr Transaction ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Member Transaction</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Loc</th>
                                        <th>Pos</th>
                                        <th>Item</th>
                                        <th>Item Desc</th>
                                        <th>Qty</th>
                                        {{--<th>Regular Price</th>--}}
                                        <th>Price</th>
                                        {{--<th>Disc Percent</th>--}}
                                        {{--<th>Disc Amount</th>--}}
                                        <th>Rebate</th>
                                        <th>Savings</th>
                                        {{--<th>Salesperson</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($mbr_transaction as $trans)
                                        <tr>
                                            <td>{{ $trans->trans_id }}</td>
                                            <td>{{ $trans->trans_type }}</td>
                                            <td>{{ $trans->trans_date }}</td>
                                            <td>{{ $trans->loc_id }}</td>
                                            <td>{{ $trans->pos_id }}</td>
                                            <td>{{ $trans->item_id }}</td>
                                            <td>{{ $trans->item_desc }}</td>
                                            <td>{{ $trans->item_qty }}</td>
                                            {{--                                            <td>{{ $trans->regular_price }}</td>--}}
                                            <td>{{ number_format($trans->unit_price,2,'.','') }}</td>
                                            {{--<td>{{ $trans->disc_percent }}</td>--}}
                                            {{--<td>{{ $trans->disc_amount }}</td>--}}
                                            <td>{{ number_format($trans->trans_points/100,2,'.','') }}</td>
                                            <td>{{ number_format($trans->mbr_savings,2,'.','') }}</td>
                                            {{--                                            <td>{{ $trans->salesperson_id }}</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
