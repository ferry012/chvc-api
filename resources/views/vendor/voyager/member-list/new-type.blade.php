<!-- !!! Add form action below -->
<form role="form" action="{{ route('voyager.member-list.update_type') }}" method="POST">
    <div class="modal fade modal-danger modal-relationships" id="new_member_type">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Update Member Type</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 10px">
                            Change the membership status or member type. Existing memberships will be ended.
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">New Member Type:</p>
                            <select class="relationship_type select2 select2-container" name="mbr_type">
                                <option value="">Choose a Member Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </option>
                                <option value="MAS" @if(rtrim($dataTypeContent['mbr_type']) == 'MAS'){{ "Active" }} selected @endif>MAS - Associate Membership</option>
                                <option value="MST" @if(rtrim($dataTypeContent['mbr_type']) == 'MST'){{ "Active" }} selected @endif>MST - Student Membership</option>
                                <option value="MSTP" @if(rtrim($dataTypeContent['mbr_type']) == 'MSTP'){{ "Active" }} selected @endif>MSTP - Student Plus Member</option>
                                <option value="MNS" @if(rtrim($dataTypeContent['mbr_type']) == 'MNS'){{ "Active" }} selected @endif>MNS - Valueclub Membership</option>
                                <option value="M08" @if(rtrim($dataTypeContent['mbr_type']) == 'M08'){{ "Active" }} selected @endif>M08 - Valueclub Membership (8 Months)</option>
                                <option value="M18" @if(rtrim($dataTypeContent['mbr_type']) == 'M18'){{ "Active" }} selected @endif>M18 - Valueclub Membership (18 Months)</option>
                                <option value="M28" @if(rtrim($dataTypeContent['mbr_type']) == 'M28'){{ "Active" }} selected @endif>M28 - Valueclub Membership (28 Months)</option>
                                <option value="M" @if(rtrim($dataTypeContent['mbr_type']) == 'M'){{ "Active" }} selected @endif>M - Valueclub Membership (2 Years)</option>
                            </select>
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">New Status:</p>
                            <select class="relationship_type select2 select2-container" name="status_level">
                                <option value="">Choose a Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </option>
                                <option value="1" @if($dataTypeContent['status_level'] == 1){{ "Active" }} selected @endif>Active</option>
                                <option value="-1" @if($dataTypeContent['status_level'] == -1){{ "Active" }} selected @endif>Expire</option>
                                <option value="0" @if($dataTypeContent['status_level'] == 0){{ "Active" }} selected @endif>Inactive</option>
                            </select>
                        </div>

                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">New Expiry:</p>
                            <input type="date" class="form-control datepicker"
                                   name="exp_date" data-date="" data-date-format="YYYY-MM-DD"
                                   value="{{ \Carbon\Carbon::parse($dataTypeContent['exp_date'])->format('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="mbr_id" value="{{ $dataTypeContent['mbr_id'] }}">
                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                        <button class="btn btn-danger btn-relationship"><i class="voyager-plus"></i>
                            <span>Update</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</form>
