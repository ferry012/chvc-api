@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());

    $rights_cs = (in_array('CS_USR',Session::get('rights')) || in_array('SUPER',Session::get('rights'))) ? true : false; // Allow CS rights
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="row">
                <form class="form-edit-add" role="form"
                      action="{{ route('voyager.member-list.update', $dataTypeContent['mbr_id']) }}"
                      method="POST" enctype="multipart/form-data">
                    <!-- PUT Method if we are editing -->
                    @if(isset($dataTypeContent['mbr_id']))
                        {{ method_field("PUT") }}
                    @endif
                    {{ csrf_field() }}
                    <div class="col-md-8">
                        <!-- Promo List -->
                        <div class="panel">

                            <!-- Basic Info -->
                            <div class="panel-heading">
                                <h3 class="panel-title panel-icon"><i class="voyager-person"></i> Member Info</h3>
                                <div class="panel-actions hidden">
                                    <a class="panel-action voyager-angle-up" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <div class="col-md-2 form-group">
                                        <label for="name">Member ID</label>
                                        <input type="text" class="form-control" readonly name="mbrId"
                                               value="{{ rtrim($dataTypeContent['mbr_id']) }}">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="name">Member Type</label>
                                        <input type="text" class="form-control" readonly name=""
                                               value="{{ rtrim($dataTypeContent['mbr_type']) }}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="name">Member Since</label>
                                        <input type="text" class="form-control" readonly name=""
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['join_date'])) }}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="name">Expiry Date</label>
                                        <input type="text" class="form-control" readonly name=""
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['exp_date'])) }}">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="name">Status</label>
                                        <select name="status_level" class="select2 form-control" readonly="" disabled="">
                                            <option value="1" @if($dataTypeContent['status_level'] == 1){{ "Active" }} selected @endif>Active</option>
                                            <option value="0" @if($dataTypeContent['status_level'] == 0){{ "Active" }} selected @endif>Inactive</option>
                                            <option value="-1" @if($dataTypeContent['status_level'] == -1){{ "Active" }} selected @endif>Expire</option>
                                            <option value="-9" @if($dataTypeContent['status_level'] == -9){{ "Active" }} selected @endif>Pre-register</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-2 form-group">
                                        <label for="name">Rebates</label>
                                        <input type="text" class="form-control" readonly name=""
                                               value="$ {{ number_format( ($dataTypeContent['points_accumulated'] - $dataTypeContent['points_redeemed'] - $dataTypeContent['points_expired'])/100 ,2,'.','') }}">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="name">Member Savings</label>
                                        <input type="text" class="form-control" readonly name=""
                                               value="$ {{ number_format( $dataTypeContent['mbr_savings'] ,2,'.','') }}">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="name">POS Login Counter</label>
                                        <select class="form-control" readonly="">
                                        @if(!empty($pos_login))
                                            @foreach($pos_login as $type)
                                                @if(in_array(trim($type->pos_id),['SSAP','QR','POS']))
                                                <option value="" @if( trim($type->pos_id) == 'SSAP'){{ "Active" }} selected @endif>
                                                    {{ $type->pos_id . ': ' . $type->source_count }}
                                                </option>
                                                @endif
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="name">Last Renewal</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Last Renewal"
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['last_renewal'])) }}">
                                    </div>
                                    <div class="col-md-3 form-group pull-right">
                                        <label for="name">Info Last Modified</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Last Modified"
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['modified_on'])) }}">
                                    </div>
                                </div>
                                @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_singular">First Name</label>
                                        <input type="text" class="form-control" name="first_name" placeholder="First Name"
                                               value="{{ $dataTypeContent['first_name'] }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">Last Name</label>
                                        <input type="text" class="form-control"
                                               name="last_name"
                                               placeholder="Last Name"
                                               value="{{ $dataTypeContent['last_name'] }}">
                                    </div>
                                </div>
                                @else
                                    <div class="row clearfix">
                                        <div class="col-md-6 form-group">
                                            <label for="display_name_singular">First Name</label>
                                            <input type="text" class="form-control" name="first_name" placeholder="First Name"
                                                   value="{{ $dataTypeContent['first_name'] }}" readonly>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="display_name_plural">Last Name</label>
                                            <input type="text" class="form-control"
                                                   name="last_name"
                                                   placeholder="Last Name"
                                                   value="{{ $dataTypeContent['last_name'] }}" readonly>
                                        </div>
                                    </div>
                                    @endif
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_singular">Email Address</label>
                                            <input required type="text" class="form-control"
                                                   name="email_addr"
                                                   placeholder="Email Address"
                                                   value="{{ $dataTypeContent['email_addr'] }}">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="display_name_plural">Contact Number</label>
                                        <input required type="text" class="form-control"
                                               name="contact_num"
                                               placeholder="Contact Number"
                                               value="{{ $dataTypeContent['contact_num'] }}">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))
                                    <div class="col-md-6 form-group">
                                        <label for="name">Date of Birth</label>
                                        <input type="date" class="form-control"
                                               name="birth_date"
                                               value="{{ date('Y-m-d', strtotime($dataTypeContent['birth_date']) ) }}">
                                    </div>
                                    @else
                                        <div class="col-md-6 form-group">
                                            <label for="name">Date of Birth</label>
                                            <input type="date" class="form-control"
                                                   name="birth_date"
                                                   value="{{ date('Y-m-d', strtotime($dataTypeContent['birth_date']) ) }}" readonly>
                                        </div>
                                    @endif
                                    <div class="col-md-3 form-group">
                                        <label for="display_name_plural">VClub E-statements</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="Your rebate balance, account expiry and savings to date."></span><br>
                                        <?php $checked = (isset($dataTypeContent['sub_ind1']) && $dataTypeContent['sub_ind1'] == 'Y'); ?>
                                        <input type="checkbox"
                                               name="sub_ind1"
                                               class="toggleswitch"
                                               data-on="{{ __('voyager::generic.yes') }}"
                                               data-off="{{ __('voyager::generic.no') }}"
                                               @if($checked) checked @endif >
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="display_name_plural">VClub exclusive promotions</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="Latest offers, new gadgets and pre-orders!"></span><br>
                                        <?php $checked = (isset($dataTypeContent['sub_ind2']) && substr($dataTypeContent['sub_ind2'], 0, 1) == 'Y'); ?>
                                        <input type="checkbox"
                                               name="sub_ind2"
                                               class="toggleswitch"
                                               data-on="{{ __('voyager::generic.yes') }}"
                                               data-off="{{ __('voyager::generic.no') }}"
                                               @if($checked) checked @endif >
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-3 form-group">
                                        <label for="name">Reset Password</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="Leave empty to keep the same."></span><br>
{{--                                        @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))--}}
                                            <input type="password" class="form-control" id="password" name="mbr_pwd" value="" autocomplete="new-password">
{{--                                        @else--}}
{{--                                            <input placeholder="Only CX/OM can reset" type="password" class="form-control" id="password" name="" value="" readonly autocomplete="new-password">--}}
{{--                                        @endif--}}
                                    </div>
                                    @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))
                                        <div class="col-md-3 form-group">
                                            <label for="name">Show URL</label>
                                            <span class="voyager-question"
                                                  aria-hidden="true"
                                                  data-toggle="tooltip"
                                                  data-placement="right"
                                                  title="It will show password reset code and url."></span><br>
                                            <div class="btn btn-dark pull-left" id="show-password" style="margin-top: 0">
                                                <span>Show</span>
                                            </div>
                                                {{--<input type="submit" style="margin-top: 0" class="btn btn-dark pull-left delete-confirm" value="Show">--}}
                                        </div>
                                    @endif
                                    <div class="col-md-3 form-group">
                                        <label for="name">Pwd Last Changed</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Last Login"
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['pwd_changed'])) }}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="name">Account Last Login</label>
                                        <input type="text" class="form-control" readonly name=""
                                               placeholder="Last Login"
                                               value="{{ date('d/m/Y H:i:s', strtotime($dataTypeContent['last_login'])) }}">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))
                                        <div class="col-md-3 form-group">
                                            <label for="name">Clear Cache</label>
                                            <span class="voyager-question"
                                                  aria-hidden="true"
                                                  data-toggle="tooltip"
                                                  data-placement="right"
                                                  title="Show the url of clear cache."></span><br>
                                            <div class="btn btn-dark pull-left" id="show-clearcache" style="margin-top: 0">
                                                <span>Show Cache</span>
                                            </div>
                                            {{--<input type="submit" style="margin-top: 0" class="btn btn-dark pull-left delete-confirm" value="Show">--}}
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <label for="name">Reminder</label>
                                            <span class="voyager-question"
                                                  aria-hidden="true"
                                                  data-toggle="tooltip"
                                                  data-placement="right"
                                                  title="Add or edit reminder."></span><br>
                                            <div class="btn btn-dark pull-left" id="show-reminder" style="margin-top: 0">
                                                <span>Add/Edit</span>
                                            </div>
                                            {{--<input type="submit" style="margin-top: 0" class="btn btn-dark pull-left delete-confirm" value="Show">--}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <!-- Member Address -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Member Address
                                        <a id="mbr-pri-addr-btn" class="voyager-angle-down" href="#" onclick="return false;"></a>
                                    </h3>
                                </div>
                                <div class="panel-body" id="mbr-pri-addr-div" style="display: none">
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line1</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. BLK 50"></span>
                                        <input type="text" class="form-control" name="primary_details[street_line1]"
                                               placeholder="Street Line1"
                                               value="@if(!empty($address)){{ $address[0]->street_line1 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line2</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. TOH TUCK ROAD"></span>
                                        <input type="text" class="form-control" name="primary_details[street_line2]"
                                               placeholder="Street Line2"
                                               value="@if(!empty($address)){{ $address[0]->street_line2 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line3</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. #09-06"></span>
                                        <input type="text" class="form-control" name="primary_details[street_line3]"
                                               placeholder="Street Line3"
                                               value="@if(!empty($address)){{ $address[0]->street_line3 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line4</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. Pte Ltd"></span>
                                        <input type="text" class="form-control" name="primary_details[street_line4]"
                                               placeholder="Street Line4"
                                               value="@if(!empty($address)){{ $address[0]->street_line4 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Country Id</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. SG"></span>
                                        <input type="text" class="form-control" name="primary_details[country_id]"
                                               placeholder="Country Id"
                                               value="@if(!empty($address)){{ $address[0]->country_id }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Postal Code</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. 596741"></span>
                                        <input type="text" class="form-control" name="primary_details[postal_code]"
                                               placeholder="Postal Code"
                                               value="@if(!empty($address)){{ $address[0]->postal_code }}@endif">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- Deliver Address -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Delivery Address
                                        <a id="mbr-delv-addr-btn" class="voyager-angle-down" href="#" onclick="return false;"></a>
                                    </h3>
                                </div>
                                <div class="panel-body" id="mbr-delv-addr-div" style="display:none">
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line1</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. BLK 50"></span>
                                        <input type="text" class="form-control" name="delivery_details[street_line1]"
                                               placeholder="Street Line1"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line1 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line2</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. TOH TUCK ROAD"></span>
                                        <input type="text" class="form-control" name="delivery_details[street_line2]"
                                               placeholder="Street Line2"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line2 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line3</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. #09-06"></span>
                                        <input type="text" class="form-control" name="delivery_details[street_line3]"
                                               placeholder="Street Line3"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line3 }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Street Line4</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. Pte Ltd"></span>
                                        <input type="text" class="form-control" name="delivery_details[street_line4]"
                                               placeholder="Street Line4"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->street_line4 }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Country Id</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. SG"></span>
                                        <input type="text" class="form-control" name="delivery_details[country_id]"
                                               placeholder="Country Id"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->country_id }}@endif">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="name">Postal Code</label>
                                        <span class="voyager-question"
                                              aria-hidden="true"
                                              data-toggle="tooltip"
                                              data-placement="right"
                                              title="ex. 596741"></span>
                                        <input type="text" class="form-control" name="delivery_details[postal_code]"
                                               placeholder="Postal Code"
                                               value="@if(!empty($delv_addr)){{ $delv_addr[0]->postal_code }}@endif">
                                    </div>
                                </div>
                            </div>
                            </div>

                            @can('read',app($dataType->model_name))
                            <div class="panel-body">
                                <button type="submit" class="btn pull-right btn-primary">{{ __('voyager::generic.submit') }}</button>
                            </div>
                            @endcan
                            @if(\Illuminate\Support\Facades\Gate::check('edit_more',app($dataType->model_name)))
                                <div class="panel-body">
                                    <input type="submit" name="anonymous"
                                           class="btn pull-right btn-danger" value="Anonymize Member"
                                           onclick="return confirm('Are you sure you want to anonymize this member?')"
                                    />
                                </div>

                                <form class="form-edit-add" role="form"
                                      action="{{ route('voyager.member-list.update', $dataTypeContent['mbr_id']) }}"
                                      method="POST" enctype="multipart/form-data">
                                    <div class="panel-body">
                                        <input type="text" name="trans_id" class="pull-right" placeholder="Receipt ID"
                                               style="margin-top: 5px; height: 35px;" />
                                        <input type="submit" name="upload_transaction" style="margin-right: 10px;"
                                               class="btn pull-right btn-primary" value="Upload Receipt"
                                        />
                                    </div>
                                </form>
                            @endif
                        </div>

                    </div>

                </form>
                <div class="col-md-4">
                    <!-- ### Rebate Claim ### -->
                    <div class="panel panel-bordered panel-dark">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Rebate Claim</h3>
                            <div class="panel-actions">
                                <a class="panel-action panel-collapsed voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" @if(empty($rebate_claim)) style="display:none" @endif>
                            <div class="row fake-table-hd">
                                <div class="col-xs-4">Last Renewal</div>
                                <div class="col-xs-3">Last Expiry</div>
                                <div class="col-xs-3">Claim Expiry</div>
                                <div class="col-xs-2">Savings</div>
                            </div>
                            <div id="bread-items">
                                    <div class="row row-dd" style="padding-top:10px;">
                                        <div class="col-xs-4" style="margin-bottom: 0">
                                            <p><strong>@if(!empty($rebate_claim)){{ date('d/m/Y', strtotime($rebate_claim->last_renewal)) }}@endif</strong></p>
                                        </div>

                                        <div class="col-xs-3" style="margin-bottom: 0">
                                            <p><strong>@if(!empty($rebate_claim)){{ date('d/m/Y', strtotime($rebate_claim->last_expiry)) }}@endif</strong></p>
                                        </div>
                                        <div class="col-xs-3" style="margin-bottom: 0">
                                            <p><strong>@if(!empty($rebate_claim)){{ '('.$rebate_claim->claim_entitled.') ' . date('d/m/Y', strtotime($rebate_claim->claim_expiry)) }}@endif</strong></p>
                                        </div>
                                        <div class="col-xs-2" style="margin-bottom: 0">
                                            <p><strong>@if(!empty($rebate_claim)){{ $rebate_claim->mbr_savings }}@endif</strong></p>
                                        </div>
                                    </div>
                            </div>
                            @if(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'Y' && \Carbon\Carbon::now() < \Carbon\Carbon::parse($rebate_claim->claim_expiry)->addDays(31) )
                                <form method="post" action="{{ route('voyager.rebate-claim.claim') }}" style="display:inline">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn pull-right btn-primary">Submit Claim</button>
                                    <input type="hidden" name="line" value="{{ $rebate_claim->line_num }}">
                                    <input type="hidden" name="id" value="{{$dataTypeContent['mbr_id']}}" class="selected_ids">
                                </form>

                            @elseif(empty($rebate_claim))
                                <button type="submit" class="btn pull-right btn-dark" disabled="">No Pending Claim</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'R')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Waiting Approved...</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'A')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Approved</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'S')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Expired</button>
                            @elseif(!empty($rebate_claim) && $rebate_claim->claim_entitled == 'Z')
                                <button type="submit" class="btn pull-right btn-dark" disabled="">Rejected</button>
                            @endif
                        </div>
                    </div>

                    <!-- ### Mbr Type ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Member Type</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row fake-table-hd">
                                <div class="col-xs-4">Type</div>
                                <div class="col-xs-4">Effective From</div>
                                <div class="col-xs-4">Effective To</div>
                            </div>
                            <div id="bread-items" style="margin-bottom: 10px">
                                @if(!empty($mbr_type))
                                    @foreach($mbr_type as $type)
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-4" style="margin-bottom: 0">
                                                <p><strong>{{ $type->mbr_type }}</strong></p>
                                            </div>
                                            <div class="col-xs-4" style="margin-bottom: 0">
                                                <p><strong>{{ date('d/m/Y', strtotime($type->eff_from)) }}</strong></p>
                                            </div>

                                            <div class="col-xs-4" style="margin-bottom: 0">
                                                <p><strong>{{ date('d/m/Y', strtotime($type->eff_to)) }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            @can('edit_more',app($dataType->model_name))
                                <div class="btn pull-right btn-dark" id="member-type-edit">Manage Membership</div>
                                @include('voyager::member-list.new-type')
                            @endcan
                        </div>
                    </div>

                    <!-- ### Mbr Points ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Member Points</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" style="max-height: 350px; overflow: auto">
                            <div class="row fake-table-hd">
                                <div class="col-xs-4">Expiry Date</div>
                                <div class="col-xs-3">Accumulated</div>
                                <div class="col-xs-3">Redeemed</div>
                                <div class="col-xs-2">Expired</div>
                            </div>
                            <div id="bread-items">
                                <?php $p_accumulated=0; $p_redeemed=0; $p_expired=0; ?>
                                @if(!empty($mbr_points))
                                    @foreach($mbr_points as $point)
                                            <?php $p_accumulated += $point->points_accumulated; $p_redeemed += $point->points_redeemed; $p_expired += $point->points_expired; ?>
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-4" style="padding-left: 30px;margin-bottom: 0">
                                                <p><strong>{{ date('d/m/Y', strtotime($point->exp_date)) }}</strong></p>
                                            </div>
                                            <div class="col-xs-3" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_accumulated }}</strong></p>
                                            </div>
                                            <div class="col-xs-3" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_redeemed }}</strong></p>
                                            </div>
                                            <div class="col-xs-2" style="margin-bottom: 0">
                                                <p><strong>{{ $point->points_expired }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="row fake-table-hd" style="margin-bottom: 10px">
                                <div class="col-xs-4" style="font-weight: bold;">Total: </div>
                                <div class="col-xs-3" style="padding-left: 15px;font-weight: bold; @if ($p_accumulated!=$dataTypeContent['points_accumulated']) color:#F00 @endif ">{{ $dataTypeContent['points_accumulated'] }}</div>
                                <div class="col-xs-3" style="padding-left: 15px;font-weight: bold; @if ($p_redeemed!=$dataTypeContent['points_redeemed']) color:#F00 @endif ">{{ $dataTypeContent['points_redeemed'] }}</div>
                                <div class="col-xs-2" style="padding-left: 15px;font-weight: bold; @if ($p_expired!=$dataTypeContent['points_expired']) color:#F00 @endif ">{{ $dataTypeContent['points_expired'] }}</div>
                            </div>
                            @can('edit_more',app($dataType->model_name))
                                <button style="margin:5px" type="submit" class="btn pull-right btn-dark" id="member-points-award">Points Adjustment</button>
                                @include('voyager::member-list.new-points-award')
                                <?php /*
                                <button style="margin:5px" type="submit" class="btn pull-right btn-dark" id="member-points-deduct">Points Deduct</button>
                                @include('voyager::member-list.new-points-deduct')
                                */ ?>
                            @endcan
                        </div>
                    </div>

                    <!-- ### Mbr Voucher ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> Member Voucher</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" style="max-height: 300px; overflow: auto">
                            <div class="row fake-table-hd">
                                <div class="col-xs-6">Coupon Id</div>
                                <div class="col-xs-3">Issue Date</div>
                                <div class="col-xs-3">Expiry Date</div>
                            </div>
                            <div id="bread-items">
                                @if(!empty($mbr_voucher))
                                    @foreach($mbr_voucher as $voucher)
                                        <div class="row row-dd" style="padding-top:10px;">
                                            <div class="col-xs-6" style="padding-left: 30px;margin-bottom: 0">
                                                @if($rights_cs==true)
                                                    <p><strong>
                                                            <a href="#" class="member-voucher-extend">{{ $voucher->coupon_id }}</a>
                                                        </strong></p>
                                                @else
                                                    <p><strong>{{ $voucher->coupon_id }}</strong></p>
                                                @endif
                                            </div>
                                            <div class="col-xs-3" style="margin-bottom: 0">
                                                <p><strong>{{ date('d/m/Y', strtotime($voucher->issue_date)) }}</strong></p>
                                            </div>
                                            <div class="col-xs-3" style="margin-bottom: 0">
                                                <p><strong>{{ date('d/m/Y', strtotime($voucher->expiry_date)) }}</strong></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            @can('edit_more_voucher',app($dataType->model_name))
                                <a href="{{ route('voyager.member-list.show_member_voucher', ['id'=>rtrim($dataTypeContent['mbr_id'])]) }}" class="btn pull-right btn-dark" id="member-type-edit">View Voucher</a>
                            @endcan
                        </div>
                    </div>

                    <!-- ### ASC Mbr ### -->
                    @if (!empty($asc_members) && count($asc_members)>0)
                        <div class="panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-search"></i> Associate Members</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body" style="max-height: 300px; overflow: auto">
                                <div class="row fake-table-hd">
                                    <div class="col-xs-5">Mbr Id</div>
                                    <div class="col-xs-5">Invite On</div>
                                    <div class="col-xs-2">Status</div>
                                </div>
                                <div id="bread-items">
                                    @if(!empty($asc_members))
                                        @foreach($asc_members as $asc_mbr)
                                            <div class="row row-dd" style="padding-top:10px;">
                                                <div class="col-xs-5" style="padding-left: 30px;margin-bottom: 0">
                                                    <p><strong>
                                                            <a href="{{ url('member-list/'.trim($asc_mbr->mbr_id).'/edit') }}">{{ $asc_mbr->mbr_id }}</a>
                                                        </strong></p>
                                                </div>
                                                <div class="col-xs-5" style="margin-bottom: 0">
                                                    <p><strong>{{ date('d/m/Y', strtotime($asc_mbr->join_date)) }}</strong></p>
                                                </div>
                                                <div class="col-xs-2" style="margin-bottom: 0">
                                                    <p><strong>{{ ($asc_mbr->status_level==1) ? 'Active' : '-' }}</strong></p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (isset($dataTypeContent['main_id']) && $dataTypeContent['main_id']!='')
                        <div class="panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-search"></i> Main Member</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body" style="max-height: 300px; overflow: auto">
                                <div class="row fake-table-hd">
                                    <div class="col-xs-6">Mbr Id</div>
                                    <div class="col-xs-6">Invite On</div>
                                </div>
                                <div id="bread-items">
                                    <div class="row row-dd" style="padding-top:10px;">
                                        <div class="col-xs-6" style="padding-left: 30px;margin-bottom: 0">
                                            <p><strong>
                                                    <a href="{{ url('member-list/'.trim($dataTypeContent['main_id']).'/edit') }}">{{ $dataTypeContent['main_id'] }}</a>
                                                </strong></p>
                                        </div>
                                        <div class="col-xs-6" style="margin-bottom: 0">
                                            <p><strong>{{ date('d/m/Y', strtotime($dataTypeContent['join_date'])) }}</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- ### Mbr Transaction ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Member Transaction</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>



                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Loc</th>
                                        <th>Pos</th>
                                        <th>Item</th>
                                        <th>Item Desc</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Rebate</th>
                                        <th>Savings</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mbr_transaction as $trans)
                                        @php
                                            $url = '/member-list/'. $dataTypeContent['mbr_id'] .'/show-member-transaction/' . trim($trans->trans_id);
                                        @endphp
                                        <tr>
                                            <td>
                                                <a href="{{ $url }}" target="_self">
                                                {{ trim($trans->trans_id) }}
                                                </a>
                                            </td>
                                            <td>{{ $trans->trans_type }}</td>
                                            <td>{{ $trans->trans_date }}</td>
                                            <td>{{ $trans->loc_id }}</td>
                                            <td>{{ $trans->pos_id }}</td>
                                            <td>{{ $trans->item_id }}</td>
                                            <td>{{ $trans->item_desc }}</td>
                                            <td>
                                                @if( !in_array($trans->trans_type,['AP','RD']))
                                                    {{ $trans->item_qty }}
                                                @endif
                                            </td>
                                            <td>
                                                @if( !in_array($trans->trans_type,['AP','RD']))
                                                    {{ number_format($trans->unit_price,2,'.','') }}
                                                @endif
                                            </td>
                                            {{--<td>{{ $trans->regular_price }}</td>--}}
                                            {{--<td>{{ $trans->disc_percent }}</td>--}}
                                            {{--<td>{{ $trans->disc_amount }}</td>--}}
                                            <td>{{ number_format($trans->trans_points/100,2,'.','') }}</td>
                                            <td>{{ number_format($trans->mbr_savings,2,'.','') }}</td>
                                            {{--<td>{{ $trans->salesperson_id }}</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Show Password URL Modal -->
    <div class="modal fade modal-danger modal-relationships" id="show_password">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Reset Password Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Member Email:</p>
                            <input type="text" class="form-control" name="mbr_id" value="{{ $dataTypeContent['email_addr'] }}" readonly>
                        </div>

                        <div class="col-md-12 relationship_details_more">
                            <div class="well">
                                <label>Password Detail:</label>
                                <p><strong>Url<span class="label_table_name"></span>:<br><span id="pwd_url"></span> </strong>
                                </p>
                                <p><strong>Code<span class="label_table_name"></span>:<br><span id="pwd_code"></span> </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End -->
    <!-- Show Reminder Modal -->
    <form role="form" action="{{ route('voyager.member-list.reminder') }}" method="POST">
        <div class="modal fade modal-danger modal-relationships" id="show_reminder">
            <div class="modal-dialog relationship-panel">
                <div class="model-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                        </button>
                        <h4 class="modal-title">Add/Edit Reminder</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 relationship_details">
                                <p class="relationship_table_select">Member Id:</p>
                                <input type="text" class="form-control" name="mbr_id" value="{{ $dataTypeContent['mbr_id'] }}" readonly>
                            </div>

                            <div class="col-md-12 relationship_details_more">
                                <div class="well">
                                    <label>Reminder:</label>
                                    <textarea class="form-control" name="meta_value">@if(!empty($reminder)){{ $reminder }}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="relationship-btn-container">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                            <button class="btn btn-danger btn-relationship"><i class="voyager-double-up"></i>
                                <span>Update</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ csrf_field() }}
    </form>
    <!-- End -->
    <!--Show Clear Cache Url-->
    <div class="modal fade modal-danger modal-relationships" id="show_clearcache">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Clear Cache URL.</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Member Email:</p>
                            <input type="text" class="form-control" name="mbr_id" value="{{ $dataTypeContent['mbr_id'] }}" readonly>
                        </div>

                        <div class="col-md-12 relationship_details_more">
                            <div class="well">
                                <label>Url Detail:</label>
                                <p><strong>Url<span class="label_table_name"></span>:<br><span id="clearcache_url"></span> </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End-->
@stop

@section('javascript')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script>
        let mbr_id = document.getElementsByName("mbr_id");
        let staffId = {{ \CHG\Voyager\Facades\Voyager::getId() }};
        console.log("Staff: "+ staffId+"\nMbr_id: "+ mbr_id[1].value);

        $(document).ready(function () {
            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [[2, 'desc']],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});

            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
            $('#bread-items').disableSelection();

            $('[data-toggle="tooltip"]').tooltip();

            $('.toggleswitch').bootstrapToggle();
        });

        $('#member-type-edit').click(function(){
            $('#new_member_type').modal('show');
        });

        $('#member-points-award').click(function(){
            $('#new_member_points_award').modal('show');
        });

        $('#member-points-deduct').click(function(){
            $('#new_member_points_deduct').modal('show');
        });

        $("#mbr-pri-addr-btn").on('click', function() {
            $('#mbr-pri-addr-div').show();
            $('#mbr-pri-addr-btn').hide();
        });

        $("#mbr-delv-addr-btn").on('click', function() {
            $('#mbr-delv-addr-div').show();
            $('#mbr-delv-addr-btn').hide();
        });

        $('#show-password').click(function(){
            populateUrlOfPassword()
            $('#show_password').modal('show');
        });

        $('#show-reminder').click(function(){
            $('#show_reminder').modal('show');
        });

        $('#show-clearcache').click(function(){
            populateUrlOfClearCache()
            $('#show_clearcache').modal('show');
        });

        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });

        $('.member-voucher-extend').on('click', function () {
            alert('Amend voucher expiry date');
            return false;
        });

        function populateUrlOfPassword(){
            var email = "{!! $dataTypeContent['email_addr'] !!}";
            if(email != undefined){
                $.get('/member-list/resetpwd/' + email, function(data){
                    debugger;
                    $('#pwd_url').text(data.data.url)
                    $('#pwd_code').text(data.data.code)
                });
            }
        }

        function populateUrlOfClearCache(){
            var mbr_id = "{!! rtrim($dataTypeContent['mbr_id']) !!}";
            if(mbr_id != undefined){
                // https://chstaff.challenger.sg/api/vc2/removeLoginRedis/V160000500/hachi12345
                let url= "https://chstaff.challenger.sg/api/vc2/removeLoginRedis/" + mbr_id + '/hachi12345';
                $('#clearcache_url').text(url);
            };
        }
    </script>
@stop
