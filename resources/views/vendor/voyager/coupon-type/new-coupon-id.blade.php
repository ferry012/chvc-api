<!-- !!! Add form action below -->
<form role="form" action="{{ route('voyager.coupon-type.create_coupon_id') }}" method="POST">
    <div class="modal fade modal-danger modal-relationships" id="add_coupon_id">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Create new coupon id </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Item ID:&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            <input type="text" class="form-control" min="1"
                                   name="item_id">
                        </div>
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Item Desc:</p>
                            <input type="text" class="form-control" min="1"
                                   name="item_desc">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                        <button class="btn btn-danger btn-relationship"><i class="voyager-double-down"></i>
                            <span>Submit</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</form>
