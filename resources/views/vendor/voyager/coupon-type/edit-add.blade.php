@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            <div class="row clearfix">
                                <div class="col-md-12 form-group">
                                    <label for="name">Member Type</label>
                                    <select class="relationship_type select2 select2-container" name="mbr_type">
                                        <option value="">Choose a Member Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </option>
                                        <option value="M08" @if(rtrim($dataTypeContent['mbr_type']) == 'M08'){{ "Active" }} selected @endif>M08 - Valueclub Membership (8 Months)</option>
                                        <option value="M18" @if(rtrim($dataTypeContent['mbr_type']) == 'M18'){{ "Active" }} selected @endif>M18 - Valueclub Membership (18 Months)</option>
                                        <option value="M28" @if(rtrim($dataTypeContent['mbr_type']) == 'M28'){{ "Active" }} selected @endif>M28 - Valueclub Membership (28 Months)</option>
                                        <option value="MST" @if(rtrim($dataTypeContent['mbr_type']) == 'MST'){{ "Active" }} selected @endif>MST - Student Membership</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12 form-group">
                                    <label for="name">Coupon ID</label>
                                    <select class="relationship_type select2" name="coupon_id">
                                        @foreach($coupons as $v)
                                            <option value="{{ $v->item_id }}" @if(isset($dataTypeContent['coupon_id']) && rtrim($dataTypeContent['coupon_id']) == $v->item_id){{ 'selected="selected"' }}@endif>{{ $v->item_desc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12 form-group">
                                    <label for="name">Effect From</label>
                                    <input type="date" class="form-control datepicker"
                                           name="eff_from" data-date="" data-date-format="YYYY-MM-DD"
                                           value="{{ \Carbon\Carbon::parse($dataTypeContent['eff_from'])->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12 form-group">
                                    <label for="name">Effect To</label>
                                    <input type="date" class="form-control datepicker"
                                           name="eff_to" data-date="" data-date-format="YYYY-MM-DD"
                                           value="{{ \Carbon\Carbon::parse($dataTypeContent['eff_to'])->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12 form-group">
                                    <label for="name">Days To Expiry</label>
                                    <input type="text" class="form-control" name="days_to_expiry"
                                           value="@if(isset($dataTypeContent['days_to_expiry'])){{ $dataTypeContent['days_to_expiry'] }}@else{{ '14' }}@endif">
                                </div>
                            </div>
                            @if(\Illuminate\Support\Facades\Gate::check('browse_partner',app($dataType->model_name)))
                                <div class="row clearfix">
                                    <div class="col-md-12 form-group">
                                        <label for="name">Amount</label>
                                        <input type="text" class="form-control" name="amount"
                                               value="@if(isset($dataTypeContent['amount'])){{ $dataTypeContent['amount'] }}@else{{ '0' }}@endif">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12 form-group">
                                        <label for="name">Partner ID</label>
                                        <input type="text" class="form-control" name="partner_id"
                                               value="@if(isset($dataTypeContent['partner_id'])){{ $dataTypeContent['partner_id'] }}@endif">
                                    </div>
                                </div>
                            @endif

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <div class="btn btn-dark pull-right" id="add-coupon-id">
                                <span>Add Coupon ID</span>
                            </div>
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    @include('voyager::coupon-type.new-coupon-id')
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
        $('#add-coupon-id').click(function(){
            $('#add_coupon_id').modal('show');
        });
    </script>
@stop
