<!-- !!! Add form action below -->
<form role="form" action="{{ url('/api/coupon/welcomepack?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl') }}" method="POST">
    <div class="modal fade modal-danger modal-relationships" id="issue_coupon">
        <div class="modal-dialog relationship-panel">
            <div class="model-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">Issue next month coupon </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 relationship_details">
                            <p class="relationship_table_select">Member ID:&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            <input type="text" class="form-control" min="1"
                                   name="mbr_id">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="mbr_type" value="M28">
                <input type="hidden" name="trans_date" value="{{ \Carbon\Carbon::now()->addMonth(1)->toDateTimeString() }}">
                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
                        <button class="btn btn-danger btn-relationship"><i class="voyager-double-down"></i>
                            <span>Submit</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</form>
