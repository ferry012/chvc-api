<?php
ini_set("display_errors", 1);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X_AUTHORIZATION, Authorization");

function cdn_url($uri)
{
    return "https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/" . $uri;
}

function base_url($url = '')
{
    return "https://2.hachi.tech/" . $url;
}


$invoiceData = $data["invoiceData"];
$paymentJS = $data["paymentJS"];
$post_url = $data["post_url"];
$rebate_post_url = $data["rebate_post_url"];
$mycards = $data["mycards"];
$renew_total = $data["renew_total"];
$renew_fee_viarebate = $data["renew_fee_viarebate"];
$renew_mbrtype = $data["renew_mbrtype"];
$last_mbrtype = $data["last_mbrtype"];
$avail_rebates = $data["avail_rebates"];
$callbackUrl = $data["callbackUrl"];
$renew_min = $data["renew_min"];
$mbr_type = $data["mbr_type"];
$upgrade_type = $data["upgrade_type"];

$email_addr = $data["email_addr"];
$mbr_id = $data["mbr_id"];
$contact_num = $data["contact_num"];
$check_url = $data["checkurl"];

?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Challenger Technologies Limited - Payment Gateway</title>
    <!--    <script type="text/javascript" src="-->
<?php //echo base_url("assets/library/jquery.min.1.7.2.js"); ?><!--"></script>-->

    <script src="<?php echo cdn_url('assets20/js/jquery-3.2.1.min.js'); ?>"></script>
    <link href="<?php echo base_url("assets/library/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo cdn_url('assets20/css/font-awesome4/css/font-awesome.css'); ?>" rel="stylesheet"
          type="text/css" media="all"/>

    <style>
        /* Main UI CSS*/
        @charset "UTF-8";
        @font-face {
            font-family: "Montserrat";
            src: url('<?php echo base_url();?>assets/fonts/Montserrat-Regular.ttf') format('truetype');
        }

        @font-face {
            font-family: "Montserrat-Bold";
            src: url('<?php echo base_url();?>assets/fonts/Montserrat-Bold.ttf') format('truetype');
            font-style: normal;
        }

        @font-face {
            font-family: "Montserrat-Light";
            src: url('<?php echo base_url();?>assets/fonts/Montserrat-Light.ttf') format('truetype');
            font-style: normal;
        }

        body {
            font-family: Montserrat, sans-serif;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        html, body {
            -webkit-overflow-scrolling: touch !important;
            overflow: auto !important;
            height: 100% !important;
        }

        b, strong {
            font-weight: 400;
            margin-bottom: 4px;
            display: block;
        }

        h2 em {
            font-style: normal;
        }

        .header {
            background-image: url('https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/assets/images/bg-toolbar-vclub.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            height: 80px;
        }

        .content {
            margin-top: 15px;
            margin-left: 0px;
            margin-right: 0px;
            padding: 0px 15px 16px;
        }

        .main-icon {
            width: 13%;
            vertical-align: -webkit-baseline-middle;
            position: fixed;
            top: 1.5%;
            left: 2%;
        }

        .titleicon {
            color: white;
            font-size: 21px;
            font-weight: 300;
            display: inline-block;
            position: fixed;
            top: 3.5%;
            left: 16%;
        }

        .form-check-label {
            font-size: 12px;
        }

        strong, b {
            font-size: 10px;
            line-height: 15px;
        }

        .row {
            margin-left: 0px;
            margin-right: 0px;
        }

        /*eof Main UI CSS*/
        .btn-block {
            width: 65%;
            margin: 0 auto;
            border-radius: 20px;
            padding: 5px 24px;
        }

        .selectdate .form-control {
            padding: 6px 5px;
        }

        .savedcard-methods--active .savedcard-method {
            border-radius: 5px;
            opacity: 1;
            width: 100%;
            z-index: 1;
            border-color: #219653;
            border-width: 2px;
            margin-top: -1px;
            margin-bottom: -1px;
        }

        .savedcard-method__logo img {
            width: 50px;
            height: 44px;
        }

        .savedcard-method .savedcard-method__logo {
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .savedcard-method .savedcard-method__label {
            color: #000;
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            font-size: 16px;
            margin-left: 20px;
            text-align: left;
            flex-grow: 1;
        }

        .savedcard-method {
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            background-color: #fff;
            border-color: #B5B5B5;
            border-style: solid;
            border-width: 1px 1px 1px;
            cursor: pointer;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin: 0 auto;
            padding: 14px 10px;
            -webkit-transition: opacity .3s, width .2s cubic-bezier(.175, .885, .32, 1.275), border-color .3s, border-radius .3s;
            transition: opacity .3s, width .2s cubic-bezier(.175, .885, .32, 1.275), border-color .3s, border-radius .3s;
            width: 98%;
            z-index: 0;
            height: 60px;
        }

        .savedcard-method, .savedcard-method__check-container {
            position: relative;
        }

        .savedcard-method .savedcard-method__label .savedcard-method__label--small {
            font-size: 13px;
            font-weight: 400;
        }

        .green-c {
            color: #219653 !important;
        }

        .renew_renewal {
            padding-left: 6px;
        }

        .pd-top {
            margin-bottom: 0px;
            /*padding-top:35px;*/
            clear: both;
        }

        .mg-bottom {
            margin-bottom: 35px;
            /*padding-top:35px;*/
            clear: both;
        }

        .braintree-toggle {
            background: #FAFAFA;
            border-radius: 4px;
            color: #000;
            cursor: pointer;
            font-size: 14px;
            margin: 0 auto;
            padding: 18px;
            text-align: center;
            width: 100%;
        }

        .braintree-toggle span {
            border-bottom: 1px solid #B5B5B5;
            padding-bottom: 1px;
        }

        .avail-rebates {
            margin-top: 0px;
            font-size: 20px;
            line-height: 34px;
            color: #013474;
        }

        .paid-amount {
            margin-top: 0px;
            font-size: 20px;
            line-height: 30px;
            color: #f53d3d;
        }

        .red-font {
            color: #f53d3d;
        }

        .rebates_value_div {
            display: flex;
            align-items: baseline;
        }

        .rebates_value_div span {
            margin-right: 16px;
            font-size: 18px;
            color: #013474;
            font-weight: 300;
        }

        .underline {
            height: 25px;
            margin-left: 20px;
            margin-right: 20px;
            border-bottom: 0.5px solid #bebebe;
        }

        h1 {
            font-size: 14px;
            text-transform: uppercase;
            margin-bottom: 10px;
            margin-top: 8px;
        }

        .mbrselect-ul {
            display: flex;
            flex-direction: row;
            margin-bottom: 20px;
            width: 100%;
        }

        .mbrselect-ul li {
            border: 1px solid #a2a2a2;
            display: block;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 8px;
            padding-left: 5px;
            padding-right: 5px;
            flex: 1;
            opacity: 0.3;
            min-height: 115px;
            max-height: 120px;
        }

        .mbrselect-ul li.active {
            opacity: 1;
        }

        .mbrselect-ul:not(.mbrupgrade-ul) li:nth-child(1) {
            border-bottom-right-radius: 0px;
            border-top-right-radius: 0px;
        }

        .mbrselect-ul:not(.mbrupgrade-ul) li:nth-child(2) {
            border-radius: 0px;
        }

        .mbrupgrade-em li:nth-child(1) {
            border-bottom-right-radius: 0px;
            border-top-right-radius: 0px;
        }

        .mbrupgrade-em li:nth-child(2) {
            border-bottom-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .mbrselect-ul:not(.mbrupgrade-ul) li:nth-child(3) {
            border-bottom-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .mbrselect-ul li h2 {
            margin-top: 0px;
            margin-bottom: 5px;
            font-weight: bold;
            font-size: 0.8em;
        }

        .mbrselect-ul li h3 {
            color: #013474;
            font-size: 1.6em;
            margin-top: 2px;
            margin-bottom: 2px;
            /*font-weight: bold;*/
        }

        .mbrselect-ul li p {
            font-size: 0.8em;
            line-height: 16px;
        }

        .description {
            line-height: 16px;
        }

        .hyperlink, .hyperlink:active, .hyperlink:hover, .hyperlink:visited {
            color: #013474;
        }

        #myModal .modal-dialog {
            margin: 0px;
        }

        #myModal .close {
            float: left;
        }

        #myModal .modal-body {
            padding: 0px;
        }

        #errorModal .modal-dialog {
            top: 50%;
            margin-top: -40px;
            -webkit-transform: translate(0, -50%);
            -moz-transform: translate(0, -50%);
            -ms-transform: translate(0, -50%);
            -o-transform: translate(0, -50%);
            transform: translate(0, -50%);
        }

        #errorModal .modal-title {
            text-align: center;
        }

        #errorModal .modal-body {
            text-align: center;
        }

        #errorModal .modal-footer {
            text-align: center;
            color: #013474 !important;
            text-transform: uppercase !important;
        }

        #errorModal .modal-footer .btn {
            color: #013474 !important;
            text-transform: uppercase !important;
            font-weight: bold;
        }

        #errorModal .modal-content {
            width: 85%;
            margin: 0 auto;
            max-width: 349px;
        }

    </style>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div id="loaderParent" style="display:none">
    <div class="loader">
        <img src="https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/assets/images/icon-login-vcclub.png"/>
    </div>
</div>
<!--<div class='header'>-->
<!--    <img class="main-icon"-->
<!--         src="https://s3-ap-southeast-1.amazonaws.com/ctl-public-test/assets/images/icon-login-vcclub.png">-->
<!--    <div class="titleicon" style="">Secure Payment</div>-->
<!--</div>-->

<div class='content '>
    <!--    <div class="col-xs-12" style="color: #000; padding: 15px 18px 10px;">-->
    <!--        <strong style="font-size:14px;">MY SAVED VISA/MASTER CARDS</strong>-->
    <!--    </div>-->
    <?php if (count($mycards) == 0) {
        $newCardShow = "display:block;";
        $savedCardShow = "display:none;";
    } else {
        $newCardShow = "display:none;";
        $savedCardShow = "display:block;";
    }
    ?>


    <?php
    $data = "";
    $firstLoad = "savedcard-methods--active ";
    $firstDisplay = "";
    foreach ($mycards as $key => $card) {
        $firstStr = substr(trim($card->pan), 0, 1);
        if ($firstStr == '4') {
            $src = "https://cdn.hachi.tech/assets/images/payment-logo/pay-visa-card.png";
            $type = "Visa";
        } else if ($firstStr == '5' || $firstStr == '2') {
            $src = "https://cdn.hachi.tech/assets/images/payment-logo/pay-master-card.png";
            $type = "Master Card";
        } else {
            $src = "https://cdn.hachi.tech/assets/images/payment-logo/pay-master-card.png";
            $type = "Master Card";
        }


        $optt = <<<str
    <div class="savedcard-methods savedcard-methods-initial">
        <div class="$firstLoad first">
            <div class="savedcard-method braintree-method--active" tabindex="0">
                <div class="savedcard-method__logo">
                    <img style="margin-left: 16px;padding: 10px 10px 10px 0px;"
                         src="$src"/>
                </div>
                <div class="savedcard-method__label"> $card->pan<br/>
                    <div class="savedcard-method__label--small">$type</div>
                </div>

                <div class="savedcard-method__check-container">
                    <div class="savedcard-method__check">
                        <div class="icon-div" style="height:auto;width:100%;$firstDisplay">
                            <span class="green-c glyphicon glyphicon-ok" aria-hidden="true">&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="savedcardnumber" value="$card->storeCardUniqueID"/>
    </div>
    <div class="row" style="height:3px;"></div>

str;
        $firstLoad = "";
        $firstDisplay = "display:none;";
        $data = $data . $optt;
    }
    echo $data;
    ?>
    <div id="type-toggle" class="braintree-toggle" tabindex="0" style="<?php echo $savedCardShow; ?>">
        <span>Choose another way to pay</span>
    </div>

    <?php if ($avail_rebates >= $renew_total) { ?>
<!-- <div class="row" style="margin-bottom:8px;">
            <span>You currently have V$ <?php echo $avail_rebates; ?> and you can use V$ <?php echo ($avail_rebates >= $renew_total) ? $renew_total : $avail_rebates; ?> to renew your membership.</span>
        </div> -->
    <?php } ?>


    <?php if ($upgrade_type == "renew") { ?>
    <div class="row" id="select_mbrtype_div">
        <h1>SELECT MEMBERSHIP TYPE</h1>
        <ul class="nav mbrselect-ul" id="renew-with-creditcard-div" style="display:none;">
            <li class="mbrselect-li" data-amount="8" data-id="!MEMBER-NEW08">
                <div>
                    <h2>8 months:</h2>
                    <h3>$8</h3>
                    <p>N/A</p>
                </div>
            </li>
            {{--            <li class="mbrselect-li" data-amount="18" data-id="!MEMBER-NEW18">--}}
            {{--                <div>--}}
            {{--                    <h2>18 months:</h2>--}}
            {{--                    <h3>$18</h3>--}}
            {{--                    <p>Earn 1% rebate</p>--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <li class="mbrselect-li" data-amount="28" data-id="!MEMBER-NEW28">
                <div>
                    <h2>28 months:</h2>
                    <h3>$28</h3>
                    <p>Earn 1%+1% rebate</p>
                </div>
            </li>
        </ul>

        <?php if ($avail_rebates >= $renew_min) { ?>
        <ul class="nav mbrselect-ul" id="renew-with-rebates-div" style="display:none;">
            <li class="mbrselect-li" data-amount="8" data-id="!MEMBER-NEW08">
                <div>
                    <h2>8 months:</h2>
                    <h3>$8</h3>
                    <p>N/A</p>
                </div>
            </li>
            {{--            <li class="mbrselect-li" data-amount="18" data-id="!MEMBER-NEW18">--}}
            {{--                <div>--}}
            {{--                    <h2>18 months:</h2>--}}
            {{--                    <h3>$18</h3>--}}
            {{--                    <p>Earn 1% rebate</p>--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <li class="mbrselect-li" data-amount="25" data-id="!MEMBER-NEW28">
                <div>
                    <h2>28 months:</h2>
                    <h3>$25</h3>
                    <p>Earn 1%+1% rebate</p>
                </div>
            </li>
        </ul>
        <?php } ?>

        <p class="description red-font margin-btm">Launch promo! Earn additional 1% rebate for $28 tier membership.</p>


        <p class="description">New membership will start when the current membership ends. <a
                    class="hyperlink ls-modal">Learn more</a>.</p>


    </div>
    <?php } ?>

    <?php if ($upgrade_type == "upgrade") { ?>
    <div class="row" id="select_mbrtype_div">
        <h1>SELECT MEMBER TYPE</h1>
        <ul class="nav mbrselect-ul  mbrupgrade-ul <?php if ($mbr_type == 'M08') {
            echo 'mbrupgrade-em';
        } ?>" id="renew-with-creditcard-div" style="display:none;">
            <?php if ($mbr_type == 'M08') { ?>
            {{--            <li class="mbrselect-li" data-amount="18" data-id="!MEMBER-NEW18">--}}
            {{--                <div>--}}
            {{--                    <h2>18 months:</h2>--}}
            {{--                    <h3>$18</h3>--}}
            {{--                    <p>Earn 1% rebate</p>--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <?php } ?>
            <li class="mbrselect-li" data-amount="28" data-id="!MEMBER-NEW28">
                <div>
                    <h2>28 months:</h2>
                    <h3>$28</h3>
                    <p>Earn 1%+1% rebate</p>
                </div>
            </li>
        </ul>

        <?php if ($avail_rebates >= $renew_min) { ?>
        <ul class="nav mbrselect-ul mbrupgrade-ul <?php if ($mbr_type == 'M08') {
            echo 'mbrupgrade-em';
        } ?>" id="renew-with-rebates-div" style="display:none;">
            <?php if ($mbr_type == 'M08') { ?>
            {{--            <li class="mbrselect-li" data-amount="18" data-id="!MEMBER-NEW18">--}}
            {{--                <div>--}}
            {{--                    <h2>18 months:</h2>--}}
            {{--                    <h3>$18</h3>--}}
            {{--                    <p>Earn 1% rebate</p>--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <?php } ?>

            <?php if($upgrade_type == "renew"){ ?>
            <li class="mbrselect-li" data-amount="25" data-id="!MEMBER-NEW28">
                <div>
                    <h2>28 months:</h2>
                    <h3>$25</h3>
                    <p>Earn 1%+1% rebate</p>
                </div>
            </li>
            <?php } ?>

            <?php if($upgrade_type == "upgrade"){ ?>
            <li class="mbrselect-li" data-amount="28" data-id="!MEMBER-NEW28">
                <div>
                    <h2>28 months:</h2>
                    <h3>$28</h3>
                    <p>Earn 1%+1% rebate</p>
                </div>
            </li>
            <?php } ?>
        </ul>
        <?php } ?>


        <p class="description">Current membership benefits will be updated to the new membership upon upgrade. <a
                    class="hyperlink ls-modal-upgrade">Learn
                more</a>.</p>

        <!--            <p class="description">New membership will start when the current membership ends. <a-->
        <!--                        class="hyperlink ls-modal">Learn more</a>.</p>-->

    </div>
    <?php } ?>


    <div class="row" style="padding-left: 5px; padding-top: 4%;margin-bottom:12px; <?php echo $newCardShow; ?>"
         id="new_credit_card_div">


        <?php if (strpos($callbackUrl, 'callback_renew') !== false) { ?>
        <?php if ($avail_rebates >= $renew_min) { ?>
        <div class="form-check col-xs-12" id="rebate-formlist">
            <input class="form-check-input" type="radio" name="selectPayment" id="rebate" value="rebates-div">
            <label class="form-check-label" for="exampleRadios1">
                SIGN UP WITH V$
            </label>
        </div>
        <?php } ?>


        <div class="form-check col-xs-12 mg-bottom" id="credit-formlist">
            <input class="form-check-input" type="radio" name="selectPayment" id="creditcard"
                   value="creditcard-div">
            <label class="form-check-label" for="exampleRadios1">
                SIGN UP WITH CREDIT CARD
            </label>
        </div>
        <?php } ?>



        <?php if (strpos($callbackUrl, 'callback_upgrade') !== false) {
        ?>
        <?php if ($avail_rebates >= $renew_min) { ?>
        <div class="form-check col-xs-12" id="rebate-formlist">
            <input class="form-check-input" type="radio" name="selectPayment" id="rebate" value="rebates-div">
            <label class="form-check-label" for="exampleRadios1">
                UPGRADE WITH V$
            </label>
        </div>
        <?php } ?>


        <div class="form-check col-xs-12 mg-bottom" id="credit-formlist">
            <input class="form-check-input" type="radio" name="selectPayment" id="creditcard"
                   value="creditcard-div">
            <label class="form-check-label" for="exampleRadios1">
                UPGRADE WITH CREDIT CARD
            </label>
        </div>
        <?php } ?>


    <!--        --><?php //if (strpos($callbackUrl, 'callback_signup') === false && $mbr_type === 'M') { ?>
        <?php if (strpos($callbackUrl, 'callback_upgrade') === false) { ?>
        <h1 class="pd-top" id="signup-credit-title">SIGN UP WITH CREDIT CARD</h1>
        <?php } ?>


        <?php if ($avail_rebates >= $renew_min) { ?>
        <div id="rebates-div" class="col-xs-12 payment_mode"
             style="border: 1px dotted #808080; border-radius: 10px;margin-top:10px;padding-left:0px;padding-right: 0px;">
            <form id="2c2p-rebate-form" action="<?php echo $rebate_post_url; ?>" method="POST">
                <div class="row" style="height:24px;"></div>

                <div class="row" style="padding-left: 5px;    padding-right: 5px;">
                    <div class="col-xs-6">
                        <div class="">
                            <strong>AVAILABLE REBATES</strong>
                        </div>
                        <div class=" card_num_div">
                            <h2 class="avail-rebates">V$ <?php echo $avail_rebates; ?></h2>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="">
                            <strong>REBATES FOR RENEWAL</strong>
                        </div>
                        <div class="rebates_value_div">
                            <h2 class="avail-rebates">V$ <em id="mbr_amount"><?php echo $renew_total; ?></em></h2>
                            <input class="form-control renew_amt" type="hidden" name="renew_amt"
                                   value="<?php echo $renew_total; ?>">
                            <input class="form-control renew_mbrtype" type="hidden" name="renew_mbrtype"
                                   value="<?php echo $renew_mbrtype; ?>">
                        </div>
                    </div>
                </div>

                <div class="underline" style="height:14px;">&nbsp;</div>

                <input type="hidden" value="<?php echo $invoiceData ?>" name="invoiceData">
                <button id="btndropin1" onclick="RebateCheckout()" style="display: block;margin-top:10px;"
                        type="button"
                        class="btn btn-danger btn-block btn-lg">&nbsp;Submit
                </button>

                <!--            <input type="button" value="Checkout"  onclick="Checkout()">-->

                <div class="row" style="height:15px;"></div>

                <input type="hidden" id="selectPaymentValue" value=""/>
                <input type="hidden" name="renew_renewal" value="<?php echo $renew_total; ?>">
                <input type="hidden" name="renew_membertype" value="<?php echo $renew_mbrtype; ?>">

            </form>
        </div>
        <?php } ?>

        <div id="creditcard-div" class="col-xs-12 payment_mode"
             style="border: 1px dotted #808080; border-radius: 10px;margin-top:10px; padding-left: 0px;    padding-right: 0px;">
            <form id="2c2p-payment-form" action="<?php echo $post_url; ?>" method="POST">
                <div class="row" style="height:15px;"></div>

                <!--                --><?php //if ($avail_rebates >= 5.00) { ?>
            <!--                    <div id="renew-both" style="display:none;">-->
                <!--                        <div class="row" style="height:9px;"></div>-->
                <!---->
                <!--                        <div class="row" style="padding-left: 5px;padding-right: 5px;">-->
                <!--                            <div class="col-xs-6">-->
                <!--                                <div class="">-->
                <!--                                    <strong>AVAILABLE REBATES</strong>-->
                <!--                                </div>-->
                <!--                                <div class=" card_num_div">-->
                <!--                                    <h2 class="avail-rebates">V$ -->
            <?php //echo $avail_rebates; ?><!--</h2>-->
                <!--                                </div>-->
                <!--                            </div>-->
                <!--                            <div class="col-xs-6">-->
                <!--                                <div class="">-->
                <!--                                    <strong>REBATES FOR RENEWAL</strong>-->
                <!--                                </div>-->
                <!--                                <div class="rebates_value_div">-->
                <!--                                    <span>V$</span>-->
                <!--                                    <input class="form-control" type="number" pattern="[0-9]*" inputmode="numeric"-->
                <!--                                           name="renew_renewal" id="rebatecredit-vamount"-->
                <!--                                           placeholder="0.00"-->
                <!--                                           style="z-index: 1;">-->
                <!--                                    -->
                <!--                                </div>-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!---->
                <!--                        <div class="underline" style="height:10px;">&nbsp;</div>-->
                <!--                        <div class="row" style="height:15px;"></div>-->
                <!--                        <div class="row" style="padding-left: 5px;    padding-right: 5px;">-->
                <!--                            <div class="col-xs-8">-->
                <!--                                <div class="">-->
                <!--                                    <strong>BALANCE AMOUNT TO BE PAID</strong>-->
                <!--                                </div>-->
                <!--                                <div class="card_num_div">-->
                <!--                                    <h2 class="paid-amount">$ <span-->
                <!--                                                id="remaining-topay">-->
            <?php //echo $renew_total; ?><!--</span>-->
                <!--                                    </h2>-->
                <!--                                </div>-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                    <div class="row" style="height:10px;"></div>-->
                <!--                --><?php //} ?>

                <div class="row" style="background-color: #e2e2e2;">
                    <img style="margin-left: 16px; width: 15%; padding: 10px 10px 10px 0px;"
                         src="https://cdn.hachi.tech/assets/images/payment-logo/pay-master-card.png"/>
                    <img style="width: 16%; padding: 10px 10px 10px 0px;"
                         src="https://cdn.hachi.tech/assets/images/payment-logo/pay-visa-card.png"/>
                </div>
                <div class="row" style="height:15px;"></div>

                <div class="row">
                    <div class="col-xs-12">
                        <strong>CARD NUMBER</strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 card_num_div">
                        <input class="form-control" type="number" pattern="[0-9]*" inputmode="numeric" id="cardNumber"
                               name="cardNumber"
                               placeholder="**** **** **** ****"
                               data-encrypt="cardnumber" oninput="maxLengthCheck(this)"
                               maxlength="16" oncopy="return false" ondrag="return false" ondrop="return false"
                               onpaste="return false" style="z-index: 1;">
                    </div>
                </div>
                <div style="height: 15px;"></div>

                <div class="row">
                    <div class="col-xs-12 verify_code_div">
                        <strong>VERIFICATION CODE</strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 cvv_div">
                        <input class="form-control cvvField" type="password" pattern="[0-9]*" inputmode="numeric"
                               id="cvv" data-encrypt="cvv" maxlength="4" autocomplete="off" placeholder="CVV2"
                               oncopy="return false" ondrag="return false" ondrop="return false" onpaste="return false"
                               style="z-index: 1;">
                    </div>
                </div>
                <div style="height: 15px;"></div>

                <div class="row">
                    <div class="col-xs-12">
                        <strong>FULL NAME ON CARD</strong>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 card_name_div">
                        <input class="form-control" type="text" id="cardholderName" placeholder="Cardholder Name"
                               name="cardholderName" oncopy="return false"
                               ondrag="return false" ondrop="return false" onpaste="return false" style="z-index: 1;">
                    </div>
                </div>
                <div style="height: 15px;"></div>

                <div class="row">
                    <div class="col-xs-12 exp_date_div">
                        <strong>EXPIRATION DATE</strong>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 mm_yyyy_div selectdate">
                        <select class="form-control" id="month" data-encrypt="month"
                                style="width: 40%; float: left; border: 1px solid #ccc;" oncopy="return false"
                                ondrag="return false" ondrop="return false" onpaste="return false">
                            <option value="" selected>MM</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>

                        <select class="form-control" id="year" data-encrypt="year"
                                style="width: 50%; float: right; border: 1px solid #ccc;" oncopy="return false"
                                ondrag="return false" ondrop="return false"
                                onpaste="return false">
                            <option value="" selected>YYYY</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                            <option value="2030">2030</option>
                            <option value="2031">2031</option>
                            <option value="2032">2032</option>
                            <option value="2033">2033</option>
                        </select>
                    </div>
                </div>

                <div class="underline">&nbsp;</div>

                <!-- hidden field to submit the backend -->
                <input type="hidden" value="" name="encryptedCardInfo">
                <input type="hidden" value="" name="maskedCardInfo">
                <input type="hidden" value="" name="expMonthCardInfo">
                <input type="hidden" value="" name="expYearCardInfo">

                <input type="hidden" name="renew_renewal" value="<?php echo $renew_total; ?>">
                <input type="hidden" name="renew_membertype" value="<?php echo $renew_mbrtype; ?>">

                <input type="hidden" value="<?php echo $invoiceData ?>" name="invoiceData">
                <button id="btndropin2" onclick="Checkout()" style="display: block;margin-top:20px;" type="button"
                        class="btn btn-danger btn-block btn-lg"><i class='fa fa-cc-mastercard'></i>&nbsp;Secure Pay
                </button>

                <div class="row" style="height:15px;"></div>
            </form>
        </div>


        <br style="height:10px;clear:both;"/>
    </div>


    <form id="2c2p-tokenize" action="<?php echo $post_url; ?>" method="POST" style="<?php echo $savedCardShow; ?>">
        <?php
        $firstLoad = "checked";
        $html = "";
        foreach ($mycards as $key => $card) {
            $opt = <<<str
<div class="radio">
<label style="display:none;"><input  type="radio" name="storeCard" value="$card->storeCardUniqueID" $firstLoad>$card->pan</label>
</div>
str;
            $html = $html . $opt;
            $firstLoad = "";
        }
        echo $html;
        ?>
        <input type="hidden" id="storeCardUniqueID" name="storeCardUniqueID">
        <input type="hidden" value="<?php echo $invoiceData ?>" name="invoiceData">
        <button type="button" id="btncheckout_store" class="btn btn-danger "><i
                    class="fa fa-cc-mastercard"></i> Secure Pay
        </button>
    </form>


</div>


<div id="errorModal" class="modal fade " role="dialog">
    <div class="modal-dialog  modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="error-modal-title"></h4>
            </div>
            <div class="modal-body">
                <p id="error-modal-body"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?php echo $paymentJS ?>"></script>
<script type="text/javascript">
    var renew_total = <?php echo $renew_total;?>;
    var avail_rebates = <?php echo $avail_rebates;?>;
    var last_mbrship_type = '<?php echo $last_mbrtype;?>';
    var upgrade_type = '<?php echo $upgrade_type;?>';

    var isProcessing = false;
    //var renew_fee_viarebate=   <?php //echo $renew_fee_viarebate;?>//;
    console.log(renew_total);

    $(document).ready(function () {
        $(".payment_mode").hide();

        // debugger;
        $('li[data-id="' + last_mbrship_type + '"]').addClass("active");
        if (upgrade_type === "renew") {
            if (!last_mbrship_type) {
                $('li[data-id="!MEMBER-NEW28"]').addClass("active");
            }
        }


        <?php if ($avail_rebates >= $renew_total) { ?>
        $("#rebate").prop("checked", true);
        $("#rebates-div").show();

        $(".mbrselect-ul").hide();
        $("#renew-with-rebates-div").show();
        $("#selectPaymentValue").val("rebates-div");
        <?php }else { ?>
        $("#creditcard").prop("checked", true);
        $("#creditcard-div").show();

        $("#rebate-formlist").hide();
        $(".mbrselect-ul").hide();
        $("#renew-with-creditcard-div").show();

        $("#selectPaymentValue").val("creditcard-div");

        <?php } ?>

        $('input[type=radio][name=selectPayment]').change(function () {
            var selected_val = this.value;
            console.log(selected_val);

            if (selected_val == 'creditcard-div') {
                $("#signup-credit-title").show();
            } else {
                $("#signup-credit-title").hide();
            }

            $(".payment_mode").hide();

            $(".mbrselect-ul").hide();
            $("#renew-with-" + selected_val).show();

            var select_mbr_type = $("input[name='renew_membertype']").val();
            console.log(select_mbr_type);
            // debugger;
            if (upgrade_type === "renew") {
                $("#renew-with-" + selected_val + " li[data-id='" + select_mbr_type + "']").click();
            } else if (upgrade_type === "upgrade") {
                $("#renew-with-" + selected_val + " li[data-id='" + select_mbr_type + "']").click();
            }

            $("#remaining-topay").text(renew_total.toFixed(2));
            $("#rebatecredit-vamount").val('');

            $("#renew-both").hide();
            $("#" + selected_val).show();
            // }

            $("#cardholderName").val('');
            $("#cvv").val('');
            $("#cardNumber").val('');
            $("#year").val('');
            $("#month").val('');


            $("#selectPaymentValue").val(selected_val);
            if (selected_val == 'rebates-div') {
                var amt_total = $("input[name='renew_amt']").val();
                if (amt_total > avail_rebates) {
                    $('.mbrselect-li').removeClass('active');
                    $('#renew-with-rebates-div li[data-id="!MEMBER-NEW28"]').click();
                }
            }

        });

        $(".ls-modal-upgrade").on('click', function (e) {
            var hachipage = {
                hachipage: {
                    func: 'openpage',
                    data: {
                        "url": 'https://support.challenger.sg/support/solutions/articles/47001139579-how-much-does-it-cost-to-sign-up-upgrade-as-a-valueclub-member-'
                    }
                }
            }
            window.parent.postMessage(hachipage, '*');
        });

        $('.ls-modal').on('click', function (e) {
            var hachipage = {
                hachipage: {
                    func: 'openpage',
                    data: {
                        "url": 'https://support.challenger.sg/support/solutions/articles/47001139579-how-much-does-it-cost-to-sign-up-upgrade-as-a-valueclub-member-'
                    }
                }
            }
            window.parent.postMessage(hachipage, '*');
        });

        //Toggle Li opacity
        $('.mbrselect-li').click(function () {
            $('.mbrselect-li').removeClass('active');
            $(this).addClass('active');

            var item_id = $(this).data('id');
            var mbr_amount = $(this).data('amount');

            var selectedMode = $("#selectPaymentValue").val();
            console.log(selectedMode);

            if (mbr_amount > avail_rebates && !(item_id == '!MEMBER-NEW28' && avail_rebates >= 25 && avail_rebates < 28)) {
                $("#rebate-formlist").hide();
            } else {
                $("#rebate-formlist").show();
            }

            if (selectedMode == 'rebates-div' && mbr_amount > avail_rebates) {
                console.log(item_id);
                console.log("here");
                $('#creditcard').click();
                if (!(item_id == '!MEMBER-NEW28' && avail_rebates >= 25 && avail_rebates < 28)) {
                    $('#renew-with-creditcard-div li[data-id="' + item_id + '"]').click();
                }
            } else {

                $("#mbr_amount").text(parseFloat(mbr_amount).toFixed(2));

                $("input[name='renew_mbrtype']").val(item_id);
                $("input[name='renew_renewal']").val(parseFloat(mbr_amount).toFixed(2));
                $("input[name='renew_amt']").val(parseFloat(mbr_amount).toFixed(2));
                $("input[name='renew_membertype']").val(item_id);

            }

        });


        $("#rebatecredit-vamount").blur('input', function (e) {
            var amount = $(this).val();
            if (amount < 5) {
                $("#remaining-topay").text(renew_total.toFixed(2));
                $("#rebatecredit-vamount").val('');
                showError("Minimum of V$5 available rebates to use this option.", "Error");
                // alert("Minimum of V$5 available rebates to use this option.");
            } else if (amount > avail_rebates) { //&& amount<(renew_total-1)
                $("#rebatecredit-vamount").val('');
                showError("Exceeded available rebates", "Error");
                // alert("Exceeded available rebates");
            } else if (amount >= renew_total && avail_rebates >= renew_total) {
                $("#rebatecredit-vamount").val('')
                showError("Maximum rebates redemption is V$" + parseFloat(renew_total - 1).toFixed(2), "Error");
                // alert("Maximum rebates redemption is V$" + parseFloat(renew_total - 1).toFixed(2));
            } else {
                if (amount >= avail_rebates) {
                    amount = avail_rebates;
                }
                $("#rebatecredit-vamount").val(parseFloat(amount).toFixed(2));
            }

            var remaining = renew_total - $(this).val();
            if (remaining <= 0) {
                remaining = "0.00";
            } else {
                remaining = remaining.toFixed(2);
            }
            $("#remaining-topay").text(remaining);

        });

        $('#btncheckout_store').on('click', function () {
            $('#storeCardUniqueID').val($("input[name=storeCard]:checked").val())
            //console.log($('#storeCardUniqueID').val());
            var form = document.getElementById("2c2p-tokenize");
            form.submit();
        });

        $(".savedcard-methods").click(function (e) {
            $(".savedcard-methods>div").removeClass("savedcard-methods--active");
            $(".savedcard-methods").find(".icon-div").hide();

            $(this).find(".first").addClass("savedcard-methods--active");
            $(this).find(".icon-div").show();

            var cardid = $(this).find("#savedcardnumber").val();

            $('#2c2p-tokenize input:radio').prop('checked', false);
            $('#2c2p-tokenize input:radio').each(function () {
                var theval = $(this).val();
                if (theval.trim() == cardid.trim()) {
                    $(this).prop('checked', true);
                }
            });
            $("#new_credit_card_div").hide();
            $("#2c2p-tokenize").show();


        });

        $("#type-toggle").click(function (e) {
            $("#new_credit_card_div").toggle();
            $("#2c2p-tokenize").toggle();

            $(".savedcard-methods>div").removeClass("savedcard-methods--active");
            $(".savedcard-methods").find(".icon-div").hide();

        });
    });

    function Checkout() {

        if (isProcessing === true) {
            showError("Transaction is in Progress now.", "Error");
            return;
        }

        if ($('input[name=selectPayment]:checked').val() == "creditcard-div") {
            // $("#cardholderName").val('');
            // $("#cvv").val('');
            if (!$("#cardNumber").val()) {
                showError("Please insert your credit card number.", "Error in Card Info");
                return;
            }
        }

        //Submit above form identified by its 'id' to the backend with encrypted card data
        My2c2p.getEncrypted("2c2p-payment-form", function (encryptedData, errCode, errDesc) {
            if (errCode != 0) {
                showError(errDesc, "Error in Card Info");
                // alert(errDesc);
            } else {
                isProcessing = true;

                //add LAST Check before checkout.
                var theUrl = '<?php echo $check_url; ?>';
                var mbr_id = '<?php echo $mbr_id; ?>';
                var email_addr = '<?php echo $email_addr; ?>';
                var contact_num = '<?php echo $contact_num; ?>';
                var params = {
                    "mbr_id": mbr_id,
                    "email_addr": email_addr,
                    "contact_num": contact_num,
                    "upgrade_type": upgrade_type
                };
                console.log(params);
                $.ajax({
                    url: theUrl,
                    headers: {
                        'X_AUTHORIZATION': 'NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG',
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(params),
                    method: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data.result);
                        console.log(data.code);

                        isProcessing = false;
                        if (data.code === 0) {
                            showError(data.result, data.message);
                            return;

                        } else {

                            //put the data to the form for submitting to backend
                            var form = document.getElementById("2c2p-payment-form");
                            form.encryptedCardInfo.value = encryptedData.encryptedCardInfo;
                            form.maskedCardInfo.value = encryptedData.maskedCardInfo;
                            form.expMonthCardInfo.value = encryptedData.expMonthCardInfo;
                            form.expYearCardInfo.value = encryptedData.expYearCardInfo;
                            form.submit();

                        }
                    },
                    error: function () {
                        console.log("Erropr");
                        isProcessing = false;
                    }
                });

            }
        });
    }

    function RebateCheckout() {
        // debugger;
        var renew_amt = $("input[name='renew_amt']").val();
        // debugger;
        if (avail_rebates < parseFloat(renew_amt)) {
            showError("You do not have enough V$.", "ValueClub");
            return;
        }

        var form = document.getElementById("2c2p-rebate-form");
        form.submit();
    }

    function showError(text, title) {
        $("#error-modal-title").text(title);
        $("#error-modal-body").text(text);
        $("#errorModal").modal('show');
    }

    function maxLengthCheck(object) {
        if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
    }


</script>

</body>
</html>


