# Oauth Server

## 1. OAuth authentication
```bash
// make private key in storage folder
php artisan passport:key --force

// create the encryption keys needed to generate secure access tokens
php artisan passport:install --force

// create client ID
php artisan passport:client --password
php artisan passport:client --personal

```

## 2. Copy the key generated in step 1 to the .env file
```php
PERSONAL_CLIENT_ID=1

PERSONAL_CLIENT_SECRET=

PASSPORT_CLIENT_ID=2

PASSPORT_CLIENT_SECRET=
```

## 3. Login
This project use laravel auth facade, so it need to change password encrypt.
```php
// change vendor/laravel/passport/src/Bridge/UserRepository.php line 53 to:
} elseif (! md5($password) == $user->getAuthPassword()) {
```

# Oauth Client

## 1. Change auth encrypt method
Details in [vendor\laravel\passport\src\Bridge\UserRepository.php](./docker/UserRepository.php) and [vendor\league\oauth2-server\src\Grant\PasswordGrant.php](./docker/PasswordGrant.php) 

## 2. Add new credentials in provider
Details in [app/Providers/CustomUserProvider](./app/Providers/CustomUserProvider)

## 3. Register custom provider
In [app/Providers/AuthServiceProvider](./app/Providers/AuthServiceProvider) add in boot function
```php
public function boot()
    {
        $this->registerPolicies();

        $this->app['auth']->provider('custom', function ($app, array $config) {
            $model = $app['config']['auth.providers.users.model'];
            return new CustomUserProvider($app['hash'], $model);
        });

    }
```

## 4. Change auth config
In config/auth.php
```php
'providers' => [
        'users' => [
            'driver' => 'custom', //change eloquent to custom
            'model' => App\Models\User::class,
        ],
],
```

# API
## 1. Framework
```bash
    +---------+           +----------+          +---------+        +---------+       +------------+
    |  Model  |           |Repository|          | Service |        | Facade  |       | Controller |
    +---------+           +----+-----+          +----+----+        +----+----+       +------+-----+
         |  handles database   |                     |                  |                   |
         +-------------------->|                     |                  |                   |
         |                     |  injects to service |                  |                   |
         |                     +-------------------->|                  |                   |
         |                     |                     |injects controller|                   |
         |                     |                     +----------------->|                   |
         |                     |                     |                  |  call facde       |
         |                     |                     |                  |<------------------+
         |                     |                     |                  |                   |
```

- **Model**: Only as an Eloquent class.
- **Repository**: Auxiliary model that handles database logic and then injects into the service.
- **Service**: Auxiliary controller that handles business logic and then injects into the controller.
- **Facade**: Provide a "static" interface to classes that are available in the service container
- **Controller**: Receives an HTTP request and calls other services.

# Document
```bash
// Install apidoc global
npm i -g apidoc
// Generate api to public
apidoc -f .php -i ./ -o public/apidoc/
apidoc -f .php -i ./app/Http/Controllers/ApiDoc/ -o public/apidoc/
```

# CAS Client

````
// For http setting.
// Change file: /Applications/XAMPP/xamppfiles/htdocs/composer/voyager/vendor/jasig/phpcas/source/CAS/Client.php
// line 317 from: $this->_server['base_url'] = 'https://' . $this->_getServerHostname();
// line 317 to: $this->_server['base_url'] = 'http://' . $this->_getServerHostname();
````


php -d memory_limit=-1 /usr/local/bin/composer.phar require hashids/hashids --ignore-platform-reqs
