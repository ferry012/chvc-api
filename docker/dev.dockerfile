FROM ys87/ubuntu18apache20php72mssql:r1
LABEL maintainer="yongsheng@challenger.sg"

WORKDIR /var/www/html
COPY ./docker/vhost.conf /etc/apache2/sites-enabled/000-default.conf
RUN apt-get update || exit 0
RUN apt-get install -y php7.2-pgsql && apt-get install -y php7.2-curl
RUN apt-get install -y php7.2-gd && apt-get install -y php7.2-zip
RUN echo "extension=pgsql.so" >> /etc/php/7.2/apache2/php.ini
RUN echo "extension=php_curl.dll" >> /etc/php/7.2/apache2/php.ini

RUN chown www-data:www-data -R /var/www/html/*
RUN chmod 775 -R *

RUN apt-get install nano -y

#RUN composer install
COPY ./docker/UserRepository.php ./vendor/laravel/passport/src/Bridge/
COPY ./docker/SessionGuard.php ./vendor/laravel/framework/src/Illuminate/Auth/
COPY ./docker/AuthenticatesUsers.php ./vendor/laravel/framework/src/Illuminate/Foundation/Auth/
COPY ./docker/Gate.php ./vendor/laravel/framework/src/Illuminate/Auth/Access/
COPY ./docker/test.env ./.env

WORKDIR /var/www

