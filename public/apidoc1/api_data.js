define({ "api": [
  {
    "type": "get",
    "url": "/api/ping",
    "title": "Get Environment",
    "group": "0.ENVIRONMENT",
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"code\": \"200\",\n     \"msg\": \"pong\",\n     \"env\": \"local\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "0.ENVIRONMENT",
    "name": "GetApiPing"
  },
  {
    "type": "get",
    "url": "/api/member/emailPreference/{mbrId}",
    "title": "Email Preference",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"first_name\": \"ABC\",\n         \"last_name\": \"DEF\",\n         \"sub_ind1\": \"Y\",\n         \"sub_ind2\": \"YYYYY\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "GetApiMemberEmailpreferenceMbrid"
  },
  {
    "type": "get",
    "url": "/api/member/verify/{mbrId}/{pwd}",
    "title": "Verify Member",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memberId",
            "description": "<p>Member id, email addr or contact num</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pwd",
            "description": "<p>password Must md5()</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"966048869\",\n         \"mbr_title\": \"MR\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"birth_date\": \"1990-01-15 00:00:00\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"status_level\": 1,\n         \"join_date\": \"2018-04-26 11:39:11.28\",\n         \"exp_date\": \"2020-04-26 23:59:59\",\n         \"mbr_type\": \"M\",\n         \"rebate_tier\": 1, // hachi membership details: mbr_rebate_times\n         \"rebate\": \"10\",\n         \"mbr_savings\": 80, // hachi membership details: mbr_total_savings\n         \"can_renew\": \"Y\", // hachi membership details: MembershipRenewal\n         \"mbr_months\": 8,\n         \"can_upgrade\": \"Y\", // hachi membership details: MemberUpgrade\n         \"main_member\": {\n             \"mbr_id\": \"V177017100\",\n             \"first_name\": \"EDMUND\",\n             \"last_name\": \"MEMBER\"\n         },\n         \"last_renewal\": \"1900-01-01 00:00:00\",\n         \"rebate_date\": \"2018-06-30 00:00:00\",\n         \"points_available\": 3151, // hachi membership details: remaining_points\n         \"points_accumulated\": 100,\n         \"points_redeemed\": 139,\n         \"expiring_rebates\": 0,\n         \"coupon\": [\n             {\n                 \"expiry_date\": \"2019-09-30 23:59:59\",\n                 \"coupon_id\": \"!WPPAPERONE\",\n                 \"coupon_code\": \"MEM-PO600580740\",\n                 \"coupon_name\": \"$2.95 off for PaperOne A4 paper\",\n                 \"coupon_excerpt\": \"Purchase PaperOne A4 paper at $2.90 (U.P $6.50)\"\n             },\n         ],\n         \"mbr_type_history\": [\n              {\n                 \"line_num\": 1,\n                 \"eff_from\": \"2017-10-04 23:59:59\",\n                 \"eff_to\": \"2019-10-04 23:59:59\",\n                 \"mbr_type\": \"M\"\n              }\n          ],\n         \"meta\": [\n             {\n                 \"gplaylimit\": \"1000000\",\n                 \"norenewreminder\": \"2021-11-25 23:59:59.000\"\n             }\n         ],\n         \"billing\": {\n             \"street_line1\": \"xxx xxx xxx\",\n             \"street_line2\": \"00\",\n             \"street_line3\": \"000\",\n             \"street_line4\": \"xxx xxx\",\n             \"country_id\": \"SG\",\n             \"postal_code\": \"123456\"\n         },\n         \"shipping\": [],\n         \"rebates_expiry_date\": [\n             {\n                 \"exp_date\": \"Jun 30 2020 12:00:00:AM\",\n                 \"points_available\": 99\n             },\n             {\n                 \"exp_date\": \"Jun 30 2021 12:00:00:AM\",\n                 \"points_available\": 1070\n             }]\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Member ID or password is null.",
          "content": "HTTP/1.1 Member ID or password is null.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member ID or password is null.\"\n }",
          "type": "json"
        },
        {
          "title": "No member found.",
          "content": "HTTP/1.1 No member found.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"No member found.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Password invalid.",
          "content": "HTTP/1.1 Password invalid.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Password invalid.\"\n }",
          "type": "json"
        },
        {
          "title": "Member locked.",
          "content": "HTTP/1.1 Member locked.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member locked.\"\n }",
          "type": "json"
        },
        {
          "title": "Member expired.",
          "content": "HTTP/1.1 Member expired.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member expired.\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "GetApiMemberVerifyMbridPwd"
  },
  {
    "type": "get",
    "url": "/api/validate/{memberId}?source={pos_id}&info={info?}",
    "title": "Validate Member",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memberId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "source",
            "description": "<p>POS ID. If source, it will response 'source_count'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "info",
            "description": "<p>Additional info to retrieve</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"966048869\",\n         \"mbr_title\": \"MR\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"birth_date\": \"1990-01-15 00:00:00\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"status_level\": 1,\n         \"join_date\": \"2018-04-26 11:39:11.28\",\n         \"exp_date\": \"2020-04-26 23:59:59\",\n         \"mbr_type\": \"M\",\n         \"rebate_tier\": 1, // hachi membership details: mbr_rebate_times\n         \"rebate\": \"10\",\n         \"mbr_savings\": 80, // hachi membership details: mbr_total_savings\n         \"can_renew\": \"Y\", // hachi membership details: MembershipRenewal\n         \"mbr_months\": 8,\n         \"can_upgrade\": \"Y\", // hachi membership details: MemberUpgrade\n         \"main_member\": {\n             \"mbr_id\": \"V177017100\",\n             \"first_name\": \"EDMUND\",\n             \"last_name\": \"MEMBER\"\n         },\n         \"last_renewal\": \"1900-01-01 00:00:00\",\n         \"rebate_date\": \"2018-06-30 00:00:00\",\n         \"points_available\": 3151, // hachi membership details: remaining_points\n         \"points_accumulated\": 100,\n         \"points_redeemed\": 139,\n         \"expiring_rebates\": 0,\n         \"source_count\": 1,\n         \"coupon\": [\n             {\n                 \"expiry_date\": \"2019-09-30 23:59:59\",\n                 \"coupon_id\": \"!WPPAPERONE\",\n                 \"coupon_code\": \"MEM-PO600580740\",\n                 \"coupon_name\": \"$2.95 off for PaperOne A4 paper\",\n                 \"coupon_excerpt\": \"Purchase PaperOne A4 paper at $2.90 (U.P $6.50)\"\n             },\n         ],\n         \"mbr_type_history\": [\n              {\n                 \"line_num\": 1,\n                 \"eff_from\": \"2017-10-04 23:59:59\",\n                 \"eff_to\": \"2019-10-04 23:59:59\",\n                 \"mbr_type\": \"M\"\n              }\n          ],\n         \"meta\": [\n             {\n                 \"gplaylimit\": \"1000000\",\n                 \"norenewreminder\": \"2021-11-25 23:59:59.000\"\n             }\n         ],\n         \"billing\": {\n             \"street_line1\": \"xxx xxx xxx\",\n             \"street_line2\": \"00\",\n             \"street_line3\": \"000\",\n             \"street_line4\": \"xxx xxx\",\n             \"country_id\": \"SG\",\n             \"postal_code\": \"123456\"\n         },\n         \"shipping\": [],\n         \"rebates_expiry_date\": [\n             {\n                 \"exp_date\": \"Jun 30 2020 12:00:00:AM\",\n                 \"points_available\": 99\n             },\n             {\n                 \"exp_date\": \"Jun 30 2021 12:00:00:AM\",\n                 \"points_available\": 1070\n             }],\n        \"payment_mode\": {\n             \"dbs\": \"Y\"\n        }\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "GetApiValidateMemberidSourcePos_idInfoInfo"
  },
  {
    "type": "post",
    "url": "/api/member/emailPreference",
    "title": "Edit Email Preference",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "sub_ind1",
            "description": "<p>Sub Ind1, 'Y' or 'N'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "sub_ind2",
            "description": "<p>Sub Ind2, 'YYYYY','NNYYY','YNYYY' or 'NYYYY'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "updated_by",
            "description": "<p>Modified By</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V2100038AS\",\n    \"sub_ind1\": \"Y\",\n    \"sub_ind2\": \"NNYYY\",\n    \"updated_by\": \"3811\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successfully updated\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "PostApiMemberEmailpreference"
  },
  {
    "type": "post",
    "url": "/api/member_item",
    "title": "Get Membership Items",
    "version": "1.0.0",
    "group": "1.MEMBERSHIP",
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n          item_type: \"M28\",\n          item_id: \"!MEMBER-NEW28\",\n          item_desc: \"28 Months\",\n          item_group: \"NEW\",\n          unit_price: \"0.00\",\n          rebate_tier: \"2.00\",\n          can_upgrade: 0,\n          day_can_renew: 180,\n          month_of_duration: 28\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/RegisterController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "PostApiMember_item"
  },
  {
    "type": "post",
    "url": "/api/otp_register",
    "title": "Pre-Register with OTP",
    "version": "1.0.0",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contact",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "source",
            "description": "<p>pos id, or HI, or VC, etc</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "first_name",
            "description": "<p>Option First name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "last_name",
            "description": "<p>Option Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mbr_type",
            "description": "<p>Option Member type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "message_id",
            "description": "<p>Option Validating OTP to complete pre-registration</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "otp_code",
            "description": "<p>Option Validating OTP to complete pre-registration</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"contact\": \"81431879\",\n    \"email\": \"test@test.com\",\n    \"source\": \"B4\",\n    \"first_name\": \"xx\",\n    \"last_name\": \"xxxxx\",\n    \"mbr_type\": \"M08\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"contact\": \"81431879\",\n         \"email\": \"test@test.com\",\n         \"source\": \"B4\",\n         \"first_name\": \"xx\",\n         \"last_name\": \"xxxxx\",\n         \"mbr_type\": \"M08\",\n         \"message\": \"Please retry this API with same message_id & valid otp_code\",\n         \"message_id\": \"X1234567890\",\n         \"otp_code\": \"\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/RegisterController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "PostApiOtp_register"
  },
  {
    "type": "post",
    "url": "/api/register",
    "title": "Pre-Register",
    "version": "1.0.0",
    "group": "1.MEMBERSHIP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contact",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "source",
            "description": "<p>pos id, or HI, or VC, etc</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "first_name",
            "description": "<p>Option First name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "last_name",
            "description": "<p>Option Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mbr_type",
            "description": "<p>Option Member type</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"contact\": \"81431879\",\n    \"email\": \"test@test.com\",\n    \"source\": \"B4\",\n    \"first_name\": \"xx\",\n    \"last_name\": \"xxxxx\",\n    \"mbr_type\": \"M08\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"V1900001B4\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"join_date\": \"2018-04-26 11:39:11.28\",\n         \"exp_date\": \"2020-04-26 23:59:59\",\n         \"mbr_type\": \"M08\",\n         \"rebate\": 0,\n         \"can_renew\": \"X\",\n         \"rebate_date\": null,\n         \"exists\": 1 //if 1 exists, 0 new member.\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "1.MEMBERSHIP",
    "name": "PostApiRegister"
  },
  {
    "type": "get",
    "url": "/qr/check",
    "title": "Check QR Login",
    "group": "1.QR_LOGIN",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "channel_code",
            "description": "<p>Channel Code</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"code\": \"1\",\n     \"msg\": \"Member Login\",\n     \"mbr_id\": \"V1111111ZZ\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/QrloginController.php",
    "groupTitle": "1.QR_LOGIN",
    "name": "GetQrCheck"
  },
  {
    "type": "get",
    "url": "/qr/init",
    "title": "Get QR Code",
    "group": "1.QR_LOGIN",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "loc_id",
            "description": "<p>Location ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pos_id",
            "description": "<p>Source ID (Point-of-Sale ID or Staff ID as an identifier)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"code\": \"1\",\n     \"msg\": \"Connection Init\",\n     \"channel_code\": \"POS2/ABC/EFG#XYZ\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/QrloginController.php",
    "groupTitle": "1.QR_LOGIN",
    "name": "GetQrInit"
  },
  {
    "type": "get",
    "url": "/qr/login",
    "title": "QR Login",
    "group": "1.QR_LOGIN",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "channel_code",
            "description": "<p>Channel Code</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"code\": \"1\",\n     \"msg\": \"Member Login successful\",\n     \"data\": {\n         \"mbr_id\": \"V1111111ZZ\"\n     }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/QrloginController.php",
    "groupTitle": "1.QR_LOGIN",
    "name": "GetQrLogin"
  },
  {
    "type": "get",
    "url": "/api/transaction/item",
    "title": "Get Item Last Purchase",
    "group": "2.TRANSACTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memberId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "itemId",
            "description": "<p>Item ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"item_id\": \"11111\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"V100000100\",\n         \"item_id\": \"11111\",\n         \"trans_id\": \"\",\n         \"trans_date\": \"\",\n         \"trans_qty\": 0,\n         \"total_num\": 0,\n         \"is_today\": false,\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "2.TRANSACTION",
    "name": "GetApiTransactionItem"
  },
  {
    "type": "get",
    "url": "/api/transaction/redemptions/{mbrId}/{from}/{to}",
    "title": "Get Redemptions",
    "group": "2.TRANSACTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "From",
            "description": "<p>Date From eg: 2019-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "To",
            "description": "<p>Date To eg: 2020-01-01</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"loc_id\": \"TRS-O\",\n         \"mbr_id\": \"V183535400\",\n         \"trans_id\": \"1RA00148\",\n         \"trans_date\": \"2018-01-01 16:28:07\",\n         \"item_desc\": \"Star Voucher Redemption\",\n         \"item_qty\": 55,\n         \"regular_price\": 1\n     },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "2.TRANSACTION",
    "name": "GetApiTransactionRedemptionsMbridFromTo"
  },
  {
    "type": "get",
    "url": "/api/transaction/retrieve/{mbrId}/{from}/{to}",
    "title": "Get Transaction",
    "group": "2.TRANSACTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "From",
            "description": "<p>Date From eg: 2019-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "To",
            "description": "<p>Date To eg: 2020-01-01</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"trans_id\": \"P8C01111\",\n         \"trans_type\": \"PS\",\n         \"trans_date\": \"2020-03-17 14:51:13\",\n         \"loc_id\": \"PIT\",\n         \"total_amount\": 8,\n         \"total_rebates\": 0,\n         \"total_savings\": 0,\n         \"items\": [\n             {\n                 \"line_num\": 1,\n                 \"item_id\": \"!MEMBER-NEW08\",\n                 \"item_desc\": \"8-Month Membership\",\n                 \"unit_price\": 8,\n                 \"regular_price\": 8,\n                 \"item_qty\": 1,\n                 \"disc_amount\": 0,\n                 \"rebates\": 0,\n                 \"mbr_savings\": 0\n             },]\n     },]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "2.TRANSACTION",
    "name": "GetApiTransactionRetrieveMbridFromTo"
  },
  {
    "type": "post",
    "url": "/api/transaction",
    "title": "Save Transaction",
    "version": "1.0.0",
    "group": "2.TRANSACTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "loc_id",
            "description": "<p>Location ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "pos_id",
            "description": "<p>Pos ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "rebates_earned",
            "description": "<p>Rebates Earned</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "invoice_num",
            "description": "<p>Invoice Number(transaction id)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_type",
            "description": "<p>Transaction Type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_date",
            "description": "<p>Transaction Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cashier_id",
            "description": "<p>Cashier ID. Option, default is ''.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cashier_name",
            "description": "<p>Cashier Name. Option, default is ''</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "calculate_points",
            "description": "<p>Whether calculate points. Option 'Y' or 'N', default is 'N'</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "customer_info",
            "description": "<p>Customer Information</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "customer_info.id",
            "description": "<p>Customer ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "items",
            "description": "<p>Items</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.line_num",
            "description": "<p>Items Line Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.item_id",
            "description": "<p>Items ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.item_desc",
            "description": "<p>Items Description</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.qty_ordered",
            "description": "<p>Items Ordered quantity</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.unit_price",
            "description": "<p>Items Unit Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "items.unit_discount",
            "description": "<p>Items Unit Discount. Options default 0</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.regular_price",
            "description": "<p>Items Regular Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items.total_price",
            "description": "<p>Items Total Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "items.trans_point",
            "description": "<p>Items Transaction Points. Options default 0</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "items.mbr_savings",
            "description": "<p>Items Member Savings. Options default 0</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "coupons",
            "description": "<p>Coupons</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "coupons.coupon_id",
            "description": "<p>Coupon ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "coupons.coupon_name",
            "description": "<p>Coupon Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "coupons.coupon_code",
            "description": "<p>Coupon Code</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "coupons.amount",
            "description": "<p>Coupon Amount</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "payments",
            "description": "<p>Payments</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "payments.pay_mode",
            "description": "<p>Payment Mode</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"loc_id\": \"BF\",\n    \"pos_id\": \"B9\",\n    \"rebates_earned\": \"83.49\",\n    \"invoice_num\": \"B9B00064\",\n    \"trans_type\": \"PS\",\n    \"trans_date\": \"2020-03-31 21:22:44\",\n    \"cashier_id\": \"3810\",\n    \"customer_info\": {\n        \"id\": \"V100000100\"\n    },\n    \"items\": [\n    {\n        \"line_num\": 1,\n        \"item_id\": \"!MEMBER-UPG18\",\n        \"item_desc\": \"XXLogitech 920-008617/8 Slim Folio for iPad 9.7\",\n        \"qty_ordered\": 1,\n        \"unit_price\": \"18.00\",\n        \"unit_discount\": \"0.00\",\n        \"regular_price\": \"18.00\",\n        \"total_price\": \"18.00\",\n        \"mbr_savings\": 66,\n        \"trans_point\": 77,\n    },],\n    \"payments\": [\n    {\n        \"pay_mode\": \"CASH\",\n        \"info\": \"\",\n        \"approval_code\": null,\n        \"trans_amount\": \"6.68\"\n    },\n    {\n        \"pay_mode\": \"CARD\",\n        \"info\": \"[UOB_AMEX/*****7522]\",\n        \"approval_code\": \"423910\",\n        \"trans_amount\": \"6.68\"\n    },\n    {\n        \"pay_mode\": \"REBATE\",\n        \"info\": \"\",\n        \"approval_code\": null,\n        \"trans_amount\": \"6.68\"\n    },\n    {\n        \"pay_mode\": \"EGIFT\",\n        \"coupon_serialno\": \"MEM-OTEPECB\"\n    },\n    {\n        \"pay_mode\": \"ECREDIT\",\n        \"coupon_serialno\": \"VCH-CR000000491\"\n    }],\n    \"coupons\": [\n        {\n            \"coupon_id\": \"!WPHP38INK\",\n            \"coupon_name\": \"$38 off HP Ink Cartridges with min spend $38 \",\n            \"coupon_code\": \"HP-AIRPVA8D657\",\n            \"amount\": null\n        },\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Transaction success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful save transaction\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Reserved rebate error",
          "content": "HTTP/1.1 reserved rebate error\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Sorry, your reserved rebate is overdue or unactivated, please try again\"\n }",
          "type": "json"
        },
        {
          "title": "Transaction error",
          "content": "HTTP/1.1 transaction error\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Whoops! There was a problem processing your transaction. Please try again.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "2.TRANSACTION",
    "name": "PostApiTransaction"
  },
  {
    "type": "get",
    "url": "/api/points/available/{mbrId}",
    "title": "Points Remaining",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"966048869\",\n         \"rebate_amount\": \"51.69\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "No member found.",
          "content": "HTTP/1.1 No member found.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"No member found.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "GetApiPointsAvailableMbrid"
  },
  {
    "type": "get",
    "url": "/api/points/check",
    "title": "Points Check",
    "version": "1.0.0",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"P9C01234\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Check success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"V100000100\",\n         \"trans_id\": \"P9C01234\",\n         \"rebate_available\": \"11.69\",\n         \"trans_rebate\": \"3.50\",\n         \"refund_status\": true\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "No Record",
          "content": "HTTP/1.1 no record\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Whoops, There is no record in our server.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "GetApiPointsCheck"
  },
  {
    "type": "post",
    "url": "/api/points/award",
    "title": "Points Award",
    "version": "1.0.0",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "rebate_amount",
            "description": "<p>Rebate amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "rebate_description",
            "description": "<p>Rebate Description</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "transfer",
            "description": "<p>Transfer 0 or 1, if 1, will ignore 'MAS'</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"P9C01234\",\n    \"rebate_amount\": 3.50,\n    \"rebate_description\": \"Valueclub App Game\",\n    \"transfer\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Award success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\":\"Successful save award\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Whoops! There was a problem processing your award. Please try again.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "PostApiPointsAward"
  },
  {
    "type": "post",
    "url": "/api/points/deduct",
    "title": "Points Deduct",
    "version": "1.0.0",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "rebate_amount",
            "description": "<p>Rebate amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "rebate_description",
            "description": "<p>Rebate Description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"P9C01234\",\n    \"rebate_amount\": -3.50,\n    \"rebate_description\": \"Valueclub App Game\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Award success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\":\"Successful save deduct\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Whoops! There was a problem processing your deduct. Please try again.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "PostApiPointsDeduct"
  },
  {
    "type": "post",
    "url": "/api/points/reserved",
    "title": "Points Reserved",
    "version": "1.0.0",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "rebate_amount",
            "description": "<p>Rebate Amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "expiry_type",
            "description": "<p>Set Expiry Date, 0 or 1. If 1 add 15 days.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"test99\",\n    \"rebate_amount\": 5\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Reserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Transaction reserved successfully\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "rebate is not enough",
          "content": "HTTP/1.1 Key rebate is not enough\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"sorry, your rebate is not enough\"\n }",
          "type": "json"
        },
        {
          "title": "Reserved error",
          "content": "HTTP/1.1 reserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Transaction reserved error.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "PostApiPointsReserved"
  },
  {
    "type": "post",
    "url": "/api/points/unreserved",
    "title": "Points Unreserved",
    "version": "1.0.0",
    "group": "3.MBR_REBATES",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mbr_id",
            "description": "<p>Option Member Id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Basket ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"test99\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Unreserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Transaction unreserved successfully\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Unreserved error",
          "content": "HTTP/1.1 unreserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Transaction unreserved error.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "3.MBR_REBATES",
    "name": "PostApiPointsUnreserved"
  },
  {
    "type": "post",
    "url": "/api/redeem/voucher",
    "title": "Redeem Voucher",
    "version": "1.0.0",
    "group": "4.REDEMPTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "voucher_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"voucher_serialno\": \"UOB001\",\n    \"mbr_id\": \"V100000100\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Reserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Redemption Successful. V$60 rebate has been added into your account\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Server error",
          "content": "HTTP/1.1 reserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Whoops! There was a problem processing your redeem. Please try again.\"\n }",
          "type": "json"
        },
        {
          "title": "Redeem expired or used",
          "content": "HTTP/1.1 reserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid coupon code\"\n }",
          "type": "json"
        },
        {
          "title": "Serial Number invalid",
          "content": "HTTP/1.1 reserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid coupon code\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.REDEMPTION",
    "name": "PostApiRedeemVoucher"
  },
  {
    "type": "post",
    "url": "/api/redemption/items",
    "title": "Get Redemption Items",
    "group": "4.REDEMPTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "min_price",
            "description": "<p>Min Price</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "max_price",
            "description": "<p>Max Price</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"min_price\": 20,\n    \"max_price\": 40\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Issue Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"loc_name\": \"Bugis Junction (Flagship Store)\",\n             \"img_name\": \"3f0fe9f597602b4a39b3f48e75c356fe.png\",\n             \"discount_value\": \"-70%\",\n             \"redeem_price\": 29.7,\n             \"coy_id\": \"CTL\",\n             \"line_num\": 94,\n             \"item_id\": \"0745883775293  \",\n             \"item_desc\": \"Belkin Boostup Wireless Charging Stand 10W (White)\",\n             \"points_required\": 2970,\n             \"eff_from\": \"2020-11-04 14:55:43\",\n             \"eff_to\": \"2021-11-04 14:55:43\",\n             \"image_name\": \"3f0fe9f597602b4a39b3f48e75c356fe.png\",\n             \"created_by\": \"ws             \",\n             \"created_on\": \"2020-11-04 14:55:43\",\n             \"modified_by\": \"               \",\n             \"modified_on\": \"2020-11-04 14:55:43\",\n             \"updated_on\": \"2020-11-04 14:55:43\",\n             \"loc_id\": \"BF\",\n             \"discount_amt\": \"70%\",\n             \"unit_price\": 79\n         },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "key code",
          "content": "HTTP/1.1 key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"key code is invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/RedemptionController.php",
    "groupTitle": "4.REDEMPTION",
    "name": "PostApiRedemptionItems"
  },
  {
    "type": "post",
    "url": "/api/redemption/transaction",
    "title": "Post Redemption",
    "group": "4.REDEMPTION",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "item_id",
            "description": "<p>Item ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "item_desc",
            "description": "<p>Item Desc</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "redeem_price",
            "description": "<p>Redeem Price</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V183535400\",\n    \"item_id\": \"8888880156688\",\n    \"item_desc\": \"ValueClub Exclusive Clarity Air True Wireless Earbuds (Grey)\",\n    \"redeem_price\": \"20.97\",\n    \"unit_price\": \"100\",\n    \"loc_id\": \"BF\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Issue Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful save redemption\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "invalid member",
          "content": "HTTP/1.1 invalid member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not eligible to redeem the item.\"\n }",
          "type": "json"
        },
        {
          "title": "Insufficient rebates",
          "content": "HTTP/1.1 Insufficient rebates\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Insufficient rebates.\"\n }",
          "type": "json"
        },
        {
          "title": "invalid item",
          "content": "HTTP/1.1 invalid item\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, the item is not valid now.\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/RedemptionController.php",
    "groupTitle": "4.REDEMPTION",
    "name": "PostApiRedemptionTransaction"
  },
  {
    "type": "get",
    "url": "/api/egifts/egift_claimed/{mbrId}/{status?}",
    "title": "eGifts Claimed",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "status",
            "description": "<p>Status Level, 0 is not in use, 1 is used. default is 0</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"image\": \"\",\n         \"expiry_date\": \"2020-04-16 23:59:59\",\n         \"voucher_amount\": 0,\n         \"status_level\": 0,\n         \"coupon_serialno\": \"MEM-9CTL6J2\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or serial no is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member ID or Serial No is null.\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "GetApiEgiftsEgift_claimedMbridStatus"
  },
  {
    "type": "get",
    "url": "/api/egifts/egift_purchased/{transId}",
    "title": "eGifts Purchased",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "transId",
            "description": "<p>Trans id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"image\": \"\",\n         \"claim_expiry\": \"2020-04-16 23:59:59\",\n         \"voucher_amount\": 0,\n         \"status_level\": 0,\n         \"coupon_serialno\": \"MEM-9CTL6J2\",\n         \"coupon_expiry_date\": \"2021-06-08 23:59:59\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Trans id invalid",
          "content": "HTTP/1.1 Trans id invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Trans id invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "GetApiEgiftsEgift_purchasedTransid"
  },
  {
    "type": "post",
    "url": "/api/egifts/claim",
    "title": "Claim Egift",
    "version": "1.0.0",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "recipient_email",
            "description": "<p>Recipient Email</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "claim_by",
            "description": "<p>Claim By</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"recipient_email\": \"test@challenger.sg\",\n    \"coupon_serialno\": \"GV8XA2WGKD\",\n    \"claim_by\": \"3810\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Claim success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful claim\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Coupon Expiry",
          "content": "HTTP/1.1 Coupon Expiry\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Coupon Expiry\"\n }",
          "type": "json"
        },
        {
          "title": "Coupon has been claimed",
          "content": "HTTP/1.1 Coupon has been claimed\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Coupon has been claimed\"\n }",
          "type": "json"
        },
        {
          "title": "Invalid Serial No",
          "content": "HTTP/1.1 Invalid Serial No\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Invalid Serial No\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "PostApiEgiftsClaim"
  },
  {
    "type": "post",
    "url": "/api/egifts/insert",
    "title": "Insert Egift",
    "version": "1.0.0",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coy_id",
            "description": "<p>Coy ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_id",
            "description": "<p>Coupon ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Trans ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "buyer_id",
            "description": "<p>Buyer ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "buyer_name",
            "description": "<p>Buyer Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "recipient_name",
            "description": "<p>Recipient Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "recipient_email",
            "description": "<p>Recipient Email</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "gift_message",
            "description": "<p>Gift Message</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "gift_image",
            "description": "<p>Gift Image</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "trans_qty",
            "description": "<p>Trans Qty</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "trans_amount",
            "description": "<p>Trans Amount</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"coy_id\": \"CTL\",\n    \"coupon_id\": \"!EGIFT-20\",\n    \"trans_id\": \"TEST1111\",\n    \"buyer_id\": \"V150357700\",\n    \"buyer_name\": \"Test\",\n    \"recipient_name\": \"test\",\n    \"recipient_email\": \"li.liangze@challenger.sg\",\n    \"gift_message\": \"Enjoy\",\n    \"gift_image\": \"test.jpg\",\n    \"trans_qty\": 100,\n    \"trans_amount\": 100\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Insert success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful insert egift\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Buyer id error",
          "content": "HTTP/1.1 buyer id error\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Sorry, buyer_id invalid.\"\n }",
          "type": "json"
        },
        {
          "title": "recipient_name invalid",
          "content": "HTTP/1.1 recipient_name invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Sorry, recipient_name invalid.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "PostApiEgiftsInsert"
  },
  {
    "type": "post",
    "url": "/api/egifts/update_egift",
    "title": "Update Egift",
    "version": "1.0.0",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "expiry_date",
            "description": "<p>Expiry Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "status_level",
            "description": "<p>Status Level only 0 or 1</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "updated_by",
            "description": "<p>Update By</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"coupon_serialno\": \"GV8XA2WGKD\",\n    \"expiry_date\": \"2022-02-01 00:00:00\",\n    \"status_level\": 0,\n    \"updated_by\": \"3810\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Update success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful update\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Invalid Serial No",
          "content": "HTTP/1.1 Invalid Serial No\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Invalid Serial No\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "PostApiEgiftsUpdate_egift"
  },
  {
    "type": "post",
    "url": "/api/egifts/update_egift_coupon",
    "title": "Update Egift Coupon",
    "version": "1.0.0",
    "group": "4.VOUCHERS-EGIFT",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "expiry_date",
            "description": "<p>Expiry Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "status_level",
            "description": "<p>Status Level only 0 or 1</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "updated_by",
            "description": "<p>Update By</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"coupon_serialno\": \"GV8XA2WGKD\",\n    \"expiry_date\": \"2022-02-01 00:00:00\",\n    \"status_level\": 0,\n    \"updated_by\": \"3810\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Update success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successful update\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Invalid Serial No",
          "content": "HTTP/1.1 Invalid Serial No\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Invalid Serial No\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/EgiftController.php",
    "groupTitle": "4.VOUCHERS-EGIFT",
    "name": "PostApiEgiftsUpdate_egift_coupon"
  },
  {
    "type": "get",
    "url": "/api/coupon/credit/{serial}",
    "title": "eCredit Serial",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial",
            "description": "<p>Coupon Serial Number</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"expiry_date\": \"2021-05-26 00:00:00\",\n         \"status_level\": 1,\n         \"coupon_serialno\": \"SPRFV-000000040\",\n         \"receipt_id1\": \"S3D18867\",\n         \"receipt_id2\": \"S3D18873\",\n         \"voucher_amount\": \"898.00\",\n         \"issue_date\": \"2021-04-26 20:22:36.663\",\n         \"utilize_date\": \"2021-04-27 09:16:36\",\n         \"mbr_id\": \"V183535400\",\n         \"reserved\": 0\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Invalid Serial",
          "content": "HTTP/1.1 Invalid Serial\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid Serial\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiCouponCreditSerial"
  },
  {
    "type": "get",
    "url": "/api/coupon/details/{mbrId}/{statusLevel?}",
    "title": "Get Coupon in Details",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memberId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "statusLevel",
            "description": "<p>Member id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"coy_id\": \"CTL\",\n         \"coupon_id\": \"!WPFB20/200    \",\n         \"coupon_serialno\": \"VCV3VE-00000634\",\n         \"promocart_id\": \"!WPFB20/200    \",\n         \"mbr_id\": \"S0124386A\",\n         \"trans_id\": \"\",\n         \"trans_date\": \"2018-01-19 21:02:09.043\",\n         \"ho_ref\": \"\",\n         \"ho_date\": \"2018-01-19 21:02:09.043\",\n         \"issue_date\": \"2018-01-19 21:02:09.773\",\n         \"sale_date\": \"2018-01-19 21:02:09.773\",\n         \"utilize_date\": \"2018-01-19 21:02:09.043\",\n         \"expiry_date\": \"2018-02-02 23:59:00\",\n         \"print_date\": \"2018-01-19 21:02:09.773\",\n         \"loc_id\": \"VC   \",\n         \"receipt_id1\": \"V3A01174       \",\n         \"receipt_id2\": \"               \",\n         \"voucher_amount\": \"-20.00\",\n         \"redeemed_amount\": \"0.00\",\n         \"expired_amount\": \"0.00\",\n         \"issue_type\": \"eVoucher\",\n         \"issue_reason\": \"ValueClub sign up / membership renewal\",\n         \"status_level\": 0,\n         \"created_by\": \"3506                                              \",\n         \"created_on\": \"2018-01-19 21:02:09.773\",\n         \"modified_by\": \"3506                                              \",\n         \"modified_on\": \"2018-01-19 21:02:09.773\",\n         \"guid\": \"77851114-18F4-4BC0-A25C-D1431F7D4363\"\n     },]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"key code is invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Voucher find error.",
          "content": "HTTP/1.1 Voucher find error.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Voucher find error.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiCouponDetailsMbridStatuslevel"
  },
  {
    "type": "get",
    "url": "/api/coupon/list/{mbrId}",
    "title": "Get Coupon in List",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"expiry_date\": \"2020-04-16 23:59:59\",\n         \"status_level\": 0,\n         \"coupon_id\": \"!WP-VCNEP-JAN  \",\n         \"coupon_serialno\": \"MEM-9CTL6J2\",\n         \"seq_num\": \"1\",\n         \"voucher_amount\": 0,\n         \"issue_date\": \"2020-03-17 16:39:05\"\n     },],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiCouponListMbrid"
  },
  {
    "type": "get",
    "url": "/api/coupon/payment/{mbrId}/{status?}",
    "title": "Payment Voucher",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "status",
            "description": "<p>Status Level, 0 is not in use, 1 is used. default is 0</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n          \"coupon_id\": \"!P-PCDREAMCTL\",\n          \"coupon_name\": \"PC Dream Payment Voucher\",\n          \"expiry_date\": \"2021-08-26 23:59:59\",\n          \"status_level\": 0,\n          \"coupon_serialno\": \"C21000000000683\",\n          \"receipt_id1\": \"\",\n          \"receipt_id2\": \"\",\n          \"voucher_amount\": \"10.00\",\n          \"issue_date\": \"2021-07-27 14:59:03\",\n          \"utilize_date\": \"1900-01-01 00:00:00\"\n     },],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiCouponPaymentMbridStatus"
  },
  {
    "type": "get",
    "url": "/api/coupon/tnc/{mbrId}/{serialNo}",
    "title": "Get Coupon T&C",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serialNo",
            "description": "<p>Coupon Serial No</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"coupon_serialno\": \"MEM-W7L6I8U\",\n         \"issue_date\": \"2019-08-14 14:47:26\",\n         \"expiry_date\": \"2019-11-30 00:00:00\",\n         \"coupon_name\": \"Purchase Valore LTL18 LED Table Lamp at $18\",\n         \"coupon_desc\": \"Purchase Valore LTL18 LED Table Lamp at $18\",\n         \"image_url\": \"\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or serial no is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member ID or Serial No is null.\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiCouponTncMbridSerialno"
  },
  {
    "type": "get",
    "url": "/api/ecredits/{mbrId}/{status?}",
    "title": "ECredits",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "status",
            "description": "<p>Status Level, 0 is not in use, 1 is used. default is 0</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"expiry_date\": \"2019-11-30 00:00:00\",\n         \"status_level\": 1,\n         \"coupon_serialno\": \"DD18-50OFF\",\n         \"receipt_id1\": \"\",\n         \"receipt_id2\": \"HIA24034\",\n         \"voucher_amount\": 0,\n         \"issue_date\": \"2018-11-01 00:00:00\",\n         \"utilize_date\": \"2018-11-22 11:14:35\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or serial no is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member ID or Serial No is null.\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "GetApiEcreditsMbridStatus"
  },
  {
    "type": "post",
    "url": "/api/coupon/credit",
    "title": "Save Credit",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_serialno",
            "description": "<p>Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"coupon_serialno\": \"V100000100\",\n    \"trans_id\": \"test99\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Save success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Save success\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "eCredit invalid",
          "content": "HTTP/1.1 eCredit invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid serial number\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiCouponCredit"
  },
  {
    "type": "post",
    "url": "/api/coupon/issue",
    "title": "Issue Coupon",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "coy_id",
            "description": "<p>COY ID, Option, default is 'CTL'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_id",
            "description": "<p>Coupon ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "promocart_id",
            "description": "<p>Promocart ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "trans_id",
            "description": "<p>Transaction ID, Option, default is ''</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "trans_date",
            "description": "<p>Transaction Date, Option, default is now()</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ho_ref",
            "description": "<p>Option, default is ''</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ho_date",
            "description": "<p>Option, default is now()</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "issue_date",
            "description": "<p>Option, default is now()</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "sale_date",
            "description": "<p>Option, default is now()</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "utilize_date",
            "description": "<p>Option, default is &quot;1900-01-01 00:00:00&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "expired_date",
            "description": "<p>Expired Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "print_date",
            "description": "<p>Print Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "loc_id",
            "description": "<p>Location ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "receipt_id1",
            "description": "<p>Receipt ID</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "voucher_amount",
            "description": "<p>Voucher Amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "issue_type",
            "description": "<p>Issue Type</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "status_level",
            "description": "<p>Status Level</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "created_by",
            "description": "<p>Created BY</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "guid",
            "description": "<p>GUID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"coy_id\": \"CTL\",\n    \"coupon_id\": \"!WPSEPBUBM99\",\n    \"coupon_serialno\": \"BFB4VE-00992562\",\n    \"promocart_id\": \"!WPSEPBUBM99\",\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"\",\n    \"ho_ref\": \"\",\n    \"expiry_date\": \"2019-09-17 23:59:00\",\n    \"print_date\": \"2019-09-17 23:59:00\",\n    \"loc_id\": \"PS\",\n    \"receipt_id1\": \"B4A04139\",\n    \"voucher_amount\": -10,\n    \"issue_type\": \"eVoucher\",\n    \"status_level\": 0,\n    \"created_by\": \"3800\",\n    \"guid\": \"6863F0C4-E9E5-4Q92-AD34-2FA8F18FB1CA\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Reserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Voucher saved successfully.\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Reserved error",
          "content": "HTTP/1.1 reserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Voucher saved error.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiCouponIssue"
  },
  {
    "type": "post",
    "url": "/api/coupon/partner",
    "title": "Issue partner Coupon",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "couponId",
            "description": "<p>Coupon Id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "partnerId",
            "description": "<p>Partner Id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "voucherAmount",
            "description": "<p>Voucher Amount</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"coupon_id\": \"M18\",\n    \"partner_id\": \"C1B1123456\",\n    \"voucher_amount\": 10\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Issue Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": \"Success issue coupon\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "key code",
          "content": "HTTP/1.1 key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid Validated Token\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiCouponPartner"
  },
  {
    "type": "post",
    "url": "/api/coupon/redeem",
    "title": "Redeem Coupon",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "voucher_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"voucher_serialno\": \"UOB001\",\n    \"mbr_id\": \"V100000100\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Reserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"coy_id\": \"CTL\",\n         \"coupon_id\": \"!WPSEPBUBM99\",\n         \"coupon_serialno\": \"BFB4VE-00992562\",\n         \"promocart_id\": \"!WPSEPBUBM99\",\n         \"mbr_id\": \"V100000100\",\n         \"trans_id\": \"\",\n         \"trans_date\": \"2021-04-23 12:42:55\",\n         \"ho_ref\": \"\",\n         \"ho_date\": \"2021-04-23 12:42:55\",\n         \"issue_date\": \"2021-04-23 12:42:55\",\n         \"sale_date\": \"2021-04-23 12:42:55\",\n         \"utilize_date\": \"2021-04-23 12:42:55\",\n         \"expiry_date\": \"2019-09-17 23:59:00\",\n         \"print_date\": \"2019-09-17 23:59:00\",\n         \"loc_id\": \"PS\",\n         \"receipt_id1\": \"B4A04139\",\n         \"receipt_id2\": \"\",\n         \"voucher_amount\": -10,\n         \"redeemed_amount\": \"0.00\",\n         \"expired_amount\": \"0.00\",\n         \"issue_type\": \"eVoucher\",\n         \"issue_reason\": \"\",\n         \"status_level\": 0,\n         \"created_by\": \"3800\",\n         \"created_on\": \"2021-04-23 12:42:55\",\n         \"modified_by\": \"hachi\",\n         \"modified_on\": \"2021-04-23 12:48:50\",\n         \"guid\": \"6863F0C4-E9E5-4Q92-AD34-2FA8F18FB1CA\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Serial Number invalid",
          "content": "HTTP/1.1\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Invalid coupon code\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/HachiController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiCouponRedeem"
  },
  {
    "type": "post",
    "url": "/api/coupon/welcomepack",
    "title": "Issue Welcome Coupon",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrType",
            "description": "<p>Member Type: only M08, M18, M28, MST</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "transId",
            "description": "<p>Trans Id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "transDate",
            "description": "<p>Trans Date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"mbr_type\": \"M18\",\n    \"trans_id\": \"C1B1123456\",\n    \"trans_date\": \"2020-01-01 14:00:00\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Issue Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": \"Success issue coupon\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "key code",
          "content": "HTTP/1.1 key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"key code is invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiCouponWelcomepack"
  },
  {
    "type": "post",
    "url": "/api/ecredits/reserved",
    "title": "eCredits Reserved",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "expiry_type",
            "description": "<p>Set Expiry Date, 0 or 1. If 1 add 15 days.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "ecredits",
            "description": "<p>ECredits</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ecredits.coy_id",
            "description": "<p>Coy ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ecredits.coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "egifts",
            "description": "<p>EGifts</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "egifts.coy_id",
            "description": "<p>Coy ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "egifts.coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "vouchers",
            "description": "<p>Vouchers</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "vouchers.coy_id",
            "description": "<p>Coy ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "vouchers.coupon_serialno",
            "description": "<p>Coupon Serial Number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"test99\",\n    \"ecredits\": [{\n        \"coy_id\": \"CTL\",\n        \"coupon_serialno\": \"VCH-CR000000492\"\n    },]\n    \"egifts\": [],\n    \"vouchers\": [{\n        \"coy_id\": \"CTL\",\n        \"coupon_serialno\": \"PC-DREAM\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Reserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"eCredit or eGift reserved successfully\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Id invalid",
          "content": "HTTP/1.1 Member Id invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, we could not find your member id.\"\n }",
          "type": "json"
        },
        {
          "title": "trans_id exists.",
          "content": "HTTP/1.1 trans_id exists\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, trans_id has exists.\"\n }",
          "type": "json"
        },
        {
          "title": "eCredit invalid",
          "content": "HTTP/1.1 eCredit invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, your eCredit is invalid or reserved. Please check.\"\n }",
          "type": "json"
        },
        {
          "title": "eGift invalid",
          "content": "HTTP/1.1 eCredit invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, your eGift is invalid or reserved. Please check.\"\n }",
          "type": "json"
        },
        {
          "title": "Voucher invalid",
          "content": "HTTP/1.1 Voucher invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, your voucher is invalid or reserved. Please check.\"\n }",
          "type": "json"
        },
        {
          "title": "Server error",
          "content": "HTTP/1.1 Server error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Whoops! There was a problem processing your reserve. Please try again.\"\n }",
          "type": "json"
        },
        {
          "title": "egifts and ecredit empty",
          "content": "HTTP/1.1\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Please at least insert egifts of ecredits.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiEcreditsReserved"
  },
  {
    "type": "post",
    "url": "/api/ecredits/unreserved",
    "title": "eCredits Unreserved",
    "version": "1.0.0",
    "group": "4.VOUCHERS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mbr_id",
            "description": "<p>Option Member Id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Trans ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V100000100\",\n    \"trans_id\": \"test99\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Unreserved success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"ECredits unreserved successfully\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Unreserved error",
          "content": "HTTP/1.1 unreserved error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"ECredits unreserved error.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/PosController.php",
    "groupTitle": "4.VOUCHERS",
    "name": "PostApiEcreditsUnreserved"
  },
  {
    "type": "get",
    "url": "/api/corporate/all_cust",
    "title": "Customer List",
    "group": "7.CORPORATE",
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"coy_id\": \"CBD\",\n         \"cust_id\": \"AA0007\",\n         \"cust_type\": \"T\",\n         \"cust_name\": \"ASIA ASG LLP\",\n         \"cust_addr\": \"0-PRIMARY\",\n         \"email_addr\": \"\",\n         \"tel_code\": \"90606565\",\n         \"mbr_id\": \"V2100096H4\",\n         \"first_name\": \"TEST\",\n         \"last_name\": \"TEST\",\n         \"coy_name\": \"TEST\",\n         \"coy_roc\": \"DSDFWKJEHER\",\n         \"coy_size\": \"1-30\",\n         \"exp_date\": \"2023-01-01 23:59:59\"\n     },]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "GetApiCorporateAll_cust"
  },
  {
    "type": "get",
    "url": "/api/corporate/profile/{mbrId}",
    "title": "Corporate Profile",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbrId",
            "description": "<p>Member id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"coy_id\": \"CTL\",\n         \"cust_id\": \"AA0006\",\n         \"cust_type\": null,\n         \"cust_name\": null,\n         \"tel_code\": \"787013142\",\n         \"email_addr\": \"test_cole2393@gmail.com\",\n         \"mbr_id\": \"V2100134H4\",\n         \"coy_name\": \"PCCONSULTANCY APAC\",\n         \"first_name\": \"TESasd11\",\n         \"last_name\": \"asdfas\",\n         \"coy_name\": \"ASICS ASIA PTE LTD\",\n         \"coy_roc\": \"DSD22FWKJEHER\",\n         \"coy_size\": \"1-30\",\n         \"exp_date\": \"2022-03-15 23:59:59\",\n         \"primary_details\": {\n             \"street_line1\": \"8 Cross Road #24-05\",\n             \"street_line2\": \"PWC Building\",\n             \"street_line3\": \"\",\n             \"street_line4\": \"\",\n             \"country_id\": \"SG\",\n             \"postal_code\": \"\"\n         },\n         \"delivery_details\": []\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        },
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Member or key code is null",
          "content": "HTTP/1.1 Member or key code is null\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Member or key code is null\"\n }",
          "type": "json"
        },
        {
          "title": "Can not find member",
          "content": "HTTP/1.1 Can not find member\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Can not find member\"\n }",
          "type": "json"
        },
        {
          "title": "Sorry, you are not officially member.",
          "content": "HTTP/1.1 Sorry, you are not officially member.\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Sorry, you are not officially member.\"\n }",
          "type": "json"
        },
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "GetApiCorporateProfileMbrid"
  },
  {
    "type": "get",
    "url": "/api/get_corporate_associate/{main_id}",
    "title": "Corporate Associate",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "main_id",
            "description": "<p>Main Member Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"mbr_id\": \"966048869\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"mbr_type\": \"M\",\n     },]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "GetApiGet_corporate_associateMain_id"
  },
  {
    "type": "get",
    "url": "/api/latest_register/{main_id}",
    "title": "Latest Register",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "main_id",
            "description": "<p>Main Member Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"mbr_id\": \"966048869\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"mbr_type\": \"M\",\n         \"status\": \"Activate\",\n         \"message\": \"Already Registered.\"\n     },]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "GetApiLatest_registerMain_id"
  },
  {
    "type": "post",
    "url": "/api/corporate_associate_register",
    "title": "Corporate Associate Register",
    "version": "1.0.0",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "main_id",
            "description": "<p>Main Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "associate",
            "description": "<p>Associate Member</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "associate.contact",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "associate.email",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "associate.first_name",
            "description": "<p>Option First name</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "associate.last_name",
            "description": "<p>Option Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "associate.role",
            "description": "<p>Role, 'CORP_STAFF' or 'CORP_PURCH'</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"main_id\": \"V2100014H4\",\n    \"associate\": [{\n        \"contact\": \"81431879\",\n        \"email\": \"test@test.com\",\n        \"first_name\": \"xx\",\n        \"last_name\": \"xxxxx\",\n        \"role\": \"CORP_STAFF\"\n    },]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [{\n         \"mbr_id\": \"V1900001B4\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"join_date\": \"2018-04-26 11:39:11.28\",\n         \"exp_date\": \"2020-04-26 23:59:59\",\n         \"mbr_type\": \"M28\",\n         \"status\": 1,\n         \"message\": \"Member Exists\" //'Email Exists', 'Contact Exists', 'Success Register', 'Unknown Error'\n     },],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Main Member Invalid",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Main Member invalid.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "PostApiCorporate_associate_register"
  },
  {
    "type": "post",
    "url": "/api/corporate_register",
    "title": "Corporate Register",
    "version": "1.0.0",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cust_id",
            "description": "<p>Cust Id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "company_name",
            "description": "<p>Company Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "company_roc",
            "description": "<p>Company Roc</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "company_size",
            "description": "<p>Company Size</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contact",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_pwd",
            "description": "<p>Password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "first_name",
            "description": "<p>Option First name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "last_name",
            "description": "<p>Option Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "birth_date",
            "description": "<p>Option Birth Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "created_by",
            "description": "<p>Option Created By</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"cust_id\": \"CP0016\",\n    \"company_name\": \"TEST\",\n    \"company_roc\": \"DSDFWKJEHER\",\n    \"company_size\": \"1-30\",\n    \"contact\": \"81431879\",\n    \"email\": \"test@test.com\",\n    \"first_name\": \"xx\",\n    \"last_name\": \"xxxxx\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"mbr_id\": \"V1900001B4\",\n         \"first_name\": \"xxx\",\n         \"last_name\": \"xxx\",\n         \"email_addr\": \"xxx@xxx.com\",\n         \"contact_num\": \"81431879\",\n         \"join_date\": \"2018-04-26 11:39:11.28\",\n         \"exp_date\": \"2020-04-26 23:59:59\",\n         \"mbr_type\": \"CORP\"\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "PostApiCorporate_register"
  },
  {
    "type": "post",
    "url": "/api/update_member",
    "title": "Update Member",
    "version": "1.0.0",
    "group": "7.CORPORATE",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Member ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "contact",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "email",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "old_pwd",
            "description": "<p>Old Password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mbr_pwd",
            "description": "<p>New Password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "status_level",
            "description": "<p>Status Level, should be 0 or 1.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "first_name",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "last_name",
            "description": "<p>Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "birth_date",
            "description": "<p>Birth Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "sub_ind1",
            "description": "<p>Sub Ind1, 'Y' or 'N'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "sub_ind2",
            "description": "<p>Sub Ind2, 'Y' or 'N'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "updated_by",
            "description": "<p>Modified By</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "role",
            "description": "<p>Role, only 'CORP_STAFF' or 'CORP_PURCH'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "exp_date",
            "description": "<p>Role, only 'CORP'</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "primary_details",
            "description": "<p>Primary Details</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.postal_code",
            "description": "<p>Postal code</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.street_line1",
            "description": "<p>Address line 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.street_line2",
            "description": "<p>Address line 2</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.street_line3",
            "description": "<p>Floor</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.street_line4",
            "description": "<p>Unit</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "primary_details.country_id",
            "description": "<p>Country Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "delivery_details",
            "description": "<p>Delivery Details</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.postal_code",
            "description": "<p>Postal code</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.street_line1",
            "description": "<p>Address line 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.street_line2",
            "description": "<p>Address line 2</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.street_line3",
            "description": "<p>Floor</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.street_line4",
            "description": "<p>Unit</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "delivery_details.country_id",
            "description": "<p>Country Id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"mbr_id\": \"V2100038AS\",\n    \"contact\": \"81431879\",\n    \"email\": \"test@test.com\",\n    \"mbr_pwd\": \"23411\",\n    \"first_name\": \"xx\",\n    \"last_name\": \"xxxxx\",\n    \"status_level\": 0,\n    \"updated_by\": \"3811\",\n    \"role\": \"CORP_PURCH\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Successfully updated\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "role error",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Associate role must be 'CORP_STAFF' or 'CORP_PURCH'.\"\n }",
          "type": "json"
        },
        {
          "title": "role error",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 404,\n     \"message\": \"Only CORP_STAFF and CORP_PURCH can change role.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/CorporateController.php",
    "groupTitle": "7.CORPORATE",
    "name": "PostApiUpdate_member"
  },
  {
    "type": "get",
    "url": "/api/contract/{contract_id}",
    "title": "Get Contract",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"list\": {\n             \"contract_id\": \"CC2110000010\",\n             \"cust_id\": \"LL0007\",\n             \"item_id\": \"!MEMBER-SUB28\",\n             \"status_level\": 2,\n             \"contract_start\": \"2021-10-20 00:00:00\",\n             \"contract_end\": \"2024-02-20 00:00:00\",\n             \"contract_amount\": \"28.00000000\",\n             \"cycle_num\": 1,\n             \"cycle_date\": \"2024-02-20 00:00:00\",\n             \"cycle_period\": \"28.00\",\n             \"ref_sys\": \"VC\",\n             \"ref_id\": \"\",\n             \"item_data\": \"[]\",\n             \"contract_data\": \"[]\",\n             \"created_by\": \"VC\",\n             \"created_on\": \"2021-10-14 14:48:04\",\n             \"modified_by\": \"VC\",\n             \"modified_on\": \"2021-10-14 14:48:04\"\n          },\n         \"trans\": [{\n             \"contract_id\": \"CC2110000010\",\n             \"cycle_num\": 0,\n             \"retry_num\": 0,\n             \"trans_date\": \"2021-10-20 00:00:00\",\n             \"trans_id\": \"TEST0001\",\n             \"trans_amount\": \"28.00000000\",\n             \"custom_data\": \"[]\",\n             \"created_by\": \"3810\",\n             \"created_on\": \"2021-10-20 15:30:48\",\n             \"modified_by\": \"3810\",\n             \"modified_on\": \"2021-10-20 15:30:48\"\n         }]\n     }\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractContract_id"
  },
  {
    "type": "get",
    "url": "/api/contract/{contract_id}/cancel",
    "title": "Cancel Contract",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Cancel Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractContract_idCancel"
  },
  {
    "type": "get",
    "url": "/api/contract/{contract_id}/check",
    "title": "Check Contract",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Check Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractContract_idCheck"
  },
  {
    "type": "get",
    "url": "/api/contract/{contract_id}/terminate",
    "title": "Terminate Contract",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Terminate Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractContract_idTerminate"
  },
  {
    "type": "get",
    "url": "/api/contract/customer/{mbr_id}",
    "title": "Get Customer",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_id",
            "description": "<p>Cust ID or mbr_id or email_addr or contact_number</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get Success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"cust_info\": {\n             \"cust_id\": \"LL0007\",\n             \"mbr_id\": \"V214630300\",\n             \"first_name\": \"leon\",\n             \"last_name\": \"lee\",\n             \"email_addr\": \"li.liangze@challenger.sg\",\n             \"contact_num\": \"81431879\",\n             \"created_by\": \"VC\",\n             \"created_on\": \"2021-10-14 14:48:04\",\n             \"modified_by\": \"VC\",\n             \"modified_on\": \"2021-10-14 14:48:04\"\n          },\n         \"agreement_list\": []\n     }\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractCustomerMbr_id"
  },
  {
    "type": "get",
    "url": "/api/contract/partner/valueclub/activate/{mbr_type}/{contract_id}/{trans_id}",
    "title": "Member Activate",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_type",
            "description": "<p>Member Type, should be 08 or 28</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Activate Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractPartnerValueclubActivateMbr_typeContract_idTrans_id"
  },
  {
    "type": "get",
    "url": "/api/contract/partner/valueclub/expire/{contract_id}",
    "title": "Member Deactivate",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Deactivate Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractPartnerValueclubExpireContract_id"
  },
  {
    "type": "get",
    "url": "/api/contract/partner/valueclub/register/{contract_id}",
    "title": "Member Register",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Register Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractPartnerValueclubRegisterContract_id"
  },
  {
    "type": "get",
    "url": "/api/contract/partner/valueclub/renew/{mbr_type}/{contract_id}/{trans_id}",
    "title": "Member Renew",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mbr_type",
            "description": "<p>Member Type, should be 08 or 28</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Transaction ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Activate Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "GetApiContractPartnerValueclubRenewMbr_typeContract_idTrans_id"
  },
  {
    "type": "post",
    "url": "/api/contract/create",
    "title": "Create Contract",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cust_id",
            "description": "<p>Customer ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "item_id",
            "description": "<p>Item ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_start",
            "description": "<p>Contract Start</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "contract_end",
            "description": "<p>Default is contract_period</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "contract_amount",
            "description": "<p>Default is item amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "cycle_period",
            "description": "<p>Default is item cycle_period</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ref_id",
            "description": "<p>Default is ''</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "created_by",
            "description": "<p>Default is 'VC'</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"cust_id\": \"LL0007\",\n    \"item_id\": \"!MEMBER-SUB28\",\n    \"contract_start\": \"2021-10-20 00:00:00\",\n    \"ref_sys\": \"VC\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"contract_id\": \"CC2110000010\",\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "PostApiContractCreate"
  },
  {
    "type": "post",
    "url": "/api/contract/customer",
    "title": "Create customer",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contact_num",
            "description": "<p>Contact number</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email_addr",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"contact\": \"81431879\",\n    \"email\": \"test@test.com\",\n    \"first_name\": \"xx\",\n    \"last_name\": \"xxxxx\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"cust_id\": \"xxx\",\n     },\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "PostApiContractCustomer"
  },
  {
    "type": "post",
    "url": "/api/contract/pay",
    "title": "Pay Contract",
    "version": "1.0.0",
    "group": "8.Contract",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "contract_id",
            "description": "<p>Contract ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_id",
            "description": "<p>Trans ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_date",
            "description": "<p>Trans Date</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "trans_amount",
            "description": "<p>Trans Amount</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "created_by",
            "description": "<p>Default is 'VC'</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"contract_id\": \"CC2110000010\",\n    \"trans_id\": \"TEST0001\",\n    \"trans_date\": \"2021-10-20 00:00:00\",\n    \"trans_amount\": 28\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Register success",
          "content": "HTTP/1.1 200 OK\n {\n     \"info\": \"Payment Successful\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Key code invalid",
          "content": "HTTP/1.1 Key code invalid\n {\n     \"status\": \"error\",\n     \"status_code\": 500,\n     \"message\": \"Key code invalid\"\n }",
          "type": "json"
        },
        {
          "title": "Member Server error",
          "content": "HTTP/1.1 server error\n {\n     \"status\": \"error\",\n     \"status_code\": 0500101,\n     \"message\": \"Server problem, cannot register member.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/ContractController.php",
    "groupTitle": "8.Contract",
    "name": "PostApiContractPay"
  },
  {
    "type": "get",
    "url": "/api/member/points/balance",
    "title": "Balance Points.",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return balance points",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": \"x.xx\",\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsBalance"
  },
  {
    "type": "get",
    "url": "/api/member/points/enough/{points}",
    "title": "Points is Enough",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return boolean",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": true,  //or false\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsEnoughPoints"
  },
  {
    "type": "get",
    "url": "/api/member/points/nextexpiry",
    "title": "Next Expiry Date",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return Next Expiry Date",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"date\": \"2019-12-31 00:00:00.000000\",\n             \"timezone_type\": 3,\n             \"timezone\": \"Asia/Singapore\"\n         }\n     ]\n     \"status\": \"success\",\n     \"status_code\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsExpiryController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsNextexpiry"
  },
  {
    "type": "get",
    "url": "/api/member/points/nextline",
    "title": "Get Next Line Number",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return next line number",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": 2,\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsNextline"
  },
  {
    "type": "get",
    "url": "/api/member/points/recentexpiry",
    "title": "Recent Expiring Points",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return recent expiring points",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"coy_id\": \"CTL\",\n             \"mbr_id\": \"0966048869\",\n             \"line_num\": 1,\n             \"exp_date\": \"2028-06-30 00:00:00\",\n             \"points_accumulated\": \"1160.00\",\n             \"points_redeemed\": \"1124.00\",\n             \"points_expired\": \"36.00\",\n             \"created_by\": \"2198\",\n             \"created_on\": \"2016-01-03 17:00:02.416\",\n             \"modified_by\": \"cherps\",\n             \"modified_on\": \"2018-07-01 01:02:24.06\",\n             \"updated_on\": \"2016-03-19 19:15:00.783\"\n         }\n     ]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsRecentexpiry"
  },
  {
    "type": "get",
    "url": "/api/member/points/summary",
    "title": "Summary Points",
    "version": "1.0.0",
    "group": "VC-APP",
    "success": {
      "examples": [
        {
          "title": "Return Summary points",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"points_accumulated\": 234,\n             \"points_redeemed\": 117,\n             \"points_expired\": 0\n         }\n     ]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Points/PointsController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberPointsSummary"
  },
  {
    "type": "get",
    "url": "/api/member/transaction/myredemption/{fdate}/{tdate}",
    "title": "Get My Redemption",
    "version": "1.0.0",
    "group": "VC-APP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fdate",
            "description": "<p>2018-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tdate",
            "description": "<p>2018-12-31</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Return all redemption data",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"coy_id\": \"CTL\",\n             \"mbr_id\": \"+0966048869\",\n             \"trans_id\": \"V2A33747\",\n             \"line_num\": 3,\n             \"trans_type\": \"RD\",\n             \"trans_date\": \"2018-07-24 17:30:43.546\",\n             \"loc_id\": \"VC\",\n             \"pos_id\": \"V2\",\n             \"item_id\": \"0811571016525\",\n             \"item_desc\": \"XXGoogle ChromeCast 2 (Black) 1-Year Warranty by Challenger\",\n             \"item_qty\": \"1.00\",\n             \"regular_price\": \"65.00\",\n             \"unit_price\": \"65.00000000\",\n             \"disc_percent\": \"0.00\",\n             \"disc_amount\": \"0.00\",\n             \"trans_points\": \"-65.00\",\n             \"salesperson_id\": \"3503\",\n             \"mbr_savings\": \"0.00\",\n             \"created_by\": \"3503\",\n             \"created_on\": \"2018-07-24 17:32:36.183\",\n             \"modified_by\": \"3503\",\n             \"modified_on\": \"2018-07-24 17:32:36.183\",\n             \"updated_on\": \"2018-07-24 17:32:37.97\",\n             \"trdate\": \"2018-07-24\",\n             \"description\": \"Rebates Redeemed\",\n             \"store\": \"Hachi.tech\",\n             \"ho_id\": \"HIB18824\",\n             \"inv_id\": \"\"\n         },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get redemption error",
          "content": "HTTP/1.1 Get redemption error\n {\n     \"status\": \"error\",\n     \"status_code\": 400,\n     \"message\": \"Something wrong to get redemption\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Transaction/TransactionController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberTransactionMyredemptionFdateTdate"
  },
  {
    "type": "get",
    "url": "/api/member/transaction/mytransaction/{fdate}/{tdate}/{tfrom?}",
    "title": "My Transaction",
    "version": "1.0.0",
    "group": "VC-APP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fdate",
            "description": "<p>2018-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tdate",
            "description": "<p>2018-12-31</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "tfrom",
            "description": "<p>'CTL' or 'HSG', default all trans_type</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Return all transaction data",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"coy_id\": \"CTL\",\n             \"mbr_id\": \"+0966048869\",\n             \"trans_id\": \"NPP100000248464\",\n             \"line_num\": 1,\n             \"trans_type\": \"HR\",\n             \"trans_date\": \"2016-11-25 16:04:13.053\",\n             \"loc_id\": \"NP\",\n             \"pos_id\": \"P1\",\n             \"item_id\": \"0763649075807\",\n             \"item_desc\": \"Seagate Backup Plus Portable 2TB Gold 2998185\",\n             \"regular_price\": \"159.00\",\n             \"unit_price\": \"129.00000000\",\n             \"disc_percent\": \"0.00\",\n             \"disc_amount\": \"0.00\",\n             \"trans_points\": \"129.00\",\n             \"salesperson_id\": \"\",\n             \"mbr_savings\": \"0.00\",\n             \"created_by\": \"2779\",\n             \"created_on\": \"2016-11-25 16:06:19.336\",\n             \"modified_by\": \"2779\",\n             \"modified_on\": \"2016-11-25 16:06:19.336\",\n             \"updated_on\": \"2016-11-25 16:06:20.09\",\n             \"tradte\": \"2016-11-25 16:04:13.053\",\n             \"item_qty\": \"1.00\",\n             \"ref_key\": \"97D91718-427A-4B5B-BAFE-89CC9FCD8689\",\n             \"store\": \"Hachi.tech\",\n             \"trdate\": \"2019-10-31\",\n             \"inv_type\": 2,\n             \"ho_id\": \"HIB18824\"\n         },\n     ]\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get transaction error",
          "content": "HTTP/1.1 Get transaction error\n {\n     \"status\": \"error\",\n     \"status_code\": 400,\n     \"message\": \"Something wrong to get transaction\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Transaction/TransactionController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberTransactionMytransactionFdateTdateTfrom"
  },
  {
    "type": "get",
    "url": "/api/member/transaction/mywarranty/{fdate}/{tdate}/{tfrom?}",
    "title": "My Warranty",
    "version": "1.0.0",
    "group": "VC-APP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fdate",
            "description": "<p>2018-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tdate",
            "description": "<p>2018-12-31</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Return all warranty data",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"ref_key\": \"\",\n             \"coy_id\": \"CTL\",\n             \"mbr_id\": \"+0966048869\",\n             \"ho_id\": \"A3C36416\",\n             \"inv_type\": 2,\n             \"trans_id\": \"V2A33747\",\n             \"line_num\": 3,\n             \"trans_type\": \"RD\",\n             \"trans_date\": \"2018-07-24 17:30:43.546\",\n             \"loc_id\": \"VC\",\n             \"pos_id\": \"V2\",\n             \"item_id\": \"0811571016525\",\n             \"item_desc\": \"XXGoogle ChromeCast 2 (Black) 1-Year Warranty by Challenger\",\n             \"item_qty\": \"1.00\",\n             \"regular_price\": \"65.00\",\n             \"unit_price\": \"65.00000000\",\n             \"disc_percent\": \"0.00\",\n             \"disc_amount\": \"0.00\",\n             \"trans_points\": \"-65.00\",\n             \"salesperson_id\": \"3503\",\n             \"mbr_savings\": \"0.00\",\n             \"created_by\": \"3503\",\n             \"created_on\": \"2018-07-24 17:32:36.183\",\n             \"modified_by\": \"3503\",\n             \"modified_on\": \"2018-07-24 17:32:36.183\",\n             \"updated_on\": \"2018-07-24 17:32:37.97\",\n             \"store\": \"Hachi.tech\",\n             \"trdate\": \"2018-07-24\",\n             \"rownum\": 1,\n             \"serial_num\": \"SDMPD68LWLMPD\",\n             \"mode_id\": \"MUUJ2ZP/A\",\n             \"item_model_id\": \"MUUJ2ZP/A\",\n             \"warranty_month\": \"12\",\n             \"ext_warranty_month\": 24,\n             \"ext_warranty_id\": \"EW2020000032866\",\n             \"prod_serial_no\": \"SDMPD68LWLMPD\",\n             \"item_type\": \"IPAD WIFI\",\n             \"warranty_year\": \"1\",\n             \"ext_warranty_invoice_id\": \"A3C36416\"\n         },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Transaction/TransactionController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberTransactionMywarrantyFdateTdateTfrom"
  },
  {
    "type": "get",
    "url": "/api/member/voucher/myegift/:fdate/:tdate",
    "title": "EGift",
    "version": "1.0.0",
    "group": "VC-APP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fdate",
            "description": "<p>2018-01-01</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tdate",
            "description": "<p>2018-12-31</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Return all egift data",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"valid_until\": \"2018-09-13 23:59:00\",\n             \"amount\": \"$10.00\",\n             \"coupon_serialno\": \"BFB1VE-00025836\",\n             \"status\": \"Expired\"\n         },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get eGift error",
          "content": "HTTP/1.1 Get eGift error\n {\n     \"status\": \"error\",\n     \"status_code\": 400,\n     \"message\": \"Something wrong to get eGift\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Voucher/VoucherController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberVoucherMyegiftFdateTdate"
  },
  {
    "type": "get",
    "url": "/api/member/voucher/myvouchers/:fdate/:tdate",
    "title": "My Vouchers",
    "version": "1.0.0",
    "group": "VC-APP",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fdate",
            "description": "<p>2018-01-01, default is last month</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tdate",
            "description": "<p>2018-12-31, default is today</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Return all voucher data",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": [\n         {\n             \"trans_id\": \"\",\n             \"status_level\": 1,\n             \"issue_date\": \"2018-08-08 12:03:06\",\n             \"coupon_serialno\": \"PCNB8-8DQ0WO\",\n             \"issue_type\": \"eVoucher\",\n             \"expiry_date\": \"2018-08-08 12:03:06\",\n             \"receipt_id2\": \"HIA16174\",\n             \"mbr_id\": \"S9222398A\",\n             \"status\": \"Redeemed\",\n             \"valid_status\": 0,\n             \"qty_reserved\": 1\n         },\n     ],\n     \"status\": \"success\",\n     \"status_code\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get voucher error",
          "content": "HTTP/1.1 Get voucher error\n {\n     \"status\": \"error\",\n     \"status_code\": 400,\n     \"message\": \"Something wrong to get voucher\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Member/Voucher/VoucherController.php",
    "groupTitle": "VC-APP",
    "name": "GetApiMemberVoucherMyvouchersFdateTdate"
  }
] });
