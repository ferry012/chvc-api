define({
  "name": "VALUECLUB API",
  "version": "1.0.1",
  "description": "API Document of Valueclub System",
  "title": "VC-API",
  "template": {
    "forceLanguage": "en"
  },
  "header": {
    "title": "Authorization",
    "content": "<hr>\n<hr>\n<h2>Authorization</h2>\n<p>All API must be authenticated. Please request for Key code from Challenger Software Team for development.</p>\n<style>\ntable th:first-of-type {\n    width: 200px;\n}\n</style>\n<h3><strong>URL AUTH:</strong></h3>\n<pre class=\"prettyprint lang-shell\">http://{api_url}?key_code={key}\n</pre>\n<h3><strong>HEADER AUTH:</strong></h3>\n<pre class=\"prettyprint lang-shell\">curl -X GET http://localhost/api/validate \\\n--header 'Authorization:Bearer {key}' \\\n--data '{\"mbr_id\":\"V00000000\"}'\n</pre>\n"
  },
  "footer": {
    "title": "Error Code",
    "content": "<h1>Exception coding</h1>\n<style>\ntable th:first-of-type {\n    width: 200px;\n}\n</style>\n<h3>Error code format description</h3>\n<table>\n<thead>\n<tr>\n<th>01</th>\n<th>003</th>\n<th>01</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Login</td>\n<td>Rest password API</td>\n<td>Specific error</td>\n</tr>\n</tbody>\n</table>\n<h3>Login</h3>\n<table>\n<thead>\n<tr>\n<th>Error Code</th>\n<th>Description</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>100101</td>\n<td>Login: Username or password is wrong</td>\n</tr>\n<tr>\n<td>100102</td>\n<td>Login: Login locked</td>\n</tr>\n<tr>\n<td>100103</td>\n<td>Login: Member type is forbid</td>\n</tr>\n<tr>\n<td>100104</td>\n<td>Login: Member status is inactivated</td>\n</tr>\n<tr>\n<td>100201</td>\n<td>Logout: You do not login</td>\n</tr>\n<tr>\n<td>100301</td>\n<td>Reset Password: Validator failed</td>\n</tr>\n<tr>\n<td>100302</td>\n<td>Reset Password: Password incorrect</td>\n</tr>\n<tr>\n<td>100304</td>\n<td>Reset Password: Server breakdown</td>\n</tr>\n<tr>\n<td>100501</td>\n<td>Update Info: Server breakdown</td>\n</tr>\n<tr>\n<td>100502</td>\n<td>Update Info: Validate error</td>\n</tr>\n</tbody>\n</table>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-08-04T02:44:28.831Z",
    "url": "https://apidocjs.com",
    "version": "0.28.1"
  }
});
